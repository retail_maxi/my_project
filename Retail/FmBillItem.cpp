//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmBillItem.h"                                
#include "FmGetBuhDoc.h"
#include "FmReplaceDocNum.h"
#include "FmReplaceReason.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTWITHLINES"
#pragma link "MLDBPanel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_get_acc_docUpdate(TObject *Sender)
{
  m_ACT_get_acc_doc->Enabled = m_dm->UpdateRegimeItem != urView && !m_dm->m_Q_linesID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_get_acc_docExecute(TObject *Sender)
{
  TFGetBuhDoc* f = new TFGetBuhDoc(this,m_dm->m_DB_main->Handle,m_dm);
  try
  {
    f->ShowModal();
    if (f->PressOK)
    {
      if( m_dm->m_Q_item->Active ) m_dm->m_Q_item->Close();
      m_dm->m_Q_item->ParamByName("ACC_DOC_ID")->AsInteger = f->m_Q_listID->AsInteger;
      m_dm->m_Q_item->Open();
      if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();

      if (!m_dm->m_Q_itemCNT_ID->IsNull && m_dm->m_Q_itemCNT_ID->AsInteger != f->m_Q_listSENDER_ID->AsInteger)
      {
        Application->MessageBox(AnsiString("��������� ����������� ��������� �� ����� �� ��������� � ����������� ���.���������! ���������� ������� ������ ���.��������!").c_str(),
                                    Application->Title.c_str(),
                                    MB_ICONERROR | MB_OK);
        return;
      }

      if (!m_dm->m_Q_itemDOC_NUM->IsNull)
      {
        Application->MessageBox(AnsiString("���.�������� ��� �������� � ����� �" + m_dm->m_Q_itemDOC_NUM->AsString + " ���������� ������� ������ ���.��������!").c_str(),
                                    Application->Title.c_str(),
                                    MB_ICONERROR | MB_OK);
        return;
      }
      m_dm->m_Q_itemACC_DOC_ID->AsInteger = f->m_Q_listID->AsInteger;
      m_dm->m_Q_itemACC_DOC_NUMBER->AsString = f->m_Q_listPPD_DOC_NUMBER->AsString;
      m_dm->m_Q_itemCNT_NAME->AsString = f->m_Q_listPPD_SENDER->AsString;
      m_dm->m_Q_itemACC_DOC_DATE->AsDateTime = f->m_Q_listPPD_ISSUED_DATE->AsDateTime;
      m_dm->m_Q_itemACC_DOC_AMOUNT->AsFloat = f->m_Q_listPPD_AMOUNT->AsFloat;
      m_dm->m_Q_itemACC_NOTE->AsString = f->m_Q_listPPD_NOTE->AsString;
      m_dm->m_Q_itemMNGR_NOTE->AsString = f->m_Q_listM_NOTE->AsString;

      m_dm->ApplyUpdatesItem();
      
      m_dm->m_Q_lines->Close();
      m_dm->m_Q_lines->Open();
    }
  }
  __finally
  {
    if (f) delete f;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_appendUpdate(TObject *Sender)
{
  (static_cast<TAction*>(Sender))->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_DBG_linesGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
  if (!State.Contains(gdSelected) &&
    (Column->FieldName == "STORED_QNT" || Column->FieldName == "NON_STORED_QNT") &&
    m_dm->m_Q_linesDELTA_QNT->AsFloat != 0
  )
  {
    Background = clYellow;
    AFont->Color = clWindowText;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::FormShow(TObject *Sender)
{
  TTSFDocument::FormShow(Sender);
  m_dm->ViewOnlyVldArp = true;
  m_CB_view_only_vld_arp->Checked = m_dm->ViewOnlyVldArp;
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_CB_view_only_vld_arpClick(TObject *Sender)
{
  m_dm->ViewOnlyVldArp = m_CB_view_only_vld_arp->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_replaceExecute(TObject *Sender)
{
  TSExecModuleDlg<TFReplaseDocNum>(this, m_dm);
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_replaceUpdate(TObject *Sender)
{
 m_ACT_replace->Enabled = (m_dm->UpdateRegimeItem == urEdit)
                            && (m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->m_fldr_rdy) ;

}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_reasonExecute(TObject *Sender)
{
  if(!m_dm->m_Q_reason->Active )
  {
     m_dm->m_Q_reason->Open();
  }
  if(m_dm->m_Q_reason->Active )
  {
    m_dm->m_Q_reason->Close();
    m_dm->m_Q_reason->Open();
  }
  m_dm->m_Q_reason->Edit();
  if (TSExecModuleDlg<TFReplaseReason>(this, m_dm))
  {
    m_dm->m_Q_reason->ApplyUpdates();
    m_dm->RefreshItemDataSets();
  }
  else
    m_dm->m_Q_reason->CancelUpdates();
}
//---------------------------------------------------------------------------

void __fastcall TFBillItem::m_ACT_reasonUpdate(TObject *Sender)
{
  m_ACT_reason->Enabled = (m_dm->UpdateRegimeItem == urEdit)
                            && (!m_dm->m_Q_linesDELTA_QNT->AsFloat == 0)
                            && (!m_dm->m_Q_linesQNT->IsNull)
                            && (m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->m_fldr_rdy) ;
}
//---------------------------------------------------------------------------


void __fastcall TFBillItem::m_ACT_prev_fldrExecute(TObject *Sender)
{
    if( m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();

    long route_id = m_dm->m_Q_folderROUTE_PREV_ID->AsInteger;

    if (route_id == 4827)
    {
      if( Application->MessageBox("��� ����������� ���� ����� �������� � ��� �������� ����� ����� �������� �� �����\r\n � ���������� � ����� �����. \r\n ����������?",
                                    Application->Title.c_str(),
                                    MB_YESNO | MB_ICONQUESTION) != IDYES )
      {
         return;
      }
    }
    //TTSFDocumentWithLines::m_ACT_prev_fldrExecute(Sender)
    Close();
    m_dm->ChangeFldr(route_id,m_dm->m_Q_itemID->AsInteger);
    m_dm->RefreshListDataSets(true);
}
//---------------------------------------------------------------------------

