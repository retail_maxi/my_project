inherited DMessageFromRKU: TDMessageFromRKU
  OldCreateOrder = False
  Left = 257
  Top = 114
  Height = 640
  Width = 858
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'SELECT d.id               AS id,'
      '       d.src_org_id       AS doc_src_org_id,'
      '       o.name             AS doc_src_org_name,'
      '       d.create_date      AS doc_create_date,'
      '       d.comp_name        AS doc_create_comp_name,'
      '       d.author           AS doc_create_user,'
      '       d.modify_date      AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user      AS doc_modify_user,'
      '       d.status           AS doc_status,'
      
        '       decode(d.status, '#39'F'#39','#39'��������'#39', '#39'D'#39','#39'�����'#39', '#39'W'#39','#39'�����' +
        '� �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 250) AS d' +
        'oc_fldr_right,'
      '       m.doc_number,'
      '       mt.name message_type,'
      '       m.host,'
      '       m.rku,'
      '       m.read_date,'
      '       m.read_user_id,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.read_user_id, 1),' +
        ' 250)) read_user_name'
      '  FROM documents        d,'
      '       folders          f,'
      '       organizations    o,'
      '       doc_messages_rku m,'
      '       messages_types   mt'
      ' WHERE f.doc_type_id = :DOC_TYPE_ID'
      '   AND f.id = d.fldr_id'
      '   AND o.id = d.src_org_id'
      '   AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '   AND m.doc_id = d.id'
      '   AND m.type_message = mt.id(+)'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    object m_Q_listDOC_NUMBER: TFloatField
      DisplayLabel = '� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'm.DOC_NUMBER'
    end
    object m_Q_listMESSAGE_TYPE: TStringField
      DisplayLabel = '��� ���������'
      FieldName = 'MESSAGE_TYPE'
      Origin = 'm.MESSAGE_TYPE'
      Size = 250
    end
    object m_Q_listHOST: TStringField
      DisplayLabel = '��� ����������'
      FieldName = 'HOST'
      Origin = 'm.host'
      Size = 64
    end
    object m_Q_listRKU: TFloatField
      DisplayLabel = '� ���'
      FieldName = 'RKU'
      Origin = 'm.RKU'
    end
    object m_Q_listREAD_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'READ_DATE'
      Origin = 'm.READ_DATE'
    end
    object m_Q_listREAD_USER_ID: TFloatField
      DisplayLabel = 'ID ������������ ������������'
      FieldName = 'READ_USER_ID'
      Origin = 'm.READ_USER_ID'
    end
    object m_Q_listREAD_USER_NAME: TStringField
      DisplayLabel = '��� ������������ ������������'
      FieldName = 'READ_USER_NAME'
      Origin = 'TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.read_user_id, 1), 250))'
      Size = 250
    end
  end
  inherited m_Q_item: TMLQuery
    BeforeClose = m_Q_itemBeforeClose
    SQL.Strings = (
      'SELECT m.doc_id AS ID,'
      '       m.doc_number,'
      '       m.doc_date,'
      '       m.type_message type_message_id,'
      '       mt.name message_type,'
      '       m.host,'
      '       m.rku,'
      '       m.read_date,'
      '       m.read_user_id,'
      '       m.note,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.read_user_id, 1),' +
        ' 250)) read_user_name'
      '  FROM doc_messages_rku m, messages_types mt'
      ' WHERE m.doc_id = :ID'
      '   AND m.type_message = mt.id(+)'
      
        '   AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_i' +
        'd = :FLDR_ID)')
    object m_Q_itemDOC_NUMBER: TFloatField
      FieldName = 'DOC_NUMBER'
    end
    object m_Q_itemTYPE_MESSAGE_ID: TFloatField
      FieldName = 'TYPE_MESSAGE_ID'
    end
    object m_Q_itemMESSAGE_TYPE: TStringField
      FieldName = 'MESSAGE_TYPE'
      Size = 250
    end
    object m_Q_itemHOST: TStringField
      FieldName = 'HOST'
      Size = 64
    end
    object m_Q_itemRKU: TFloatField
      FieldName = 'RKU'
    end
    object m_Q_itemREAD_DATE: TDateTimeField
      FieldName = 'READ_DATE'
    end
    object m_Q_itemREAD_USER_ID: TFloatField
      FieldName = 'READ_USER_ID'
    end
    object m_Q_itemREAD_USER_NAME: TStringField
      FieldName = 'READ_USER_NAME'
      Size = 250
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_messages_rku'
      'set'
      ' TYPE_MESSAGE = :TYPE_MESSAGE_ID,'
      '  HOST = :HOST,'
      '  RKU = :RKU,'
      '  NOTE = :NOTE'
      'where'
      '  DOC_ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into doc_messages_rku'
      
        '  (DOC_ID, TYPE_MESSAGE, HOST, RKU, READ_DATE, READ_USER_ID, DOC' +
        '_NUMBER, DOC_DATE, NOTE)'
      'values'
      
        '  (:ID, :TYPE_MESSAGE_ID, :HOST, :RKU, :READ_DATE, :READ_USER_ID' +
        ', :DOC_NUMBER,:DOC_DATE, :NOTE)')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      'END;')
  end
  object m_Q_get_cnt_id: TQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT e.person_cnt_id cnt_id'
      '  FROM employeers e'
      ' WHERE e.user_name = USER')
    Left = 235
    Top = 305
    object m_Q_get_cnt_idCNT_ID: TFloatField
      FieldName = 'CNT_ID'
      Origin = 'TSDBMAIN.EMPLOYEERS.PERSON_CNT_ID'
    end
  end
  object m_Q_type_mess_list: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT ID, NAME FROM messages_types'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 45
    Top = 305
    object m_Q_type_mess_listID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_type_mess_listNAME: TStringField
      DisplayLabel = '��� ���������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
  end
  object m_Q_user_list: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT cnt_id,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(cnt_id, 1), 250)) c' +
        'nt_name'
      '  FROM persons_contractors')
    Macros = <>
    Left = 150
    Top = 305
    object m_Q_user_listCNT_ID: TFloatField
      FieldName = 'CNT_ID'
    end
    object m_Q_user_listCNT_NAME: TStringField
      FieldName = 'CNT_NAME'
      Size = 250
    end
  end
end
