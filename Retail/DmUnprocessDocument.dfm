inherited DUnprocessDocument: TDUnprocessDocument
  OldCreateOrder = False
  Left = 1065
  Top = 178
  Width = 741
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_main: TMLQuery
    SQL.Strings = (
      'select'
      '   org_name,'
      '   doc_id,'
      '   doc_name, '
      '   create_date, '
      '   author,'
      '   cnt_id,'
      '   position,'
      '   modify_user,'
      '   fldr_name,'
      '   modify_date,'
      '   sender_name,'
      '   receiver_name,'
      '   supplier_name,'
      '   dop_info'
      'from'
      '('
      'select'
      '    NULL as org_name,'
      '    NULL as doc_id,'
      '    '#39'����� ��������'#39'   as doc_name, '
      '    NULL as create_date, '
      '    NULL as author,'
      '    NULL as cnt_id,'
      '    NULL as position,'
      '    NULL as modify_user,'
      '    NULL as fldr_name,'
      '    NULL as modify_date,'
      '    NULL as sender_name,'
      '    NULL as receiver_name,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      'from dual'
      'union all'
      'select'
      '    o.name as org_name,'
      '    v.doc_id,'
      '    dt.name as doc_name, '
      '    v.create_date, '
      
        '    trim(rpad(pkg_persons.fnc_user_full_name(v.author, v.create_' +
        'date),150)) as author,'
      '    sm.cnt_id as cnt_id,'
      '    st.position as position,'
      
        '    trim(rpad(pkg_persons.fnc_user_full_name(v.modify_user, v.cr' +
        'eate_date),150)) as modify_user,'
      '    f.name as fldr_name,'
      '    v.modify_date,'
      '    src.name as sender_name,'
      '    dest.name as receiver_name,'
      '    supplier_name,'
      '    dop_info'
      'from ('
      '  --��� ������� ������� '
      '  select /*+ INDEX(D XPK_DOCUMENTS)*/'
      '    d.src_org_id, '
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    i.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      
        '    trim( rpad( pkg_contractors.fnc_contr_full_name( ppd.sender_' +
        'id ), 250 ) ) as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    rtl_incomes  i,'
      '    prepay_documents ppd'
      
        '  where i.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and i.doc_id = d.id'
      '    and d.fldr_id = 2432 --�����'
      '    and ppd.doc_id = i.acc_doc_id'
      '  union all'
      
        '  --������������� ���� ������� ������� (� �.�.��); �������������' +
        '�� ������'
      '  -- ��� ����������� ��������; �������� ������� � ��������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    r.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    rtl_edit_qnt   r'
      
        '  where r.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and r.id = d.id'
      
        '    and (d.fldr_id in ( 2448, 2443, 2444, 2451, 2452, 2446, 9801' +
        '4, 7077154, 6534889, 8597996, 13712638 ) --�����, �����������, �' +
        '���������� ����'
      '        or (d.fldr_id = 6534890'
      '            and not exists (select 1 from doc_folders_history h'
      
        '                            where h.doc_id = r.id and h.src_fldr' +
        '_id = 6534889'
      '                            and h.dest_fldr_id = 6534890'
      '                            and user_name = '#39'ROBOT'#39')))'
      '  union all'
      '  -- ��� ����������� � ��� ��������� � ��'
      '  select '
      '    t.dest_org_id as src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    t.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    os.id as sender_id,'
      '    t.dest_org_id as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    doc_act_ttn_in t,'
      '    organizations o,'
      '    organizations os'
      
        '  where t.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and t.doc_id = d.id'
      '    and d.fldr_id in  (98014,7077163) --�����,����������'
      '    and o.id = t.dest_org_id AND o.enable = '#39'Y'#39
      '    AND os.id = d.src_org_id'
      '  union all'
      ''
      '  --�������- ��-��������� �� �������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    a.doc_date as create_date,                      '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      
        '    trim( rpad( pkg_contractors.fnc_contr_full_name( a.sender_id' +
        ' ), 250 ) ) as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    return_acts  a'
      
        '  where a.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <> '#39'D'#39
      '    and a.doc_id = d.id'
      
        '    and d.fldr_id in  ( 2454, 2459 ) --�����, �� ��������, �����' +
        '��������'
      '  union all'
      '   --���� ��������������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id,   '
      '    a.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    rtl_inv_acts   a'
      
        '  where a.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and a.doc_id = d.id'
      
        '    and d.fldr_id in  (2458,2465) --�������, � ������, ���������' +
        '���'
      '  union all'
      '    --���� ��� �������'
      '  select /*+ INDEX(D XPK_DOCUMENTS)*/'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    b.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    b.src_org_id as sender_id,'
      '    d.src_org_id as receiver_id,'
      '    NULL as supplier_name,'
      
        '   substr(nvl2(b.cnt_id,'#39'���������: '#39'||pkg_contractors.fnc_contr' +
        '_name(b.cnt_id)||'#39';'#39','#39#39')||nvl2(tp.cnt_id,'#39' ��������: '#39'||pkg_cont' +
        'ractors.fnc_contr_name(tp.cnt_id),'#39#39'),1,250) as dop_info'
      '  from '
      '    documents d,'
      '    rtl_bills b,'
      '    organizations os,'
      '    taxpayer tp'
      
        '  where b.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and b.doc_id = d.id'
      '    and b.src_org_id = os.id(+)'
      '    and tp.id(+) = b.taxpayer_id'
      '    and d.fldr_id = 2480 --�������'
      '  union all '
      '  --��� ��������� � ���������� ��������� �� (���������) '
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    t.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    t.dest_org_id as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    rtl_ttn_out t,'
      '    organizations o'
      
        '  where                t.doc_date between :BEGIN_DATE and :END_D' +
        'ATE +1-1/24/60 and d.status <>'#39'D'#39
      '    and t.doc_id = d.id'
      '    and d.fldr_id in (2482, 2484) --�����/�������'
      '    and o.id = d.src_org_id'
      '  union all'
      '    --��������� ��� ���'
      '  select /*+ INDEX(D XPK_DOCUMENTS)*/'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pd.issued_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      
        '    trim( rpad( pkg_contractors.fnc_contr_full_name( pd.sender_i' +
        'd ), 250 ) ) as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    prepay_documents pd,'
      '    organizations o'
      
        '  where pd.issued_date between :BEGIN_DATE and :END_DATE +1-1/24' +
        '/60 and d.status <>'#39'D'#39
      '    and pd.doc_id = d.id'
      '    and d.src_org_id = o.id'
      '    and o.org_type_id = 2'
      '    and d.fldr_id in (923,929,910)'
      '    and pd.sender_id != 2005675 --�����, �� ��� �������� �����'
      '  union all'
      '    --��� ������� ������� ��'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    i.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      
        '    trim( rpad( pkg_contractors.fnc_contr_full_name( ppd.sender_' +
        'id ), 250 ) ) as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    fk_incomes i,'
      '    prepay_documents ppd    '
      
        '  where i.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and i.doc_id = d.id'
      '    and d.fldr_id = 7077148 --�����'
      '    and ppd.doc_id = i.acc_doc_id'
      '  union all'
      '    --��� �� ���������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    t.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    t.dest_org_id as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from            '
      '    documents d,'
      '    fk_ttn_out t'
      
        '  where t.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60 ' +
        'and d.status <>'#39'D'#39
      '    and t.doc_id = d.id'
      '    and d.fldr_id = 7077152 --�����'
      '  union all'
      '    --���������� ����������� ����� ��������������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pd.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_displace pd'
      
        '  where pd.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pd.doc_id = d.id'
      '    and d.fldr_id = 7077122 --�����'
      '  union all'
      '    --������� ������������'
      '  select '
      '    d.src_org_id,'
      '    d.id    as doc_id,'
      '    d.doc_type_id, '
      '    pd.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_division pd'
      
        '  where pd.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pd.doc_id = d.id'
      '    and d.fldr_id = 2149 -- �����'
      '  union all'
      
        '    --�������� ������� � �����������; ����������� ������������ �' +
        '� ��� � ��'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pp.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_processing pp'
      
        '  where pp.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pp.doc_id = d.id'
      '    and d.fldr_id in (7077143,7077145) -- �����'
      '  union all'
      '    --�������� ����� � �/� � ������.�������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pd.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_debit pd'
      
        '  where pd.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pd.doc_id = d.id'
      
        '    and d.fldr_id in (7077199, 13712639) -- �����, ����������� �' +
        '���'
      '  union all'
      '    --��� �������������� ��'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pi.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_inv pi'
      
        '  where pi.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pi.doc_id = d.id'
      '    and d.fldr_id in (1842,1843) -- �����, ��������'
      '  union all'
      '    --������������ �� � ������������ � ��'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pp.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_pack pp'
      
        '  where pp.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pp.doc_id = d.id'
      '    and d.fldr_id = 98026 -- �����'
      '  union all'
      '    --��������� �� ������� ��'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    ra.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    return_acts_fk ra'
      
        '  where ra.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and ra.doc_id = d.id'
      '    and d.fldr_id in (7077274,7077276) -- �����, �� ��������'
      '  union all    '
      '    --������� ������ � ������������ � ��������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    po.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    prod_trans_dining po'
      
        '  where po.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status                   <>'#39'D'#39
      '      and po.doc_id = d.id'
      '    and d.fldr_id = 6534885 -- �����'
      '  union all'
      '    --������.����'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    po.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_defect po'
      
        '  where po.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and po.doc_id = d.id'
      '    and d.fldr_id = 1869 -- �����'
      '  union all       '
      '  --������� ������������'
      '  select '
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    pd.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    NULL as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      '  from '
      '    documents d,'
      '    products_division pd'
      
        '  where pd.doc_date between :BEGIN_DATE and :END_DATE +1-1/24/60' +
        ' and d.status <>'#39'D'#39
      '    and pd.doc_id = d.id'
      '    and d.fldr_id = 2149 -- �����'
      '    union all'
      '--���������� �� ���'
      'select s.org_id as src_org_id,'
      '       s.doc_id,'
      '       d.doc_type_id,'
      '       s.doc_date as create_date,'
      '       d.author, '
      '       d.modify_user,'
      '       d.fldr_id,'
      '       d.modify_date,'
      '       NULL as sender_id,'
      '       NULL as receiver_id,'
      '       NULL as supplier_name,'
      '       NULL as dop_info      '
      '  from sales_rtt s,'
      '       documents d'
      ' where d.id = s.doc_id'
      '   and d.status != '#39'D'#39
      '   and d.fldr_id in (13712210, 13712292, 13712307,13712349)'
      
        '   and s.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      'UNION ALL'
      '--��� �������� �����'
      'select ttn.org_id as src_org_id,'
      '       ttn.doc_id,'
      '       d.doc_type_id,'
      '       ttn.doc_date as create_date,'
      '       d.author, '
      '       d.modify_user,'
      '       d.fldr_id,'
      '       d.modify_date,'
      '       NULL as sender_id,'
      '       NULL as receiver_id,'
      
        '       trim( rpad( pkg_contractors.fnc_contr_full_name( ttn.cnt_' +
        'id ), 250 ) ) as supplier_name,'
      '       NULL as dop_info'
      '  from doc_ttn_in_egais ttn,'
      '       documents d'
      ' where d.id = ttn.doc_id'
      '   and d.status != '#39'D'#39
      '   and d.fldr_id in (13712480, --�����'
      '                     13712481, --������������'
      '                     13712515, --�� ����������'
      
        '                     13712513, --��������� �����������(��� ����.' +
        '-����)'
      '                     13712509, --�� �������������'
      '                     13712511, --� �������������'
      '                     13712517, --��������� �����������'
      '                     13712508) --��� �������'
      
        '   and ttn.doc_date between :begin_date and :end_date + 1 - 1/24' +
        '/60'
      '   union all   '
      '  --��� ������� ������� ��� �� �����     '
      '  select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   material_incomes m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.gr_dep_id'
      
        ' and d.fldr_id = TO_NUMBER(fnc_get_constant('#39'FLDR_MATERIAL_INCOM' +
        'E_NEW'#39')) --�����'
      ' union all'
      ' --��� ������� �� ��������� ��� ����� �����'
      ' select d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   material_outcomes m,'
      '   documents d,'
      '   organizations o,'
      '   fin_respons_centers r'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and r.org_id = o.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and m.resp_id = r.id'
      
        ' and d.fldr_id = TO_NUMBER(fnc_get_constant('#39'FLDR_MATERIAL_OUTCO' +
        'ME_NEW'#39')) --�����'
      ' union all'
      ' --��� �������������� ��� '
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   material_inventory m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.gr_dep_id'
      
        ' and d.fldr_id in (TO_NUMBER(fnc_get_constant('#39'FLDR_MAT_INVENTOR' +
        'Y_NEW'#39')), TO_NUMBER(fnc_get_constant('#39'FLDR_MAT_INVENTORY_CTRL'#39'))' +
        ') --����, ��������'
      ' union all'
      ' --������� ������ �� ������ �����'
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.issued_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   doc_carry_mat m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.issued_date between :begin_date and :end_date + 1 - 1/2' +
        '4/60'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.gr_dep_id'
      
        ' and d.fldr_id in (TO_NUMBER(fnc_get_constant('#39'FLDR_DOC_CARRY_MA' +
        'T_NEW'#39')), TO_NUMBER(fnc_get_constant('#39'FLDR_DOC_CARRY_MAT_SEND'#39'))' +
        ') --�����, ������������'
      ' union all'
      ' --��� ����������� ��� ���'
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   mat_diff_act m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.gr_dep_id'
      
        ' and d.fldr_id = TO_NUMBER(fnc_get_constant('#39'FLDR_MAT_DIFF_ACT_N' +
        'EW'#39')) --����������'
      ' union all'
      ' --��� �������� ���'
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   material_transfers m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.gr_dep_id'
      
        ' and d.fldr_id = TO_NUMBER(fnc_get_constant('#39'FLDR_MATERIAL_TRANS' +
        'FER_NEW'#39')) --�����'
      ' union all'
      ' --������ �� ������ '
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   repair_outfit m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.gr_dep_id'
      
        ' and d.fldr_id in (TO_NUMBER(fnc_get_constant('#39'FLDR_REPAIR_OUTFI' +
        'T_NEW'#39')), TO_NUMBER(fnc_get_constant('#39'FLDR_REPAIR_OUTFIT_WRK'#39')))' +
        ' --�����, � ������'
      ' union all'
      ' --��� �� ������ ����������, ����������� '
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   prop_to_empl m,'
      '   documents d,'
      '   organizations o,'
      '   depositories dep, '
      '   staff_table s,'
      '   staffmen sm,'
      '   employeers e'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and m.emp_id = e.person_cnt_id'
      ' and e.person_cnt_id = sm.cnt_id'
      ' AND sm.fire_date IS NULL'
      ' and sm.staff_table_id = s.id'
      ' and s.dep_id = dep.id'
      ' and dep.org_id = o.id'
      
        ' and d.fldr_id in (TO_NUMBER(fnc_get_constant('#39'FLDR_PROP_TO_EMPL' +
        '_NK_NEW'#39')), TO_NUMBER(fnc_get_constant('#39'FLDR_PROP_TO_EMPL_NK_SEN' +
        'D'#39'))) --�����, �� �����'
      ' union all'
      ' --������� ����������, ����������� �� �����'
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   prop_to_storage m,'
      '   documents d,'
      '   organizations o,'
      '   retail.rtl_link_rtt_deps l'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      ' and o.id = l.org_id'
      ' and l.gr_dep_id = m.dep_id'
      
        ' and d.fldr_id = TO_NUMBER(fnc_get_constant('#39'FLDR_PROP_TO_STORAG' +
        'E_NK_NEW'#39')) --�����'
      ' union all'
      ' --��� �������� ������������ ����������'
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   material_transfers_new m,'
      '   documents d,'
      '   organizations o,'
      '   depositories dep, '
      '   staff_table s,'
      '   staffmen sm'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and m.cnt_id = sm.cnt_id'
      ' and s.id = sm.staff_table_id'
      ' and s.dep_id = dep.id'
      ' AND sm.fire_date IS NULL'
      ' and dep.org_id = o.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      
        ' and d.fldr_id = TO_NUMBER(fnc_get_constant('#39'FLDR_MATERIAL_TRANS' +
        'FER_NEW_NEW'#39')) --�����'
      ' union all'
      ' --������� ���������� �� ����������'
      ' select'
      '    d.src_org_id,'
      '    d.id as doc_id,'
      '    d.doc_type_id, '
      '    m.doc_date as create_date, '
      '    d.author, '
      '    d.modify_user,'
      '    d.fldr_id,'
      '    d.modify_date,'
      '    d.src_org_id as sender_id,'
      '    NULL as receiver_id,'
      '    NULL as supplier_name,'
      '    NULL as dop_info'
      ' from'
      '   return_matetials m,'
      '   documents d,'
      '   organizations o,'
      '   depositories dep, '
      '   staff_table s,'
      '   staffmen sm'
      
        ' where m.doc_date between :begin_date and :end_date + 1 - 1/24/6' +
        '0'
      ' and d.status <>'#39'D'#39
      ' and m.doc_id = d.id'
      ' and m.cnt_id = sm.cnt_id'
      ' and s.id = sm.staff_table_id'
      ' AND sm.fire_date IS NULL'
      ' and s.dep_id = dep.id'
      ' and dep.org_id = o.id'
      ' and o.enable = '#39'Y'#39
      ' and o.org_type_id = 2'
      
        ' and d.fldr_id in (TO_NUMBER(fnc_get_constant('#39'FLDR_RETURN_MATET' +
        'IALS_NEW'#39')), TO_NUMBER(fnc_get_constant('#39'FLDR_RETURN_MATETIALS_C' +
        'ONTR'#39'))) --�����, �� ��������'
      '    ) v,'
      '    organizations o,'
      '    folders f,'
      '    document_types dt,'
      '    organizations dest,'
      '    organizations src,'
      '    employeers e,'
      '    staff_table st,'
      '    staffmen sm'
      '  where'
      '    v.src_org_id = o.id'
      '    and v.fldr_id = f.id '
      '    and dt.id = v.doc_type_id and o.enable = '#39'Y'#39
      '    and dest.id (+) = v.receiver_id '
      '    and src.id (+) = v.sender_id'
      '    and e.user_name = v.author'
      '    and e.person_cnt_id = sm.cnt_id(+)'
      '    AND sm.fire_date(+) IS NULL'
      '    and sm.staff_table_id = st.id(+)'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end>
    object m_Q_mainORG_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'ORG_NAME'
      Origin = 'ORG_NAME'
      Size = 250
    end
    object m_Q_mainDOC_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'DOC_ID'
      Origin = 'DOC_ID'
    end
    object m_Q_mainDOC_NAME: TStringField
      DisplayLabel = '������������ ���������'
      FieldName = 'DOC_NAME'
      Origin = 'DOC_NAME'
      Size = 100
    end
    object m_Q_mainCREATE_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'CREATE_DATE'
      Origin = 'CREATE_DATE'
    end
    object m_Q_mainAUTHOR: TStringField
      DisplayLabel = '����� ���������'
      FieldName = 'AUTHOR'
      Origin = 'AUTHOR'
      Size = 150
    end
    object m_Q_mainMODIFY_USER: TStringField
      DisplayLabel = '���������������� ������������'
      FieldName = 'MODIFY_USER'
      Origin = 'MODIFY_USER'
      Size = 150
    end
    object m_Q_mainFLDR_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'FLDR_NAME'
      Origin = 'FLDR_NAME'
      Size = 100
    end
    object m_Q_mainMODIFY_DATE: TDateTimeField
      DisplayLabel = '���� �����������'
      FieldName = 'MODIFY_DATE'
      Origin = 'MODIFY_DATE'
    end
    object m_Q_mainSENDER_NAME: TStringField
      DisplayLabel = '�����-�����������'
      FieldName = 'SENDER_NAME'
      Origin = 'SENDER_NAME'
      Size = 250
    end
    object m_Q_mainRECEIVER_NAME: TStringField
      DisplayLabel = '�����-����������'
      FieldName = 'RECEIVER_NAME'
      Origin = 'RECEIVER_NAME'
      Size = 250
    end
    object m_Q_mainSUPPLIER_NAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'SUPPLIER_NAME'
      Origin = 'SUPPLIER_NAME'
      Size = 250
    end
    object m_Q_mainCNT_ID: TFloatField
      DisplayLabel = 'CNT_ID ������ ���������'
      FieldName = 'CNT_ID'
      Origin = 'CNT_ID'
    end
    object m_Q_mainPOSITION: TStringField
      DisplayLabel = '��������� ������ ���������'
      FieldName = 'POSITION'
      Origin = 'POSITION'
      Size = 100
    end
    object m_Q_mainDOP_INFO: TStringField
      DisplayLabel = '���. ����������'
      FieldName = 'DOP_INFO'
      Origin = 'dop_info'
      Size = 250
    end
  end
  object m_Q_org: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id,name'
      'FROM '
      '(SELECT -1 as id,'#39'���'#39' as name'
      'from dual'
      'union'
      'SELECT id,name'
      'FROM organizations'
      'WHERE org_type_id = 2 and enable ='#39'Y'#39')'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 32
    Top = 200
    object m_Q_orgID: TFloatField
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_orgNAME: TStringField
      DisplayLabel = '��������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
  end
  object m_Q_org_fict: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT -1 as id, rpad('#39'��� '#39',250) as name'
      'FROM dual')
    UpdateObject = m_U_org_fict
    Macros = <>
    Left = 112
    Top = 200
    object m_Q_org_fictID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_org_fictNAME: TStringField
      DisplayLabel = '��������'
      FieldName = 'NAME'
      Size = 250
    end
  end
  object m_U_org_fict: TMLUpdateSQL
    Left = 192
    Top = 200
  end
end
