inherited FInvSheetAlco: TFInvSheetAlco
  Left = 838
  Top = 295
  Caption = '�������'
  ClientHeight = 278
  ClientWidth = 427
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 247
    Width = 427
    TabOrder = 2
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 233
      inherited m_SBTN_save: TBitBtn
        Caption = '���������'
      end
    end
  end
  object m_panel_SN: TPanel [1]
    Left = 0
    Top = 0
    Width = 427
    Height = 113
    Align = alTop
    TabOrder = 0
    object d2: TLabel
      Left = 16
      Top = 33
      Width = 31
      Height = 13
      Caption = '�����'
      WordWrap = True
    end
    object d1: TLabel
      Left = 16
      Top = 73
      Width = 34
      Height = 13
      Caption = '�����'
      WordWrap = True
    end
    object d3: TLabel
      Left = 8
      Top = 8
      Width = 287
      Height = 13
      Caption = '���������� �� ����� � ������ ��������������� �����'
    end
    object edt_rank: TEdit
      Left = 96
      Top = 32
      Width = 137
      Height = 21
      TabOrder = 0
      OnKeyPress = edt_rankKeyPress
    end
    object edt_num: TEdit
      Left = 96
      Top = 64
      Width = 137
      Height = 21
      TabOrder = 1
      OnKeyPress = edt_numKeyPress
    end
  end
  object m_panel_alc: TPanel [2]
    Left = 0
    Top = 113
    Width = 427
    Height = 128
    Align = alTop
    TabOrder = 1
    object d5: TLabel
      Left = 24
      Top = 59
      Width = 43
      Height = 13
      Caption = '�������'
      WordWrap = True
    end
    object d6: TLabel
      Left = 24
      Top = 84
      Width = 59
      Height = 13
      Caption = '����������'
      WordWrap = True
    end
    object d4: TLabel
      Left = 8
      Top = 25
      Width = 128
      Height = 13
      Caption = '���������� �� ��������'
    end
    object m_DBE_alccode: TDBEdit
      Left = 94
      Top = 57
      Width = 129
      Height = 21
      Color = clInactiveCaption
      DataField = 'ALCCODE'
      DataSource = m_DS_alco
      ReadOnly = True
      TabOrder = 0
    end
    object m_LV_alccode: TMLLovListView
      Left = 227
      Top = 57
      Width = 181
      Height = 21
      Lov = m_LOV_alccode
      UpDownWidth = 480
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 1
      ParentColor = False
    end
    object m_DBE_qnt: TDBEdit
      Left = 94
      Top = 81
      Width = 129
      Height = 21
      DataField = 'QNT'
      DataSource = m_DS_alco
      TabOrder = 2
    end
  end
  inherited m_IL_main: TImageList
    Left = 144
    Top = 136
  end
  inherited m_ACTL_main: TActionList
    Left = 80
    Top = 136
    inherited m_ACT_apply_updates: TAction
      Caption = '���������'
      OnExecute = m_ACT_apply_updatesExecuteAll
    end
  end
  object m_DS_alccode: TDataSource
    DataSet = DInvSheet.m_Q_alccode
    Left = 302
    Top = 155
  end
  object m_LOV_alccode: TMLLov
    DataFieldKey = 'ALCCODE'
    DataFieldValue = 'FULLNAME'
    DataSource = m_DS_alco
    DataFieldKeys = 'PREF_ALCCODE'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_alccode
    AutoOpenList = True
    Left = 326
    Top = 155
  end
  object m_DS_alco: TDataSource
    DataSet = DInvSheet.m_Q_alco
    Left = 30
    Top = 147
  end
  object m_DS_fsm: TDataSource
    Left = 8
    Top = 40
  end
end
