inherited DMessagesRKU: TDMessagesRKU
  OldCreateOrder = False
  Left = 395
  Top = 243
  Height = 640
  Width = 903
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
  end
  inherited m_Q_main: TMLQuery
    SQL.Strings = (
      'SELECT id,'
      '       doc_src_org_id,'
      '       doc_src_org_name,'
      '       doc_create_date,'
      '       doc_create_comp_name,'
      '       doc_create_user,'
      '       doc_modify_date,'
      '       doc_modify_comp_name,'
      '       doc_modify_user,'
      '       doc_status,'
      '       doc_status_name,'
      '       doc_status_change_date,'
      '       doc_fldr_id,'
      '       doc_fldr_name,'
      '       doc_fldr_right,'
      '       doc_type_id,'
      '       doc_type_name,'
      '       doc_number,'
      '       doc_date,'
      '       message_type,'
      '       host,'
      '       rku,'
      '       send_date,'
      '       create_user_id,'
      '       create_user_name,'
      '       read_date,'
      '       read_user_id,'
      '       read_user_name'
      '  FROM (SELECT d.id AS id,'
      '               d.src_org_id AS doc_src_org_id,'
      '               o.name AS doc_src_org_name,'
      '               d.create_date AS doc_create_date,'
      '               d.comp_name AS doc_create_comp_name,'
      '               d.author AS doc_create_user,'
      '               d.modify_date AS doc_modify_date,'
      '               d.modify_comp_name AS doc_modify_comp_name,'
      '               d.modify_user AS doc_modify_user,'
      '               d.status AS doc_status,'
      
        '               decode(d.status, '#39'F'#39','#39'��������'#39',   '#39'D'#39','#39'�����'#39', ' +
        #39'W'#39','#39'������ �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '               d.status_change_date AS doc_status_change_date,'
      '               d.fldr_id AS doc_fldr_id,'
      '               f.name AS doc_fldr_name,'
      
        '               rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 2' +
        '50) AS doc_fldr_right,'
      '               m.doc_number,'
      '               m.doc_date,'
      '               mt.name message_type,'
      '               m.host,'
      '               to_char(m.rku) rku,'
      '               to_date(NULL) send_date,'
      '               to_number(NULL) create_user_id,'
      '               to_char('#39#39') create_user_name,'
      '               m.read_date,'
      '               m.read_user_id,'
      
        '               TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.read_user' +
        '_id, 1), 250)) read_user_name,'
      '               d.doc_type_id,'
      '               dt.name doc_type_name'
      '          FROM documents        d,'
      '               folders          f,'
      '               organizations    o,'
      '               doc_messages_rku m,'
      '               messages_types   mt,'
      '               document_types   dt'
      '         WHERE f.doc_type_id = :DOC_TYPE_FROM_RKU'
      '           AND f.id = d.fldr_id'
      '           AND o.id = d.src_org_id'
      '           AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '           AND m.doc_id = d.id'
      '           AND d.doc_type_id = dt.id'
      '           AND m.type_message = mt.id(+)'
      
        '           AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_i' +
        'd = :ORG_ID))'
      '        UNION ALL'
      '        SELECT d.id AS id,'
      '               d.src_org_id AS doc_src_org_id,'
      '               o.name AS doc_src_org_name,'
      '               d.create_date AS doc_create_date,'
      '               d.comp_name AS doc_create_comp_name,'
      '               d.author AS doc_create_user,'
      '               d.modify_date AS doc_modify_date,'
      '               d.modify_comp_name AS doc_modify_comp_name,'
      '               d.modify_user AS doc_modify_user,'
      '               d.status AS doc_status,'
      
        '               decode(d.status, '#39'F'#39','#39'��������'#39',   '#39'D'#39','#39'�����'#39', ' +
        #39'W'#39','#39'������ �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '               d.status_change_date AS doc_status_change_date,'
      '               d.fldr_id AS doc_fldr_id,'
      '               f.name AS doc_fldr_name,'
      
        '               rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 2' +
        '50) AS doc_fldr_right,'
      '               m.doc_number,'
      '               m.doc_date,'
      '               mt.name message_type,'
      '               m.host,'
      '               (SELECT TRIM(RPAD(csl(rku),250))'
      
        '                  FROM doc_messages_rku_items WHERE doc_id = m.d' +
        'oc_id) rku,'
      '               m.send_date,'
      '               m.create_user_id,'
      
        '               TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_us' +
        'er_id, 1), 250)) create_user_name,'
      '               to_date(NULL) read_date,'
      '               to_number(NULL) read_user_id,'
      '               to_char('#39#39') read_user_name,'
      '               d.doc_type_id,'
      
        '               dt.name doc_type_name                            ' +
        ' '
      '          FROM documents        d,'
      '               folders          f,'
      '               organizations    o,'
      '               doc_messages_rku m,'
      '               messages_types   mt,'
      '               document_types   dt'
      '         WHERE f.doc_type_id = :DOC_TYPE_TO_RKU'
      '           AND f.id = d.fldr_id'
      '           AND o.id = d.src_org_id'
      '           AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '           AND m.doc_id = d.id'
      '           AND d.doc_type_id = dt.id'
      '           AND m.type_message = mt.id(+)'
      
        '           AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_i' +
        'd = :ORG_ID)))'
      
        ' WHERE doc_date BETWEEN :BEGIN_DATE AND :END_DATE + 1 - 1/24/60/' +
        '60'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = nil
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_FROM_RKU'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_TO_RKU'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_mainID: TFloatField
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_mainDOC_SRC_ORG_ID: TFloatField
      FieldName = 'DOC_SRC_ORG_ID'
    end
    object m_Q_mainDOC_SRC_ORG_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'DOC_SRC_ORG_NAME'
      Origin = 'DOC_SRC_ORG_NAME'
      Size = 250
    end
    object m_Q_mainDOC_CREATE_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'DOC_CREATE_DATE'
      Origin = 'DOC_CREATE_DATE'
    end
    object m_Q_mainDOC_CREATE_COMP_NAME: TStringField
      DisplayLabel = '������� ��� ������'
      FieldName = 'DOC_CREATE_COMP_NAME'
      Origin = 'DOC_CREATE_COMP_NAME'
      Size = 50
    end
    object m_Q_mainDOC_CREATE_USER: TStringField
      DisplayLabel = '������������ ��������� def'
      FieldName = 'DOC_CREATE_USER'
      Origin = 'DOC_CREATE_USER'
    end
    object m_Q_mainDOC_MODIFY_DATE: TDateTimeField
      DisplayLabel = '���� �����������'
      FieldName = 'DOC_MODIFY_DATE'
      Origin = 'DOC_MODIFY_DATE'
    end
    object m_Q_mainDOC_MODIFY_COMP_NAME: TStringField
      DisplayLabel = '������� ��� �������������'
      FieldName = 'DOC_MODIFY_COMP_NAME'
      Origin = 'DOC_MODIFY_COMP_NAME'
      Size = 50
    end
    object m_Q_mainDOC_MODIFY_USER: TStringField
      DisplayLabel = '������������ ����������������'
      FieldName = 'DOC_MODIFY_USER'
      Origin = 'DOC_MODIFY_USER'
      Size = 50
    end
    object m_Q_mainDOC_STATUS: TStringField
      FieldName = 'DOC_STATUS'
      FixedChar = True
      Size = 1
    end
    object m_Q_mainDOC_STATUS_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'DOC_STATUS_NAME'
      Origin = 'DOC_STATUS_NAME'
      Size = 16
    end
    object m_Q_mainDOC_STATUS_CHANGE_DATE: TDateTimeField
      DisplayLabel = '���� ��������� �������'
      FieldName = 'DOC_STATUS_CHANGE_DATE'
      Origin = 'DOC_STATUS_CHANGE_DATE'
    end
    object m_Q_mainDOC_FLDR_ID: TFloatField
      FieldName = 'DOC_FLDR_ID'
    end
    object m_Q_mainDOC_FLDR_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'DOC_FLDR_NAME'
      Origin = 'DOC_FLDR_NAME'
      Size = 100
    end
    object m_Q_mainDOC_FLDR_RIGHT: TStringField
      FieldName = 'DOC_FLDR_RIGHT'
      Size = 250
    end
    object m_Q_mainDOC_NUMBER: TFloatField
      DisplayLabel = '� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'DOC_NUMBER'
    end
    object m_Q_mainMESSAGE_TYPE: TStringField
      DisplayLabel = '��� ���������'
      FieldName = 'MESSAGE_TYPE'
      Origin = 'MESSAGE_TYPE'
      Size = 250
    end
    object m_Q_mainHOST: TStringField
      DisplayLabel = '����'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
    object m_Q_mainSEND_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'SEND_DATE'
      Origin = 'SEND_DATE'
    end
    object m_Q_mainCREATE_USER_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'CREATE_USER_ID'
      Origin = 'CREATE_USER_ID'
    end
    object m_Q_mainCREATE_USER_NAME: TStringField
      DisplayLabel = '������������ ���������'
      FieldName = 'CREATE_USER_NAME'
      Origin = 'CREATE_USER_NAME'
      Size = 250
    end
    object m_Q_mainREAD_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'READ_DATE'
      Origin = 'READ_DATE'
    end
    object m_Q_mainREAD_USER_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'READ_USER_ID'
      Origin = 'READ_USER_ID'
    end
    object m_Q_mainREAD_USER_NAME: TStringField
      DisplayLabel = '������������ �����������'
      FieldName = 'READ_USER_NAME'
      Origin = 'READ_USER_NAME'
      Size = 250
    end
    object m_Q_mainDOC_TYPE_NAME: TStringField
      DisplayLabel = '��� ���������'
      FieldName = 'DOC_TYPE_NAME'
      Origin = 'DOC_TYPE_NAME'
      Size = 100
    end
    object m_Q_mainDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'DOC_DATE'
      Origin = 'DOC_DATE'
    end
    object m_Q_mainDOC_TYPE_ID: TFloatField
      FieldName = 'DOC_TYPE_ID'
    end
    object m_Q_mainRKU: TStringField
      DisplayLabel = '���'
      FieldName = 'RKU'
      Origin = 'RKU'
      Size = 250
    end
  end
  object m_Q_doc_ids: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT max(id) as last_id'
      'FROM'
      '('
      'SELECT id,'
      '       doc_src_org_id,'
      '       doc_src_org_name,'
      '       doc_create_date,'
      '       doc_create_comp_name,'
      '       doc_create_user,'
      '       doc_modify_date,'
      '       doc_modify_comp_name,'
      '       doc_modify_user,'
      '       doc_status,'
      '       doc_status_name,'
      '       doc_status_change_date,'
      '       doc_fldr_id,'
      '       doc_fldr_name,'
      '       doc_fldr_right,'
      '       doc_type_id,'
      '       doc_type_name,'
      '       doc_number,'
      '       doc_date,'
      '       message_type,'
      '       host,'
      '       rku,'
      '       send_date,'
      '       create_user_id,'
      '       create_user_name,'
      '       read_date,'
      '       read_user_id,'
      '       read_user_name'
      '  FROM (SELECT d.id AS id,'
      '               d.src_org_id AS doc_src_org_id,'
      '               o.name AS doc_src_org_name,'
      '               d.create_date AS doc_create_date,'
      '               d.comp_name AS doc_create_comp_name,'
      '               d.author AS doc_create_user,'
      '               d.modify_date AS doc_modify_date,'
      '               d.modify_comp_name AS doc_modify_comp_name,'
      '               d.modify_user AS doc_modify_user,'
      '               d.status AS doc_status,'
      
        '               decode(d.status, '#39'F'#39','#39'��������'#39',   '#39'D'#39','#39'�����'#39', ' +
        #39'W'#39','#39'������ �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '               d.status_change_date AS doc_status_change_date,'
      '               d.fldr_id AS doc_fldr_id,'
      '               f.name AS doc_fldr_name,'
      
        '               rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 2' +
        '50) AS doc_fldr_right,'
      '               m.doc_number,'
      '               m.doc_date,'
      '               mt.name message_type,'
      '               m.host,'
      '               to_char(m.rku) rku,'
      '               to_date(NULL) send_date,'
      '               to_number(NULL) create_user_id,'
      '               to_char('#39#39') create_user_name,'
      '               m.read_date,'
      '               m.read_user_id,'
      
        '               TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.read_user' +
        '_id, 1), 250)) read_user_name,'
      '               d.doc_type_id,'
      '               dt.name doc_type_name'
      '          FROM documents        d,'
      '               folders          f,'
      '               organizations    o,'
      '               doc_messages_rku m,'
      '               messages_types   mt,'
      '               document_types   dt'
      '         WHERE f.doc_type_id = :DOC_TYPE_FROM_RKU'
      '           AND f.id = d.fldr_id'
      '           AND o.id = d.src_org_id'
      '           AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '           AND m.doc_id = d.id'
      '           AND d.doc_type_id = dt.id'
      '           AND m.type_message = mt.id(+)'
      '        UNION ALL'
      '        SELECT d.id AS id,'
      '               d.src_org_id AS doc_src_org_id,'
      '               o.name AS doc_src_org_name,'
      '               d.create_date AS doc_create_date,'
      '               d.comp_name AS doc_create_comp_name,'
      '               d.author AS doc_create_user,'
      '               d.modify_date AS doc_modify_date,'
      '               d.modify_comp_name AS doc_modify_comp_name,'
      '               d.modify_user AS doc_modify_user,'
      '               d.status AS doc_status,'
      
        '               decode(d.status, '#39'F'#39','#39'��������'#39',   '#39'D'#39','#39'�����'#39', ' +
        #39'W'#39','#39'������ �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '               d.status_change_date AS doc_status_change_date,'
      '               d.fldr_id AS doc_fldr_id,'
      '               f.name AS doc_fldr_name,'
      
        '               rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 2' +
        '50) AS doc_fldr_right,'
      '               m.doc_number,'
      '               m.doc_date,'
      '               mt.name message_type,'
      '               m.host,'
      '               (SELECT TRIM(RPAD(csl(rku),250))'
      
        '                  FROM doc_messages_rku_items WHERE doc_id = m.d' +
        'oc_id) rku,'
      '               m.send_date,'
      '               m.create_user_id,'
      
        '               TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_us' +
        'er_id, 1), 250)) create_user_name,'
      '               to_date(NULL) read_date,'
      '               to_number(NULL) read_user_id,'
      '               to_char('#39#39') read_user_name,'
      '               d.doc_type_id,'
      
        '               dt.name doc_type_name                            ' +
        ' '
      '          FROM documents        d,'
      '               folders          f,'
      '               organizations    o,'
      '               doc_messages_rku m,'
      '               messages_types   mt,'
      '               document_types   dt'
      '         WHERE f.doc_type_id = :DOC_TYPE_TO_RKU'
      '           AND f.id = d.fldr_id'
      '           AND o.id = d.src_org_id'
      '           AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '           AND m.doc_id = d.id'
      '           AND d.doc_type_id = dt.id'
      '           AND m.type_message = mt.id(+))'
      
        ' WHERE doc_date BETWEEN :BEGIN_DATE AND :END_DATE + 1 - 1/24/60/' +
        '60'
      ''
      ''
      ')'
      'ORDER BY id'
      ' '
      ' ')
    UpdateObject = m_U_main
    Macros = <>
    Left = 24
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_FROM_RKU'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_TO_RKU'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_doc_idsLAST_ID: TFloatField
      FieldName = 'LAST_ID'
    end
  end
  object m_Q_new_docs: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id,'
      '       doc_src_org_id,'
      '       doc_src_org_name,'
      '       doc_create_date,'
      '       doc_create_comp_name,'
      '       doc_create_user,'
      '       doc_modify_date,'
      '       doc_modify_comp_name,'
      '       doc_modify_user,'
      '       doc_status,'
      '       doc_status_name,'
      '       doc_status_change_date,'
      '       doc_fldr_id,'
      '       doc_fldr_name,'
      '       doc_fldr_right,'
      '       doc_type_id,'
      '       doc_type_name,'
      '       doc_number,'
      '       doc_date,'
      '       message_type,'
      '       host,'
      '       rku,'
      '       send_date,'
      '       create_user_id,'
      '       create_user_name,'
      '       read_date,'
      '       read_user_id,'
      '       read_user_name'
      '  FROM (SELECT d.id AS id,'
      '               d.src_org_id AS doc_src_org_id,'
      '               o.name AS doc_src_org_name,'
      '               d.create_date AS doc_create_date,'
      '               d.comp_name AS doc_create_comp_name,'
      '               d.author AS doc_create_user,'
      '               d.modify_date AS doc_modify_date,'
      '               d.modify_comp_name AS doc_modify_comp_name,'
      '               d.modify_user AS doc_modify_user,'
      '               d.status AS doc_status,'
      
        '               decode(d.status, '#39'F'#39','#39'��������'#39',   '#39'D'#39','#39'�����'#39', ' +
        #39'W'#39','#39'������ �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '               d.status_change_date AS doc_status_change_date,'
      '               d.fldr_id AS doc_fldr_id,'
      '               f.name AS doc_fldr_name,'
      
        '               rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 2' +
        '50) AS doc_fldr_right,'
      '               m.doc_number,'
      '               m.doc_date,'
      '               mt.name message_type,'
      '               m.host,'
      '               to_char(m.rku) rku,'
      '               to_date(NULL) send_date,'
      '               to_number(NULL) create_user_id,'
      '               to_char('#39#39') create_user_name,'
      '               m.read_date,'
      '               m.read_user_id,'
      
        '               TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.read_user' +
        '_id, 1), 250)) read_user_name,'
      '               d.doc_type_id,'
      '               dt.name doc_type_name'
      '          FROM documents        d,'
      '               folders          f,'
      '               organizations    o,'
      '               doc_messages_rku m,'
      '               messages_types   mt,'
      '               document_types   dt'
      '         WHERE f.doc_type_id = :DOC_TYPE_FROM_RKU'
      '           AND f.id = d.fldr_id'
      '           AND o.id = d.src_org_id'
      '           AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '           AND m.doc_id = d.id'
      '           AND d.doc_type_id = dt.id'
      '           AND m.type_message = mt.id(+)'
      '        UNION ALL'
      '        SELECT d.id AS id,'
      '               d.src_org_id AS doc_src_org_id,'
      '               o.name AS doc_src_org_name,'
      '               d.create_date AS doc_create_date,'
      '               d.comp_name AS doc_create_comp_name,'
      '               d.author AS doc_create_user,'
      '               d.modify_date AS doc_modify_date,'
      '               d.modify_comp_name AS doc_modify_comp_name,'
      '               d.modify_user AS doc_modify_user,'
      '               d.status AS doc_status,'
      
        '               decode(d.status, '#39'F'#39','#39'��������'#39',   '#39'D'#39','#39'�����'#39', ' +
        #39'W'#39','#39'������ �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '               d.status_change_date AS doc_status_change_date,'
      '               d.fldr_id AS doc_fldr_id,'
      '               f.name AS doc_fldr_name,'
      
        '               rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 2' +
        '50) AS doc_fldr_right,'
      '               m.doc_number,'
      '               m.doc_date,'
      '               mt.name message_type,'
      '               m.host,'
      '               (SELECT TRIM(RPAD(csl(rku),250))'
      
        '                  FROM doc_messages_rku_items WHERE doc_id = m.d' +
        'oc_id) rku,'
      '               m.send_date,'
      '               m.create_user_id,'
      
        '               TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_us' +
        'er_id, 1), 250)) create_user_name,'
      '               to_date(NULL) read_date,'
      '               to_number(NULL) read_user_id,'
      '               to_char('#39#39') read_user_name,'
      '               d.doc_type_id,'
      
        '               dt.name doc_type_name                            ' +
        ' '
      '          FROM documents        d,'
      '               folders          f,'
      '               organizations    o,'
      '               doc_messages_rku m,'
      '               messages_types   mt,'
      '               document_types   dt'
      '         WHERE f.doc_type_id = :DOC_TYPE_TO_RKU'
      '           AND f.id = d.fldr_id'
      '           AND o.id = d.src_org_id'
      '           AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '           AND m.doc_id = d.id'
      '           AND d.doc_type_id = dt.id'
      '           AND m.type_message = mt.id(+))'
      
        ' WHERE doc_date BETWEEN :BEGIN_DATE AND :END_DATE + 1 - 1/24/60/' +
        '60'
      ' AND id > :LAST_ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 128
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_FROM_RKU'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_TO_RKU'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LAST_ID'
        ParamType = ptInput
      end>
    object m_Q_new_docsID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_new_docsDOC_SRC_ORG_ID: TFloatField
      FieldName = 'DOC_SRC_ORG_ID'
    end
    object m_Q_new_docsDOC_SRC_ORG_NAME: TStringField
      FieldName = 'DOC_SRC_ORG_NAME'
      Size = 250
    end
    object m_Q_new_docsDOC_CREATE_DATE: TDateTimeField
      FieldName = 'DOC_CREATE_DATE'
    end
    object m_Q_new_docsDOC_CREATE_COMP_NAME: TStringField
      FieldName = 'DOC_CREATE_COMP_NAME'
      Size = 50
    end
    object m_Q_new_docsDOC_CREATE_USER: TStringField
      FieldName = 'DOC_CREATE_USER'
    end
    object m_Q_new_docsDOC_MODIFY_DATE: TDateTimeField
      FieldName = 'DOC_MODIFY_DATE'
    end
    object m_Q_new_docsDOC_MODIFY_COMP_NAME: TStringField
      FieldName = 'DOC_MODIFY_COMP_NAME'
      Size = 50
    end
    object m_Q_new_docsDOC_MODIFY_USER: TStringField
      FieldName = 'DOC_MODIFY_USER'
      Size = 50
    end
    object m_Q_new_docsDOC_STATUS: TStringField
      FieldName = 'DOC_STATUS'
      FixedChar = True
      Size = 1
    end
    object m_Q_new_docsDOC_STATUS_NAME: TStringField
      FieldName = 'DOC_STATUS_NAME'
      Size = 16
    end
    object m_Q_new_docsDOC_STATUS_CHANGE_DATE: TDateTimeField
      FieldName = 'DOC_STATUS_CHANGE_DATE'
    end
    object m_Q_new_docsDOC_FLDR_ID: TFloatField
      FieldName = 'DOC_FLDR_ID'
    end
    object m_Q_new_docsDOC_FLDR_NAME: TStringField
      FieldName = 'DOC_FLDR_NAME'
      Size = 100
    end
    object m_Q_new_docsDOC_FLDR_RIGHT: TStringField
      FieldName = 'DOC_FLDR_RIGHT'
      Size = 250
    end
    object m_Q_new_docsDOC_TYPE_ID: TFloatField
      FieldName = 'DOC_TYPE_ID'
    end
    object m_Q_new_docsDOC_TYPE_NAME: TStringField
      FieldName = 'DOC_TYPE_NAME'
      Size = 100
    end
    object m_Q_new_docsDOC_NUMBER: TFloatField
      FieldName = 'DOC_NUMBER'
    end
    object m_Q_new_docsDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_new_docsMESSAGE_TYPE: TStringField
      FieldName = 'MESSAGE_TYPE'
      Size = 250
    end
    object m_Q_new_docsHOST: TStringField
      FieldName = 'HOST'
      Size = 64
    end
    object m_Q_new_docsRKU: TStringField
      FieldName = 'RKU'
      Size = 250
    end
    object m_Q_new_docsSEND_DATE: TDateTimeField
      FieldName = 'SEND_DATE'
    end
    object m_Q_new_docsCREATE_USER_ID: TFloatField
      FieldName = 'CREATE_USER_ID'
    end
    object m_Q_new_docsCREATE_USER_NAME: TStringField
      FieldName = 'CREATE_USER_NAME'
      Size = 250
    end
    object m_Q_new_docsREAD_DATE: TDateTimeField
      FieldName = 'READ_DATE'
    end
    object m_Q_new_docsREAD_USER_ID: TFloatField
      FieldName = 'READ_USER_ID'
    end
    object m_Q_new_docsREAD_USER_NAME: TStringField
      FieldName = 'READ_USER_NAME'
      Size = 250
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 248
    Top = 128
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
end
