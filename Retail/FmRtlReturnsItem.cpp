//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlReturnsItem.h"
#include "FmRtlReturnLine.h"
#include "FmRtlReturnsRecond.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ToolEdit"
#pragma link "RXDBCtrl"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFRtlReturnsItem::TFRtlReturnsItem(TComponent *p_owner,
                                              TTSDDocument *p_dm_document): TTSFDocument(p_owner,
                                                                                         p_dm_document),
                                                                            m_dm(static_cast<TDRtlReturns*>(DMDocument))
{
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::m_ACT_append_lineExecute(TObject *Sender)
{
  m_dm->UpdateRegimeLine = 2;

  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();

  m_dm->m_Q_sale_lines->ParamByName("REPORT_DOC_ID")->AsInteger = m_dm->m_Q_itemCHECK_DOC_ID->AsInteger;
  m_dm->m_Q_sale_lines->ParamByName("CHECK_NUM")->AsInteger = m_dm->m_Q_itemCHECK_NUM->AsInteger;
  m_dm->m_Q_sale_lines->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;

  if( !m_dm->m_Q_itemCHECK_NUM->IsNull ) m_dm->m_Q_sale_lines->Open();
  try
  {
    m_dm->m_Q_line->Open();
    try
    {
      m_dm->m_Q_line->Insert();
      m_dm->m_Q_lineDOC_ID->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_lineLINE_ID->AsInteger = m_dm->GetSqNextVal("SQ_RTL_RETURN_LINES");
      m_dm->m_Q_lineQUANTITY->AsFloat = 0;

      if( TSExecModuleDlg<TFRtlReturnLine>(this, m_dm) )
      {
        if( !m_dm->m_Q_itemCHECK_NUM->IsNull )
        {
          m_dm->m_Q_lineSALE_DOC_ID->AsInteger = m_dm->m_Q_sale_linesDOC_ID->AsInteger;
          m_dm->m_Q_lineSALE_LINE_ID->AsInteger = m_dm->m_Q_sale_linesLINE_ID->AsInteger;
          m_dm->m_Q_lineNMCL_ID->AsInteger = m_dm->m_Q_sale_linesNMCL_ID->AsInteger;
          m_dm->m_Q_lineASSORT_ID->AsInteger = m_dm->m_Q_sale_linesASSORT_ID->AsInteger;
          m_dm->m_Q_lineWEIGHT_CODE->AsInteger = m_dm->m_Q_sale_linesWEIGHT_CODE->AsInteger;
          m_dm->m_Q_lineBAR_CODE->AsString = m_dm->m_Q_sale_linesBAR_CODE->AsString;
          m_dm->m_Q_lineOUT_PRICE->AsFloat = m_dm->m_Q_sale_linesOUT_PRICE->AsFloat;
          m_dm->m_Q_lineIN_PRICE->AsFloat = m_dm->m_Q_sale_linesIN_PRICE->AsFloat;
          m_dm->m_Q_lineNDS->AsFloat = m_dm->m_Q_sale_linesNDS->AsFloat;
          m_dm->m_Q_lineSHOP_DEP_ID->AsFloat = m_dm->m_Q_sale_linesSHOP_DEP_ID->AsFloat;
          if( m_dm->m_Q_lineQUANTITY->AsFloat >  m_dm->m_Q_sale_linesQUANTITY->AsFloat )
            throw Exception("���������� �� �������� �� ����� ���� ������ ���������� �� �������");
        }
        else
        {
          m_dm->m_Q_lineSALE_DOC_ID->Clear();
          m_dm->m_Q_lineSALE_LINE_ID->Clear();
          m_dm->m_Q_lineWEIGHT_CODE->AsInteger = 0;
          m_dm->m_Q_lineBAR_CODE->Clear();

          m_dm->m_Q_price->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
          m_dm->m_Q_price->ParamByName("ASSORT_ID")->AsInteger = m_dm->m_Q_lineASSORT_ID->AsInteger;
          m_dm->m_Q_price->Open();
          try
          {
            m_dm->m_Q_lineOUT_PRICE->AsFloat = m_dm->m_Q_priceOUT_PRICE->AsFloat;
            m_dm->m_Q_lineIN_PRICE->AsFloat = m_dm->m_Q_priceIN_PRICE->AsFloat;
          }
          __finally
          {
            m_dm->m_Q_price->Close();
          }
        }

        m_dm->m_Q_line->Post();

        if( m_dm->m_Q_lineQUANTITY->AsFloat < 0.001 ) throw Exception("���������� ������ ���� ������ 0");

        m_dm->m_Q_line->Database->ApplyUpdates(OPENARRAY(TDBDataSet*,(m_dm->m_Q_line)));

        m_dm->m_Q_recalc->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
        m_dm->m_Q_recalc->ExecSQL();

        m_dm->RefreshItemDataSets();
      }
    }
    __finally
    {
      m_dm->m_Q_line->Close();
    };
  }
  __finally
  {
    m_dm->m_Q_sale_lines->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::m_ACT_append_lineUpdate(TObject *Sender)
{
  m_ACT_append_line->Enabled = (m_dm->UpdateRegimeItem != urView) &&
                               (m_dm->m_Q_itemRET_USER_ID->AsInteger == -1);

  MLLovListView1->Enabled = m_dm->m_Q_linesLINE_ID->IsNull;
  MLLovListView2->Enabled = m_dm->m_Q_linesLINE_ID->IsNull;
  MLLovListView3->Enabled = m_dm->m_Q_linesLINE_ID->IsNull;
  DBEdit1->Enabled = m_dm->m_Q_linesLINE_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::m_ACT_delete_lineExecute(TObject *Sender)
{
 if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();

  if( Application->MessageBox("������� ������?",
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO ) == IDYES )
  {
    m_dm->m_Q_line->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_line->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_linesLINE_ID->AsInteger;
    m_dm->m_Q_line->Open();
    try
    {
      m_dm->m_Q_line->Delete();
      m_dm->m_Q_line->Database->ApplyUpdates(OPENARRAY(TDBDataSet*,(m_dm->m_Q_line)));
    }
    __finally
    {
      m_dm->m_Q_line->Close();
    }

    m_dm->m_Q_recalc->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_recalc->ExecSQL();

    m_dm->RefreshItemDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::m_ACT_delete_lineUpdate(TObject *Sender)
{
  m_ACT_delete_line->Enabled = (m_dm->UpdateRegimeItem != urView) &&
                               (m_dm->m_Q_itemRET_USER_ID->AsInteger == -1) &&
                                !m_dm->m_Q_linesLINE_ID->IsNull
                                &&m_dm->m_Q_itemBUF_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::MLLov3AfterApply(TObject *Sender)
{
     if (m_dm->m_Q_item->State != dsInsert && m_dm->m_Q_item->State != dsEdit)
     m_dm->m_Q_item->Edit();
     m_dm->m_Q_itemRET_USER_ID->AsInteger = -1;
     m_dm->m_Q_itemRET_USER_NAME->AsString = "����";
     m_dm->m_Q_itemREPORT_DOC_ID->Clear();
     m_dm->m_Q_itemREPORT_NUM->Clear();
     m_dm->m_Q_itemFISCAL_NUM->Clear();
     m_dm->m_Q_itemREPORT_DATE->Clear();
     MLLovListView4->Enabled = false;
   DBEdit2->Enabled = (m_dm->m_Q_itemRET_USER_ID->AsInteger == -1);
   m_DBCB_cash->Enabled = false;
   if(m_dm->m_Q_itemRET_USER_ID->AsInteger == -1)
   {
     m_dm->m_Q_itemCASH->AsString = 'Y';
   }
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::FormShow(TObject *Sender)
{
   TTSFDocument::FormShow(Sender);
    m_BCS_item->Active = (m_dm->UpdateRegimeItem != urView);
//   if (m_dm->m_Q_itemCHECK_DOC_ID->IsNull || int(m_dm->m_Q_checksCHECK_DATE->AsDateTime) != int(m_dm->m_Q_itemISSUED_DATE->AsDateTime)){
   MLLovListView4->Enabled = false;
//   } else MLLovListView4->Enabled = true;
   m_DBCB_cash->Enabled = false;
   /*if(m_dm->m_Q_itemRET_USER_ID->AsInteger == -1)
   {
     m_dm->m_Q_itemCASH->AsString = 'Y';
   }*/
   DBEdit2->Enabled = (m_dm->m_Q_itemRET_USER_ID->AsInteger == -1);
   m_dm->EditCond = false;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::MLLov4AfterApply(TObject *Sender)
{
  if (m_dm->m_Q_item->State != dsInsert && m_dm->m_Q_item->State != dsEdit)
  m_dm->m_Q_item->Edit();
   m_dm->m_Q_itemBUY_FIO->Clear();
   DBEdit2->Enabled = (m_dm->m_Q_itemRET_USER_ID->AsInteger == -1);
   m_DBCB_cash->Enabled = false;
   if(m_dm->m_Q_itemRET_USER_ID->AsInteger == -1)
   {
      m_dm->m_Q_itemCASH->AsString = 'Y';
   }
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::MLLov3AfterClear(TObject *Sender)
{
   if (m_dm->m_Q_item->State != dsInsert && m_dm->m_Q_item->State != dsEdit)
   m_dm->m_Q_item->Edit();
   m_dm->m_Q_itemRET_USER_ID->Clear();
   m_dm->m_Q_itemRET_USER_NAME->Clear();
   m_dm->m_Q_itemREPORT_DOC_ID->Clear();
   m_dm->m_Q_itemREPORT_NUM->Clear();
   m_dm->m_Q_itemFISCAL_NUM->Clear();
   m_dm->m_Q_itemREPORT_DATE->Clear();
   MLLovListView4->Enabled = false;
   m_dm->m_Q_itemCASH->AsString = 'Y'; //�� ��������� ��������, null ���� �� �����
   m_DBCB_cash->Enabled = false;
   m_dm->m_Q_itemBUY_FIO->Clear();
   DBEdit2->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::MLLov4AfterClear(TObject *Sender)
{
   if (m_dm->m_Q_item->State != dsInsert && m_dm->m_Q_item->State != dsEdit)
   m_dm->m_Q_item->Edit();
   m_dm->m_Q_itemBUY_FIO->Clear();
   DBEdit2->Enabled = false;
   m_dm->m_Q_itemCASH->AsString = 'Y'; //�� ��������� ��������, null ���� �� �����
   m_DBCB_cash->Enabled = false;
}
//---------------------------------------------------------------------------


void __fastcall TFRtlReturnsItem::m_ACT_edit_recondExecute(TObject *Sender)
{
  if (m_dm->m_Q_item->State != dsInsert && m_dm->m_Q_item->State != dsEdit)
  m_dm->m_Q_item->Edit();
  
  TFRtlReturnsRecond *f = new TFRtlReturnsRecond(this, m_dm, urInsert);
  try
  {
    f->ShowModal();
  }
  __finally
  {
    delete f;
  }

  m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsItem::m_ACT_edit_recondUpdate(TObject *Sender)
{
   m_ACT_edit_recond->Enabled = (m_dm->UpdateRegimeItem != urView) &&
                                (m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrPrint ||
                                 m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrNew);
}
//---------------------------------------------------------------------------



void __fastcall TFRtlReturnsItem::m_ACT_noteExecute(TObject *Sender)
{
    AnsiString msg = "�������� ���������� � ��-3:\n"
                    "�������� ���������� � ���������� ���������� �������� ������ ��� ������������ ��������� ����.\n\n"
                    "���� ��������� �������:\n"
                    "-������� ���������� � ���� �������\n"
                    "-������� �� ������� ����������� � ���������� Fresh Food �/��� Dry Food\n"
                    "-���� �� ������� �������� �������� � ���� ������� ������� - ���\n";
    ShowMessage(msg);
    return;
}
//---------------------------------------------------------------------------


void __fastcall TFRtlReturnsItem::m_ACT_VIEWExecute(TObject *Sender)
{
       m_dm->UpdateRegimeLine = 1;

        if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();

         TSExecModuleDlg<TFRtlReturnLine>(this, m_dm);  
}
//---------------------------------------------------------------------------



void __fastcall TFRtlReturnsItem::m_BCS_itemEnd(TObject *Sender)
{
    std::string v_bar_code = m_BCS_item->GetResult().c_str();
    std::size_t posFN= v_bar_code.find("&fn");
    std::size_t posFP= v_bar_code.find("&fp");
    std::size_t posI= v_bar_code.find("&i");

    std::string v_fn_number = v_bar_code.substr(posFN+4,posI-posFN-4);
    std::string v_fn_doc_id = v_bar_code.substr(posI+3,posFP-posI-3);

    if(m_dm->m_Q_find_check->Active)m_dm->m_Q_find_check->Close();
    m_dm->m_Q_find_check->ParamByName("fn_number")->AsString = v_fn_number.c_str();
    m_dm->m_Q_find_check->ParamByName("fn_doc_id")->AsString = v_fn_doc_id.c_str();
    m_dm->m_Q_find_check->Open();

    m_dm->m_Q_find_check->First();
    if(m_dm->m_Q_find_checkCHECK_NUM->IsNull) {
       if (m_dm->m_Q_item->State != dsInsert && m_dm->m_Q_item->State != dsEdit)
       m_dm->m_Q_item->Edit();
       m_dm->m_Q_itemRET_USER_ID->Clear();
       m_dm->m_Q_itemRET_USER_NAME->Clear();
       m_dm->m_Q_itemREPORT_DOC_ID->Clear();
       m_dm->m_Q_itemREPORT_NUM->Clear();
       m_dm->m_Q_itemFISCAL_NUM->Clear();
       m_dm->m_Q_itemREPORT_DATE->Clear();
       MLLovListView4->Enabled = false;
       m_dm->m_Q_itemCASH->AsString = 'Y'; //�� ��������� ��������, null ���� �� �����
       m_DBCB_cash->Enabled = false;
       m_dm->m_Q_itemBUY_FIO->Clear();
       DBEdit2->Enabled = false;
       MessageBox(0, "��� �� ������!","����", MB_OK);
       return;
    }
    else {
      m_dm->m_Q_itemCHECK_NUM->AsString = m_dm->m_Q_find_checkCHECK_NUM->AsString;
      m_dm->m_Q_itemCHECK_DATE->AsDateTime = m_dm->m_Q_find_checkCHECK_DATE->AsDateTime;
      m_dm->m_Q_itemCHECK_SUM->AsFloat = m_dm->m_Q_find_checkCHECK_SUM->AsFloat;
      m_dm->m_Q_itemCHECK_DOC_ID->AsInteger = m_dm->m_Q_find_checkID->AsInteger;
      m_dm->m_Q_itemCASH->AsString = m_dm->m_Q_find_checkCASH->AsString;
      m_dm->m_Q_itemSTART_DATE->AsDateTime = m_dm->m_Q_find_checkCHECK_DATE->AsDateTime;
      m_dm->m_Q_itemEND_DATE->AsDateTime = m_dm->m_Q_find_checkCHECK_DATE->AsDateTime;
    }
/*
    char str[250];
    memset(str,0,sizeof(str));
    sprintf(str," bar_code = %s\n fn = %s\n fn_doc_id = %s",v_bar_code.c_str(), v_fn_number.c_str(), v_fn_doc_id.c_str());
    MessageBox(0, str,"����", MB_OK);
*/    
}
//---------------------------------------------------------------------------

