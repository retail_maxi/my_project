//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmGetShetDoc.h"
#include "MLFuncs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFGetSchetDoc::TFGetSchetDoc(TComponent* Owner, hDBIDb p_db_handle, TTSDCustom *p_dm): TForm(Owner),
                        m_db_handle(p_db_handle),
                        m_reg_root_key(HKEY_CURRENT_USER),
                        m_dm(p_dm),
                        m_Schet_doc(m_dm->GetSysRight(POLICY_OF_DOCUMENT_TYPE,"TSRetail.Bill",TTSDocumentTypeAccess().AccessMask))
{
  TRegistry *reg = new TRegistry;
  TDateTime m_start = int(m_dm->GetSysDate()) - 1;
  TDateTime m_end = int(m_dm->GetSysDate());
  int m_diapason = 1;
  try
  {
    reg->RootKey = m_reg_root_key;
    if( reg->OpenKey(MLGetLibraryRegKey(this),true) )
    {
      if( reg->ValueExists("DIAPASON_DATE") )
         m_diapason = reg->ReadInteger("DIAPASON_DATE");

      reg->CloseKey();
    }
  }
  __finally
  {
    delete reg;
  }
  m_start = m_end - m_diapason;

  FPressOK = false;
  m_DBSchetDoc->Handle = m_db_handle;
  m_Q_list->ParamByName("ACCESS_MASK")->AsString = TTSDocumentAccess("").AccessMask;
  SetBeginEndDate(m_start, m_end);
  m_ACT_set_begin_end_date->BeginDate = BeginDate;
  m_ACT_set_begin_end_date->EndDate = EndDate;
  if( m_Q_list->Active ) m_Q_list->Close();
  m_Q_list->Open();
}
//---------------------------------------------------------------------------

__fastcall TFGetSchetDoc::~TFGetSchetDoc()
{

  if( m_Q_list->Active ) m_Q_list->Close();
  m_DBSchetDoc->Handle = NULL;
  TRegistry *reg = new TRegistry;
  try
  {
    reg->RootKey = m_reg_root_key;
    if( reg->OpenKey(MLGetLibraryRegKey(this),true) )
    {
      reg->WriteInteger("DIAPASON_DATE", EndDate - BeginDate);

      reg->CloseKey();
    }
  }
  __finally
  {
    delete reg;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_BBTN_closeClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

bool __fastcall TFGetSchetDoc::GetPressOK()
{
  return FPressOK;
}
//---------------------------------------------------------------------------

long __fastcall TFGetSchetDoc::GetDocId()
{
  return m_doc_id;
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::SetDocId(long p_value)
{
  m_doc_id = p_value;
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                  m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

TDateTime __fastcall TFGetSchetDoc::GetBeginDate()
{
  return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TFGetSchetDoc::GetEndDate()
{
  return m_Q_list->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::SetBeginEndDate(const TDateTime &p_begin_date,
                                             const TDateTime &p_end_date)
{
  if( (int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  if( m_Q_list->Active )
  {
    m_Q_list->Close();
    m_Q_list->Open();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::FormShow(TObject *Sender)
{
  m_DBG_list->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_saveExecute(TObject *Sender)
{
  FPressOK = true;
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_saveUpdate(TObject *Sender)
{
  m_ACT_save->Enabled = !m_Q_listID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_DBG_listDblClick(TObject *Sender)
{
  if (m_ACT_save->Enabled) m_ACT_saveExecute(Sender);
}
//---------------------------------------------------------------------------
 
void __fastcall TFGetSchetDoc::m_ACT_insertExecute(TObject *Sender)
{
  TTSDocumentControl *m_doc = new TTSDocumentControl("TSRetail.Bill");
  bool res = false;
  long id = -1;
  try
  {
    if (m_doc->CanExecuteInsert)
    {
      TMLParams m_param;
      res = m_doc->ExecuteInsert(&id, m_param);
    }
  }
  __finally
  {
    if (m_doc) delete m_doc;
  }
  if (res)
  {
    m_Q_list->Close();
    m_Q_list->Open();
    m_Q_list->Locate(m_Q_listID->FieldName, id, TLocateOptions());
    if (m_Q_listID->AsInteger == id)
    {
      FPressOK = true;
      Close();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_insertUpdate(TObject *Sender)
{
  m_ACT_insert->Enabled = m_Schet_doc.CanInsert;
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_editExecute(TObject *Sender)
{
	TTSDocumentControl *m_doc = new TTSDocumentControl("TSRetail.Bill");
  bool res = false;
  long id = m_Q_listID->AsInteger;
  try
  {
    if (m_doc->CanExecuteEdit)
    {
      TMLParams m_param;
      res = m_doc->ExecuteEdit(id, m_param);
    }
  }
  __finally
  {
    if (m_doc) delete m_doc;
  }
  if (res)
  {
    m_Q_list->Close();
    m_Q_list->Open();
    m_Q_list->Locate(m_Q_listID->FieldName, id, TLocateOptions());
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_editUpdate(TObject *Sender)
{
  m_ACT_edit->Enabled = !m_Q_listID->IsNull &&
                        TTSDocumentAccess(m_Q_listDOC_FLDR_RIGHT->AsString).CanUpdate &&
                        (m_Q_listDOC_STATUS->AsString != "D");
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_deleteExecute(TObject *Sender)
{
	TTSDocumentControl *m_doc = new TTSDocumentControl("TSRetail.Bill");
  bool res = false;
  long id = m_Q_listID->AsInteger;
  try
  {
    if (m_doc->CanExecuteDelete)
    {
      TMLParams m_param;
      res = m_doc->ExecuteDelete(id, m_param);
    }
  }
  __finally
  {
    if (m_doc) delete m_doc;
  }
  if (res)
  {
    m_Q_list->Close();
    m_Q_list->Open();
    m_Q_list->Locate(m_Q_listID->FieldName, id, TLocateOptions());
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = TTSDocumentAccess(m_Q_listDOC_FLDR_RIGHT->AsString).CanDelete &&
                          (m_Q_listDOC_STATUS->AsString != "D") &&
                          !m_Q_listID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_viewExecute(TObject *Sender)
{
	TTSDocumentControl *m_doc = new TTSDocumentControl("TSRetail.Bill");
  try
  {
    if (m_doc->CanExecuteView)
    {
      long id = m_Q_listID->AsInteger;
      TMLParams m_param;
      m_doc->ExecuteView(id, m_param);
    }
  }
  __finally
  {
    if (m_doc) delete m_doc;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGetSchetDoc::m_ACT_viewUpdate(TObject *Sender)
{
  m_ACT_view->Enabled = TTSDocumentAccess(m_Q_listDOC_FLDR_RIGHT->AsString).CanSelect &&
                        !m_Q_listID->IsNull;
}
//---------------------------------------------------------------------------

