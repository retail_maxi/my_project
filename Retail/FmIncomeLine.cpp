//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmIncomeLine.h"
#include "TSErrors.h"
#include "MLFuncs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmDocumentLine"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "MLDBPanel"
#pragma link "FrmShelfLifeLists"
#pragma link "FrmRtlIncomeLine"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFIncomeLine::TFIncomeLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm) : TTSFDocumentLine(p_owner,p_dm),
                                                                            m_dm(static_cast<TDIncome*>(p_dm)),
                                                                            m_calc_flag(false)
{
  m_FRM_shelf_life->DbHandle = m_dm->m_DB_main->Handle;
  m_FRM_shelf_life->OrgId = m_dm->HomeOrgId;
  m_FRM_shelf_life->ReadOnly = false; //m_dm->m_Q_line->ReadOnly;
  m_FRM_shelf_life->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_shelf_life->AssortmentId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
  m_FRM_shelf_life->OpenDataSet();

  m_FRM_rtl_line->DbHandle = m_dm->m_DB_main->Handle;

  if (m_dm->m_Q_bar_codes->Active) m_dm->m_Q_bar_codes->Close();
  m_dm->m_Q_bar_codes->Open();
  if (m_dm->m_Q_sublines->Active) m_dm->m_Q_sublines->Close();
  m_dm->m_Q_sublines->Open();
};

//---------------------------------------------------------------------------
__fastcall TFIncomeLine::~TFIncomeLine()
{
  m_FRM_shelf_life->CloseDataSet();

  m_FRM_rtl_line->DbHandle = NULL;
  if (m_dm->m_Q_bar_codes->Active) m_dm->m_Q_bar_codes->Close();
  if (m_dm->m_Q_sublines->Active) m_dm->m_Q_sublines->Close();
};

//---------------------------------------------------------------------------

AnsiString __fastcall TFIncomeLine::BeforeLineApply(TWinControl *p_focused)
{
  AnsiString m_err_nmcl = m_dm->CheckRtlIncNmcl(m_dm->m_Q_lineNMCL_ID->AsInteger,m_dm->m_Q_lineASSORTMENT_ID->AsInteger);

  if (m_err_nmcl != "")
  {
    if( Application->MessageBox(m_err_nmcl.c_str(),
                              Application->Title.c_str(),
                              MB_ICONWARNING | MB_YESNO) == IDNO )
      {
        m_FRM_rtl_line->m_LV_nmcls->SetFocus();
        return "������������ ������������!";
      }
  }
  if (m_dm->m_Q_lineQNT->IsNull)
  {
    m_DBE_qnt->SetFocus();
    return "������� ����������";
  }
  if (m_dm->m_Q_lineSRC_AMOUNT->AsFloat == 0)
  {
    m_DBE_src_amount->SetFocus();
    return "������� ����� �� ���������";
  }
  if (m_dm->m_Q_lineIN_PRICE->AsFloat == 0)
  {
    m_DBE_in_price->SetFocus();
    return "������� ����*";
  }
  if (m_dm->m_Q_lineCOUNTRY_ID->IsNull)
  {
    m_LV_countries->SetFocus();
    return "������� ������ �������������";
  }
  if (m_dm->m_Q_lineCOUNTRY_ID->AsInteger != 129 &&  m_dm->m_Q_lineSCD_NUM->AsString.IsEmpty())
  {
    m_DBE_num_gtd->SetFocus();
    return "������� � ���";
  }
  if (m_dm->CurrLinesKind == TDIncome::lkNonstored && m_dm->m_Q_lineNON_STORING_REASON_ID->IsNull)
  {
    m_LV_reason->SetFocus();
    return "������� ���������� \"�� � �����\"";
  }

  return m_FRM_rtl_line->BeforeApply(p_focused);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::FormShow(TObject *Sender)
{
  TTSFDocumentLine::FormShow(Sender);

  if (m_dm->UpdateRegimeLine != urView && m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();

  m_dm->OnlyOpen = true;
  m_CB_view_only_open->Checked = m_dm->OnlyOpen;

  m_LV_reason->Visible = m_dm->CurrLinesKind == TDIncome::lkNonstored;
  m_L_reason->Visible = m_LV_reason->Visible;
  if (m_dm->CurrLinesKind == TDIncome::lkNonstored)
    Caption = m_dm->CurrLinesKind == TDIncome::lkNonstored ? "������ �� � �����" : "������ � �����";

  m_FRM_rtl_line->OrgId = m_dm->HomeOrgId;
  m_FRM_rtl_line->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_rtl_line->AssortId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
  m_FRM_rtl_line->OnlyOpen = m_dm->OnlyOpen;
  m_FRM_rtl_line->ReadOnly = m_dm->UpdateRegimeLine == urView;
  m_FRM_rtl_line->OpenDataSet();

  if (m_dm->UpdateRegimeLine == urInsert && !m_dm->m_Q_lineNMCL_ID->IsNull)
  {
    m_FRM_rtl_line->ChangeNmcl = false;
    try
    {
      m_FRM_rtl_linem_LOV_nmclsAfterApply(NULL);
      m_FRM_rtl_linem_LOV_assortAfterApply(NULL);
    }
    __finally
    {
      m_FRM_rtl_line->ChangeNmcl = true;
    }
    m_DBE_qnt->SetFocus();
  }

  if (CheckShelfHours())
  {
    m_DTP_make_time->Visible = true;
    m_DTP_make_time->Time = m_dm->m_Q_lineMAKE_DATE->AsFloat - int(m_dm->m_Q_lineMAKE_DATE->AsDateTime);
    m_DTP_make_time->Enabled = m_dm->UpdateRegimeLine != urView;
   }
  else
   {
     m_DTP_make_time->Visible = false;
   }

   m_BCS_subitem->Active = (m_dm->UpdateRegimeLine != urView);

}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::CalcValues(RecalcKind p_kind)
{
  if (m_calc_flag || m_dm->UpdateRegimeLine == urView) return;

  m_calc_flag = true;
  try
  {
     //if (m_dm->m_Q_lineOUT_PRICE->AsFloat < 0) throw ETSError("���� �� ����� ���� �������������");
     if (m_dm->m_Q_lineIN_PRICE->AsFloat < 0) throw ETSError("����* �� ����� ���� �������������");
     if (m_dm->m_Q_lineQNT->AsFloat < 0 && m_dm->m_Q_lineSTORED->AsInteger == 1) throw ETSError("���������� �� ����� ���� �������������");
     if (m_dm->m_Q_lineSRC_AMOUNT->AsFloat < 0 && m_dm->m_Q_lineSTORED->AsInteger == 1) throw ETSError("����� �� ��������� �� ����� ���� �������������");
     if (m_dm->m_Q_lineOUT_AMOUNT->AsFloat < 0 && m_dm->m_Q_lineSTORED->AsInteger == 1) throw ETSError("����� � ����� ���������� �� ����� ���� �������������");

     if (m_dm->m_Q_line->State != dsBrowse) m_dm->m_Q_line->Post();
     m_dm->m_Q_line->Edit();
     double
       qnt = m_dm->m_Q_lineQNT->AsFloat,
       in_price = m_dm->m_Q_lineIN_PRICE->AsFloat,
       out_price = m_dm->m_Q_lineOUT_PRICE->AsFloat,
       src_sum = m_dm->m_Q_lineSRC_AMOUNT->AsFloat;
     switch (p_kind)
     {
       case rkAll:
         if (m_dm->m_Q_itemSENDER_ID->AsInteger == 2000906 || m_dm->m_Q_itemSENDER_ID->AsInteger == 2069191)
           m_dm->m_Q_lineSRC_AMOUNT->AsFloat = MLDRound(qnt * in_price, 2);
         else
           m_dm->m_Q_lineIN_PRICE->AsFloat = qnt ? MLDRound(src_sum / qnt, 2) : 0;
         m_dm->m_Q_lineOUT_AMOUNT->AsFloat = MLDRound(out_price * qnt, 2);
         break;
       case rkInPrice:
         //m_dm->m_Q_lineSRC_AMOUNT->AsFloat = qnt * in_price;
         m_dm->m_Q_lineIN_PRICE->AsFloat = qnt ? MLDRound(src_sum / qnt, 2) : 0;
         m_dm->m_Q_lineINCREASE_PRC->AsFloat = in_price ? MLDRound((out_price/in_price-1)*100,2) : 0;
         break;
       case rkInSum:
         m_dm->m_Q_lineIN_PRICE->AsFloat = qnt ? MLDRound(m_dm->m_Q_lineSRC_AMOUNT->AsFloat/qnt,2) : 0;
         //m_dm->m_Q_lineOUT_PRICE->AsFloat = MLDRound((m_dm->m_Q_lineINCREASE_PRC->AsFloat/100 + 1) * m_dm->m_Q_lineIN_PRICE->AsFloat,2);
         m_dm->m_Q_lineOUT_AMOUNT->AsFloat = MLDRound(qnt * m_dm->m_Q_lineOUT_PRICE->AsFloat,2);
         m_dm->m_Q_lineINCREASE_PRC->AsFloat =
           m_dm->m_Q_lineIN_PRICE->AsFloat ?
           MLDRound((m_dm->m_Q_lineOUT_PRICE->AsFloat/m_dm->m_Q_lineIN_PRICE->AsFloat-1)*100,2) :
           0;
         break;
       case rkOutPrice:
         m_dm->m_Q_lineOUT_AMOUNT->AsFloat = MLDRound(qnt * out_price,2);
         m_dm->m_Q_lineINCREASE_PRC->AsFloat =
           in_price ?
           MLDRound((m_dm->m_Q_lineOUT_PRICE->AsFloat/m_dm->m_Q_lineIN_PRICE->AsFloat-1)*100,2) :
           0;
         break;
       case rkPercent:
         /*m_dm->m_Q_lineOUT_PRICE->AsFloat =
           in_price ?
           MLDRound((m_dm->m_Q_lineINCREASE_PRC->AsFloat/100 + 1) * m_dm->m_Q_lineIN_PRICE->AsFloat,2) :
           0;  */
         m_dm->m_Q_lineOUT_AMOUNT->AsFloat = MLDRound(qnt * m_dm->m_Q_lineOUT_PRICE->AsFloat,2);
         break;
     }
     m_dm->m_Q_line->Post();
  }
  __finally
  {
    m_calc_flag = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_DBE_qntExit(TObject *Sender)
{
  CalcValues(rkAll);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_DBE_src_amountExit(TObject *Sender)
{
  CalcValues(rkInSum);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_DBE_increase_prcExit(TObject *Sender)
{
//  CalcValues(rkPercent);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_DBE_in_priceExit(TObject *Sender)
{
  CalcValues(rkInPrice);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_DBE_out_priceExit(TObject *Sender)
{
  CalcValues(rkOutPrice);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_ACT_apply_updatesExecute(TObject *Sender)
{
  if(m_SBTN_save->CanFocus()) m_SBTN_save->SetFocus();
  CalcValues(rkAll);
  CalcValues(rkInSum);
  if (m_dm->m_Q_line->State != dsEdit) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineNMCL_ID->AsInteger = m_FRM_rtl_line->NmclId;
  m_dm->m_Q_lineASSORTMENT_ID->AsInteger = m_FRM_rtl_line->AssortId;
  m_dm->m_Q_line->Post();

//  TTSFDocumentLine::m_ACT_apply_updatesExecute(Sender);
  m_dm->ApplyUpdatesLine();
/*  if (m_P_wei->Visible && m_dm->NeedUpdatesMake() )
  {
  if (m_dm->m_Q_make_date->State != dsEdit) m_dm->m_Q_make_date->Edit();
   m_dm->m_Q_make_dateNMCL_ID->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
   m_dm->m_Q_make_dateASSORT_ID->AsInteger = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
   m_dm->ApplyUpdatesMake();
  }*/
  TTSFDialog::m_ACT_apply_updatesExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_FRM_rtl_linem_LOV_nmclsAfterApply(TObject *Sender)
{
  m_FRM_rtl_line->m_LOV_nmclsAfterApply(Sender);

  if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();

  m_dm->m_Q_lineNMCL_ID->AsInteger = m_FRM_rtl_line->NmclId;
  m_dm->m_Q_lineASSORTMENT_ID->AsInteger = m_FRM_rtl_line->AssortId;

  m_LV_countries->Enabled = !m_dm->m_Q_lineNMCL_ID->IsNull;

  /*
  m_dm->m_Q_assort->Close();
  m_dm->m_Q_assort->Open();

  m_dm->m_Q_nmcl_desc->Close();
  m_dm->m_Q_nmcl_desc->Open();
  m_dm->m_Q_lineASSORTMENT_ID->AsInteger = m_dm->m_Q_nmcl_descASSORTMENT_ID->AsInteger;
  m_dm->m_Q_lineASSORTMENT_NAME->AsString = m_dm->m_Q_nmcl_descASSORTMENT_NAME->AsString;

  m_dm->m_Q_is_assorted->Close();
  m_dm->m_Q_is_assorted->Open();
  if (!m_dm->m_Q_is_assortedVAL->AsInteger)
  {
    m_dm->m_Q_lineASSORTMENT_ID->Clear();
    m_dm->m_Q_lineASSORTMENT_NAME->Clear();
    m_LV_assort->Enabled = false;
  }
  else
    m_LV_assort->Enabled = true;
  */

  m_dm->m_Q_prices->Close();
  m_dm->m_Q_prices->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
  if (!m_dm->m_Q_lineASSORTMENT_ID->IsNull)
    m_dm->m_Q_prices->ParamByName("ASSORTMENT_ID")->AsInteger = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
  else
    m_dm->m_Q_prices->ParamByName("ASSORTMENT_ID")->Clear();
  m_dm->m_Q_prices->Open();
  //m_dm->m_Q_lineIN_PRICE->AsInteger = m_dm->m_Q_pricesIN_PRICE->AsInteger;
  m_dm->m_Q_lineOUT_PRICE->AsString = m_dm->m_Q_pricesOUT_PRICE->AsString;
  m_dm->m_Q_lineCUR_REST->AsFloat = m_dm->m_Q_pricesCUR_REST->AsFloat;

  m_dm->m_Q_get_gtd_list->Close();
  m_dm->m_Q_get_gtd_list->Open();
  if (!m_dm->m_Q_get_gtd_listCOUNTRY_ID->IsNull)
  {
    m_dm->m_Q_lineCOUNTRY_ID->AsInteger = m_dm->m_Q_get_gtd_listCOUNTRY_ID->AsInteger;
    m_dm->m_Q_lineCOUNTRY_NAME->AsString = m_dm->m_Q_get_gtd_listCOUNTRY_NAME->AsString;
    m_dm->m_Q_lineSCD_NUM->AsString = m_dm->m_Q_get_gtd_listNUM_GTD->AsString;
  }
  else
  {
    m_dm->m_Q_lineCOUNTRY_ID->AsInteger = 129;
    m_dm->m_Q_lineCOUNTRY_NAME->AsString = "������";
    m_dm->m_Q_lineSCD_NUM->AsString = "";
  }
  
  if (m_dm->m_Q_nmcl_info->Active) m_dm->m_Q_nmcl_info->Close();
  m_dm->m_Q_nmcl_info->Open();
  m_dm->m_Q_lineNOTE_BC->AsString  = m_dm->m_Q_nmcl_infoBAR_CODE_NOTE->AsString;
  m_dm->m_Q_nmcl_info->Close();

  m_dm->m_Q_line->Post();

  m_FRM_shelf_life->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_shelf_life->AssortmentId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;

  m_dm->m_Q_bar_codes->Close();
  m_dm->m_Q_bar_codes->Open();

  m_dm->m_Q_sublines->Close();
  m_dm->m_Q_sublines->Open();

  m_dm->m_Q_tax_rate->Close();
  m_dm->m_Q_tax_rate->Open();

  m_DTP_make_time->Visible = CheckShelfHours() ;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_FRM_rtl_linem_LOV_nmclsAfterClear(TObject *Sender)
{
  m_FRM_rtl_line->m_LOV_nmclsAfterClear(Sender);

  m_LV_countries->Enabled = false;

  if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineNMCL_ID->Clear();
  m_dm->m_Q_lineASSORTMENT_ID->Clear();

  m_dm->m_Q_lineCOUNTRY_ID->AsInteger = 129;
  m_dm->m_Q_lineCOUNTRY_NAME->AsString = "������";
  m_dm->m_Q_lineSCD_NUM->AsString = "";
  m_dm->m_Q_line->Post();

  m_FRM_shelf_life->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_shelf_life->AssortmentId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;

  m_dm->m_Q_bar_codes->Close();
  m_dm->m_Q_bar_codes->Open();

  m_dm->m_Q_sublines->Close();
  m_dm->m_Q_sublines->Open();

  m_dm->m_Q_tax_rate->Close();
  m_dm->m_Q_tax_rate->Open();

  m_DTP_make_time->Visible = CheckShelfHours() ;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_FRM_rtl_linem_LOV_assortAfterApply(TObject *Sender)
{
  if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineASSORTMENT_ID->AsInteger = m_FRM_rtl_line->AssortId;

  m_dm->m_Q_prices->Close();
  m_dm->m_Q_prices->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
  if (!m_dm->m_Q_lineASSORTMENT_ID->IsNull)
    m_dm->m_Q_prices->ParamByName("ASSORTMENT_ID")->AsInteger = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
  else
    m_dm->m_Q_prices->ParamByName("ASSORTMENT_ID")->Clear();
  m_dm->m_Q_prices->Open();
  //m_dm->m_Q_lineIN_PRICE->AsInteger = m_dm->m_Q_pricesIN_PRICE->AsInteger;
  m_dm->m_Q_lineOUT_PRICE->AsString = m_dm->m_Q_pricesOUT_PRICE->AsString;
  m_dm->m_Q_lineCUR_REST->AsFloat = m_dm->m_Q_pricesCUR_REST->AsFloat;

  if (m_dm->m_Q_nmcl_info->Active) m_dm->m_Q_nmcl_info->Close();
  m_dm->m_Q_nmcl_info->Open();
  m_dm->m_Q_lineNOTE_BC->AsString = m_dm->m_Q_nmcl_infoBAR_CODE_NOTE->AsString;
  m_dm->m_Q_nmcl_info->Close();

  m_dm->m_Q_bar_codes->Close();
  m_dm->m_Q_bar_codes->Open();

  m_dm->m_Q_sublines->Close();
  m_dm->m_Q_sublines->Open();

  m_FRM_shelf_life->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_shelf_life->AssortmentId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;

  m_DTP_make_time->Visible = CheckShelfHours() ;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_FRM_rtl_linem_LOV_assortAfterClear(TObject *Sender)
{
  if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineASSORTMENT_ID->Clear();

  m_dm->m_Q_bar_codes->Close();
  m_dm->m_Q_bar_codes->Open();

  m_dm->m_Q_sublines->Close();
  m_dm->m_Q_sublines->Open();

  m_FRM_shelf_life->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_shelf_life->AssortmentId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;

  m_DTP_make_time->Visible = CheckShelfHours() ;
}
//---------------------------------------------------------------------------


void __fastcall TFIncomeLine::m_CB_view_only_openClick(TObject *Sender)
{
  m_dm->OnlyOpen = m_CB_view_only_open->Checked;
  m_FRM_rtl_line->OnlyOpen = m_dm->OnlyOpen;  
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_DTP_make_timeChange(TObject *Sender)
{
  m_dm->MakeDate = int(m_dm->m_Q_lineMAKE_DATE->AsDateTime) + m_DTP_make_time->Time - int(m_DTP_make_time->Time);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_ACT_apply_updatesUpdate(TObject *Sender)
{
  m_ACT_apply_updates->Enabled = m_dm->NeedUpdatesLine();
}
//---------------------------------------------------------------------------
bool __fastcall TFIncomeLine::CheckShelfHours()
{
   bool res = false;
   m_P_wei->Visible = false;
   // ���������� ������ "�� � �����"
   if (m_dm->m_Q_lineSTORED->AsInteger == 0) return res;

   if (m_dm->m_Q_chk_hours->Active)  m_dm->m_Q_chk_hours->Close();
    m_dm->m_Q_chk_hours->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
    m_dm->m_Q_chk_hours->ParamByName("ASSORT_ID")->AsInteger = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
    m_dm->m_Q_chk_hours->Open();
  if (m_dm->m_Q_chk_hoursIS_WEIGHT->AsInteger  > 0)
  {
  m_P_wei->Visible = true;
  res = !m_dm->m_Q_chk_hoursSHELF_LIFE->IsNull;

   if (!res && m_dm->UpdateRegimeLine != urView)
   // ������� �����
   {
     if (m_dm->m_Q_line->State != dsEdit ) m_dm->m_Q_line->Edit();

      if(m_dm->m_Q_lineMAKE_DATE->IsNull)
       m_dm->m_Q_lineMAKE_DATE->Clear();
      else
      m_dm->m_Q_lineMAKE_DATE->AsDateTime = int(m_dm->m_Q_lineMAKE_DATE->AsDateTime);
   }
  }
  //else
  //m_P_wei->Visible = false;
  return res;
}
//---------------------------------------------------------------------------
void __fastcall TFIncomeLine::m_BCS_subitemBegin(TObject *Sender)
{
  m_old_sublines_grid_options = m_MLDBG_sublines->OptionsML;
  m_MLDBG_sublines->OptionsML = TMLGridOptions() << goWithoutFind;

}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_BCS_subitemEnd(TObject *Sender)
{
  try
  {
    m_PC_bottom->ActivePage = m_TS_sublines;
    m_dm->m_Q_sub_ins->ParamByName("DOC_ID")->Value = m_dm->m_Q_linesID->Value;
    m_dm->m_Q_sub_ins->ParamByName("LINE_ID")->Value = m_dm->m_Q_linesLINE_ID->Value;
    m_dm->m_Q_sub_ins->ParamByName("src_pdf_alccode")->AsString = m_BCS_subitem->GetResult();

    //ShowMessage(m_BCS_subitem->GetResult());
    if (m_BCS_subitem->GetResult().Length() > 0){
    m_dm->m_Q_sub_ins->ExecSQL();
    m_dm->m_Q_sublines->Close();
    m_dm->m_Q_sublines->Open();
    }
  }
  __finally
  {
    m_MLDBG_sublines->OptionsML = m_old_sublines_grid_options;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_MLDBG_sublinesEnter(TObject *Sender)
{
  //m_BCS_subitem->Active = true;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_MLDBG_sublinesExit(TObject *Sender)
{
  //m_BCS_subitem->Active = false;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_ACT_deleteExecute(TObject *Sender)
{
  m_dm->m_Q_sub_del->ParamByName("DOC_ID")->Value = m_dm->m_Q_sublinesID->Value;
  m_dm->m_Q_sub_del->ParamByName("LINE_ID")->Value = m_dm->m_Q_sublinesLINE_ID->Value;
  m_dm->m_Q_sub_del->ParamByName("SUB_LINE_ID")->Value = m_dm->m_Q_sublinesSUB_LINE_ID->Value;
  m_dm->m_Q_sub_del->ExecSQL();
  m_dm->m_Q_sublines->Close();
  m_dm->m_Q_sublines->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeLine::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = ( m_dm->UpdateRegimeLine != urView && !m_dm->m_Q_sublinesID->IsNull );
}
//---------------------------------------------------------------------------


