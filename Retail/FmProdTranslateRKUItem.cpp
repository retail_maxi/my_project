//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmProdTranslateRKUItem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "MLLovView"
#pragma link "MLLov"
#pragma link "RxLookup"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

AnsiString __fastcall TFProdTranslateRKUItem::BeforeItemApply(TWinControl **p_focused)
{
  if( m_dm->UpdateRegimeItem == urDelete ) return "";

  if(m_dm->m_Q_itemORG_ID->IsNull)
  {
    *p_focused = m_LV_org;
    return "������� �����������!";
  }
  if(m_dm->m_Q_itemNMCL_ID->IsNull)
  {
    *p_focused = m_LV_nmcl;
    return "������� ������������ �������!";
  }
  if(m_dm->m_Q_itemASSORT_ID->IsNull)
  {
    *p_focused = m_LV_assort;
    return "������� ����������� �������!";
  }
  if(m_dm->m_Q_itemTO_GR_DEP_ID->IsNull)
  {
    *p_focused = m_LV_gr_dep;
    return "������� ����� ��������!";
  }
  if(m_dm->m_Q_itemTO_NMCL_ID->IsNull)
  {
    *p_focused = m_LV_nmcl_to;
    return "������� ������������ ��������!";
  }
  if(m_dm->m_Q_itemTO_ASSORT_ID->IsNull)
  {
    *p_focused = m_LV_assort_to;
    return "������� ����������� ��������!";
  }
  if(m_dm->m_Q_itemCOEF->AsFloat == 0)
  {
    *p_focused = m_DBE_coef;
    return "������� �����������!";
  }
  if(m_dm->m_Q_itemNMCL_ID->AsInteger == m_dm->m_Q_itemTO_NMCL_ID->AsInteger &&
     m_dm->m_Q_itemASSORT_ID->AsInteger == m_dm->m_Q_itemTO_ASSORT_ID->AsInteger)
  {
    *p_focused = m_LV_nmcl_to;
    return "������������ �� ����� ���������!";
  }

  return "";
}
//---------------------------------------------------------------------------


void __fastcall TFProdTranslateRKUItem::m_LOV_orgAfterApply(
      TObject *Sender)
{
  if (m_dm->m_Q_item->State != dsEdit) m_dm->m_Q_item->Edit();
  m_dm->m_Q_itemTO_GR_DEP_ID->Clear();
  m_dm->m_Q_itemTO_GR_DEP_NAME->Clear();
  m_dm->m_Q_item->Post();
}
//---------------------------------------------------------------------------


void __fastcall TFProdTranslateRKUItem::m_LOV_nmclAfterApply(
      TObject *Sender)
{
  if (m_dm->m_Q_item->State != dsEdit) m_dm->m_Q_item->Edit();
  m_dm->m_Q_itemASSORT_ID->Clear();
  m_dm->m_Q_itemASSORT_NAME->Clear();
  m_dm->m_Q_item->Post();
}
//---------------------------------------------------------------------------

void __fastcall TFProdTranslateRKUItem::m_LOV_nmcl_toAfterApply(
      TObject *Sender)
{
  if (m_dm->m_Q_item->State != dsEdit) m_dm->m_Q_item->Edit();
  m_dm->m_Q_itemTO_ASSORT_ID->Clear();
  m_dm->m_Q_itemTO_ASSORT_NAME->Clear();
  m_dm->m_Q_item->Post();
}
//---------------------------------------------------------------------------

