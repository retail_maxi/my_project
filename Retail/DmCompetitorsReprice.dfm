inherited DCompetitorsReprice: TDCompetitorsReprice
  Left = 344
  Top = 134
  Height = 640
  Width = 870
  inherited m_Q_list: TMLQuery
    BeforeOpen = m_Q_listBeforeOpen
    SQL.Strings = (
      'SELECT d.id AS id,'
      '       d.src_org_id AS doc_src_org_id,'
      '       o.name AS doc_src_org_name,'
      '       d.create_date AS doc_create_date,'
      '       d.comp_name AS doc_create_comp_name,'
      '       d.author AS doc_create_user,'
      '       d.modify_date AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user AS doc_modify_user,'
      '       d.status AS doc_status,'
      
        '       decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �' +
        '� ������'#39','#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) AS doc' +
        '_fldr_right,'
      '       dr.issued_date,'
      '       dr.doc_number,'
      '       dr.org_id as org_id,'
      '       dr.note,'
      '       od.name as org_name'
      'FROM documents d,'
      '     folders f,'
      '     organizations o,'
      '     organizations od,'
      '     doc_comp_reprice dr'
      'WHERE f.doc_type_id = :DOC_TYPE_ID'
      '  AND f.id = d.fldr_id'
      '  AND o.id = d.src_org_id'
      '  AND fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '  AND dr.doc_id = d.id'
      '  AND od.id = dr.org_id'
      
        '  AND (od.id = :HOME_ORG_ID OR :HOME_ORG_ID = 3 OR :HOME_ORG_ID ' +
        '= 37)'
      '  AND dr.issued_date BETWEEN :BEGIN_DATE AND :END_DATE'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_listISSUED_DATE: TDateTimeField
      DisplayLabel = '���� �����'
      FieldName = 'ISSUED_DATE'
      Origin = 'dr.issued_date'
    end
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '����� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'dr.doc_number'
      Size = 50
    end
    object m_Q_listORG_ID: TFloatField
      Tag = 5
      DisplayLabel = 'ID �����'
      FieldName = 'ORG_ID'
      Origin = 'dr.org_id'
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'dr.note'
      Size = 250
    end
    object m_Q_listORG_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'ORG_NAME'
      Origin = 'od.name'
      Size = 250
    end
  end
  inherited m_Q_item: TMLQuery
    BeforeClose = m_Q_itemBeforeClose
    SQL.Strings = (
      'SELECT '
      '    dr.doc_id as id,'
      '    dr.issued_date,'
      '    dr.doc_number,'
      '    dr.org_id as org_id,'
      '    dr.note,'
      '    od.name as org_name'
      'FROM organizations od,'
      '     doc_comp_reprice dr'
      'WHERE dr.doc_id = :ID'
      
        '  AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_id' +
        ' = :FLDR_ID) '
      '  AND od.id = dr.org_id'
      '')
    object m_Q_itemISSUED_DATE: TDateTimeField
      FieldName = 'ISSUED_DATE'
    end
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 50
    end
    object m_Q_itemORG_ID: TFloatField
      FieldName = 'ORG_ID'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemORG_NAME: TStringField
      FieldName = 'ORG_NAME'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_comp_reprice'
      'set'
      '  ORG_ID = :ORG_ID,'
      '  DOC_NUMBER = :DOC_NUMBER,'
      '  ISSUED_DATE = :ISSUED_DATE,'
      '  NOTE = :NOTE'
      'where'
      '  DOC_ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into doc_comp_reprice'
      '  (DOC_ID, ORG_ID, DOC_NUMBER, ISSUED_DATE, NOTE)'
      'values'
      '  (:ID, :ORG_ID, :DOC_NUMBER, :ISSUED_DATE, :NOTE)')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select v.id, '
      '      v.line_id,'
      '       v.nmcl_id,'
      '      v.nmcl_name,'
      '      v.mu_name,'
      '      v.in_price,'
      '      v.out_price,'
      '      v.new_price,'
      '      v.note'
      'from'
      '( select :ID AS id, '
      '       l.line_id, '
      '       ni.id as nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       mu.name as mu_name,'
      
        '       pkg_rtl_prices.fnc_cur_in_price(ni.id, null, :ORG_ID)  AS' +
        ' in_price,'
      
        '       pkg_rtl_prices.fnc_cur_out_price(ni.id, null, :ORG_ID)  A' +
        'S out_price,'
      '       l.new_price, '
      '       l.note '
      'from nomenclature_items ni,'
      '     measure_units mu,'
      '     doc_comp_reprice_items l,'
      '     (  select distinct g.nmcl_id '
      '        from goods_record_cards g,'
      '             goods.retail_goods_cards rc'
      '        where rc.card_id = g.id'
      '             and rc.store_qnt>0'
      '     ) store_nmcls'
      'where l.nmcl_id(+) = ni.id '
      '    and l.doc_id(+) = :ID'
      '    and ni.prod_type_id = 464'
      '    AND mu.id = ni.meas_unit_id'
      '    and store_nmcls.nmcl_id = ni.id'
      ') v'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'v.id'
    end
    inherited m_Q_linesLINE_ID: TFloatField
      Origin = 'v.line_id'
      Visible = False
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'v.nmcl_id'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'v.nmcl_name'
      Size = 100
    end
    object m_Q_linesMU_NAME: TStringField
      DisplayLabel = '�� ���.'
      FieldName = 'MU_NAME'
      Origin = 'v.mu_name'
      Size = 30
    end
    object m_Q_linesIN_PRICE: TFloatField
      DisplayLabel = '����*'
      FieldName = 'IN_PRICE'
      Origin = 'v.in_price'
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '���� �� �����'
      FieldName = 'OUT_PRICE'
      Origin = 'v.out_price'
    end
    object m_Q_linesNEW_PRICE: TFloatField
      DisplayLabel = '����� ����'
      FieldName = 'NEW_PRICE'
      Origin = 'v.new_price'
    end
  end
  inherited m_Q_line: TMLQuery
    BeforeOpen = m_Q_lineBeforeOpen
    SQL.Strings = (
      'SELECT l.doc_id AS id, '
      '       l.line_id, '
      '       l.nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       l.new_price, '
      
        '    --   pkg_goods_prices.fnc_cur_price(l.nmcl_id ,null,dr.org_i' +
        'd)  AS cur_price,'
      '       l.note,'
      '       :COMP_ID as comp_id,'
      '       dcp.comp_price'
      'FROM doc_comp_reprice_items l,'
      '     nomenclature_items ni,'
      '     doc_comp_prices dcp'
      'WHERE l.doc_id = :ID '
      '      AND ni.id = l.nmcl_id '
      '      AND l.line_id = :LINE_ID'
      '      AND dcp.doc_id(+) = l.doc_id'
      '      AND dcp.line_id(+) = l.line_id'
      '      AND dcp.comp_id(+) = :COMP_ID')
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COMP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'COMP_ID'
        ParamType = ptInput
      end>
    inherited m_Q_lineID: TFloatField
      Visible = False
    end
    inherited m_Q_lineLINE_ID: TFloatField
      Visible = False
    end
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_lineNEW_PRICE: TFloatField
      FieldName = 'NEW_PRICE'
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Size = 252
    end
    object m_Q_lineCOMP_ID: TFloatField
      FieldName = 'COMP_ID'
    end
    object m_Q_lineCOMP_PRICE: TFloatField
      FieldName = 'COMP_PRICE'
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      'begin'
      ' if (:OLD_COMP_PRICE is not null) then'
      '   update doc_comp_prices'
      '   set comp_price = :COMP_PRICE'
      '   where doc_id = :ID '
      '      and line_id = :LINE_ID'
      '      and comp_id = :COMP_ID;'
      ' else'
      '    insert into doc_comp_prices'
      '    (doc_id, line_id, comp_id, comp_price)'
      '    values '
      '    (:ID, :LINE_ID, :COMP_ID, :COMP_PRICE);'
      ' end if;'
      'end;')
    InsertSQL.Strings = (
      'begin'
      '  insert into doc_comp_reprice_items'
      '  (doc_id, line_id, nmcl_id, note)'
      '  values'
      '  (:ID, :LINE_ID, :NMCL_ID, :NOTE);'
      ''
      '  insert into doc_comp_prices'
      '  (doc_id, line_id, comp_id, comp_price)'
      '  values'
      '  (:ID, :LINE_ID, :COMP_ID, :COMP_PRICE);'
      'end;')
    DeleteSQL.Strings = (
      'begin'
      '  delete from doc_comp_prices'
      '  where doc_id = :OLD_ID and'
      '      line_id = :OLD_LINE_ID and'
      '      comp_id = :OLD_COMP_ID;'
      ' '
      '  delete from doc_comp_reprice_items'
      '  where doc_id = :OLD_ID and'
      '        line_id = :OLD_LINE_ID'
      '        and not exists (  select comp_id  from doc_comp_prices'
      
        '                          where doc_id = :OLD_ID and  line_id = ' +
        ':OLD_LINE_ID ); '
      'end;')
    IgnoreRowsAffected = True
  end
  object m_Q_comp_one: TMLQuery
    BeforeOpen = m_Q_comp_oneBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select'
      '    c.name as comp_name'
      'from '
      '    competitors c'
      'where c.id = :COMP_ID')
    Macros = <>
    Left = 152
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COMP_ID'
        ParamType = ptInput
      end>
    object m_Q_comp_oneCOMP_NAME: TStringField
      FieldName = 'COMP_NAME'
      Origin = 'TSDBMAIN.COMPETITORS.NAME'
      Size = 250
    end
  end
  object m_Q_competitors: TMLQuery
    BeforeOpen = m_Q_competitorsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select c.id, c.org_id, c.name'
      'from competitors c'
      'where ( c.org_id in (select opp.id_parent '
      '                   from org_paging_prices opp '
      '                   where opp.id_child = :ORG_ID)'
      '   OR c.org_id = :ORG_ID ) AND'
      '   (c.enabled = '#39'Y'#39' OR EXISTS (select 1 '
      '                                  from doc_comp_prices dcp'
      
        '                                  where dcp.doc_id = :ID AND dcp' +
        '.comp_id = c.id'
      '                                  ))'
      '')
    Macros = <>
    Left = 56
    Top = 376
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_competitorsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.COMPETITORS.ID'
    end
    object m_Q_competitorsORG_ID: TFloatField
      FieldName = 'ORG_ID'
      Origin = 'TSDBMAIN.COMPETITORS.ORG_ID'
    end
    object m_Q_competitorsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.COMPETITORS.NAME'
      Size = 250
    end
  end
  object m_Q_lines_start: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select v.id, '
      '      v.line_id,'
      '       v.nmcl_id,'
      '      v.nmcl_name,'
      '      v.mu_name,'
      '      v.in_price,'
      '      v.out_price,'
      '      v.new_price,'
      '      v.note'
      '')
    Macros = <>
    Left = 216
    Top = 400
    object m_Q_lines_startID: TFloatField
      FieldName = 'ID'
      Visible = False
    end
    object m_Q_lines_startLINE_ID: TFloatField
      DisplayLabel = 'LID'
      FieldName = 'LINE_ID'
      Visible = False
    end
    object m_Q_lines_startNMCL_ID: TFloatField
      DisplayLabel = '��� ������������'
      FieldName = 'NMCL_ID'
    end
    object m_Q_lines_startNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_lines_startMU_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MU_NAME'
      Size = 30
    end
    object m_Q_lines_startIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
    end
    object m_Q_lines_startOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
    end
    object m_Q_lines_startNEW_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'NEW_PRICE'
    end
  end
  object m_Q_lines_end: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      ' from'
      '( select :ID AS id, '
      '       l.line_id, '
      '       ni.id as nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       mu.name as mu_name,'
      
        '       pkg_rtl_prices.fnc_cur_in_price(ni.id, null, :ORG_ID)  AS' +
        ' in_price,'
      
        '       pkg_rtl_prices.fnc_cur_out_price(ni.id, null, :ORG_ID)  A' +
        'S out_price,'
      '       l.new_price, '
      '       l.note '
      'from nomenclature_items ni,'
      '     measure_units mu,'
      '     doc_comp_reprice_items l,'
      '     (  select distinct g.nmcl_id '
      '        from goods_record_cards g,'
      '             goods.retail_goods_cards rc'
      '        where rc.card_id = g.id'
      '             and rc.store_qnt>0'
      '     ) store_nmcls'
      'where l.nmcl_id(+) = ni.id '
      '    and l.doc_id(+) = :ID'
      '    and ni.prod_type_id = 464'
      '    AND mu.id = ni.meas_unit_id'
      '    and store_nmcls.nmcl_id = ni.id'
      ') v'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 312
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'ORG_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'ORG_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_org: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select o.id, o.name'
      'from organizations o'
      'where o.id = SESSION_GLOBALS.GET_HOME_ORG_ID')
    Macros = <>
    Left = 56
    Top = 440
    object m_Q_orgID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_orgNAME: TStringField
      FieldName = 'NAME'
      Size = 250
    end
  end
end
