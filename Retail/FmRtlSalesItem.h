//---------------------------------------------------------------------------

#ifndef FmRtlSalesItemH
#define FmRtlSalesItemH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENT.h"
#include "DmRtlSales.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "MLDBPanel.h"
#include "TSReportControls.h"
//---------------------------------------------------------------------------

class TFRtlSalesItem : public TTSFDocument
{
__published:
  TPanel *Panel1;
  TDataSource *m_DS_checks;
  TMLDBGrid *MLDBGrid1;
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TMLDBPanel *MLDBPanel1;
  TMLDBPanel *MLDBPanel2;
  TMLDBPanel *MLDBPanel3;
  TMLDBPanel *MLDBPanel4;
  TMLDBPanel *MLDBPanel5;
  TMLDBPanel *MLDBPanel6;
  TMLDBGrid *MLDBGrid2;
  TDataSource *m_DS_lines;
  TPanel *Panel2;
  TAction *m_ACT_print_check;
  TSpeedButton *SpeedButton1;
  void __fastcall m_ACT_print_checkExecute(TObject *Sender);
  void __fastcall m_ACT_print_checkUpdate(TObject *Sender);
private:
  TDRtlSales *m_dm;
  TTSReportControl m_check;
public:
  __fastcall TFRtlSalesItem(TComponent *p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

