//---------------------------------------------------------------------------

#ifndef FmGetSchetDocH
#define FmGetSchetDocH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBTables.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <ActnList.hpp>
#include "MLActionsControls.h"
#include <DBActns.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "MLQuery.h"
#include "RxQuery.hpp"
#include "TSDMCUSTOM.h"
#include "TSAccessControl.h"
#include "TSFormControls.h"
//---------------------------------------------------------------------------
class TFGetSchetDoc : public TForm
{
__published:	// IDE-managed Components
  TPanel *m_P_main_control;
  TToolBar *m_TB_main_control_buttons_dialog;
  TBitBtn *m_SBTN_save;
  TBitBtn *m_BBTN_close;
  TImageList *m_IL_main;
  TDatabase *m_DBSchetDoc;
  TToolBar *m_TB_Schet_doc;
  TActionList *m_AL_main;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TSpeedButton *m_SBTN_set_date;
  TToolButton *m_TBTN_filter;
  TMLDSFilter *m_ACT_filter;
  TMLDSOrder *m_ACT_order;
  TMLDBGridFind *m_ACT_find;
  TMLDBGridFindAgain *m_ACT_find_again;
  TMLDBGridSetColumns *m_ACT_set_columns;
  TToolButton *m_TBTN_find;
  TToolButton *m_TBTN_find_again;
  TToolButton *m_TBTN_order;
  TToolButton *m_TBTN_sep1;
  TToolButton *m_TBTN_set_columns;
  TToolButton *m_TBTN_sep2;
  TMLDBGrid *m_DBG_list;
  TMLQuery *m_Q_list;
  TDataSource *m_DS_list;
  TAction *m_ACT_save;
  TToolBar *m_TB_main_control_buttons_list;
  TSpeedButton *m_SBTN_insert;
  TSpeedButton *m_SBTN_edit;
  TSpeedButton *m_SBTN_delete;
  TSpeedButton *m_SBTN_view;
  TAction *m_ACT_insert;
  TAction *m_ACT_edit;
  TAction *m_ACT_delete;
  TAction *m_ACT_view;
        TFloatField *m_Q_listID;
        TFloatField *m_Q_listDOC_SRC_ORG_ID;
        TStringField *m_Q_listDOC_SRC_ORG_NAME;
        TStringField *m_Q_listDOC_CREATE_COMP_NAME;
        TDateTimeField *m_Q_listDOC_CREATE_DATE;
        TStringField *m_Q_listDOC_CREATE_USER;
        TStringField *m_Q_listDOC_STATUS;
        TStringField *m_Q_listDOC_STATUS_NAME;
        TDateTimeField *m_Q_listDOC_STATUS_CHANGE_DATE;
        TFloatField *m_Q_listDOC_FLDR_ID;
        TStringField *m_Q_listDOC_FLDR_NAME;
        TStringField *m_Q_listDOC_FLDR_RIGHT;
        TStringField *m_Q_listDOC_MODIFY_COMP_NAME;
        TStringField *m_Q_listDOC_MODIFY_USER;
        TDateTimeField *m_Q_listDOC_MODIFY_DATE;
        TStringField *m_Q_listDOC_NUMBER;
        TDateTimeField *m_Q_listDOC_DATE;
        TDateTimeField *m_Q_listON_DATE;
        TFloatField *m_Q_listAMOUNT;
        TStringField *m_Q_listNOTE;
        TStringField *m_Q_listACC_NOTE;
        TFloatField *m_Q_listCNT_ID;
        TStringField *m_Q_listCNT_NAME;
        TFloatField *m_Q_listMNGR_ID;
        TStringField *m_Q_listMNGR_NAME;
        TFloatField *m_Q_listSRC_ORG_ID;
        TStringField *m_Q_listSRC_ORG_NAME;
        TStringField *m_Q_listPPD_DOC_NUMBER;
        TStringField *m_Q_listPPD_BUH_NUMBER;
        TDateTimeField *m_Q_listPPD_ISSUED_DATE;
        TDateTimeField *m_Q_listPPD_INCOME_DATE;
        TFloatField *m_Q_listPPD_AMOUNT;
        TFloatField *m_Q_listPPD_AMOUNT_NONDS20;
        TFloatField *m_Q_listPPD_AMOUNT_NDS20;
        TFloatField *m_Q_listPPD_AMOUNT_NONDS10;
        TFloatField *m_Q_listPPD_AMOUNT_NDS10;
        TFloatField *m_Q_listPPD_AMOUNT_NDS0;
        TFloatField *m_Q_listPPD_AMOUNT_NONDS;
        TStringField *m_Q_listPPD_NOTE;
        TStringField *m_Q_listINCOME_NOTE;
  void __fastcall m_BBTN_closeClick(TObject *Sender);
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_ACT_saveExecute(TObject *Sender);
  void __fastcall m_ACT_saveUpdate(TObject *Sender);
  void __fastcall m_DBG_listDblClick(TObject *Sender);
  void __fastcall m_ACT_insertExecute(TObject *Sender);
  void __fastcall m_ACT_insertUpdate(TObject *Sender);
  void __fastcall m_ACT_editExecute(TObject *Sender);
  void __fastcall m_ACT_editUpdate(TObject *Sender);
  void __fastcall m_ACT_deleteExecute(TObject *Sender);
  void __fastcall m_ACT_deleteUpdate(TObject *Sender);
  void __fastcall m_ACT_viewExecute(TObject *Sender);
  void __fastcall m_ACT_viewUpdate(TObject *Sender);
private:	// User declarations
  HKEY m_reg_root_key;
  bool FPressOK;
  bool __fastcall GetPressOK();
  hDBIDb m_db_handle;
  TTSDCustom *m_dm;
  long m_doc_id;
  long __fastcall GetDocId();
  void __fastcall SetDocId(long p_value);
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
  TTSDocumentTypeAccess m_Schet_doc;
public:		// User declarations
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
  __property bool PressOK  = { read=GetPressOK };
  __property long DocId  = { read=GetDocId, write=SetDocId };
  __fastcall TFGetSchetDoc(TComponent* Owner, hDBIDb p_db_handle, TTSDCustom *p_dm);
  __fastcall ~TFGetSchetDoc();
};
//---------------------------------------------------------------------------
#endif
