inherited FZreportsUnloading: TFZreportsUnloading
  Left = 358
  Top = 274
  PixelsPerInch = 96
  TextHeight = 13
  object m_P_param: TPanel [3]
    Left = 0
    Top = 26
    Width = 708
    Height = 135
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object m_SBTN_period: TSpeedButton
      Left = 155
      Top = 12
      Width = 250
      Height = 25
      Action = m_ACT_set_period
    end
    object m_L_dates: TLabel
      Left = 19
      Top = 16
      Width = 52
      Height = 13
      Caption = '�� ������'
    end
    object m_L_taxpayer: TLabel
      Left = 16
      Top = 48
      Width = 120
      Height = 13
      Caption = '�� �����������������'
    end
    object m_BBTN_refresh: TBitBtn
      Left = 19
      Top = 79
      Width = 126
      Height = 25
      Action = m_ACT_convert
      Caption = '�������� � xls'
      TabOrder = 0
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000008484000084840000848400008484000000000000FFFF
        0000848484008484840084848400848484008484840084848400848484008484
        84000000000084840000FFFF0000FFFF0000FFFFFF0000000000FF00FF000000
        0000FFFF00008484000084840000848400008484000084840000848400000000
        000084840000FFFF0000FFFF0000FFFFFF0000000000FF00FF00FF00FF00FF00
        FF0000000000FFFF000084840000848400008484000000000000000000008484
        0000FFFF0000FFFF0000FFFFFF00000000008484000000000000FF00FF00FF00
        FF00FF00FF0000000000FFFF000084840000000000008484000084840000FFFF
        0000FFFF0000FFFFFF0000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00000000000000000084840000FFFF0000FFFF0000FFFF
        0000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000000084840000FFFF0000FFFF0000FFFF0000FFFF
        FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000000084840000FFFF0000FFFF0000FFFFFF00FFFFFF000000
        0000848400008484840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000000084840000FFFF0000FFFF0000FFFFFF000000000000000000FFFF
        000084840000848400008484840000000000FF00FF00FF00FF00FF00FF000000
        000084840000FFFF0000FFFF0000FFFFFF0000000000FF00FF00FF00FF000000
        0000FFFF000084840000848400008484840000000000FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0000000000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object m_DBLCB_tax: TDBLookupComboBox
      Left = 155
      Top = 45
      Width = 250
      Height = 21
      KeyField = 'ID'
      ListField = 'NAME'
      ListFieldIndex = 1
      ListSource = m_DS_tax
      TabOrder = 1
      OnCloseUp = m_DBLCB_taxCloseUp
    end
  end
  object m_DBG_main: TMLDBGrid [4]
    Left = 0
    Top = 161
    Width = 708
    Height = 287
    Align = alClient
    DataSource = m_DS_main
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterColor = clWindow
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    OptionsML = [goWithoutSetColumns, goWithoutTitleFilter, goWithoutDrowTitle]
    Columns = <
      item
        FieldName = 'DOC_ID'
        Footers = <>
      end
      item
        FieldName = 'ORG_ID'
        Footers = <>
      end
      item
        FieldName = 'ORG_NAME'
        Width = 200
        Footers = <>
      end
      item
        FieldName = 'FISCAL_NUM'
        Footers = <>
      end
      item
        FieldName = 'REPORT_NUM'
        Footers = <>
      end
      item
        FieldName = 'REPORT_DATE'
        Footers = <>
      end
      item
        FieldName = 'REPORT_SUM'
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_convert: TMLDBGridConvert
      MLDBGrid = m_DBG_main
    end
    object m_ACT_set_period: TAction
      OnExecute = m_ACT_set_periodExecute
    end
  end
  inherited m_ACTL_main_add: TActionList
    Top = 40
  end
  object m_DS_main: TDataSource
    DataSet = DZreportsUnloading.m_Q_main
    Left = 16
    Top = 176
  end
  object m_DS_tax: TDataSource
    DataSet = DZreportsUnloading.m_Q_tax
    Left = 344
    Top = 74
  end
end
