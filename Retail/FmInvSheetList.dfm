inherited FInvSheetList: TFInvSheetList
  Left = 479
  Top = 255
  Width = 739
  Height = 559
  Caption = 'FInvSheetList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 471
    Width = 723
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 626
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 238
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 502
    Width = 723
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 723
    Height = 445
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 53
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 71
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 84
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 106
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 138
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 104
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 151
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 183
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 37
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 133
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 39
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ACT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 723
    inherited m_P_tb_main: TPanel
      Width = 676
      inherited m_TB_main: TToolBar
        Width = 676
        object ToolButton1: TToolButton
          Left = 302
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_DBLCB_organizations: TDBLookupComboBox
          Left = 310
          Top = 2
          Width = 185
          Height = 21
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = m_DS_orgs
          TabOrder = 0
          OnClick = m_DBLCB_organizationsClick
        end
        object ToolButton2: TToolButton
          Left = 495
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 18
          Style = tbsSeparator
        end
        object m_SBTN_set_date: TSpeedButton
          Left = 503
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 676
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'InvSheet'
      Caption = 'm_ACT_set_begin_end_date'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 40049
      EndDate = 40049
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DInvSheet.m_Q_list
  end
  object m_DS_orgs: TDataSource
    DataSet = DInvSheet.m_Q_orgs
    Left = 256
    Top = 8
  end
end
