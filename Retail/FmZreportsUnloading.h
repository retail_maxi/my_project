//---------------------------------------------------------------------------

#ifndef FmZreportsUnloadingH
#define FmZreportsUnloadingH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmZreportsUnloading.h"
#include "MLActionsControls.h"
#include "TSFmOperation.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Db.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
//---------------------------------------------------------------------------
class TFZreportsUnloading : public TTSFOperation
{
__published:	// IDE-managed Components
   TPanel *m_P_param;
   TDataSource *m_DS_main;
   TMLDBGrid *m_DBG_main;
   TSpeedButton *m_SBTN_period;
   TLabel *m_L_dates;
   TLabel *m_L_taxpayer;
   TBitBtn *m_BBTN_refresh;
   TDBLookupComboBox *m_DBLCB_tax;
   TDataSource *m_DS_tax;
   TAction *m_ACT_set_period;
   void __fastcall m_DBLCB_taxCloseUp(TObject *Sender);
   void __fastcall m_ACT_set_periodExecute(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
private:	// User declarations
   TDZreportsUnloading *m_dm;
   void __fastcall SetPeriodCaption(TDateTime p_begin_date, TDateTime p_end_date);
public:		// User declarations
   __fastcall TFZreportsUnloading(TComponent* p_owner, TTSDOperation *p_dm_operation);
};
//---------------------------------------------------------------------------
#endif
