//---------------------------------------------------------------------------

#ifndef AlcoJournalImplH
#define AlcoJournalImplH
//---------------------------------------------------------------------------
#include "TSModuleTemplate.h"
#include "DmAlcoJournal.h"
#include "FmAlcoJournal.h"
#include "TSRetail_TLB.h"

//---------------------------------------------------------------------------
#undef TS_ADDITION_INTERFACE_DECLARE
#define TS_ADDITION_INTERFACE_DECLARE TS_OPERATION_INTERFACE_DECLARE
#undef TS_ADDITION_INTERFACE_ENTRY
#define TS_ADDITION_INTERFACE_ENTRY TS_OPERATION_INTERFACE_ENTRY
#undef TS_ADDITION_INTERFACE_IMPL
#define TS_ADDITION_INTERFACE_IMPL TS_OPERATION_INTERFACE_IMPL

TS_FORM_COCLASS_IMPL(AlcoJournal)

//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------

