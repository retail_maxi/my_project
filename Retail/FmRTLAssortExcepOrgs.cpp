//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRTLAssortExcepOrgs.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void RTLAssortExcepOrgsDlg(TComponent* p_owner, int p_id, int p_org_group_id, TTSDCustom* p_dm, bool p_edit)
{
  TFRTLAssortExcepOrgs *form = new TFRTLAssortExcepOrgs(p_owner, p_dm,  p_edit);
  try
  {
    form->AfterCreate();

    try
    {
      
      form->m_Q_org->ParamByName("ID")->AsInteger = p_id;
      form->m_Q_org->Open();
      form->m_Q_all_org->ParamByName("ID")->AsInteger = p_id;
      form->m_Q_all_org->ParamByName("ORG_GROUP_ID")->AsInteger = p_org_group_id;
      form->m_Q_all_org->Open();
      form->ShowModal() == IDOK;
    }
    __finally
    {
      form->m_Q_all_org->Close();
      form->m_Q_org->Close();
    }
  }
  __finally
  {
    form->BeforeDestroy();
    delete form;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepOrgs::m_ACT_addExecute(TObject *Sender)
{

    if( Application->MessageBox(AnsiString("��������� �����?").c_str(),
                                Application->Title.c_str(),
                                MB_ICONQUESTION | MB_YESNO) == IDYES )
    { 
      m_Q_add_org->ParamByName("ID")->AsInteger = m_Q_org->ParamByName("ID")->AsInteger;
      m_Q_add_org->ParamByName("SQ_ID")->AsInteger = DMCustom->FillTmpReportParamValues(m_DBG_all_org, "ID");
      m_Q_add_org->ExecSQL();

      m_Q_all_org->Close();
      m_Q_org->Close();
      m_Q_org->Open();
      m_Q_all_org->Open();
    }


}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepOrgs::m_ACT_addUpdate(TObject *Sender)
{

    m_ACT_add->Enabled = m_Q_all_orgID->AsInteger > 0 && is_edit ;

}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepOrgs::m_ACT_deleteExecute(TObject *Sender)
{
  if( Application->MessageBox(AnsiString("�������� �����?").c_str(),
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES )
  {

    m_Q_delete_org->ParamByName("ID")->AsInteger = m_Q_org->ParamByName("ID")->AsInteger;
    m_Q_delete_org->ParamByName("SQ_ID")->AsInteger = DMCustom->FillTmpReportParamValues(m_DBG_org, "ORG_ID");
    m_Q_delete_org->ExecSQL();

    m_Q_all_org->Close();
    m_Q_org->Close();
    m_Q_org->Open();
    m_Q_all_org->Open();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepOrgs::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = m_Q_orgORG_ID->AsInteger > 0 && is_edit ;
}
//---------------------------------------------------------------------------


