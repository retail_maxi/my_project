//---------------------------------------------------------------------------

#ifndef FmCompRepriceSummOsvH
#define FmCompRepriceSummOsvH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmCompetitorsRepriceSumm.h"
#include <Db.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
//---------------------------------------------------------------------------

class TFCompRepriceSummOsv : public TTSFDialog
{
__published:
        TMLDBGrid *m_MLDB_gr;
  TAction *m_ACT_add;
  TAction *m_ACT_delete;
        TDataSource *m_DS_tm;
        TDataSource *m_DS_main;
        void __fastcall m_ACT_apply_updatesUpdate(TObject *Sender);
private:
  TDCompetitorsRepriceSumm *m_dm;
public:
  __fastcall TFCompRepriceSummOsv(TComponent* p_owner,
                                     TTSDCustom *p_dm_custom,
                                     const AnsiString &p_caption);
  __fastcall ~TFCompRepriceSummOsv();
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
