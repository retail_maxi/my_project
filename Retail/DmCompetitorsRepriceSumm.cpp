//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmCompetitorsRepriceSumm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDCompetitorsRepriceSumm::TDCompetitorsRepriceSumm(TComponent* p_owner,
                            AnsiString p_prog_id):
                             TTSDDocumentWithLines(p_owner,p_prog_id),
                             m_cur_comp_id(-1)//,
//                              m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
    SetBeginEndDate(int(GetSysDate()) - 7,int(GetSysDate()));

}
//---------------------------------------------------------------------------


__fastcall TDCompetitorsRepriceSumm::~TDCompetitorsRepriceSumm(){

}
void __fastcall TDCompetitorsRepriceSumm::m_Q_itemAfterInsert(
      TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_itemAfterInsert(DataSet);
  m_Q_itemDOC_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemDOC_NUMBER->AsString = GetDocNumber(DocTypeId);  

  m_Q_org_group->Open();
  if (!m_Q_org_groupID->IsNull)
  {
    m_Q_itemORG_GROUP_ID->AsInteger = m_Q_org_groupID->AsInteger;
    m_Q_itemORG_GROUP_NAME->AsString = m_Q_org_groupNAME->AsString;
  }
  if (m_Q_org_group->Active) m_Q_org_group->Close();

}
//---------------------------------------------------------------------------


void __fastcall TDCompetitorsRepriceSumm::m_Q_lineAfterInsert(
      TDataSet *DataSet)
{
    TTSDDocumentWithLines::m_Q_lineAfterInsert(DataSet);
 /*   m_Q_lineNMCL_ID->AsInteger = m_Q_linesNMCL_ID->AsInteger;
    m_Q_lineNMCL_NAME->AsString = m_Q_linesNMCL_NAME->AsString;
    m_Q_lineCOMP_ID->AsInteger = CurCompId;      */
}
//---------------------------------------------------------------------------


void __fastcall TDCompetitorsRepriceSumm::m_Q_linesBeforeOpen(
      TDataSet *DataSet)
{
     TTSDDocumentWithLines::m_Q_linesBeforeOpen(DataSet);
     m_Q_lines->ParamByName("ORG_GROUP_ID")->AsInteger = m_Q_itemORG_GROUP_ID->AsInteger;
}
//---------------------------------------------------------------------------


void __fastcall TDCompetitorsRepriceSumm::SetBeginEndDate(const TDateTime &p_begin_date,
                                                 const TDateTime &p_end_date)
{
  if( (int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  if( m_Q_list->Active ) RefreshListDataSets();
}



void __fastcall TDCompetitorsRepriceSumm::m_Q_competitorsBeforeOpen(
      TDataSet *DataSet)
{
     m_Q_competitors->ParamByName("ORG_GROUP_ID")->AsInteger = m_Q_itemORG_GROUP_ID->AsInteger;
     m_Q_competitors->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDCompetitorsRepriceSumm::m_Q_itemBeforeOpen(
      TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_itemBeforeOpen(DataSet);
/*  if( (UpdateRegimeItem == urInsert) && (!ChangingItem) )
    FreeDocNumber(DocTypeId,m_Q_itemDOC_NUMBER->AsString);  */
}
//---------------------------------------------------------------------------


