//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmNoCashSales.h"
#include "XLSXConverter.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "MLLogin"
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "RxQuery"
#pragma link "TSConnectionContainer"
#pragma link "TSDMOPERATION"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDNoCashSales::TDNoCashSales(TComponent* p_owner,AnsiString p_prog_id):
                          TTSDOperation(p_owner,p_prog_id)
{
    SetBeginEndDate(int(GetSysDate()),int(GetSysDate()));
}
//---------------------------------------------------------------------------

void __fastcall TDNoCashSales::SetBeginEndDate(const TDateTime &p_begin_date,
                                           const TDateTime &p_end_date)
{
    begin_date = int(p_begin_date);
    end_date = int(p_end_date);
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDNoCashSales::GetBeginDate()
{
    return begin_date;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDNoCashSales::GetEndDate()
{
    return end_date;
}
//---------------------------------------------------------------------------

void __fastcall TDNoCashSales::m_Q_mainBeforeOpen(TDataSet *DataSet)
{
  m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = begin_date;
  m_Q_main->ParamByName("END_DATE")->AsDateTime = end_date;
}
//---------------------------------------------------------------------------
bool __fastcall TDNoCashSales::DoConvertXLSXDefault(AnsiString p_file_name)
{
  bool res = false;
  AllXLSXImport *xlsx = new AllXLSXImport(p_file_name);
  try
  {
    unsigned long int el_count = 11;
    //long j = 4;
    AnsiString chk_dt;
    xlsx->BeforeProcess(el_count);
      //title
    xlsx->BeforeStep(1);
      //cols
    xlsx->StepTitle(5.7109375, 1);
    xlsx->StepTitle(11.7109375, 2);
    xlsx->StepTitle(19.7109375, 3);
    xlsx->StepTitle(17.7109375, 4);
    xlsx->StepTitle(17.7109375, 5);
    xlsx->StepTitle(11.7109375, 6);

    xlsx->AfterStep(true);

    xlsx->BeforeStep(1);
    xlsx->StepProcess(1, 3, "����� � �������� �� ������������ ������� � ���", 0, true, 4);
    xlsx->AfterStep(false);
    if (m_Q_check_org->Active) m_Q_check_org->Close();
    m_Q_check_org->Open();
    xlsx->BeforeStep(2);
    xlsx->StepProcess(2, 4, "���: "+m_Q_check_orgNAME->AsString, 1, true, 5);
    m_Q_check_org->Close();
    xlsx->AfterStep(false);
    int p_all_sum = 0;
    int p_all_nds = 0;
    long row_chk = 3;
    long element_count = 2;
    int rec_count;
    int merge_count=0;
    int merge_row=4;
    AnsiString merge_str = "";
    //������ ����� �� �����
    m_Q_check_dates->First();
    while (!m_Q_check_dates->Eof)
    {
      int p_date_maxi_sum = 0;
      int p_date_rme_sum = 0;
      int p_date_maxi_nds = 0;
      int p_date_rme_nds = 0;
      xlsx->BeforeStep(row_chk,30);
      xlsx->StepProcess(row_chk, 4, "������� �� ������������ ������� �� "+m_Q_check_datesCHECK_DATE->AsString, element_count, true, 5);
      xlsx->AfterStep(false);

      row_chk++;
      element_count++;
      xlsx->BeforeStep(row_chk);
      xlsx->StepProcess(row_chk, 1, "� �/�", element_count, true, 7);
      element_count++;
      xlsx->StepProcess(row_chk, 2, "����� ����������� ������������", element_count, true, 7);
      element_count++;
      xlsx->StepProcess(row_chk, 3, "����� �������", element_count, true, 7);
      element_count++;
      xlsx->StepProcess(row_chk, 4, "����� �������", element_count, true, 7);
      element_count++;
      xlsx->StepProcess(row_chk, 5, "", element_count, true, 7);
      element_count++;
      xlsx->StepProcess(row_chk, 6, "����������", element_count, true, 7);
      xlsx->AfterStep(false);

      row_chk++;
      element_count++;
      xlsx->BeforeStep(row_chk);
      xlsx->StepProcess(row_chk,1,"", element_count, true,7);
      element_count++;
      xlsx->StepProcess(row_chk,2,"", element_count, true,7);
      element_count++;
      xlsx->StepProcess(row_chk,3,"", element_count, true,7);
      element_count++;
      xlsx->StepProcess(row_chk,4,"�� ��� \"�������� �����\"", element_count, true,7);
      element_count++;
      xlsx->StepProcess(row_chk,5,"�� ���", element_count, true,7);
      element_count++;
      xlsx->StepProcess(row_chk,6,"", element_count, true,7);
      xlsx->AfterStep(false);
      merge_str=merge_str+"A"+IntToStr(merge_row)+":A"+IntToStr(merge_row+1)+
                ",B"+IntToStr(merge_row)+":B"+IntToStr(merge_row+1)+
                ",C"+IntToStr(merge_row)+":C"+IntToStr(merge_row+1)+
                ",D"+IntToStr(merge_row)+":E"+IntToStr(merge_row)+
                ",F"+IntToStr(merge_row)+":F"+IntToStr(merge_row+1)+",";
        //end title
      long row = row_chk;
      //������ ����� �� ��
      if (m_Q_check_fn->Active) m_Q_check_fn->Close();
      m_Q_check_fn->ParamByName("CHECK_DATE")->AsDateTime = m_Q_check_datesCHECK_DATE->AsDateTime;
      m_Q_check_fn->Open(); m_Q_check_fn->First();
      merge_count=merge_count+7+2*(m_Q_check_fn->RecordCount);
      while (!m_Q_check_fn->Eof)
      {
        int p_fn_maxi_sum = 0;
        int p_fn_rme_sum = 0;
        int p_fn_maxi_nds = 0;
        int p_fn_rme_nds = 0;
        rec_count = 1;
        if (m_Q_data_by_date->Active) m_Q_data_by_date->Close();
        m_Q_data_by_date->ParamByName("CHECK_DATE")->AsDateTime = m_Q_check_datesCHECK_DATE->AsDateTime;
        m_Q_data_by_date->ParamByName("FISCAL_NUM")->AsInteger = m_Q_check_fnFISCAL_NUM->AsInteger;
        m_Q_data_by_date->Open(); m_Q_data_by_date->First();
        while (!m_Q_data_by_date->Eof)
        {
            //data
            //
          if (row>row_chk)
            xlsx->AfterStep(false);
          row++;
          xlsx->BeforeStep(row);
          element_count++;
          xlsx->StepProcess(row,1,rec_count, element_count, false,8);
          element_count++; rec_count++;
          xlsx->StepProcess(row,2,m_Q_data_by_dateFISCAL_NUM->AsInteger, element_count, false,8);
          element_count++;
          xlsx->StepProcess(row,3,m_Q_data_by_dateCHECK_DATE->AsString, element_count, true,8);
          element_count++;
          xlsx->StepProcess(row,4,m_Q_data_by_dateMAXI_SUM->AsInteger, element_count, false,8);
          p_date_maxi_sum=p_date_maxi_sum+m_Q_data_by_dateMAXI_SUM->AsInteger;
          p_fn_maxi_sum=p_fn_maxi_sum+m_Q_data_by_dateMAXI_SUM->AsInteger;
          element_count++;
          xlsx->StepProcess(row,5,m_Q_data_by_dateRMI_SUM->AsInteger, element_count, false,8);
          p_date_rme_sum=p_date_rme_sum+m_Q_data_by_dateRMI_SUM->AsInteger;
          p_fn_rme_sum=p_fn_rme_sum+m_Q_data_by_dateRMI_SUM->AsInteger;
          if (m_Q_data_by_dateRMI_SUM->AsInteger==0)
          {
            p_fn_maxi_nds=p_fn_maxi_nds+m_Q_data_by_dateNDS->AsInteger;
            p_date_maxi_nds=p_date_maxi_nds+m_Q_data_by_dateNDS->AsInteger;
          }
          if (m_Q_data_by_dateMAXI_SUM->AsInteger==0)
          {
            p_fn_rme_nds=p_fn_rme_nds+m_Q_data_by_dateNDS->AsInteger;
            p_date_rme_nds=p_date_rme_nds+m_Q_data_by_dateNDS->AsInteger;
          }
          element_count++;
          xlsx->StepProcess(row,6,"", element_count, false,8);
          m_Q_data_by_date->Next();
        }
        if (m_Q_data_by_date->RecordCount > 0) xlsx->AfterStep(false);
            //end data
        //����� �� ��
        row++;
        xlsx->BeforeStep(row);
        element_count++;
        xlsx->StepProcess(row,1,"����� �� ����������� ������������ � "+m_Q_check_fnFISCAL_NUM->AsString+":", element_count, true,8);
        element_count++;
        xlsx->StepProcess(row,2,"", element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,3,"", element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,4,p_fn_maxi_sum, element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,5,p_fn_rme_sum, element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,6,"", element_count, false,8);
        xlsx->AfterStep(false);
        row++;
        xlsx->BeforeStep(row);
        element_count++;
        xlsx->StepProcess(row,1,"� ��� ����� ���:", element_count, true,8);
        element_count++;
        xlsx->StepProcess(row,2,"", element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,3,"", element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,4,p_fn_maxi_nds, element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,5,p_fn_rme_nds, element_count, false,8);
        element_count++;
        xlsx->StepProcess(row,6,"", element_count, false,8);
        xlsx->AfterStep(false);
        row_chk=row;
        merge_row=merge_row+2+m_Q_data_by_date->RecordCount;
        merge_str=merge_str+"A"+IntToStr(merge_row)+":C"+IntToStr(merge_row)+
                  ",A"+IntToStr(merge_row+1)+":C"+IntToStr(merge_row+1)+",";
        //����� ������ �� ��
        m_Q_check_fn->Next();
      }
      //����� ����� �� ��.
      //���� �� ����. �����.
      row++;
      xlsx->BeforeStep(row);
      element_count++;
      xlsx->StepProcess(row,1,"����� �� "+m_Q_check_datesCHECK_DATE->AsString+":", element_count, true,8);
      element_count++;
      xlsx->StepProcess(row,2,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,3,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,4,p_date_maxi_sum, element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,5,p_date_rme_sum, element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,6,"", element_count, false,8);
      xlsx->AfterStep(false);
      p_all_sum=p_all_sum+p_date_maxi_sum+p_date_rme_sum;
      //���� �� ����. ���.
      row++;
      xlsx->BeforeStep(row);
      element_count++;
      xlsx->StepProcess(row,1,"� ��� ����� ���:", element_count, true,8);
      element_count++;
      xlsx->StepProcess(row,2,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,3,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,4,p_date_maxi_nds, element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,5,p_date_rme_nds, element_count, false,8);
      element_count++;
      xlsx->StepProcess(row,6,"", element_count, false,8);
      xlsx->AfterStep(false);
      p_all_nds=p_all_nds+p_date_maxi_nds+p_date_rme_nds;
      //����� ������ �� ����.
      merge_row=merge_row+2;
      merge_str=merge_str+"A"+IntToStr(merge_row)+":C"+IntToStr(merge_row)+
                  ",A"+IntToStr(merge_row+1)+":C"+IntToStr(merge_row+1)+",";
      merge_row=merge_row+3;
      row_chk=row+1;
      element_count++;
      m_Q_check_dates->Next();
    }
    //����� ����� �� �����
    //�������� �����.
      row_chk++;
      xlsx->BeforeStep(row_chk);
      xlsx->StepProcess(row_chk,1,"����� �� ������:", element_count, true,8);
      element_count++;
      xlsx->StepProcess(row_chk,2,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,3,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,4,p_all_sum, element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,5,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,6,"", element_count, false,8);
      xlsx->AfterStep(false);
      //�������� ���.
      row_chk++;
      xlsx->BeforeStep(row_chk);
      element_count++;
      xlsx->StepProcess(row_chk,1,"� ��� ����� ���:", element_count, true,8);
      element_count++;
      xlsx->StepProcess(row_chk,2,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,3,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,4,p_all_nds, element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,5,"", element_count, false,8);
      element_count++;
      xlsx->StepProcess(row_chk,6,"", element_count, false,8);
      xlsx->AfterStep(false);
      //����� ������.
    merge_count=merge_count+4;
    merge_str=merge_str+"A"+IntToStr(merge_row)+":C"+IntToStr(merge_row)+
                  ",D"+IntToStr(merge_row)+":E"+IntToStr(merge_row)+
                  ",A"+IntToStr(merge_row+1)+":C"+IntToStr(merge_row+1)+
                  ",D"+IntToStr(merge_row+1)+":E"+IntToStr(merge_row+1);

    AnsiString temp_str=merge_str;
    xlsx->BeforeMerge(merge_count);
    for (int i=1; i<merge_count; i++)
    {
      merge_str=temp_str.SubString(1,temp_str.Pos(",")-1);
      temp_str=temp_str.SubString(temp_str.Pos(",")+1,temp_str.Length());
      xlsx->StepMerge(merge_str);
    }
    xlsx->StepMerge(temp_str);
    xlsx->AfterMerge();
    xlsx->AfterProcess();
    m_Q_check_fn->Close();
    m_Q_data_by_date->Close();
    res = true;
     // m_dm->m_Q_data4xls->EnableControls();
  }
   __finally
  {
    if (xlsx) delete xlsx;
  }
  return res;
}
//---------------------------------------------------------------------------
void __fastcall TDNoCashSales::m_Q_check_datesBeforeOpen(TDataSet *DataSet)
{
  m_Q_check_dates->ParamByName("BEGIN_DATE")->AsDateTime = begin_date;
  m_Q_check_dates->ParamByName("END_DATE")->AsDateTime = end_date;
}
//---------------------------------------------------------------------------

