//---------------------------------------------------------------------------

#ifndef FmIncomeItemH
#define FmIncomeItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmIncome.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "FmIncomeLine.h"
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Mask.hpp>
#include "TSPanelContainer.h"
#include "RXCtrls.hpp"
#include "DLBCScaner.h"
#include <DBCtrls.hpp>
#include "FmNmclAddressList.h"
#include "TSFormControls.h"
#include <Dialogs.hpp>
#include "MLCustomContainer.h"
#include "TSReportContainer.h"
//---------------------------------------------------------------------------
class TFIncomeItem : public TTSFDocumentWithLines
{
__published:
  TDataSource *m_DS_gdp;
  TMLLov *m_LOV_gdp;
  TAction *m_ACT_get_acc_doc;
  TDataSource *m_DS_receivers;
  TMLLov *m_LOV_receivers;
  TAction *m_ACT_set_not_include_act_verification;
  TAction *m_ACT_set_include_act_verification;
  TPopupMenu *m_PM_include_act_verif;
  TMenuItem *N2;
  TMenuItem *N1;
  TRxSpeedButton *m_RSB_include_act_verif;
  TDLBCScaner *m_BCS_item;
  TAction *m_ACT_load_act_tcd;
  TAction *m_ACT_clear_act_tcd;
  TPopupMenu *m_PM_act_tcd;
  TMenuItem *m_MI_load_act_tcd;
  TMenuItem *m_MI_clear_act_tcd;
  TRxSpeedButton *m_RSB_DCT;
  TAction *m_ACT_set_corr_amt;
    TAction *m_ACT_view_source;
        TMenuItem *m_MI_add_bill;
        TAction *m_ACT_rtl_bill_link;
        TPanel *m_P_bot;
        TLabel *m_L_kind;
        TComboBox *m_CB_type;
        TLabel *Label3;
        TMLDBPanel *m_DBPL_w_corr;
        TLabel *m_L_corr_amt;
        TMLDBPanel *m_DBP_corr_amt;
        TSpeedButton *m_SB_corr_amt;
        TLabel *m_L_in_amount;
        TMLDBPanel *m_DBP_in_amount;
        TLabel *m_L_note;
        TDBEdit *m_DBE_note;
        TLabel *m_L_receiver;
        TMLLovListView *m_LLV_receiver;
        TLabel *m_L_inc_started;
        TDBDateEdit *m_DBDE_inc_started_date;
        TDateTimePicker *m_DTP_inc_started_time;
        TLabel *m_L_inc_finfished;
        TDBDateEdit *m_DBDE_inc_finished_date;
        TDateTimePicker *m_DTP_inc_finished_time;
        TPanel *m_P_schet;
        TPanel *Panel1;
        TLabel *m_L_buh_doc_note;
        TMLDBPanel *m_DBP_buh_doc_note;
        TLabel *m_L_mngr_note;
        TMLDBPanel *m_DBP_mngr_note;
        TLabel *m_L_buh_doc_cnt;
        TMLDBPanel *m_DBP_buh_doc_cnt;
        TLabel *m_L_buh_doc_number;
        TMLDBPanel *m_DBP_buh_doc_number;
        TLabel *m_L_buh_doc_date;
        TMLDBPanel *m_DBP_buh_doc_date;
        TLabel *m_L_buh_doc_id;
        TMLDBPanel *m_DBP_buh_doc_id;
        TLabel *m_L_buh_doc_amount;
        TMLDBPanel *m_DBP_buh_doc_amount;
        TLabel *m_L_contract;
        TMLDBPanel *m_DBP_contract;
        TLabel *m_L_doc_number;
        TMLDBPanel *m_DBP_doc_number;
        TLabel *m_L_issued_date;
        TMLDBPanel *m_DBP_issued_date;
        TLabel *m_L_gdp_name;
        TMLLovListView *m_LLV_gdp;
        TBitBtn *m_BBTN_get_acc_doc;
        TSpeedButton *m_SB_view_source;
        TBitBtn *m_BTN_get_schet;
        TAction *m_ACT_get_schet_doc;
        TLabel *Label1;
        TMLDBPanel *m_DBP_shet_doc_number;
        TLabel *Label2;
        TMLDBPanel *m_MDB_sch_date;
        TMLDBPanel *m_MDB_sch_from;
        TMLDBPanel *m_MDB_sch_to;
        TLabel *Label4;
        TPageControl *m_P_spec;
        TTabSheet *m_TS_spec;
        TTabSheet *m_TS_diff;
        TMLDBGrid *m_DBG_liness;
        TMLDBGrid *m_DBG_diff;
        TDataSource *m_DS_diff;
        TLabel *Label5;
   TSpeedButton *m_SBTN_view_source;
   TAction *m_ACT_income_diff;
  TMLDBPanel *m_DBP_non_stored;
  TLabel *Label6;
  TSpeedButton *m_SBTN_sign;
  TAction *m_ACT_sign;
  TDBEdit *m_DBE_sign;
  TDBEdit *m_DBE_sign_date;
        TLabel *m_L_ttn_number;
        TLabel *m_L_ttn_date;
        TMLDBPanel *m_DBP_ttn_number;
        TMLDBPanel *m_DBP_ttn_date;
        TTSReportContainer *m_TSRC_blt_rep;
        TLabel *Label7;
        TDBEdit *m_DBE_inc_hour;
        TDBEdit *m_DBE_inc_minute;
        TLabel *Label8;
        TLabel *Label9;
        TDBCheckBox *m_DBCB_with_recount;
        TLabel *m_L1;
        TMLDBPanel *MLDBPanel1;
        TPanel *m2;
        TButton *btnCreateInc;
        TButton *btnCreateEdInc;
  void __fastcall m_ACT_get_acc_docUpdate(TObject *Sender);
  void __fastcall m_ACT_get_acc_docExecute(TObject *Sender);
  void __fastcall m_DTP_inc_started_timeChange(TObject *Sender);
  void __fastcall m_DTP_inc_finished_timeChange(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_CB_typeClick(TObject *Sender);
  void __fastcall m_ACT_set_not_include_act_verificationUpdate(
          TObject *Sender);
  void __fastcall m_ACT_set_not_include_act_verificationExecute(
          TObject *Sender);
  void __fastcall m_ACT_set_include_act_verificationUpdate(
          TObject *Sender);
  void __fastcall m_ACT_set_include_act_verificationExecute(
          TObject *Sender);
  void __fastcall m_ACT_appendUpdate(TObject *Sender);
  void __fastcall m_BCS_itemBegin(TObject *Sender);
  void __fastcall m_BCS_itemEnd(TObject *Sender);
  void __fastcall m_ACT_load_act_tcdUpdate(TObject *Sender);
  void __fastcall m_ACT_load_act_tcdExecute(TObject *Sender);
  void __fastcall m_ACT_clear_act_tcdUpdate(TObject *Sender);
  void __fastcall m_ACT_clear_act_tcdExecute(TObject *Sender);
  void __fastcall m_DBG_linesEnter(TObject *Sender);
  void __fastcall m_DBG_linesExit(TObject *Sender);
  void __fastcall m_ACT_set_corr_amtUpdate(TObject *Sender);
  void __fastcall m_ACT_set_corr_amtExecute(TObject *Sender);
    void __fastcall m_ACT_view_sourceUpdate(TObject *Sender);
    void __fastcall m_ACT_view_sourceExecute(TObject *Sender);
        void __fastcall m_ACT_rtl_bill_linkExecute(TObject *Sender);
        void __fastcall m_ACT_rtl_bill_linkUpdate(TObject *Sender);
    void __fastcall m_ACT_next_fldrExecute(TObject *Sender);
        void __fastcall m_ACT_deleteUpdate(TObject *Sender);
        void __fastcall m_ACT_get_schet_docExecute(TObject *Sender);
        void __fastcall m_ACT_get_schet_docUpdate(TObject *Sender);
        void __fastcall m_DBG_diffGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
        void __fastcall m_P_specChange(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall m_ACT_income_diffExecute(TObject *Sender);
   void __fastcall m_ACT_income_diffUpdate(TObject *Sender);
  void __fastcall m_ACT_signUpdate(TObject *Sender);
  void __fastcall m_ACT_signExecute(TObject *Sender);
        void __fastcall btnCreateIncClick(TObject *Sender);
        void __fastcall btnCreateEdIncClick(TObject *Sender);
private:
  TDIncome* m_dm;
  bool m_can_choose_ver_option;
  bool m_can_load_rtl_bill ;
  bool m_can_sign ;
  TMLGridOptions m_old_lines_grid_options;
  TS_FORM_LINE_SHOW(TFIncomeLine)
  AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
  void __fastcall SetParamLines(bool p_value);
  TTSDocumentControl m_income_diff_ctrl;
public:
  __fastcall TFIncomeItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm)
    : TTSFDocumentWithLines(p_owner,p_dm),
    m_dm(static_cast<TDIncome*>(p_dm)),
    m_can_choose_ver_option(TTSOperationAccess(p_dm->DRight->GetMaskOperRight(StrToInt64(p_dm->GetConstValue("OPER_SET_STATE_INCLUDE_ACT_VERIF")),TTSOperationAccess().AccessMask)).CanRun),
    m_can_load_rtl_bill(TTSOperationAccess(p_dm->DRight->GetMaskOperRight(StrToInt64(p_dm->GetConstValue("OPER_LOAD_BILL_TO_RTL_INCOME")),TTSOperationAccess().AccessMask)).CanRun),
    m_income_diff_ctrl("TSRetail2.DocIncomeDiff"),
    m_can_sign(TTSOperationAccess(p_dm->DRight->GetMaskOperRight(StrToInt64(p_dm->GetConstValue("OPER_RTL_INCOME_SIGN")),TTSOperationAccess().AccessMask)).CanRun)
    {};
  void __fastcall SchetUpd();
};
//---------------------------------------------------------------------------
#endif
