//---------------------------------------------------------------------------

#ifndef DmBuhZreportsH
#define DmBuhZreportsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMDOCUMENT.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------

class TDBuhZreports : public TTSDDocument
{
__published:
   TFloatField *m_Q_listFISCAL_NUM;
   TFloatField *m_Q_listREPORT_NUM;
   TDateTimeField *m_Q_listREPORT_DATE;
   TDateTimeField *m_Q_listPRINT_DATE;
   TFloatField *m_Q_listREPORT_SUM;
   TFloatField *m_Q_listREPORT_10_SUM;
   TFloatField *m_Q_listREPORT_18_SUM;
   TStringField *m_Q_listTAXPAYER_NAME;
   TFloatField *m_Q_itemFISCAL_NUM;
   TFloatField *m_Q_itemREPORT_NUM;
   TDateTimeField *m_Q_itemREPORT_DATE;
   TDateTimeField *m_Q_itemPRINT_DATE;
   TFloatField *m_Q_itemREPORT_SUM;
   TFloatField *m_Q_itemREPORT_10_SUM;
   TFloatField *m_Q_itemREPORT_18_SUM;
   TStringField *m_Q_itemTAXPAYER_NAME;
   TMLQuery *m_Q_srpm;
   TFloatField *m_Q_srpmDEP_NUMBER;
   TFloatField *m_Q_srpmNMCL_ID;
   TStringField *m_Q_srpmNMCL_NAME;
   TFloatField *m_Q_srpmNDS;
   TFloatField *m_Q_srpmOUT_PRICE;
   TFloatField *m_Q_srpmQUANTITY;
   TFloatField *m_Q_listSRPM_SUM;
   TFloatField *m_Q_itemSRPM_SUM;
   TFloatField *m_Q_itemGROUND_DOC_ID;
   TFloatField *m_Q_listORG_ID;
   TStringField *m_Q_listORG_NAME;
   TFloatField *m_Q_itemORG_ID;
   TStringField *m_Q_itemORG_NAME;
    TStringField *m_Q_itemSTOCK_NAME;
        TFloatField *m_Q_listBONUS_POINT_SUM;
        TFloatField *m_Q_itemBONUS_POINT_SUM;
        TFloatField *m_Q_listREPORT_0_SUM;
        TFloatField *m_Q_itemREPORT_0_SUM;
        TMLQuery *m_Q_ret;
        TFloatField *m_Q_retDEP_NUMBER;
        TFloatField *m_Q_retNMCL_ID;
        TStringField *m_Q_retNMCL_NAME;
        TFloatField *m_Q_retNDS;
        TFloatField *m_Q_retOUT_PRICE;
        TFloatField *m_Q_retQUANTITY;
        TFloatField *m_Q_listRET_SUM;
        TFloatField *m_Q_itemRET_SUM;
        TMLQuery *m_Q_folders_next;
        TFloatField *m_Q_folders_nextSRC_FLDR_ID;
        TFloatField *m_Q_folders_nextID;
        TFloatField *m_Q_folders_nextDEST_FLDR_ID;
        TStringField *m_Q_folders_nextNAME;
        TStringField *m_Q_folders_nextROUTE_NEXT_RIGHT;
        TStringField *m_Q_itemDOC_STATUS;
   TFloatField *m_Q_listTAXPAYER_ID;
   TFloatField *m_Q_listNO_CASH_SUM;
   TFloatField *m_Q_itemNO_CASH_SUM;
   void __fastcall m_Q_srpmBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_retBeforeOpen(TDataSet *DataSet);
private:
  int m_home_org_id;
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
public:
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
public:
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
  __property int HomeOrgId = {read=m_home_org_id};
public:
  __fastcall TDBuhZreports(TComponent* p_owner, AnsiString p_prog_id);
ML_BEGIN_DATA_SETS
  ML_BASE_DATA_SETS(TTSDDocument)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_srpm)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_ret)
ML_END_DATA_SETS
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
