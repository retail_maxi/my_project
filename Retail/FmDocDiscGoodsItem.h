//---------------------------------------------------------------------------

#ifndef FmDocDiscGoodsItemH
#define FmDocDiscGoodsItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmDocDiscGoods.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "FmDocDiscGoodsLine.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include "MLLov.h"
#include "MLLovView.h"
#include <Dialogs.hpp>
#include "RXCtrls.hpp"
//---------------------------------------------------------------------------
class TFDocDiscGoodsItem : public TTSFDocumentWithLines
{
__published:	// IDE-managed Components
  TDBEdit *m_DBE_note;
  TMLDBPanel *m_DBE_doc_number;
  TMLDBPanel *MLDBPanel2;
  TLabel *Label1;
  TLabel *Label2;
        TLabel *m_L_note;
  TDBDateEdit *m_DBDE_begin;
  TDBDateEdit *m_DBDE_end;
  TLabel *Label4;
  TLabel *Label5;
  TDataSource *m_DS_nmcls;
  TMLLov *m_LOV_nmcls;
  TMLLovListView *m_LV_nmcls;
  TLabel *Label6;
  TDataSource *m_DS_acts;
  TMLLov *m_LOV_acts;
  TMLLovListView *m_LV_acts;
  TLabel *Label7;
  TDataSource *m_DS_assorts;
  TMLLov *m_LOV_assorts;
  TMLLovListView *m_LV_assorts;
  TLabel *Label8;
        TDataSource *m_DS_disc_reasons;
        TMLLov *m_LOV_disc_reasons;
        TLabel *m_L_act_price;
        TDBEdit *m_DBE_act_price;
        TDBEdit *m_DBE_out_price_nd;
        TLabel *m_L_price_bef_act;
        TPageControl *m_PC_lines;
        TTabSheet *m_TS_RTT;
        TPanel *Panel1;
        TToolBar *ToolBar1;
        TSpeedButton *m_SB_append;
        TSpeedButton *m_SB_edit;
        TSpeedButton *m_SB_delete;
        TSpeedButton *m_SB_view;
        TToolBar *m_TB_next;
        TRxSpeedButton *m_RSB_fldr_next;
        TToolBar *m_TB_prev;
        TRxSpeedButton *m_RSB_fldr_prev;
        TPopupMenu *m_PM_fldr_next;
        TToolBar *m_TB_left_bottom;
        TSpeedButton *m_SB_add_all_rtt;
        TAction *m_ACT_add_all_rtt;
   TAction *m_ACT_add_prod_sale;
   TAction *m_ACT_edit_prod_sale;
   TAction *m_ACT_del_prod_sale;
        TMLDBGrid *m_DBG_rtt;
        TLabel *m_L_disc_types;
        TMLLovListView *m_LV_disc_reasons;
        TLabel *m_L_pull_date;
        TLabel *m_L_1;
        TMLDBPanel *m_P_DOC_DATE;
        TDBDateEdit *m_DBDE_PULL_DATE;
        TPopupMenu *m_PM_fldr_prev;
        TCheckBox *m_CB_urgent_reprice;
        TSpeedButton *m_SB_view_nmcl_sales;
        TAction *m_ACT_view_nmcl_sales;
        TDBCheckBox *m_DBCB_supp_comp;
        TLabel *Label3;
        TMLDBPanel *MLDBPanel1;
        TLabel *Label9;
        TDBEdit *m_DBE_out_price;
        TLabel *m_L_out_price_nd;
        TDBEdit *m_DBE_cnt_sale;
        TDBEdit *m_DBE_cnt_price_comp;
        TLabel *m_L_cnt;
        TLabel *m_L_cnt_sale;
        TLabel *m_L_cnt_price_comp;
        TMLLovListView *m_LV_cnt;
        TDataSource *m_DS_cnt;
        TMLLov *m_LOV_cnt;
        TDBEdit *m_DBE_cnt_comp_sale;
        TDBEdit *m_DBE_cnt_method_comp;
        TLabel *m_L_cnt_comp_sale;
        TLabel *m_L_cnt_method_comp;
  void __fastcall m_ACT_editUpdate(TObject *Sender);
  void __fastcall m_LOV_nmclsAfterApply(TObject *Sender);
  void __fastcall m_LOV_nmclsAfterClear(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_LOV_actsAfterClear(TObject *Sender);
        void __fastcall m_ACT_insertUpdate(TObject *Sender);
        void __fastcall m_ACT_deleteExecute(TObject *Sender);
        void __fastcall m_ACT_add_all_rttExecute(TObject *Sender);
        void __fastcall m_ACT_add_all_rttUpdate(TObject *Sender);
        void __fastcall m_CB_urgent_repriceClick(TObject *Sender);
        void __fastcall m_PM_fldr_prevPopup(TObject *Sender);
        void __fastcall m_PM_fldr_nextPopup(TObject *Sender);
        void __fastcall m_ACT_view_nmcl_salesExecute(TObject *Sender);
        void __fastcall m_ACT_view_nmcl_salesUpdate(TObject *Sender);
        void __fastcall m_ACT_deleteUpdate(TObject *Sender);
        void __fastcall m_ACT_appendExecute(TObject *Sender);
        void __fastcall m_DBCB_supp_compClick(TObject *Sender);
        void __fastcall m_DBG_rttGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
protected:
  TS_FORM_LINE_SHOW(TFDocDiscGoodsLine)
protected:
  virtual AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
private:
  TDDocDiscGoods *m_dm;
  void __fastcall ShowFldrMenu();
  void __fastcall ShowFldrMenuPrev();
  void __fastcall PMFldrNextClick(TObject *Sender);
  void __fastcall BeforeMoveRoute(long p_item_id,
                                              long fldr_id,
                                              const AnsiString &fldr_name,
                                              const AnsiString &fldr_right,
                                              long p_route_id,
                                              const AnsiString &p_route_name,
                                              const AnsiString &p_route_right,
                                              long p_dest_fldr_id,
                                              const AnsiString &p_dest_fldr_name,
                                              const AnsiString &p_dest_fldr_right,
                                              AnsiString *p_params);
public:
     TTSOperationControl m_view_nmcl_sales;
     int __fastcall getRouteId(int p_src_fldr_id, int p_dest_fldr_id);
public:
  __fastcall TFDocDiscGoodsItem(TComponent *p_owner,
                             TTSDDocumentWithLines *p_dm_doc_with_ln): TTSFDocumentWithLines(p_owner,
                                                                                             p_dm_doc_with_ln),
                                                                       m_view_nmcl_sales("TSRtlOperations.ViewNmclSales"),
                                                                       m_dm(static_cast<TDDocDiscGoods*>(DMDocumentWithLines))
                                                                       {};
};
//---------------------------------------------------------------------------
#endif
