//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlSalesList.h"
#include "FmRtlSalesPeriod.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFRtlSalesList::TFRtlSalesList(TComponent* p_owner,
                                          TTSDDocument *p_dm_document): TTSFDocumentList(p_owner,
                                                                                         p_dm_document),
                                                                        m_dm(static_cast<TDRtlSales*>(DMDocument))
{
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                        m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);
  if (m_dm->BeginDate.DateTimeString().Length()>11)
    m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString().SubString(1, m_dm->BeginDate.DateTimeString().Length()-3);
  else
    m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString();
  if (m_dm->EndDate.DateTimeString().Length())
    m_SBTN_period->Caption =m_SBTN_period->Caption  + " �� " + m_dm->EndDate.DateTimeString().SubString(1, m_dm->EndDate.DateTimeString().Length()-3);
  else
    m_SBTN_period->Caption =m_SBTN_period->Caption + " �� " + m_dm->EndDate.DateTimeString();

  m_DBLCB_organizations->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_organizations->Enabled = false;
  else
    m_DBLCB_organizations->Enabled = true;
}
//---------------------------------------------------------------------------


void __fastcall TFRtlSalesList::m_ACT_find_kasUpdate(TObject *Sender)
{
  //m_ACT_find_kas->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesList::m_ACT_periodExecute(TObject *Sender)
{
  if(TSExecModuleDlg<TFRtlSalesPeriod>(this,m_dm))
  {
    if (m_dm->BeginDate.DateTimeString().Length()>11)
      m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString().SubString(1, m_dm->BeginDate.DateTimeString().Length()-3);
    else
      m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString();
    if (m_dm->EndDate.DateTimeString().Length())
      m_SBTN_period->Caption =m_SBTN_period->Caption  + " �� " + m_dm->EndDate.DateTimeString().SubString(1, m_dm->EndDate.DateTimeString().Length()-3);
    else
      m_SBTN_period->Caption =m_SBTN_period->Caption + " �� " + m_dm->EndDate.DateTimeString();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesList::m_CB_tabacClick(TObject *Sender)
{
  m_dm->IsTabac = m_CB_tabac->Checked;
  m_dm->RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesList::m_DBLCB_organizationsClick(TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;
}
//---------------------------------------------------------------------------

