inherited DMessageToRKU: TDMessageToRKU
  OldCreateOrder = False
  Left = 294
  Top = 240
  Height = 640
  Width = 857
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'SELECT d.id               AS id,'
      '       d.src_org_id       AS doc_src_org_id,'
      '       o.name             AS doc_src_org_name,'
      '       d.create_date      AS doc_create_date,'
      '       d.comp_name        AS doc_create_comp_name,'
      '       d.author           AS doc_create_user,'
      '       d.modify_date      AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user      AS doc_modify_user,'
      '       d.status           AS doc_status,'
      
        '       decode(d.status, '#39'F'#39','#39'��������'#39', '#39'D'#39','#39'�����'#39', '#39'W'#39','#39'�����' +
        '� �� ������'#39', '#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id, :ACCESS_MASK), 250) AS d' +
        'oc_fldr_right,'
      '       m.doc_number,'
      '       mt.name message_type,'
      '       m.host,'
      '       m.rku,'
      '       m.send_date,'
      '       m.create_user_id,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_user_id, 1' +
        '), 250)) create_user_name'
      '  FROM documents        d,'
      '       folders          f,'
      '       organizations    o,'
      '       doc_messages_rku m,'
      '       messages_types   mt'
      ' WHERE f.doc_type_id = :DOC_TYPE_ID'
      '   AND f.id = d.fldr_id'
      '   AND o.id = d.src_org_id'
      '   AND fnc_mask_folder_right(f.id, '#39'Select'#39') = 1'
      '   AND m.doc_id = d.id'
      '   AND m.type_message = mt.id(+)'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    object m_Q_listMESSAGE_TYPE: TStringField
      DisplayLabel = '��� ���������'
      FieldName = 'MESSAGE_TYPE'
      Origin = 'mt.name'
      Size = 250
    end
    object m_Q_listHOST: TStringField
      DisplayLabel = '��� ����������'
      FieldName = 'HOST'
      Origin = 'm.host'
      Size = 64
    end
    object m_Q_listRKU: TFloatField
      DisplayLabel = '���'
      FieldName = 'RKU'
      Origin = 'm.rku'
    end
    object m_Q_listSEND_DATE: TDateTimeField
      DisplayLabel = '���� �����������'
      FieldName = 'SEND_DATE'
      Origin = 'm.send_date'
    end
    object m_Q_listCREATE_USER_ID: TFloatField
      DisplayLabel = 'ID ������������ ����������'
      FieldName = 'CREATE_USER_ID'
      Origin = 'm.create_user_id'
    end
    object m_Q_listCREATE_USER_NAME: TStringField
      DisplayLabel = '��� ������������ ����������'
      FieldName = 'CREATE_USER_NAME'
      Origin = 
        'TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_user_id, 1), 250)' +
        ')'
      Size = 250
    end
    object m_Q_listDOC_NUMBER: TFloatField
      DisplayLabel = '� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'm.doc_number'
    end
  end
  inherited m_Q_item: TMLQuery
    BeforeClose = m_Q_itemBeforeClose
    SQL.Strings = (
      'SELECT m.doc_id AS ID,'
      '       m.doc_number,'
      '       m.doc_date,'
      '       m.type_message type_message_id,'
      '       mt.name message_type,'
      '       m.host,'
      '       (SELECT TRIM(RPAD(csl(rku),250))'
      
        '          FROM doc_messages_rku_items WHERE doc_id = m.doc_id) r' +
        'ku,'
      '       m.send_date,'
      '       m.create_user_id,'
      '       m.note,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_user_id, 1' +
        '), 250)) create_user_name'
      '  FROM doc_messages_rku m, messages_types mt'
      ' WHERE m.doc_id = :ID'
      '   AND m.type_message = mt.id(+)'
      
        '   AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_i' +
        'd = :FLDR_ID)')
    object m_Q_itemTYPE_MESSAGE_ID: TFloatField
      FieldName = 'TYPE_MESSAGE_ID'
    end
    object m_Q_itemMESSAGE_TYPE: TStringField
      FieldName = 'MESSAGE_TYPE'
      Size = 250
    end
    object m_Q_itemHOST: TStringField
      FieldName = 'HOST'
      Size = 64
    end
    object m_Q_itemSEND_DATE: TDateTimeField
      FieldName = 'SEND_DATE'
    end
    object m_Q_itemCREATE_USER_ID: TFloatField
      FieldName = 'CREATE_USER_ID'
    end
    object m_Q_itemCREATE_USER_NAME: TStringField
      FieldName = 'CREATE_USER_NAME'
      Origin = 
        'TRIM(RPAD(pkg_persons.Fnc_Person_Name(m.create_user_id, 1), 250)' +
        ')'
      Size = 250
    end
    object m_Q_itemDOC_NUMBER: TFloatField
      FieldName = 'DOC_NUMBER'
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemRKU: TStringField
      FieldName = 'RKU'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_messages_rku'
      'set'
      ' TYPE_MESSAGE = :TYPE_MESSAGE_ID,'
      '  HOST = :HOST,'
      '  NOTE = :NOTE'
      'where'
      '  DOC_ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into doc_messages_rku'
      '  (DOC_ID, TYPE_MESSAGE, SEND_DATE, CREATE_USER_ID, DOC_NUMBER, '
      'DOC_DATE, NOTE)'
      'values'
      '  (:ID, :TYPE_MESSAGE_ID, :SEND_DATE, :CREATE_USER_ID, '
      ':DOC_NUMBER,:DOC_DATE, :NOTE)')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      'END; ')
  end
  object m_Q_type_mess_list: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT ID, NAME FROM messages_types'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 45
    Top = 305
    object m_Q_type_mess_listID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_type_mess_listNAME: TStringField
      DisplayLabel = '��� ���������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
  end
  object m_Q_user_list: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT cnt_id,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(cnt_id, 1), 250)) c' +
        'nt_name'
      '  FROM persons_contractors')
    Macros = <>
    Left = 150
    Top = 305
    object m_Q_user_listCNT_ID: TFloatField
      FieldName = 'CNT_ID'
    end
    object m_Q_user_listCNT_NAME: TStringField
      FieldName = 'CNT_NAME'
      Size = 250
    end
  end
  object m_Q_get_cnt_id: TQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT e.person_cnt_id cnt_id'
      '  FROM employeers e'
      ' WHERE e.user_name = USER')
    Left = 235
    Top = 305
    object m_Q_get_cnt_idCNT_ID: TFloatField
      FieldName = 'CNT_ID'
      Origin = 'TSDBMAIN.EMPLOYEERS.PERSON_CNT_ID'
    end
  end
  object m_Q_rku_list: TQuery
    BeforeOpen = m_Q_rku_listBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT rku, is_read'
      '  FROM doc_messages_rku_items'
      ' WHERE doc_id = :ID')
    Left = 45
    Top = 373
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_rku_listRKU: TFloatField
      FieldName = 'RKU'
      Origin = 'TSDBMAIN.DOC_MESSAGES_RKU_ITEMS.RKU'
    end
    object m_Q_rku_listIS_READ: TStringField
      FieldName = 'IS_READ'
      Origin = 'TSDBMAIN.DOC_MESSAGES_RKU_ITEMS.IS_READ'
      FixedChar = True
      Size = 1
    end
  end
  object m_Q_items_clear: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'DELETE  FROM doc_messages_rku_items'
      ' WHERE doc_id = :ID')
    Left = 140
    Top = 370
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  object m_Q_items_add: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'INSERT INTO doc_messages_rku_items '
      '(DOC_ID, RKU) '
      'VALUES '
      '(:ID, :RKU)')
    Left = 235
    Top = 370
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'RKU'
        ParamType = ptInput
      end>
  end
  object m_Q_not_read: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_not_readBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT cnt_id,'
      
        '       TRIM(RPAD(pkg_persons.Fnc_Person_Name(cnt_id, 1), 250)) c' +
        'nt_name,       '
      '       rku,'
      '       is_read,'
      '       read_date'
      '  FROM doc_messages_rku_items'
      ' WHERE doc_id = :ID'
      ' AND '
      ' ('
      '   (:IS_READ = 0)'
      '   OR (:IS_READ = 1 AND is_read = '#39'Y'#39')'
      '   OR (:IS_READ = 2 AND is_read = '#39'N'#39')'
      ' )'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 350
    Top = 375
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IS_READ'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IS_READ'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IS_READ'
        ParamType = ptInput
      end>
    object m_Q_not_readCNT_ID: TFloatField
      DisplayLabel = 'ID ����������'
      FieldName = 'CNT_ID'
      Origin = 'CNT_ID'
    end
    object m_Q_not_readCNT_NAME: TStringField
      DisplayLabel = '��� ����������'
      FieldName = 'CNT_NAME'
      Origin = 'TRIM(RPAD(pkg_persons.Fnc_Person_Name(cnt_id, 1), 250))'
      Size = 250
    end
    object m_Q_not_readRKU: TFloatField
      DisplayLabel = '���'
      FieldName = 'RKU'
      Origin = 'RKU'
    end
    object m_Q_not_readIS_READ: TStringField
      DisplayLabel = '���������'
      FieldName = 'IS_READ'
      FixedChar = True
      Size = 1
    end
    object m_Q_not_readREAD_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'READ_DATE'
      Origin = 'READ_DATE'
    end
  end
  object m_Q_resend: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'declare'
      'v_new_doc_id integer;'
      'v_route_id integer;'
      'begin'
      
        '    prc_insert_doc(v_new_doc_id, '#39'F'#39', to_number(fnc_get_constant' +
        '('#39'DT_MESSAGES_TO_RKU'#39')));'
      '    prc_chg_status(v_new_doc_id, '#39'W'#39');'
      '    '
      
        '    insert into doc_messages_rku(DOC_ID,TYPE_MESSAGE,HOST,SEND_D' +
        'ATE,CREATE_USER_ID,DOC_NUMBER,DOC_DATE,NOTE)'
      
        '    select v_new_doc_id,TYPE_MESSAGE,UPPER(SYS_CONTEXT('#39'USERENV'#39 +
        ', '#39'HOST'#39')),SYSDATE,CREATE_USER_ID,%DOC_NUMBER,sysdate,NOTE from ' +
        'doc_messages_rku '
      '    where doc_id = %doc_id;'
      '    '
      '    commit;'
      '    '
      '    insert into doc_messages_rku_items(doc_id, rku, is_read)'
      '    select v_new_doc_id, rku, '#39'N'#39' from doc_messages_rku_items'
      '    where doc_id = %doc_id;'
      '    '
      '    commit;'
      ''
      '    select'
      '        max(r.id) keep (dense_rank first order by r.sort_id)'
      '    into'
      '        v_route_id'
      '    from'
      '        folders f,'
      '        fldr_routes r'
      '    where'
      
        '        f.id = to_number(fnc_get_constant('#39'FLDR_MESSAGES_TO_RKU_' +
        'NEW'#39'))'
      '        and r.src_fldr_id = f.id;'
      ''
      '    prc_chg_fldr(v_new_doc_id,v_route_id);'
      ''
      '    prc_chg_status(v_new_doc_id, '#39'F'#39');'
      '    commit;'
      'end;')
    Macros = <
      item
        DataType = ftString
        Name = 'DOC_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'doc_id'
        ParamType = ptInput
      end>
    Left = 384
    Top = 312
  end
  object m_Q_resend_doc: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'declare'
      'pragma autonomous_transaction;'
      'v_doc_id integer;'
      'begin'
      
        '    prc_insert_doc(v_doc_id , '#39'F'#39', to_number(fnc_get_constant('#39'D' +
        'T_MESSAGES_TO_RKU'#39')));'
      '    prc_chg_status(v_doc_id , '#39'W'#39');'
      ''
      
        '     insert into doc_messages_rku(DOC_ID,HOST,SEND_DATE,DOC_DATE' +
        ', DOC_NUMBER)'
      
        '    values (v_doc_id,UPPER(SYS_CONTEXT('#39'USERENV'#39', '#39'HOST'#39')),SYSDA' +
        'TE,sysdate, :DOC_NUMBER);'
      '    prc_chg_status(v_doc_id , '#39'F'#39');'
      '    commit;'
      'end;')
    Left = 499
    Top = 313
    ParamData = <
      item
        DataType = ftString
        Name = 'DOC_NUMBER'
        ParamType = ptInput
      end>
  end
end
