//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvSheetLine.h"
#include "FmInvSheetAlco.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmDocumentLine"
#pragma link "MLDBPanel"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "FrmRtlIncomeLine"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma link "RXCtrls"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFInvSheetLine::TFInvSheetLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm)
                                         : TTSFDocumentLine(p_owner,p_dm),
                                         m_dm(static_cast<TDInvSheet*>(p_dm))
{
  m_FRM_rtl_line->DbHandle = m_dm->m_DB_main->Handle;
}
//---------------------------------------------------------------------------

__fastcall TFInvSheetLine::~TFInvSheetLine()
{
  m_FRM_rtl_line->DbHandle = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::FormShow(TObject *Sender)
{
  TTSFDocumentLine::FormShow(Sender);
  m_BCS_line->Active = (m_dm->UpdateRegimeItem != urView);
  
  m_TP_start_date->DateTime = m_dm->m_Q_lineSTART_DATE->AsDateTime - int(m_dm->m_Q_lineSTART_DATE->AsDateTime);
  //m_TP_start_date->Enabled = m_dm->UpdateRegimeLine != urView;
  m_TP_start_date->Enabled = false;

  m_DE_start_date->Enabled = false;

  m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();

  if (!m_dm->m_Q_lineNMCL_ID->IsNull) ActiveControl = m_DBE_items;

  m_FRM_rtl_line->OrgId = m_dm->HomeOrgId;
  m_FRM_rtl_line->NmclId = m_dm->m_Q_lineNMCL_ID->AsInteger;
  m_FRM_rtl_line->AssortId = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
  m_FRM_rtl_line->ReadOnly = m_dm->UpdateRegimeLine == urView;
  m_FRM_rtl_line->OpenDataSet();
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_TP_start_dateChange(TObject *Sender)
{
  /*
  if (m_dm->UpdateRegimeLine != urView)
  {
    if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
    m_dm->m_Q_lineSTART_DATE->AsDateTime = int(m_dm->m_Q_lineSTART_DATE->AsDateTime) +
                                           m_TP_start_date->DateTime - int(m_TP_start_date->DateTime);
  }
  */
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_clear_start_dateUpdate(TObject *Sender)
{
  //m_ACT_clear_start_date->Enabled = m_dm->UpdateRegimeLine != urView;
  m_ACT_clear_start_date->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_clear_start_dateExecute(TObject *Sender)
{
  /*
  if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineSTART_DATE->Clear();

  m_TP_start_date->DateTime = m_dm->m_Q_lineSTART_DATE->AsDateTime - int(m_dm->m_Q_lineSTART_DATE->AsDateTime);
  m_TP_start_date->Enabled = m_dm->UpdateRegimeLine != urView;
  */
}
//---------------------------------------------------------------------------

AnsiString __fastcall TFInvSheetLine::BeforeLineApply(TWinControl *p_focused)
{
  if (m_dm->m_Q_lineITEMS_QNT->IsNull)
  {
    m_DBE_items->SetFocus();
    return "������� ����������";
  }
  if (m_dm->m_Q_lineITEMS_QNT->AsFloat < 0.001 || m_dm->m_Q_lineITEMS_QNT->AsFloat > m_dm->MaxQnt)
  {
    m_DBE_items->SetFocus();
    return "�������� ����������";
  }

  return m_FRM_rtl_line->BeforeApply(p_focused);
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_apply_updatesUpdate(TObject *Sender)
{
  /*
  TTSFDocumentLine::m_ACT_apply_updatesUpdate(Sender);

  m_ACT_apply_updates->Enabled = m_ACT_apply_updates->Enabled || m_FRM_rtl_line->NeedUpdates();
  */

  m_ACT_apply_updates->Enabled = m_dm->NeedUpdatesLine() || m_FRM_rtl_line->NeedUpdates();
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_apply_updatesExecute(TObject *Sender)
{
 if (m_dm->m_Q_line->State != dsEdit) m_dm->m_Q_line->Edit();
 m_dm->m_Q_lineNMCL_ID->AsInteger = m_FRM_rtl_line->NmclId;
 m_dm->m_Q_lineASSORTMENT_ID->AsInteger = m_FRM_rtl_line->AssortId;
 m_dm->m_Q_line->Post();

 TTSFDocumentLine::m_ACT_apply_updatesExecute(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TFInvSheetLine::m_ACT_insExecute(TObject *Sender)
{
  if (m_dm->m_Q_line->State != dsEdit) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineNMCL_ID->AsInteger = m_FRM_rtl_line->NmclId;
  m_dm->m_Q_lineASSORTMENT_ID->AsInteger = m_FRM_rtl_line->AssortId;
  m_dm->m_Q_line->Post();
  /*TTSFDocumentLine::m_ACT_apply_updatesExecute(Sender);
  int a = m_dm->m_Q_lineNMCL_ID->AsInteger;
  int b = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;   */

  if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine(); 
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Insert();
  if(m_dm->m_Q_line->Active){
        m_dm->m_Q_line->Close();}
  m_dm->m_Q_line->Open();
  TSExecModuleDlg<TFInvSheetAlco>(this,m_dm);
      TFInvSheetLine::Show();
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_insUpdate(TObject *Sender)
{
  m_ACT_ins->Enabled = m_dm->UpdateRegimeLine != urView
                             &&m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_editExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Edit();
  if(m_dm->m_Q_line->Active){
        m_dm->m_Q_line->Close();}
  m_dm->m_Q_line->Open();
  TSExecModuleDlg<TFInvSheetAlco>(this,m_dm);
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_editUpdate(TObject *Sender)
{
  m_ACT_edit->Enabled = m_dm->UpdateRegimeLine != urView
                        && !m_dm->m_Q_alcSUB_LINE_ID->IsNull
                        && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_delExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Delete();
  m_dm->m_Q_alco->ApplyUpdates();
  if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();       
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_ACT_delUpdate(TObject *Sender)
{
  m_ACT_del->Enabled = m_dm->UpdateRegimeLine != urView
                       && !m_dm->m_Q_alcSUB_LINE_ID->IsNull
                       && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_BCS_lineBegin(TObject *Sender)
{
  m_temp_grid_options = m_DBG_alco->OptionsML;
  m_DBG_alco->OptionsML = TMLGridOptions() << goWithoutFind;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::m_BCS_lineEnd(TObject *Sender)
{
  try
  {
    String s = m_BCS_line->GetResult();

     if  (s.SubString(4,1)  == "-"){
     try
      {
         String kod ="";
         String seria ="";
         String number ="";
         if   (s.SubString(1,1)  == "1")
         {
           kod = s.SubString(1,3);
           seria = s.SubString(5,3);
           number = s.SubString(8,9);
          // ShowMessage("1_"+kod+"_"+seria+"_"+number);
         }
         else
         {
           kod = s.SubString(1,3);
           seria = s.SubString(5,3);
           number = s.SubString(8,8);
          // ShowMessage("2 "+kod+"  "+seria+"  "+number);
         }
         m_dm->m_Q_get_kod_fsm->ParamByName("VALUE")->AsString = kod;
          if (m_dm->m_Q_get_kod_fsm->Active) m_dm->m_Q_get_kod_fsm->Close();
            m_dm->m_Q_get_kod_fsm->Open();

         m_dm->m_kod_snnum_chek->ParamByName("TYPE_ID")->AsInteger = m_dm->m_Q_get_kod_fsmTYPE_ID->AsInteger;
         m_dm->m_kod_snnum_chek->ParamByName("RANK")->AsString =  seria;
         m_dm->m_kod_snnum_chek->ParamByName("LINE_NUMBER")->AsString = number;
         
          if (m_dm->m_kod_snnum_chek->Active) m_dm->m_kod_snnum_chek->Close();
            m_dm->m_kod_snnum_chek->Open();

         if (m_dm->m_kod_snnum_chekPDF_BARCODE->IsNull ) {
            throw ETSError("��������� �������� ����� �� ���� ������������� � �����. ������ ������� ����� ������ �� �-��������������");
           } else{
                    m_dm->m_Q_get_alccode->ParamByName("PDF_BAR_CODE")->AsString = m_dm->m_kod_snnum_chekPDF_BARCODE->AsString;
                    if (m_dm->m_Q_get_alccode->Active) m_dm->m_Q_get_alccode->Close();
                    m_dm->m_Q_get_alccode->Open();

                    while (!m_dm->m_Q_alc->Eof)
                    {
                      if (m_dm->m_Q_alcPDF_BAR_CODE->AsString ==  m_dm->m_kod_snnum_chekPDF_BARCODE->AsString)
                        throw ETSError("�������� ����� ��� ���������!");
                      m_dm->m_Q_alc->Next();
                    }

                   m_dm->m_alco_nmcl_chek->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
                   m_dm->m_alco_nmcl_chek->ParamByName("ASSORTMENT_ID")->AsInteger =  m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
                   m_dm->m_alco_nmcl_chek->ParamByName("PREF_ALCCODE")->AsString =  m_dm->m_Q_get_alccodeALCCODE->AsString;
                    if (m_dm->m_alco_nmcl_chek->Active) m_dm->m_alco_nmcl_chek->Close();
                    m_dm->m_alco_nmcl_chek->Open();

                   if (m_dm->m_alco_nmcl_chekALCO->IsNull)
                    {
                      throw ETSError("�������� ����� �� ��������� � ������������!");
                    }

                   m_dm->m_Q_ins_alco->ParamByName("ID")->AsInteger =  m_dm->m_Q_lineID->AsInteger;
                   m_dm->m_Q_ins_alco->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_lineLINE_ID->AsInteger;
                   m_dm->m_Q_ins_alco->ParamByName("PDF_BAR_CODE")->AsString =  m_dm->m_kod_snnum_chekPDF_BARCODE->AsString;
                   m_dm->m_Q_ins_alco->ParamByName("ALCCODE")->AsString =  m_dm->m_Q_get_alccodeALCCODE->AsString;
                   m_dm->m_Q_ins_alco->ParamByName("QNT")->AsInteger =  1;
                   m_dm->m_Q_ins_alco->ExecSQL();

                   if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
                   m_dm->m_Q_alc->Open();
             }
      }
      __finally
      {
        m_DBG_alco->OptionsML = m_temp_grid_options;
        m_dm->m_Q_get_kod_fsm->Close();
      }
   }
   else {
      while (!m_dm->m_Q_alc->Eof)
      {
        if (m_dm->m_Q_alcPDF_BAR_CODE->AsString == s)
          throw ETSError("������ �������� ����� ��� ���������!");
        m_dm->m_Q_alc->Next();
      }
      if (m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();
      m_dm->m_Q_pdf_bar_code->Close();
      m_dm->m_Q_pdf_bar_code->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_lineID->AsInteger;
      m_dm->m_Q_pdf_bar_code->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_lineLINE_ID->AsInteger;
      m_dm->m_Q_pdf_bar_code->ParamByName("PDF_BAR_CODE")->AsString = m_BCS_line->GetResult();
      m_dm->m_Q_pdf_bar_code->ExecSQL();

     }

    if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
    m_dm->m_Q_alc->Open();
  }
  __finally
  {
    m_DBG_alco->OptionsML = m_temp_grid_options;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetLine::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  m_BCS_line->Active = false;
}
//---------------------------------------------------------------------------

