inherited FProdTranslateRKUList: TFProdTranslateRKUList
  Left = 314
  Top = 182
  Width = 715
  Caption = 'FProdTranslateRKUList'
  PixelsPerInch = 96
  TextHeight = 13
  object m_SPL_hist: TSplitter [0]
    Left = 0
    Top = 269
    Width = 707
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    Visible = False
  end
  inherited m_P_main_control: TPanel
    Width = 707
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 610
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 222
    end
    object m_DCB_hist: TCheckBox
      Left = 8
      Top = 8
      Width = 137
      Height = 17
      Action = m_ACT_show_hist
      TabOrder = 2
    end
  end
  inherited m_SB_main: TStatusBar
    Width = 707
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 707
    Height = 243
    ParentFont = False
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 150
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ASSORT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ASSORT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TO_GR_DEP_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TO_GR_DEP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 150
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TO_NMCL_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TO_NMCL_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TO_ASSORT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TO_ASSORT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'COEF'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 707
    inherited m_P_tb_main: TPanel
      Width = 660
      inherited m_TB_main: TToolBar
        Width = 660
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 660
    end
  end
  object m_GB_hist: TGroupBox [5]
    Left = 0
    Top = 272
    Width = 707
    Height = 176
    Align = alBottom
    Caption = '�������'
    TabOrder = 4
    Visible = False
    object m_DBG_hist: TMLDBGrid
      Left = 2
      Top = 15
      Width = 703
      Height = 159
      Align = alClient
      DataSource = m_DS_hist
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      FooterColor = clWindow
      AutoFitColWidths = True
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
      KeyColumnFind = -1
      ValueColumnFind = -1
      Columns = <
        item
          FieldName = 'ID'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'RTL_PROD_RKU_ID'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'ORG_ID'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'ORG_NAME'
          Title.TitleButton = True
          Width = 150
          Footers = <>
        end
        item
          FieldName = 'NMCL_ID'
          Title.TitleButton = True
          Footers = <>
        end
        item
          FieldName = 'NMCL_NAME'
          Title.TitleButton = True
          Width = 200
          Footers = <>
        end
        item
          FieldName = 'ASSORT_ID'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'ASSORT_NAME'
          Title.TitleButton = True
          Width = 100
          Footers = <>
        end
        item
          FieldName = 'TO_GR_DEP_ID'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'TO_GR_DEP_NAME'
          Title.TitleButton = True
          Width = 150
          Footers = <>
        end
        item
          FieldName = 'TO_NMCL_ID'
          Title.TitleButton = True
          Footers = <>
        end
        item
          FieldName = 'TO_NMCL_NAME'
          Title.TitleButton = True
          Width = 200
          Footers = <>
        end
        item
          FieldName = 'TO_ASSORT_ID'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'TO_ASSORT_NAME'
          Title.TitleButton = True
          Width = 100
          Footers = <>
        end
        item
          FieldName = 'COEF'
          Title.TitleButton = True
          Footers = <>
        end
        item
          FieldName = 'OPER_TYPE'
          Title.TitleButton = True
          Visible = False
          Footers = <>
        end
        item
          FieldName = 'OPER_TYPE_NAME'
          Title.TitleButton = True
          Width = 70
          Footers = <>
        end
        item
          FieldName = 'OPER_DATE'
          Title.TitleButton = True
          Footers = <>
        end
        item
          FieldName = 'USER_NAME'
          Title.TitleButton = True
          Width = 100
          Footers = <>
        end
        item
          FieldName = 'COMP_NAME'
          Title.TitleButton = True
          Width = 100
          Footers = <>
        end>
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_show_hist: TAction
      Category = 'List'
      Caption = '�������'
      OnExecute = m_ACT_show_histExecute
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DProdTranslateRKU.m_Q_list
  end
  object m_DS_hist: TDataSource
    DataSet = DProdTranslateRKU.m_Q_hist
    Left = 32
    Top = 304
  end
end
