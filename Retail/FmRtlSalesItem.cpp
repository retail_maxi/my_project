//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlSalesItem.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFRtlSalesItem::TFRtlSalesItem(TComponent *p_owner,
                                          TTSDDocument *p_dm_document): TTSFDocument(p_owner,
                                                                                     p_dm_document),
                                                                        m_dm(static_cast<TDRtlSales*>(DMDocument)),
                                                                        m_check("SHOP_CHECK")
{
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesItem::m_ACT_print_checkExecute(TObject *Sender)
{
  TMLParams v_par;
  v_par.InitParam("REPORT_DOC_ID", m_dm->m_Q_item_checksREPORT_DOC_ID->AsInteger);
  v_par.InitParam("CHECK_NUM", m_dm->FloatField1->AsInteger);
  m_check.Execute(true, v_par);
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesItem::m_ACT_print_checkUpdate(TObject *Sender)
{
  m_ACT_print_check->Enabled = true;
}
//---------------------------------------------------------------------------

