//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRTLAssortExcepNmcls.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void RTLAssortExcepNmclsDlg(TComponent* p_owner, int p_id, TTSDCustom* p_dm, bool p_edit)
{
  TFRTLAssortExcepNmcls *form = new TFRTLAssortExcepNmcls(p_owner, p_dm,  p_edit);
  try
  {
    form->AfterCreate();

    try
    {
      
      form->m_Q_nmcl->ParamByName("ID")->AsInteger = p_id;
      form->m_Q_nmcl->Open();
      form->m_Q_all_nmcl->ParamByName("ID")->AsInteger = p_id;
      form->m_Q_all_nmcl->Open();
      form->ShowModal() == IDOK;
    }
    __finally
    {
      form->m_Q_all_nmcl->Close();
      form->m_Q_nmcl->Close();
    }
  }
  __finally
  {
    form->BeforeDestroy();
    delete form;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepNmcls::m_ACT_addExecute(TObject *Sender)
{

    if( Application->MessageBox(AnsiString("��������� ������?").c_str(),
                                Application->Title.c_str(),
                                MB_ICONQUESTION | MB_YESNO) == IDYES )
    { 
      m_Q_add_nmcl->ParamByName("ID")->AsInteger = m_Q_nmcl->ParamByName("ID")->AsInteger;
      m_Q_add_nmcl->ParamByName("SQ_ID")->AsInteger = DMCustom->FillTmpReportParamValues(m_DBG_all_nmcl, "ID");
      m_Q_add_nmcl->ExecSQL();

      m_Q_all_nmcl->Close();
      m_Q_nmcl->Close();
      m_Q_nmcl->Open();
      m_Q_all_nmcl->Open();
    }


}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepNmcls::m_ACT_addUpdate(TObject *Sender)
{

    m_ACT_add->Enabled = m_Q_all_nmclID->AsInteger > 0 && is_edit ;

}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepNmcls::m_ACT_deleteExecute(TObject *Sender)
{
  if( Application->MessageBox(AnsiString("�������� ������?").c_str(),
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES )
  {

    m_Q_delete_nmcl->ParamByName("ID")->AsInteger = m_Q_nmcl->ParamByName("ID")->AsInteger;
    m_Q_delete_nmcl->ParamByName("SQ_ID")->AsInteger = DMCustom->FillTmpReportParamValues(m_DBG_nmcl, "NMCL_ID");
    m_Q_delete_nmcl->ExecSQL();

    m_Q_all_nmcl->Close();
    m_Q_nmcl->Close();
    m_Q_nmcl->Open();
    m_Q_all_nmcl->Open();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFRTLAssortExcepNmcls::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = m_Q_nmclNMCL_ID->AsInteger > 0 && is_edit ;
}
//---------------------------------------------------------------------------

