//---------------------------------------------------------------------------

#ifndef DmInvActH
#define DmInvActH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>

//---------------------------------------------------------------------------
enum TTypeNmclLoad {tnlGoods=0, tnlInvs=1};
//---------------------------------------------------------------------------
class TDInvAct : public TTSDDocumentWithLines
{
__published:
  TStringField *m_Q_listDOC_NUMBER;
  TDateTimeField *m_Q_listDOC_DATE;
  TDateTimeField *m_Q_listDEF_START_DATE;
  TStringField *m_Q_listNOTE;
  TStringField *m_Q_itemDOC_NUMBER;
  TDateTimeField *m_Q_itemDOC_DATE;
  TDateTimeField *m_Q_itemDEF_START_DATE;
  TStringField *m_Q_itemNOTE;
  TFloatField *m_Q_itemDIFF_AMT_OUT;
  TDateTimeField *m_Q_linesSTART_DATE;
  TFloatField *m_Q_linesNMCL_ID;
  TStringField *m_Q_linesNMCL_NAME;
  TStringField *m_Q_linesMU_NAME;
  TFloatField *m_Q_linesIN_PRICE;
  TFloatField *m_Q_linesOUT_PRICE;
  TFloatField *m_Q_linesSTART_ITEMS;
  TFloatField *m_Q_linesFACT_ITEMS;
  TFloatField *m_Q_linesDIFF_ITEMS;
  TFloatField *m_Q_linesDIFF_AMT_IN;
  TFloatField *m_Q_linesDIFF_AMT_OUT;
  TFloatField *m_Q_linesCARD_ID;
  TStringField *m_Q_linesNOTE;
  TDateTimeField *m_Q_lineSTART_DATE;
  TFloatField *m_Q_lineNMCL_ID;
  TStringField *m_Q_lineNMCL_NAME;
  TStringField *m_Q_lineMU_NAME;
  TFloatField *m_Q_lineIN_PRICE;
  TFloatField *m_Q_lineOUT_PRICE;
  TFloatField *m_Q_lineSTART_ITEMS;
  TFloatField *m_Q_lineFACT_ITEMS;
  TStringField *m_Q_lineNOTE;
  TFloatField *m_Q_lineCARD_ID;
  TMLQuery *m_Q_sheet_lines;
  TDateTimeField *m_Q_sheet_linesSTART_DATE;
  TFloatField *m_Q_sheet_linesITEMS_QNT;
  TStringField *m_Q_sheet_linesNOTE;
  TQuery *m_Q_unbind_lines;
  TFloatField *m_Q_sheet_linesDOC_ID;
  TFloatField *m_Q_sheet_linesLINE_ID;
  TQuery *m_Q_freeze;
  TQuery *m_Q_class_groups;
  TFloatField *m_Q_class_groupsCLASS_ID;
  TStringField *m_Q_class_groupsNAME;
  TStoredProc *m_SP_create_act_for_group;
  TMLQuery *m_Q_gr_deps;
  TFloatField *m_Q_gr_depsID;
  TStringField *m_Q_gr_depsNAME;
  TFloatField *m_Q_itemGR_DEP_ID;
  TStringField *m_Q_itemGR_DEP_NAME;
  TStringField *m_Q_listGR_DEP_NAME;
  TQuery *m_Q_load_nmcls;
  TMLQuery *m_Q_nmcls;
  TFloatField *m_Q_nmclsNMCL_ID;
  TStringField *m_Q_nmclsNMCL_NAME;
  TStringField *m_Q_nmclsMEAS_NAME;
  TStringField *m_Q_nmclsOK;
  TStringField *m_Q_nmclsSAVE_STR;
  TUpdateSQL *m_U_nmcls;
  TFloatField *m_Q_nmclsASSORTMENT_ID;
  TStringField *m_Q_nmclsASSORTMENT_NAME;
  TQuery *m_Q_load_sheets;
  TQuery *m_Q_zero_fact;
  TStringField *m_Q_lineASSORTMENT_NAME;
  TStringField *m_Q_linesASSORTMENT_NAME;
  TStringField *m_Q_sheet_linesDOC_NUMBER;
  TDateTimeField *m_Q_sheet_linesDOC_DATE;
  TMLQuery *m_Q_nmcls_i;
  TFloatField *m_Q_nmcls_iPT_ID;
  TStringField *m_Q_nmcls_iPT_NAME;
  TFloatField *m_Q_nmcls_iNMCL_ID;
  TStringField *m_Q_nmcls_iNMCL_NAME;
  TFloatField *m_Q_nmcls_iASSORTMENT_ID;
  TStringField *m_Q_nmcls_iMEAS_NAME;
  TStringField *m_Q_nmcls_iSAVE_STR;
  TStringField *m_Q_nmcls_iOK;
  TUpdateSQL *m_U_nmcls_i;
  TStringField *m_Q_nmcls_iASSORTMENT_NAME;
        TFloatField *m_Q_listSUMIN;
        TFloatField *m_Q_listSUMOUT;
        TFloatField *m_Q_linesRECALC_ITEMS;
        TFloatField *m_Q_linesIS_RECALC;
        TFloatField *m_Q_lineRECALC_ITEMS;
        TFloatField *m_Q_lineIS_RECALC;
        TQuery *m_Q_mark_recalc;
        TMLUpdateSQL *m_U_fict_lines;
        TQuery *m_Q_folders_next;
        TFloatField *m_Q_folders_nextSRC_FLDR_ID;
        TFloatField *m_Q_folders_nextID;
        TFloatField *m_Q_folders_nextDEST_FLDR_ID;
        TStringField *m_Q_folders_nextNAME;
        TStringField *m_Q_folders_nextROUTE_NEXT_RIGHT;
        TStringField *m_Q_folders_nextROUTE_ACCESS_MASK;
        TStringField *m_Q_itemDOC_STATUS;
        TFloatField *m_Q_itemRECALC_PERCENT;
        TMLQuery *m_Q_cnt_list;
        TFloatField *m_Q_cnt_listCNT_ID;
        TStringField *m_Q_cnt_listCNT_NAME;
        TStringField *m_Q_cnt_listPOSITION;
        TFloatField *m_Q_folders_nextFLDR_COUNT;
        TQuery *m_Q_recalc;
        TQuery *m_Q_check_order;
        TFloatField *m_Q_check_orderDOC_ID;
        TMLQuery *m_Q_edit_inv_item;
        TDateTimeField *m_Q_check_orderNEXT_DATE;
        TStringField *m_Q_itemNOTE_NON_PREPARE;
        TStringField *m_Q_listNOTE_NON_PREPARE;
        TStringField *m_Q_linesNON_PREPARE;
        TStringField *m_Q_lineNON_PREPARE;
        TMLQuery *m_Q_edit_inv_prepare;
  TFloatField *m_Q_linesFACT_FROM;
  TFloatField *m_Q_linesFACT_TO;
  TFloatField *m_Q_lineFACT_FROM;
  TFloatField *m_Q_lineFACT_TO;
  TFloatField *m_Q_linesTRANS_RKU_COEF;
  TFloatField *m_Q_lineTRANS_RKU_COEF;
  TFloatField *m_Q_linesPRODPP;
        TMLQuery *m_Q_upd_prepare;
        TMLQuery *m_Q_fact_hst;
        TFloatField *m_Q_fact_hstNMCL_ID;
        TDateTimeField *m_Q_fact_hstMODIFY_DATE;
        TStringField *m_Q_fact_hstMODIFY_USER;
        TFloatField *m_Q_fact_hstFACT_ITEMS_OLD;
        TFloatField *m_Q_fact_hstFACT_ITEMS_NEW;
        TFloatField *m_Q_fact_hstPT_ID;
        TStringField *m_Q_fact_hstPT_NAME;
        TStringField *m_Q_fact_hstNMCL_NAME;
        TStringField *m_Q_fact_hstMODIFY_FIO;
        TFloatField *m_Q_listMAX_ERR_A;
        TFloatField *m_Q_listMAX_ERR_B;
        TFloatField *m_Q_itemMAX_ERR_A;
        TFloatField *m_Q_itemMAX_ERR_B;
        TStringField *m_Q_nmclsGNAME;
        TStringField *m_Q_nmclsCTNAME;
        TStringField *m_Q_linesGNAME;
        TStringField *m_Q_linesCTNAME;
        TStringField *m_Q_linesSCTNAME;
        TFloatField *m_Q_linesGROUP_ID;
        TFloatField *m_Q_nmclsGROUP_ID;
        TFloatField *m_Q_nmclsCAT_ID;
        TFloatField *m_Q_linesCAT_ID;
        TFloatField *m_Q_linesSUBCAT_ID;
        TMLQuery *m_Q_ch_cur_rest;
        TFloatField *m_Q_ch_cur_restNMCL_ID;
        TMemoField *m_Q_itemNOTE_AUDITOR;
        TStringField *m_Q_listNOTE_AUDITOR;
        TQuery *m_Q_orgs;
        TFloatField *m_Q_orgsID;
        TStringField *m_Q_orgsNAME;
        TMLQuery *m_Q_alco;
        TFloatField *m_Q_alcoSUB_LINE_ID;
        TStringField *m_Q_alcoPDF_BAR_CODE;
        TStringField *m_Q_alcoALCCODE;
        TMLQuery *m_Q_alccode;
        TStringField *m_Q_alccodePREF_ALCCODE;
        TStringField *m_Q_alccodeNAME;
        TMLUpdateSQL *m_U_alco;
        TFloatField *m_Q_alcoID;
        TFloatField *m_Q_alcoLINE_ID;
        TStringField *m_Q_alcoFULLNAME;
        TFloatField *m_Q_lineASSORTMENT_ID;
        TMLQuery *m_Q_alc;
        TFloatField *m_Q_alcID;
        TFloatField *m_Q_alcLINE_ID;
        TFloatField *m_Q_alcSUB_LINE_ID;
        TStringField *m_Q_alcPDF_BAR_CODE;
        TStringField *m_Q_alcALCCODE;
        TStringField *m_Q_alcFULLNAME;
        TMLQuery *m_Q_pdf_bar_code;
        TFloatField *m_Q_alcSTART_ITEMS;
        TFloatField *m_Q_alcFACT_ITEMS;
        TFloatField *m_Q_alcRECALC_ITEMS;
        TFloatField *m_Q_alcoSTART_ITEMS;
        TFloatField *m_Q_alcoFACT_ITEMS;
        TFloatField *m_Q_alcoRECALC_ITEMS;
        TFloatField *m_Q_itemFLDR_ID;
        TMLQuery *m_Q_del_items;
        TFloatField *m_Q_nmclsSUBCAT_ID;
        TStringField *m_Q_nmclsSUBCAT_NAME;
        TStringField *m_Q_linesTYPE_AB;
        TQuery *m_Q_freeze_group;
        TQuery *m_Q_freeze_with_sales;
        TQuery *m_Q_freeze_group_with_sales;
        TFloatField *m_Q_linesITEMS_PRED;
        TMLQuery *m_Q_reason;
        TFloatField *m_Q_reasonREASON_ID;
        TStringField *m_Q_reasonNAME;
        TFloatField *m_Q_itemREASON_ID;
        TStringField *m_Q_itemREASON_NAME;
        TFloatField *m_Q_listREASON_ID;
        TStringField *m_Q_listREASON_NAME;
        TMLQuery *m_Q_num_acts_list;
        TStringField *m_Q_num_acts_listDOC_NUMBER;
        TStringField *m_Q_itemDOC_NUM_RECALC;
        TStringField *m_Q_listDOC_NUM_RECALC;
        TFloatField *m_Q_listBONUS_COUNTER_AB;
        TFloatField *m_Q_listBONUS_AUDITOR_AB;
  void __fastcall m_Q_sheet_linesBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_itemBeforeClose(TDataSet *DataSet);
  void __fastcall m_Q_nmclsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_nmcls_iBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_linesIS_RECALCChange(TField *Sender);
        void __fastcall m_Q_folders_nextBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_check_orderBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_fact_hstBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_ch_cur_restBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcoBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alccodeBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcoAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_alcBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_gr_depsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_num_acts_listBeforeOpen(TDataSet *DataSet);
private:
  TTypeNmclLoad m_type_nmcl_load;
  int m_new_fldr, m_cnf_fldr, m_chk_fldr, m_recalc_fldr;
  TS_DOC_LINE_SQ_NAME("SQ_RTL_INV_ACT_ITEMS")
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
  int m_home_org_id;
  bool m_right_note_auditor;
  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_value);
private:
  void __fastcall SetTypeNmclLoad(TTypeNmclLoad p_value);
public:
  __property TTypeNmclLoad TypeNmclLoad ={read=m_type_nmcl_load, write=SetTypeNmclLoad};
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
  __property int NewFldr = {read=m_new_fldr};
  __property int CnfFldr = {read=m_cnf_fldr};
  __property int ChkFldr = {read=m_chk_fldr};
  __property int HomeOrgId = {read = m_home_org_id};
  __property int OrgId = {read=GetOrgId, write=SetOrgId};
  __property int RecalcFldr = {read=m_recalc_fldr};
  __property bool RightNoteAuditor = {read = m_right_note_auditor};
  __fastcall TDInvAct(TComponent* p_owner, AnsiString p_prog_id);
  __fastcall ~TDInvAct();
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date, const TDateTime &p_end_date);
  int __fastcall CreateActForGroup();
  virtual long __fastcall InsertLine();
  void __fastcall EditInvSubItems(int p_value, int p_line_id, AnsiString p_fldr_name, TDateTime p_new_date , AnsiString p_note );
  void __fastcall EditNonPrepare(char p_new_prepare);
  int m_sq_rs_id;

};
//---------------------------------------------------------------------------
#endif
