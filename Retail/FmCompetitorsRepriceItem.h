//---------------------------------------------------------------------------

#ifndef FmCompetitorsRepriceItemH
#define FmCompetitorsRepriceItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmCompetitorsReprice.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include "FmCompetitorsRepriceLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFCompetitorsRepriceItem : public TTSFDocumentWithLines
{
__published:	// IDE-managed Components
   TLabel *m_L_doc_number;
   TMLDBPanel *m_DBE_DOC_NUMBER;
   TMLDBPanel *m_DBE_ISSUED_DATE;
   TLabel *m_L_org;
   TLabel *m_L_note;
   TDBEdit *m_DBE_note;
   TLabel *m_L_doc_date;
   TMLDBPanel *m_DBE_org;
   void __fastcall m_ACT_editExecute(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall m_DBG_linesColEnter(TObject *Sender);
   void __fastcall m_ACT_editUpdate(TObject *Sender);
   void __fastcall m_ACT_deleteUpdate(TObject *Sender);
   void __fastcall m_ACT_viewUpdate(TObject *Sender);
   void __fastcall m_DBG_linesDblClick(TObject *Sender);
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
protected:
  TS_FORM_LINE_SHOW(TFCompetitorsRepriceLine)
 // AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
private:	// User declarations
  TDCompetitorsReprice *m_dm;
public:		// User declarations
  __fastcall TFCompetitorsRepriceItem(TComponent *p_owner,TTSDDocumentWithLines *p_dm_doc_with_ln):
                  TTSFDocumentWithLines(p_owner,p_dm_doc_with_ln),
                  m_dm(static_cast<TDCompetitorsReprice*>(DMDocumentWithLines)) {};
  void __fastcall RefreshItem();
  void __fastcall ResetItem();
};
//---------------------------------------------------------------------------
#endif
