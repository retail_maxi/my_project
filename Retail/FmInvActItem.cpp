//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvActItem.h"
#include "TSErrors.h"
#include "FmSetCnt.h"
#include "FmPutInv.h"
#include "TSFmDBMemoDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTWITHLINES"
#pragma link "MLDBPanel"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "RXCtrls"
#pragma link "MLCustomContainer"
#pragma link "TSFormContainer"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFInvActItem::TFInvActItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm)
                                     :TTSFDocumentWithLines(p_owner,p_dm),
                                     m_dm(static_cast<TDInvAct*>(p_dm)),
                                     m_view_rep_act_ctrl("TSRtlOperations.RepActInventory")
{
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_freezeUpdate(TObject *Sender)
{
  m_ACT_freeze->Enabled = m_dm->UpdateRegimeItem != urView && m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_freezeExecute(TObject *Sender)
{
  if  (m_DBG_lines->SelectedRows->Count>0)
  {
    if (MessageDlg(
          "���������� ���������� ������� �� ������ "+Now().DateString()+" "+Now().TimeString()+" ?",
          mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0
        ) == mrYes)
    {
      m_dm->m_sq_rs_id = m_dm->FillTmpReportParamValues(m_DBG_lines, "LINE_ID", 0);
      m_dm->m_Q_freeze_group->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_freeze_group->ParamByName("SQ_ID")->AsInteger = m_dm->m_sq_rs_id;
      m_dm->m_Q_freeze_group->ExecSQL();
      m_dm->RefreshItemDataSets();
    }
  }
  else
  {
    if (MessageDlg(
          "���������� ������� ���� � �������� ���� ��������� ?",
          mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0
        ) == mrYes)
    {
      m_dm->m_Q_freeze->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_freeze->ExecSQL();
      m_dm->RefreshItemDataSets();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_LOV_gr_depBeforeApply(TObject *Sender)
{
  if (m_dm->m_Q_lines->RecordCount) throw ETSError("������ ������ ����� ����� ��������� ������");
}
//---------------------------------------------------------------------------     

AnsiString __fastcall TFInvActItem::BeforeItemApply(TWinControl **p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete) {
    if(m_dm->m_Q_check_order->Active) m_dm->m_Q_check_order->Close();
    m_dm->m_Q_check_order->Open();

    //���� ��������� ������, ���������� �������� �������� ����
    if(!m_dm->m_Q_check_orderDOC_ID->IsNull)
    {
       TDateTime m_date = m_dm->m_Q_check_orderNEXT_DATE->AsDateTime;
       AnsiString m_note = "";
       bool fl;
       try {
        fl = ExecPutInvDlg(this, m_dm, &m_date, &m_note);
       }
       catch(...) {
         //������� �������� ������, ������ �������
       }

       if (fl)
       {
          m_dm->m_Q_edit_inv_item->Prepare();
          m_dm->m_Q_edit_inv_item->ParamByName("NOTE")->AsString = m_note;
          m_dm->m_Q_edit_inv_item->ParamByName("NEW_DATE")->AsDateTime = m_date;
          m_dm->m_Q_edit_inv_item->ParamByName("ID")->AsInteger = m_dm->m_Q_listID->AsInteger;
          m_dm->m_Q_edit_inv_item->ExecSQL();
          m_dm->m_Q_edit_inv_item->UnPrepare();
       }
       else
       {
         return "���������� ������� ����� ���� ���������� ��������������!";
       }
    }
    return "";
  }

  if (m_dm->m_Q_itemGR_DEP_ID->IsNull )
  {
    *p_focused = m_LV_gr_dep;
    return "�������� �����!";
  }
  
  return "";
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_appendUpdate(TObject *Sender)
{
  m_ACT_append->Enabled = m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->NewFldr && m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_CB_show_nullsClick(TObject *Sender)
{
  m_dm->m_Q_lines->ParamByName("SHOW_NULLS")->AsInteger = m_CB_show_nulls->Checked;
  m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_load_sheetsUpdate(TObject *Sender)
{
  m_ACT_load_sheets->Enabled = m_dm->UpdateRegimeItem != urView && (m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->NewFldr)||
                                                                   (m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->RecalcFldr);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_load_sheetsExecute(TObject *Sender)
{
  if (MessageDlg(
        "��������� ��������� ����������� ��������?",
        mtConfirmation,TMsgDlgButtons() << mbYes << mbNo,0
      ) == mrYes)
  {
    m_dm->m_Q_load_sheets->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_load_sheets->ExecSQL();
    m_dm->RefreshItemDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::FormShow(TObject *Sender)
{
  TTSFDocumentWithLines::FormShow(Sender);
  if( m_ACT_prev_fldr->Visible && m_ACT_next_fldr->Visible ) {
        //���������� - ����� ����� �� �������?
        m_TB_main_control_buttons_document->Align = alLeft;
        m_TB_main_control_buttons_document->Align = alRight;
  }

  m_CB_show_nulls->Checked = m_dm->m_Q_lines->ParamByName("SHOW_NULLS")->AsInteger;
 
  m_dm->m_Q_linesIS_RECALC->ReadOnly = m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr ;

    m_ACT_prev_fldr->Visible = m_dm->m_Q_folderROUTE_PREV_ID->AsInteger > 0;
    m_ACT_next_fldr->Visible = m_dm->m_Q_folderROUTE_NEXT_ID->AsInteger > 0;

    //� ��������� ���� ����������� ������� � ��������� �� ������
    m_dm->m_Q_folders_next->Close();
    m_SBTN_next_fldr->Visible = false;
    m_PM_fldr_next->Items->Clear();
    m_dm->m_Q_folders_next->Open();
    m_RSB_fldr_next->Enabled = false;

    if (!m_dm->m_Q_folders_next->Eof)
    {
        m_RSB_fldr_next->Caption = m_dm->m_Q_folders_nextNAME->AsString;
        m_RSB_fldr_next->Enabled = true;
    }

    while (!m_dm->m_Q_folders_next->Eof)
    {
        TMenuItem *NewItem = new TMenuItem(m_PM_fldr_next);
        NewItem->Caption = m_dm->m_Q_folders_nextNAME->AsString;
        NewItem->Tag = m_dm->m_Q_folders_nextID->AsInteger;
        NewItem->ImageIndex = 16;
        NewItem->OnClick = PMFldrNextClick;
        m_PM_fldr_next->Items->Add(NewItem);
        if (m_dm->m_Q_folders_next->RecordCount > 1 &&
           m_dm->m_Q_folders_next->RecNo < m_dm->m_Q_folders_next->RecordCount)
        {
          TMenuItem *NewSepItem = new TMenuItem(m_PM_fldr_next);
          NewSepItem->Caption = "-";
          m_PM_fldr_next->Items->Add(NewSepItem);
        }

        m_dm->m_Q_folders_next->Next();
    }

    m_dm->m_Q_folders_next->Close();    

    if (m_dm->m_Q_itemDOC_STATUS->AsString == 'D')
        m_RSB_fldr_next->Enabled = false;

    for (int i = 0; i < m_DBG_lines->Columns->Count; i++)
   {
     if (m_DBG_lines->Columns->Items[i]->FieldName == "IS_RECALC" )
       m_DBG_lines->Columns->Items[i]->Visible = m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr ||
                                                 m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->RecalcFldr ;
   }
   m_LV_num_acts->Visible = (m_dm->m_Q_itemREASON_ID->AsInteger == 1);
   m_L_doc_num_recalc->Visible = (m_dm->m_Q_itemREASON_ID->AsInteger == 1);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_zero_factUpdate(TObject *Sender)
{
  m_ACT_zero_fact->Enabled = m_dm->UpdateRegimeItem != urView &&
                             !m_dm->m_Q_linesLINE_ID->IsNull &&
                             (m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->NewFldr||
                              m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->RecalcFldr);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_zero_factExecute(TObject *Sender)
{
  if (MessageDlg(
        "�������� ���� � �������������� �������?",mtConfirmation,TMsgDlgButtons() << mbYes << mbNo, 0
      ) == mrYes)
  {
    m_dm->m_Q_zero_fact->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_zero_fact->ExecSQL();
    m_dm->RefreshItemDataSets();
  }
}
//---------------------------------------------------------------------------



void __fastcall TFInvActItem::m_ACT_mark_to_recalcUpdate(TObject *Sender)
{
 // ������������� ������� ��� ��������� ����� ������ � ����� "��������"
  m_ACT_mark_to_recalc->Enabled = m_dm->UpdateRegimeItem != urView && m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr ;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_mark_to_recalcExecute(TObject *Sender)
{

  MarkToRecalc(1);

}
//---------------------------------------------------------------------------
void __fastcall TFInvActItem::MarkToRecalc(int p_value)
{
 TStrings *StringList = new TStringList();
 long m_sq_id = 0;
 AnsiString m_str = "";
 if (p_value == 1)
    m_str = "�������� ���������� ������� '� ���������'?" ;
 else
    m_str = "����� � ���������� ������� ������� '� ���������'?" ;

  if (MessageDlg(
        m_str ,mtConfirmation,TMsgDlgButtons() << mbYes << mbNo, 0
      ) == mrYes)
  {
   try
   {
    for (int i = 0; i < m_DBG_lines->SelectedRows->Count; i++ )
     {
      m_dm->m_Q_lines->GotoBookmark((void *)m_DBG_lines->SelectedRows->Items[i].c_str());
      StringList->Add(m_dm->m_Q_linesLINE_ID->AsInteger);
     }
    m_sq_id = m_dm->FillTmpReportParamValues(StringList, m_sq_id);
    m_dm->m_Q_mark_recalc->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_mark_recalc->ParamByName("SQ_ID")->AsInteger = m_sq_id ;
    m_dm->m_Q_mark_recalc->ParamByName("IS_PR")->AsInteger = p_value;
    m_dm->m_Q_mark_recalc->ExecSQL();

    m_dm->RefreshItemDataSets();
    }

    __finally
    {
     delete StringList;
    }
 }
}
//---------------------------------------------------------------------------
void __fastcall TFInvActItem::PMFldrNextClick(TObject *Sender)
{
    TMenuItem *ClickedItem = dynamic_cast<TMenuItem*>(Sender);
    if (ClickedItem)
    {
        if (m_dm->NeedUpdatesItem())
            m_dm->ApplyUpdatesItem();

        int item_id = m_dm->m_Q_itemID->AsInteger;
        int fldr_id = m_dm->m_Q_folderFLDR_ID->AsInteger;
        AnsiString fldr_name = m_dm->m_Q_folderFLDR_NAME->AsString;
        AnsiString fldr_right = m_dm->m_Q_folderFLDR_RIGHT->AsString;
        int route_id = ClickedItem->Tag;
        if (route_id == 2586 ){
          if (m_dm->m_Q_ch_cur_rest->Active) m_dm->m_Q_ch_cur_rest->Close();
          m_dm->m_Q_ch_cur_rest->Open();
          AnsiString msg = "��������� ��� ��������������?";
          if (!m_dm->m_Q_ch_cur_restNMCL_ID->IsNull)
            msg = "�� ������ " + m_dm->m_Q_ch_cur_restNMCL_ID->AsString + " ��������� �������, ���������� �������������� �������. " + msg;
          if (m_dm->m_Q_ch_cur_rest->Active) m_dm->m_Q_ch_cur_rest->Close();
          if (MessageDlg(msg, mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0) != mrYes) return;
        }
        AnsiString route_name = ClickedItem->Caption;
        AnsiString route_right = m_dm->m_Q_folderROUTE_NEXT_RIGHT->AsString;
        int dest_fldr_id = m_dm->m_Q_folderFLDR_NEXT_ID->AsInteger;
        AnsiString dest_fldr_name = m_dm->m_Q_folderFLDR_NEXT_NAME->AsString;
        AnsiString dest_fldr_right = m_dm->m_Q_folderFLDR_NEXT_RIGHT->AsString;
        AnsiString params = "";

        BeforeMoveRoute(
            item_id,
            fldr_id,
            fldr_name,
            fldr_right,
            route_id,
            route_name,
            route_right,
            dest_fldr_id,
            dest_fldr_name,
            dest_fldr_right,
            &params
        );

        //Close();

        m_dm->ChangeFldr(ClickedItem->Tag,item_id,params);

        AfterMoveRoute(
            item_id,
            fldr_id,
            fldr_name,
            fldr_right,
            route_id,
            route_name,
            route_right,
            dest_fldr_id,
            dest_fldr_name,
            dest_fldr_right
        );

        Close();
        m_dm->RefreshListDataSets(true);
    }
}
//---------------------------------------------------------------------------


void __fastcall TFInvActItem::m_ACT_cnt_linkExecute(TObject *Sender)
{
   if (m_dm->NeedUpdatesItem())
            m_dm->ApplyUpdatesItem();
  SetCntDlg(this, m_dm->m_Q_itemID->AsInteger, m_dm, m_dm->UpdateRegimeItem != urView);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_clear_recalcExecute(TObject *Sender)
{
  MarkToRecalc(0);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_clear_recalcUpdate(TObject *Sender)
{
 m_ACT_clear_recalc->Enabled = m_dm->UpdateRegimeItem != urView && m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr ;        
}
//---------------------------------------------------------------------------



void __fastcall TFInvActItem::m_DBG_linesGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
  if (State.Contains(gdSelected) ||
      State.Contains(gdFocused)  ||
      State.Contains(gdFixed))
  {
        return;
  }

  if ((m_dm->m_Q_linesCARD_ID->IsNull || m_dm->m_Q_linesNOTE->AsString.Pos("������") == 1) &&  !State.Contains(gdSelected))
  {
     Background = clCream;
     AFont->Color = clBlack;
  }


  if(!m_dm->m_Q_linesPRODPP->IsNull &&
     (Column->Field->FieldName == "NMCL_ID" ||
      Column->Field->FieldName == "NMCL_NAME" ||
      Column->Field->FieldName == "ASSORTMENT_NAME"))
  {
    Background = clYellow;
    AFont->Color = clBlack;
  }
}
//---------------------------------------------------------------------------



void __fastcall TFInvActItem::m_ACT_recalcExecute(TObject *Sender)
{
    m_dm->m_Q_recalc->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_recalc->ExecSQL();
    m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_recalcUpdate(TObject *Sender)
{
  m_ACT_recalc->Enabled = m_dm->UpdateRegimeItem != urView && m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------



void __fastcall TFInvActItem::m_ACT_change_prepareExecute(TObject *Sender)
{
//������ ��������� "������������" (chekc/uncheck)

        if (m_dm->m_Q_linesNON_PREPARE->AsString == 'N')
        {
           m_dm->EditNonPrepare('Y');
        }
        else m_dm->EditNonPrepare('N');
}
//---------------------------------------------------------------------------


void __fastcall TFInvActItem::m_DBG_linesMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        if (Button == mbRight && m_dm->UpdateRegimeItem != urView &&
                m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr )
        {
                m_PM_change_prepare->Popup(int(Mouse->CursorPos.x), int(Mouse->CursorPos.y));
        }
}
//---------------------------------------------------------------------------



void __fastcall TFInvActItem::m_ACT_non_prepareUpdate(TObject *Sender)
{
   m_ACT_non_prepare->Enabled = m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_non_prepareExecute(TObject *Sender)
{
  long line_id = m_dm->m_Q_linesLINE_ID->AsInteger;
  long sq_id = m_dm->FillTmpReportParamValues(m_DBG_lines, "LINE_ID");
  m_dm->m_Q_upd_prepare->ParamByName("sq_id")->AsInteger = sq_id;
  m_dm->m_Q_upd_prepare->ParamByName("doc_id")->AsInteger = m_dm->m_Q_linesID->AsInteger;
  m_dm->m_Q_upd_prepare->ExecSQL();
  m_dm->RefreshItemDataSets();
  m_dm->m_Q_lines->Locate(m_dm->m_Q_linesLINE_ID->FieldName,line_id,TLocateOptions());
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_hstUpdate(TObject *Sender)
{
  m_ACT_hst->Enabled = !m_dm->m_Q_linesNMCL_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_hstExecute(TObject *Sender)
{
  m_FC_card_history->CreateObject();
  try
  {
    int nmcl_id = m_dm->m_Q_linesNMCL_ID->AsInteger;

    m_FC_card_history->ParamsImpl->SetParam("NMCL_ID", nmcl_id);
    m_FC_card_history->ParamsImpl->SetParam("START_DATE", int(m_dm->GetSysDate())-14);
    m_FC_card_history->ParamsImpl->SetParam("END_DATE", int(m_dm->GetSysDate()));
    m_FC_card_history->ParamsImpl->SetParam("TYPE_ID", 2);
    m_FC_card_history->FormImpl->ShowModal();
  }
  __finally
  {
    m_FC_card_history->DestroyObject();
  }
}
//---------------------------------------------------------------------------


void __fastcall TFInvActItem::m_ACT_analUpdate(TObject *Sender)
{
  m_ACT_anal->Enabled = !m_dm->m_Q_linesNMCL_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_analExecute(TObject *Sender)
{
  TMLParams params;
  params.InitParam("BEGIN_DATE", m_dm->BeginDate);
  params.InitParam("END_DATE", m_dm->EndDate);
  params.InitParam("GR_DEP_ID", m_dm->m_Q_itemGR_DEP_ID->AsInteger);
  params.InitParam("NMCL_ID", m_dm->m_Q_linesNMCL_ID->AsInteger);
  m_view_rep_act_ctrl.Execute(true,params);
}
//---------------------------------------------------------------------------


void __fastcall TFInvActItem::m_ACT_auditor_reportExecute(TObject *Sender)
{
  bool view = m_dm->m_Q_item->ReadOnly;
  if (m_dm->m_Q_item->ParamByName("FLDR_ID")->AsInteger == m_dm->CnfFldr)
     m_dm->m_Q_item->ReadOnly = true;
  if ( TSExecDBMemoDlg (this, "����� �������� � ��������������", m_dm->m_Q_item, "NOTE_AUDITOR", 500,100, ssVertical) == IDOK)
       {
         if (!m_dm->m_Q_itemNOTE_AUDITOR->IsNull)
          {
            m_dm->m_Q_item->Edit();
            m_dm->m_Q_item->Post();
            m_dm->m_Q_item->ApplyUpdates();
          }

       }
  m_dm->m_Q_item->ReadOnly = view;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_auditor_reportUpdate(TObject *Sender)
{
   m_ACT_auditor_report->Enabled = m_dm->RightNoteAuditor;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_CB_show_nonzeroClick(TObject *Sender)
{
     if (m_CB_show_nonzero->Checked )
     {
      m_dm->m_Q_lines->MacroByName("WHERE_NON_ZERO")->AsString = " AND ((i.start_items) > 0 or (i.start_items = 0 and i.fact_items > 0)) ";
     }
     else
     {
      m_dm->m_Q_lines->MacroByName("WHERE_NON_ZERO")->Clear();
     }
     m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------



void __fastcall TFInvActItem::m_ACT_deleteExecute(TObject *Sender)
{
      if( Application->MessageBox("������� ���������� �������?",
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES ){
        long sq_id = m_dm->FillTmpReportParamValues(m_DBG_lines, "LINE_ID");
        m_dm->m_Q_del_items->ParamByName("DOC_ID")->AsInteger =m_dm->m_Q_itemID->AsInteger;
        m_dm->m_Q_del_items->ParamByName("sq_id")->AsInteger = sq_id;
        m_dm->m_Q_del_items->ExecSQL();
        m_dm->RefreshItemDataSets();
      }
}
//---------------------------------------------------------------------------




void __fastcall TFInvActItem::m_ACT_freeze_with_salesExecute(
      TObject *Sender)
{
    if  (m_DBG_lines->SelectedRows->Count>0)
  {
    if (MessageDlg(
          "���������� ���������� ������� �� ������ "+Now().DateString()+" "+Now().TimeString()+" ?",
          mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0
        ) == mrYes)
    {
      m_dm->m_sq_rs_id = m_dm->FillTmpReportParamValues(m_DBG_lines, "LINE_ID", 0);
      m_dm->m_Q_freeze_group_with_sales->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_freeze_group_with_sales->ParamByName("SQ_ID")->AsInteger = m_dm->m_sq_rs_id;
      m_dm->m_Q_freeze_group_with_sales->ExecSQL();
      m_dm->RefreshItemDataSets();
    }
  }
  else
  {
    if (MessageDlg(
          "���������� ������� ���� � �������� ���� ��������� ?",
          mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0
        ) == mrYes)
    {
      m_dm->m_Q_freeze_with_sales->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_freeze_with_sales->ExecSQL();
      m_dm->RefreshItemDataSets();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_ACT_freeze_with_salesUpdate(
      TObject *Sender)
{
   m_ACT_freeze_with_sales->Enabled = m_dm->UpdateRegimeItem != urView && m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_LOV_reasonAfterApply(TObject *Sender)
{
   m_dm->m_Q_item->Edit();
   m_dm->m_Q_itemDOC_NUM_RECALC->Clear();
   m_dm->m_Q_item->Post();
   m_LV_num_acts->Visible = (m_dm->m_Q_itemREASON_ID->AsInteger == 1);
   m_L_doc_num_recalc->Visible = (m_dm->m_Q_itemREASON_ID->AsInteger == 1);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActItem::m_LOV_reasonAfterClear(TObject *Sender)
{
   m_dm->m_Q_item->Edit();
   m_dm->m_Q_itemDOC_NUM_RECALC->Clear();
   m_dm->m_Q_item->Post();
   m_LV_num_acts->Visible = (m_dm->m_Q_itemREASON_ID->AsInteger == 1);
   m_L_doc_num_recalc->Visible = (m_dm->m_Q_itemREASON_ID->AsInteger == 1);
}
//---------------------------------------------------------------------------

