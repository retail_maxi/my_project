inherited FSetCnt: TFSetCnt
  Left = 295
  Top = 215
  Width = 741
  Height = 504
  BorderStyle = bsSizeable
  Caption = '���������'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 446
    Width = 733
    TabOrder = 1
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 539
      inherited m_BBTN_close: TBitBtn
        Caption = '�������'
      end
    end
  end
  object m_P_main: TPanel [1]
    Left = 0
    Top = 0
    Width = 733
    Height = 446
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 305
      Top = 0
      Width = 5
      Height = 446
      Cursor = crHSplit
    end
    object m_P_left: TPanel
      Left = 0
      Top = 0
      Width = 305
      Height = 446
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object m_DBG_cnt: TMLDBGrid
        Left = 0
        Top = 0
        Width = 305
        Height = 446
        Align = alClient
        DataSource = m_DS_cnt
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'CNT_ID'
            Footers = <>
          end
          item
            FieldName = 'CNT_NAME'
            Width = 87
            Footers = <>
          end
          item
            FieldName = 'IS_MAIN'
            Width = 104
            KeyList.Strings = (
              '1'
              '0')
            Checkboxes = True
            Footers = <>
          end>
      end
    end
    object m_P_all: TPanel
      Left = 310
      Top = 0
      Width = 423
      Height = 446
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object m_P_btns: TPanel
        Left = 0
        Top = 0
        Width = 24
        Height = 446
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object m_SBTN_add: TSpeedButton
          Left = 0
          Top = 0
          Width = 23
          Height = 22
          Action = m_ACT_add
          ParentShowHint = False
          ShowHint = True
        end
        object m_SBTN_delete: TSpeedButton
          Left = 0
          Top = 28
          Width = 23
          Height = 22
          Action = m_ACT_delete
          ParentShowHint = False
          ShowHint = True
        end
      end
      object m_PC_all_cnt: TPageControl
        Left = 24
        Top = 0
        Width = 399
        Height = 446
        ActivePage = m_TS_cnt
        Align = alClient
        TabOrder = 1
        object m_TS_cnt: TTabSheet
          Caption = '��� ����������'
          object m_DBG_all_cnt: TMLDBGrid
            Left = 0
            Top = 41
            Width = 391
            Height = 377
            Align = alClient
            DataSource = m_DS_all_cnt
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterColor = clWindow
            AutoFitColWidths = True
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
            Columns = <
              item
                FieldName = 'CNT_ID'
                Title.TitleButton = True
                Footers = <>
              end
              item
                FieldName = 'CNT_NAME'
                Title.TitleButton = True
                Width = 250
                Footers = <>
              end
              item
                FieldName = 'POSITION'
                Title.TitleButton = True
                Width = 150
                Footers = <>
              end>
          end
          object m_P_topp: TPanel
            Left = 0
            Top = 0
            Width = 391
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label1: TLabel
              Left = 8
              Top = 16
              Width = 80
              Height = 13
              Caption = '�������������'
            end
          end
          object m_DBLUB_dep: TDBLookUpTreeView
            Left = 96
            Top = 10
            Width = 289
            Height = 21
            CanSelectParents = True
            DropDownRows = 11
            ParentColor = False
            TabOrder = 2
            TabStop = True
            TreeViewColor = clWindow
            TreeViewCursor = crDefault
            TreeViewDragMode = dmManual
            TreeViewFont.Charset = DEFAULT_CHARSET
            TreeViewFont.Color = clWindowText
            TreeViewFont.Height = -11
            TreeViewFont.Name = 'MS Sans Serif'
            TreeViewFont.Style = []
            TreeViewIndent = 19
            TreeViewReadOnly = False
            TreeViewShowButtons = True
            TreeViewShowHint = False
            TreeViewShowLines = True
            TreeViewShowRoot = True
            TreeViewSortType = stText
            DividedChar = '.'
            ListSource = m_DS_department
            KeyField = 'DEP_ID'
            ListField = 'NAME'
            Options = [trDBCanDelete, trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
            ParentField = 'MASTER_ID'
            TextStyle = tvtsShort
            DataField = 'DEP_ID'
            DataSource = m_DS_dep
          end
        end
      end
    end
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_close: TAction
      Caption = '�������'
    end
    inherited m_ACT_apply_updates: TAction
      Enabled = False
      Visible = False
    end
    object m_ACT_add: TAction
      Category = 'DocActions'
      Caption = '<'
      Hint = '�������� � ������'
      OnExecute = m_ACT_addExecute
      OnUpdate = m_ACT_addUpdate
    end
    object m_ACT_delete: TAction
      Category = 'DocActions'
      Caption = '>'
      Hint = '��������� �� ������'
      OnExecute = m_ACT_deleteExecute
      OnUpdate = m_ACT_deleteUpdate
    end
  end
  object m_DS_cnt: TDataSource
    DataSet = m_Q_cnt
    Left = 64
    Top = 32
  end
  object m_DS_all_cnt: TDataSource
    DataSet = m_Q_all_cnt
    Left = 34
    Top = 64
  end
  object m_Q_add_cnt: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '   prc_add_cnt_to_inv_act(:DOC_ID, :SQ_ID);'
      'END;')
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_delete_cnt: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '   prc_delete_cnt_from_inv_act(:DOC_ID, :SQ_ID);'
      'END;')
    Top = 128
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_DS_department: TDataSource
    DataSet = m_Q_department
    Left = 536
    Top = 40
  end
  object m_Q_all_cnt: TMLQuery
    BeforeOpen = m_Q_all_cntBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT s.cnt_id,'
      
        '       TRIM(RPAD(pkg_persons.fnc_person_name(s.cnt_id, 0),250)) ' +
        'AS cnt_name, '
      '       st.position'
      'FROM departments d,'
      '     staff_table st,'
      '     staffmen s'
      'WHERE d.id = st.dep_id'
      '  AND st.id = s.staff_table_id'
      '  AND s.fire_date IS NULL'
      '  AND d.id IN (select d.id'
      '                       from departments d'
      '                       start with d.id = :DEP_ID'
      '                       connect by prior d.id = d.master_id  )'
      '  AND NOT EXISTS (SELECT 1 FROM rtl_inv_act_cnt rc'
      '                  WHERE rc.doc_id = :DOC_ID'
      '                    AND rc.cnt_id = s.cnt_id) '
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DEP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_all_cntCNT_ID: TFloatField
      DisplayLabel = 'ID ����.'
      FieldName = 'CNT_ID'
      Origin = 's.cnt_id'
    end
    object m_Q_all_cntCNT_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'CNT_NAME'
      Origin = 'TRIM(RPAD(pkg_persons.fnc_person_name(s.cnt_id, 0),250)) '
      Size = 250
    end
    object m_Q_all_cntPOSITION: TStringField
      DisplayLabel = '���������'
      FieldName = 'POSITION'
      Origin = 'st.position'
      Size = 100
    end
  end
  object m_U_cnt: TMLUpdateSQL
    Left = 32
    Top = 32
  end
  object m_DS_dep: TDataSource
    DataSet = m_Q_curr_dep
    Left = 418
    Top = 56
  end
  object m_Q_curr_dep: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT :DEP_ID AS dep_id'
      'FROM dual')
    UpdateObject = m_U_curr_dep
    Macros = <>
    Left = 384
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DEP_ID'
        ParamType = ptInput
      end>
    object m_Q_curr_depDEP_ID: TFloatField
      FieldName = 'DEP_ID'
      OnChange = m_Q_curr_depDEP_IDChange
    end
  end
  object m_U_curr_dep: TMLUpdateSQL
    IgnoreRowsAffected = True
    Left = 386
    Top = 88
  end
  object m_Q_department: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id AS dep_id,  name, master_id'
      'FROM globalref.departments d'
      '/*'
      'WHERE d.id IN (SELECT a.row_id'
      '               FROM employeers e,'
      '                    admin.access_for_row_activity a'
      '               WHERE e.user_name = user AND'
      '                     a.emp_id = e.person_cnt_id AND'
      '                     a.policy_id =  11 AND'
      '                     a.row_id = d.id AND'
      '                     a.access_id = 8'
      '               UNION ALL'
      '               SELECT r.row_id'
      '               FROM employeers e,'
      '                    admin.templ_to_empl t,'
      '                    admin.templ_acc_for_row_act  r'
      '               WHERE e.user_name = user AND'
      '                     t.emp_id = e.person_cnt_id AND'
      '                     t.org_id IS NULL AND'
      '                     r.templ_id = t.templ_id AND'
      '                     r.policy_id = 11 AND'
      '                     r.row_id = d.id AND'
      '                     r.access_id = 8'
      '               UNION ALL       '
      '               SELECT r.row_id'
      '               FROM employeers e,'
      '                    admin.templ_to_empl t,'
      '                    admin.templ_acc_for_row_act  r'
      '               WHERE e.user_name = user AND'
      '                     t.emp_id = e.person_cnt_id AND'
      
        '                     t.org_id = session_globals.get_home_org_id ' +
        ' AND'
      '                     r.templ_id = t.templ_id AND'
      '                     r.policy_id = 11 AND'
      '                     r.row_id = d.id AND'
      '                     r.access_id = 8)*/'
      'ORDER BY name')
    Macros = <>
    Left = 568
    Top = 40
    object m_Q_departmentDEP_ID: TFloatField
      FieldName = 'DEP_ID'
    end
    object m_Q_departmentNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.DEPARTMENTS.NAME'
      Size = 100
    end
    object m_Q_departmentMASTER_ID: TFloatField
      FieldName = 'MASTER_ID'
      Origin = 'TSDBMAIN.DEPARTMENTS.MASTER_ID'
    end
  end
  object m_Q_upd_cnt: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  prc_upd_cnt_to_inv_act(:DOC_ID, :CNT_ID, :IS_MAIN);'
      'END;')
    Top = 160
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CNT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IS_MAIN'
        ParamType = ptInput
      end>
  end
  object m_Q_cnt: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT ra.doc_id,'
      '       r.cnt_id,'
      
        '       TRIM(RPAD(pkg_persons.fnc_person_name(r.cnt_id, 0),250)) ' +
        'AS cnt_name,'
      '        nvl(r.is_main,0) AS is_main'
      'FROM rtl_inv_acts ra,'
      '     rtl_inv_act_cnt r     '
      'WHERE ra.doc_id = :DOC_ID'
      '  AND ra.doc_id = r.doc_id    '
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_cnt
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Top = 32
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_cntDOC_ID: TFloatField
      FieldName = 'DOC_ID'
    end
    object m_Q_cntCNT_ID: TFloatField
      DisplayLabel = 'ID ����.'
      FieldName = 'CNT_ID'
      Origin = 'r.cnt_id'
    end
    object m_Q_cntCNT_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'CNT_NAME'
      Origin = 'TRIM(RPAD(pkg_persons.fnc_person_name(r.cnt_id, 0),250)) '
      Size = 250
    end
    object m_Q_cntIS_MAIN: TFloatField
      DisplayLabel = '������� ��.'
      FieldName = 'IS_MAIN'
      OnChange = m_Q_cntIS_MAINChange
    end
  end
end
