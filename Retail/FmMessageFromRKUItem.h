//---------------------------------------------------------------------------

#ifndef FmMessageFromRKUItemH
#define FmMessageFromRKUItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENT.h"
#include "DmMessageFromRKU.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFMessageFromRKUItem : public TTSFDocument
{
__published:	// IDE-managed Components
    TLabel *m_L_doc_number;
    TMLDBPanel *m_DBP_doc_number;
    TLabel *m_L_doc_date;
    TMLDBPanel *m_DBP_doc_date;
    TLabel *m_L_send_date;
    TMLDBPanel *m_DBP_send_date;
    TDataSource *m_DS_type_mess;
    TMLLov *m_LOV_type_mess;
    TLabel *m_L_host;
    TLabel *m_L_rku;
    TDBEdit *m_DBE_rku;
    TLabel *m_L_type_mess;
    TMLLovListView *m_LV_type_message;
    TLabel *m_L_cre_user_id;
    TMLDBPanel *m_DBP_read_user;
    TDBMemo *m_DBM_note;
    TLabel *m_L_note;
    TMLDBPanel *m_DBP_host;
private:	// User declarations
    TDMessageFromRKU *m_dm;
public:		// User declarations
    __fastcall TFMessageFromRKUItem(TComponent* p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------
#endif
