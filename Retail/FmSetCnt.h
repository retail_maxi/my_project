//---------------------------------------------------------------------------

#ifndef FmSetCntH
#define FmSetCntH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmInvAct.h"
#include "MLQuery.h"
#include "RxQuery.hpp"
#include <DBTables.hpp>
#include "dbtree_l.hpp"
#include "MLUpdateSQL.h"
//---------------------------------------------------------------------------

class TFSetCnt : public TTSFDialog
{
__published:
  TPanel *m_P_main;
  TPanel *m_P_left;
  TPanel *m_P_all;
  TSplitter *Splitter1;
  TPanel *m_P_btns;
  TSpeedButton *m_SBTN_add;
        TPageControl *m_PC_all_cnt;
        TTabSheet *m_TS_cnt;
  TSpeedButton *m_SBTN_delete;
        TDataSource *m_DS_cnt;
        TMLDBGrid *m_DBG_all_cnt;
  TAction *m_ACT_add;
  TAction *m_ACT_delete;
        TDataSource *m_DS_all_cnt;
        TQuery *m_Q_add_cnt;
        TQuery *m_Q_delete_cnt;
        TPanel *m_P_topp;
        TLabel *Label1;
        TDataSource *m_DS_department;
        TMLQuery *m_Q_all_cnt;
        TFloatField *m_Q_all_cntCNT_ID;
        TStringField *m_Q_all_cntCNT_NAME;
        TStringField *m_Q_all_cntPOSITION;
        TMLUpdateSQL *m_U_cnt;
        TDataSource *m_DS_dep;
        TMLQuery *m_Q_curr_dep;
        TFloatField *m_Q_curr_depDEP_ID;
        TMLUpdateSQL *m_U_curr_dep;
        TMLQuery *m_Q_department;
        TFloatField *m_Q_departmentDEP_ID;
        TStringField *m_Q_departmentNAME;
        TFloatField *m_Q_departmentMASTER_ID;
        TDBLookUpTreeView *m_DBLUB_dep;
        TMLDBGrid *m_DBG_cnt;
        TQuery *m_Q_upd_cnt;
        TMLQuery *m_Q_cnt;
        TFloatField *m_Q_cntDOC_ID;
        TFloatField *m_Q_cntCNT_ID;
        TStringField *m_Q_cntCNT_NAME;
        TFloatField *m_Q_cntIS_MAIN;
  void __fastcall m_ACT_addExecute(TObject *Sender);
  void __fastcall m_ACT_addUpdate(TObject *Sender);
  void __fastcall m_ACT_deleteExecute(TObject *Sender);
  void __fastcall m_ACT_deleteUpdate(TObject *Sender);
        void __fastcall m_Q_all_cntBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_curr_depDEP_IDChange(TField *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_Q_cntIS_MAINChange(TField *Sender);
private:
  bool is_edit;
public:
  __fastcall TFSetCnt(TComponent* p_owner,
                     TTSDCustom* p_dm_custom, bool p_edit): TTSFDialog(p_owner,
                                                          p_dm_custom),
                                                          is_edit(p_edit) {};
};
//---------------------------------------------------------------------------

void SetCntDlg(TComponent* p_owner, int p_doc_id, TTSDCustom* p_dm, bool p_edit = false);
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

