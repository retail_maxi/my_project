//---------------------------------------------------------------------------

#ifndef FmRtlSalesListH
#define FmRtlSalesListH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENTLIST.h"
#include "DmRtlSales.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "FmFindKass.h"
#include <Dialogs.hpp>
#include <DBCtrls.hpp>
//---------------------------------------------------------------------------

class TFRtlSalesList : public TTSFDocumentList
{
__published:
  TToolButton *m_TBTN_sep6;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TMLDBGrid *MLDBGrid1;
  TDataSource *m_DS_checks;
  TSplitter *Splitter1;
        TAction *m_ACT_period;
        TSpeedButton *m_SBTN_period;
        TCheckBox *m_CB_tabac;
        TToolButton *ToolButton1;
        TDBLookupComboBox *m_DBLCB_organizations;
        TToolButton *ToolButton2;
        TDataSource *m_DS_orgs;
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_find_kasUpdate(TObject *Sender);
        void __fastcall m_ACT_periodExecute(TObject *Sender);
        void __fastcall m_CB_tabacClick(TObject *Sender);
        void __fastcall m_DBLCB_organizationsClick(TObject *Sender);
private:
  TDRtlSales *m_dm;
public:
  __fastcall TFRtlSalesList(TComponent* p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

