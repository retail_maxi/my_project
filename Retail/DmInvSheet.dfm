inherited DInvSheet: TDInvSheet
  OldCreateOrder = False
  Left = 507
  Top = 271
  Height = 640
  Width = 974
  inherited m_DB_main: TDatabase
    AliasName = 'comlabs'
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'select'
      '  d.id id,'
      '  d.src_org_id doc_src_org_id,'
      '  o.name doc_src_org_name,'
      '  d.create_date doc_create_date,'
      '  d.comp_name doc_create_comp_name,'
      '  d.author doc_create_user,'
      '  d.modify_date doc_modify_date,'
      '  d.modify_comp_name doc_modify_comp_name,'
      '  d.modify_user doc_modify_user,'
      '  d.status doc_status,'
      
        '  decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �� ���' +
        '���'#39','#39'������'#39') doc_status_name,'
      '  d.status_change_date doc_status_change_date,'
      '  d.fldr_id doc_fldr_id,'
      '  f.name doc_fldr_name,'
      
        '  substr(fnc_mask_folder_right(f.id,:ACCESS_MASK),1,250) doc_fld' +
        'r_right,'
      '  s.doc_number,'
      '  s.doc_date,'
      '  s.note,'
      
        '  nvl2(a.doc_id,'#39'�'#39' || a.doc_number || '#39' �� '#39' || to_char(a.doc_d' +
        'ate,'#39'dd.mm.yyyy'#39'),'#39'<���>'#39') act'
      'from '
      '  documents d,'
      '  folders f,'
      '  organizations o,'
      '  rtl_inv_sheets s,'
      '  rtl_inv_acts a'
      'where f.doc_type_id = :DOC_TYPE_ID'
      '  and f.id = d.fldr_id'
      '  and o.id = d.src_org_id'
      '  and fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '  and s.doc_id = d.id'
      '  and trunc(s.doc_date) between :BEGIN_DATE AND :END_DATE'
      '  and a.doc_id (+) = s.inv_doc_id'
      
        '  AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_id = :ORG_' +
        'ID))'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���������'
      DisplayWidth = 10
      FieldName = 'DOC_NUMBER'
      Origin = 's.doc_number'
      Size = 100
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'DOC_DATE'
      Origin = 's.doc_date'
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 30
      FieldName = 'NOTE'
      Origin = 's.note'
      Size = 250
    end
    object m_Q_listACT: TStringField
      DisplayLabel = '���'
      DisplayWidth = 30
      FieldName = 'ACT'
      Origin = 
        'nvl2(a.doc_id,'#39'�'#39' || a.doc_number || '#39' �� '#39' || to_char(a.doc_dat' +
        'e,'#39'dd.mm.yyyy'#39'),'#39'<���>'#39')'
      Size = 115
    end
  end
  inherited m_Q_item: TMLQuery
    BeforeClose = m_Q_itemBeforeClose
    SQL.Strings = (
      'select '
      '  s.doc_id id, s.doc_number, s.doc_date, s.note, s.inv_doc_id, '
      
        '  nvl2(s.inv_doc_id,a.doc_number || '#39' �� '#39' || to_char(a.doc_date' +
        ','#39'dd.mm.yyyy'#39'),'#39#39') act_number,'
      '  d.fldr_id'
      'from rtl_inv_sheets s, rtl_inv_acts a, documents d'
      
        'where s.doc_id = :ID and :ID = (select id from documents where i' +
        'd = :ID and fldr_id = :FLDR_ID)'
      '  and a.doc_id (+) = s.inv_doc_id'
      '  and d.id = s.doc_id')
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 100
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemINV_DOC_ID: TFloatField
      FieldName = 'INV_DOC_ID'
    end
    object m_Q_itemACT_NUMBER: TStringField
      FieldName = 'ACT_NUMBER'
      Size = 115
    end
    object m_Q_itemFLDR_ID: TFloatField
      FieldName = 'FLDR_ID'
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_inv_sheets'
      'set '
      '  doc_number = :DOC_NUMBER, '
      '  doc_date = :DOC_DATE, '
      '  note = :NOTE,'
      '  inv_doc_id = :INV_DOC_ID'
      'where doc_id = :OLD_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_inv_sheets (doc_id, doc_number, doc_date, note, ' +
        'inv_doc_id)'
      'values (:ID, :DOC_NUMBER, :DOC_DATE, :NOTE, :INV_DOC_ID)')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      'END; ')
  end
  inherited m_Q_lines: TMLQuery
    AfterOpen = m_Q_linesAfterOpen
    SQL.Strings = (
      'SELECT '
      ' *'
      'FROM'
      '('
      'select '
      '  si.doc_id id,'
      '  si.line_id, '
      '  si.start_date,'
      '  si.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.short_name mu_name,'
      '  a.name assortment_name,'
      '  si.items_qnt,'
      '  si.note,'
      '  ni.prod_type_id,'
      '  pt.name as prod_type_name,'
      '  ( /*�������� �������� �����*/ '
      
        '    SELECT max('#39'��'#39') FROM rtl_inv_sheets ris2, rtl_inv_sheet_sub' +
        'items risub2, documents d'
      '    WHERE ris2.inv_doc_id = ris.inv_doc_id '
      '      AND ris2.doc_id != ris.doc_id '
      '      AND risub2.doc_id = ris2.doc_id '
      
        '      AND exists(SELECT null FROM rtl_inv_sheet_subitems risub_c' +
        'ur '
      
        '            WHERE risub_cur.doc_id = ris.doc_id AND risub_cur.pd' +
        'f_bar_code = risub2.pdf_bar_code '
      '            AND risub_cur.line_id = si.line_id '
      '      )'
      '      and d.id = ris2.doc_id'
      '      and d.fldr_id = 2456 -- �������'
      '      and d.status != '#39'D'#39
      '   ) as dubl_pdf_code '
      'from '
      '  rtl_inv_sheet_items si,'
      '  rtl_inv_sheets ris,'
      '  nomenclature_items ni,'
      '  assortment a,'
      '  measure_units mu,'
      '  products_types pt'
      'where si.doc_id = :ID  '
      '  and ris.doc_id = si.doc_id'
      '  and ni.id = si.nmcl_id'
      '  and a.id = si.assortment_id'
      '  and mu.id = ni.meas_unit_id'
      '  and ni.prod_type_id = pt.id'
      ') v'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'ID'
    end
    object m_Q_linesSTART_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'START_DATE'
      Origin = 'v.start_date'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'v.nmcl_id'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      DisplayWidth = 30
      FieldName = 'NMCL_NAME'
      Origin = 'v.NMCL_NAME'
      Size = 100
    end
    object m_Q_linesMU_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MU_NAME'
      Origin = 'v.MU_NAME'
      Size = 10
    end
    object m_Q_linesASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 30
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'v.ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_linesITEMS_QNT: TFloatField
      DisplayLabel = '����������'
      FieldName = 'ITEMS_QNT'
      Origin = 'v.items_qnt'
    end
    object m_Q_linesNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 40
      FieldName = 'NOTE'
      Origin = 'v.note'
      Size = 250
    end
    object m_Q_linesPROD_TYPE_ID: TFloatField
      DisplayLabel = 'ID ������ ������'
      FieldName = 'PROD_TYPE_ID'
      Origin = 'v.PROD_TYPE_ID'
    end
    object m_Q_linesPROD_TYPE_NAME: TStringField
      DisplayLabel = '������ ������'
      FieldName = 'PROD_TYPE_NAME'
      Origin = 'v.PROD_TYPE_NAME'
      Size = 100
    end
    object m_Q_linesDUBL_PDF_CODE: TStringField
      DisplayLabel = '������������ �������� �����'
      DisplayWidth = 20
      FieldName = 'DUBL_PDF_CODE'
      Origin = 'v.DUBL_PDF_CODE'
      FixedChar = True
      Size = 2
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'select '
      '  si.doc_id id,'
      '  si.line_id, '
      '  si.start_date,'
      '  si.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.short_name mu_name,'
      '  a.id assortment_id,'
      '  a.name assortment_name,'
      '  si.items_qnt, '
      '  si.note'
      'from '
      '  rtl_inv_sheet_items si,'
      '  nomenclature_items ni,'
      '  assortment a,'
      '  measure_units mu'
      'where si.doc_id = :ID'
      '  and si.line_id = :LINE_ID'
      '  and ni.id = si.nmcl_id'
      '  and a.id = si.assortment_id'
      '  and mu.id = ni.meas_unit_id')
    object m_Q_lineSTART_DATE: TDateTimeField
      FieldName = 'START_DATE'
      Origin = 'TSDBMAIN.RTL_INV_SHEET_ITEMS.START_DATE'
    end
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.RTL_INV_SHEET_ITEMS.NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_lineMU_NAME: TStringField
      FieldName = 'MU_NAME'
      Origin = 'TSDBMAIN.MEASURE_UNITS.SHORT_NAME'
      Size = 10
    end
    object m_Q_lineASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
      Origin = 'TSDBMAIN.ASSORTMENT.ID'
    end
    object m_Q_lineASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
    object m_Q_lineITEMS_QNT: TFloatField
      FieldName = 'ITEMS_QNT'
      Origin = 'TSDBMAIN.RTL_INV_SHEET_ITEMS.ITEMS_QNT'
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_INV_SHEET_ITEMS.NOTE'
      Size = 250
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_inv_sheet_items'
      'set'
      '  start_date = :START_DATE,'
      '  nmcl_id = :NMCL_ID,'
      '  assortment_id = :ASSORTMENT_ID,'
      '  items_qnt = :ITEMS_QNT,'
      '  note = :NOTE'
      'where doc_id = :OLD_ID and line_id = :OLD_LINE_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_inv_sheet_items (doc_id, line_id, start_date, nm' +
        'cl_id, assortment_id, items_qnt, note)'
      
        'values (:ID, :LINE_ID, :START_DATE, :NMCL_ID, :ASSORTMENT_ID, :I' +
        'TEMS_QNT, :NOTE)')
    DeleteSQL.Strings = (
      'begin'
      
        'delete from rtl_inv_sheet_subitems where doc_id = :OLD_ID and li' +
        'ne_id = :OLD_LINE_ID;'
      
        'delete from rtl_inv_sheet_items where doc_id = :OLD_ID and line_' +
        'id = :OLD_LINE_ID;'
      'end;')
    IgnoreRowsAffected = True
  end
  object m_Q_acts: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      '  a.doc_id,'
      
        '  a.doc_number || '#39' �� '#39' || to_char(a.doc_date,'#39'dd.mm.yyyy'#39') act' +
        '_number,'
      '  a.note'
      'from '
      '  rtl_inv_acts a,'
      '  documents d'
      'where d.id = a.doc_id'
      '  and d.fldr_id in (2458, 1607337)'
      '  and d.src_org_id = session_globals.get_home_org_id'
      '  and d.status != '#39'D'#39
      '%WHERE_CLAUSE'
      'order by doc_number')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 56
    Top = 304
    object m_Q_actsDOC_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'DOC_ID'
      Origin = 'a.doc_id'
    end
    object m_Q_actsACT_NUMBER: TStringField
      DisplayLabel = '�'
      DisplayWidth = 30
      FieldName = 'ACT_NUMBER'
      Origin = 'a.doc_number || '#39' �� '#39' || to_char(a.doc_date,'#39'dd.mm.yyyy'#39')'
      Size = 115
    end
    object m_Q_actsNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 30
      FieldName = 'NOTE'
      Origin = 'a.note'
      Size = 250
    end
  end
  object m_Q_nmcls: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct'
      '  ni.id nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.short_name meas_name'
      'from '
      '  nmcl_bar_codes nbc,'
      '  nomenclature_items ni,'
      '  measure_units mu'
      'where '
      '  ni.id = nbc.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '%WHERE_CLAUSE'
      'order by ni.name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 60
    Top = 364
    object m_Q_nmclsNMCL_ID: TFloatField
      DisplayLabel = '���'
      FieldName = 'NMCL_ID'
      Origin = 'ni.id'
    end
    object m_Q_nmclsNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_nmclsMEAS_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MEAS_NAME'
      Origin = 'mu.short_name'
      Size = 10
    end
  end
  object m_Q_assortments: TMLQuery
    BeforeOpen = m_Q_assortmentsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct'
      '  a.id,'
      '  a.name'
      'from '
      '  nmcl_bar_codes nbc,'
      '  assortment a'
      'where '
      '  nbc.nmcl_id = :NMCL_ID'
      '  and a.id = nbc.assortment_id'
      '%WHERE_CLAUSE'
      'order by a.name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 148
    Top = 364
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_assortmentsID: TFloatField
      FieldName = 'ID'
      Origin = 'a.id'
    end
    object m_Q_assortmentsNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'a.name'
      Size = 100
    end
  end
  object m_Q_barcode_info: TQuery
    AfterClose = m_Q_barcode_infoAfterClose
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select b.nmcl_id, ni.name nmcl_name, b.assortment_id, a.name ass' +
        'ortment_name'
      'from nmcl_bar_codes b, nomenclature_items ni, assortment a'
      
        'where b.bar_code = :BARCODE and ni.id = b.nmcl_id and a.id = ass' +
        'ortment_id')
    Left = 144
    Top = 304
    ParamData = <
      item
        DataType = ftString
        Name = 'BARCODE'
        ParamType = ptInput
      end>
    object m_Q_barcode_infoNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.NMCL_BAR_CODES.NMCL_ID'
    end
    object m_Q_barcode_infoNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_barcode_infoASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
      Origin = 'TSDBMAIN.NMCL_BAR_CODES.ASSORTMENT_ID'
    end
    object m_Q_barcode_infoASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
  end
  object m_Q_fill_lines: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '      PKG_RTL_INV.Prc_Fill_Lines_Inv_Sheet(:DOC_ID);'
      'END;')
    Macros = <>
    Left = 56
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 552
    Top = 240
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_Q_alco: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcoBeforeOpen
    AfterInsert = m_Q_alcoAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select s.doc_id id,'
      '       s.line_id,'
      '       s.sub_line_id,'
      '       s.pdf_bar_code,'
      
        '       DECODE(s.alccode,'#39'XXXXXXXXXXXXXXXXXXX'#39','#39'-1'#39',s.alccode) as' +
        ' alccode,'
      '       s.qnt,'
      '       ep.fullname'
      'from rtl_inv_sheet_subitems s,'
      '        egais_pref ep'
      'where doc_id = :DOC_ID'
      'and line_id = :LINE_ID'
      'and sub_line_id = :SUB_LINE_ID'
      'and ep.alccode(+) = s.alccode')
    UpdateObject = m_U_alco
    Macros = <>
    Left = 304
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SUB_LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_alcoID: TFloatField
      DisplayLabel = 'DOC_ID'
      FieldName = 'ID'
      Origin = 'S.DOC_ID'
    end
    object m_Q_alcoLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'S.LINE_ID'
    end
    object m_Q_alcoSUB_LINE_ID: TFloatField
      FieldName = 'SUB_LINE_ID'
      Origin = 'S.SUB_LINE_ID'
    end
    object m_Q_alcoPDF_BAR_CODE: TStringField
      DisplayLabel = '��� �������� �����'
      FieldName = 'PDF_BAR_CODE'
      Origin = 'S.PDF_BAR_CODE'
      Size = 100
    end
    object m_Q_alcoALCCODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'ALCCODE'
      Origin = 'DECODE(s.alccode,'#39'XXXXXXXXXXXXXXXXXXX'#39','#39'-1'#39',s.alccode)'
      Size = 50
    end
    object m_Q_alcoQNT: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QNT'
      Origin = 'S.QNT'
    end
    object m_Q_alcoFULLNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'FULLNAME'
      Origin = 'EP.FULLNAME'
      Size = 255
    end
  end
  object m_U_alco: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_inv_sheet_subitems'
      
        'set alccode =  DECODE( :ALCCODE, '#39'-1'#39','#39'XXXXXXXXXXXXXXXXXXX'#39', :AL' +
        'CCODE ), '
      '      qnt = :QNT'
      'where doc_id = :OLD_ID'
      'and line_id = :OLD_LINE_ID'
      'and sub_line_id = :OLD_SUB_LINE_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_inv_sheet_subitems (doc_id, line_id, sub_line_id' +
        ', pdf_bar_code, alccode, '
      'qnt)'
      
        'values (:ID, :LINE_ID, sq_rtl_inv_sheet_subitems.nextval, :PDF_B' +
        'AR_CODE, '
      
        'DECODE( :ALCCODE , '#39'-1'#39','#39'XXXXXXXXXXXXXXXXXXX'#39', :ALCCODE ), :QNT ' +
        ')')
    DeleteSQL.Strings = (
      
        'delete from rtl_inv_sheet_subitems where doc_id = :OLD_ID and li' +
        'ne_id = '
      ':OLD_LINE_ID and sub_line_id = :OLD_SUB_LINE_ID')
    IgnoreRowsAffected = True
    Left = 376
    Top = 304
  end
  object m_Q_alc: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcBeforeOpen
    AfterInsert = m_Q_alcAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT *'
      'FROM'
      '('
      'select s.doc_id id,'
      '       s.line_id,'
      '       s.sub_line_id,'
      '       s.pdf_bar_code,'
      '       s.alccode,'
      '       s.qnt,'
      '       ep.fullname,'
      '      ( /*�������� �������� �����*/ '
      
        '        SELECT max('#39'��'#39') FROM rtl_inv_sheets ris2, rtl_inv_sheet' +
        '_subitems risub2, documents d'
      '        WHERE ris2.inv_doc_id = ris.inv_doc_id '
      '          AND ris2.doc_id != ris.doc_id '
      '          AND risub2.doc_id = ris2.doc_id '
      '          AND risub2.pdf_bar_code = s.pdf_bar_code'
      '          and d.id = ris2.doc_id'
      '          and d.fldr_id = 2456 -- �������'
      '          and d.status != '#39'D'#39
      '       )  as dubl_pdf_code'
      'from rtl_inv_sheet_subitems s,'
      '     rtl_inv_sheets ris, '
      '     egais_pref ep'
      'where s.doc_id = :DOC_ID'
      'and s.line_id = :LINE_ID'
      'and ris.doc_id = s.doc_id'
      'and ep.alccode(+) = s.alccode'
      ') v'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 448
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_alcID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_alcLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'LINE_ID'
    end
    object m_Q_alcSUB_LINE_ID: TFloatField
      FieldName = 'SUB_LINE_ID'
      Origin = 'SUB_LINE_ID'
    end
    object m_Q_alcPDF_BAR_CODE: TStringField
      DisplayLabel = '��� �������� �����'
      FieldName = 'PDF_BAR_CODE'
      Origin = 'PDF_BAR_CODE'
      Size = 100
    end
    object m_Q_alcALCCODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object m_Q_alcQNT: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QNT'
      Origin = 'QNT'
    end
    object m_Q_alcFULLNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      Size = 255
    end
    object m_Q_alcDUBL_PDF_CODE: TStringField
      DisplayLabel = '������������ �������� �����'
      DisplayWidth = 25
      FieldName = 'DUBL_PDF_CODE'
      Origin = 'DUBL_PDF_CODE'
      FixedChar = True
      Size = 2
    end
  end
  object m_Q_alccode: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alccodeBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '
      '  t.pref_alccode,'
      '  t.name'
      'FROM '
      '('
      'SELECT '
      
        '      DECODE(ep.alccode,'#39'XXXXXXXXXXXXXXXXXXX'#39','#39'-1'#39',ep.alccode) a' +
        's pref_alccode,'
      '      ep.fullname as name'
      '    FROM egais_pref ep'
      '    WHERE exists('
      
        '      SELECT null FROM egais_nmcl ne WHERE ne.nmcl_id = :NMCL_ID' +
        ' AND ne.assortment_id = :ASSORTMENT_ID AND ne.pref_alccode = ep.' +
        'alccode'
      '      UNION ALL'
      
        '      SELECT null FROM nmcl_alcosets na, egais_nmcl ne WHERE na.' +
        'main_nmcl_id = :NMCL_ID AND :ASSORTMENT_ID = 7 AND ne.nmcl_id = ' +
        'na.nmcl_id AND ne.pref_alccode = ep.alccode'
      '    ) '
      '    OR ep.alccode = '#39'XXXXXXXXXXXXXXXXXXX'#39
      ') t'
      '%WHERE_EXPRESSION'
      'ORDER BY name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 240
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end>
    object m_Q_alccodePREF_ALCCODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'PREF_ALCCODE'
      Origin = 'PREF_ALCCODE'
      Size = 100
    end
    object m_Q_alccodeNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 255
    end
  end
  object m_Q_pdf_bar_code: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcBeforeOpen
    AfterInsert = m_Q_alcAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'insert into rtl_inv_sheet_subitems (doc_id, line_id, sub_line_id' +
        ', pdf_bar_code, alccode, qnt)'
      
        'values (:DOC_ID, :LINE_ID, sq_rtl_inv_sheet_subitems.nextval, :P' +
        'DF_BAR_CODE, pkg_bar_code.get_egais_alccode(:PDF_BAR_CODE), 1)')
    Macros = <>
    Left = 248
    Top = 360
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end>
  end
  object m_kod_snnum_chek: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select barcod as PDF_BARCODE'
      'from'
      '(select PDF_BARCODE as barcod'
      'from  DOC_EGAIS_BARCODE_ITEMS'
      'where '
      '       type_id = :type_id and '
      '       rank = :rank and '
      '       line_number = :line_number and'
      '       PDF_BARCODE is not null'
      'INTERSECT'
      'select PDF_BARCODE as barcod'
      '  from rtl_egais_barcode_items)')
    Macros = <>
    Left = 520
    Top = 376
    ParamData = <
      item
        DataType = ftInteger
        Name = 'type_id'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'rank'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'line_number'
        ParamType = ptInput
      end>
    object m_kod_snnum_chekPDF_BARCODE: TStringField
      FieldName = 'PDF_BARCODE'
      Origin = 'DOC_EGAIS_BARCODE_ITEMS.PDF_BARCODE'
      Size = 255
    end
  end
  object m_Q_get_alccode: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select pkg_bar_code.get_egais_alccode(:PDF_BAR_CODE) as alccode,'
      
        '           pkg_bar_code.fnc_get_PDF_CODE(:PDF_BAR_CODE)AS PDF_BA' +
        'R_CODE'
      ' from dual'
      '')
    Macros = <>
    Left = 624
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end>
    object m_Q_get_alccodeALCCODE: TMemoField
      FieldName = 'ALCCODE'
      BlobType = ftMemo
      Size = 4000
    end
    object m_Q_get_alccodePDF_BAR_CODE: TMemoField
      FieldName = 'PDF_BAR_CODE'
      BlobType = ftMemo
      Size = 4000
    end
  end
  object m_Q_ins_alco: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'insert into rtl_inv_sheet_subitems (doc_id, line_id, sub_line_id' +
        ', pdf_bar_code, alccode, '
      'qnt)'
      
        'values (:ID, :LINE_ID, sq_rtl_inv_sheet_subitems.nextval, :PDF_B' +
        'AR_CODE,  :ALCCODE , :QNT )'
      '')
    Macros = <>
    Left = 424
    Top = 439
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ALCCODE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'QNT'
        ParamType = ptInput
      end>
    object m2: TFloatField
      FieldName = 'TYPE_ID'
      Origin = 'TSDBMAIN.EGAIS_KOD_FSM.ID'
    end
    object m3: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.EGAIS_KOD_FSM.NAME'
      Size = 200
    end
    object m4: TStringField
      FieldName = 'VALUE'
      Size = 32
    end
  end
  object m_alco_nmcl_chek: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select max(pref_alccode) as alco  from egais_nmcl'
      'where '
      '       nmcl_id = :nmcl_id and '
      '      assortment_id = :assortment_id and '
      '      pref_alccode = :pref_alccode ')
    Macros = <>
    Left = 520
    Top = 480
    ParamData = <
      item
        DataType = ftInteger
        Name = 'nmcl_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'assortment_id'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'pref_alccode'
        ParamType = ptInput
      end>
    object m_alco_nmcl_chekALCO: TStringField
      FieldName = 'ALCO'
      Size = 50
    end
  end
  object m_type_barcod: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id,'
      '         value,'
      '         name'
      'from   DOC_EGAIS_BARCODE_ITEMS, EGAIS_kod_fsm'
      'where '
      '       id = type_id and '
      '       rank = :rank and '
      '       line_number = :line_number'
      '      and PDF_BARCODE is not null')
    Macros = <>
    Left = 520
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'rank'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'line_number'
        ParamType = ptInput
      end>
    object m_type_barcodID: TFloatField
      FieldName = 'ID'
      Origin = 'TOPT1.EGAIS_KOD_FSM.ID'
    end
    object m_type_barcodVALUE: TStringField
      FieldName = 'VALUE'
      Size = 5
    end
    object m_type_barcodNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TOPT1.EGAIS_KOD_FSM.NAME'
      Size = 200
    end
  end
  object m_Q_get_kod_fsm: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id as TYPE_ID,'
      '         value,'
      '         name'
      'from   EGAIS_kod_fsm'
      'where '
      '       VALUE = :VALUE '
      '')
    Macros = <>
    Left = 416
    Top = 383
    ParamData = <
      item
        DataType = ftString
        Name = 'VALUE'
        ParamType = ptInput
      end>
    object m_Q_get_kod_fsmTYPE_ID: TFloatField
      FieldName = 'TYPE_ID'
      Origin = 'TOPT1.EGAIS_KOD_FSM.ID'
    end
    object m_Q_get_kod_fsmVALUE: TStringField
      FieldName = 'VALUE'
      Origin = 'TOPT1.EGAIS_KOD_FSM.NAME'
      Size = 5
    end
    object m_Q_get_kod_fsmNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TOPT1.EGAIS_KOD_FSM.NAME'
      Size = 200
    end
  end
  object m_Q_del_dubl_bar_code: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '  pkg_rtl_inv.prc_del_dubl_pdf_code(:inv_sheet_doc_id);'
      'end;  ')
    Macros = <>
    Left = 184
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'inv_sheet_doc_id'
        ParamType = ptInput
      end>
  end
end
