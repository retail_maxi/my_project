inherited FNmcls: TFNmcls
  Left = 339
  Top = 232
  BorderStyle = bsSingle
  Caption = '����� ������������'
  ClientHeight = 555
  ClientWidth = 759
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 503
    Width = 759
    Height = 33
    object m_SBTN_select_all: TSpeedButton [0]
      Left = 2
      Top = 2
      Width = 97
      Height = 25
      Action = m_ACT_select_all
    end
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 565
      Height = 33
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 468
      Height = 33
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 536
    Width = 759
  end
  inherited m_P_main_top: TPanel
    Width = 759
    inherited m_P_tb_main_add: TPanel
      Left = 712
    end
    inherited m_P_tb_main: TPanel
      Width = 712
      inherited m_TB_main: TToolBar
        Width = 712
        object m_TBTN_sep4: TToolButton
          Left = 183
          Top = 2
          Width = 8
          Caption = 'm_TBTN_sep4'
          ImageIndex = 11
          Style = tbsSeparator
        end
        object m_CB_nonzero: TCheckBox
          Left = 191
          Top = 2
          Width = 169
          Height = 24
          Caption = '��� ������� ��������'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          OnClick = m_CB_nonzeroClick
        end
      end
    end
  end
  object m_PC_nmcl: TPageControl [3]
    Left = 0
    Top = 26
    Width = 759
    Height = 477
    ActivePage = m_TS_goods
    Align = alClient
    TabOrder = 3
    OnChange = m_PC_nmclChange
    object m_TS_goods: TTabSheet
      Caption = '������'
      object m_DBG_nmcls: TMLDBGrid
        Left = 0
        Top = 0
        Width = 751
        Height = 449
        Align = alClient
        DataSource = m_DS_nmcls
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        TitleLines = 1
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'GROUP_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'GNAME'
            Title.TitleButton = True
            Width = 100
            Footers = <>
          end
          item
            FieldName = 'CAT_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'CTNAME'
            ReadOnly = True
            Title.TitleButton = True
            Width = 100
            Footers = <>
          end
          item
            FieldName = 'SUBCAT_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'SUBCAT_NAME'
            Title.TitleButton = True
            Width = 100
            Footers = <>
          end
          item
            FieldName = 'NMCL_ID'
            ReadOnly = True
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_NAME'
            ReadOnly = True
            Title.TitleButton = True
            Width = 200
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_ID'
            Title.TitleButton = True
            Width = 98
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_NAME'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'MEAS_NAME'
            ReadOnly = True
            Title.TitleButton = True
            Width = 76
            Footers = <>
          end
          item
            FieldName = 'OK'
            Title.TitleButton = True
            Width = 50
            KeyList.Strings = (
              'Y'
              'N')
            Checkboxes = True
            Footers = <>
          end>
      end
    end
    object m_TS_invs: TTabSheet
      Caption = '����������� ���������'
      ImageIndex = 1
      object m_DBG_nmcls_i: TMLDBGrid
        Left = 0
        Top = 0
        Width = 751
        Height = 449
        Align = alClient
        DataSource = m_DS_nmcls_i
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        TitleLines = 1
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'PT_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'PT_NAME'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_NAME'
            Title.TitleButton = True
            Width = 137
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_ID'
            Title.TitleButton = True
            Width = 96
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_NAME'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'MEAS_NAME'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'OK'
            Title.TitleButton = True
            KeyList.Strings = (
              'Y'
              'N')
            Checkboxes = True
            Footers = <>
          end>
      end
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_select_all: TAction
      Category = 'Nmcls'
      Caption = '�������� ���'
      Hint = '�������� ��� �����������'
      OnExecute = m_ACT_select_allExecute
      OnUpdate = m_ACT_select_allUpdate
    end
  end
  inherited m_IL_main_add: TImageList
    Left = 104
    Top = 65528
  end
  inherited m_ACTL_main_add: TActionList
    Left = 136
  end
  inherited m_PM_main_add: TPopupMenu
    Left = 256
  end
  object m_DS_nmcls: TDataSource
    DataSet = DInvAct.m_Q_nmcls
    Left = 8
    Top = 40
  end
  object m_DS_nmcls_i: TDataSource
    DataSet = DInvAct.m_Q_nmcls_i
    Left = 52
    Top = 42
  end
end
