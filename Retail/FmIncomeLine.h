//---------------------------------------------------------------------------

#ifndef FmIncomeLineH
#define FmIncomeLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmIncome.h"
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "FrmShelfLifeLists.h"
#include "FrmRtlIncomeLine.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Dialogs.hpp>
#include "DLBCScaner.h"
//---------------------------------------------------------------------------
class TFIncomeLine : public TTSFDocumentLine
{
__published:
  TLabel *m_L_qnt;
  TDBEdit *m_DBE_qnt;
  TMLDBPanel *m_DBP_tax_rate;
  TLabel *m_L_tax_rate;
  TDataSource *m_DS_tax_rate;
  TLabel *Label5;
  TDBEdit *m_DBE_in_price;
  TLabel *Label7;
  TDBEdit *m_DBE_out_price;
  TLabel *Label9;
  TDBEdit *m_DBE_amount_out;
  TPageControl *m_PC_bottom;
  TTabSheet *m_TS_extra;
  TMLLov *m_LOV_countries;
  TDataSource *m_DS_countries;
  TLabel *m_L_country_name;
  TMLLovListView *m_LV_countries;
  TLabel *m_L_num_gtd;
  TDBEdit *m_DBE_num_gtd;
  TMLLov *m_LOV_reason;
  TDataSource *m_DS_reasons;
  TLabel *m_L_reason;
  TMLLovListView *m_LV_reason;
  TLabel *m_L_note;
  TDBEdit *m_DBE_note;
  TLabel *Label6;
  TDBEdit *m_DBE_increase_prc;
  TLabel *m_L_src_amount;
  TDBEdit *m_DBE_src_amount;
  TTabSheet *m_TS_shelf_life_;
  TFRtlIncomeLine *m_FRM_rtl_line;
  TFRShelfLifeLists *m_FRM_shelf_life;
  TLabel *Label1;
  TDBEdit *m_DBE_bc_note;
  TToolButton *ToolButton1;
  TToolButton *ToolButton2;
  TCheckBox *m_CB_view_only_open;
  TLabel *Label2;
  TDBEdit *m_DBE_curr_rest;
  TTabSheet *m_TS_bar_codes;
  TMLDBGrid *m_MLDBG_bar_codes;
  TDataSource *m_DS_bar_codes;
        TDataSource *m_DS_make_date;
        TPanel *m_P_wei;
        TLabel *Label3;
        TDBDateEdit *m_DBE_make_date;
        TDateTimePicker *m_DTP_make_time;
        TTabSheet *m_TS_sublines;
        TMLDBGrid *m_MLDBG_sublines;
        TDataSource *m_DS_sublines;
        TDLBCScaner *m_BCS_subitem;
        TPanel *m_P_lines_control;
        TToolBar *m_TB_lines_control_button;
        TSpeedButton *m_SBTN_delete;
        TAction *m_ACT_delete;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_DBE_qntExit(TObject *Sender);
  void __fastcall m_DBE_src_amountExit(TObject *Sender);
  void __fastcall m_DBE_increase_prcExit(TObject *Sender);
  void __fastcall m_DBE_in_priceExit(TObject *Sender);
  void __fastcall m_DBE_out_priceExit(TObject *Sender);
  void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
  void __fastcall m_FRM_rtl_linem_LOV_nmclsAfterApply(TObject *Sender);
  void __fastcall m_FRM_rtl_linem_LOV_nmclsAfterClear(TObject *Sender);
  void __fastcall m_FRM_rtl_linem_LOV_assortAfterApply(TObject *Sender);
  void __fastcall m_FRM_rtl_linem_LOV_assortAfterClear(TObject *Sender);
  void __fastcall m_CB_view_only_openClick(TObject *Sender);
        void __fastcall m_DTP_make_timeChange(TObject *Sender);
        void __fastcall m_ACT_apply_updatesUpdate(TObject *Sender);
        void __fastcall m_BCS_subitemBegin(TObject *Sender);
        void __fastcall m_BCS_subitemEnd(TObject *Sender);
        void __fastcall m_MLDBG_sublinesEnter(TObject *Sender);
        void __fastcall m_MLDBG_sublinesExit(TObject *Sender);
        void __fastcall m_ACT_deleteExecute(TObject *Sender);
        void __fastcall m_ACT_deleteUpdate(TObject *Sender);
private:
  enum RecalcKind {rkAll, rkOutPrice, rkOutSum, rkInPrice, rkInSum, rkPercent};
  TDIncome* m_dm;
  bool m_calc_flag;
  TMLGridOptions m_old_sublines_grid_options;

  virtual AnsiString __fastcall BeforeLineApply(TWinControl *p_focused);
  void __fastcall CalcValues(RecalcKind p_kind);
  bool __fastcall CheckShelfHours();
public:
  __fastcall TFIncomeLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
  __fastcall ~TFIncomeLine();  
};
//---------------------------------------------------------------------------
#endif
