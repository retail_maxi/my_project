//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmMessageFromRKU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TDMessageFromRKU::TDMessageFromRKU(TComponent* p_owner,
                                              AnsiString p_prog_id)
    : TTSDDocument(p_owner,p_prog_id)
{
}
//---------------------------------------------------------------------------

void __fastcall TDMessageFromRKU::m_Q_itemBeforeClose(TDataSet *DataSet)
{
  if( (UpdateRegimeItem == urInsert) && (!ChangingItem) )
    FreeDocNumber(DocTypeId,m_Q_itemDOC_NUMBER->AsString);    
}
//---------------------------------------------------------------------------

void __fastcall TDMessageFromRKU::m_Q_itemAfterInsert(TDataSet *DataSet)
{
    TTSDDocument::m_Q_itemAfterInsert(DataSet);
    m_Q_itemREAD_USER_ID->AsInteger = GetCntId();
    m_Q_itemDOC_NUMBER->AsInteger = StrToInt64(GetDocNumber(DocTypeId));
    m_Q_itemDOC_DATE->AsDateTime = GetSysDate();
}
//---------------------------------------------------------------------------

int __fastcall TDMessageFromRKU::GetCntId()
{
    if (m_Q_get_cnt_id->Active) m_Q_get_cnt_id->Close();
    m_Q_get_cnt_id->Open();
    return m_Q_get_cnt_idCNT_ID->AsInteger;
}
//---------------------------------------------------------------------------
