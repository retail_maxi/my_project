//---------------------------------------------------------------------------

#ifndef FmDocRtlCouponDiscItemH
#define FmDocRtlCouponDiscItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmDocRtlCouponDisc.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "FmDocRtlCouponDiscLine.h"
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Mask.hpp>
#include "TSPanelContainer.h"
#include "RXCtrls.hpp"
#include "DLBCScaner.h"
#include <DBCtrls.hpp>
#include "TSFormControls.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFDocRtlCouponDiscItem : public TTSFDocumentWithLines
{
__published:
  TAction *m_ACT_get_acc_doc;
  TAction *m_ACT_set_not_include_act_verification;
  TAction *m_ACT_set_include_act_verification;
  TPopupMenu *m_PM_include_act_verif;
  TMenuItem *N2;
  TMenuItem *N1;
  TAction *m_ACT_load_act_tcd;
  TAction *m_ACT_clear_act_tcd;
  TPopupMenu *m_PM_act_tcd;
  TMenuItem *m_MI_load_act_tcd;
  TMenuItem *m_MI_clear_act_tcd;
  TAction *m_ACT_set_corr_amt;
    TAction *m_ACT_view_source;
        TMenuItem *m_MI_add_bill;
        TAction *m_ACT_rtl_bill_link;
        TPanel *m_P_schet;
        TAction *m_ACT_get_schet_doc;
   TAction *m_ACT_income_diff;
  TAction *m_ACT_sign;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TDBEdit *m_DOC_number;
        TDBEdit *m_doc_date;
        TLabel *Label4;
        TDBDateEdit *m_DBE_begin_date;
        TDBDateEdit *m_DBE_end_date;
        TLabel *Label5;
        TDataSource *m_DS_org;
        TLabel *Label6;
        TDBMemo *m_DBM_note;
        TMLDBGrid *m_DBG_liness;
   TDBEdit *m_DBE_orgs;
   TBitBtn *btnACT_edit_kpp_num;
   TAction *m_ACT_sel_orgs;
  void __fastcall FormShow(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall m_ACT_sel_orgsExecute(TObject *Sender);
   void __fastcall m_ACT_sel_orgsUpdate(TObject *Sender);
private:
  TDDocRtlCouponDisc* m_dm;
  TS_FORM_LINE_SHOW(TFDocRtlCouponDiscLine)
  AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
public:
  __fastcall TFDocRtlCouponDiscItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm)
    : TTSFDocumentWithLines(p_owner,p_dm),
    m_dm(static_cast<TDDocRtlCouponDisc*>(p_dm))
    {};

};
//---------------------------------------------------------------------------
#endif
