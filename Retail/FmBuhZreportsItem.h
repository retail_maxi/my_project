//---------------------------------------------------------------------------

#ifndef FmBuhZreportsItemH
#define FmBuhZreportsItemH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENT.h"
#include "DmBuhZreports.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
#include "RXCtrls.hpp"
//---------------------------------------------------------------------------

class TFBuhZreportsItem : public TTSFDocument
{
__published:
   TPanel *m_P_sprm;
   TDataSource *m_DS_srpm;
   TPanel *m_P_header;
   TLabel *m_L_taxpayer;
   TMLDBPanel *m_DBP_TAXPAYER_NAME;
   TLabel *m_L_rep_sum;
   TMLDBPanel *m_DBP_REPORT_SUM;
   TLabel *m_L_sprm_sum;
   TMLDBPanel *m_DBP_SRPM_SUM;
   TLabel *m_L_tax10;
   TDBEdit *m_DBE_REPORT_10_SUM;
   TLabel *m_L_tax18;
   TDBEdit *m_DBE_REPORT_18_SUM;
   TAction *m_ACT_show_ground;
   TLabel *m_L_4;
        TMLDBPanel *m_P_REPORT_DATE;
   TLabel *m_L_5;
   TMLDBPanel *m_DBP_FISCAL_NUM;
   TLabel *m_L_6;
   TMLDBPanel *m_DBP_REPORT_NUM;
   TBitBtn *m_BBTN_close1;
    TLabel *m_L_stock;
    TMLDBPanel *m_DBP_stock_name;
        TLabel *Label1;
        TMLDBPanel *MLDBPanel1;
        TLabel *Label2;
        TDBEdit *m_DBE_REPORT_0_SUM;
        TPageControl *m_PC_spec;
        TTabSheet *m_TS_srpm;
        TMLDBGrid *m_DBG_srpm;
        TTabSheet *m_TS_ret;
        TMLDBGrid *MLDBGrid1;
        TDataSource *m_DS_ret;
        TLabel *m_L_ret_sum;
        TDBEdit *m_DBE_RET_SUM;
        TToolBar *m_TB_fldr_next;
        TRxSpeedButton *m_RSB_fldr_next;
        TPopupMenu *m_PM_fldr_next;
   TLabel *m_L_no_cash_sum;
   TMLDBPanel *m_DBP_no_cash_sum;
   void __fastcall m_ACT_show_groundExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:
  TDBuhZreports *m_dm;
  void __fastcall PMFldrNextClick(TObject *Sender);
public:
  __fastcall TFBuhZreportsItem(TComponent *p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

