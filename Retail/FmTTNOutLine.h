//---------------------------------------------------------------------------

#ifndef FmTTNOutLineH
#define FmTTNOutLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmTTNOut.h"
#include <Dialogs.hpp>
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFTTNOutLine : public TTSFDocumentLine
{
__published:
        TLabel *m_L_nmcl;
        TMLDBPanel *m_DBP_nmcl;
        TMLDBPanel *m_DBP_assort;
        TLabel *m_L_qnt;
        TDBEdit *m_DBE_qnt;
        TLabel *m_L_assort;
private:
  TDTTNOut *m_dm;
protected:
  virtual AnsiString __fastcall BeforeLineApply(TWinControl *p_focused);
public:
  __fastcall TFTTNOutLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm)
                         : TTSFDocumentLine(p_owner,p_dm),
                         m_dm(static_cast<TDTTNOut*>(p_dm))
                         {}
};
//---------------------------------------------------------------------------
#endif
