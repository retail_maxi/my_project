inherited FDocDiscGoodsItem: TFDocDiscGoodsItem
  Left = 508
  Top = 259
  Width = 1009
  Height = 705
  ActiveControl = m_DBDE_begin
  Caption = 'FDocDiscGoodsItem'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 65
    Top = 120
    Width = 39
    Height = 13
    Caption = '������'
  end
  inherited m_P_main_control: TPanel
    Top = 619
    Width = 993
    Height = 29
    AutoSize = False
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 896
      Height = 29
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 605
      Height = 29
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 196
      Width = 195
      Height = 29
      Visible = False
      inherited m_SBTN_next_fldr: TSpeedButton
        Width = 98
      end
    end
    object m_TB_next: TToolBar
      Left = 498
      Top = 0
      Width = 107
      Height = 29
      Align = alRight
      AutoSize = True
      ButtonHeight = 25
      EdgeBorders = []
      TabOrder = 3
      Wrapable = False
      object m_RSB_fldr_next: TRxSpeedButton
        Left = 0
        Top = 2
        Width = 107
        Height = 25
        DropDownMenu = m_PM_fldr_next
        Caption = '���������'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDD
          DDDDDDDDDD0900DDDDDDDDDDDD099900DDDD00000009999900DD099999999999
          990000000009999900DDDDDDDD099900DDDDDDDDDD0900DDDDDDDDDDDD00DDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        Layout = blGlyphLeft
      end
    end
    object m_TB_prev: TToolBar
      Left = 391
      Top = 0
      Width = 107
      Height = 29
      Align = alRight
      AutoSize = True
      ButtonHeight = 25
      EdgeBorders = []
      TabOrder = 4
      Wrapable = False
      object m_RSB_fldr_prev: TRxSpeedButton
        Left = 0
        Top = 2
        Width = 107
        Height = 25
        DropDownMenu = m_PM_fldr_prev
        Caption = '���������'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000000000000000FF000000FF000000FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000000000000000FF000000FF000000FF000000FF000000FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
          FF000000FF000000FF000000FF000000FF000000FF0000000000FF00FF00FF00
          FF0000000000000000000000FF000000FF000000FF000000FF000000FF000000
          0000000000000000000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF0000000000000000000000FF000000FF000000FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        Layout = blGlyphLeft
      end
    end
    object m_TB_left_bottom: TToolBar
      Left = 0
      Top = 0
      Width = 131
      Height = 29
      Align = alLeft
      AutoSize = True
      ButtonHeight = 25
      Caption = 'm_TB_main_control_buttons_document'
      EdgeBorders = []
      TabOrder = 5
      Wrapable = False
      object m_SB_add_all_rtt: TSpeedButton
        Left = 0
        Top = 2
        Width = 131
        Height = 25
        Action = m_ACT_add_all_rtt
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000084
          000000840000008400000084000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000084
          000000FF000000FF00000084000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000084000000840000000000000084
          000000FF000000FF00000084000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000000000000000000000000000000000000084
          000000FF000000FF000000840000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF0000000000C6C6C6000084000000840000008400000084
          000000FF000000FF000000840000008400000084000000840000FF00FF000000
          0000000000000000000000000000C6C6C60000FF000000FF000000FF000000FF
          000000FF000000FF000000FF000000FF000000FF000000FF0000FF00FF000000
          0000C6C6C6000084000000000000C6C6C60000FF000000FF000000FF000000FF
          000000FF000000FF000000FF000000FF000000FF000000FF0000FF00FF000000
          0000C6C6C60000FF000000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C60000FF000000FF000000840000008400000084000000840000FF00FF000000
          0000C6C6C60000FF00000000000000000000000000000000000000000000C6C6
          C60000FF000000FF000000840000000000000000000000000000FF00FF000000
          0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FF000000000000C6C6
          C60000FF000000FF0000008400000000000000000000FF00FF00FF00FF000000
          000000000000000000000000000000000000C6C6C60000FF000000000000C6C6
          C60000FF000000FF0000008400000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C60000FF000000000000C6C6
          C600C6C6C600C6C6C600C6C6C60000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C60000FF0000000000000000
          000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C600C6C6C600C6C6C600C6C6
          C60000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 648
    Width = 993
  end
  inherited m_P_main_top: TPanel
    Width = 993
    inherited m_P_tb_main: TPanel
      Width = 946
      inherited m_TB_main: TToolBar
        Width = 946
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 946
    end
  end
  inherited m_P_header: TPanel
    Width = 993
    Height = 247
    object Label1: TLabel
      Left = 28
      Top = 12
      Width = 71
      Height = 13
      Caption = '� ���������'
    end
    object Label2: TLabel
      Left = 239
      Top = 12
      Width = 12
      Height = 13
      Caption = '��'
    end
    object m_L_note: TLabel
      Left = 37
      Top = 173
      Width = 61
      Height = 13
      Caption = '����������'
      FocusControl = m_DBE_note
    end
    object Label4: TLabel
      Left = 34
      Top = 40
      Width = 63
      Height = 13
      Caption = '��������� �'
      FocusControl = m_DBDE_begin
    end
    object Label5: TLabel
      Left = 240
      Top = 40
      Width = 12
      Height = 13
      Caption = '��'
      FocusControl = m_DBDE_end
    end
    object Label6: TLabel
      Left = 244
      Top = 95
      Width = 30
      Height = 13
      Caption = '�����'
      FocusControl = m_LV_nmcls
    end
    object Label7: TLabel
      Left = 47
      Top = 70
      Width = 51
      Height = 13
      Caption = '��� �����'
      FocusControl = m_LV_acts
    end
    object Label8: TLabel
      Left = 35
      Top = 120
      Width = 65
      Height = 13
      Caption = '�����������'
      FocusControl = m_LV_assorts
    end
    object m_L_act_price: TLabel
      Left = 23
      Top = 198
      Width = 77
      Height = 13
      Caption = '���� �� �����'
      FocusControl = m_DBE_act_price
    end
    object m_L_price_bef_act: TLabel
      Left = 223
      Top = 222
      Width = 123
      Height = 13
      Caption = '���� �� ����� �� �����'
      FocusControl = m_DBE_out_price_nd
    end
    object m_L_disc_types: TLabel
      Left = 17
      Top = 146
      Width = 82
      Height = 13
      Caption = '������� ������'
      FocusControl = m_LV_disc_reasons
    end
    object m_L_pull_date: TLabel
      Left = 499
      Top = 198
      Width = 91
      Height = 13
      Caption = '���� �������� ��'
    end
    object m_L_1: TLabel
      Left = 417
      Top = 40
      Width = 39
      Height = 13
      Caption = '������'
    end
    object Label9: TLabel
      Left = 49
      Top = 96
      Width = 50
      Height = 13
      Caption = 'ID ������'
    end
    object m_L_out_price_nd: TLabel
      Left = 263
      Top = 198
      Width = 81
      Height = 13
      Caption = '���� ��� �����'
      FocusControl = m_DBE_out_price_nd
    end
    object m_L_cnt: TLabel
      Left = 743
      Top = 138
      Width = 57
      Height = 13
      Caption = '���������'
      FocusControl = m_DBE_out_price_nd
      Visible = False
    end
    object m_L_cnt_sale: TLabel
      Left = 743
      Top = 160
      Width = 74
      Height = 13
      Caption = '������ ������'
      FocusControl = m_DBE_out_price_nd
      Visible = False
    end
    object m_L_cnt_price_comp: TLabel
      Left = 743
      Top = 180
      Width = 263
      Height = 13
      Caption = '���� �������/��������� ��� ������� �����������'
      FocusControl = m_DBE_out_price_nd
      Visible = False
    end
    object m_L_cnt_comp_sale: TLabel
      Left = 744
      Top = 200
      Width = 196
      Height = 13
      Caption = '����������� � ��. ���������� ������'
      FocusControl = m_DBE_out_price_nd
      Visible = False
    end
    object m_L_cnt_method_comp: TLabel
      Left = 743
      Top = 222
      Width = 138
      Height = 13
      Caption = '�����/������ �����������'
      FocusControl = m_DBE_out_price_nd
      Visible = False
    end
    object m_DBE_note: TDBEdit
      Left = 109
      Top = 170
      Width = 621
      Height = 21
      DataField = 'NOTE'
      DataSource = m_DS_item
      TabOrder = 6
    end
    object m_DBE_doc_number: TMLDBPanel
      Left = 108
      Top = 8
      Width = 121
      Height = 21
      DataField = 'DOC_NUMBER'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 9
    end
    object MLDBPanel2: TMLDBPanel
      Left = 268
      Top = 8
      Width = 121
      Height = 21
      DataField = 'DOC_DATE'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 10
    end
    object m_DBDE_begin: TDBDateEdit
      Left = 108
      Top = 36
      Width = 121
      Height = 21
      DataField = 'BEGIN_DATE'
      DataSource = m_DS_item
      NumGlyphs = 2
      TabOrder = 0
    end
    object m_DBDE_end: TDBDateEdit
      Left = 268
      Top = 36
      Width = 121
      Height = 21
      DataField = 'END_DATE'
      DataSource = m_DS_item
      NumGlyphs = 2
      TabOrder = 1
    end
    object m_LV_nmcls: TMLLovListView
      Left = 284
      Top = 91
      Width = 621
      Height = 21
      Lov = m_LOV_nmcls
      UpDownWidth = 621
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 3
      ParentColor = False
    end
    object m_LV_acts: TMLLovListView
      Left = 108
      Top = 63
      Width = 621
      Height = 21
      Lov = m_LOV_acts
      UpDownWidth = 621
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 2
      ParentColor = False
    end
    object m_LV_assorts: TMLLovListView
      Left = 108
      Top = 118
      Width = 621
      Height = 21
      Lov = m_LOV_assorts
      UpDownWidth = 621
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 4
      ParentColor = False
    end
    object m_DBE_act_price: TDBEdit
      Left = 109
      Top = 195
      Width = 100
      Height = 21
      DataField = 'ACT_PRICE'
      DataSource = m_DS_item
      TabOrder = 7
    end
    object m_DBE_out_price_nd: TDBEdit
      Left = 365
      Top = 195
      Width = 116
      Height = 21
      DataField = 'PRICE_BEF_ACT'
      DataSource = m_DS_item
      TabOrder = 8
    end
    object m_LV_disc_reasons: TMLLovListView
      Left = 109
      Top = 144
      Width = 621
      Height = 21
      Lov = m_LOV_disc_reasons
      UpDownWidth = 621
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 5
      ParentColor = False
    end
    object m_P_DOC_DATE: TMLDBPanel
      Left = 464
      Top = 36
      Width = 265
      Height = 21
      DataField = 'CAT_MEN_NAME'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 11
    end
    object m_DBDE_PULL_DATE: TDBDateEdit
      Left = 604
      Top = 195
      Width = 121
      Height = 21
      DataField = 'PULL_DATE'
      DataSource = m_DS_item
      NumGlyphs = 2
      TabOrder = 12
    end
    object m_DBCB_supp_comp: TDBCheckBox
      Left = 744
      Top = 116
      Width = 217
      Height = 17
      Caption = '����������� �� ������� ����������'
      DataField = 'SUPP_COMP'
      DataSource = m_DS_item
      TabOrder = 13
      ValueChecked = '1'
      ValueUnchecked = '0'
      OnClick = m_DBCB_supp_compClick
    end
    object m_DBE_out_price: TDBEdit
      Left = 365
      Top = 218
      Width = 116
      Height = 21
      DataField = 'PRICE_BEF_ACT_ON_CARD'
      DataSource = m_DS_item
      TabOrder = 14
    end
    object m_DBE_cnt_sale: TDBEdit
      Left = 824
      Top = 158
      Width = 177
      Height = 21
      DataField = 'CNT_SALE'
      DataSource = m_DS_item
      TabOrder = 15
      Visible = False
    end
    object m_DBE_cnt_price_comp: TDBEdit
      Left = 1013
      Top = 176
      Width = 132
      Height = 21
      DataField = 'CNT_PRICE_COMP'
      DataSource = m_DS_item
      TabOrder = 16
      Visible = False
    end
    object m_LV_cnt: TMLLovListView
      Left = 805
      Top = 134
      Width = 412
      Height = 21
      Lov = m_LOV_cnt
      UpDownWidth = 621
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 17
      ParentColor = False
      Visible = False
    end
    object m_DBE_cnt_comp_sale: TDBEdit
      Left = 949
      Top = 198
      Width = 196
      Height = 21
      DataField = 'CNT_COMP_SALE'
      DataSource = m_DS_item
      TabOrder = 18
      Visible = False
    end
    object m_DBE_cnt_method_comp: TDBEdit
      Left = 896
      Top = 220
      Width = 249
      Height = 21
      DataField = 'CNT_METHOD_COMP'
      DataSource = m_DS_item
      TabOrder = 19
      Visible = False
    end
  end
  inherited m_DBG_lines: TMLDBGrid
    Left = 472
    Top = 368
    Width = 261
    Height = 65
    Align = alNone
    Visible = False
    Columns = <
      item
        FieldName = 'GRP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 103
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'LINE_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 91
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CAT_MEN_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CAT_MEN_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SEC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SEC_MNG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SEC_MNG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'STORE_ITEMS'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ITEMS_WEEK_1'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ITEMS_WEEK_2'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ITEMS_WEEK_3'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'IN_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'OUT_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'OUT_PRICE_ND'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'INC_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DAY_STORE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_lines_control: TPanel
    Top = 584
    Width = 993
    Height = 35
    Visible = False
    inherited m_TB_lines_control_button: TToolBar
      Height = 35
    end
  end
  object m_PC_lines: TPageControl [7]
    Left = 0
    Top = 273
    Width = 993
    Height = 311
    ActivePage = m_TS_RTT
    Align = alClient
    TabOrder = 6
    object m_TS_RTT: TTabSheet
      Caption = '������ ���'
      object Panel1: TPanel
        Left = 0
        Top = 248
        Width = 985
        Height = 35
        Align = alBottom
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 0
        object m_SB_view_nmcl_sales: TSpeedButton
          Left = 388
          Top = 2
          Width = 150
          Height = 25
          Action = m_ACT_view_nmcl_sales
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00C0C0C0000000000000000000000000000000
            00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000800000000000FFFFFF00FFFFFF00C0C0C000C0C0
            C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF00FF00FFFFFF000000
            0000FF00FF00FF00FF000000800000000000FFFFFF00FFFFFF00000000000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF00FF0000000000FFFF
            FF00FF00FF00FF00FF000000800000000000FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF00FF0000FFFF00FFFF
            FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
            FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF00FF0000000000FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FFFFFF00FF00FF00FF00FF000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
            FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FF00FF00FF00FF00FF00
            FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C0000000
            0000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FF00FF00FF00FF00FF00
            FF00FF00FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
            FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FF00FF00FF00FF00FF00
            FF00FF00FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
            0000FFFFFF0000000000FFFFFF00C0C0C000FFFFFF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000000000000000FFFFFF0000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000800000000000FFFFFF000000000000000000FFFF
            FF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000800000000000FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000080000000000000000000FFFFFF00000000000000
            0000FFFFFF000000000000000000FFFFFF0000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C0C0C000000000000000
            0000C0C0C0000000000000000000C0C0C00000000000FF00FF00}
        end
        object ToolBar1: TToolBar
          Left = 0
          Top = 0
          Width = 388
          Height = 35
          Align = alLeft
          AutoSize = True
          ButtonHeight = 25
          Caption = 'm_TB_lines_control_button'
          EdgeBorders = []
          TabOrder = 0
          object m_SB_append: TSpeedButton
            Left = 0
            Top = 2
            Width = 97
            Height = 25
            Action = m_ACT_append
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00000000000080000000800000008000000080
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00000000000080000000FF000000FF00000080
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00000000000080000000FF000000FF00000080
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
              0000000000000000000000000000000000000080000000FF000000FF00000080
              00000000000000000000000000000000000000000000FF00FF00FF00FF000000
              0000C0C0C0000080000000800000008000000080000000FF000000FF00000080
              00000080000000800000008000000080000000000000FF00FF00FF00FF000000
              0000C0C0C00000FF000000FF000000FF000000FF000000FF000000FF000000FF
              000000FF000000FF000000FF00000080000000000000FF00FF00FF00FF000000
              0000C0C0C00000FF000000FF000000FF000000FF000000FF000000FF000000FF
              000000FF000000FF000000FF00000080000000000000FF00FF00FF00FF000000
              0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C00000FF000000FF00000080
              00000080000000800000008000000080000000000000FF00FF00FF00FF000000
              000000000000000000000000000000000000C0C0C00000FF000000FF00000080
              00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF0000000000C0C0C00000FF000000FF00000080
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF0000000000C0C0C00000FF000000FF00000080
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF0000000000C0C0C000C0C0C000C0C0C000C0C0
              C00000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
              000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
          end
          object m_SB_edit: TSpeedButton
            Left = 97
            Top = 2
            Width = 97
            Height = 25
            Action = m_ACT_edit
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000FF00FF00FF00FF000000
              0000C0C0C0000080800000808000008080000080800000808000008080000080
              80000080800000808000008080000080800000000000FF00FF00FF00FF00FF00
              FF0000000000C0C0C00000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
              FF0000FFFF0000FFFF000080800000000000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF0000000000C0C0C00000FFFF0000FFFF0000FFFF0000FFFF0000FF
              FF0000FFFF000080800000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF0000000000C0C0C00000FFFF0000FFFF0000FFFF0000FF
              FF000080800000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF0000000000C0C0C00000FFFF0000FFFF000080
              800000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C0C0C000008080000000
              0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
          end
          object m_SB_delete: TSpeedButton
            Left = 194
            Top = 2
            Width = 97
            Height = 25
            Action = m_ACT_delete
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000FF00FF00FF00FF000000
              0000C0C0C0000000800000008000000080000000800000008000000080000000
              80000000800000008000000080000000800000000000FF00FF00FF00FF000000
              0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
              FF000000FF000000FF000000FF000000800000000000FF00FF00FF00FF000000
              0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
              FF000000FF000000FF000000FF000000800000000000FF00FF00FF00FF000000
              0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
              C000C0C0C000C0C0C000C0C0C000C0C0C00000000000FF00FF00FF00FF000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
          end
          object m_SB_view: TSpeedButton
            Left = 291
            Top = 2
            Width = 97
            Height = 25
            Action = m_ACT_view
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000FF00FF00FF00FF000000
              0000C0C0C0008080800080808000808080008080800080808000808080008080
              80008080800080808000808080008080800000000000FF00FF00FF00FF00FF00
              FF0000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF008080800000000000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF0000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF008080800000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF0000000000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFF
              FF008080800000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF0000000000C0C0C000FFFFFF00FFFFFF008080
              800000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C0C0C000808080000000
              0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
          end
        end
        object m_CB_urgent_reprice: TCheckBox
          Left = 846
          Top = 2
          Width = 137
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '������� ����������'
          TabOrder = 1
          OnClick = m_CB_urgent_repriceClick
        end
      end
      object m_DBG_rtt: TMLDBGrid
        Left = 0
        Top = 0
        Width = 985
        Height = 248
        Align = alClient
        DataSource = m_DS_lines
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        FooterRowCount = 1
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterColor = clWindow
        UseMultiTitle = True
        SumList.Active = True
        SumList.VirtualRecords = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        OnGetCellParams = m_DBG_rttGetCellParams
        Columns = <
          item
            FieldName = 'ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'LINE_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'GRP_NAME'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'ORG_NAME'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'MODEL_TYPE'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'NOTE'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'FACT_MARGIN_SUM'
            Title.TitleButton = True
            Width = 70
            Footers = <>
          end
          item
            FieldName = 'FACT_MARGIN'
            Title.TitleButton = True
            Width = 70
            Footers = <>
          end
          item
            FieldName = 'DAY_STORE'
            Title.TitleButton = True
            Width = 70
            Footers = <
              item
                ValueType = fvtSum
              end>
          end
          item
            FieldName = 'STORE_ITEMS'
            Title.TitleButton = True
            Width = 70
            Footers = <
              item
                ValueType = fvtSum
              end>
          end
          item
            FieldName = 'ITEMS_WEEK_1'
            Title.TitleButton = True
            Width = 70
            Footers = <
              item
                ValueType = fvtSum
              end>
          end
          item
            FieldName = 'ITEMS_WEEK_2'
            Title.TitleButton = True
            Width = 70
            Footers = <
              item
                ValueType = fvtSum
              end>
          end
          item
            FieldName = 'ITEMS_WEEK_3'
            Title.TitleButton = True
            Width = 70
            Footers = <
              item
                ValueType = fvtSum
              end>
          end
          item
            FieldName = 'CUR_PRICE_IN'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'OUT_PRICE'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'OUT_PRICE_ND'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'INC_DATE_RTT'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'INC_DATE_RC'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'ACTION_TYPE_NAME'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'ACTION_BEGIN_DATE'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'ACTION_END_DATE'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'ACTION_OUT_PRICE'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'PULL_DATE'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'TOTAL_PROFIT'
            Title.TitleButton = True
            Width = 80
            Footers = <
              item
                ValueType = fvtSum
              end>
          end
          item
            FieldName = 'MARGIN_AF_ACT'
            Title.TitleButton = True
            Width = 80
            Footers = <>
          end
          item
            FieldName = 'CAPACITY'
            Title.TitleButton = True
            Width = 75
            Footers = <>
          end>
      end
    end
  end
  object MLDBPanel1: TMLDBPanel [8]
    Left = 112
    Top = 116
    Width = 121
    Height = 21
    DataField = 'NMCL_ID'
    DataSource = m_DS_item
    Alignment = taLeftJustify
    BevelWidth = 0
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 7
  end
  inherited m_IL_main: TImageList
    Bitmap = {
      494C010117001800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000006000000001002000000000000060
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084000000FF000000FF
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000840000000000000084000000FF000000FF
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000084000000FF000000FF
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C6000084000000840000008400000084000000FF000000FF
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      000000000000C6C6C60000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000000000000000000000000000C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00848484000000000000000000000000000000000000000000C6C6C6000084
      000000000000C6C6C60000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400000000000000000000000000000000000000000000000000C6C6C60000FF
      000000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FF000000FF
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      000000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000C6C6C60000FF
      00000000000000000000000000000000000000000000C6C6C60000FF000000FF
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600FFFFFF00FFFFFF0084848400000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FF000000000000C6C6C60000FF000000FF
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C6008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000000000C6C6C60000FF000000FF
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000000000C6C6C600C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000C6C6C6000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      000000840000008400000000000000000000000000000000000000000000C6C6
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00008484000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      0000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000084
      840000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      000000000000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF00008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FF000000FF000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FFFF0000FFFF0000848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C6000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FF000000FF000000840000008400000084
      000000840000008400000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      840000000000FFFFFF0000000000000000008484840084848400848484000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFF0000848484008484
      8400848484008484840084848400848484008484840084848400000000008484
      0000FFFF0000FFFF0000FFFFFF00000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      000084840000848400008484000084840000848400000000000084840000FFFF
      0000FFFF0000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF000000000000000000000000008484840000000000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000848400008484000084840000000000000000000084840000FFFF0000FFFF
      0000FFFFFF000000000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      840000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFF000084840000000000008484000084840000FFFF0000FFFF0000FFFF
      FF00000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000000000
      FF000000FF000000FF00000000000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      8400FFFFFF008484840000000000000000008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000FFFF0000FFFF0000FFFF0000FFFFFF000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000840000000000FF000000
      FF000000FF000000FF0000000000000000000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      840000000000848484000000000000000000FFFFFF0084848400000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000084840000FFFF0000FFFF0000FFFF0000FFFFFF00000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000840000000000FF000000
      FF000000FF00000000000000000000000000000000000000FF000000FF000000
      FF000000000000000000000000000000000000000000FFFFFF00000000008484
      8400FFFFFF008484840000000000000000008484840084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000000000
      000084840000FFFF0000FFFF0000FFFFFF00FFFFFF0000000000848400008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000008484
      0000FFFF0000FFFF0000FFFFFF000000000000000000FFFF0000848400008484
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF0000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF0000000000848484000000000000000000000000000000000084840000FFFF
      0000FFFF0000FFFFFF0000000000000000000000000000000000FFFF00008484
      0000848400008484840000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF00000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000084848400848484008484
      840084848400848484000000FF000000FF008484840084848400000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484000000000000000000848484008484840084848400848484008484
      8400840000008400000084000000000000000000000084848400848484008484
      840084848400000000000000FF000000FF008484840084848400848484008484
      8400840000008400000084000000000000000000000084848400848484008484
      8400848484000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000000000000000000000000000
      0000848400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000848484000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF008484
      84000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000008484000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF000000000084848400000000000000000084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF000000
      00000000FF000000FF000000FF000000FF000000000084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF00000000008484
      84000000FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF000000
      00000000000000000000848400000000000000000000FFFFFF00000000000000
      000000000000000000000000000000000000000000008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000000000
      00000000FF000000FF000000FF000000FF000000FF008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400000000000000000084848400848484008484
      8400000000000000000000000000000000000000000000000000FFFFFF000000
      FF000000FF000000FF00848484000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF000000
      FF000000FF000000FF00848484000000FF000000FF000000FF00FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF0000000000848484000000000000000000FFFFFF00848484000000
      00000000000000000000000000000000000000000000FFFFFF000000FF000000
      FF000000FF000000FF0084848400000000000000FF000000FF000000FF00FFFF
      FF000000000084848400000000000000000000000000FFFFFF00000000000000
      000000000000000000008484840000000000000000000000000000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF000000FF000000
      FF000000FF000000FF0084848400000000000000FF000000FF000000FF00FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF008484840000000000000000008484840084848400FFFF
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF00000000008484840000000000FFFFFF000000FF000000FF000000
      FF00000000008484840000000000000000000000000000000000FFFFFF008484
      8400000000000000000084848400000000000000000084848400FFFFFF000000
      00000000000084000000000000000000000000000000000000000000FF000000
      FF000000FF00000000008484840000000000000000000000FF000000FF000000
      FF00000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF0000000000848484000000FF000000
      FF000000000084848400000000000000000000000000FFFFFF00000000008484
      840000000000000000000000000000000000000000008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      84000000000000000000000000000000000000000000848484000000FF000000
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF00848484000000FF000000
      FF000000FF008484840000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF00848484000000FF000000
      FF000000FF008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF000000000084848400000000000000
      FF000000FF000000FF00000000000000000000000000FFFFFF00000000008484
      840000000000000000000000000000000000000000008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      8400000000000000000000000000000000000000000084848400000000000000
      FF000000FF000000FF00000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000FF000000FF000000FF00000000000000000000000000FFFFFF008484
      8400FFFFFF00000000000000000000000000FFFFFF0084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000000000000000000000FFFFFF0084848400FFFFFF000000
      00000000FF000000FF000000FF00000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000848484000000FF00000000008400000084000000840000008400
      0000840000008400000000000000840000008400000084000000840000008400
      0000840000008400000000000000000000008400000084000000840000008400
      0000840000008400000000000000840000008400000084000000840000008400
      000084000000840000000000FF00000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008484840000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      84008484840000000000000000000000000000000000000000000000FF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000840000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      000000000000000000000000000000000000000000000000FF00000084000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400848484000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400000084008484840000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8400000084000000840000008400848484000000840000008400000084000000
      8400000084008484840000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400000000000000000000000000000000000000000000000000000000
      FF00000084000000840000008400000084000000840000008400000084000000
      8400848484000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000008484000000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084000000840000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF00000000000000000084840000FFFFFF00FFFFFF000000
      0000000000000000000084840000000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000000000000000000000000000FFFFFF008484
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000000000
      FF00000084000000840000008400848484000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      00008400000084000000840000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF000000000084848400000000000000000000000000000000000000FF000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400848484000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000000000000000FF000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400000084008484840000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000840000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      FF000000840000000000000000000000000000000000000000000000FF000000
      8400000084000000840000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000000000000000000084000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000840000008400000084000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000600000000100010000000000000300000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFF030000FFFFFFFFF8030000
      FFFFFFFFF8030000FFFFFFFFF0000000FFFF8001F00000008001800180000000
      8001C003800000008001E007800000008001F00F800000008001F81F80010000
      8001FC3F80010000FFFFFE7FF8030000FFFFFFFFF8030000FFFFFFFFF81F0000
      FFFFFFFFF81F0000FFFFFFFFFFFF0000FFFFFC00FFFFFFFFFFFFF000F81FFFFF
      FFFFC000F81FFFFFFFFF0000F81FFFFFFCFF0000F81F8001FC3F000080018001
      FC0F00008001C003000300008001E007000000008001F00F000300008001F81F
      FC0F00018001FC3FFC3F0003F81FFE7FFCFF0007F81FFFFFFFFF001FF81FFFFF
      FFFF007FF81FFFFFFFFF01FFFFFFFFFFFFFFFFFFFFFFFFFFF81FC007000FFFFF
      F81FBFEB000FFFFFF81F0005000FFFFFF81F7E31000FFF3F80017E35000FFC3F
      80010006000FF03F80017FEA000FC00080018014008F00008001C00A1144C000
      8001E0010AB8F03FF81FE007057CFC3FF81FF007FAFCFF3FF81FF003FDF8FFFF
      F81FF803FE04FFFFFFFFFFFFFFFFFFFFFFEAFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      F0E1FFF08FFF8FFF800100008C018C01000100008FFF88FF40018001FFFFF0FF
      2001C000FFFFF07F4001E0008FFF823F2003F00F8C0180014823F00F8FFF878F
      2003E007FFFFFFCF4513C003FFFFFFC728A381818FFF8FE3451303C08C018C01
      000307E08FFF8FFD0007FFFFFFFFFFFFFFC7E07BE07BF870FFC7E77BE77BF870
      FFD7F3F1F3F1F870801780018001800000330000000000004079441340134000
      201C202320232801400045134013441300032023000328034503451345034513
      28832023200328A3450340134003451328A328A328A328A34511451345114513
      00010003000100030007000700070007CFFFFFFFFFFFFFC787CFC001F83FFFC7
      83878031E00FFFD781038031C7C78017C00380318FE30033E00780019FF34779
      F00F80013FF92A1CF81F80013FF94400F81F8FF13FFF28A3F01F8FF13FFF4513
      E00F8FF13FC128A3C1078FF19FE14513C3838FF18FF128A3E7C38FF5C7C14513
      FFE38001E00D0003FFFFFFFFF83F000700000000000000000000000000000000
      000000000000}
  end
  inherited m_ACTL_main: TActionList
    Left = 32
    Top = 24
    inherited m_ACT_prev_fldr: TAction
      Visible = False
    end
    inherited m_ACT_next_fldr: TAction
      Visible = False
    end
    object m_ACT_add_all_rtt: TAction
      Category = 'Item'
      Caption = '�������� ��� ���'
      ImageIndex = 22
      OnExecute = m_ACT_add_all_rttExecute
      OnUpdate = m_ACT_add_all_rttUpdate
    end
    object m_ACT_add_prod_sale: TAction
      Caption = '��������...'
      ImageIndex = 12
    end
    object m_ACT_edit_prod_sale: TAction
      Caption = '��������...'
      ImageIndex = 19
    end
    object m_ACT_del_prod_sale: TAction
      Caption = '�������'
      ImageIndex = 20
    end
    object m_ACT_view_nmcl_sales: TAction
      Category = 'Item'
      Caption = '�������� ����./������'
      Hint = '�������� ���������� � ������'
      OnExecute = m_ACT_view_nmcl_salesExecute
      OnUpdate = m_ACT_view_nmcl_salesUpdate
    end
  end
  inherited m_DS_lines: TDataSource
    DataSet = DDocDiscGoods.m_Q_lines
    Left = 40
    Top = 360
  end
  inherited m_DS_item: TDataSource
    DataSet = DDocDiscGoods.m_Q_item
    Top = 16
  end
  object m_DS_nmcls: TDataSource
    DataSet = DDocDiscGoods.m_Q_nmcls
    Left = 364
    Top = 114
  end
  object m_LOV_nmcls: TMLLov
    DataFieldKey = 'NMCL_ID'
    DataFieldValue = 'NMCL_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_nmcls
    AutoOpenList = True
    OnAfterApply = m_LOV_nmclsAfterApply
    OnAfterClear = m_LOV_nmclsAfterClear
    Left = 412
    Top = 114
  end
  object m_DS_acts: TDataSource
    DataSet = DDocDiscGoods.m_Q_acts
    Left = 124
    Top = 90
  end
  object m_LOV_acts: TMLLov
    DataFieldKey = 'ACTION_TYPE_ID'
    DataFieldValue = 'ACTION_TYPE_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_acts
    AutoOpenList = True
    OnAfterClear = m_LOV_actsAfterClear
    Left = 156
    Top = 90
  end
  object m_DS_assorts: TDataSource
    DataSet = DDocDiscGoods.m_Q_assorts
    Left = 124
    Top = 142
  end
  object m_LOV_assorts: TMLLov
    DataFieldKey = 'ASSORTMENT_ID'
    DataFieldValue = 'ASSORTMENT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_assorts
    AutoOpenList = True
    Left = 156
    Top = 142
  end
  object m_DS_disc_reasons: TDataSource
    DataSet = DDocDiscGoods.m_Q_reasons
    Left = 124
    Top = 176
  end
  object m_LOV_disc_reasons: TMLLov
    DataFieldKey = 'REASON_ID'
    DataFieldValue = 'REASON_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_disc_reasons
    AutoOpenList = True
    Left = 172
    Top = 176
  end
  object m_PM_fldr_next: TPopupMenu
    Images = m_IL_main
    OnPopup = m_PM_fldr_nextPopup
    Left = 448
    Top = 449
  end
  object m_PM_fldr_prev: TPopupMenu
    Images = m_IL_main
    OnPopup = m_PM_fldr_prevPopup
    Left = 400
    Top = 449
  end
  object m_DS_cnt: TDataSource
    DataSet = DDocDiscGoods.m_Q_cnt
    Left = 1076
    Top = 152
  end
  object m_LOV_cnt: TMLLov
    DataFieldKey = 'CNT_ID'
    DataFieldValue = 'CNT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'CNT_ID'
    DataFieldValues = 'CNT_NAME'
    DataSourceList = m_DS_cnt
    AutoOpenList = True
    Left = 1116
    Top = 152
  end
end
