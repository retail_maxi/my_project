//---------------------------------------------------------------------------

#ifndef FmRtlPlanMarginSeedListH
#define FmRtlPlanMarginSeedListH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMREFBOOK.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmRtlPlanMarginSeed.h"
#include "TSFmRefbook.h"
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------

class TFRtlPlanMarginSeedList : public TTSFRefbook
{
__published:
        TAction *m_ACT_actived;
        TToolButton *ToolButton1;
        TPanel *m_P_filters;
        TDBEdit *m_DBE_note;
        TLabel *m_L_nmcl;
private:	// User declarations
  TDRtlPlanMarginSeed *m_dm;
public:
  __fastcall TFRtlPlanMarginSeedList(TComponent* p_owner,
                               TTSDRefbook *p_dm_refbook): TTSFRefbook(p_owner,
                                                                       p_dm_refbook),
                               m_dm(static_cast<TDRtlPlanMarginSeed*>(p_dm_refbook)) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
