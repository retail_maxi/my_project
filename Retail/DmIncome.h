//---------------------------------------------------------------------------

#ifndef DmIncomeH
#define DmIncomeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDIncome : public TTSDDocumentWithLines
{
__published:
  TStringField *m_Q_listDOC_NUMBER;
  TDateTimeField *m_Q_listDOC_DATE;
  TStringField *m_Q_listGDP_NAME;
  TFloatField *m_Q_listRECEIVER_ID;
  TStringField *m_Q_listRECEIVER_NAME;
  TDateTimeField *m_Q_listINC_STARTED;
  TDateTimeField *m_Q_listINC_FINISHED;
  TFloatField *m_Q_listAMOUNT_IN;
  TFloatField *m_Q_listAMOUNT_OUT;
  TStringField *m_Q_listNOTE;
  TStringField *m_Q_listACC_NOTE;
  TFloatField *m_Q_listCNT_ID;
  TStringField *m_Q_listCNT_NAME;
  TFloatField *m_Q_listMNGR_ID;
  TStringField *m_Q_listMNGR_NAME;
  TFloatField *m_Q_itemFLDR_ID;
  TFloatField *m_Q_itemSRC_ORG_ID;
  TStringField *m_Q_itemDOC_NUMBER;
  TDateTimeField *m_Q_itemDOC_DATE;
  TFloatField *m_Q_itemGR_DEP_ID;
  TStringField *m_Q_itemGDP_NAME;
  TFloatField *m_Q_itemRECEIVER_ID;
  TStringField *m_Q_itemRECEIVER_NAME;
  TDateTimeField *m_Q_itemINC_STARTED;
  TDateTimeField *m_Q_itemINC_FINISHED;
  TFloatField *m_Q_itemAMOUNT_IN;
  TFloatField *m_Q_itemAMOUNT_OUT;
  TStringField *m_Q_itemNOTE;
  TFloatField *m_Q_itemACC_DOC_ID;
  TStringField *m_Q_itemCNT_NAME;
  TStringField *m_Q_itemACC_NOTE;
  TStringField *m_Q_itemACC_DOC_NUMBER;
  TDateTimeField *m_Q_itemACC_DOC_DATE;
  TFloatField *m_Q_itemACC_DOC_AMOUNT;
  TStringField *m_Q_itemMNGR_NOTE;
  TFloatField *m_Q_itemCONTRACT_ID;
  TStringField *m_Q_itemCONTRACT_NOTE;
  TMLQuery *m_Q_nmcls;
  TFloatField *m_Q_nmclsID;
  TFloatField *m_Q_nmclsLINE_ID;
  TFloatField *m_Q_nmclsNMCL_ID;
  TStringField *m_Q_nmclsNMCL_NAME;
  TStringField *m_Q_nmclsMU_NAME;
  TFloatField *m_Q_nmclsQNT;
  TFloatField *m_Q_nmclsIN_PRICE;
  TFloatField *m_Q_nmclsOUT_PRICE;
  TStringField *m_Q_nmclsSTORED;
  TQuery *m_Q_orgs;
  TFloatField *m_Q_orgsID;
  TStringField *m_Q_orgsNAME;
  TMLUpdateSQL *m_U_fict_org;
  TMLQuery *m_Q_fict_org;
  TFloatField *m_Q_fict_orgID;
  TMLQuery *m_Q_gdp_names;
  TFloatField *m_Q_gdp_namesGD_ID;
  TStringField *m_Q_gdp_namesGD_NAME;
  TMLQuery *m_Q_dup_acc_doc;
  TFloatField *m_Q_dup_acc_docACC_DOC_ID;
  TMLQuery *m_Q_receivers;
  TFloatField *m_Q_receiversID;
  TStringField *m_Q_receiversNAME;
  TStringField *m_Q_receiversDEPARTMENT;
  TFloatField *m_Q_linesNMCL_ID;
  TStringField *m_Q_linesNMCL_NAME;
  TStringField *m_Q_linesMEAS_NAME;
  TFloatField *m_Q_linesASSORTMENT_ID;
  TStringField *m_Q_linesASSORTMENT_NAME;
  TFloatField *m_Q_linesIN_PRICE;
  TFloatField *m_Q_linesOUT_PRICE;
  TFloatField *m_Q_linesQNT;
  TStringField *m_Q_linesCOUNTRY_NAME;
  TStringField *m_Q_linesSCD_NUM;
  TStringField *m_Q_linesREASON_NAME;
  TStringField *m_Q_linesNOTE;
  TStringField *m_Q_linesSTORED_DESC;
  TFloatField *m_Q_linesSTORED;
  TFloatField *m_Q_linesSRC_AMOUNT;
  TFloatField *m_Q_lineNMCL_ID;
  TFloatField *m_Q_lineCARD_ID;
  TStringField *m_Q_lineNMCL_NAME;
  TFloatField *m_Q_lineASSORTMENT_ID;
  TStringField *m_Q_lineASSORTMENT_NAME;
  TStringField *m_Q_lineMU_NAME;
  TFloatField *m_Q_lineIN_PRICE;
  TFloatField *m_Q_lineOUT_PRICE;
  TFloatField *m_Q_lineQNT;
  TFloatField *m_Q_lineSRC_AMOUNT;
  TFloatField *m_Q_lineSTORED;
  TStringField *m_Q_lineNOTE;
  TFloatField *m_Q_lineNON_STORING_REASON_ID;
  TStringField *m_Q_lineREASON_NAME;
  TFloatField *m_Q_lineCOUNTRY_ID;
  TStringField *m_Q_lineCOUNTRY_NAME;
  TStringField *m_Q_lineSCD_NUM;
  TFloatField *m_Q_linesCOUNTRY_ID;
  TFloatField *m_Q_linesTAX_RATE;
  TFloatField *m_Q_linesNOT_INCLUDE_ACT_VERIF;
  TMLQuery *m_Q_nmcl_list;
  TFloatField *m_Q_nmcl_listNMCL_ID;
  TStringField *m_Q_nmcl_listNMCL_NAME;
  TStringField *m_Q_nmcl_listMU_NAME;
  TMLQuery *m_Q_assort;
  TFloatField *m_Q_assortID;
  TStringField *m_Q_assortNAME;
  TQuery *m_Q_tax_rate;
  TFloatField *m_Q_tax_rateVALUE;
  TFloatField *m_Q_lineOUT_AMOUNT;
  TFloatField *m_Q_linesOUT_AMOUNT;
  TMLQuery *m_Q_countries;
  TFloatField *m_Q_countriesID;
  TStringField *m_Q_countriesNAME;
  TMLQuery *m_Q_reasons;
  TFloatField *m_Q_reasonsID;
  TStringField *m_Q_reasonsNAME;
  TMLQuery *m_Q_nmcl_desc;
  TFloatField *m_Q_nmcl_descASSORTMENT_ID;
  TStringField *m_Q_nmcl_descASSORTMENT_NAME;
  TMLQuery *m_Q_get_gtd_list;
  TFloatField *m_Q_get_gtd_listCOUNTRY_ID;
  TStringField *m_Q_get_gtd_listNUM_GTD;
  TStringField *m_Q_get_gtd_listCOUNTRY_NAME;
  TMLQuery *m_Q_act_tcd;
  TFloatField *m_Q_act_tcdDOC_ID;
  TStringField *m_Q_act_tcdDOC_NUMBER;
  TDateTimeField *m_Q_act_tcdDOC_DATE;
  TDateTimeField *m_Q_act_tcdINC_STARTED;
  TDateTimeField *m_Q_act_tcdINC_FINISHED;
  TFloatField *m_Q_act_tcdRECEIVER_ID;
  TStringField *m_Q_act_tcdRECEIVER_NAME;
  TStringField *m_Q_act_tcdNOTE;
  TStringField *m_Q_act_tcdOK;
  TMLUpdateSQL *m_U_act_tcd;
  TQuery *m_Q_load_act_tcd;
  TQuery *m_Q_clear_act_tcd;
  TFloatField *m_Q_lineINCREASE_PRC;
  TQuery *m_Q_prices;
  TFloatField *m_Q_pricesIN_PRICE;
  TFloatField *m_Q_pricesOUT_PRICE;
  TFloatField *m_Q_itemCORR_AMT;
  TFloatField *m_Q_listCORR_AMT;
  TFloatField *m_Q_linesPERCENT;
  TFloatField *m_Q_listACC_DOC_ID;
  TStringField *m_Q_listCNT_DOC_NUMBER;
  TFloatField *m_Q_itemSUM_WITH_CORR;
  TMLQuery *m_Q_check_rtl_inc_nmcl;
  TStringField *m_Q_check_rtl_inc_nmclRES;
  TStringField *m_Q_lineBAR_CODE_NOTE;
  TMLQuery *m_Q_nmcl_info;
  TMLQuery *m_Q_prc_fill_data_4_print_labels;
  TFloatField *m_Q_pricesCUR_REST;
  TMLQuery *m_Q_bar_codes;
  TFloatField *m_Q_bar_codesID;
  TStringField *m_Q_bar_codesBAR_CODE;
  TFloatField *m_Q_bar_codesNMCL_ID;
  TFloatField *m_Q_bar_codesASSORTMENT_ID;
  TFloatField *m_Q_bar_codesIN_MAIN_MEAS;
  TStringField *m_Q_bar_codesNOTE;
  TFloatField *m_Q_lineCUR_REST;
        TDateTimeField *m_Q_linesMAKE_DATE;
        TDateTimeField *m_Q_lineMAKE_DATE;
        TMLQuery *m_Q_make_date;
        TDateTimeField *m_Q_make_dateMAKE_DATE;
        TStringField *m_Q_make_dateNOTE;
        TMLUpdateSQL *m_U_make_date;
        TFloatField *m_Q_make_dateNMCL_ID;
        TFloatField *m_Q_make_dateID;
        TFloatField *m_Q_make_dateASSORT_ID;
        TMLQuery *m_Q_chk_hours;
        TFloatField *m_Q_chk_hoursSHELF_LIFE;
        TFloatField *m_Q_chk_hoursIS_WEIGHT;
        TFloatField *m_Q_make_dateINC_DOC_ID;
        TMLQuery *m_Q_rtl_bill_list;
        TFloatField *m_Q_rtl_bill_listDOC_ID;
        TStringField *m_Q_rtl_bill_listDOC_NUMBER;
        TDateTimeField *m_Q_rtl_bill_listDOC_DATE;
        TDateTimeField *m_Q_rtl_bill_listON_DATE;
        TFloatField *m_Q_rtl_bill_listAMOUNT;
        TFloatField *m_Q_rtl_bill_listCNT_ID;
        TStringField *m_Q_rtl_bill_listCNT_NAME;
        TFloatField *m_Q_rtl_bill_listMNGR_ID;
        TStringField *m_Q_rtl_bill_listMNGR_NAME;
        TFloatField *m_Q_rtl_bill_listSRC_ORG_ID;
        TStringField *m_Q_rtl_bill_listSRC_ORG_NAME;
        TStringField *m_Q_rtl_bill_listOK;
        TMLUpdateSQL *m_U_rtl_bill;
        TMLQuery *m_Q_add_items_by_bill;
        TStringField *m_Q_lineNOTE_BC;
        TStringField *m_Q_bar_codesNOTE_BC;
        TStringField *m_Q_nmcl_infoBAR_CODE_NOTE;
    TMLQuery *m_Q_nmcl_addr_list;
    TFloatField *m_Q_nmcl_addr_listLINE_ID;
    TFloatField *m_Q_nmcl_addr_listNMCL_ID;
    TStringField *m_Q_nmcl_addr_listNMCL_NAME;
    TFloatField *m_Q_nmcl_addr_listASSORTMENT_ID;
    TStringField *m_Q_nmcl_addr_listASSORT_NAME;
    TStringField *m_Q_nmcl_addr_listADDRESS;
    TFloatField *m_Q_nmcl_addr_listCNT_ADDR;
    TMLQuery *m_Q_address_list;
    TFloatField *m_Q_address_listLINE_ID;
    TFloatField *m_Q_address_listNMCL_ID;
    TFloatField *m_Q_address_listASSORT_ID;
    TFloatField *m_Q_address_listSHL_MEAS_UNIT_ID;
    TStringField *m_Q_address_listSHELF_NAME;
    TFloatField *m_Q_address_listSHELF_LIFE;
    TStringField *m_Q_address_listADDRESS;
    TQuery *m_Q_chg_address;
        TFloatField *m_Q_itemARCH_BILL_DOC_ID;
        TFloatField *m_Q_itemSENDER_ID;
        TFloatField *m_Q_itemSCHT_DOC_ID;
        TStringField *m_Q_itemSCHT_DOC_NUMBER;
        TDateTimeField *m_Q_itemSCHT_DOC_DATE;
        TStringField *m_Q_itemSCHT_SRC_ONAME;
        TStringField *m_Q_itemSCHT_DST_ONAME;
        TMLQuery *m_Q_load_schet;
        TFloatField *m_Q_load_schetDOC_ID;
        TFloatField *m_Q_load_schetACC_DOC_ID;
        TStringField *m_Q_load_schetDOC_NUMBER;
        TDateTimeField *m_Q_load_schetDOC_DATE;
        TFloatField *m_Q_load_schetSRC_ID;
        TStringField *m_Q_load_schetSRC_NAME;
        TFloatField *m_Q_load_schetDST_ID;
        TStringField *m_Q_load_schetDST_NAME;
        TMLQuery *m_Q_set_schet_acc;
        TMLQuery *m_Q_lines_diff;
        TFloatField *m_Q_lines_diffID;
        TFloatField *m_Q_lines_diffLINE_ID;
        TFloatField *m_Q_lines_diffNMCL_ID;
        TStringField *m_Q_lines_diffNMCL_NAME;
        TStringField *m_Q_lines_diffMEAS_NAME;
        TFloatField *m_Q_lines_diffASSORTMENT_ID;
        TStringField *m_Q_lines_diffASSORTMENT_NAME;
        TFloatField *m_Q_lines_diffIN_PRICE;
        TFloatField *m_Q_lines_diffOUT_PRICE;
        TFloatField *m_Q_lines_diffARP;
        TFloatField *m_Q_lines_diffSCHET;
        TFloatField *m_Q_lines_diffDIFF;
        TFloatField *m_Q_lines_diffOUT_AMOUNT;
        TFloatField *m_Q_lines_diffSRC_AMOUNT;
        TStoredProc *m_SP_check_schet;
   TFloatField *m_Q_itemDIFF_DOC_ID;
  TFloatField *m_Q_itemAMOUNT_NON_STORED;
  TFloatField *m_Q_listAMOUNT_NON_STORED;
  TMLQuery *m_Q_our_cnt;
  TFloatField *m_Q_our_cntCNT_ID;
  TStringField *m_Q_itemSIGN;
  TDateTimeField *m_Q_itemSIGN_DATE;
  TMLQuery *m_Q_sign;
  TFloatField *FloatField1;
  TStringField *m_Q_listSIGN;
  TDateTimeField *m_Q_listSIGN_DATE;
        TStringField *m_Q_itemTTN_NUMBER;
        TDateTimeField *m_Q_itemTTN_DATE;
        TStringField *m_Q_linesTORG2;
        TFloatField *m_Q_linesPROD_TYPE_ID;
        TStringField *m_Q_linesPROD_NAME;
  TStringField *m_Q_itemNO_TORG2;
        TMLQuery *m_Q_fict;
        TMLUpdateSQL *m_U_fict;
        TFloatField *m_Q_fictPAR;
        TMLQuery *m_Q_upd_amt;
        TFloatField *m_Q_itemINC_HOUR;
        TFloatField *m_Q_itemINC_MINUTE;
        TFloatField *m_Q_listINC_HOUR;
        TFloatField *m_Q_listINC_MINUTE;
        TDateTimeField *m_Q_linesPULL_DATE;
        TStringField *m_Q_listWITH_RECOUNT;
        TStringField *m_Q_itemWITH_RECOUNT;
        TStringField *m_Q_listGR_DEP_NAME;
        TStringField *m_Q_itemGR_DEP_NAME;
        TDateTimeField *m_Q_nmclsPULL_DATE;
        TStringField *m_Q_dup_acc_docDOC_NUMBER;
        TDateTimeField *m_Q_dup_acc_docDOC_DATE;
        TMLQuery *m_Q_sublines;
        TStringField *m_Q_linesPREF_ALCCODE_LIST;
        TFloatField *m_Q_sublinesID;
        TFloatField *m_Q_sublinesLINE_ID;
        TFloatField *m_Q_sublinesSUB_LINE_ID;
        TStringField *m_Q_sublinesPREF_ALCCODE;
        TStringField *m_Q_sublinesPDF_ALCCODE;
        TMLQuery *m_Q_sub_del;
        TMLQuery *m_Q_sub_ins;
        TStoredProc *m_SP_CREATE_INC;
        TStoredProc *m_SP_CREATE_ED_INC;
  void __fastcall m_Q_nmclsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_listAfterOpen(TDataSet *DataSet);
  void __fastcall m_Q_listBeforeClose(TDataSet *DataSet);
  void __fastcall m_Q_gdp_namesBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_tax_rateBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_assortBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_nmcl_descBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_get_gtd_listBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_lineAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_lineAfterClose(TDataSet *DataSet);
  void __fastcall m_Q_act_tcdBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemBeforeClose(TDataSet *DataSet);
  void __fastcall m_Q_nmcl_infoBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_bar_codesBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_make_dateAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_make_dateBeforeOpen(TDataSet *DataSet);
    void __fastcall m_Q_nmcl_addr_listBeforeOpen(TDataSet *DataSet);
    void __fastcall m_Q_address_listBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_load_schetBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_lines_diffBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemSENDER_IDChange(TField *Sender);
  void __fastcall m_Q_our_cntBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_sublinesBeforeOpen(TDataSet *DataSet);
public:
  enum LineKind {lkNonstored=0, lkStored=1, lkAny=2};
private:
  int m_home_org_id;
  int m_fldr_new, m_fldr_cnf, m_fldr_chk, m_fldr_arc;
  int m_dct_act_fldr_filled_id;
  int m_param_not_include_act_verif;
  long m_sq_id;
  bool m_show_nmcls;
  bool m_only_open;
  int p_sq_doc;
  AnsiString m_last_bar_code;
private:
  TS_DOC_LINE_SQ_NAME("SQ_RTL_INCOME_ITEMS")
  LineKind __fastcall GetCurrLinesKind();
  void __fastcall SetCurrLinesKind(LineKind k);
  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_id);
  void __fastcall SetShowNmcls(bool p_value);
  void __fastcall SetOnlyOpen(bool p_value);
  TDateTime __fastcall GetBeginDate() const {return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;};
  TDateTime __fastcall GetEndDate() const {return m_Q_list->ParamByName("END_DATE")->AsDateTime;};
  TDateTime __fastcall GetMakeDate();
  void __fastcall SetMakeDate(TDateTime p_dValue);
public:
  __property LineKind CurrLinesKind = {read = GetCurrLinesKind, write = SetCurrLinesKind};
  __property int OrgId = { read = GetOrgId, write = SetOrgId };
  __property int HomeOrgId = { read = m_home_org_id };
  __property int FldrNew = { read = m_fldr_new };
  __property int FldrCnf = { read = m_fldr_cnf };
  __property int FldrChk = { read = m_fldr_chk };
  __property int FldrArc = { read = m_fldr_arc };
  __property int SqDoc = { read = p_sq_doc, write = p_sq_doc};
  __property bool ShowNmcls = { read = m_show_nmcls, write = SetShowNmcls };
  __property bool OnlyOpen = { read = m_only_open, write = SetOnlyOpen };
  __property TDateTime BeginDate = { read = GetBeginDate };
  __property TDateTime EndDate = { read = GetEndDate };
  __property int ParamNotIncludeActVerif = {read=m_param_not_include_act_verif};
  __property AnsiString LastBarCode = {read=m_last_bar_code, write=m_last_bar_code};
  __property long SqId = {read=m_sq_id, write=m_sq_id};
  __property TDateTime MakeDate = {read=GetMakeDate,write=SetMakeDate};
public:
  __fastcall TDIncome(TComponent* p_owner, AnsiString p_prog_id);
  __fastcall ~TDIncome();
  AnsiString __fastcall CheckRtlIncNmcl(int p_nmcl_id,int p_assortment_id);
  void __fastcall SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date);
  void SetVerOption(bool val);
  long __fastcall FillTmpReportParamValues2(long p_sq);
  void __fastcall ClearTmpReportParamValues2(long p_sq);
  void __fastcall FillData4PrintLabel(int p_doc_id);
  void __fastcall ChangeAddr();
};
//---------------------------------------------------------------------------
#endif
