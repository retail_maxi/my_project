inherited FRTLAssortExcepNmcls: TFRTLAssortExcepNmcls
  Left = 633
  Top = 231
  Width = 1113
  Height = 504
  BorderStyle = bsSizeable
  Caption = '������'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 435
    Width = 1097
    TabOrder = 1
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 903
      inherited m_BBTN_close: TBitBtn
        Caption = '�������'
      end
    end
  end
  object m_P_main: TPanel [1]
    Left = 0
    Top = 0
    Width = 1097
    Height = 435
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 345
      Top = 0
      Width = 5
      Height = 435
      Cursor = crHSplit
    end
    object m_P_left: TPanel
      Left = 0
      Top = 0
      Width = 345
      Height = 435
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object m_DBG_nmcl: TMLDBGrid
        Left = 0
        Top = 0
        Width = 345
        Height = 435
        Align = alClient
        DataSource = m_DS_nmcl
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'NMCL_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_NAME'
            Title.TitleButton = True
            Width = 250
            Footers = <>
          end>
      end
    end
    object m_P_all: TPanel
      Left = 350
      Top = 0
      Width = 747
      Height = 435
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object m_P_btns: TPanel
        Left = 0
        Top = 0
        Width = 24
        Height = 435
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object m_SBTN_add: TSpeedButton
          Left = 0
          Top = 0
          Width = 23
          Height = 22
          Action = m_ACT_add
          ParentShowHint = False
          ShowHint = True
        end
        object m_SBTN_delete: TSpeedButton
          Left = 0
          Top = 28
          Width = 23
          Height = 22
          Action = m_ACT_delete
          ParentShowHint = False
          ShowHint = True
        end
      end
      object m_PC_all_cnt: TPageControl
        Left = 24
        Top = 0
        Width = 723
        Height = 435
        ActivePage = m_TS_org
        Align = alClient
        TabOrder = 1
        object m_TS_org: TTabSheet
          Caption = '��� ������'
          object m_DBG_all_nmcl: TMLDBGrid
            Left = 0
            Top = 0
            Width = 715
            Height = 407
            Align = alClient
            DataSource = m_DS_all_nmcl
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterColor = clWindow
            AutoFitColWidths = True
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
            Columns = <
              item
                FieldName = 'ID'
                Title.TitleButton = True
                Footers = <>
              end
              item
                FieldName = 'NAME'
                Title.TitleButton = True
                Width = 250
                Footers = <>
              end>
          end
        end
      end
    end
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_close: TAction
      Caption = '�������'
    end
    inherited m_ACT_apply_updates: TAction
      Enabled = False
      Visible = False
    end
    object m_ACT_add: TAction
      Category = 'DocActions'
      Caption = '<'
      Hint = '�������� � ������'
      OnExecute = m_ACT_addExecute
      OnUpdate = m_ACT_addUpdate
    end
    object m_ACT_delete: TAction
      Category = 'DocActions'
      Caption = '>'
      Hint = '��������� �� ������'
      OnExecute = m_ACT_deleteExecute
      OnUpdate = m_ACT_deleteUpdate
    end
  end
  object m_DS_nmcl: TDataSource
    DataSet = m_Q_nmcl
    Left = 64
    Top = 32
  end
  object m_DS_all_nmcl: TDataSource
    DataSet = m_Q_all_nmcl
    Left = 34
    Top = 64
  end
  object m_Q_add_nmcl: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      'INSERT INTO rtl_assort_exception_nmcls(assort_excep_id, nmcl_id)'
      'SELECT :ID, to_number(t.value) AS nmcl_id'
      '  FROM tmp_report_param_values t'
      ' WHERE t.id = :SQ_ID'
      '    AND NOT EXISTS (SELECT 1'
      '                    FROM rtl_assort_exception_nmcls r'
      
        '                   WHERE r.assort_excep_id = :ID AND r.nmcl_id =' +
        ' to_number(t.value)'
      '                  );'
      'end;')
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_delete_nmcl: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  DELETE FROM  rtl_assort_exception_nmcls r'
      '  WHERE r.assort_excep_id = :ID'
      
        '  AND r.nmcl_id in (SELECT DISTINCT to_number(t.value) AS nmcl_i' +
        'd'
      '                      FROM tmp_report_param_values t'
      '                     WHERE t.id = :SQ_ID) ; '
      'END;')
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_all_nmcl: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct ni.id, trim(rpad(ni.name,250)) as name '
      '  from rtl_classifier_rs rcr,'
      '       rtl_subsubcategories rss,'
      '       rtl_subcategories rs,'
      '       rtl_categories rc,'
      '       nomenclature_items ni,'
      '       rtl_assort_exception re'
      ' where rcr.subsubcat_id = rss.id'
      '   and rss.subcat_id = rs.id'
      '   and rs.cat_id = rc.id'
      '   and ni.id = rcr.nmcl_id'
      '   and re.cat_id = rc.id'
      '   and re.id = :ID'
      '   and (nvl(re.subcat_id,-1) = -1 or re.subcat_id = rs.id)'
      '   and (nvl(re.subsubcat_id,-1) = -1 or re.subsubcat_id = rs.id)'
      
        '   and not exists (select 1 from rtl_assort_exception_nmcls rn w' +
        'here rn.assort_excep_id = re.id and rn.nmcl_id = ni.id)'
      '%WHERE_CLAUSE'
      'order by trim(rpad(ni.name,250))')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_all_nmclID: TFloatField
      DisplayLabel = 'ID SKU'
      FieldName = 'ID'
      Origin = 'ni.ID'
    end
    object m_Q_all_nmclNAME: TStringField
      DisplayLabel = '������������ SKU'
      FieldName = 'NAME'
      Origin = 'trim(rpad(ni.name,250))'
      Size = 100
    end
  end
  object m_U_nmcl: TMLUpdateSQL
    Left = 32
    Top = 32
  end
  object m_Q_nmcl: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT assort_excep_id, nmcl_id,trim(rpad(ni.name, 250)) as nmcl' +
        '_name'
      '  FROM rtl_assort_exception_nmcls rn,'
      '       nomenclature_items ni'
      '  WHERE rn.nmcl_id = ni.id'
      '    AND rn.assort_excep_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_nmcl
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Top = 32
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_nmclNMCL_ID: TFloatField
      DisplayLabel = 'ID SKU'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_nmclNMCL_NAME: TStringField
      DisplayLabel = '������������ SKU'
      FieldName = 'NMCL_NAME'
      Origin = 'trim(rpad(ni.name, 250))'
      Size = 250
    end
  end
end
