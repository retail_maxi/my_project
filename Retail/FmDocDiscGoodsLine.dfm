inherited FDocDiscGoodsLine: TFDocDiscGoodsLine
  Left = 1041
  Top = 334
  ActiveControl = m_PC_main
  Caption = 'FDocDiscGoodsLine'
  ClientHeight = 258
  ClientWidth = 590
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 208
    Width = 590
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 396
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 299
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 239
    Width = 590
  end
  inherited m_P_main_top: TPanel
    Width = 590
    inherited m_P_tb_main_add: TPanel
      Left = 543
    end
    inherited m_P_tb_main: TPanel
      Width = 543
      inherited m_TB_main: TToolBar
        Width = 543
      end
    end
  end
  object m_PC_main: TPageControl [3]
    Left = 0
    Top = 26
    Width = 590
    Height = 182
    ActivePage = m_TS_new
    Align = alClient
    TabOrder = 3
    object m_TS_new: TTabSheet
      Caption = '����� �����'
      object m_DBG_list: TMLDBGrid
        Left = 0
        Top = 0
        Width = 582
        Height = 154
        Align = alClient
        DataSource = m_DS_all
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnKeyDown = m_DBG_listKeyDown
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'ID'
            Title.TitleButton = True
            Width = 30
            Footers = <>
          end
          item
            FieldName = 'NAME'
            Title.TitleButton = True
            Width = 200
            Footers = <>
          end
          item
            FieldName = 'CHECKED'
            Title.TitleButton = True
            KeyList.Strings = (
              '1'
              '2')
            Checkboxes = True
            Footers = <>
          end>
      end
    end
    object m_TS_old: TTabSheet
      Caption = '����� �����'
      ImageIndex = 1
      object Label1: TLabel
        Left = 48
        Top = 20
        Width = 24
        Height = 13
        Caption = '����'
      end
      object Label2: TLabel
        Left = 40
        Top = 52
        Width = 30
        Height = 13
        Caption = '�����'
      end
      object Label4: TLabel
        Left = 8
        Top = 80
        Width = 61
        Height = 13
        Caption = '����������'
      end
      object m_DBLC_grp: TRxDBLookupCombo
        Left = 80
        Top = 16
        Width = 477
        Height = 21
        DropDownCount = 8
        DataField = 'GRP_ID'
        DataSource = m_DS_line
        LookupField = 'ID'
        LookupDisplay = 'NAME'
        LookupSource = m_DS_org_groups
        TabOrder = 0
      end
      object m_LV_orgs: TMLLovListView
        Left = 80
        Top = 48
        Width = 481
        Height = 21
        Lov = m_LOV_orgs
        UpDownWidth = 481
        UpDownHeight = 121
        Interval = 500
        TabStop = True
        TabOrder = 1
        ParentColor = False
      end
      object m_DBE_note: TDBEdit
        Left = 80
        Top = 76
        Width = 481
        Height = 21
        DataField = 'NOTE'
        DataSource = m_DS_line
        TabOrder = 2
      end
    end
  end
  inherited m_DS_line: TDataSource
    DataSet = DDocDiscGoods.m_Q_line
  end
  object m_DS_org_groups: TDataSource
    DataSet = DDocDiscGoods.m_Q_org_groups
    Left = 464
    Top = 64
  end
  object m_DS_orgs: TDataSource
    DataSet = DDocDiscGoods.m_Q_orgs
    Left = 464
    Top = 100
  end
  object m_LOV_orgs: TMLLov
    DataFieldKey = 'LINE_ID'
    DataFieldValue = 'ORG_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_orgs
    AutoOpenList = True
    Left = 492
    Top = 100
  end
  object m_DS_all: TDataSource
    DataSet = DDocDiscGoods.m_Q_sel_orgs
    Left = 416
    Top = 64
  end
end
