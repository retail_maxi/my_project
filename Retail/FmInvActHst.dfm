inherited FInvActHst: TFInvActHst
  Left = 261
  Top = 301
  Width = 715
  Height = 235
  BorderStyle = bsSizeable
  Caption = '������� ���������'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 166
    Width = 699
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 505
      inherited m_BBTN_close: TBitBtn
        Caption = '�������'
      end
    end
  end
  object m_DBG_history: TMLDBGrid [1]
    Left = 0
    Top = 0
    Width = 699
    Height = 166
    Align = alClient
    DataSource = m_DS_history
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterColor = clWindow
    TitleLines = 2
    UseMultiTitle = True
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    Columns = <
      item
        FieldName = 'PT_ID'
        Footers = <>
      end
      item
        FieldName = 'PT_NAME'
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'MODIFY_DATE'
        Footers = <>
      end
      item
        FieldName = 'MODIFY_USER'
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'MODIFY_FIO'
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'FACT_ITEMS_OLD'
        Footers = <>
      end
      item
        FieldName = 'FACT_ITEMS_NEW'
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_close: TAction
      Caption = '�������'
    end
    inherited m_ACT_apply_updates: TAction
      Visible = False
    end
  end
  object m_DS_history: TDataSource
    DataSet = DInvAct.m_Q_fact_hst
    Left = 8
    Top = 40
  end
end
