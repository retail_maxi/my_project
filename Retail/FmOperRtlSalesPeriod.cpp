//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmOperRtlSalesPeriod.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

__fastcall TFOperRtlSalesPeriod::TFOperRtlSalesPeriod(TComponent* p_owner, TDOperRtlSales* p_dm)
    :TTSFDialog(p_owner,p_dm),
    m_dm(p_dm)
{

}

void __fastcall TFOperRtlSalesPeriod::FormShow(TObject *Sender)
{
  m_DTP_DateFrom->DateTime = m_dm->BeginDate;
  m_DTP_TimeFrom->DateTime = m_dm->BeginDate;
  m_DTP_DateTo->DateTime = m_dm->EndDate;
  m_DTP_TimeTo->DateTime = m_dm->EndDate;
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSalesPeriod::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  m_dm->SetBeginEndDate(int(m_DTP_DateFrom->DateTime) + m_DTP_TimeFrom->DateTime - int(m_DTP_TimeFrom->DateTime),
                        int(m_DTP_DateTo->DateTime) + m_DTP_TimeTo->DateTime - int(m_DTP_TimeTo->DateTime));
}
//---------------------------------------------------------------------------

