//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvSheetAlco.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
__fastcall TFInvSheetAlco::TFInvSheetAlco(TComponent* p_owner,
                                  TTSDDocumentWithLines* p_dm) : TTSFDialog( p_owner, p_dm),
                                  m_dm(static_cast<TDInvSheet*>(p_dm))

{

}
//---------------------------------------------------------------------------
/*bool ExecInvSheetAlcoDlg(TComponent* p_owner, TTSDDocumentWithLines *p_dm, TDateTime *p_date, AnsiString *p_note)
{

}  */
void __fastcall TFInvSheetAlco::m_ACT_apply_updatesExecuteAll(TObject *Sender)
{
  if (!m_dm->m_Q_alcoALCCODE->IsNull)
  {
   m_ACT_apply_updatesExecute(Sender);
        if  (edt_rank->Text !="" || edt_num->Text !="")   {
          mACT_apply_updatesClick(Sender);
        //  TTSFDialog::m_ACT_apply_updatesExecute(Sender);
          } else {
              //   TTSFDialog::m_ACT_apply_updatesExecute(Sender);
                 TFInvSheetAlco::Close();
                 }
  } else if  (edt_rank->Text !="" || edt_num->Text !="")   {
                mACT_apply_updatesClick(Sender);
                // TTSFDialog::m_ACT_apply_updatesExecute(Sender);
                 TFInvSheetAlco::Close();
  } else {
       m_LV_alccode->SetFocus() ;
       throw ETSError("���� �����!");
   }
}
//---------------------------------------------------------------------------
void __fastcall TFInvSheetAlco::m_ACT_apply_updatesExecute(TObject *Sender)
{
  if (m_dm->m_Q_alcoALCCODE->IsNull)
  {
   m_LV_alccode->SetFocus() ;
   throw ETSError("�������� �������!");
  }
  if (m_dm->m_Q_alcoQNT->IsNull)
  {
   m_DBE_qnt->SetFocus() ;
   throw ETSError("������� ����������!");
  }
  m_LV_alccode->SetFocus();
  if ((!m_dm->m_Q_alcoPDF_BAR_CODE->IsNull) && (m_dm->m_Q_alcoQNT->AsInteger != 1))
  {
   m_DBE_qnt->SetFocus() ;
   throw ETSError("������ ������ ����������!");
  }
  m_dm->m_Q_alco->ApplyUpdates();

  if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();
}
//---------------------------------------------------------------------------
void __fastcall TFInvSheetAlco::mACT_apply_updatesClick(TObject *Sender)
{
  if (edt_rank->Text =="")
  {
   edt_rank->SetFocus() ;
   throw ETSError("��������� �����!");
  }
  if (edt_num->Text =="")
  {
   edt_num->SetFocus() ;
   throw ETSError("��������� �����!");
  }

         m_dm->m_type_barcod->ParamByName("RANK")->AsString = edt_rank->Text;
         m_dm->m_type_barcod->ParamByName("LINE_NUMBER")->AsString = edt_num->Text;
         if (m_dm->m_type_barcod->Active) m_dm->m_type_barcod->Close();
            m_dm->m_type_barcod->Open();
           if (m_dm->m_type_barcodVALUE->IsNull ) {
            throw ETSError("��������� �������� ����� �� ���� ������������� � �����. ������ ������� ����� ������ �� �-��������������!");
           }

         m_dm->m_kod_snnum_chek->ParamByName("TYPE_ID")->AsInteger = m_dm->m_type_barcodID->AsInteger;
         m_dm->m_kod_snnum_chek->ParamByName("RANK")->AsString = edt_rank->Text;
         m_dm->m_kod_snnum_chek->ParamByName("LINE_NUMBER")->AsString = edt_num->Text;
         //m_dm->m_kod_snnum_chek->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
         //m_dm->m_kod_snnum_chek->ParamByName("ASSORT_ID")->AsInteger = m_dm->m_Q_lineASSORTMENT_ID->AsInteger;

          if (m_dm->m_kod_snnum_chek->Active) m_dm->m_kod_snnum_chek->Close();
            m_dm->m_kod_snnum_chek->Open();

         if (m_dm->m_kod_snnum_chekPDF_BARCODE->IsNull ) {
            throw ETSError("��������� �������� ����� �� ���� ������������� � �����. ������ ������� ����� ������ �� �-��������������");
           } else{
                    m_dm->m_Q_get_alccode->ParamByName("PDF_BAR_CODE")->AsString = m_dm->m_kod_snnum_chekPDF_BARCODE->AsString;
                    if (m_dm->m_Q_get_alccode->Active) m_dm->m_Q_get_alccode->Close();
                    m_dm->m_Q_get_alccode->Open();

                    while (!m_dm->m_Q_alc->Eof)
                    {
                      if (m_dm->m_Q_alcPDF_BAR_CODE->AsString ==  m_dm->m_kod_snnum_chekPDF_BARCODE->AsString)
                        throw ETSError("�������� ����� ��� ���������!");
                      m_dm->m_Q_alc->Next();
                    }

                    m_dm->m_alco_nmcl_chek->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_lineNMCL_ID->AsInteger;
                    m_dm->m_alco_nmcl_chek->ParamByName("ASSORTMENT_ID")->AsInteger =  m_dm->m_Q_lineASSORTMENT_ID->AsInteger;
                    m_dm->m_alco_nmcl_chek->ParamByName("PREF_ALCCODE")->AsString =  m_dm->m_Q_get_alccodeALCCODE->AsString;
                    if (m_dm->m_alco_nmcl_chek->Active) m_dm->m_alco_nmcl_chek->Close();
                    m_dm->m_alco_nmcl_chek->Open();

                    if (m_dm->m_alco_nmcl_chekALCO->IsNull)
                    {
                      int v_nmcl_id = m_dm->m_Q_lineNMCL_ID->AsInteger;
                      String v_nmcl_name = m_dm->m_Q_lineNMCL_NAME->AsString;
                      String v_egais_id = m_dm->m_Q_get_alccodeALCCODE->AsString;
                      char str[1000];
                      ShowMessage(v_nmcl_name);
                      sprintf(str, "������������� ��������� �������� �����(%s) �� ������������� ������������(ID:%d, %s)! ���������� ���������� � ����������� �� ����� ������� ��.", v_egais_id, v_nmcl_id, v_nmcl_name);
                      throw ETSError(str);
                    }
                   m_dm->m_Q_ins_alco->ParamByName("ID")->AsInteger =  m_dm->m_Q_lineID->AsInteger;
                   m_dm->m_Q_ins_alco->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_lineLINE_ID->AsInteger;
                   m_dm->m_Q_ins_alco->ParamByName("PDF_BAR_CODE")->AsString =  m_dm->m_kod_snnum_chekPDF_BARCODE->AsString;
                   m_dm->m_Q_ins_alco->ParamByName("ALCCODE")->AsString =  m_dm->m_Q_get_alccodeALCCODE->AsString;
                   m_dm->m_Q_ins_alco->ParamByName("QNT")->AsInteger =  1;
                   m_dm->m_Q_ins_alco->ExecSQL();

                   
                   if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
                   m_dm->m_Q_alc->Open();
             }
}
//---------------------------------------------------------------------------


void __fastcall TFInvSheetAlco::FormShow(TObject *Sender)
{
if  ( !m_dm->m_Q_alcoQNT->IsNull)
         {
          TFInvSheetAlco::Height = TFInvSheetAlco::Height - m_panel_SN->Height;
          m_panel_SN->Visible = false;
          m_panel_SN->Height = 0;
         }
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetAlco::edt_rankKeyPress(TObject *Sender,
      char &Key)
{
      if (  ((Key < '0') || (Key > '9')) && (Key != VK_DELETE) &&
    (Key != VK_BACK) && (Key != VK_TAB)) Key = NULL;        
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetAlco::edt_numKeyPress(TObject *Sender, char &Key)
{
      if (  ((Key < '0') || (Key > '9')) && (Key != VK_DELETE) &&
    (Key != VK_BACK) && (Key != VK_TAB)) Key = NULL;
}
//---------------------------------------------------------------------------


void __fastcall TFInvSheetAlco::m_ACT_closeExecute(TObject *Sender)
{
  TFInvSheetAlco::Close();
}
//---------------------------------------------------------------------------

