//---------------------------------------------------------------------------

#ifndef FmSelectOrgH
#define FmSelectOrgH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "RxQuery.hpp"
#include <Db.hpp>
#include <DBTables.hpp>
#include "MLQuery.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFSelectOrg : public TForm
{
__published:
  TCheckListBox *m_CLB_org;
  TPanel *m_P_main_control;
  TToolBar *m_TB_main_control_buttons_dialog;
  TBitBtn *m_SBTN_save;
  TBitBtn *m_BBTN_close;
  TImageList *m_IL_main;
  TActionList *m_ACTL_main;
  TAction *m_ACT_close;
  TAction *m_ACT_apply_updates;
  TLabel *m_L_org;
  TDatabase *m_DB_gl;
  TRxQuery *m_Q_organizations_list;
  TFloatField *m_Q_organizations_listID;
  TStringField *m_Q_organizations_listNAME;
  TFloatField *m_Q_organizations_listCHECKED;
  TUpdateSQL *m_U_organizations_list;
  TRxQuery *m_Q_organizations;
  TFloatField *m_Q_organizationsID;
  TStringField *m_Q_organizationsNAME;
  TUpdateSQL *m_U_organizations;
  TQuery *m_Q_fill_tmp_rep_param_val;
  TFloatField *m_Q_fill_tmp_rep_param_valID;
  TMemoField *m_Q_fill_tmp_rep_param_valVALUE;
  TUpdateSQL *m_U_fill_tmp_rep_param_val;
  TQuery *m_Q_sq_next_val;
  TQuery *m_Q_kill_tmp_rep_param_val;
        TBitBtn *BitBtn1;
        TAction *m_ACT_sel_file;
        TOpenDialog *m_OD_import_Excel;
        TMLQuery *m_Q_insert_data;
        TMLQuery *m_Q_del_data;
        TStoredProc *m_SP_create_doc;
  void __fastcall m_ACT_closeExecute(TObject *Sender);
  void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
  void __fastcall m_Q_organizations_listBeforeOpen(TDataSet *DataSet);
        void __fastcall m_ACT_sel_fileExecute(TObject *Sender);
        void __fastcall m_ACT_apply_updatesUpdate(TObject *Sender);
private:
  bool FPressOK;
  bool __fastcall GetPressOK();
  hDBIDb m_db_handle;
  long m_sq_id;
  bool load_data;
  long __fastcall FillTmpReportParamValues(TStrings *p_str, long p_sq);
  int __fastcall GetSqNextVal(const AnsiString &p_sq_name);
public:
  __fastcall TFSelectOrg(TComponent* p_owner,
                         hDBIDb p_db_handle);
  __fastcall ~TFSelectOrg();
  __property bool PressOK  = { read=GetPressOK };
};
//---------------------------------------------------------------------------

bool ExecSelectOrgDlg(TComponent *p_owner,
                      hDBIDb p_db_handle);
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

