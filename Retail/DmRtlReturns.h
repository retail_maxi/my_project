//---------------------------------------------------------------------------

#ifndef DmRtlReturnsH
#define DmRtlReturnsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMDOCUMENT.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------

class TDRtlReturns : public TTSDDocument
{
__published:
  TDateTimeField *m_Q_listISSUED_DATE;
  TFloatField *m_Q_listFISCAL_NUM;
  TFloatField *m_Q_listREPORT_NUM;
  TDateTimeField *m_Q_listREPORT_DATE;
  TFloatField *m_Q_listCHECK_NUM;
  TDateTimeField *m_Q_listCHECK_DATE;
  TFloatField *m_Q_listCHECK_SUM;
  TFloatField *m_Q_listOUT_RETURN_SUM;
  TFloatField *m_Q_itemREPORT_DOC_ID;
  TDateTimeField *m_Q_itemISSUED_DATE;
  TFloatField *m_Q_itemFISCAL_NUM;
  TFloatField *m_Q_itemREPORT_NUM;
  TDateTimeField *m_Q_itemREPORT_DATE;
  TFloatField *m_Q_itemCASH_USER_ID;
  TFloatField *m_Q_itemCHECK_DOC_ID;
  TFloatField *m_Q_itemCHECK_NUM;
  TDateTimeField *m_Q_itemCHECK_DATE;
  TFloatField *m_Q_itemCHECK_SUM;
  TFloatField *m_Q_itemOUT_RETURN_SUM;
  TStringField *m_Q_listCASH_USER_NAME;
  TStringField *m_Q_itemCASH_USER_NAME;
  TMLQuery *m_Q_lines;
  TFloatField *m_Q_linesLINE_ID;
  TFloatField *m_Q_linesNMCL_ID;
  TStringField *m_Q_linesNMCL_NAME;
  TFloatField *m_Q_linesASSORT_ID;
  TStringField *m_Q_linesASSORT_NAME;
  TStringField *m_Q_linesBAR_CODE;
  TFloatField *m_Q_linesOUT_PRICE;
  TFloatField *m_Q_linesQUANTITY;
  TMLQuery *m_Q_zreports;
  TFloatField *m_Q_zreportsFISCAL_NUM;
  TFloatField *m_Q_zreportsREPORT_NUM;
  TDateTimeField *m_Q_zreportsREPORT_DATE;
  TMLQuery *m_Q_cashs;
  TFloatField *m_Q_zreportsDOC_ID;
  TFloatField *m_Q_cashsCASH_USER_ID;
  TStringField *m_Q_cashsCASH_USER_NAME;
  TMLQuery *m_Q_checks;
  TFloatField *m_Q_checksCHECK_NUM;
  TDateTimeField *m_Q_checksCHECK_DATE;
  TFloatField *m_Q_checksCHECK_SUM;
  TFloatField *m_Q_checksREPORT_DOC_ID;
  TMLQuery *m_Q_sale_lines;
  TMLQuery *m_Q_nmcls;
  TMLQuery *m_Q_assorts;
  TFloatField *m_Q_nmclsNMCL_ID;
  TStringField *m_Q_nmclsNMCL_NAME;
  TFloatField *m_Q_assortsASSORT_ID;
  TStringField *m_Q_assortsASSORT_NAME;
  TQuery *m_Q_recalc;
  TQuery *m_Q_price;
  TFloatField *m_Q_priceOUT_PRICE;
  TFloatField *m_Q_priceIN_PRICE;
  TStringField *m_Q_itemNOTE;
  TStringField *m_Q_listNOTE;
        TFloatField *m_Q_itemRETURN_NUMBER;
        TFloatField *m_Q_listRETURN_NUMBER;
        TMLQuery *m_Q_cash_ret;
        TFloatField *m_Q_itemRET_USER_ID;
        TStringField *m_Q_itemRET_USER_NAME;
        TStringField *m_Q_itemCASH;
        TStringField *m_Q_itemBUY_FIO;
        TStringField *m_Q_checksCASH;
        TFloatField *m_Q_cash_retCOMPLETE_USER_ID;
        TStringField *m_Q_cash_retCASH_USER_NAME;
        TMLQuery *m_Q_zks;
        TFloatField *m_Q_zksZKS;
        TStringField *m_Q_listCASH;
        TMLQuery *m_Q_line;
        TMLUpdateSQL *m_U_line;
        TFloatField *m_Q_lineDOC_ID;
        TFloatField *m_Q_lineLINE_ID;
        TFloatField *m_Q_lineSALE_DOC_ID;
        TFloatField *m_Q_lineSALE_LINE_ID;
        TFloatField *m_Q_lineNMCL_ID;
        TStringField *m_Q_lineNMCL_NAME;
        TFloatField *m_Q_lineASSORT_ID;
        TStringField *m_Q_lineASSORT_NAME;
        TFloatField *m_Q_lineWEIGHT_CODE;
        TStringField *m_Q_lineBAR_CODE;
        TFloatField *m_Q_lineOUT_PRICE;
        TFloatField *m_Q_lineIN_PRICE;
        TFloatField *m_Q_lineQUANTITY;
        TFloatField *m_Q_lineNDS;
        TFloatField *m_Q_lineSHOP_DEP_ID;
        TFloatField *m_Q_sale_linesLINE_ID;
        TFloatField *m_Q_sale_linesNMCL_ID;
        TStringField *m_Q_sale_linesNMCL_NAME;
        TStringField *m_Q_sale_linesASSORT_NAME;
        TStringField *m_Q_sale_linesBAR_CODE;
        TFloatField *m_Q_sale_linesOUT_PRICE;
        TFloatField *m_Q_sale_linesQUANTITY;
        TFloatField *m_Q_sale_linesWEIGHT_CODE;
        TFloatField *m_Q_sale_linesDOC_ID;
        TFloatField *m_Q_sale_linesASSORT_ID;
        TFloatField *m_Q_sale_linesIN_PRICE;
        TFloatField *m_Q_sale_linesNDS;
        TFloatField *m_Q_sale_linesSHOP_DEP_ID;
        TFloatField *m_Q_linesIS_ONE_PIECE;
        TFloatField *m_Q_lineIS_ONE_PIECE;
        TDateTimeField *m_Q_itemSTART_DATE;
        TMLQuery *m_Q_check_nmcl;
        TFloatField *m_Q_check_nmclCH;
   TStringField *m_Q_itemIS_CHECK;
   TStringField *m_Q_itemDECISION_RETURN;
   TStringField *m_Q_itemCASHIER_ERROR;
        TMLQuery *m_Q_staff_list;
        TFloatField *m_Q_staff_listADM_USER_ID;
        TStringField *m_Q_staff_listADM_USER_NAME;
        TFloatField *m_Q_itemADM_USER_ID;
        TStringField *m_Q_itemADM_USER_NAME;
        TQuery *m_Q_orgs;
        TFloatField *m_Q_orgsID;
        TStringField *m_Q_orgsNAME;
        TMLQuery *m_Q_form_quan;
        TFloatField *m_Q_form_quanERR_QUANT;
        TFloatField *m_Q_listBUF_ID;
        TFloatField *m_Q_itemBUF_ID;
        TMLQuery *m_Q_get_taxpayer;
        TFloatField *m_Q_get_taxpayerFISCAL_TAXPAYER_ID;
        TDateTimeField *m_Q_itemEND_DATE;
        TMLQuery *m_Q_alc;
        TMLQuery *m_Q_alco;
        TMLUpdateSQL *m_U_alco;
        TMLQuery *m_Q_pdf_bar_code;
        TMLQuery *m_get_PDF_BAR_CODE;
        TMemoField *m_get_PDF_BAR_CODEPDF_BAR_CODE_OUT;
        TMLQuery *m_chek_pdf;
        TFloatField *m_chek_pdfIS_NEED_CRT;
        TFloatField *m_Q_alcoDOC_ID;
        TFloatField *m_Q_alcoLINE_ID;
        TFloatField *m_Q_alcoSUB_LINE_ID;
        TFloatField *m_Q_alcoNMCL_ID;
        TFloatField *m_Q_alcoASSORT_ID;
        TStringField *m_Q_alcoPDF_BAR_CODE;
        TFloatField *m_Q_alcoEGAIS_PRICE;
        TFloatField *m_Q_alcoVOLUME;
        TFloatField *m_Q_alcoPRICE_MRZ;
        TStringField *m_Q_alcoIS_FULL;
        TFloatField *m_Q_alcoCHECK_NUM;
        TFloatField *m_Q_alcoREPORT_NUM;
        TStringField *m_Q_alcoINN;
        TStringField *m_Q_alcoKPP;
        TStringField *m_Q_alcoALC_CODE;
        TFloatField *m_Q_alcDOC_ID;
        TFloatField *m_Q_alcLINE_ID;
        TFloatField *m_Q_alcSUB_LINE_ID;
        TFloatField *m_Q_alcNMCL_ID;
        TFloatField *m_Q_alcASSORT_ID;
        TStringField *m_Q_alcPDF_BAR_CODE;
        TFloatField *m_Q_alcEGAIS_PRICE;
        TFloatField *m_Q_alcVOLUME;
        TFloatField *m_Q_alcPRICE_MRZ;
        TStringField *m_Q_alcIS_FULL;
        TFloatField *m_Q_alcCHECK_NUM;
        TFloatField *m_Q_alcREPORT_NUM;
        TStringField *m_Q_alcINN;
        TStringField *m_Q_alcKPP;
        TStringField *m_Q_alcALC_CODE;
   TStringField *m_Q_zreportsTAXPAYER_NAME;
   TDateTimeField *m_Q_zreportsCREATE_DATE;
   TMLQuery *m_Q_find_check;
   TFloatField *m_Q_find_checkCHECK_NUM;
   TDateTimeField *m_Q_find_checkCHECK_DATE;
   TFloatField *m_Q_find_checkCHECK_SUM;
   TFloatField *m_Q_find_checkID;
   TStringField *m_Q_find_checkCASH;
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_cashsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_assortsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_cash_retBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_checksBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_check_nmclBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_staff_listBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_form_quanBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_zreportsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcoAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_alcoBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_pdf_bar_codeAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_pdf_bar_codeBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcBeforeOpen(TDataSet *DataSet);
   void __fastcall m_Q_find_checkBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_nmclsBeforeOpen(TDataSet *DataSet);
private:
  int m_home_org_id;
  int m_fldr_print;
  int m_fldr_new;
  bool m_edit_cond;
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_value);
public:
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
public:
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
  __property int HomeOrgId = {read=m_home_org_id};
  __property int OrgId = {read=GetOrgId, write=SetOrgId};
  __property int FldrPrint = {read=m_fldr_print};
  __property int FldrNew = {read=m_fldr_new};
  __property bool EditCond = {read=m_edit_cond,write=m_edit_cond};
   int UpdateRegimeLine;
public:
  __fastcall TDRtlReturns(TComponent* p_owner, AnsiString p_prog_id);
  __fastcall ~TDRtlReturns();
ML_BEGIN_DATA_SETS
  ML_BASE_DATA_SETS(TTSDDocument)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_lines)
ML_END_DATA_SETS
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
