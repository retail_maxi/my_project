//---------------------------------------------------------------------------

#ifndef FmDocMessagesRKUListH
#define FmDocMessagesRKUListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFmDBDialog.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmMessageToRKU.h"
#include <CheckLst.hpp>
#include "TSFMDIALOG.h"
//---------------------------------------------------------------------------
class TFDocMessagesRKUList : public TTSFDialog
{
__published:	// IDE-managed Components
    TCheckListBox *m_CLB_rku_list;
    TToolBar *ToolBar1;
    TAction *m_ACT_clr_all;
    TAction *m_ACT_sel_all;
    TSpeedButton *m_SB_clr_all;
    TSpeedButton *m_SB_sel_all;
    void __fastcall m_ACT_clr_allExecute(TObject *Sender);
    void __fastcall m_ACT_sel_allExecute(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TFDocMessagesRKUList(TComponent *p_owner);
};
//---------------------------------------------------------------------------
#endif
 