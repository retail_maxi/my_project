inherited FInvActAlco: TFInvActAlco
  Left = 688
  Top = 375
  Caption = '�������'
  ClientHeight = 146
  ClientWidth = 401
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel [0]
    Left = 8
    Top = 26
    Width = 43
    Height = 13
    Caption = '�������'
    WordWrap = True
  end
  object Label3: TLabel [1]
    Left = 8
    Top = 50
    Width = 51
    Height = 26
    Caption = '���� �� ���������'
    WordWrap = True
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 88
    Width = 28
    Height = 13
    Caption = '����'
    WordWrap = True
  end
  object Label2: TLabel [3]
    Left = 252
    Top = 58
    Width = 57
    Height = 17
    Caption = '�� �����'
    WordWrap = True
  end
  inherited m_P_main_control: TPanel
    Top = 115
    Width = 401
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 207
      inherited m_SBTN_save: TBitBtn
        Caption = '���������'
      end
    end
  end
  object m_DBE_alccode: TDBEdit [5]
    Left = 78
    Top = 24
    Width = 129
    Height = 21
    Color = clInactiveCaption
    DataField = 'ALCCODE'
    DataSource = m_DS_alco
    ReadOnly = True
    TabOrder = 1
  end
  object m_LV_alccode: TMLLovListView [6]
    Left = 211
    Top = 24
    Width = 181
    Height = 21
    Lov = m_LOV_alccode
    UpDownWidth = 480
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 2
    ParentColor = False
  end
  object m_DBE_recalc_items: TDBEdit [7]
    Left = 78
    Top = 55
    Width = 129
    Height = 21
    DataField = 'RECALC_ITEMS'
    DataSource = m_DS_alco
    TabOrder = 3
  end
  object m_DBE_fact_items: TDBEdit [8]
    Left = 78
    Top = 85
    Width = 129
    Height = 21
    DataField = 'FACT_ITEMS'
    DataSource = m_DS_alco
    TabOrder = 4
  end
  object m_P_start_items: TMLDBPanel [9]
    Left = 317
    Top = 55
    Width = 73
    Height = 21
    DataField = 'START_ITEMS'
    DataSource = m_DS_alco
    Alignment = taLeftJustify
    BevelWidth = 0
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 5
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_apply_updates: TAction
      Caption = '���������'
    end
  end
  object m_DS_alccode: TDataSource
    DataSet = DInvAct.m_Q_alccode
    Left = 254
    Top = 19
  end
  object m_LOV_alccode: TMLLov
    DataFieldKey = 'ALCCODE'
    DataFieldValue = 'FULLNAME'
    DataSource = m_DS_alco
    DataFieldKeys = 'PREF_ALCCODE'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_alccode
    AutoOpenList = True
    Left = 286
    Top = 19
  end
  object m_DS_alco: TDataSource
    DataSet = DInvAct.m_Q_alco
    Left = 6
    Top = 3
  end
end
