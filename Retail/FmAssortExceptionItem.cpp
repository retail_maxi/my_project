//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmAssortExceptionItem.h"
#include "FmRTLAssortExcepOrgs.h"
#include "FmRTLAssortExcepNmcls.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
__fastcall TFAssortExceptionItem::TFAssortExceptionItem(TComponent* p_owner, TTSDRefbook *p_dm_refbook):
    TTSFRefbookItem(p_owner, p_dm_refbook),
    m_dm(static_cast<TDAssortException*>(p_dm_refbook))
{
}
//---------------------------------------------------------------------------


void __fastcall TFAssortExceptionItem::FormShow(TObject *Sender)
{
    TTSFRefbookItem::FormShow(Sender);
}
//---------------------------------------------------------------------------

AnsiString __fastcall TFAssortExceptionItem::BeforeItemApply(TWinControl **p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete) return "";
  if (m_dm->m_Q_itemCAT_ID->IsNull)
  {
    *p_focused = m_LV_nmcl;
    return "�������� ��������� ������";
  }
  return "";
}

void __fastcall TFAssortExceptionItem::m_LOV_nmclAfterClear(
      TObject *Sender)
{
  //m_LOV_subcat->Clear();
  m_LV_subcat->Refresh();
  m_LV_nmcl->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFAssortExceptionItem::m_LOV_subcatAfterClear(
      TObject *Sender)
{
  //m_LOV_subsubcat->Clear();
  m_LV_subsubcat->Refresh();
  m_LV_subcat->SetFocus();
}
//---------------------------------------------------------------------------



void __fastcall TFAssortExceptionItem::m_LOV_subcatAfterApply(
      TObject *Sender)
{
  m_LV_nmcl->Refresh();
  m_LV_subsubcat->Refresh();
  m_LV_subcat->SetFocus();
}
//---------------------------------------------------------------------------


void __fastcall TFAssortExceptionItem::m_LOV_s(
      TObject *Sender)
{
  m_LV_nmcl->Refresh();
  m_LV_subcat->Refresh();
  m_LV_subsubcat->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFAssortExceptionItem::m_LOV_nmclAfterApply(
      TObject *Sender)
{
  m_LV_subsubcat->Refresh();
  m_LV_subcat->Refresh();
  m_LV_nmcl->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFAssortExceptionItem::m_ACT_edit_orgExecute(
      TObject *Sender)
{
  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  RTLAssortExcepOrgsDlg(this, m_dm->m_Q_itemID->AsInteger, m_dm->m_Q_itemORG_GROUP_ID->AsInteger, m_dm, m_dm->UpdateRegimeItem != urView);
  m_dm->m_Q_orgs->Close();
  m_dm->m_Q_orgs->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFAssortExceptionItem::m_ACT_edit_nmclExecute(
      TObject *Sender)
{
    if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
    RTLAssortExcepNmclsDlg(this, m_dm->m_Q_itemID->AsInteger, m_dm, m_dm->UpdateRegimeItem != urView);
    m_dm->m_Q_sku->Close();
    m_dm->m_Q_sku->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFAssortExceptionItem::m_ACT_edit_orgUpdate(
      TObject *Sender)
{
    m_ACT_edit_org->Enabled =  m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFAssortExceptionItem::m_ACT_edit_nmclUpdate(
      TObject *Sender)
{
    m_ACT_edit_nmcl->Enabled =  m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------





