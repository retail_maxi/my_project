inherited FIncomeItem: TFIncomeItem
  Left = 272
  Top = 184
  Width = 1239
  Height = 631
  Caption = 'FIncomeItem'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 543
    Width = 1223
    TabOrder = 1
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 1126
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 835
      AutoSize = False
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 641
    end
    object m2: TPanel
      Left = 0
      Top = 0
      Width = 298
      Height = 31
      Align = alLeft
      AutoSize = True
      TabOrder = 3
      object btnCreateInc: TButton
        Left = 1
        Top = 0
        Width = 144
        Height = 31
        Caption = '������� �� ���������'
        TabOrder = 0
        OnClick = btnCreateIncClick
      end
      object btnCreateEdInc: TButton
        Left = 145
        Top = -1
        Width = 152
        Height = 31
        Caption = '������� �������������'
        TabOrder = 1
        OnClick = btnCreateEdIncClick
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 574
    Width = 1223
  end
  inherited m_P_main_top: TPanel
    Width = 1223
    inherited m_P_tb_main: TPanel
      Width = 1176
      inherited m_TB_main: TToolBar
        Width = 1176
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 1176
    end
  end
  inherited m_P_header: TPanel
    Width = 1223
    Height = 311
    object m_P_bot: TPanel
      Left = 0
      Top = 173
      Width = 1223
      Height = 132
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object m_L_kind: TLabel
        Left = 32
        Top = 93
        Width = 62
        Height = 13
        Caption = '������ ����'
      end
      object Label3: TLabel
        Left = 188
        Top = 87
        Width = 85
        Height = 26
        Alignment = taRightJustify
        Caption = '�����* � ������'#13#10'����. �����'
      end
      object m_L_corr_amt: TLabel
        Left = 377
        Top = 87
        Width = 72
        Height = 26
        Alignment = taRightJustify
        Caption = '���������-�� �����'
        WordWrap = True
      end
      object m_SB_corr_amt: TSpeedButton
        Left = 520
        Top = 89
        Width = 21
        Height = 21
        Action = m_ACT_set_corr_amt
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000C6C6C6000084840000848400008484000084840000848400008484000084
          84000084840000848400008484000084840000000000FF00FF00FF00FF00FF00
          FF0000000000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
          FF0000FFFF0000FFFF000084840000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FF
          FF0000FFFF000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000C6C6C60000FFFF0000FFFF0000FFFF0000FF
          FF000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C60000FFFF0000FFFF000084
          840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6C600008484000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object m_L_in_amount: TLabel
        Left = 546
        Top = 95
        Width = 38
        Height = 13
        Caption = '�����*'
      end
      object m_L_note: TLabel
        Left = 35
        Top = 62
        Width = 63
        Height = 13
        Caption = '����������'
        FocusControl = m_DBE_note
      end
      object m_L_receiver: TLabel
        Left = 37
        Top = 34
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = '����������'
        FocusControl = m_LLV_receiver
      end
      object m_L_inc_started: TLabel
        Left = 28
        Top = 0
        Width = 71
        Height = 26
        Alignment = taRightJustify
        Caption = '������ ������������'
        WordWrap = True
      end
      object m_L_inc_finfished: TLabel
        Left = 308
        Top = 0
        Width = 71
        Height = 26
        Alignment = taRightJustify
        Caption = '��������� ������������'
        WordWrap = True
      end
      object Label6: TLabel
        Left = 696
        Top = 94
        Width = 52
        Height = 13
        Caption = '�� � �����'
      end
      object m_SBTN_sign: TSpeedButton
        Left = 560
        Top = 0
        Width = 133
        Height = 22
        Action = m_ACT_sign
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF008400000084000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00008400000084000084000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF008400000000840000008400000084000084000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
          0000008400000084000000FF0000008400000084000000840000008400008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000084
          00000084000000840000FF00FF0000FF00000084000000840000008400008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000FF
          00000084000000FF0000FF00FF00FF00FF0000FF000000840000008400000084
          000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000FF0000FF00FF00FF00FF00FF00FF00FF00FF0000FF0000008400000084
          00000084000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000FF00000084
          00000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000FF
          0000008400000084000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000FF0000008400000084000084000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000FF0000008400000084000084000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000FF00000084000000840000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000FF0000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object Label7: TLabel
        Left = 561
        Top = 34
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = '����� ��������:'
      end
      object Label8: TLabel
        Left = 710
        Top = 35
        Width = 8
        Height = 13
        Caption = '�.'
      end
      object Label9: TLabel
        Left = 782
        Top = 35
        Width = 23
        Height = 13
        Caption = '���.'
      end
      object m_L1: TLabel
        Left = 46
        Top = 119
        Width = 49
        Height = 13
        Caption = '����� ��'
      end
      object m_CB_type: TComboBox
        Left = 104
        Top = 90
        Width = 78
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnClick = m_CB_typeClick
        Items.Strings = (
          '�� � �����'
          '� �����'
          '���')
      end
      object m_DBPL_w_corr: TMLDBPanel
        Left = 277
        Top = 90
        Width = 95
        Height = 21
        DataField = 'SUM_WITH_CORR'
        DataSource = m_DS_item
        Alignment = taRightJustify
        BevelOuter = bvNone
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 1
      end
      object m_DBP_corr_amt: TMLDBPanel
        Left = 453
        Top = 90
        Width = 68
        Height = 21
        DataField = 'CORR_AMT'
        DataSource = m_DS_item
        Alignment = taRightJustify
        BevelOuter = bvLowered
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 2
      end
      object m_DBP_in_amount: TMLDBPanel
        Left = 589
        Top = 90
        Width = 100
        Height = 21
        DataField = 'AMOUNT_IN'
        DataSource = m_DS_item
        Alignment = taRightJustify
        BevelOuter = bvLowered
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 3
      end
      object m_DBE_note: TDBEdit
        Left = 104
        Top = 58
        Width = 548
        Height = 21
        DataField = 'NOTE'
        DataSource = m_DS_item
        TabOrder = 4
      end
      object m_LLV_receiver: TMLLovListView
        Left = 104
        Top = 29
        Width = 449
        Height = 21
        Lov = m_LOV_receivers
        UpDownWidth = 505
        UpDownHeight = 121
        Interval = 500
        TabStop = True
        TabOrder = 5
        ParentColor = False
      end
      object m_DBDE_inc_started_date: TDBDateEdit
        Left = 104
        Top = 2
        Width = 89
        Height = 21
        DataField = 'INC_STARTED'
        DataSource = m_DS_item
        NumGlyphs = 2
        TabOrder = 6
      end
      object m_DTP_inc_started_time: TDateTimePicker
        Left = 202
        Top = 2
        Width = 73
        Height = 21
        CalAlignment = dtaLeft
        Date = 39058.5147162037
        Time = 39058.5147162037
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkTime
        ParseInput = False
        TabOrder = 7
        OnChange = m_DTP_inc_started_timeChange
      end
      object m_DBDE_inc_finished_date: TDBDateEdit
        Left = 386
        Top = 2
        Width = 89
        Height = 21
        DataField = 'INC_FINISHED'
        DataSource = m_DS_item
        NumGlyphs = 2
        TabOrder = 8
      end
      object m_DTP_inc_finished_time: TDateTimePicker
        Left = 482
        Top = 2
        Width = 73
        Height = 21
        CalAlignment = dtaLeft
        Date = 39058.5147162037
        Time = 39058.5147162037
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkTime
        ParseInput = False
        TabOrder = 9
        OnChange = m_DTP_inc_finished_timeChange
      end
      object m_DBP_non_stored: TMLDBPanel
        Left = 752
        Top = 90
        Width = 81
        Height = 21
        DataField = 'AMOUNT_NON_STORED'
        DataSource = m_DS_item
        Alignment = taRightJustify
        BevelOuter = bvLowered
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 10
      end
      object m_DBE_sign: TDBEdit
        Left = 696
        Top = 0
        Width = 57
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'SIGN'
        DataSource = m_DS_item
        ReadOnly = True
        TabOrder = 11
      end
      object m_DBE_sign_date: TDBEdit
        Left = 760
        Top = 0
        Width = 75
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'SIGN_DATE'
        DataSource = m_DS_item
        ReadOnly = True
        TabOrder = 12
      end
      object m_DBE_inc_hour: TDBEdit
        Left = 651
        Top = 29
        Width = 57
        Height = 21
        TabStop = False
        DataField = 'INC_HOUR'
        DataSource = m_DS_item
        TabOrder = 13
      end
      object m_DBE_inc_minute: TDBEdit
        Left = 723
        Top = 29
        Width = 57
        Height = 21
        TabStop = False
        DataField = 'INC_MINUTE'
        DataSource = m_DS_item
        TabOrder = 14
      end
      object m_DBCB_with_recount: TDBCheckBox
        Left = 679
        Top = 58
        Width = 154
        Height = 21
        TabStop = False
        BiDiMode = bdLeftToRight
        Caption = '� ���������� ����������'
        DataField = 'WITH_RECOUNT'
        DataSource = m_DS_item
        ParentBiDiMode = False
        ReadOnly = True
        TabOrder = 15
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object MLDBPanel1: TMLDBPanel
        Left = 106
        Top = 114
        Width = 265
        Height = 21
        DataField = 'GR_DEP_NAME'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 16
      end
    end
    object m_P_schet: TPanel
      Left = 0
      Top = 141
      Width = 1223
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object Label1: TLabel
        Left = 24
        Top = 8
        Width = 73
        Height = 13
        Caption = '��. ���������'
      end
      object Label2: TLabel
        Left = 208
        Top = 11
        Width = 11
        Height = 13
        Caption = '��'
        FocusControl = m_MDB_sch_date
      end
      object Label4: TLabel
        Left = 557
        Top = 8
        Width = 6
        Height = 13
        Caption = '�'
      end
      object Label5: TLabel
        Left = 296
        Top = 8
        Width = 48
        Height = 13
        Caption = '��������'
      end
      object m_BTN_get_schet: TBitBtn
        Left = 780
        Top = 0
        Width = 55
        Height = 28
        Action = m_ACT_get_schet_doc
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object m_DBP_shet_doc_number: TMLDBPanel
        Left = 104
        Top = 4
        Width = 97
        Height = 21
        DataField = 'SCHT_DOC_NUMBER'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 1
      end
      object m_MDB_sch_date: TMLDBPanel
        Left = 224
        Top = 3
        Width = 65
        Height = 21
        DataField = 'SCHT_DOC_DATE'
        DataSource = m_DS_item
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 2
      end
      object m_MDB_sch_from: TMLDBPanel
        Left = 352
        Top = 3
        Width = 201
        Height = 21
        DataField = 'SCHT_SRC_ONAME'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 3
      end
      object m_MDB_sch_to: TMLDBPanel
        Left = 572
        Top = 3
        Width = 201
        Height = 21
        DataField = 'SCHT_DST_ONAME'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 4
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1223
      Height = 141
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object m_L_buh_doc_note: TLabel
        Left = 0
        Top = 121
        Width = 97
        Height = 13
        Caption = '���������� � ���. '
        FocusControl = m_DBP_buh_doc_note
      end
      object m_L_mngr_note: TLabel
        Left = 44
        Top = 93
        Width = 53
        Height = 13
        Caption = '��������'
        FocusControl = m_DBP_mngr_note
      end
      object m_L_buh_doc_cnt: TLabel
        Left = 40
        Top = 66
        Width = 58
        Height = 13
        Caption = '���������'
        FocusControl = m_DBP_buh_doc_cnt
      end
      object m_L_buh_doc_number: TLabel
        Left = 20
        Top = 40
        Width = 71
        Height = 13
        Caption = '���. ��������'
        FocusControl = m_DBP_buh_doc_number
      end
      object m_L_buh_doc_date: TLabel
        Left = 208
        Top = 40
        Width = 11
        Height = 13
        Caption = '��'
        FocusControl = m_DBP_buh_doc_date
      end
      object m_L_buh_doc_id: TLabel
        Left = 292
        Top = 40
        Width = 11
        Height = 13
        Caption = 'ID'
        FocusControl = m_DBP_buh_doc_id
      end
      object m_L_buh_doc_amount: TLabel
        Left = 400
        Top = 40
        Width = 49
        Height = 13
        Caption = '�� �����'
        FocusControl = m_DBP_buh_doc_amount
      end
      object m_L_contract: TLabel
        Left = 560
        Top = 40
        Width = 44
        Height = 13
        Caption = '�������'
        FocusControl = m_DBP_buh_doc_amount
      end
      object m_L_doc_number: TLabel
        Left = 25
        Top = 12
        Width = 68
        Height = 13
        Caption = '� ���������'
      end
      object m_L_issued_date: TLabel
        Left = 224
        Top = 14
        Width = 70
        Height = 13
        Caption = '���� �������'
      end
      object m_L_gdp_name: TLabel
        Left = 400
        Top = 12
        Width = 31
        Height = 13
        Caption = '�����'
        FocusControl = m_LLV_gdp
      end
      object m_SB_view_source: TSpeedButton
        Left = 780
        Top = 70
        Width = 55
        Height = 32
        Action = m_ACT_view_source
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000C6C6C6008484840084848400848484008484840084848400848484008484
          84008484840084848400848484008484840000000000FF00FF00FF00FF00FF00
          FF0000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF008484840000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF008484840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
          FF008484840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C600FFFFFF00FFFFFF008484
          840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000C6C6C600848484000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object m_SBTN_view_source: TSpeedButton
        Left = 780
        Top = 106
        Width = 55
        Height = 32
        Action = m_ACT_income_diff
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FF00FF00C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FF00FF00FF00FF00C6C6C600FFFFFF00FFFF
          FF00FFFFFF00C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C6000000FF000000FF000000FF00C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF000000FF00000084000000FF000000FF00C6C6
          C600FFFFFF00C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00000084000000FF00000084000000FF000000
          FF00C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C600C6C6C600000084000000FF00000084000000
          FF000000FF00FF00FF00C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000FF000000
          FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
          84000000FF00000084000000840000008400FF00FF00FF00FF00C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
          C600C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FFFFFF00FFFF
          FF00FF00FF00FFFFFF00FFFFFF00FF00FF00FFFFFF00FFFFFF00FF00FF00FFFF
          FF00FFFFFF0084848400C6C6C600FF00FF00FF00FF00FF00FF00848484008484
          8400FFFFFF008484840084848400FFFFFF008484840084848400FFFFFF008484
          840084848400FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00}
        ParentFont = False
      end
      object m_L_ttn_number: TLabel
        Left = 552
        Top = 66
        Width = 36
        Height = 13
        Caption = '��� �'
      end
      object m_L_ttn_date: TLabel
        Left = 692
        Top = 66
        Width = 11
        Height = 13
        Caption = '��'
      end
      object m_DBP_buh_doc_note: TMLDBPanel
        Left = 104
        Top = 117
        Width = 669
        Height = 21
        DataField = 'ACC_NOTE'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 0
      end
      object m_DBP_mngr_note: TMLDBPanel
        Left = 104
        Top = 89
        Width = 669
        Height = 21
        DataField = 'MNGR_NOTE'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 1
      end
      object m_DBP_buh_doc_cnt: TMLDBPanel
        Left = 104
        Top = 62
        Width = 441
        Height = 21
        DataField = 'CNT_NAME'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 2
      end
      object m_DBP_buh_doc_number: TMLDBPanel
        Left = 104
        Top = 36
        Width = 97
        Height = 21
        DataField = 'ACC_DOC_NUMBER'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 3
      end
      object m_DBP_buh_doc_date: TMLDBPanel
        Left = 224
        Top = 36
        Width = 65
        Height = 21
        DataField = 'ACC_DOC_DATE'
        DataSource = m_DS_item
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 4
      end
      object m_DBP_buh_doc_id: TMLDBPanel
        Left = 304
        Top = 36
        Width = 89
        Height = 21
        DataField = 'ACC_DOC_ID'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 5
      end
      object m_DBP_buh_doc_amount: TMLDBPanel
        Left = 456
        Top = 36
        Width = 89
        Height = 21
        DataField = 'ACC_DOC_AMOUNT'
        DataSource = m_DS_item
        Alignment = taRightJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 6
      end
      object m_DBP_contract: TMLDBPanel
        Left = 609
        Top = 36
        Width = 164
        Height = 21
        DataField = 'CONTRACT_NOTE'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 7
      end
      object m_DBP_doc_number: TMLDBPanel
        Left = 104
        Top = 10
        Width = 81
        Height = 21
        DataField = 'DOC_NUMBER'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 8
      end
      object m_DBP_issued_date: TMLDBPanel
        Left = 304
        Top = 10
        Width = 89
        Height = 21
        DataField = 'DOC_DATE'
        DataSource = m_DS_item
        BevelOuter = bvLowered
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clInactiveBorder
        UseDockManager = True
        ParentColor = False
        TabOrder = 9
      end
      object m_LLV_gdp: TMLLovListView
        Left = 440
        Top = 8
        Width = 393
        Height = 21
        Lov = m_LOV_gdp
        UpDownWidth = 393
        UpDownHeight = 121
        Interval = 500
        TabStop = True
        TabOrder = 10
        ParentColor = False
      end
      object m_BBTN_get_acc_doc: TBitBtn
        Left = 780
        Top = 35
        Width = 55
        Height = 32
        Action = m_ACT_get_acc_doc
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 11
      end
      object m_DBP_ttn_number: TMLDBPanel
        Left = 592
        Top = 62
        Width = 97
        Height = 21
        DataField = 'TTN_NUMBER'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 12
      end
      object m_DBP_ttn_date: TMLDBPanel
        Left = 706
        Top = 62
        Width = 65
        Height = 21
        DataField = 'TTN_DATE'
        DataSource = m_DS_item
        BevelWidth = 0
        BorderWidth = 1
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 13
      end
    end
  end
  inherited m_DBG_lines: TMLDBGrid
    Left = 776
    Top = 320
    Width = 17
    Height = 16
    Align = alNone
    Enabled = False
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    TitleFont.Name = 'MS Sans Serif'
    Visible = False
    FooterRowCount = 1
    FooterFont.Name = 'MS Sans Serif'
    SumList.Active = True
    Columns = <
      item
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_lines_control: TPanel
    Top = 510
    Width = 1223
    object m_RSB_include_act_verif: TRxSpeedButton [0]
      Left = 464
      Top = 2
      Width = 137
      Height = 25
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      DropDownMenu = m_PM_include_act_verif
      Caption = '�� �������� � ������'
      OnClick = m_ACT_set_not_include_act_verificationExecute
    end
    inherited m_TB_lines_control_button: TToolBar
      Width = 461
      object m_RSB_DCT: TRxSpeedButton
        Left = 388
        Top = 2
        Width = 73
        Height = 25
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        DropDownMenu = m_PM_act_tcd
        Caption = '���'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000084000000840000008400000084
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000084000000FF000000FF00000084
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000084000000FF000000FF00000084
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000000000000000000000000000000000000084000000FF000000FF00000084
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000C6C6C6000084000000840000008400000084000000FF000000FF00000084
          00000084000000840000008400000084000000000000FF00FF00FF00FF000000
          0000C6C6C60000FF000000FF000000FF000000FF000000FF000000FF000000FF
          000000FF000000FF000000FF00000084000000000000FF00FF00FF00FF000000
          0000C6C6C60000FF000000FF000000FF000000FF000000FF000000FF000000FF
          000000FF000000FF000000FF00000084000000000000FF00FF00FF00FF000000
          0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C60000FF000000FF00000084
          00000084000000840000008400000084000000000000FF00FF00FF00FF000000
          000000000000000000000000000000000000C6C6C60000FF000000FF00000084
          00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C60000FF000000FF00000084
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C60000FF000000FF00000084
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000C6C6C600C6C6C600C6C6C600C6C6
          C60000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
          000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        Layout = blGlyphLeft
      end
    end
  end
  object m_P_spec: TPageControl [6]
    Left = 0
    Top = 337
    Width = 1223
    Height = 173
    ActivePage = m_TS_spec
    Align = alClient
    TabOrder = 6
    OnChange = m_P_specChange
    object m_TS_spec: TTabSheet
      Caption = '������������'
      object m_DBG_liness: TMLDBGrid
        Left = 0
        Top = 0
        Width = 1215
        Height = 145
        Align = alClient
        DataSource = m_DS_lines
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnEnter = m_DBG_linesEnter
        OnExit = m_DBG_linesExit
        FooterRowCount = 1
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        SumList.Active = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'LINE_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'NMCL_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_NAME'
            Title.TitleButton = True
            Width = 123
            Footers = <>
          end
          item
            FieldName = 'MEAS_NAME'
            Title.TitleButton = True
            Width = 58
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_NAME'
            Title.TitleButton = True
            Width = 93
            Footers = <>
          end
          item
            FieldName = 'IN_PRICE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'PERCENT'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'TAX_RATE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'OUT_PRICE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'QNT'
            Title.TitleButton = True
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            FieldName = 'SRC_AMOUNT'
            Title.TitleButton = True
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            FieldName = 'COUNTRY_NAME'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'SCD_NUM'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NOTE'
            Title.TitleButton = True
            Width = 106
            Footers = <>
          end
          item
            FieldName = 'STORED_DESC'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NOT_INCLUDE_ACT_VERIF'
            Title.TitleButton = True
            Width = 152
            KeyList.Strings = (
              '1'
              '0')
            Checkboxes = True
            Footers = <>
          end
          item
            FieldName = 'REASON_NAME'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'MAKE_DATE'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'TORG2'
            Title.TitleButton = True
            Width = 44
            Footers = <>
          end
          item
            FieldName = 'PROD_TYPE_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'PROD_NAME'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'PULL_DATE'
            Title.TitleButton = True
            Footers = <>
          end>
      end
    end
    object m_TS_diff: TTabSheet
      Caption = '���������� �� �����'
      ImageIndex = 1
      object m_DBG_diff: TMLDBGrid
        Left = 0
        Top = 0
        Width = 1215
        Height = 145
        Align = alClient
        DataSource = m_DS_diff
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterRowCount = 1
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        UseMultiTitle = True
        AutoFitColWidths = True
        SumList.Active = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        OnGetCellParams = m_DBG_diffGetCellParams
        Columns = <
          item
            FieldName = 'ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'LINE_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'NMCL_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_NAME'
            Title.TitleButton = True
            Width = 200
            Footers = <>
          end
          item
            FieldName = 'MEAS_NAME'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_NAME'
            Title.TitleButton = True
            Width = 150
            Footers = <>
          end
          item
            FieldName = 'IN_PRICE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'OUT_PRICE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'ARP'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'SCHET'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'DIFF'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'OUT_AMOUNT'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'SRC_AMOUNT'
            Title.TitleButton = True
            Footers = <>
          end>
      end
    end
  end
  inherited m_IL_main: TImageList
    Bitmap = {
      494C010119001D00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000008000000001002000000000000080
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000008400000084000000840000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000008400000084
      000000FF00000084000000840000008400000084000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000840000008400000084
      00000000000000FF000000840000008400000084000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF00000084000000FF
      0000000000000000000000FF0000008400000084000000840000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000000
      000000000000000000000000000000FF00000084000000840000008400008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000840000008400000084
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000008400000084
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000084
      0000008400008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000008400000084000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000084000000840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000008400000084000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000C6C6C6000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000000000000000000000000000C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000FF000000FF000000FF00C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000C6C6C6000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400000000000000000000000000000000000000000084000000008400000084
      0000008400000084000000C6420000C642000084000000840000008400000084
      000000840000008400000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00000084000000FF000000FF00C6C6C600FFFFFF00C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000C6C6C6000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      000000000000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000C6420000C6420000C6
      420000C6420000C6420000C6420000C6420000C6420000C6420000C6420000C6
      420000C64200008400000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000084000000FF00000084000000FF000000FF00C6C6C600C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600FFFFFF00FFFFFF0084848400000000000000
      0000000000000000000000000000000000000000000000C6420000C6420000C6
      420000C6420000C6420000C6420000C6420000C6420000C6420000C6420000C6
      420000C64200000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600000084000000FF00000084000000FF000000FF000000
      0000C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C6008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000084000000FF000000FF000000FF000000
      FF0000000000FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000FF000000
      8400000084000000840000000000FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000C6420000C642000084000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6
      C600C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000C6420000C642000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF008484
      8400C6C6C600FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400FFFFFF008484
      840084848400FFFFFF008484840084848400FFFFFF008484840084848400FFFF
      FF0000000000FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000C6C6C6000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      000000840000008400000000000000000000000000000000000000000000C6C6
      C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00008484000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      0000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000084
      840000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      000000000000C6C6C60000FFFF0000FFFF0000FFFF0000FFFF00008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FF000000FF000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FFFF0000FFFF0000848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C6000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084000000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000084
      000000840000008400000084000000FF000000FF000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C60000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FF000000FF000000840000008400000084
      000000840000008400000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C60000FF000000FF000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      840000000000FFFFFF0000000000000000008484840084848400848484000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFF0000848484008484
      8400848484008484840084848400848484008484840084848400000000008484
      0000FFFF0000FFFF0000FFFFFF00000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000FFFF00008484
      000084840000848400008484000084840000848400000000000084840000FFFF
      0000FFFF0000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF000000000000000000000000008484840000000000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000848400008484000084840000000000000000000084840000FFFF0000FFFF
      0000FFFFFF000000000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      840000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFF000084840000000000008484000084840000FFFF0000FFFF0000FFFF
      FF00000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000000000
      FF000000FF000000FF00000000000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      8400FFFFFF008484840000000000000000008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000FFFF0000FFFF0000FFFF0000FFFFFF000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000840000000000FF000000
      FF000000FF000000FF0000000000000000000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      840000000000848484000000000000000000FFFFFF0084848400000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000084840000FFFF0000FFFF0000FFFF0000FFFFFF00000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000840000000000FF000000
      FF000000FF00000000000000000000000000000000000000FF000000FF000000
      FF000000000000000000000000000000000000000000FFFFFF00000000008484
      8400FFFFFF008484840000000000000000008484840084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000000000
      000084840000FFFF0000FFFF0000FFFFFF00FFFFFF0000000000848400008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000008484
      0000FFFF0000FFFF0000FFFFFF000000000000000000FFFF0000848400008484
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF0000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF0000000000848484000000000000000000000000000000000084840000FFFF
      0000FFFF0000FFFFFF0000000000000000000000000000000000FFFF00008484
      0000848400008484840000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF00000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000084848400848484008484
      840084848400848484000000FF000000FF008484840084848400000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484000000000000000000848484008484840084848400848484008484
      8400840000008400000084000000000000000000000084848400848484008484
      840084848400000000000000FF000000FF008484840084848400848484008484
      8400840000008400000084000000000000000000000084848400848484008484
      8400848484000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000000000000000000000000000
      0000848400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000848484000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF008484
      84000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000008484000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF000000000084848400000000000000000084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF000000
      00000000FF000000FF000000FF000000FF000000000084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF00000000008484
      84000000FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF000000
      00000000000000000000848400000000000000000000FFFFFF00000000000000
      000000000000000000000000000000000000000000008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000000000
      00000000FF000000FF000000FF000000FF000000FF008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400000000000000000084848400848484008484
      8400000000000000000000000000000000000000000000000000FFFFFF000000
      FF000000FF000000FF00848484000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF000000
      FF000000FF000000FF00848484000000FF000000FF000000FF00FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF0000000000848484000000000000000000FFFFFF00848484000000
      00000000000000000000000000000000000000000000FFFFFF000000FF000000
      FF000000FF000000FF0084848400000000000000FF000000FF000000FF00FFFF
      FF000000000084848400000000000000000000000000FFFFFF00000000000000
      000000000000000000008484840000000000000000000000000000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF000000FF000000
      FF000000FF000000FF0084848400000000000000FF000000FF000000FF00FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF008484840000000000000000008484840084848400FFFF
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF00000000008484840000000000FFFFFF000000FF000000FF000000
      FF00000000008484840000000000000000000000000000000000FFFFFF008484
      8400000000000000000084848400000000000000000084848400FFFFFF000000
      00000000000084000000000000000000000000000000000000000000FF000000
      FF000000FF00000000008484840000000000000000000000FF000000FF000000
      FF00000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF0000000000848484000000FF000000
      FF000000000084848400000000000000000000000000FFFFFF00000000008484
      840000000000000000000000000000000000000000008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      84000000000000000000000000000000000000000000848484000000FF000000
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF00848484000000FF000000
      FF000000FF008484840000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF00848484000000FF000000
      FF000000FF008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF000000000084848400000000000000
      FF000000FF000000FF00000000000000000000000000FFFFFF00000000008484
      840000000000000000000000000000000000000000008484840000000000FFFF
      FF000000000084000000000000000000000000000000FFFFFF00000000008484
      8400000000000000000000000000000000000000000084848400000000000000
      FF000000FF000000FF00000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000FF000000FF000000FF00000000000000000000000000FFFFFF008484
      8400FFFFFF00000000000000000000000000FFFFFF0084848400FFFFFF000000
      0000000000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000000000000000000000FFFFFF0084848400FFFFFF000000
      00000000FF000000FF000000FF00000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000848484000000FF00000000008400000084000000840000008400
      0000840000008400000000000000840000008400000084000000840000008400
      0000840000008400000000000000000000008400000084000000840000008400
      0000840000008400000000000000840000008400000084000000840000008400
      000084000000840000000000FF00000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008484840000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      84008484840000000000000000000000000000000000000000000000FF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000840000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      000000000000000000000000000000000000000000000000FF00000084000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400848484000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400000084008484840000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8400000084000000840000008400848484000000840000008400000084000000
      8400000084008484840000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400000000000000000000000000000000000000000000000000000000
      FF00000084000000840000008400000084000000840000008400000084000000
      8400848484000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000008484000000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084000000840000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF00000000000000000084840000FFFFFF00FFFFFF000000
      0000000000000000000084840000000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000000000000000000000000000FFFFFF008484
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000000000
      FF00000084000000840000008400848484000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      00008400000084000000840000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF000000000084848400000000000000000000000000000000000000FF000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400848484000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      00000000000084848400000000000000000000000000000000000000FF000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400000084008484840000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000840000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0084848400FFFFFF00000000008484840000000000FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      FF000000840000000000000000000000000000000000000000000000FF000000
      8400000084000000840000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000FFFFFF008484
      8400FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000000000000000000084000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000840000008400000084000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000800000000100010000000000000400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF000000000000F3FF000000000000
      F1FF000000000000E0FF000000000000803F000000000000883F000000000000
      8C1F000000000000DE0F000000000000FF0F000000000000FF87000000000000
      FFC3000000000000FFE1000000000000FFF1000000000000FFFB000000000000
      FFFF000000000000FFFF000000000000FFFFFFFFFFFFFFF0FFFFFFFFFFFF0000
      FFFFFFFF8E7F0000FFFFFFFF8C010000FFFF80018C7F080080018001FC7F0600
      8001C003FC7F00008001E007800300008001F00F800100008001F81F80070010
      8001FC3FFC7F0008FFFFFE7FFC7F0002FFFFFFFF8C7F0000FFFFFFFF8C010000
      FFFFFFFF8CFF2480FFFFFFFFFFFF0008FFFFFC00FFFFFFFFFFFFF000F81FFFFF
      FFFFC000F81FFFFFFFFF0000F81FFFFFFCFF0000F81F8001FC3F000080018001
      FC0F00008001C003000300008001E007000000008001F00F000300008001F81F
      FC0F00018001FC3FFC3F0003F81FFE7FFCFF0007F81FFFFFFFFF001FF81FFFFF
      FFFF007FF81FFFFFFFFF01FFFFFFFFFFFFFFFFFFFFFFFFFFF81FC007000FFFFF
      F81FBFEB000FFFFFF81F0005000FFFFFF81F7E31000FFF3F80017E35000FFC3F
      80010006000FF03F80017FEA000FC00080018014008F00008001C00A1144C000
      8001E0010AB8F03FF81FE007057CFC3FF81FF007FAFCFF3FF81FF003FDF8FFFF
      F81FF803FE04FFFFFFFFFFFFFFFFFFFFFFEAFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      F0E1FFF08FFF8FFF800100008C018C01000100008FFF88FF40018001FFFFF0FF
      2001C000FFFFF07F4001E0008FFF823F2003F00F8C0180014823F00F8FFF878F
      2003E007FFFFFFCF4513C003FFFFFFC728A381818FFF8FE3451303C08C018C01
      000307E08FFF8FFD0007FFFFFFFFFFFFFFC7E07BE07BF870FFC7E77BE77BF870
      FFD7F3F1F3F1F870801780018001800000330000000000004079441340134000
      201C202320232801400045134013441300032023000328034503451345034513
      28832023200328A3450340134003451328A328A328A328A34511451345114513
      00010003000100030007000700070007CFFFFFFFFFFFFFC787CFC001F83FFFC7
      83878031E00FFFD781038031C7C78017C00380318FE30033E00780019FF34779
      F00F80013FF92A1CF81F80013FF94400F81F8FF13FFF28A3F01F8FF13FFF4513
      E00F8FF13FC128A3C1078FF19FE14513C3838FF18FF128A3E7C38FF5C7C14513
      FFE38001E00D0003FFFFFFFFF83F000700000000000000000000000000000000
      000000000000}
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_get_acc_doc: TAction
      Category = 'Income'
      Caption = '...'
      Hint = '������� ������������� ��������'
      OnExecute = m_ACT_get_acc_docExecute
      OnUpdate = m_ACT_get_acc_docUpdate
    end
    object m_ACT_set_not_include_act_verification: TAction
      Category = 'Income'
      Caption = '�� �������� � ������'
      OnExecute = m_ACT_set_not_include_act_verificationExecute
      OnUpdate = m_ACT_set_not_include_act_verificationUpdate
    end
    object m_ACT_set_include_act_verification: TAction
      Category = 'Income'
      Caption = '�������� � ������'
      OnExecute = m_ACT_set_include_act_verificationExecute
      OnUpdate = m_ACT_set_include_act_verificationUpdate
    end
    object m_ACT_load_act_tcd: TAction
      Category = 'Income'
      Caption = '��������� � ����� ������� ���'
      ImageIndex = 18
      OnExecute = m_ACT_load_act_tcdExecute
      OnUpdate = m_ACT_load_act_tcdUpdate
    end
    object m_ACT_clear_act_tcd: TAction
      Category = 'Income'
      Caption = '�������� ����� � ������ ������� �� ���'
      ImageIndex = 20
      OnExecute = m_ACT_clear_act_tcdExecute
      OnUpdate = m_ACT_clear_act_tcdUpdate
    end
    object m_ACT_set_corr_amt: TAction
      Category = 'Income'
      Hint = '��������...'
      ImageIndex = 19
      OnExecute = m_ACT_set_corr_amtExecute
      OnUpdate = m_ACT_set_corr_amtUpdate
    end
    object m_ACT_view_source: TAction
      Category = 'Income'
      Hint = '�������� ���. ���.'
      ImageIndex = 21
      OnExecute = m_ACT_view_sourceExecute
      OnUpdate = m_ACT_view_sourceUpdate
    end
    object m_ACT_rtl_bill_link: TAction
      Category = 'Income'
      Caption = '��������� ����'
      Hint = '��������� ����'
      ImageIndex = 22
      OnExecute = m_ACT_rtl_bill_linkExecute
      OnUpdate = m_ACT_rtl_bill_linkUpdate
    end
    object m_ACT_get_schet_doc: TAction
      Category = 'Income'
      Caption = '...'
      OnExecute = m_ACT_get_schet_docExecute
      OnUpdate = m_ACT_get_schet_docUpdate
    end
    object m_ACT_income_diff: TAction
      Category = 'Income'
      Caption = '���'
      Hint = '��������� ��� ����������� ���'
      ImageIndex = 23
      OnExecute = m_ACT_income_diffExecute
      OnUpdate = m_ACT_income_diffUpdate
    end
    object m_ACT_sign: TAction
      Category = 'Income'
      Caption = '�������� ��� �����'
      ImageIndex = 24
      OnExecute = m_ACT_signExecute
      OnUpdate = m_ACT_signUpdate
    end
  end
  inherited m_DS_lines: TDataSource
    DataSet = DIncome.m_Q_lines
    Left = 24
    Top = 344
  end
  inherited m_DS_item: TDataSource
    DataSet = DIncome.m_Q_item
  end
  object m_DS_gdp: TDataSource
    DataSet = DIncome.m_Q_gdp_names
    Left = 544
    Top = 26
  end
  object m_LOV_gdp: TMLLov
    DataFieldKey = 'GR_DEP_ID'
    DataFieldValue = 'GDP_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'GD_ID'
    DataFieldValues = 'GD_NAME'
    DataSourceList = m_DS_gdp
    AutoOpenList = True
    Left = 576
    Top = 26
  end
  object m_DS_receivers: TDataSource
    DataSet = DIncome.m_Q_receivers
    Left = 304
    Top = 226
  end
  object m_LOV_receivers: TMLLov
    DataFieldKey = 'RECEIVER_ID'
    DataFieldValue = 'RECEIVER_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_receivers
    AutoOpenList = True
    Left = 336
    Top = 226
  end
  object m_PM_include_act_verif: TPopupMenu
    Left = 480
    Top = 431
    object N2: TMenuItem
      Action = m_ACT_set_not_include_act_verification
    end
    object N1: TMenuItem
      Action = m_ACT_set_include_act_verification
    end
  end
  object m_BCS_item: TDLBCScaner
    Tick = 50
    BeginKey = 2
    EndKey = 3
    BeginAlternateKeys.Strings = (
      '8')
    EndAlternateKeys.Strings = (
      '13')
    OnBegin = m_BCS_itemBegin
    OnEnd = m_BCS_itemEnd
    Top = 64
  end
  object m_PM_act_tcd: TPopupMenu
    Images = m_IL_main
    Left = 368
    Top = 437
    object m_MI_load_act_tcd: TMenuItem
      Action = m_ACT_load_act_tcd
    end
    object m_MI_clear_act_tcd: TMenuItem
      Action = m_ACT_clear_act_tcd
    end
    object m_MI_add_bill: TMenuItem
      Action = m_ACT_rtl_bill_link
    end
  end
  object m_DS_diff: TDataSource
    DataSet = DIncome.m_Q_lines_diff
    Left = 104
    Top = 344
  end
  object m_TSRC_blt_rep: TTSReportContainer
    ProgId = 'TSReportServer.TSFReport'
    Left = 324
    Top = 362
  end
end
