//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlPlanMarginSeedItem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

AnsiString __fastcall TFRtlPlanMarginSeedItem::BeforeItemApply(TWinControl **p_focused)
{
  TDRtlPlanMarginSeed *dm = static_cast<TDRtlPlanMarginSeed*>(DMRefbook);

  if( dm->UpdateRegimeItem == urDelete ) return "";

  return "";
}
//---------------------------------------------------------------------------


