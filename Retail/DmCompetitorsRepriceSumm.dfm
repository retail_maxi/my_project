inherited DCompetitorsRepriceSumm: TDCompetitorsRepriceSumm
  Left = 371
  Top = 275
  Height = 640
  Width = 870
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'SELECT d.id AS id,'
      '       d.src_org_id AS doc_src_org_id,'
      '       o.name AS doc_src_org_name,'
      '       d.create_date AS doc_create_date,'
      '       d.comp_name AS doc_create_comp_name,'
      '       d.author AS doc_create_user,'
      '       d.modify_date AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user AS doc_modify_user,'
      '       d.status AS doc_status,'
      
        '       decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �' +
        '� ������'#39','#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) AS doc' +
        '_fldr_right,'
      '       dr.doc_date,'
      '       dr.doc_number,'
      '       dr.org_group_id as org_group_id,'
      '       dr.note,'
      '       og.name as org_group_name'
      'FROM documents d,'
      '     folders f,'
      '     organizations o,'
      '     org_groups og,'
      '     doc_comp_summ_reprices dr'
      'WHERE f.doc_type_id = :DOC_TYPE_ID'
      '  AND f.id = d.fldr_id'
      '  AND o.id = d.src_org_id'
      '  AND fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '  AND dr.doc_id = d.id'
      '  AND og.id = dr.org_group_id'
      '  AND dr.doc_date BETWEEN :BEGIN_DATE AND :END_DATE'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '����� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'dr.doc_number'
      Size = 50
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'dr.note'
      Size = 250
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'DOC_DATE'
      Origin = 'dr.doc_date'
    end
    object m_Q_listORG_GROUP_ID: TFloatField
      DisplayLabel = 'ID �����'
      FieldName = 'ORG_GROUP_ID'
      Origin = 'dr.org_group_id'
    end
    object m_Q_listORG_GROUP_NAME: TStringField
      DisplayLabel = '����'
      FieldName = 'ORG_GROUP_NAME'
      Origin = 'og.name'
      Size = 250
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'SELECT '
      '    dr.doc_id as id,'
      '    dr.doc_date,'
      '    dr.doc_number,'
      '    dr.org_group_id as org_group_id,'
      '    dr.note,'
      '    og.name as org_group_name'
      'FROM org_groups og,'
      '     doc_comp_summ_reprices dr'
      'WHERE dr.doc_id = :ID'
      
        '  AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_id' +
        ' = :FLDR_ID) '
      '  AND og.id = dr.org_group_id'
      '')
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 50
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemORG_GROUP_ID: TFloatField
      FieldName = 'ORG_GROUP_ID'
    end
    object m_Q_itemORG_GROUP_NAME: TStringField
      FieldName = 'ORG_GROUP_NAME'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_comp_summ_reprices'
      'set'
      '  ORG_GROUP_ID = :ORG_GROUP_ID,'
      '  DOC_NUMBER = :DOC_NUMBER,'
      '  DOC_DATE = :DOC_DATE,'
      '  NOTE = :NOTE'
      'where'
      '  DOC_ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into doc_comp_summ_reprices'
      '  (DOC_ID, ORG_GROUP_ID, DOC_NUMBER, DOC_DATE, NOTE)'
      'values'
      '  (:ID, :ORG_GROUP_ID, :DOC_NUMBER, :DOC_DATE, :NOTE)')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select * from ('
      ' select :ID AS id,'
      '       i.line_id, '
      '       ni.id as nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       a.id assort_id,'
      '       a.name assort_name,'
      '       mu.name as mu_name,'
      '       i.min_in_price,'
      '       i.max_in_price,'
      '       i.avg_weight_prcie,'
      
        '       round(abs(decode(i.avg_weight_prcie,0,null,(i.avg_weight_' +
        'prcie-i.min_in_price)/ i.avg_weight_prcie))*100,2) AS percent_mi' +
        'n,'
      
        '       round(abs(decode(i.avg_weight_prcie,0,null,(i.max_in_pric' +
        'e-i.avg_weight_prcie)/ i.avg_weight_prcie))*100,2) AS percent_ma' +
        'x,'
      '       '
      
        '       (select min(pkg_rtl_prices.fnc_cur_out_price(i.nmcl_id,i.' +
        'assort_id,g.org_id)) '
      '         from groups_organizations g, organizations o'
      '        where g.org_id = o.id'
      '          and g.grp_id = 19'
      '        ) min_org_gproup_price,'
      
        '        (select min(pkg_rtl_prices.fnc_cur_out_price(i.nmcl_id,i' +
        '.assort_id,g.org_id))'
      '         from groups_organizations g, organizations o'
      '        where g.org_id = o.id'
      '          and g.grp_id = d.org_group_id'
      '        ) max_org_gproup_price,'
      '        i.min_comp_price,'
      '        i.max_comp_price,'
      '        i.resolve_price,'
      '        (select decode(kvi, '#39'Y'#39', 1, null) from cur_tag_pricing'
      
        '         where nmcl_id = ni.id and org_gr_id = (SELECT max(og.id' +
        ')  '
      #9'  FROM org_groups og, groups_organizations goo'
      #9'  WHERE og.type_id = 7'
      #9'    AND og.id = goo.grp_id'
      #9'    AND goo.org_id IN (SELECT goo2.org_id'
      #9'                  FROM groups_organizations goo2'
      
        #9'                    WHERE goo2.grp_id = :ORG_GROUP_ID))) as kvi' +
        ','
      
        '        (select decode(keysku, '#39'Y'#39', 1, null) from cur_tag_pricin' +
        'g'
      
        '         where nmcl_id = ni.id and org_gr_id = (SELECT max(og.id' +
        ')'
      #9'  FROM org_groups og, groups_organizations goo'
      #9'  WHERE og.type_id = 7'
      #9'    AND og.id = goo.grp_id'
      #9'    AND goo.org_id IN (SELECT goo2.org_id'
      #9'                  FROM groups_organizations goo2'
      
        #9'                    WHERE goo2.grp_id = :ORG_GROUP_ID))) as key' +
        'sku,'
      '        (select decode(epp, '#39'Y'#39', 1, null) from cur_tag_pricing'
      
        '         where nmcl_id = ni.id and org_gr_id = (SELECT max(og.id' +
        ')'
      #9'  FROM org_groups og, groups_organizations goo'
      #9'  WHERE og.type_id = 7'
      #9'    AND og.id = goo.grp_id'
      #9'    AND goo.org_id IN (SELECT goo2.org_id'
      #9'                  FROM groups_organizations goo2'
      #9'                    WHERE goo2.grp_id = :ORG_GROUP_ID))) as epp'
      'from nomenclature_items ni,'
      '     measure_units mu,'
      '     assortment a,'
      '     doc_comp_summ_reprice_items i,'
      '     doc_comp_summ_reprices d'
      'where  i.doc_id = :ID'
      '    and i.nmcl_id = ni.id'
      '    and i.assort_id = a.id'
      '    and d.doc_id = i.doc_id'
      '    and :ORG_GROUP_ID = :ORG_GROUP_ID'
      '--    and ni.prod_type_id = 464'
      '    AND mu.id = ni.meas_unit_id'
      '  --  and store_nmcls.nmcl_id = ni.id'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 376
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'ID'
    end
    inherited m_Q_linesLINE_ID: TFloatField
      Visible = False
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_linesASSORT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'ASSORT_ID'
      Origin = 'ASSORT_ID'
    end
    object m_Q_linesASSORT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORT_NAME'
      Origin = 'ASSORT_NAME'
      Size = 100
    end
    object m_Q_linesMU_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MU_NAME'
      Origin = 'MU_NAME'
      Size = 30
    end
    object m_Q_linesMIN_IN_PRICE: TFloatField
      DisplayLabel = '���� �������|min'
      FieldName = 'MIN_IN_PRICE'
    end
    object m_Q_linesMAX_IN_PRICE: TFloatField
      DisplayLabel = '���� �������|max'
      FieldName = 'MAX_IN_PRICE'
    end
    object m_Q_linesPERCENT_MIN: TFloatField
      DisplayLabel = '���������� %|min'
      FieldName = 'PERCENT_MIN'
    end
    object m_Q_linesPERCENT_MAX: TFloatField
      DisplayLabel = '���������� %|max'
      FieldName = 'PERCENT_MAX'
    end
    object m_Q_linesAVG_WEIGHT_PRCIE: TFloatField
      DisplayLabel = '���������������� ���������� ����'
      FieldName = 'AVG_WEIGHT_PRCIE'
    end
    object m_Q_linesMIN_ORG_GPROUP_PRICE: TFloatField
      DisplayLabel = '��������� ���� �����|min'
      FieldName = 'MIN_ORG_GPROUP_PRICE'
    end
    object m_Q_linesMAX_ORG_GPROUP_PRICE: TFloatField
      DisplayLabel = '��������� ���� �����|max'
      FieldName = 'MAX_ORG_GPROUP_PRICE'
    end
    object m_Q_linesMIN_COMP_PRICE: TFloatField
      DisplayLabel = '���� �����������|min'
      FieldName = 'MIN_COMP_PRICE'
      Origin = 'MIN_COMP_PRICE'
    end
    object m_Q_linesMAX_COMP_PRICE: TStringField
      DisplayLabel = '���� �����������|max'
      FieldName = 'MAX_COMP_PRICE'
      Origin = 'MAX_COMP_PRICE'
      FixedChar = True
      Size = 18
    end
    object m_Q_linesRESOLVE_PRICE: TStringField
      DisplayLabel = '�������'
      FieldName = 'RESOLVE_PRICE'
      Origin = 'RESOLVE_PRICE'
      FixedChar = True
      Size = 18
    end
    object m_Q_linesKVI: TFloatField
      DisplayLabel = '������� �� ��������������� | KVI'
      FieldName = 'KVI'
      Origin = 'KVI'
    end
    object m_Q_linesKEYSKU: TFloatField
      DisplayLabel = '������� �� ��������������� | KEYSKU'
      FieldName = 'KEYSKU'
      Origin = 'KEYSKU'
    end
    object m_Q_linesEPP: TFloatField
      DisplayLabel = '������� �� ��������������� | EPP'
      FieldName = 'EPP'
      Origin = 'EPP'
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'SELECT l.doc_id AS id, '
      '       l.line_id, '
      '       l.nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       l.assort_id,'
      '       a.name as assort_name,'
      '       l.resolve_price'
      'FROM doc_comp_summ_reprice_items l,'
      '     nomenclature_items ni,'
      '     assortment a'
      'WHERE l.doc_id = :ID     '
      '      AND l.line_id = :LINE_ID'
      '       AND ni.id = l.nmcl_id '
      '      AND a.id(+) = l.assort_id')
    inherited m_Q_lineID: TFloatField
      Visible = False
    end
    inherited m_Q_lineLINE_ID: TFloatField
      Visible = False
    end
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_lineASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
    end
    object m_Q_lineASSORT_NAME: TStringField
      FieldName = 'ASSORT_NAME'
      Size = 100
    end
    object m_Q_lineRESOLVE_PRICE: TStringField
      FieldName = 'RESOLVE_PRICE'
      FixedChar = True
      Size = 18
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      '   update doc_comp_summ_reprice_items'
      '   set resolve_price = :RESOLVE_PRICE'
      '   where doc_id = :ID '
      '      and line_id = :LINE_ID'
      '')
    InsertSQL.Strings = (
      '   update doc_comp_summ_reprice_items'
      '   set resolve_price = :RESOLVE_PRICE'
      '   where doc_id = :ID '
      '      and line_id = :LINE_ID')
    DeleteSQL.Strings = (
      '  delete from doc_comp_summ_reprice_items'
      '  where doc_id = :OLD_ID and'
      '      line_id = :OLD_LINE_ID')
    IgnoreRowsAffected = True
  end
  object m_Q_lines_start: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'with cc as('
      
        '      select cg.id as comp_id, ri.nmcl_id, min(comp_price) min_c' +
        'omp_price, max(comp_price) max_comp_price '
      '          from doc_comp_prices p, '
      '               doc_comp_reprice r, '
      '               doc_comp_reprice_items ri,'
      '               documents d,'
      '               competitors c,'
      '               document_links dl,'
      '               competitor_groups cg'
      '        where d.id = r.doc_id '
      '          and r.doc_id = ri.doc_id'
      '          and ri.doc_id = p.doc_id'
      '          and ri.line_id = p.line_id          '
      '          and c.id = p.comp_id'
      '          and c.group_id = cg.id'
      '          and dl.src_doc_id = :ID'
      '          and dl.dest_doc_id = d.id'
      '          and cg.org_group_id =:ORG_GROUP_ID'
      '        group by cg.id, ri.nmcl_id'
      ')'
      'select * from ('
      'select :ID AS id, '
      '       i.line_id, '
      '       ni.id as nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       a.id assort_id,'
      '       a.name assort_name,'
      '       mu.name as mu_name,'
      '       i.min_in_price,'
      '       i.max_in_price,'
      '       i.avg_weight_prcie,'
      
        '       round(abs(decode(i.avg_weight_prcie,0,null,(i.avg_weight_' +
        'prcie-i.min_in_price)/ i.avg_weight_prcie))*100,2) AS percent_mi' +
        'n,'
      
        '       round(abs(decode(i.avg_weight_prcie,0,null,(i.max_in_pric' +
        'e-i.avg_weight_prcie)/ i.avg_weight_prcie))*100,2) AS percent_ma' +
        'x,'
      '       '
      
        '       (select min(pkg_rtl_prices.fnc_cur_out_price(i.nmcl_id,i.' +
        'assort_id,g.org_id)) '
      '         from groups_organizations g, organizations o'
      '        where g.org_id = o.id'
      '          and g.grp_id = d.org_group_id'
      '          and o.enable = '#39'Y'#39
      '          and org_type_id = 2'
      
        '          and pkg_rtl_prices.fnc_cur_out_price(i.nmcl_id,i.assor' +
        't_id,g.org_id) != 0'
      '        ) min_org_gproup_price,'
      
        '        (select max(pkg_rtl_prices.fnc_cur_out_price(i.nmcl_id,i' +
        '.assort_id,g.org_id))'
      '         from groups_organizations g, organizations o'
      '        where g.org_id = o.id'
      '          and g.grp_id = d.org_group_id'
      '          and o.enable = '#39'Y'#39
      '          and org_type_id = 2'
      '        ) max_org_gproup_price,'
      '        i.min_comp_price,'
      '        i.max_comp_price,'
      '        i.resolve_price,'
      '        (select decode(kvi, '#39'Y'#39', 1, null) from cur_tag_pricing'
      
        '         where nmcl_id = ni.id and org_gr_id = (SELECT max(og.id' +
        ')'
      #9'  FROM org_groups og, groups_organizations goo'
      #9'  WHERE og.type_id = 7'
      #9'    AND og.id = goo.grp_id'
      #9'    AND goo.org_id IN (SELECT goo2.org_id'
      #9'                  FROM groups_organizations goo2'
      
        #9'                    WHERE goo2.grp_id = :ORG_GROUP_ID))) as kvi' +
        ','
      
        '        (select decode(keysku, '#39'Y'#39', 1, null) from cur_tag_pricin' +
        'g'
      
        '         where nmcl_id = ni.id and org_gr_id = (SELECT max(og.id' +
        ')'
      #9'  FROM org_groups og, groups_organizations goo'
      #9'  WHERE og.type_id = 7'
      #9'    AND og.id = goo.grp_id'
      #9'    AND goo.org_id IN (SELECT goo2.org_id'
      #9'                  FROM groups_organizations goo2'
      
        #9'                    WHERE goo2.grp_id = :ORG_GROUP_ID))) as key' +
        'sku,'
      '        (select decode(epp, '#39'Y'#39', 1, null) from cur_tag_pricing'
      
        '         where nmcl_id = ni.id and org_gr_id = (SELECT max(og.id' +
        ')'
      #9'  FROM org_groups og, groups_organizations goo'
      #9'  WHERE og.type_id = 7'
      #9'    AND og.id = goo.grp_id'
      #9'    AND goo.org_id IN (SELECT goo2.org_id'
      #9'                  FROM groups_organizations goo2'
      
        #9'                    WHERE goo2.grp_id = :ORG_GROUP_ID))) as epp' +
        ' '
      ''
      ' '
      ' ')
    Macros = <>
    Left = 264
    Top = 478
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end>
    object m_Q_lines_startID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_lines_startLINE_ID: TFloatField
      FieldName = 'LINE_ID'
    end
    object m_Q_lines_startNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_lines_startNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_lines_startASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
    end
    object m_Q_lines_startASSORT_NAME: TStringField
      FieldName = 'ASSORT_NAME'
      Size = 100
    end
    object m_Q_lines_startMU_NAME: TStringField
      FieldName = 'MU_NAME'
      Size = 30
    end
    object m_Q_lines_startMIN_IN_PRICE: TFloatField
      FieldName = 'MIN_IN_PRICE'
    end
    object m_Q_lines_startMAX_IN_PRICE: TFloatField
      FieldName = 'MAX_IN_PRICE'
    end
    object m_Q_lines_startAVG_WEIGHT_PRCIE: TFloatField
      FieldName = 'AVG_WEIGHT_PRCIE'
    end
    object m_Q_lines_startPERCENT_MIN: TStringField
      FieldName = 'PERCENT_MIN'
      Size = 40
    end
    object m_Q_lines_startPERCENT_MAX: TStringField
      FieldName = 'PERCENT_MAX'
      Size = 40
    end
    object m_Q_lines_startMIN_ORG_GPROUP_PRICE: TFloatField
      FieldName = 'MIN_ORG_GPROUP_PRICE'
    end
    object m_Q_lines_startMAX_ORG_GPROUP_PRICE: TFloatField
      FieldName = 'MAX_ORG_GPROUP_PRICE'
    end
    object m_Q_lines_startMIN_COMP_PRICE: TFloatField
      FieldName = 'MIN_COMP_PRICE'
    end
    object m_Q_lines_startMAX_COMP_PRICE: TStringField
      FieldName = 'MAX_COMP_PRICE'
      FixedChar = True
      Size = 18
    end
    object m_Q_lines_startRESOLVE_PRICE: TStringField
      FieldName = 'RESOLVE_PRICE'
      FixedChar = True
      Size = 18
    end
    object m_Q_lines_startKVI: TFloatField
      FieldName = 'KVI'
    end
    object m_Q_lines_startKEYSKU: TFloatField
      FieldName = 'KEYSKU'
    end
    object m_Q_lines_startEPP: TFloatField
      FieldName = 'EPP'
    end
  end
  object m_Q_lines_end: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'from nomenclature_items ni,'
      '     measure_units mu,'
      '     assortment a,     '
      '     doc_comp_summ_reprice_items i,'
      '     doc_comp_summ_reprices d   '
      'where  i.doc_id = :ID'
      '    and i.nmcl_id = ni.id '
      '    and i.assort_id = a.id '
      '    and d.doc_id = i.doc_id'
      '--    and ni.prod_type_id = 464'
      '    AND mu.id = ni.meas_unit_id'
      '  --  and store_nmcls.nmcl_id = ni.id'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 344
    Top = 478
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_org_group: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select og.id, og.name'
      'from org_groups og'
      'where og.id = 19')
    Macros = <>
    Left = 176
    Top = 480
    object m_Q_org_groupID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_org_groupNAME: TStringField
      FieldName = 'NAME'
      Size = 250
    end
  end
  object m_Q_org_groups: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name '
      'from org_groups'
      'where type_id = 2378'
      '%WHERE_CLAUSE'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 56
    Top = 312
    object m_Q_org_groupsID: TFloatField
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_org_groupsNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'name'
      Size = 250
    end
  end
  object m_Q_comp_one: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select'
      '    c.name as comp_name'
      'from '
      '    competitors c'
      'where c.id = :COMP_ID')
    Macros = <>
    Left = 152
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COMP_ID'
        ParamType = ptInput
      end>
    object m_Q_comp_oneCOMP_NAME: TStringField
      FieldName = 'COMP_NAME'
      Origin = 'TSDBMAIN.COMPETITORS.NAME'
      Size = 250
    end
  end
  object m_Q_competitors: TMLQuery
    BeforeOpen = m_Q_competitorsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select cg.id, cg.org_group_id, cg.name'
      'from competitor_groups cg'
      'where '
      '    cg.org_group_id = :ORG_GROUP_ID'
      '    and '
      '   (cg.enabled = '#39'Y'#39' OR EXISTS (select 1 '
      
        '                                  from doc_comp_prices dcp, docu' +
        'ment_links dl, competitors c                                 '
      
        '                                  where dcp.doc_id = dl.dest_doc' +
        '_id '
      '                                    AND dcp.comp_id = cg.id'
      '                                    AND cg.id = c.group_id'
      '                                    AND dl.src_doc_id =:ID '
      '                                    AND dl.rel_type_id = 1'
      '                                  ))')
    Macros = <>
    Left = 56
    Top = 376
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object m_Q_competitorsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.COMPETITORS.ID'
    end
    object m_Q_competitorsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.COMPETITORS.NAME'
      Size = 250
    end
    object m_Q_competitorsORG_GROUP_ID: TFloatField
      FieldName = 'ORG_GROUP_ID'
      Origin = 'TSDBMAIN.COMPETITOR_GROUPS.ORG_GROUP_ID'
    end
  end
  object m_Q_org_grps: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name, (select count(*) from competitor_groups '
      'where enabled = '#39'Y'#39
      '  and org_group_id = og.id) qnt'
      'from org_groups og'
      'where type_id = 2378'
      '%WHERE_CLAUSE'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 256
    Top = 312
    object m_Q_org_grpsID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_org_grpsNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
    object m_Q_org_grpsQNT: TFloatField
      FieldName = 'QNT'
      Visible = False
    end
  end
  object m_SP_fill_lines: TStoredProc
    DatabaseName = 'TSDBMain'
    SessionName = 'Default'
    StoredProcName = 'RETAIL.PRC_FILL_COMP_SUMM_REPR_LINES'
    Left = 256
    Top = 408
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_RES'
        ParamType = ptOutput
      end>
  end
  object m_Q_osnov: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      '   select cg.id as comp_id, '
      '          cg.name as comp_name, '
      '          ri.nmcl_id, '
      '          o.id org_id, '
      '          o.name org_name, '
      '          comp_price,'
      '          c.name address_comp'
      '          from doc_comp_prices p, '
      '               doc_comp_reprice r, '
      '               doc_comp_reprice_items ri,'
      '               organizations o,'
      '               documents d,'
      '               competitors c,'
      '               document_links dl,'
      '               competitor_groups cg'
      '        where d.id = r.doc_id '
      '          and r.doc_id = ri.doc_id'
      '          and ri.doc_id = p.doc_id'
      '          and ri.line_id = p.line_id          '
      '          and c.id = p.comp_id'
      '          and c.group_id = cg.id'
      '          and dl.src_doc_id = :ID'
      '          and dl.dest_doc_id = d.id'
      '          and o.id = r.org_id'
      '          and nmcl_id = :NMCL_ID'
      '          and cg.id =:GROUP_ID'
      '          %WHERE_CLAUSE'
      '          %ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 416
    Top = 478
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'GROUP_ID'
        ParamType = ptInput
      end>
    object m_Q_osnovORG_ID: TFloatField
      DisplayLabel = 'ID �����'
      FieldName = 'ORG_ID'
      Origin = 'o.id'
    end
    object m_Q_osnovORG_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'ORG_NAME'
      Origin = 'o.name'
      Size = 250
    end
    object m_Q_osnovADDRESS_COMP: TStringField
      DisplayLabel = '����� ����������'
      FieldName = 'ADDRESS_COMP'
      Origin = 'c.name'
      Size = 250
    end
    object m_Q_osnovCOMP_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'COMP_PRICE'
      Origin = 'p.comp_price'
    end
  end
end
