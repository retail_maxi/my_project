//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmCompetitorsRepriceItem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLDBPanel"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------


void __fastcall TFCompetitorsRepriceItem::m_ACT_editExecute(
      TObject *Sender)
{

  if (m_dm->m_Q_linesLINE_ID->IsNull)
  {
    TTSFDocumentWithLines::m_ACT_appendExecute(Sender);
  }
  else  {
    long line_id = m_dm->m_Q_linesLINE_ID->AsInteger;
    TTSFDocumentWithLines::m_ACT_editExecute(Sender);
         if( line_id > 0 )
        m_dm->m_Q_lines->Locate(m_dm->m_Q_linesLINE_ID->FieldName,line_id,TLocateOptions());

    }

}
//---------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceItem::RefreshItem()
{
    //�������� ������ �����������
    if (  m_dm->m_Q_competitors->Active) m_dm->m_Q_competitors->Close();
    m_dm->m_Q_competitors->Open();

     //������� m_Q_lines
    m_dm->m_Q_lines->Close();
    m_dm->m_Q_lines->SQL->Clear();

    ResetItem();

    m_dm->m_Q_lines->SQL->AddStrings(m_dm->m_Q_lines_start->SQL);

    m_dm->m_Q_competitors->First();
    int i=1; AnsiString colname = "col";
    while (!m_dm->m_Q_competitors->Eof)
    {
       //�������� ������� � m_Q_lines
       TFloatField *f = new TFloatField(this);
       f->FieldName = colname+i;
       f->Name= AnsiString("m_Q_lines")+colname+i;
       f->DisplayLabel = m_dm->m_Q_competitorsNAME->AsString;
       f->DataSet = m_dm->m_Q_lines;
       f->Tag =   m_dm->m_Q_competitorsID->AsInteger; //id ���������� ��� ����������� ����

       //�������� SQL-�����
       m_dm->m_Q_lines->SQL->Add(", (select dcp.comp_price from doc_comp_prices dcp ");
       m_dm->m_Q_lines->SQL->Add(" where dcp.comp_id = "+m_dm->m_Q_competitorsID->AsString);
       m_dm->m_Q_lines->SQL->Add("   and dcp.doc_id = v.id   and dcp.line_id = v.line_id ");
       m_dm->m_Q_lines->SQL->Add("  ) AS "+colname+i);

       i++;
       m_dm->m_Q_competitors->Next();
    }
      
    //�������� � sql  m_Q_lines
    m_dm->m_Q_lines->SQL->AddStrings(m_dm->m_Q_lines_end->SQL );

    //�������

    TParam *res = m_dm->m_Q_lines->Macros->FindParam("WHERE_CLAUSE");
    if( res )      res->Value = "";
    res = NULL;
    res = m_dm->m_Q_lines->Macros->FindParam("WHERE_EXPRESSION");
    if( res  )      res->Value = "";
    res = NULL;
    res = m_dm->m_Q_lines->Macros->FindParam("ORDER_CLAUSE");
    if( res)        res->Value = "";
    res = NULL;
    res = m_dm->m_Q_lines->Macros->FindParam("ORDER_EXPRESSION");
    if( res)        res->Value = "";

    m_DS_lines->DataSet = NULL;
    m_DS_lines->DataSet = m_dm->m_Q_lines;

    m_DBG_lines->Columns->RebuildColumns();
    //������������ ������ � ��������� �����
    for( int i = 0; i < m_DBG_lines->Columns->Count; i++ )
       m_DBG_lines->Columns->Items[i]->Title->TitleButton = true;

    m_dm->m_Q_lines->DoFilter(m_dm->m_Q_lines->FilterSettings);
    m_dm->m_Q_lines->DoOrder(m_dm->m_Q_lines->OrderSettings);

    for (int i=0; i<m_DBG_lines->Columns->Count; i++)
    {
       m_DBG_lines->Columns->Items[i]->Visible = m_dm->m_Q_lines->Fields->Fields[i]->Visible;
    }

    m_dm->m_Q_lines->Open();

}


//--------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceItem::ResetItem()
{
  for (int i=m_dm->m_Q_lines->FieldCount; i>m_dm->m_Q_lines_start->FieldCount; i--)
  {
      TField *f =  m_dm->m_Q_lines->Fields->Fields[i-1];
      delete f;
  }
}
//--------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceItem::FormShow(TObject *Sender)
{
     RefreshItem();
     TTSFDocumentWithLines::FormShow(Sender);
}
//---------------------------------------------------------------------------



void __fastcall TFCompetitorsRepriceItem::m_DBG_linesColEnter(
      TObject *Sender)
{
    m_dm->CurCompId = m_DBG_lines->SelectedField->Tag;
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceItem::m_ACT_editUpdate(TObject *Sender)
{
  m_ACT_edit->Enabled =  (m_DBG_lines->SelectedField->Tag > 0) &&
                           (m_dm->UpdateRegimeItem != urView);


}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceItem::m_ACT_deleteUpdate(
      TObject *Sender)
{
  m_ACT_delete->Enabled =  (m_DBG_lines->SelectedField->Tag > 0) &&
                           (m_dm->UpdateRegimeItem != urView) && !m_dm->m_Q_linesLINE_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceItem::m_ACT_viewUpdate(TObject *Sender)
{
     m_ACT_view->Enabled =  (m_DBG_lines->SelectedField->Tag > 0 &&
                             !m_DBG_lines->SelectedField->Value.IsNull() );
}
//---------------------------------------------------------------------------



void __fastcall TFCompetitorsRepriceItem::m_DBG_linesDblClick(
      TObject *Sender)
{
  if (m_ACT_edit->Enabled ) m_ACT_editExecute(Sender);
  else if (m_ACT_view->Enabled ) m_ACT_viewExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceItem::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   TTSFDocumentWithLines::FormClose(Sender, Action);
   ResetItem();
}
//---------------------------------------------------------------------------


