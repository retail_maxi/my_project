//---------------------------------------------------------------------------

#ifndef FmRtlSalesPeriodH
#define FmRtlSalesPeriodH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmRtlSales.h"
#include "TSFMDIALOG.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
#include <ToolWin.hpp>
#include "TSFmDBDialog.h"
//---------------------------------------------------------------------------
class TFRtlSalesPeriod : public TTSFDialog
{
__published:	// IDE-managed Components
        TDateTimePicker *m_DTP_DateFrom;
        TDateTimePicker *m_DTP_DateTo;
        TDateTimePicker *m_DTP_TimeFrom;
        TDateTimePicker *m_DTP_TimeTo;
        TLabel *Label1;
        TLabel *Label2;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
private:	// User declarations
    TDRtlSales* m_dm;
public:		// User declarations
    __fastcall TFRtlSalesPeriod(TComponent* p_owner, TDRtlSales* p_dm);
};
#endif
