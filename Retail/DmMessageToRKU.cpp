//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmMessageToRKU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TDMessageToRKU::TDMessageToRKU(TComponent* p_owner,
                                          AnsiString p_prog_id)
    : TTSDDocument(p_owner,p_prog_id),
    is_read(1),
    cnt_RKU(StrToInt64(GetConstValue("CNT_RKU")))
{
}
//---------------------------------------------------------------------------

int __fastcall TDMessageToRKU::GetCntId()
{
    if (m_Q_get_cnt_id->Active) m_Q_get_cnt_id->Close();
    m_Q_get_cnt_id->Open();
    return m_Q_get_cnt_idCNT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDMessageToRKU::m_Q_itemAfterInsert(TDataSet *DataSet)
{
    TTSDDocument::m_Q_itemAfterInsert(DataSet);
    m_Q_itemCREATE_USER_ID->AsInteger = GetCntId();
    m_Q_itemDOC_NUMBER->AsInteger = StrToInt64(GetDocNumber(DocTypeId));
    m_Q_itemDOC_DATE->AsDateTime = GetSysDate();

    m_Q_type_mess_list->Open();
    try
    {
      m_Q_itemTYPE_MESSAGE_ID->AsInteger = 0;
      m_Q_type_mess_list->Locate("ID", 0, TLocateOptions());
      m_Q_itemMESSAGE_TYPE->AsString = m_Q_type_mess_listNAME->AsString;
    }
    __finally
    {
      m_Q_type_mess_list->Close();
    }
}
//---------------------------------------------------------------------------

void __fastcall TDMessageToRKU::m_Q_itemBeforeClose(TDataSet *DataSet)
{
  if( (UpdateRegimeItem == urInsert) && (!ChangingItem) )
    FreeDocNumber(DocTypeId,m_Q_itemDOC_NUMBER->AsString);
}
//---------------------------------------------------------------------------

void __fastcall TDMessageToRKU::m_Q_rku_listBeforeOpen(TDataSet *DataSet)
{
    m_Q_rku_list->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDMessageToRKU::ItemsClear()
{
    m_Q_items_clear->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
    m_Q_items_clear->ExecSQL();
}
//---------------------------------------------------------------------------

void __fastcall TDMessageToRKU::ItemsAdd(int p_rku)
{
    m_Q_items_add->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
    m_Q_items_add->ParamByName("RKU")->AsInteger = p_rku;
    m_Q_items_add->ExecSQL();
}
//---------------------------------------------------------------------------

void __fastcall TDMessageToRKU::m_Q_not_readBeforeOpen(TDataSet *DataSet)
{
    m_Q_not_read->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
    m_Q_not_read->ParamByName("IS_READ")->AsInteger = is_read;
}
//---------------------------------------------------------------------------



