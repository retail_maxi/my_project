//---------------------------------------------------------------------------

#ifndef FmOperRtlSalesItemH
#define FmOperRtlSalesItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFMSUBITEM.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmOperRtlSales.h"
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Grids.hpp>
#include "MLDBPanel.h"
#include "TSReportControls.h"
//---------------------------------------------------------------------------
class TFOperRtlSalesItem : public TTSFSubItem
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *m_L_COMPLETE;
        TLabel *m_L_SALE_SUM;
        TLabel *m_L_BUYER_SUM;
        TLabel *m_L_COMPLETE_USER_ID;
        TLabel *m_L_COMPLETE_USER_NAME;
        TLabel *m_L_HOST;
        TMLDBPanel *m_MLDBP_COMPLETE;
        TMLDBPanel *m_MLDBP_SALE_SUM;
        TMLDBPanel *m_MLDBP_BUYER_SUM;
        TMLDBPanel *m_MLDBP_COMPLETE_USER_ID;
        TMLDBPanel *m_MLDBP_COMPLETE_USER_NAME;
        TMLDBPanel *m_MLDBP_HOST;
        TMLDBGrid *MLDBGrid1;
        TPanel *Panel2;
        TSpeedButton *m_SBTN_print_check;
        TMLDBGrid *MLDBGrid2;
        TDataSource *m_DS_item;
        TDataSource *m_DS_checks;
        TDataSource *m_DS_lines;
        TAction *m_ACT_print_check;
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall m_ACT_print_checkExecute(TObject *Sender);
        void __fastcall m_ACT_print_checkUpdate(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
  ETSUpdateRegime m_regime;
  TDOperRtlSales *m_dm;
  TWinControl *m_start_win_control;
  bool FPressOK;
  bool __fastcall GetPressOK();
  TTSReportControl m_check;
protected:
public:		// User declarations
        __fastcall TFOperRtlSalesItem(TComponent* p_owner,
                                      TDOperRtlSales *p_dm);
         __fastcall ~TFOperRtlSalesItem(){};
          __property bool PressOK  = { read=GetPressOK };
};
//---------------------------------------------------------------------------
#endif
 