//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmAssortExceptionList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
__fastcall TFAssortExceptionList::TFAssortExceptionList(TComponent* p_owner, TTSDRefbook *p_dm_refbook):
    TTSFRefbook(p_owner, p_dm_refbook),
    m_dm(static_cast<TDAssortException*>(p_dm_refbook))
{
}
//---------------------------------------------------------------------------


