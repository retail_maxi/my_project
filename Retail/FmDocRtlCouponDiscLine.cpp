//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocRtlCouponDiscLine.h"
#include "TSErrors.h"
#include "MLFuncs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmDocumentLine"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "MLDBPanel"
#pragma link "FrmShelfLifeLists"
#pragma link "FmDocRtlCouponDiscLine"
#pragma link "DBGridEh"
#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFDocRtlCouponDiscLine::TFDocRtlCouponDiscLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm) : TTSFDocumentLine(p_owner,p_dm),
                                                                            m_dm(static_cast<TDDocRtlCouponDisc*>(p_dm))
{
};

//---------------------------------------------------------------------------
__fastcall TFDocRtlCouponDiscLine::~TFDocRtlCouponDiscLine()
{

};

//---------------------------------------------------------------------------

AnsiString __fastcall TFDocRtlCouponDiscLine::BeforeLineApply(TWinControl *p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete ) return "";

  if( m_dm->m_Q_lineNMCL_ID->IsNull )
    {
        m_LV_nmcl->SetFocus();
        return "�������� ������������!";
    }

  if ( m_dm->m_Q_linePERCENT_DISC->IsNull && m_dm->m_Q_linePRICE_DISC->IsNull)
    {
        m_DBE_repcent->SetFocus();
        return "��������� ���� '������� ������' ��� ���� '���� �� ������'!";
    }

  if ( !m_dm->m_Q_linePERCENT_DISC->IsNull && !m_dm->m_Q_linePRICE_DISC->IsNull)
    {
        m_DBE_repcent->SetFocus();
        return "������ ���� ��������� ���� ���� '������� ������', ���� ���� '���� �� ������'!";
    }

  if( !m_dm->m_Q_lineLIMIT_QNT->IsNull)
    {
      if ( m_dm->m_Q_lineLIMIT_QNT->AsInteger <= 0 )
      {
        m_DBE_limit->SetFocus();
        return "�������� ���� ����������� �� ���������� ������ ���� ������ ����!";
      }
    }

 return "";
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscLine::FormShow(TObject *Sender)
{
  TTSFDocumentLine::FormShow(Sender);

}


