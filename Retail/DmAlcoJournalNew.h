//---------------------------------------------------------------------------

#ifndef DmAlcoJournalNewH
#define DmAlcoJournalNewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMOPERATION.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include <Registry.hpp>
#include "MLFuncs.h"
//---------------------------------------------------------------------------
class TDAlcoJournalNew : public TTSDOperation
{
__published:
        TMLQuery *m_Q_taxpayer;
        TMLQuery *m_Q_taxpayer_fict;
        TMLUpdateSQL *m_U_taxpayer_fict;
        TFloatField *m_Q_taxpayerID;
        TStringField *m_Q_taxpayerNAME;
        TFloatField *m_Q_taxpayer_fictID;
        TStringField *m_Q_taxpayer_fictNAME;
        TMLQuery *m_Q_org;
        TStringField *m_Q_orgNAME;
        TStoredProc *m_SP_fill_tmp_alco_class_item_v;
        TStoredProc *m_SP_fill_buf_alco_jour;
	TFloatField *m_Q_taxpayerCNT_ID;
	TStringField *m_Q_taxpayerINN;
	TStringField *m_Q_taxpayerKPP;
	TMLQuery *m_Q_address;
	TFloatField *m_Q_addressORG_ID;
	TFloatField *m_Q_addressADDR_TYPE_ID;
	TStringField *m_Q_addressADDR_TYPE_NAME;
	TStringField *m_Q_addressZIP;
	TFloatField *m_Q_addressPLACE_ID;
	TStringField *m_Q_addressPLACE_NAME;
	TStringField *m_Q_addressPLACE_PHONE_CODE;
	TFloatField *m_Q_addressCOUNTRY_ID;
	TStringField *m_Q_addressCOUNTRY_NAME;
	TStringField *m_Q_addressSTREET;
	TFloatField *m_Q_addressBUILDING;
	TStringField *m_Q_addressEX_BUILDING;
	TFloatField *m_Q_addressROOM;
	TStringField *m_Q_addressFULL_NAME;
	TMemoField *m_Q_addressSHOW_ADDRESS;
	TStringField *m_Q_taxpayer_fictINN;
	TStringField *m_Q_taxpayer_fictKPP;
	TFloatField *m_Q_mainNUM;
	TDateTimeField *m_Q_mainCHECK_DATE;
	TStringField *m_Q_mainBAR_CODE;
	TFloatField *m_Q_mainNMCL_ID;
	TStringField *m_Q_mainNMCL_NAME;
	TFloatField *m_Q_mainCAPACITY;
	TFloatField *m_Q_mainQUANTITY;
	TStringField *m_Q_mainALCO_CODE;
	TDateTimeField *m_Q_mainORDER_BY_DATE;
        void __fastcall m_Q_mainBeforeOpen(TDataSet *DataSet);
	void __fastcall m_Q_orgBeforeOpen(TDataSet *DataSet);
	void __fastcall m_Q_addressBeforeOpen(TDataSet *DataSet);
private:
    TDateTime __fastcall GetBeginDate();
    TDateTime __fastcall GetEndDate();
public:
    int m_gen;
    __fastcall TDAlcoJournalNew(TComponent* p_owner, AnsiString p_prog_id);
    __fastcall ~TDAlcoJournalNew();
    int m_home_org_id;
    void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};

};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
 