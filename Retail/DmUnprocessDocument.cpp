//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmUnprocessDocument.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDUnprocessDocument::TDUnprocessDocument(TComponent* p_owner, AnsiString p_prog_id):
    TTSDOperation(p_owner, p_prog_id),
    m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
  SetBeginEndDate(Now()-1, Now());
}
//---------------------------------------------------------------------------

void TDUnprocessDocument::SetFictFilterORGValues()
{

}
//---------------------------------------------------------------------------
void TDUnprocessDocument::SaveToReestrFilterValue(char *FilterName, int FilterValue)
{
    TRegistry *reg = new TRegistry;
    try
    {
        reg->RootKey = HKEY_CURRENT_USER;
        if(reg->OpenKey(MLGetLibraryRegKey(this),true))
        {
            reg->WriteInteger(FilterName, FilterValue);
            reg->CloseKey();
        }
    }
    __finally
    {
      delete reg;
    }
}
//---------------------------------------------------------------------------
int TDUnprocessDocument::GetFromReestrFilterValue(char *FilterName)
{
    int TempValue = -1;
    TRegistry *reg = new TRegistry;
    try
    {
        reg->RootKey = HKEY_CURRENT_USER;
        if(reg->OpenKey(MLGetLibraryRegKey(this),true))
        {
           if(reg->ValueExists(FilterName))
            {
                TempValue = reg->ReadInteger(FilterName);
                reg->CloseKey();
            }
        }
    }
    __finally
    {
      delete reg;
    }
    return TempValue;
}

TDateTime __fastcall TDUnprocessDocument::GetBeginDate()
{
  return m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDUnprocessDocument::GetEndDate()
{
  return m_Q_main->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDUnprocessDocument::SetBeginEndDate(const TDateTime &p_begin_date,
                                                const TDateTime &p_end_date)
{
  if( (int(m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_main->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_main->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  RefreshDataSets();
}
//---------------------------------------------------------------------------



