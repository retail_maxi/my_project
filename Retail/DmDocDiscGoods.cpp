//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
                                         
#include "DmDocDiscGoods.h"
#include "TSAccessControl.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDDocDiscGoods::TDDocDiscGoods(TComponent* p_owner,
                                        AnsiString p_prog_id): TTSDDocumentWithLines(p_owner,
                                                                                     p_prog_id),
                                        m_fldr_new(StrToInt64(GetConstValue("FLDR_DISC_GOODS_NEW"))),
                                        m_fldr_sogl(StrToInt64(GetConstValue("FLDR_DISC_GOODS_SOGL_KN"))),
                                        m_fldr_utv(StrToInt64(GetConstValue("FLDR_DISC_GOODS_UTV"))),
                                        m_fldr_in_act(StrToInt64(GetConstValue("FLDR_DISC_GOODS_IN_ACT"))),
                                        m_fldr_arc(StrToInt64(GetConstValue("FLDR_DISC_GOODS_ARC"))),
                                        m_fldr_cansel(StrToInt64(GetConstValue("FLDR_DISC_GOODS_CANSEL")))
{
  TDateTime sys_date = GetSysDate();
  SetBeginEndDate(int(sys_date),int(sys_date));
  m_can_grp_chg = TTSOperationAccess(DRight->GetMaskOperRight(StrToInt64(GetConstValue("OPER_CAN_GROUP_PROCCESING")),TTSOperationAccess().AccessMask)).CanRun;
  m_Q_folders_next->ParamByName("ROUTE_ACCESS_MASK")->AsString = TTSRouteAccess().AccessMask;
  m_Q_folders_prev->ParamByName("ROUTE_ACCESS_MASK")->AsString = TTSRouteAccess().AccessMask;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDDocDiscGoods::GetBeginDate()
{
  return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDDocDiscGoods::GetEndDate()
{
  return m_Q_list->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::SetBeginEndDate(const TDateTime &p_begin_date,
                                            const TDateTime &p_end_date)
{
  if( (int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  if( m_Q_list->Active ) RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDDocument::m_Q_itemAfterInsert(DataSet);  
  m_Q_itemDOC_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemDOC_NUMBER->AsInteger = StrToInt64(GetDocNumber(DocTypeId));
  m_Q_itemACTION_TYPE_ID->AsInteger = 27;
  m_Q_itemACTION_TYPE_NAME->AsString = "��������� �� ����������� �������";
  m_Q_itemSUPP_COMP->AsInteger = 0;
  m_Q_itemURGENT_REPRICE->AsInteger = 0;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_assortsBeforeOpen(TDataSet *DataSet)
{
  m_Q_assorts->ParamByName("NMCL_ID")->AsInteger = m_Q_itemNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_lineAfterInsert(TDataSet *DataSet)
{
  m_Q_lineID->AsInteger = m_Q_itemID->AsInteger;
  m_Q_lineLINE_ID->AsInteger = -1;
  m_Q_lineGRP_ID->AsInteger = m_Q_org_groupsID->AsInteger;
  m_Q_lineORG_NAME->AsString = "���";
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_orgsBeforeOpen(TDataSet *DataSet)
{
  m_Q_orgs->ParamByName("GRP_ID")->AsInteger = m_Q_org_groupsID->AsInteger;
  m_Q_orgs->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_is_assortedBeforeOpen(TDataSet *DataSet)
{
  m_Q_is_assorted->ParamByName("NMCL_ID")->AsInteger = m_Q_itemNMCL_ID->AsInteger;
}

void __fastcall TDDocDiscGoods::ApplyUpdatesItem()
{
  TTSDDocumentWithLines::ApplyUpdatesItem();
  RefreshItemDataSets();
}
//---------------------------------------------------------------------------


void __fastcall TDDocDiscGoods::m_Q_linesBeforeOpen(TDataSet *DataSet)
{
  TTSDDocumentWithLines:: m_Q_linesBeforeOpen(DataSet);
  m_Q_lines->ParamByName("nmcl_id")->AsInteger = m_Q_itemNMCL_ID->AsInteger;
  m_Q_lines->ParamByName("assort_id")->AsInteger = m_Q_itemASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_sub_lineBeforeOpen(TDataSet *DataSet)
{
  m_Q_sub_line->ParamByName("id")->AsInteger = m_Q_itemID->AsInteger;

}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_orgs_listBeforeOpen(TDataSet *DataSet)
{
    m_Q_orgs_list->ParamByName("ID")->AsInteger = m_Q_listID->AsInteger;
    m_Q_orgs_list->ParamByName("NMCL_ID")->AsInteger = m_Q_listNMCL_ID->AsInteger;
    m_Q_orgs_list->ParamByName("ASSORT_ID")->AsInteger = m_Q_listASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_sel_orgsBeforeOpen(TDataSet *DataSet)
{
   m_Q_sel_orgs->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;     
}
//---------------------------------------------------------------------------


void __fastcall TDDocDiscGoods::m_Q_chk_docsBeforeOpen(TDataSet *DataSet)
{
   m_Q_chk_docs->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------



void __fastcall TDDocDiscGoods::m_Q_get_out_priceBeforeOpen(
      TDataSet *DataSet)
{
    m_Q_get_out_price->ParamByName("ORG_ID")->AsInteger = m_Q_linesLINE_ID->AsInteger;
    m_Q_get_out_price->ParamByName("NMCL_ID")->AsInteger = m_Q_itemNMCL_ID->AsInteger;
    m_Q_get_out_price->ParamByName("ASSORT_ID")->AsInteger = m_Q_itemASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_folders_next_allBeforeOpen(
      TDataSet *DataSet)
{
  m_Q_folders_next_all->ParamByName("sq")->AsInteger = SqFldr;
  m_Q_folders_next_all->ParamByName("fldr_id")->AsInteger = m_Q_listDOC_FLDR_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_folders_prev_allBeforeOpen(
      TDataSet *DataSet)
{
  m_Q_folders_prev_all->ParamByName("sq")->AsInteger = SqFldr;
  m_Q_folders_prev_all->ParamByName("fldr_id")->AsInteger = m_Q_listDOC_FLDR_ID->AsInteger;
}
//---------------------------------------------------------------------------


void __fastcall TDDocDiscGoods::m_Q_chk_nmclBeforeOpen(TDataSet *DataSet)
{
   m_Q_chk_nmcl->ParamByName("DOC_ID")->AsInteger = m_Q_docsDOC_ID->AsInteger;
   m_Q_chk_nmcl->ParamByName("NMCL_ID")->AsInteger = m_Q_docsNMCL_ID->AsInteger;
   m_Q_chk_nmcl->ParamByName("ASSORTMENT_ID")->AsInteger = m_Q_docsASSORTMENT_ID->AsInteger;
   m_Q_chk_nmcl->ParamByName("FLDR_NEW")->AsInteger = FldrNew;
   m_Q_chk_nmcl->ParamByName("FLDR_SOGL")->AsInteger = FldrSogl;
   m_Q_chk_nmcl->ParamByName("FLDR_UTV")->AsInteger = FldrUtv;
}
//---------------------------------------------------------------------------

void __fastcall TDDocDiscGoods::m_Q_cntBeforeOpen(TDataSet *DataSet)
{
   m_Q_cnt->ParamByName("NMCL_ID")->AsInteger = m_Q_itemNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------





void __fastcall TDDocDiscGoods::m_Q_reasonsBeforeOpen(TDataSet *DataSet)
{
   m_Q_reasons->ParamByName("ACT_TYPE_ID")->AsInteger = m_Q_itemACTION_TYPE_ID->AsInteger;
}
//---------------------------------------------------------------------------

