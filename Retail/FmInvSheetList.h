//---------------------------------------------------------------------------

#ifndef FmInvSheetListH
#define FmInvSheetListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmInvSheet.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTLIST.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFInvSheetList : public TTSFDocumentList
{
__published:
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TToolButton *ToolButton1;
  TSpeedButton *m_SBTN_set_date;
        TDBLookupComboBox *m_DBLCB_organizations;
        TToolButton *ToolButton2;
        TDataSource *m_DS_orgs;
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_DBLCB_organizationsClick(TObject *Sender);
private:
  TDInvSheet* m_dm;
public:
  __fastcall TFInvSheetList(TComponent* p_owner, TTSDDocument *p_dm)
                           : TTSFDocumentList(p_owner,p_dm),
                           m_dm(static_cast<TDInvSheet*>(p_dm))
                           {};
};
//---------------------------------------------------------------------------
#endif
