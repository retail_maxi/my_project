//---------------------------------------------------------------------------

#ifndef DmCompetitorsRepriceSummH
#define DmCompetitorsRepriceSummH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMDOCUMENTWITHLINES.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDCompetitorsRepriceSumm : public TTSDDocumentWithLines
{
__published:	// IDE-managed Components
   TStringField *m_Q_listDOC_NUMBER;
   TStringField *m_Q_listNOTE;
   TStringField *m_Q_itemDOC_NUMBER;
   TStringField *m_Q_itemNOTE;
   TMLQuery *m_Q_lines_start;
   TMLQuery *m_Q_lines_end;
        TMLQuery *m_Q_org_group;
        TFloatField *m_Q_org_groupID;
        TStringField *m_Q_org_groupNAME;
        TDateTimeField *m_Q_listDOC_DATE;
        TFloatField *m_Q_listORG_GROUP_ID;
        TStringField *m_Q_listORG_GROUP_NAME;
        TDateTimeField *m_Q_itemDOC_DATE;
        TFloatField *m_Q_itemORG_GROUP_ID;
        TStringField *m_Q_itemORG_GROUP_NAME;
        TMLQuery *m_Q_org_groups;
        TStringField *m_Q_org_groupsNAME;
        TFloatField *m_Q_org_groupsID;
        TMLQuery *m_Q_comp_one;
        TStringField *m_Q_comp_oneCOMP_NAME;
        TMLQuery *m_Q_competitors;
        TFloatField *m_Q_competitorsID;
        TStringField *m_Q_competitorsNAME;
        TFloatField *m_Q_competitorsORG_GROUP_ID;
        TMLQuery *m_Q_org_grps;
        TFloatField *m_Q_org_grpsID;
        TStringField *m_Q_org_grpsNAME;
        TFloatField *m_Q_linesNMCL_ID;
        TStringField *m_Q_linesNMCL_NAME;
        TStringField *m_Q_linesMU_NAME;
        TFloatField *m_Q_linesMIN_IN_PRICE;
        TFloatField *m_Q_linesMAX_IN_PRICE;
        TFloatField *m_Q_linesAVG_WEIGHT_PRCIE;
        TFloatField *m_Q_linesMIN_ORG_GPROUP_PRICE;
        TFloatField *m_Q_linesMAX_ORG_GPROUP_PRICE;
        TFloatField *m_Q_linesMIN_COMP_PRICE;
        TStringField *m_Q_linesMAX_COMP_PRICE;
        TStringField *m_Q_linesRESOLVE_PRICE;
        TFloatField *m_Q_linesASSORT_ID;
        TStringField *m_Q_linesASSORT_NAME;
        TFloatField *m_Q_lines_startID;
        TFloatField *m_Q_lines_startLINE_ID;
        TFloatField *m_Q_lines_startNMCL_ID;
        TStringField *m_Q_lines_startNMCL_NAME;
        TFloatField *m_Q_lines_startASSORT_ID;
        TStringField *m_Q_lines_startASSORT_NAME;
        TStringField *m_Q_lines_startMU_NAME;
        TFloatField *m_Q_lines_startMIN_IN_PRICE;
        TFloatField *m_Q_lines_startMAX_IN_PRICE;
        TFloatField *m_Q_lines_startAVG_WEIGHT_PRCIE;
        TStringField *m_Q_lines_startPERCENT_MIN;
        TStringField *m_Q_lines_startPERCENT_MAX;
        TFloatField *m_Q_lines_startMIN_ORG_GPROUP_PRICE;
        TFloatField *m_Q_lines_startMAX_ORG_GPROUP_PRICE;
        TFloatField *m_Q_lines_startMIN_COMP_PRICE;
        TStringField *m_Q_lines_startMAX_COMP_PRICE;
        TStringField *m_Q_lines_startRESOLVE_PRICE;
        TStoredProc *m_SP_fill_lines;
        TFloatField *m_Q_lineNMCL_ID;
        TStringField *m_Q_lineNMCL_NAME;
        TFloatField *m_Q_lineASSORT_ID;
        TStringField *m_Q_lineASSORT_NAME;
        TStringField *m_Q_lineRESOLVE_PRICE;
        TMLQuery *m_Q_osnov;
        TFloatField *m_Q_osnovORG_ID;
        TStringField *m_Q_osnovORG_NAME;
        TFloatField *m_Q_osnovCOMP_PRICE;
        TStringField *m_Q_osnovADDRESS_COMP;
        TFloatField *m_Q_linesPERCENT_MIN;
        TFloatField *m_Q_linesPERCENT_MAX;
        TFloatField *m_Q_org_grpsQNT;
        TFloatField *m_Q_linesKVI;
        TFloatField *m_Q_linesKEYSKU;
        TFloatField *m_Q_linesEPP;
        TFloatField *m_Q_lines_startKVI;
        TFloatField *m_Q_lines_startKEYSKU;
        TFloatField *m_Q_lines_startEPP;
   void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
   void __fastcall m_Q_lineAfterInsert(TDataSet *DataSet);
   void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_competitorsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_itemBeforeOpen(TDataSet *DataSet);
private:	// User declarations
  int m_cur_comp_id;
  int m_home_org_id;
protected:
  TS_DOC_LINE_SQ_NAME("SQ_DOC_COMP_REPRICE_ITEMS")
public:		// User declarations
   __fastcall TDCompetitorsRepriceSumm(TComponent* p_owner,AnsiString p_prog_id);
   __fastcall ~TDCompetitorsRepriceSumm();   
   __property int CurCompId = {read = m_cur_comp_id, write = m_cur_comp_id};
   void __fastcall SetBeginEndDate(const TDateTime &p_begin_date, const TDateTime &p_end_date);
};
//---------------------------------------------------------------------------
#endif
