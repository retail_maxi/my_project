//---------------------------------------------------------------------------

#ifndef FmDocRtlCouponDiscLineH
#define FmDocRtlCouponDiscLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmDocRtlCouponDisc.h"
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "FrmShelfLifeLists.h"
#include "FmDocRtlCouponDiscLine.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
//---------------------------------------------------------------------------
class TFDocRtlCouponDiscLine : public TTSFDocumentLine
{
__published:
  TLabel *m_L_qnt;
  TLabel *Label5;
        TDBEdit *m_DBE_repcent;
  TLabel *Label9;
  TLabel *m_L_src_amount;
  TToolButton *ToolButton1;
  TToolButton *ToolButton2;
        TDBEdit *m_DBE_limit;
        TMLLovListView *m_LV_nmcl;
        TDataSource *m_DS_nmcl;
        TMLLov *m_LOV_nmcl;
        TMLLovListView *m_LV_assort;
        TDataSource *m_DS_assort;
        TMLLov *m_LOV_assort;
        TLabel *Label1;
        TDBEdit *m_DBE_price_disc;
  void __fastcall FormShow(TObject *Sender);
private:
  TDDocRtlCouponDisc* m_dm;
  virtual AnsiString __fastcall BeforeLineApply(TWinControl *p_focused);
public:
  __fastcall TFDocRtlCouponDiscLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
  __fastcall ~TFDocRtlCouponDiscLine();  
};
//---------------------------------------------------------------------------
#endif
