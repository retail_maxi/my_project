//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmTTNOutList.h"
#include "FmSelectOrg.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTLIST"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFTTNOutList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);

  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
  m_DBLCB_organizations->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_organizations->Enabled = false;
  else
    m_DBLCB_organizations->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutList::m_ACT_load_excelExecute(TObject *Sender)
{
  ExecSelectOrgDlg(this, m_dm->m_DB_main->Handle);
}
//---------------------------------------------------------------------------
void __fastcall TFTTNOutList::m_DBLCB_organizationsClick(TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;
}
//---------------------------------------------------------------------------



void __fastcall TFTTNOutList::m_DBG_listGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
  if (m_dm->m_Q_listMAXIMUM->AsInteger > 0)
  {
    Background = clYellow;
    AFont->Color = clBlack;
  }
  TTSFDocumentList::m_DBG_listGetCellParams(Sender,Column,AFont,Background,State);
}
//---------------------------------------------------------------------------

