//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmAccountSet.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------


bool ExecAccountSetDlg(TComponent *p_owner, TDIncome *p_dm)
{
  bool res = false;

  try
  {
    p_dm->m_Q_rtl_bill_list->Open();

    TFAccountSet *f_act_tcd_set = new TFAccountSet(p_owner,p_dm);
    try
    {
      p_dm->ReDefineDataSources(f_act_tcd_set);


      res = f_act_tcd_set->ShowModal() == IDOK;

      if( res )
      {
       /* if( (p_dm->m_Q_add_items_by_bill->State == dsInsert) ||
            (p_dm->m_Q_add_items_by_bill->State == dsEdit) )
          p_dm->m_Q_add_items_by_bill->Post();*/
          
        p_dm->m_Q_add_items_by_bill->ParamByName("BILL_DOC_ID")->AsInteger = p_dm->m_Q_rtl_bill_listDOC_ID->AsInteger ;
        p_dm->m_Q_add_items_by_bill->ParamByName("DOC_ID")->AsInteger = p_dm->m_Q_itemID->AsInteger;
        p_dm->m_Q_add_items_by_bill->ExecSQL();

        p_dm->RefreshItemDataSets();
      }
    }
    __finally
    {
      delete f_act_tcd_set;
    }
  }
  __finally
  {
    if( p_dm->m_Q_rtl_bill_list->Active ) p_dm->m_Q_rtl_bill_list->Close();
  }

  return res;
}
//---------------------------------------------------------------------------

