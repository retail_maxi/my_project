//---------------------------------------------------------------------------

#ifndef DmMessagesRKUH
#define DmMessagesRKUH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMOPERATION.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include "TSAccessControl.h"
//---------------------------------------------------------------------------
class TDMessagesRKU : public TTSDOperation
{
__published:	// IDE-managed Components
    TFloatField *m_Q_mainID;
    TFloatField *m_Q_mainDOC_SRC_ORG_ID;
    TStringField *m_Q_mainDOC_SRC_ORG_NAME;
    TDateTimeField *m_Q_mainDOC_CREATE_DATE;
    TStringField *m_Q_mainDOC_CREATE_COMP_NAME;
    TStringField *m_Q_mainDOC_CREATE_USER;
    TDateTimeField *m_Q_mainDOC_MODIFY_DATE;
    TStringField *m_Q_mainDOC_MODIFY_COMP_NAME;
    TStringField *m_Q_mainDOC_MODIFY_USER;
    TStringField *m_Q_mainDOC_STATUS;
    TStringField *m_Q_mainDOC_STATUS_NAME;
    TDateTimeField *m_Q_mainDOC_STATUS_CHANGE_DATE;
    TFloatField *m_Q_mainDOC_FLDR_ID;
    TStringField *m_Q_mainDOC_FLDR_NAME;
    TStringField *m_Q_mainDOC_FLDR_RIGHT;
    TFloatField *m_Q_mainDOC_NUMBER;
    TStringField *m_Q_mainMESSAGE_TYPE;
    TStringField *m_Q_mainHOST;
    TDateTimeField *m_Q_mainSEND_DATE;
    TFloatField *m_Q_mainCREATE_USER_ID;
    TStringField *m_Q_mainCREATE_USER_NAME;
    TDateTimeField *m_Q_mainREAD_DATE;
    TFloatField *m_Q_mainREAD_USER_ID;
    TStringField *m_Q_mainREAD_USER_NAME;
    TStringField *m_Q_mainDOC_TYPE_NAME;
    TDateTimeField *m_Q_mainDOC_DATE;
    TFloatField *m_Q_mainDOC_TYPE_ID;
    TStringField *m_Q_mainRKU;
        TMLQuery *m_Q_doc_ids;
        TMLQuery *m_Q_new_docs;
        TFloatField *m_Q_new_docsID;
        TFloatField *m_Q_new_docsDOC_SRC_ORG_ID;
        TStringField *m_Q_new_docsDOC_SRC_ORG_NAME;
        TDateTimeField *m_Q_new_docsDOC_CREATE_DATE;
        TStringField *m_Q_new_docsDOC_CREATE_COMP_NAME;
        TStringField *m_Q_new_docsDOC_CREATE_USER;
        TDateTimeField *m_Q_new_docsDOC_MODIFY_DATE;
        TStringField *m_Q_new_docsDOC_MODIFY_COMP_NAME;
        TStringField *m_Q_new_docsDOC_MODIFY_USER;
        TStringField *m_Q_new_docsDOC_STATUS;
        TStringField *m_Q_new_docsDOC_STATUS_NAME;
        TDateTimeField *m_Q_new_docsDOC_STATUS_CHANGE_DATE;
        TFloatField *m_Q_new_docsDOC_FLDR_ID;
        TStringField *m_Q_new_docsDOC_FLDR_NAME;
        TStringField *m_Q_new_docsDOC_FLDR_RIGHT;
        TFloatField *m_Q_new_docsDOC_TYPE_ID;
        TStringField *m_Q_new_docsDOC_TYPE_NAME;
        TFloatField *m_Q_new_docsDOC_NUMBER;
        TDateTimeField *m_Q_new_docsDOC_DATE;
        TStringField *m_Q_new_docsMESSAGE_TYPE;
        TStringField *m_Q_new_docsHOST;
        TStringField *m_Q_new_docsRKU;
        TDateTimeField *m_Q_new_docsSEND_DATE;
        TFloatField *m_Q_new_docsCREATE_USER_ID;
        TStringField *m_Q_new_docsCREATE_USER_NAME;
        TDateTimeField *m_Q_new_docsREAD_DATE;
        TFloatField *m_Q_new_docsREAD_USER_ID;
        TStringField *m_Q_new_docsREAD_USER_NAME;
        TFloatField *m_Q_doc_idsLAST_ID;
        TQuery *m_Q_orgs;
        TFloatField *m_Q_orgsID;
        TStringField *m_Q_orgsNAME;
private:	// variable
    int m_doc_type_from_rku_id;
    int m_doc_type_to_rku_id;
    int m_home_org_id;
    TTSDocumentTypeAccess m_from_rku;
    TTSDocumentTypeAccess m_to_rku;
    AnsiString m_sel_doc_type;
private: // functions
    TDateTime __fastcall GetBeginDate();
    TDateTime __fastcall GetEndDate();
    bool __fastcall GetCanViewItem();
    bool __fastcall GetCanInsertItem();
    bool __fastcall GetCanEditItem();
    bool __fastcall GetCanDeleteItem();
    int __fastcall GetOrgId();
    void __fastcall SetOrgId(int p_value);
public: // property
    __property TDateTime BeginDate = {read=GetBeginDate};
    __property TDateTime EndDate = {read=GetEndDate};
    __property AnsiString SelDocType = {read=m_sel_doc_type};
    __property bool CanViewItem = {read=GetCanViewItem};
    __property bool CanInsertItem = {read=GetCanInsertItem};
    __property bool CanEditItem = {read=GetCanEditItem};
    __property bool CanDeleteItem = {read=GetCanDeleteItem};
    __property int DocFromRKU = {read=m_doc_type_from_rku_id};
    __property int DocToRKU = {read=m_doc_type_to_rku_id};
public:		// User declarations
    AnsiString m_rec_id_list;
    int m_last_id;

    __property int OrgId = {read=GetOrgId, write=SetOrgId};
    __property int HomeOrgId  = { read=m_home_org_id };

    __fastcall TDMessagesRKU(TComponent* p_owner, AnsiString p_prog_id);
    __fastcall ~TDMessagesRKU();
    void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                    const TDateTime &p_end_date);
};
//---------------------------------------------------------------------------
#endif
