//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvSheetList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTLIST"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFInvSheetList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);
  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
  m_DBLCB_organizations->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_organizations->Enabled = false;
  else
    m_DBLCB_organizations->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetList::m_DBLCB_organizationsClick(TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;
}
//---------------------------------------------------------------------------

