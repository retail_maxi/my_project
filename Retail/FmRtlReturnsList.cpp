//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlReturnsList.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFRtlReturnsList::TFRtlReturnsList(TComponent* p_owner,
                                              TTSDDocument *p_dm_document): TTSFDocumentList(p_owner,
                                                                                             p_dm_document),
                                                                            m_dm(static_cast<TDRtlReturns*>(DMDocument))
{
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                        m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);

  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
  m_DBLCB_organizations->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_organizations->Enabled = false;
  else
    m_DBLCB_organizations->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsList::m_DBLCB_organizationsClick(
      TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsList::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = m_dm->CanDeleteItem&&!m_dm->m_Q_listID->IsNull
                          &&m_dm->m_Q_listBUF_ID->IsNull;
}
//---------------------------------------------------------------------------

