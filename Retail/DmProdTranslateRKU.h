//---------------------------------------------------------------------------

#ifndef DmProdTranslateRKUH
#define DmProdTranslateRKUH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMREFBOOK.h"
#include "TSReportContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include "StrUtils.hpp"
//---------------------------------------------------------------------------

class TDProdTranslateRKU : public TTSDRefbook
{
__published:
  TMLQuery *m_Q_hist;
  TFloatField *m_Q_listORG_ID;
  TStringField *m_Q_listORG_NAME;
  TFloatField *m_Q_listNMCL_ID;
  TStringField *m_Q_listNMCL_NAME;
  TFloatField *m_Q_listASSORT_ID;
  TStringField *m_Q_listASSORT_NAME;
  TFloatField *m_Q_listTO_GR_DEP_ID;
  TStringField *m_Q_listTO_GR_DEP_NAME;
  TFloatField *m_Q_listTO_NMCL_ID;
  TStringField *m_Q_listTO_NMCL_NAME;
  TFloatField *m_Q_listTO_ASSORT_ID;
  TStringField *m_Q_listTO_ASSORT_NAME;
  TFloatField *m_Q_listCOEF;
  TFloatField *m_Q_histID;
  TFloatField *m_Q_histRTL_PROD_RKU_ID;
  TFloatField *m_Q_histORG_ID;
  TStringField *m_Q_histORG_NAME;
  TFloatField *m_Q_histNMCL_ID;
  TStringField *m_Q_histNMCL_NAME;
  TFloatField *m_Q_histASSORT_ID;
  TStringField *m_Q_histASSORT_NAME;
  TFloatField *m_Q_histTO_GR_DEP_ID;
  TStringField *m_Q_histTO_GR_DEP_NAME;
  TFloatField *m_Q_histTO_NMCL_ID;
  TStringField *m_Q_histTO_NMCL_NAME;
  TFloatField *m_Q_histTO_ASSORT_ID;
  TStringField *m_Q_histTO_ASSORT_NAME;
  TFloatField *m_Q_histCOEF;
  TStringField *m_Q_histOPER_TYPE;
  TDateTimeField *m_Q_histOPER_DATE;
  TStringField *m_Q_histUSER_NAME;
  TStringField *m_Q_histCOMP_NAME;
  TStringField *m_Q_histOPER_TYPE_NAME;
  TFloatField *m_Q_itemORG_ID;
  TStringField *m_Q_itemORG_NAME;
  TFloatField *m_Q_itemNMCL_ID;
  TStringField *m_Q_itemNMCL_NAME;
  TFloatField *m_Q_itemASSORT_ID;
  TStringField *m_Q_itemASSORT_NAME;
  TFloatField *m_Q_itemTO_GR_DEP_ID;
  TStringField *m_Q_itemTO_GR_DEP_NAME;
  TFloatField *m_Q_itemTO_NMCL_ID;
  TStringField *m_Q_itemTO_NMCL_NAME;
  TFloatField *m_Q_itemTO_ASSORT_ID;
  TStringField *m_Q_itemTO_ASSORT_NAME;
  TFloatField *m_Q_itemCOEF;
  TMLQuery *m_Q_org;
  TFloatField *m_Q_orgID;
  TStringField *m_Q_orgNAME;
  TMLQuery *m_Q_nmcl;
  TFloatField *m_Q_nmclID;
  TStringField *m_Q_nmclNAME;
  TMLQuery *m_Q_nmcl_to;
  TFloatField *FloatField1;
  TStringField *StringField1;
  TMLQuery *m_Q_assort;
  TFloatField *m_Q_assortID;
  TStringField *m_Q_assortNAME;
  TMLQuery *m_Q_assort_to;
  TFloatField *FloatField2;
  TStringField *StringField2;
  TMLQuery *m_Q_gr_dep_to;
  TFloatField *m_Q_gr_dep_toID;
  TStringField *m_Q_gr_dep_toNAME;
  void __fastcall m_Q_histBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_listAfterScroll(TDataSet *DataSet);
  void __fastcall m_Q_assortBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_assort_toBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_gr_dep_toBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
protected:
  TS_REFB_SQ_NAME("sq_rtl_prod_translate_rku")
public:
  bool showHist;
  int m_home_org_id;
  __fastcall TDProdTranslateRKU(TComponent* p_owner,
                           AnsiString p_prog_id): TTSDRefbook(p_owner,
                                                              p_prog_id),
             m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
             {
                showHist = false;
             };
   __fastcall TDProdTranslateRKU::~TDProdTranslateRKU()
   {
     if(m_Q_hist->Active) m_Q_hist->Close();
   }
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
