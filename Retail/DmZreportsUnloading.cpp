//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmZreportsUnloading.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "MLLogin"
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "RxQuery"
#pragma link "TSConnectionContainer"
#pragma link "TSDMOPERATION"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
__fastcall TDZreportsUnloading::TDZreportsUnloading(TComponent* p_owner, AnsiString p_prog_id)
               :TTSDOperation(p_owner, p_prog_id)
{
   SetBeginEndDate(int(GetSysDate()),GetSysDate());
   m_Q_tax->Open();
   m_Q_tax->First();
   SetTaxpayer(m_Q_taxID->AsInteger);
}

//---------------------------------------------------------------------------
__fastcall TDZreportsUnloading::~TDZreportsUnloading()
{
   m_Q_tax->Close();
}

//---------------------------------------------------------------------------
void __fastcall TDZreportsUnloading::SetBeginEndDate(const TDateTime &p_begin_date,
                                             const TDateTime &p_end_date)
{
    if (m_begin_date == p_begin_date &&   m_end_date == p_end_date) return;
    
    m_begin_date = p_begin_date;
    m_end_date = p_end_date;
    RefreshDataSets();
}
//---------------------------------------------------------------------------
void __fastcall TDZreportsUnloading::SetTaxpayer(int p_taxpayer)
{
    if (m_taxpayer == p_taxpayer)   return;

    m_taxpayer =  p_taxpayer;
    RefreshDataSets();
}

void __fastcall TDZreportsUnloading::m_Q_mainBeforeOpen(TDataSet *DataSet)
{
   m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = m_begin_date;
   m_Q_main->ParamByName("END_DATE")->AsDateTime = m_end_date;
   m_Q_main->ParamByName("TAXPAYER_ID")->AsInteger = m_taxpayer;    
}
//---------------------------------------------------------------------------

