inherited DRtlPlanMarginSeed: TDRtlPlanMarginSeed
  Left = 285
  Top = 161
  Width = 741
  inherited m_DB_main: TDatabase
    Connected = False
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'select id,'
      '       purchase,'
      '       transfer,'
      '       price_gm,'
      '       price_sm  '
      'from rtl_pm_seed'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 'ID'
    end
    object m_Q_listPURCHASE: TFloatField
      DisplayLabel = '���������� ����'
      FieldName = 'PURCHASE'
      Origin = 'PURCHASE'
    end
    object m_Q_listTRANSFER: TFloatField
      DisplayLabel = '������������ ����'
      FieldName = 'TRANSFER'
      Origin = 'TRANSFER'
    end
    object m_Q_listPRICE_GM: TFloatField
      DisplayLabel = '��������� ���� �� �� �����'
      FieldName = 'PRICE_GM'
      Origin = 'PRICE_GM'
    end
    object m_Q_listPRICE_SM: TFloatField
      DisplayLabel = '��������� ���� ��'
      FieldName = 'PRICE_SM'
      Origin = 'PRICE_SM'
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'select id,'
      '       purchase,'
      '       transfer,'
      '       price_gm,'
      '       price_sm  '
      'from rtl_pm_seed'
      'where id = :ID')
    object m_Q_itemPURCHASE: TFloatField
      FieldName = 'PURCHASE'
      Origin = 'TSDBMAIN.RTL_PM_SEED.PURCHASE'
    end
    object m_Q_itemTRANSFER: TFloatField
      FieldName = 'TRANSFER'
      Origin = 'TSDBMAIN.RTL_PM_SEED.TRANSFER'
    end
    object m_Q_itemPRICE_GM: TFloatField
      FieldName = 'PRICE_GM'
      Origin = 'TSDBMAIN.RTL_PM_SEED.PRICE_GM'
    end
    object m_Q_itemPRICE_SM: TFloatField
      FieldName = 'PRICE_SM'
      Origin = 'TSDBMAIN.RTL_PM_SEED.PRICE_SM'
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_pm_seed'
      'set purchase = :PURCHASE,'
      '    transfer = :TRANSFER,'
      '    price_gm = :PRICE_GM,'
      '    price_sm = :PRICE_SM'
      'where id = :ID')
    InsertSQL.Strings = (
      'insert into rtl_pm_seed(id,purchase,transfer,price_gm,price_sm)'
      
        'VALUES (sq_rtl_pm_seed.nextval, :PURCHASE, :TRANSFER, :PRICE_GM,' +
        ' :PRICE_SM)')
    DeleteSQL.Strings = (
      'delete from rtl_pm_seed'
      'where id = :ID')
  end
end
