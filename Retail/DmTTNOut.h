//---------------------------------------------------------------------------

#ifndef DmTTNOutH
#define DmTTNOutH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include "RtlCardOut.h"
//---------------------------------------------------------------------------
class TDTTNOut : public TTSDDocumentWithLines
{
__published:
  TStringField *m_Q_listDOC_NUMBER;
  TDateTimeField *m_Q_listDOC_DATE;
  TStringField *m_Q_listDEST_ORG_NAME;
  TFloatField *m_Q_listAMOUNT_IN;
  TFloatField *m_Q_listAMOUNT_OUT;
  TStringField *m_Q_listNOTE;
  TFloatField *m_Q_itemFLDR_ID;
  TFloatField *m_Q_itemDEST_ORG_ID;
  TStringField *m_Q_itemDEST_ORG_NAME;
  TStringField *m_Q_itemDOC_NUMBER;
  TDateTimeField *m_Q_itemDOC_DATE;
  TFloatField *m_Q_itemAMOUNT_IN;
  TFloatField *m_Q_itemAMOUNT_OUT;
  TStringField *m_Q_itemNOTE;
  TFloatField *m_Q_linesNMCL_ID;
  TStringField *m_Q_linesNMCL_NAME;
  TStringField *m_Q_linesMEAS_NAME;
  TFloatField *m_Q_linesASSORTMENT_ID;
  TStringField *m_Q_linesASSORTMENT_NAME;
  TFloatField *m_Q_linesITEMS_QNT;
  TFloatField *m_Q_linesIN_PRICE;
  TFloatField *m_Q_linesOUT_PRICE;
  TStringField *m_Q_linesNOTE;
  TFloatField *m_Q_linesCARD_ID;
  TFloatField *m_Q_lineCARD_ID;
  TFloatField *m_Q_lineNMCL_ID;
  TStringField *m_Q_lineNMCL_NAME;
  TFloatField *m_Q_lineASSORTMENT_ID;
  TStringField *m_Q_lineASSORTMENT_NAME;
  TStringField *m_Q_lineMU_NAME;
  TFloatField *m_Q_lineITEMS_QNT;
  TFloatField *m_Q_lineIN_PRICE;
  TFloatField *m_Q_lineOUT_PRICE;
  TStringField *m_Q_lineNOTE;
  TMLQuery *m_Q_orgs;
  TFloatField *m_Q_orgsID;
  TStringField *m_Q_orgsNAME;
  TQuery *m_Q_get_in_price;
  TFloatField *m_Q_get_in_priceVAL;
        TFloatField *m_Q_linesSTORE_QNT;
        TFloatField *m_Q_lineCUR_REST;
        TStringField *m_Q_linesRTL_NAME;
        TMLQuery *m_Q_barcode_info;
        TFloatField *m_Q_barcode_infoNMCL_ID;
        TFloatField *m_Q_barcode_infoASSORT_ID;
        TFloatField *m_Q_barcode_infoITEMS_QNT;
        TStringField *m_Q_barcode_infoERROR;
        TFloatField *m_Q_barcode_infoCARD_ID;
        TFloatField *m_Q_barcode_infoOUT_PRICE;
        TMLQuery *m_Q_upins_line;
        TFloatField *m_Q_barcode_infoIN_PRICE;
        TQuery *m_Q_orgs2;
        TFloatField *m_Q_orgs2ID;
        TStringField *m_Q_orgs2NAME;
        TFloatField *m_Q_listMAXIMUM;
  void __fastcall m_Q_get_in_priceBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_barcode_infoBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_orgsBeforeOpen(TDataSet *DataSet);
private:
  int m_home_org_id, m_fldr_new;

  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_value);

  TS_DOC_LINE_SQ_NAME("SQ_RTL_TTN_OUT_ITEMS")
  TDateTime __fastcall GetBeginDate() const {return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;};
  TDateTime __fastcall GetEndDate() const {return m_Q_list->ParamByName("END_DATE")->AsDateTime;};
public:
  __fastcall TDTTNOut(TComponent* p_owner, AnsiString p_prog_id);
  void __fastcall SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date);
  void __fastcall UpdateCard(const TCardOut &p_card_out);

  __property int OrgId = {read=GetOrgId, write=SetOrgId};
  __property int HomeOrgId  = { read=m_home_org_id };

  __property TDateTime BeginDate = { read = GetBeginDate };
  __property TDateTime EndDate = { read = GetEndDate };
  __property int FldrNew = { read = m_fldr_new };
  __fastcall ~TDTTNOut();
};
//---------------------------------------------------------------------------
#endif
