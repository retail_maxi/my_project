//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmRtlReturns.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TDRtlReturns::TDRtlReturns(TComponent* p_owner,
                                      AnsiString p_prog_id): TTSDDocument(p_owner,
                                                                          p_prog_id),
                                                             m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID"))),
                                                             m_fldr_print(StrToInt64(GetConstValue("FLDR_RTL_RETURNS_PRNT"))),
                                                             m_fldr_new(StrToInt64(GetConstValue("FLDR_RTL_RETURNS_NEW")))
{
  SetBeginEndDate(int(GetSysDate()),int(GetSysDate())+1);
  m_edit_cond = false;
  m_Q_orgs->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
}
//---------------------------------------------------------------------------
__fastcall TDRtlReturns::~TDRtlReturns()
{
  if( m_Q_orgs->Active ) m_Q_orgs->Close();
}

//---------------------------------------------------------------------------
TDateTime __fastcall TDRtlReturns::GetBeginDate()
{
  return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDRtlReturns::GetEndDate()
{
  return m_Q_list->ParamByName("END_DATE")->AsDateTime;     
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::SetBeginEndDate(const TDateTime &p_begin_date,
                                              const TDateTime &p_end_date)
{
  if( (int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  if( m_Q_list->Active ) RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDDocument::m_Q_itemAfterInsert(DataSet);

  m_Q_itemISSUED_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemCASH->AsString = 'Y';
  m_Q_itemRET_USER_ID->AsInteger = -1;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_cashsBeforeOpen(TDataSet *DataSet)
{
  m_Q_cashs->ParamByName("REPORT_DOC_ID")->AsInteger = m_Q_itemREPORT_DOC_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_linesBeforeOpen(TDataSet *DataSet)
{
  m_Q_lines->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_assortsBeforeOpen(TDataSet *DataSet)
{
  m_Q_assorts->ParamByName("NMCl_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_cash_retBeforeOpen(TDataSet *DataSet)
{
  m_Q_cash_ret->ParamByName("REPORT_DOC_ID")->AsInteger = m_Q_itemCHECK_DOC_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_checksBeforeOpen(TDataSet *DataSet)
{
  m_Q_checks->ParamByName("START_DATE")->AsDateTime = m_Q_itemSTART_DATE->AsDateTime;
  m_Q_checks->ParamByName("END_DATE")->AsDateTime = m_Q_itemEND_DATE->AsDateTime;
  m_Q_checks->ParamByName("HOME_ORG_ID")->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_check_nmclBeforeOpen(TDataSet *DataSet)
{
  m_Q_check_nmcl->ParamByName("ORG_ID")->AsInteger = m_home_org_id;
  m_Q_check_nmcl->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_check_nmcl->ParamByName("ASSORT_ID")->AsInteger = m_Q_lineASSORT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_staff_listBeforeOpen(TDataSet *DataSet)
{
   m_Q_staff_list->ParamByName("ORG_ID")->AsInteger = m_Q_listDOC_SRC_ORG_ID->AsInteger;
}
//---------------------------------------------------------------------------

int __fastcall TDRtlReturns::GetOrgId()
{
  return m_Q_list->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::SetOrgId(int p_value)
{
  if( GetOrgId() == p_value ) return;

  m_Q_list->ParamByName("ORG_ID")->AsInteger = p_value;

  if( m_Q_list->Active )
  {
    CloseListDataSets();
    OpenListDataSets();
  }
}
//---------------------------------------------------------------------------
void __fastcall TDRtlReturns::m_Q_form_quanBeforeOpen(TDataSet *DataSet)
{
  m_Q_form_quan->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_form_quan->ParamByName("QUANTITY")->AsFloat = m_Q_lineQUANTITY->AsFloat;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_zreportsBeforeOpen(TDataSet *DataSet)
{
   if (m_Q_itemCHECK_DOC_ID->IsNull)
     m_Q_zreports->ParamByName("REPORT_DOC_ID")->AsInteger = -1;
   else
     m_Q_zreports->ParamByName("REPORT_DOC_ID")->AsInteger = m_Q_itemCHECK_DOC_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_alcoAfterInsert(TDataSet *DataSet)
{
  m_Q_alcoDOC_ID->AsInteger = m_Q_itemID->AsInteger;
  m_Q_alcoLINE_ID->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_alcoBeforeOpen(TDataSet *DataSet)
{
  m_Q_alco->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;
  m_Q_alco->ParamByName("LINE_ID")->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_alcAfterInsert(TDataSet *DataSet)
{
   m_Q_alcDOC_ID->AsInteger =m_Q_itemID->AsInteger;
   m_Q_alcLINE_ID->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_pdf_bar_codeAfterInsert(
      TDataSet *DataSet)
{
   m_Q_alcDOC_ID->AsInteger = m_Q_itemID->AsInteger;
   m_Q_alcLINE_ID->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_pdf_bar_codeBeforeOpen(TDataSet *DataSet)
{
    m_Q_alc->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;
    m_Q_alc->ParamByName("LINE_ID")->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_alcBeforeOpen(TDataSet *DataSet)
{
    m_Q_alc->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;
    m_Q_alc->ParamByName("LINE_ID")->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_find_checkBeforeOpen(TDataSet *DataSet)
{
  m_Q_find_check->ParamByName("HOME_ORG_ID")->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlReturns::m_Q_nmclsBeforeOpen(TDataSet *DataSet)
{
  m_Q_nmcls->ParamByName("REPORT_DOC_ID")->AsInteger = m_Q_itemREPORT_DOC_ID->AsInteger;
  m_Q_nmcls->ParamByName("HOME_ORG_ID")->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

