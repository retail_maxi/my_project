inherited DRtlZReports: TDRtlZReports
  OldCreateOrder = False
  Left = 339
  Top = 315
  Height = 546
  Width = 867
  inherited m_DB_main: TDatabase
    AliasName = 'ANALYTICAL'
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'SELECT d.id AS id,'
      '       d.src_org_id AS doc_src_org_id,'
      '       o.name AS doc_src_org_name,'
      '       d.create_date AS doc_create_date,'
      '       d.comp_name AS doc_create_comp_name,'
      '       d.author AS doc_create_user,'
      '       d.modify_date AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user AS doc_modify_user,'
      '       d.status AS doc_status,'
      
        '       decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �' +
        '� ������'#39','#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) AS doc' +
        '_fldr_right,'
      '       zr.fiscal_num,'
      '       zr.report_num,'
      '       zr.report_date,'
      '       zr.print_date,'
      '       zr.report_sum,'
      '       zr.sales_sum_disc,'
      '       zr.srpm_sum,'
      '       zr.report_10_sum,'
      '       zr.report_18_sum,'
      '       zr.bonus_point_sum,'
      '       t.name as taxpayer_name,'
      '       nvl(zr.ret_sum,0) as ret_sum'
      'FROM documents d,'
      '     folders f,'
      '     organizations o,'
      '     rtl_zreports zr,'
      '     taxpayer t'
      'WHERE f.doc_type_id = :DOC_TYPE_ID'
      '  AND f.id = d.fldr_id'
      '  AND o.id = d.src_org_id'
      '  AND fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '  AND zr.doc_id = d.id'
      '  AND zr.report_date BETWEEN :BEGIN_DATE AND :END_DATE '
      '  AND t.id = zr.fiscal_taxpayer_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      '')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_listFISCAL_NUM: TFloatField
      DisplayLabel = '���'
      FieldName = 'FISCAL_NUM'
      Origin = 'zr.fiscal_num'
    end
    object m_Q_listREPORT_NUM: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'REPORT_NUM'
      Origin = 'zr.report_num'
    end
    object m_Q_listREPORT_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'REPORT_DATE'
      Origin = 'zr.report_date'
    end
    object m_Q_listTAXPAYER_NAME: TStringField
      DisplayLabel = '����������������'
      FieldName = 'TAXPAYER_NAME'
      Origin = 't.name'
      Size = 100
    end
    object m_Q_listPRINT_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'PRINT_DATE'
      Origin = 'zr.print_date'
    end
    object m_Q_listREPORT_SUM: TFloatField
      DisplayLabel = '����� ������'
      FieldName = 'REPORT_SUM'
      Origin = 'zr.report_sum'
      currency = True
    end
    object m_Q_listREPORT_10_SUM: TFloatField
      DisplayLabel = '����� ���10'
      FieldName = 'REPORT_10_SUM'
      Origin = 'zr.report_10_sum'
      currency = True
    end
    object m_Q_listREPORT_18_SUM: TFloatField
      DisplayLabel = '����� ���18'
      FieldName = 'REPORT_18_SUM'
      Origin = 'zr.report_18_sum'
      currency = True
    end
    object m_Q_listSRPM_SUM: TFloatField
      DisplayLabel = '����� ����'
      FieldName = 'SRPM_SUM'
      Origin = 'zr.srpm_sum'
      currency = True
    end
    object m_Q_listSALES_SUM_DISC: TFloatField
      DisplayLabel = '����� ������'
      FieldName = 'SALES_SUM_DISC'
      Origin = 'zr.sales_sum_disk'
      currency = True
    end
    object m_Q_listBONUS_POINT_SUM: TFloatField
      DisplayLabel = '����� �������'
      FieldName = 'BONUS_POINT_SUM'
      Origin = 'zr.bonus_point_sum'
    end
    object m_Q_listRET_SUM: TFloatField
      DisplayLabel = '����� ���������'
      FieldName = 'RET_SUM'
      Origin = 'zr.ret_sum'
      currency = True
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'SELECT zr.doc_id AS id,'
      '  zr.fiscal_num,'
      '  zr.report_num,'
      '  zr.report_date,'
      '  zr.print_date,'
      '  zr.report_sum,'
      '  zr.sales_sum_disc,'
      '  zr.srpm_sum,'
      '  zr.report_10_sum,'
      '  zr.report_18_sum,'
      '  zr.bonus_point_sum,'
      '  t.name as taxpayer_name,'
      '  st.name stock_name,'
      '  nvl(zr.ret_sum,0) as ret_sum'
      'FROM rtl_zreports zr,'
      '     taxpayer t, stock_ref st'
      'WHERE zr.doc_id = :ID'
      '  AND t.id = zr.fiscal_taxpayer_id'
      
        '  AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_id' +
        ' = :FLDR_ID)'
      '  AND st.id(+) = zr.stock_id')
    object m_Q_itemFISCAL_NUM: TFloatField
      FieldName = 'FISCAL_NUM'
    end
    object m_Q_itemREPORT_NUM: TFloatField
      FieldName = 'REPORT_NUM'
    end
    object m_Q_itemREPORT_DATE: TDateTimeField
      FieldName = 'REPORT_DATE'
    end
    object m_Q_itemPRINT_DATE: TDateTimeField
      FieldName = 'PRINT_DATE'
    end
    object m_Q_itemREPORT_SUM: TFloatField
      FieldName = 'REPORT_SUM'
      currency = True
    end
    object m_Q_itemREPORT_10_SUM: TFloatField
      FieldName = 'REPORT_10_SUM'
      currency = True
    end
    object m_Q_itemREPORT_18_SUM: TFloatField
      FieldName = 'REPORT_18_SUM'
      currency = True
    end
    object m_Q_itemTAXPAYER_NAME: TStringField
      FieldName = 'TAXPAYER_NAME'
      Size = 100
    end
    object m_Q_itemSRPM_SUM: TFloatField
      FieldName = 'SRPM_SUM'
      currency = True
    end
    object m_Q_itemSALES_SUM_DISC: TFloatField
      FieldName = 'SALES_SUM_DISC'
      currency = True
    end
    object m_Q_itemSTOCK_NAME: TStringField
      FieldName = 'STOCK_NAME'
      Size = 150
    end
    object m_Q_itemBONUS_POINT_SUM: TFloatField
      DisplayLabel = '����� �������'
      FieldName = 'BONUS_POINT_SUM'
    end
    object m_Q_itemRET_SUM: TFloatField
      FieldName = 'RET_SUM'
      currency = True
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      ' update rtl_zreports'
      ' set report_10_sum = :REPORT_10_SUM,'
      '  report_18_sum = :REPORT_18_SUM'
      ' where doc_id = :OLD_ID')
  end
  object m_Q_oper: TMLQuery
    BeforeOpen = m_Q_operBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        ' select trim(rpad(pkg_contractors.fnc_contr_full_name(o.cnt_id),' +
        '250)) as cnt_name, '
      '        o.cnt_sum , nvl(o.ret_sum,0) as ret_sum'
      ' from rtl_zreports_oper o'
      ' where o.doc_id = :Z_ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 40
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Z_ID'
        ParamType = ptInput
      end>
    object m_Q_operCNT_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'CNT_NAME'
      Origin = 'trim(rpad(pkg_contractors.fnc_contr_full_name(o.cnt_id),250))'
      Size = 250
    end
    object m_Q_operCNT_SUM: TFloatField
      DisplayLabel = '�����'
      DisplayWidth = 50
      FieldName = 'CNT_SUM'
      Origin = 'o.cnt_sum'
      currency = True
    end
    object m_Q_operRET_SUM: TFloatField
      DisplayLabel = '��������'
      DisplayWidth = 50
      FieldName = 'RET_SUM'
      Origin = 'o.ret_sum'
      currency = True
    end
  end
  object m_Q_sprm: TMLQuery
    BeforeOpen = m_Q_sprmBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      ' select d.dep_number,'
      '  s.nmcl_id,'
      '  ni.name as nmcl_name,'
      '  s.nds,'
      '  s.out_price,'
      '  s.quantity'
      ' from rtl_zreports_srpm s,'
      '  shop_departments d,'
      '  nomenclature_items ni'
      ' where s.doc_id = :Z_ID'
      '  and d.id = s.shop_dep_id'
      '  and ni.id = s.nmcl_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 120
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Z_ID'
        ParamType = ptInput
      end>
    object m_Q_sprmDEP_NUMBER: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'DEP_NUMBER'
      Origin = 's.dep_number'
    end
    object m_Q_sprmNMCL_ID: TFloatField
      DisplayLabel = '���'
      FieldName = 'NMCL_ID'
      Origin = 's.nmcl_id'
    end
    object m_Q_sprmNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_sprmNDS: TFloatField
      DisplayLabel = '���'
      FieldName = 'NDS'
      Origin = 's.nds'
    end
    object m_Q_sprmOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 's.out_price'
      currency = True
    end
    object m_Q_sprmQUANTITY: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QUANTITY'
      Origin = 's.quantity'
    end
  end
  object m_Q_ret: TMLQuery
    BeforeOpen = m_Q_retBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      ' select d.dep_number,'
      '  s.nmcl_id,'
      '  ni.name as nmcl_name,'
      '  s.nds,'
      '  s.out_price,'
      '  s.quantity'
      ' from rtl_zreports_ret s,'
      '  shop_departments d,'
      '  nomenclature_items ni'
      ' where s.doc_id = :Z_ID'
      '  and d.id = s.shop_dep_id'
      '  and ni.id = s.nmcl_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 184
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Z_ID'
        ParamType = ptInput
      end>
    object m_Q_retDEP_NUMBER: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'DEP_NUMBER'
      Origin = 'd.dep_number'
    end
    object m_Q_retNMCL_ID: TFloatField
      DisplayLabel = '���'
      FieldName = 'NMCL_ID'
      Origin = 's.NMCL_ID'
    end
    object m_Q_retNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.NAME'
      Size = 100
    end
    object m_Q_retNDS: TFloatField
      DisplayLabel = '���'
      FieldName = 'NDS'
      Origin = 's.NDS'
    end
    object m_Q_retOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 's.OUT_PRICE'
      currency = True
    end
    object m_Q_retQUANTITY: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QUANTITY'
      Origin = 's.QUANTITY'
    end
  end
end
