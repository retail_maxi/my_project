//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlReturnLine.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DLBCScaner"
#pragma link "RXCtrls"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnLine::FormShow(TObject *Sender)
{
  if( m_dm->m_Q_itemCHECK_NUM->IsNull )
  {
    Panel2->Visible = true;
    m_DBG_lines->Visible = false;
    Panel1->Visible = false;
    m_DBG_alco->Visible = false;
    m_P_lines_control->Visible = false;
  }
  else
  {
    Panel2->Visible = false;
    m_DBG_lines->Visible = true;
    Panel1->Visible = true;
    m_DBG_alco->Visible = true;
    m_P_lines_control->Visible = true;
  }


  if (m_dm->UpdateRegimeLine == 1){
                Panel2->Visible = false;
                m_DBG_lines->Visible = false;
                Panel1->Visible = false;
                m_DBG_alco->Visible = true;
                m_P_lines_control->Visible = true;
       if  (m_dm->UpdateRegimeItem != urView){
   m_BCS_line->Active = true;
       }
       else {
   m_BCS_line->Active = false;
       }
  }  else {
                m_DBG_alco->Visible = false;
                m_P_lines_control->Visible = false;
  }

   if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
    m_dm->m_Q_alc->Open();
       if (m_dm->m_Q_alco->Active) m_dm->m_Q_alco->Close();
    m_dm->m_Q_alco->Open();


}
//---------------------------------------------------------------------------


void __fastcall TFRtlReturnLine::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  if (m_dm->m_Q_form_quan->Active) m_dm->m_Q_form_quan->Close();
  m_dm->m_Q_form_quan->Open();
  if (m_dm->m_Q_form_quanERR_QUANT->AsInteger >0)
  {
    ShowMessage("��� ���������� �������� ������ ������� ������� ����������!");
    return;
  }
  else
  {
    if (m_dm->m_Q_itemCHECK_NUM->IsNull)
    {
      if (m_dm->m_Q_check_nmcl->Active) m_dm->m_Q_check_nmcl->Close();
      m_dm->m_Q_check_nmcl->Open();
      if (m_dm->m_Q_check_nmclCH->AsInteger == 0)
      {
        ShowMessage("����� �� ������ � ��������!");
        m_dm->m_Q_lineNMCL_ID->Clear();
        m_dm->m_Q_lineNMCL_NAME->Clear();
        m_dm->m_Q_lineASSORT_ID->Clear();
        m_dm->m_Q_lineASSORT_NAME->Clear();
        return;
      }
    }
    TTSFSubItem::m_ACT_apply_updatesExecute(Sender);
  }
  m_dm->m_Q_form_quan->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnLine::m_ACT_delUpdate(TObject *Sender)
{
      m_ACT_del->Enabled = m_dm->UpdateRegimeItem != urView
                       && !m_dm->m_Q_alcSUB_LINE_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnLine::m_ACT_delExecute(TObject *Sender)
{
   //if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Delete();
  m_dm->m_Q_alco->ApplyUpdates();
  if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();            
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnLine::m_BCS_lineBegin(TObject *Sender)
{
   m_temp_grid_options = m_DBG_alco->OptionsML;
   m_DBG_alco->OptionsML = TMLGridOptions() << goWithoutFind;            
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnLine::m_BCS_lineEnd(TObject *Sender)
{
  try
  {
        if (m_dm->m_Q_form_quan->Active) m_dm->m_Q_form_quan->Close();
        m_dm->m_Q_form_quan->Open();
        if (m_dm->m_Q_form_quanERR_QUANT->AsInteger >0)
        {
          ShowMessage("��� ���������� �������� ������ ������� ������� ����������!");
          return;
        }
        else
        {
          if (m_dm->m_Q_itemCHECK_NUM->IsNull)
          {
            if (m_dm->m_Q_check_nmcl->Active) m_dm->m_Q_check_nmcl->Close();
            m_dm->m_Q_check_nmcl->Open();
            if (m_dm->m_Q_check_nmclCH->AsInteger == 0)
            {
              ShowMessage("����� �� ������ � ��������!");
              m_dm->m_Q_lineNMCL_ID->Clear();
              m_dm->m_Q_lineNMCL_NAME->Clear();
              m_dm->m_Q_lineASSORT_ID->Clear();
              m_dm->m_Q_lineASSORT_NAME->Clear();
              return;
            }
          }
          TTSFSubItem::m_ACT_apply_updatesExecute(Sender);
        }

    String s = m_BCS_line->GetResult();
    
    m_dm->m_get_PDF_BAR_CODE->ParamByName("pdf_bar_code")->AsString = s;
    if (m_dm->m_get_PDF_BAR_CODE->Active) m_dm->m_get_PDF_BAR_CODE->Close();
      m_dm->m_get_PDF_BAR_CODE->Open();
    s = m_dm->m_get_PDF_BAR_CODEPDF_BAR_CODE_OUT->AsString;


    m_dm->m_chek_pdf ->ParamByName("nmcl_id")->AsInteger = m_dm->m_Q_linesNMCL_ID->AsInteger;
    m_dm->m_chek_pdf ->ParamByName("pdf_bar_code")->AsString = s;

  if (m_dm->m_chek_pdf->Active) m_dm->m_chek_pdf->Close();
      m_dm->m_chek_pdf->Open();

     if   (m_dm->m_chek_pdfIS_NEED_CRT->IsNull  )
     {
       throw ETSError("������ �������� ����� �� ��������� � ������������!");
     }
     else
     { }

    while (!m_dm->m_Q_alc->Eof)
    {
      if (m_dm->m_Q_alcPDF_BAR_CODE->AsString == s)
        throw ETSError("������ �������� ����� ��� ���������!");
      m_dm->m_Q_alc->Next();
    }
    if (m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();
    m_dm->m_Q_pdf_bar_code->Close();
    m_dm->m_Q_pdf_bar_code->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_pdf_bar_code->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_linesLINE_ID->AsInteger;
    m_dm->m_Q_pdf_bar_code->ParamByName("PDF_BAR_CODE")->AsString = s;
    m_dm->m_Q_pdf_bar_code->ExecSQL();

    if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
    m_dm->m_Q_alc->Open();
  }
  __finally
  {
    m_DBG_alco->OptionsML = m_temp_grid_options;
  }
}
//---------------------------------------------------------------------------


