inherited FMessageFromRKUItem: TFMessageFromRKUItem
  Left = 256
  Top = 114
  Caption = 'FMessageFromRKUItem'
  ClientHeight = 300
  ClientWidth = 696
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_doc_number: TLabel [0]
    Left = 74
    Top = 45
    Width = 71
    Height = 13
    Caption = '� ���������'
  end
  object m_L_doc_date: TLabel [1]
    Left = 223
    Top = 45
    Width = 77
    Height = 13
    Caption = '���� ��������'
  end
  object m_L_send_date: TLabel [2]
    Left = 416
    Top = 45
    Width = 94
    Height = 13
    Caption = '���� �����������'
  end
  object m_L_host: TLabel [3]
    Left = 57
    Top = 75
    Width = 88
    Height = 13
    Caption = '��� ����������'
  end
  object m_L_rku: TLabel [4]
    Left = 109
    Top = 105
    Width = 36
    Height = 13
    Caption = '� ���'
    FocusControl = m_DBE_rku
  end
  object m_L_type_mess: TLabel [5]
    Left = 65
    Top = 135
    Width = 79
    Height = 13
    Caption = '��� ���������'
    FocusControl = m_LV_type_message
  end
  object m_L_cre_user_id: TLabel [6]
    Left = 3
    Top = 225
    Width = 142
    Height = 13
    Caption = '������������ �����������'
  end
  object m_L_note: TLabel [7]
    Left = 85
    Top = 160
    Width = 58
    Height = 13
    Caption = 'C��������'
    FocusControl = m_DBM_note
  end
  inherited m_P_main_control: TPanel
    Top = 250
    Width = 696
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 599
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 308
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 114
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 281
    Width = 696
  end
  inherited m_P_main_top: TPanel
    Width = 696
    inherited m_P_tb_main: TPanel
      Width = 649
      inherited m_TB_main: TToolBar
        Width = 649
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 649
    end
  end
  object m_DBP_doc_number: TMLDBPanel [11]
    Left = 150
    Top = 40
    Width = 61
    Height = 21
    DataField = 'DOC_NUMBER'
    DataSource = m_DS_item
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 3
  end
  object m_DBP_doc_date: TMLDBPanel [12]
    Left = 305
    Top = 40
    Width = 101
    Height = 21
    DataField = 'DOC_DATE'
    DataSource = m_DS_item
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 4
  end
  object m_DBP_send_date: TMLDBPanel [13]
    Left = 515
    Top = 40
    Width = 101
    Height = 21
    DataField = 'READ_DATE'
    DataSource = m_DS_item
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 5
  end
  object m_DBE_rku: TDBEdit [14]
    Left = 150
    Top = 100
    Width = 61
    Height = 21
    DataField = 'RKU'
    DataSource = m_DS_item
    TabOrder = 6
  end
  object m_LV_type_message: TMLLovListView [15]
    Left = 150
    Top = 130
    Width = 451
    Height = 21
    Lov = m_LOV_type_mess
    UpDownWidth = 456
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 7
    ParentColor = False
  end
  object m_DBP_read_user: TMLDBPanel [16]
    Left = 150
    Top = 220
    Width = 451
    Height = 21
    DataField = 'READ_USER_NAME'
    DataSource = m_DS_item
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 8
  end
  object m_DBM_note: TDBMemo [17]
    Left = 150
    Top = 160
    Width = 451
    Height = 51
    DataField = 'NOTE'
    DataSource = m_DS_item
    MaxLength = 250
    TabOrder = 9
  end
  object m_DBP_host: TMLDBPanel [18]
    Left = 150
    Top = 70
    Width = 256
    Height = 21
    DataField = 'HOST'
    DataSource = m_DS_item
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 10
  end
  inherited m_DS_item: TDataSource
    DataSet = DMessageFromRKU.m_Q_item
  end
  object m_DS_type_mess: TDataSource
    DataSet = DMessageFromRKU.m_Q_type_mess_list
    Left = 235
    Top = 125
  end
  object m_LOV_type_mess: TMLLov
    DataFieldKey = 'TYPE_MESSAGE_ID'
    DataFieldValue = 'MESSAGE_TYPE'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_type_mess
    AutoOpenList = True
    Left = 265
    Top = 125
  end
end
