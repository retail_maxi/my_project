//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocRtlCouponDiscList.h"
#include "TSErrors.h"
#include "BarCode.h"
#include "TSFmDBDateDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTLIST"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);

  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
}
//---------------------------------------------------------------------------


void __fastcall TFDocRtlCouponDiscList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------


void __fastcall TFDocRtlCouponDiscList::m_ACT_editUpdate(TObject *Sender)
{
 m_SBTN_edit->Enabled = (m_dm->m_Q_listDOC_FLDR_ID->AsInteger == 8252121)||
                        (m_dm->m_Q_listDOC_FLDR_ID->AsInteger == 8252206);
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscList::m_ACT_deleteUpdate(TObject *Sender)
{
  m_SBTN_delete->Enabled = (m_dm->m_Q_listDOC_FLDR_ID->AsInteger == 8252121);
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscList::m_ACT_end_dateUpdate(
      TObject *Sender)
{
  m_ACT_end_date->Enabled = !m_dm->m_Q_listID->IsNull && (m_dm->m_Q_listDOC_FLDR_ID->AsInteger == 8252122 || m_dm->m_Q_listDOC_FLDR_ID->AsInteger == 8252123) && m_can_change_date;
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscList::m_ACT_end_dateExecute(
      TObject *Sender)
{
  long doc_id = m_dm->m_Q_listID->AsInteger;
  if (m_dm->m_Q_set_date->Active) m_dm->m_Q_set_date->Close();
  m_dm->m_Q_set_date->ParamByName("doc_id")->AsInteger = doc_id;
  m_dm->m_Q_set_date->Open();
  TDateTime dt = m_dm->m_Q_set_dateEND_DATE->AsDateTime;
  if (TSExecDBDateDlg(this, "���� ���������", m_dm->m_Q_set_date, "END_DATE")
      && (dt < m_dm->m_Q_set_dateEND_DATE->AsDateTime)){
    m_dm->m_Q_upd_date->ParamByName("doc_id")->AsInteger = doc_id;
    m_dm->m_Q_upd_date->ParamByName("end_date")->AsDateTime = m_dm->m_Q_set_dateEND_DATE->AsDateTime;
    m_dm->m_Q_upd_date->ExecSQL();
    m_dm->RefreshListDataSets();
    m_dm->m_Q_list->Locate(m_dm->m_Q_listID->FieldName,doc_id,TLocateOptions());
  }
}
//---------------------------------------------------------------------------

