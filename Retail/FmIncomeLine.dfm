inherited FIncomeLine: TFIncomeLine
  Left = 802
  Top = 156
  ActiveControl = m_FRM_rtl_line.m_LV_nmcls
  Caption = 'FIncomeLine'
  ClientHeight = 427
  ClientWidth = 692
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_qnt: TLabel [0]
    Left = 28
    Top = 112
    Width = 59
    Height = 13
    Caption = '����������'
  end
  object m_L_tax_rate: TLabel [1]
    Left = 584
    Top = 112
    Width = 24
    Height = 13
    Caption = '���'
  end
  object Label5: TLabel [2]
    Left = 48
    Top = 144
    Width = 30
    Height = 13
    Caption = '����*'
  end
  object Label7: TLabel [3]
    Left = 52
    Top = 176
    Width = 26
    Height = 13
    Caption = '����'
  end
  object Label9: TLabel [4]
    Left = 196
    Top = 176
    Width = 34
    Height = 13
    Caption = '�����'
  end
  object Label6: TLabel [5]
    Left = 364
    Top = 112
    Width = 58
    Height = 13
    Caption = '�������, %'
  end
  object m_L_src_amount: TLabel [6]
    Left = 180
    Top = 112
    Width = 58
    Height = 13
    Caption = '����� ���.'
  end
  object Label2: TLabel [7]
    Left = 512
    Top = 176
    Width = 88
    Height = 13
    Caption = '������� �������'
  end
  inherited m_P_main_control: TPanel
    Top = 377
    Width = 692
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 498
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 401
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 408
    Width = 692
  end
  inherited m_P_main_top: TPanel
    Width = 692
    TabOrder = 6
    inherited m_P_tb_main_add: TPanel
      Left = 645
    end
    inherited m_P_tb_main: TPanel
      Width = 645
      inherited m_TB_main: TToolBar
        Width = 645
        ButtonHeight = 22
        inherited m_SB_ods: TSpeedButton
          Height = 22
        end
        object ToolButton1: TToolButton
          Left = 183
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 11
          Style = tbsSeparator
        end
        object ToolButton2: TToolButton
          Left = 191
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 12
          Style = tbsSeparator
        end
        object m_CB_view_only_open: TCheckBox
          Left = 199
          Top = 2
          Width = 289
          Height = 22
          Caption = '�������� ������ �������� �������'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = m_CB_view_only_openClick
        end
      end
    end
  end
  object m_DBE_qnt: TDBEdit [11]
    Left = 96
    Top = 108
    Width = 69
    Height = 21
    DataField = 'QNT'
    DataSource = m_DS_line
    TabOrder = 2
    OnExit = m_DBE_qntExit
  end
  object m_DBP_tax_rate: TMLDBPanel [12]
    Left = 616
    Top = 108
    Width = 65
    Height = 21
    DataField = 'VALUE'
    DataSource = m_DS_tax_rate
    Alignment = taRightJustify
    BevelWidth = 0
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 10
  end
  object m_DBE_in_price: TDBEdit [13]
    Left = 96
    Top = 140
    Width = 69
    Height = 21
    DataField = 'IN_PRICE'
    DataSource = m_DS_line
    ParentColor = True
    ReadOnly = True
    TabOrder = 4
    OnExit = m_DBE_in_priceExit
  end
  object m_DBE_out_price: TDBEdit [14]
    Left = 96
    Top = 172
    Width = 69
    Height = 21
    TabStop = False
    Color = clBtnFace
    DataField = 'OUT_PRICE'
    DataSource = m_DS_line
    ReadOnly = True
    TabOrder = 7
  end
  object m_DBE_amount_out: TDBEdit [15]
    Left = 244
    Top = 172
    Width = 89
    Height = 21
    TabStop = False
    Color = clBtnFace
    DataField = 'OUT_AMOUNT'
    DataSource = m_DS_line
    ReadOnly = True
    TabOrder = 8
  end
  object m_PC_bottom: TPageControl [16]
    Left = 0
    Top = 201
    Width = 692
    Height = 176
    ActivePage = m_TS_extra
    Align = alBottom
    TabOrder = 11
    object m_TS_extra: TTabSheet
      Caption = '�������������'
      object m_L_country_name: TLabel
        Left = 50
        Top = 12
        Width = 118
        Height = 13
        Caption = '������ �������������'
        FocusControl = m_LV_countries
      end
      object m_L_num_gtd: TLabel
        Left = 448
        Top = 12
        Width = 36
        Height = 13
        Caption = '� ���'
        FocusControl = m_DBE_num_gtd
      end
      object m_L_reason: TLabel
        Left = 41
        Top = 44
        Width = 128
        Height = 13
        Caption = '���������� "�� � �����"'
      end
      object m_L_note: TLabel
        Left = 107
        Top = 76
        Width = 63
        Height = 13
        Caption = '����������'
      end
      object Label1: TLabel
        Left = 80
        Top = 108
        Width = 91
        Height = 13
        Caption = '���������� � ��'
      end
      object m_LV_countries: TMLLovListView
        Left = 176
        Top = 8
        Width = 241
        Height = 21
        Lov = m_LOV_countries
        UpDownWidth = 249
        UpDownHeight = 120
        Interval = 500
        TabStop = True
        TabOrder = 0
        ParentColor = False
      end
      object m_DBE_num_gtd: TDBEdit
        Left = 492
        Top = 8
        Width = 181
        Height = 21
        DataField = 'SCD_NUM'
        DataSource = m_DS_line
        TabOrder = 1
      end
      object m_LV_reason: TMLLovListView
        Left = 176
        Top = 40
        Width = 241
        Height = 21
        Lov = m_LOV_reason
        UpDownWidth = 241
        UpDownHeight = 121
        Interval = 500
        TabStop = True
        TabOrder = 2
        ParentColor = False
      end
      object m_DBE_note: TDBEdit
        Left = 176
        Top = 72
        Width = 497
        Height = 21
        DataField = 'NOTE'
        DataSource = m_DS_line
        TabOrder = 3
      end
      object m_DBE_bc_note: TDBEdit
        Left = 175
        Top = 103
        Width = 501
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'NOTE_BC'
        DataSource = m_DS_line
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
      end
    end
    object m_TS_shelf_life_: TTabSheet
      Caption = '����� ��������'
      ImageIndex = 1
      inline m_FRM_shelf_life: TFRShelfLifeLists
        Left = -12
        Top = -6
      end
    end
    object m_TS_bar_codes: TTabSheet
      Caption = '����� ����'
      ImageIndex = 2
      object m_MLDBG_bar_codes: TMLDBGrid
        Left = 0
        Top = 0
        Width = 684
        Height = 148
        Align = alClient
        DataSource = m_DS_bar_codes
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'BAR_CODE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'NMCL_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'ASSORTMENT_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'IN_MAIN_MEAS'
            Title.TitleButton = True
            Width = 71
            Footers = <>
          end
          item
            FieldName = 'NOTE'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'NOTE_BC'
            Title.TitleButton = True
            Footers = <>
          end>
      end
    end
    object m_TS_sublines: TTabSheet
      Caption = '�������� �����'
      ImageIndex = 3
      object m_MLDBG_sublines: TMLDBGrid
        Left = 0
        Top = 0
        Width = 684
        Height = 115
        Align = alClient
        DataSource = m_DS_sublines
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnEnter = m_MLDBG_sublinesEnter
        OnExit = m_MLDBG_sublinesExit
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'LINE_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'SUB_LINE_ID'
            Title.TitleButton = True
            Visible = False
            Footers = <>
          end
          item
            FieldName = 'PREF_ALCCODE'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'PDF_ALCCODE'
            Title.TitleButton = True
            Footers = <>
          end>
      end
      object m_P_lines_control: TPanel
        Left = 0
        Top = 115
        Width = 684
        Height = 33
        Align = alBottom
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 1
        object m_TB_lines_control_button: TToolBar
          Left = 0
          Top = 0
          Width = 97
          Height = 33
          Align = alLeft
          AutoSize = True
          ButtonHeight = 25
          Caption = 'm_TB_lines_control_button'
          EdgeBorders = []
          TabOrder = 0
          object m_SBTN_delete: TSpeedButton
            Left = 0
            Top = 2
            Width = 97
            Height = 25
            Action = m_ACT_delete
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000FF00FF00FF00FF000000
              0000C0C0C0000000800000008000000080000000800000008000000080000000
              80000000800000008000000080000000800000000000FF00FF00FF00FF000000
              0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
              FF000000FF000000FF000000FF000000800000000000FF00FF00FF00FF000000
              0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
              FF000000FF000000FF000000FF000000800000000000FF00FF00FF00FF000000
              0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
              C000C0C0C000C0C0C000C0C0C000C0C0C00000000000FF00FF00FF00FF000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
          end
        end
      end
    end
  end
  object m_DBE_increase_prc: TDBEdit [17]
    Left = 436
    Top = 108
    Width = 89
    Height = 21
    TabStop = False
    DataField = 'INCREASE_PRC'
    DataSource = m_DS_line
    ReadOnly = True
    TabOrder = 9
  end
  object m_DBE_src_amount: TDBEdit [18]
    Left = 244
    Top = 108
    Width = 89
    Height = 21
    DataField = 'SRC_AMOUNT'
    DataSource = m_DS_line
    TabOrder = 3
    OnExit = m_DBE_src_amountExit
  end
  inline m_FRM_rtl_line: TFRtlIncomeLine [19]
    Top = 32
    Width = 689
    TabOrder = 1
    inherited m_LV_nmcls: TMLLovListView
      Width = 521
      UpDownWidth = 521
    end
    inherited m_LV_assort: TMLLovListView
      Width = 401
      UpDownWidth = 401
    end
    inherited m_LOV_nmcls: TMLLov
      OnAfterApply = m_FRM_rtl_linem_LOV_nmclsAfterApply
      OnAfterClear = m_FRM_rtl_linem_LOV_nmclsAfterClear
    end
    inherited m_LOV_assort: TMLLov
      OnAfterApply = m_FRM_rtl_linem_LOV_assortAfterApply
      OnAfterClear = m_FRM_rtl_linem_LOV_assortAfterClear
    end
    inherited m_Q_assort_list: TMLQuery
      SQL.Strings = (
        'SELECT assortment_id, '
        '            assortment_name           '
        'FROM ('
        'SELECT b.assortment_id, '
        '               a.name AS assortment_name           '
        'FROM nmcl_bar_codes b, assortment a'
        'where :NMCL_ID > 0 '
        '   AND b.nmcl_id = :NMCL_ID '
        '   AND a.id = b.assortment_id'
        '  AND b.assortment_id != 7'
        '  AND b.enable = '#39'Y'#39
        'UNION ALL'
        'SELECT id AS assortment_id'
        '           , name AS assortment_name'
        'FROM assortment where id=7'
        ' AND :NMCL_ID < 0'
        ')'
        '%WHERE_EXPRESSION'
        'ORDER BY  assortment_name ')
    end
  end
  object m_DBE_curr_rest: TDBEdit [20]
    Left = 608
    Top = 172
    Width = 73
    Height = 21
    TabStop = False
    Color = clBtnFace
    DataField = 'CUR_REST'
    DataSource = m_DS_line
    ReadOnly = True
    TabOrder = 12
  end
  object m_P_wei: TPanel [21]
    Left = 392
    Top = 136
    Width = 297
    Height = 33
    BevelOuter = bvNone
    TabOrder = 13
    object Label3: TLabel
      Left = 6
      Top = 8
      Width = 99
      Height = 13
      Caption = '���� ������������'
    end
    object m_DBE_make_date: TDBDateEdit
      Left = 112
      Top = 4
      Width = 97
      Height = 21
      DataField = 'MAKE_DATE'
      DataSource = m_DS_line
      NumGlyphs = 2
      TabOrder = 0
    end
    object m_DTP_make_time: TDateTimePicker
      Left = 216
      Top = 4
      Width = 73
      Height = 21
      CalAlignment = dtaLeft
      Date = 39058
      Time = 39058
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkTime
      ParseInput = False
      TabOrder = 1
      OnChange = m_DTP_make_timeChange
    end
  end
  inherited m_IL_main: TImageList [22]
  end
  inherited m_ACTL_main: TActionList [23]
    object m_ACT_delete: TAction
      Caption = '�������'
      OnExecute = m_ACT_deleteExecute
      OnUpdate = m_ACT_deleteUpdate
    end
  end
  inherited m_IL_sb: TImageList [24]
  end
  inherited m_T_sb: TTimer [25]
  end
  inherited m_IL_main_add: TImageList [26]
  end
  inherited m_ACTL_main_add: TActionList [27]
    Left = 104
  end
  inherited m_PM_main_add: TPopupMenu [28]
    Left = 104
  end
  inherited m_SD_ods: TSaveDialog [29]
  end
  inherited m_DS_line: TDataSource
    DataSet = DIncome.m_Q_line
    Left = 296
    Top = 0
  end
  object m_DS_tax_rate: TDataSource
    DataSet = DIncome.m_Q_tax_rate
    Left = 544
    Top = 104
  end
  object m_LOV_countries: TMLLov
    DataFieldKey = 'COUNTRY_ID'
    DataFieldValue = 'COUNTRY_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_countries
    AutoOpenList = True
    Left = 312
    Top = 228
  end
  object m_DS_countries: TDataSource
    DataSet = DIncome.m_Q_countries
    Left = 284
    Top = 228
  end
  object m_LOV_reason: TMLLov
    DataFieldKey = 'NON_STORING_REASON_ID'
    DataFieldValue = 'REASON_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_reasons
    AutoOpenList = True
    Left = 312
    Top = 264
  end
  object m_DS_reasons: TDataSource
    DataSet = DIncome.m_Q_reasons
    Left = 280
    Top = 264
  end
  object m_DS_bar_codes: TDataSource
    DataSet = DIncome.m_Q_bar_codes
    Left = 100
    Top = 273
  end
  object m_DS_make_date: TDataSource
    DataSet = DIncome.m_Q_make_date
    Left = 568
    Top = 168
  end
  object m_DS_sublines: TDataSource
    DataSet = DIncome.m_Q_sublines
    Left = 12
    Top = 273
  end
  object m_BCS_subitem: TDLBCScaner
    Tick = 50
    BeginKey = 2
    EndKey = 3
    OnBegin = m_BCS_subitemBegin
    OnEnd = m_BCS_subitemEnd
    Top = 64
  end
end
