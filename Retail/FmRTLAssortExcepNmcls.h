//---------------------------------------------------------------------------

#ifndef FmRTLAssortExcepNmclsH
#define FmRTLAssortExcepNmclsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmInvAct.h"
#include "MLQuery.h"
#include "RxQuery.hpp"
#include <DBTables.hpp>
#include "dbtree_l.hpp"
#include "MLUpdateSQL.h"
//---------------------------------------------------------------------------

class TFRTLAssortExcepNmcls : public TTSFDialog
{
__published:
  TPanel *m_P_main;
  TPanel *m_P_left;
  TPanel *m_P_all;
  TSplitter *Splitter1;
  TPanel *m_P_btns;
  TSpeedButton *m_SBTN_add;
  TPageControl *m_PC_all_cnt;
        TTabSheet *m_TS_org;
  TSpeedButton *m_SBTN_delete;
  TDataSource *m_DS_nmcl;
  TMLDBGrid *m_DBG_all_nmcl;
  TAction *m_ACT_add;
  TAction *m_ACT_delete;
        TDataSource *m_DS_all_nmcl;
        TQuery *m_Q_add_nmcl;
        TQuery *m_Q_delete_nmcl;
        TMLQuery *m_Q_all_nmcl;
        TMLUpdateSQL *m_U_nmcl;
        TMLDBGrid *m_DBG_nmcl;
        TMLQuery *m_Q_nmcl;
        TFloatField *m_Q_all_nmclID;
        TStringField *m_Q_all_nmclNAME;
        TFloatField *m_Q_nmclNMCL_ID;
        TStringField *m_Q_nmclNMCL_NAME;
  void __fastcall m_ACT_addExecute(TObject *Sender);
  void __fastcall m_ACT_addUpdate(TObject *Sender);
  void __fastcall m_ACT_deleteExecute(TObject *Sender);
  void __fastcall m_ACT_deleteUpdate(TObject *Sender);

private:
  bool is_edit;
public:
  __fastcall TFRTLAssortExcepNmcls(TComponent* p_owner,
                     TTSDCustom* p_dm_custom, bool p_edit): TTSFDialog(p_owner,
                                                          p_dm_custom),
                                                          is_edit(p_edit) {};
};
//---------------------------------------------------------------------------

void RTLAssortExcepNmclsDlg(TComponent* p_owner, int p_id, TTSDCustom* p_dm, bool p_edit = false);
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

