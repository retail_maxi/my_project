//---------------------------------------------------------------------------

#ifndef DmInvSheetH
#define DmInvSheetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDInvSheet : public TTSDDocumentWithLines
{
__published:
  TStringField *m_Q_listDOC_NUMBER;
  TDateTimeField *m_Q_listDOC_DATE;
  TStringField *m_Q_listNOTE;
  TStringField *m_Q_itemDOC_NUMBER;
  TDateTimeField *m_Q_itemDOC_DATE;
  TStringField *m_Q_itemNOTE;
  TDateTimeField *m_Q_linesSTART_DATE;
  TFloatField *m_Q_linesNMCL_ID;
  TStringField *m_Q_linesNMCL_NAME;
  TStringField *m_Q_linesMU_NAME;
  TStringField *m_Q_linesASSORTMENT_NAME;
  TFloatField *m_Q_linesITEMS_QNT;
  TStringField *m_Q_linesNOTE;
  TStringField *m_Q_listACT;
  TDateTimeField *m_Q_lineSTART_DATE;
  TFloatField *m_Q_lineNMCL_ID;
  TStringField *m_Q_lineNMCL_NAME;
  TStringField *m_Q_lineMU_NAME;
  TFloatField *m_Q_lineASSORTMENT_ID;
  TStringField *m_Q_lineASSORTMENT_NAME;
  TFloatField *m_Q_lineITEMS_QNT;
  TStringField *m_Q_lineNOTE;
  TFloatField *m_Q_itemINV_DOC_ID;
  TMLQuery *m_Q_acts;
  TFloatField *m_Q_actsDOC_ID;
  TStringField *m_Q_actsNOTE;
  TStringField *m_Q_itemACT_NUMBER;
  TStringField *m_Q_actsACT_NUMBER;
  TMLQuery *m_Q_nmcls;
  TFloatField *m_Q_nmclsNMCL_ID;
  TStringField *m_Q_nmclsNMCL_NAME;
  TStringField *m_Q_nmclsMEAS_NAME;
  TMLQuery *m_Q_assortments;
  TFloatField *m_Q_assortmentsID;
  TStringField *m_Q_assortmentsNAME;
  TQuery *m_Q_barcode_info;
  TFloatField *m_Q_barcode_infoNMCL_ID;
  TStringField *m_Q_barcode_infoNMCL_NAME;
  TFloatField *m_Q_barcode_infoASSORTMENT_ID;
  TStringField *m_Q_barcode_infoASSORTMENT_NAME;
  TFloatField *m_Q_linesPROD_TYPE_ID;
  TStringField *m_Q_linesPROD_TYPE_NAME;
        TFloatField *m_Q_itemFLDR_ID;
        TMLQuery *m_Q_fill_lines;
        TQuery *m_Q_orgs;
        TFloatField *m_Q_orgsID;
        TStringField *m_Q_orgsNAME;
        TMLQuery *m_Q_alco;
        TFloatField *m_Q_alcoID;
        TFloatField *m_Q_alcoLINE_ID;
        TFloatField *m_Q_alcoSUB_LINE_ID;
        TStringField *m_Q_alcoPDF_BAR_CODE;
        TStringField *m_Q_alcoALCCODE;
        TFloatField *m_Q_alcoQNT;
        TStringField *m_Q_alcoFULLNAME;
        TMLUpdateSQL *m_U_alco;
        TMLQuery *m_Q_alc;
        TFloatField *m_Q_alcID;
        TFloatField *m_Q_alcLINE_ID;
        TFloatField *m_Q_alcSUB_LINE_ID;
        TStringField *m_Q_alcPDF_BAR_CODE;
        TStringField *m_Q_alcALCCODE;
        TFloatField *m_Q_alcQNT;
        TStringField *m_Q_alcFULLNAME;
        TMLQuery *m_Q_alccode;
        TStringField *m_Q_alccodePREF_ALCCODE;
        TStringField *m_Q_alccodeNAME;
        TMLQuery *m_Q_pdf_bar_code;
        TStringField *m_Q_linesDUBL_PDF_CODE;
        TStringField *m_Q_alcDUBL_PDF_CODE;
        TMLQuery *m_kod_snnum_chek;
        TStringField *m_kod_snnum_chekPDF_BARCODE;
        TMLQuery *m_Q_get_alccode;
        TMemoField *m_Q_get_alccodeALCCODE;
        TMemoField *m_Q_get_alccodePDF_BAR_CODE;
        TMLQuery *m_Q_ins_alco;
        TFloatField *m2;
        TStringField *m3;
        TStringField *m4;
        TMLQuery *m_alco_nmcl_chek;
        TStringField *m_alco_nmcl_chekALCO;
        TMLQuery *m_type_barcod;
        TFloatField *m_type_barcodID;
        TStringField *m_type_barcodVALUE;
        TStringField *m_type_barcodNAME;
        TMLQuery *m_Q_get_kod_fsm;
        TFloatField *m_Q_get_kod_fsmTYPE_ID;
        TStringField *m_Q_get_kod_fsmVALUE;
        TStringField *m_Q_get_kod_fsmNAME;
        TMLQuery *m_Q_del_dubl_bar_code;
  void __fastcall m_Q_assortmentsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_itemBeforeClose(TDataSet *DataSet);
  void __fastcall m_Q_barcode_infoAfterClose(TDataSet *DataSet);
  void __fastcall m_Q_lineAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_linesAfterOpen(TDataSet *DataSet);
        void __fastcall m_Q_alccodeBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcoBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcoAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_alcBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_alcAfterInsert(TDataSet *DataSet);
private:
 int m_home_org_id;
 int m_new_fldr;
 int m_max_qnt;
  TS_DOC_LINE_SQ_NAME("SQ_RTL_INV_SHEET_ITEMS")
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_value);
public:
  __fastcall TDInvSheet(TComponent* p_owner, AnsiString p_prog_id);
  __property int HomeOrgId = {read=m_home_org_id};
  __property int OrgId = {read=GetOrgId, write=SetOrgId};
  __property int NewFldr = {read = m_new_fldr};
  __property int MaxQnt = {read = m_max_qnt};
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date, const TDateTime &p_end_date);
  __fastcall ~TDInvSheet();
};
//---------------------------------------------------------------------------
#endif
