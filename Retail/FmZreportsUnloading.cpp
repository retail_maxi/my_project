//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmZreportsUnloading.h"
#include "FmSetDateTimePeriod.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmOperation"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFZreportsUnloading::TFZreportsUnloading(TComponent* p_owner, TTSDOperation *p_dm_operation):
    TTSFOperation(p_owner, p_dm_operation),
    m_dm(static_cast<TDZreportsUnloading*>(p_dm_operation))
{
}
//---------------------------------------------------------------------------
void __fastcall TFZreportsUnloading::m_DBLCB_taxCloseUp(TObject *Sender)
{
  m_dm->SetTaxpayer(m_DBLCB_tax->KeyValue);
}
//---------------------------------------------------------------------------
void __fastcall TFZreportsUnloading::SetPeriodCaption(TDateTime p_begin_date, TDateTime p_end_date)
{
      if (p_begin_date.DateTimeString().Length()>11)
        m_SBTN_period->Caption = "� " + p_begin_date.DateTimeString().SubString(1, p_begin_date.DateTimeString().Length()-3);
      else
        m_SBTN_period->Caption = "� " + p_begin_date.DateTimeString();
      if (p_end_date.DateTimeString().Length()>11)
        m_SBTN_period->Caption =m_SBTN_period->Caption  + " �� " + p_end_date.DateTimeString().SubString(1, p_end_date.DateTimeString().Length()-3);
      else
        m_SBTN_period->Caption =m_SBTN_period->Caption + " �� " + p_end_date.DateTimeString();

}
//---------------------------------------------------------------------------

void __fastcall TFZreportsUnloading::m_ACT_set_periodExecute(
      TObject *Sender)
{
  TFSetDateTimePeriod *f = new TFSetDateTimePeriod(this,m_dm->m_begin_date, m_dm->m_end_date);
  try
  {
    if (f->ShowModal() == IDOK)
    {
      SetPeriodCaption(f->BeginDate, f->EndDate);
      m_dm->SetBeginEndDate( f->BeginDate,f->EndDate);

    }
  }
  __finally
  {
    delete f;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFZreportsUnloading::FormShow(TObject *Sender)
{
  TTSFOperation::FormShow(Sender);
  SetPeriodCaption(m_dm->m_begin_date,m_dm->m_end_date);
  if (m_dm->m_Q_tax->FindFirst())
  {
      m_DBLCB_tax->KeyValue = m_dm->m_Q_taxID->AsInteger;
      m_DBLCB_taxCloseUp(Sender);
  }
}
//---------------------------------------------------------------------------

