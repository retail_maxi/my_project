//---------------------------------------------------------------------------

#ifndef FmRtlReturnsItemH
#define FmRtlReturnsItemH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENT.h"
#include "DmRtlReturns.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "MLDBPanel.h"
#include "MLLovView.h"
#include "MLLov.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
#include "ToolEdit.hpp"
#include "RXDBCtrl.hpp"
#include "DLBCScaner.h"
//---------------------------------------------------------------------------

class TFRtlReturnsItem : public TTSFDocument
{
__published:
  TPanel *m_P_top;
  TMLDBGrid *m_DBG_lines;
  TDataSource *m_DS_lines;
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TMLDBPanel *MLDBPanel1;
  TMLDBPanel *MLDBPanel2;
  TMLLovListView *MLLovListView1;
  TMLDBPanel *MLDBPanel3;
  TMLLovListView *MLLovListView2;
  TMLDBPanel *MLDBPanel4;
  TMLDBPanel *MLDBPanel5;
  TMLDBPanel *MLDBPanel6;
  TMLLovListView *MLLovListView3;
  TMLLov *MLLov1;
  TMLLov *MLLov2;
  TMLLov *MLLov3;
  TDataSource *m_DS_zreports;
  TDataSource *m_DS_cashs;
  TDataSource *m_DS_checks;
  TAction *m_ACT_append_line;
  TAction *m_ACT_delete_line;
  TPanel *Panel1;
  TSpeedButton *m_SBTN_append_line;
  TSpeedButton *m_SBTN_delete_line;
  TLabel *Label10;
  TDBEdit *DBEdit1;
        TLabel *Label11;
        TDBEdit *m_DBE_RETURN_NUMBER;
        TLabel *Label12;
        TMLLovListView *MLLovListView4;
        TDataSource *m_DS_cash_ret;
        TMLLov *MLLov4;
        TLabel *Label13;
        TDBEdit *DBEdit2;
        TDBCheckBox *m_DBCB_cash;
        TLabel *Label14;
        TDBDateEdit *m_DBDE_start_date;
   TBitBtn *BitBtn1;
   TAction *m_ACT_edit_recond;
   TLabel *Label15;
        TLabel *Label16;
        TMLLovListView *MLLovListView5;
        TMLLov *MLLov5;
        TDataSource *m_DS_staff_list;
        TDBDateEdit *m_DBDE_END_DATE;
        TLabel *m_L_1;
        TSpeedButton *m_SPB_note;
        TAction *m_ACT_note;
        TSpeedButton *m_SBTN_ACT_note;
        TAction *m_ACT_VIEW;
   TDLBCScaner *m_BCS_item;
  void __fastcall m_ACT_append_lineExecute(TObject *Sender);
  void __fastcall m_ACT_append_lineUpdate(TObject *Sender);
  void __fastcall m_ACT_delete_lineExecute(TObject *Sender);
  void __fastcall m_ACT_delete_lineUpdate(TObject *Sender);
        void __fastcall MLLov3AfterApply(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall MLLov4AfterApply(TObject *Sender);
        void __fastcall MLLov3AfterClear(TObject *Sender);
        void __fastcall MLLov4AfterClear(TObject *Sender);
   void __fastcall m_ACT_edit_recondExecute(TObject *Sender);
   void __fastcall m_ACT_edit_recondUpdate(TObject *Sender);
        void __fastcall m_ACT_noteExecute(TObject *Sender);
        void __fastcall m_ACT_VIEWExecute(TObject *Sender);
   void __fastcall m_BCS_itemEnd(TObject *Sender);
private:
  TDRtlReturns *m_dm;
public:
  __fastcall TFRtlReturnsItem(TComponent *p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

