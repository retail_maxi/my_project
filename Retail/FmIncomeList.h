//---------------------------------------------------------------------------

#ifndef FmIncomeListH
#define FmIncomeListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmIncome.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTLIST.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include "DLBCScaner.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFIncomeList : public TTSFDocumentList
{
__published:
  TMLDBGrid *m_DBG_nmcls;
  TDataSource *m_DS_nmcls;
  TSplitter *m_S_h;
  TToolButton *ToolButton1;
  TDBLookupComboBox *m_DBLCB_org;
  TDataSource *m_DS_orgs;
  TDataSource *m_DS_fict_org;
  TCheckBox *m_CB_show_nmcls;
  TAction *m_ACT_goto_doc;
  TSpeedButton *m_SBTN_goto_act;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TToolButton *ToolButton2;
  TSpeedButton *m_SBTN_set_date;
  TToolButton *ToolButton3;
  TEdit *m_E_nmcl_id;
  TToolButton *ToolButton4;
  TEdit *m_E_nmcl_name;
  TAction *m_ACT_nmcl_filter;
  TToolButton *m_TBTN_nmcl_filter;
  TAction *m_ACT_select_all;
  TSpeedButton *m_SBTN_select_all;
  TDLBCScaner *m_BCS_search;
  TAction *m_ACT_check_arrival;
  TSpeedButton *m_SB_check_arrival;
  TToolBar *ToolBar1;
  TSpeedButton *m_SB_stiker_info;
  TAction *m_ACT_print_label;
        TPanel *m_P_main_control2;
        TToolBar *ToolBar4;
        TSpeedButton *SpeedButton8;
        TSpeedButton *SpeedButton1;
        TAction *m_ACT_copy_income;
        TAction *m_ACT_create_edit_qnt;
  void __fastcall m_DS_fict_orgDataChange(TObject *Sender, TField *Field);
  void __fastcall m_CB_show_nmclsClick(TObject *Sender);
  void __fastcall m_ACT_goto_docUpdate(TObject *Sender);
  void __fastcall m_ACT_goto_docExecute(TObject *Sender);
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_E_nmcl_idChange(TObject *Sender);
  void __fastcall m_E_nmcl_nameChange(TObject *Sender);
  void __fastcall m_ACT_nmcl_filterExecute(TObject *Sender);
  void __fastcall m_DBG_nmclsDblClick(TObject *Sender);
  void __fastcall m_ACT_insertUpdate(TObject *Sender);
  void __fastcall m_ACT_editUpdate(TObject *Sender);
  void __fastcall m_ACT_deleteUpdate(TObject *Sender);
  void __fastcall m_ACT_select_allUpdate(TObject *Sender);
  void __fastcall m_ACT_select_allExecute(TObject *Sender);
  void __fastcall m_ACT_status_barUpdate(TObject *Sender);
  void __fastcall m_BCS_searchBegin(TObject *Sender);
  void __fastcall m_BCS_searchEnd(TObject *Sender);
  void __fastcall m_DBG_listEnter(TObject *Sender);
  void __fastcall m_DBG_listExit(TObject *Sender);
  void __fastcall m_ACT_check_arrivalExecute(TObject *Sender);
  void __fastcall m_ACT_check_arrivalUpdate(TObject *Sender);
  void __fastcall m_ACT_print_labelExecute(TObject *Sender);
  void __fastcall m_ACT_print_labelUpdate(TObject *Sender);
private:
  TDIncome* m_dm;
  enum {NmclFilterOffImgIdx = 1, NmclFilterOnImgIdx = 2};
  TMLGridOptions m_tmp_list_grid_opts, m_tmp_nmcls_grid_opts;
  TTSReportControl m_rep_ch_arrival_ctrl;
  TTSOperationControl m_rtl_print_labels;
public:
  __fastcall TFIncomeList(TComponent* p_owner, TTSDDocument *p_dm)
    : TTSFDocumentList(p_owner,p_dm),
    m_dm(static_cast<TDIncome*>(p_dm)),
    m_rtl_print_labels("TSRtlOperations.RtlPrintLabels"),
    m_rep_ch_arrival_ctrl("RTL_CHECK_ARRIVAL",&DoFormNew)
    {};
};
//---------------------------------------------------------------------------
#endif
