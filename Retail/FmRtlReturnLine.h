//---------------------------------------------------------------------------

#ifndef FmRtlReturnLineH
#define FmRtlReturnLineH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMSUBITEM.h"
#include "DmRtlReturns.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Db.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "MLDBPanel.h"
#include "MLLovView.h"
#include "MLLov.h"
#include <Dialogs.hpp>
#include "DLBCScaner.h"
#include "RXCtrls.hpp"
//---------------------------------------------------------------------------

class TFRtlReturnLine : public TTSFSubItem
{
__published:
  TPanel *Panel1;
  TLabel *Label1;
  TDataSource *m_DS_lines;
  TMLDBGrid *m_DBG_lines;
  TDataSource *m_DS_line;
  TDBEdit *DBEdit1;
  TPanel *Panel2;
  TLabel *Label2;
  TLabel *Label3;
  TMLLovListView *MLLovListView1;
  TMLLovListView *MLLovListView2;
  TMLDBPanel *MLDBPanel1;
  TMLDBPanel *MLDBPanel2;
  TDataSource *m_DS_nmcls;
  TDataSource *m_DS_assorts;
  TMLLov *MLLov1;
  TMLLov *MLLov2;
  TLabel *Label4;
  TDBEdit *DBEdit2;
        TPanel *m_P_lines_control;
        TRxSpeedButton *m1;
        TToolBar *m_TB_lines_control_button;
        TSpeedButton *m_SBTN_delete;
        TMLDBGrid *m_DBG_alco;
        TDLBCScaner *m_BCS_line;
        TDataSource *m_DS_alc;
        TAction *m_ACT_del;
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall m_ACT_delUpdate(TObject *Sender);
        void __fastcall m_ACT_delExecute(TObject *Sender);
        void __fastcall m_BCS_lineBegin(TObject *Sender);
        void __fastcall m_BCS_lineEnd(TObject *Sender);
private:
  TDRtlReturns *m_dm;
  TMLGridOptions  m_temp_grid_options;
public:
  __fastcall TFRtlReturnLine(TComponent* p_owner,
                             TTSDCustom *p_dm_custom): TTSFSubItem(p_owner,
                                                                   p_dm_custom),
                                                       m_dm(static_cast<TDRtlReturns*>(p_dm_custom)) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
