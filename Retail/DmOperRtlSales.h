//---------------------------------------------------------------------------

#ifndef DmOperRtlSalesH
#define DmOperRtlSalesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMOPERATION.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include "TSAccessControl.h"
//---------------------------------------------------------------------------
class TDOperRtlSales : public TTSDOperation
{
__published:	// IDE-managed Components
        TMLQuery *m_Q_checks;
        TFloatField *m_Q_checksCHECK_NUM;
        TDateTimeField *m_Q_checksCHECK_DATE;
        TFloatField *m_Q_checksCHECK_SUM;
        TDateTimeField *m_Q_checksPRINT_DATE;
        TFloatField *m_Q_checksPRINT_USER_ID;
        TStringField *m_Q_checksPRINT_USER_NAME;
        TStringField *m_Q_checksHOST;
        TQuery *m_Q_orgs;
        TFloatField *m_Q_orgsID;
        TStringField *m_Q_orgsNAME;
        TFloatField *m_Q_mainID;
        TDateTimeField *m_Q_mainCOMPLETE;
        TFloatField *m_Q_mainSALE_SUM;
        TFloatField *m_Q_mainBUYER_SUM;
        TFloatField *m_Q_mainCOMPLETE_USER_ID;
        TStringField *m_Q_mainCOMPLETE_USER_NAME;
        TStringField *m_Q_mainHOST;
        TStringField *m_Q_mainCASH;
        TFloatField *m_Q_mainORG_ID;
        TStringField *m_Q_mainORG_NAME;
        TDataSource *m_DS_main;
        TMLQuery *m_Q_item;
        TMLQuery *m_Q_item_checks;
        TMLQuery *m_Q_lines;
        TFloatField *m_Q_linesLINE_ID;
        TFloatField *m_Q_linesNMCL_ID;
        TStringField *m_Q_linesNMCL_NAME;
        TFloatField *m_Q_linesASSORT_ID;
        TStringField *m_Q_linesASSORT_NAME;
        TFloatField *m_Q_linesWEIGHT_CODE;
        TStringField *m_Q_linesBAR_CODE;
        TFloatField *m_Q_linesOUT_PRICE;
        TFloatField *m_Q_linesQUANTITY;
        TFloatField *m_Q_linesDEP_NUMBER;
        TFloatField *m_Q_linesNDS;
        TFloatField *m_Q_linesCHECK_NUMBER;
        TFloatField *m_Q_linesCARD_ID;
        TDataSource *m_DS_item;
        TDateTimeField *m_Q_itemCOMPLETE;
        TFloatField *m_Q_itemSALE_SUM;
        TFloatField *m_Q_itemBUYER_SUM;
        TFloatField *m_Q_itemCOMPLETE_USER_ID;
        TStringField *m_Q_itemCOMPLETE_USER_NAME;
        TStringField *m_Q_itemHOST;
        TFloatField *m_Q_itemID;
        TFloatField *m_Q_item_checksREPORT_DOC_ID;
        TFloatField *m_Q_item_checksCHECK_NUM;
        TDateTimeField *m_Q_item_checksCHECK_DATE;
        TFloatField *m_Q_item_checksCHECK_SUM;
        TDateTimeField *m_Q_item_checksPRINT_DATE;
        TFloatField *m_Q_item_checksPRINT_USER_ID;
        TStringField *m_Q_item_checksPRINT_USER_NAME;
        TStringField *m_Q_item_checksHOST;
        void __fastcall m_Q_mainBeforeOpen(TDataSet *DataSet);
private:	// User declarations
  bool is_tabac;
  int m_home_org_id;
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_value);
public:
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
  __property int OrgId = {read=GetOrgId, write=SetOrgId};
  __property int HomeOrgId = {read=m_home_org_id};
  __property bool IsTabac = {read=is_tabac, write=is_tabac};

public:		// User declarations
    __fastcall TDOperRtlSales(TComponent* p_owner, AnsiString p_prog_id);
    __fastcall ~TDOperRtlSales();
ML_BEGIN_DATA_SETS
  ML_BASE_DATA_SETS(TTSDOperation)
  ML_DATA_SET_ENTRY((*DataSets),m_Q_checks)
ML_END_DATA_SETS

};
//---------------------------------------------------------------------------
#endif
