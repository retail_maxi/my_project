inherited FRTLAssortExcepOrgs: TFRTLAssortExcepOrgs
  Left = 633
  Top = 231
  Width = 741
  Height = 504
  BorderStyle = bsSizeable
  Caption = '�����'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 435
    Width = 725
    TabOrder = 1
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 531
      inherited m_BBTN_close: TBitBtn
        Caption = '�������'
      end
    end
  end
  object m_P_main: TPanel [1]
    Left = 0
    Top = 0
    Width = 725
    Height = 435
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 345
      Top = 0
      Width = 5
      Height = 435
      Cursor = crHSplit
    end
    object m_P_left: TPanel
      Left = 0
      Top = 0
      Width = 345
      Height = 435
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object m_DBG_org: TMLDBGrid
        Left = 0
        Top = 0
        Width = 345
        Height = 435
        Align = alClient
        DataSource = m_DS_org
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'ORG_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'ORG_NAME'
            Title.TitleButton = True
            Width = 250
            Footers = <>
          end>
      end
    end
    object m_P_all: TPanel
      Left = 350
      Top = 0
      Width = 375
      Height = 435
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object m_P_btns: TPanel
        Left = 0
        Top = 0
        Width = 24
        Height = 435
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object m_SBTN_add: TSpeedButton
          Left = 0
          Top = 0
          Width = 23
          Height = 22
          Action = m_ACT_add
          ParentShowHint = False
          ShowHint = True
        end
        object m_SBTN_delete: TSpeedButton
          Left = 0
          Top = 28
          Width = 23
          Height = 22
          Action = m_ACT_delete
          ParentShowHint = False
          ShowHint = True
        end
      end
      object m_PC_all_cnt: TPageControl
        Left = 24
        Top = 0
        Width = 351
        Height = 435
        ActivePage = m_TS_org
        Align = alClient
        TabOrder = 1
        object m_TS_org: TTabSheet
          Caption = '��� �����'
          object m_DBG_all_org: TMLDBGrid
            Left = 0
            Top = 0
            Width = 343
            Height = 407
            Align = alClient
            DataSource = m_DS_all_org
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterColor = clWindow
            AutoFitColWidths = True
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
            Columns = <
              item
                FieldName = 'ID'
                Title.TitleButton = True
                Footers = <>
              end
              item
                FieldName = 'NAME'
                Title.TitleButton = True
                Width = 250
                Footers = <>
              end>
          end
        end
      end
    end
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_close: TAction
      Caption = '�������'
    end
    inherited m_ACT_apply_updates: TAction
      Enabled = False
      Visible = False
    end
    object m_ACT_add: TAction
      Category = 'DocActions'
      Caption = '<'
      Hint = '�������� � ������'
      OnExecute = m_ACT_addExecute
      OnUpdate = m_ACT_addUpdate
    end
    object m_ACT_delete: TAction
      Category = 'DocActions'
      Caption = '>'
      Hint = '��������� �� ������'
      OnExecute = m_ACT_deleteExecute
      OnUpdate = m_ACT_deleteUpdate
    end
  end
  object m_DS_org: TDataSource
    DataSet = m_Q_org
    Left = 64
    Top = 32
  end
  object m_DS_all_org: TDataSource
    DataSet = m_Q_all_org
    Left = 34
    Top = 64
  end
  object m_Q_add_org: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      'INSERT INTO rtl_assort_exception_orgs(assort_excep_id, org_id)'
      'SELECT :ID, to_number(t.value) AS orgl_id'
      '  FROM tmp_report_param_values t'
      ' WHERE t.id = :SQ_ID'
      '    AND NOT EXISTS (SELECT 1'
      '                    FROM rtl_assort_exception_orgs r'
      
        '                   WHERE r.assort_excep_id = :ID AND r.org_id = ' +
        'to_number(t.value)'
      '                  );'
      'end;')
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_delete_org: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  DELETE FROM  rtl_assort_exception_orgs r'
      '  WHERE r.assort_excep_id = :ID'
      
        '    AND r.org_id in (SELECT DISTINCT to_number(t.value) AS org_i' +
        'd'
      '                        FROM tmp_report_param_values t'
      '                       WHERE t.id = :SQ_ID) ; '
      'END;')
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_all_org: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name '
      '  from organizations o'
      
        ' where not exists (select 1 from rtl_assort_exception_orgs ro wh' +
        'ere ro.org_id = o.id and ro.assort_excep_id = :ID)'
      
        '     and (exists (select 1 from groups_organizations gor where g' +
        'or.org_id = o.id and grp_id = :ORG_GROUP_ID) or :ORG_GROUP_ID = ' +
        '-1)'
      '     and org_type_id = 2'
      '     and enable = '#39'Y'#39
      '%WHERE_CLAUSE'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_GROUP_ID'
        ParamType = ptInput
      end>
    object m_Q_all_orgID: TFloatField
      DisplayLabel = 'ID �����'
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_all_orgNAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
  end
  object m_U_org: TMLUpdateSQL
    Left = 32
    Top = 32
  end
  object m_Q_org: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT assort_excep_id, org_id, trim(rpad(o.name, 250)) as org_n' +
        'ame'
      '  FROM rtl_assort_exception_orgs ro,'
      '            organizations o'
      '  WHERE ro.org_id = o.id'
      '    AND ro.assort_excep_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_org
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Top = 32
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_orgORG_ID: TFloatField
      DisplayLabel = 'ID �����'
      FieldName = 'ORG_ID'
      Origin = 'ORG_ID'
    end
    object m_Q_orgORG_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'ORG_NAME'
      Origin = 'trim(rpad(o.name, 250))'
      Size = 250
    end
  end
end
