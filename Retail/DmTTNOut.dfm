inherited DTTNOut: TDTTNOut
  OldCreateOrder = False
  Left = 317
  Top = 275
  Height = 640
  Width = 1121
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      
        'with max_store as (select max(rc.store_qnt) as max_store_qnt, ti' +
        '.doc_id'
      '                   from rtl_ttn_out_items ti,'
      '                        goods_record_cards c,'
      '                        retail_goods_cards rc'
      '                  where c.id = ti.card_id'
      '                    and c.id = rc.card_id'
      '                  group by ti.doc_id)'
      ''
      'select'
      '  d.id,'
      '  d.src_org_id doc_src_org_id,'
      '  o.name doc_src_org_name,'
      '  d.comp_name doc_create_comp_name,'
      '  d.create_date doc_create_date,'
      '  d.author doc_create_user,'
      '  d.status doc_status,'
      
        '  decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �� ���' +
        '���'#39','#39'������'#39') doc_status_name,'
      '  d.status_change_date doc_status_change_date,'
      '  d.fldr_id doc_fldr_id,'
      '  f.name doc_fldr_name,'
      
        '  rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) doc_fldr_ri' +
        'ght,'
      '  d.modify_comp_name doc_modify_comp_name,'
      '  d.modify_user doc_modify_user,'
      '  d.modify_date doc_modify_date,'
      '  t.doc_number,'
      '  t.doc_date,'
      '  d_org.name dest_org_name,'
      '  t.amount_in,'
      '  t.amount_out,'
      '  t.note,'
      '  case'
      '    when (f.id = 2483 and t.dest_org_id = 3)'
      '    then max_store.max_store_qnt'
      '    else null'
      '  end as maximum -- ��� ��������� (������ � 1111589 plada)'
      'from '
      '  documents d,'
      '  folders f,'
      '  rtl_ttn_out t,'
      '  organizations o,'
      '  organizations d_org,'
      '  max_store'
      'where '
      '  f.doc_type_id = :DOC_TYPE_ID'
      '  and f.id = d.fldr_id'
      '  and t.doc_id = d.id'
      
        '  and t.doc_date between to_date(:BEGIN_DATE) and to_date(:END_D' +
        'ATE) + 1 - 1/24/60/60'
      '  and o.id = d.src_org_id'
      '  and d_org.id = t.dest_org_id'
      '  and fnc_mask_folder_right(f.id,'#39'Select'#39') = '#39'1'#39
      
        '  AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_id = :ORG_' +
        'ID))'
      '  and max_store.doc_id(+) = t.doc_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���������'
      DisplayWidth = 10
      FieldName = 'DOC_NUMBER'
      Origin = 't.doc_number'
      Size = 100
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'DOC_DATE'
      Origin = 't.doc_date'
    end
    object m_Q_listDEST_ORG_NAME: TStringField
      DisplayLabel = '��� �����'
      DisplayWidth = 20
      FieldName = 'DEST_ORG_NAME'
      Origin = 'd_org.name'
      Size = 250
    end
    object m_Q_listAMOUNT_IN: TFloatField
      DisplayLabel = '�����|� ������� �����'
      FieldName = 'AMOUNT_IN'
      Origin = 't.amount_in'
      currency = True
    end
    object m_Q_listAMOUNT_OUT: TFloatField
      DisplayLabel = '�����|� ����� ����������'
      FieldName = 'AMOUNT_OUT'
      Origin = 't.amount_out'
      currency = True
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 40
      FieldName = 'NOTE'
      Origin = 't.note'
      Size = 250
    end
    object m_Q_listMAXIMUM: TFloatField
      FieldName = 'MAXIMUM'
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'select'
      '  t.doc_id id,'
      '  d.fldr_id,'
      '  t.dest_org_id,'
      '  o.name dest_org_name,'
      '  t.doc_number,'
      '  t.doc_date,'
      '  t.amount_in,'
      '  t.amount_out,'
      '  t.note'
      'from '
      '  rtl_ttn_out t, '
      '  documents d,'
      '  organizations o'
      'where '
      '  t.doc_id = :ID'
      '  and d.id = t.doc_id'
      '  and d.fldr_id = :FLDR_ID'
      '  and o.id = t.dest_org_id')
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_ID'
        ParamType = ptInput
      end>
    object m_Q_itemFLDR_ID: TFloatField
      FieldName = 'FLDR_ID'
      Origin = 'TSDBMAIN.DOCUMENTS.FLDR_ID'
    end
    object m_Q_itemDEST_ORG_ID: TFloatField
      FieldName = 'DEST_ORG_ID'
      Origin = 'TSDBMAIN.RTL_TTN_OUT.DEST_ORG_ID'
    end
    object m_Q_itemDEST_ORG_NAME: TStringField
      FieldName = 'DEST_ORG_NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Origin = 'TSDBMAIN.RTL_TTN_OUT.DOC_NUMBER'
      Size = 100
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
      Origin = 'TSDBMAIN.RTL_TTN_OUT.DOC_DATE'
    end
    object m_Q_itemAMOUNT_IN: TFloatField
      FieldName = 'AMOUNT_IN'
      Origin = 'TSDBMAIN.RTL_TTN_OUT.AMOUNT_IN'
      currency = True
    end
    object m_Q_itemAMOUNT_OUT: TFloatField
      FieldName = 'AMOUNT_OUT'
      Origin = 'TSDBMAIN.RTL_TTN_OUT.AMOUNT_OUT'
      currency = True
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_TTN_OUT.NOTE'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_ttn_out'
      'set'
      '  dest_org_id = :DEST_ORG_ID,'
      '  doc_date = :DOC_DATE,'
      '  doc_number = :DOC_NUMBER,'
      '  amount_in = :AMOUNT_IN,'
      '  amount_out = :AMOUNT_OUT,'
      '  note = :NOTE'
      'where'
      '  doc_id = :ID')
    InsertSQL.Strings = (
      'insert into rtl_ttn_out '
      
        '  (doc_id, dest_org_id, doc_date, doc_number, amount_in, amount_' +
        'out, note)'
      'values '
      
        '  (:ID, :DEST_ORG_ID, :DOC_DATE, :DOC_NUMBER, :AMOUNT_IN, :AMOUN' +
        'T_OUT, :NOTE)')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select'
      '  id,'
      '  line_id,'
      '  card_id,'
      '  nmcl_id,'
      '  nmcl_name,'
      '  meas_name,'
      '  assortment_id,'
      '  assortment_name,'
      '  items_qnt,'
      '  in_price,'
      '  out_price,'
      '  note,'
      '  store_qnt,'
      '  rtl_name'
      'from('
      'select'
      '  ti.doc_id id,'
      '  ti.line_id line_id,'
      '  ti.card_id,'
      '  c.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  c.assortment_id,'
      '  a.name assortment_name,'
      '  ti.items_qnt,'
      '  ti.in_price,'
      '  ti.out_price,'
      '  ti.note,'
      '  rc.store_qnt,'
      
        '  trim(rpad(pkg_rtl_rs_nmcl_model.fnc_get_nmcl_model_org_name(c.' +
        'nmcl_id, c.assortment_id, c.org_id),250)) as rtl_name'
      'from'
      '  rtl_ttn_out_items ti,'
      '  goods_record_cards c,'
      '  retail_goods_cards rc,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where ti.doc_id = :ID'
      '  and c.id = ti.card_id'
      '  and c.id = rc.card_id'
      '  and ni.id = c.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and a.id = c.assortment_id'
      ' order by ni.prod_type_id,  ni.name'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    inherited m_Q_linesLINE_ID: TFloatField
      Origin = 'line_id'
    end
    object m_Q_linesCARD_ID: TFloatField
      DisplayLabel = 'ID �������� ������'
      FieldName = 'CARD_ID'
      Origin = 'card_id'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'nmcl_id'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      DisplayWidth = 20
      FieldName = 'NMCL_NAME'
      Origin = 'nmcl_name'
      Size = 100
    end
    object m_Q_linesMEAS_NAME: TStringField
      DisplayLabel = '��. ���.'
      DisplayWidth = 20
      FieldName = 'MEAS_NAME'
      Origin = 'meas_name'
      Size = 30
    end
    object m_Q_linesASSORTMENT_ID: TFloatField
      DisplayLabel = '��� ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'assortment_id'
    end
    object m_Q_linesASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 20
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'assortment_name'
      Size = 100
    end
    object m_Q_linesITEMS_QNT: TFloatField
      DisplayLabel = '����������'
      FieldName = 'ITEMS_QNT'
      Origin = 'items_qnt'
    end
    object m_Q_linesIN_PRICE: TFloatField
      DisplayLabel = '����|�������'
      FieldName = 'IN_PRICE'
      Origin = 'in_price'
      currency = True
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '����|����������'
      FieldName = 'OUT_PRICE'
      Origin = 'out_price'
      currency = True
    end
    object m_Q_linesNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 40
      FieldName = 'NOTE'
      Origin = 'note'
      Size = 250
    end
    object m_Q_linesSTORE_QNT: TFloatField
      DisplayLabel = '������� �� ���'
      FieldName = 'STORE_QNT'
      Origin = 'store_qnt'
    end
    object m_Q_linesRTL_NAME: TStringField
      DisplayLabel = '������� �������'
      DisplayWidth = 20
      FieldName = 'RTL_NAME'
      Size = 250
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'select'
      '  ti.doc_id id,'
      '  ti.line_id,'
      '  ti.card_id,'
      '  c.nmcl_id,'
      '  ni.name nmcl_name,'
      '  c.assortment_id,'
      '  a.name assortment_name,'
      '  mu.name mu_name,'
      '  ti.items_qnt,'
      '  pkg_goods_cards.FNC_GET_CARD_REST_CUR(ti.card_id) as cur_rest,'
      '  ti.in_price,'
      '  ti.out_price,'
      '  ti.note'
      'from'
      '  rtl_ttn_out_items ti,'
      '  goods_record_cards c,'
      '  nomenclature_items ni, '
      '  measure_units mu, '
      '  assortment a'
      'where'
      '  ti.doc_id = :ID'
      '  and ti.line_id = :LINE_ID'
      '  and c.id = ti.card_id'
      '  and ni.id = c.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and a.id = c.assortment_id')
    object m_Q_lineCARD_ID: TFloatField
      FieldName = 'CARD_ID'
      Origin = 'TSDBMAIN.RTL_TTN_OUT_ITEMS.CARD_ID'
    end
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.GOODS_RECORD_CARDS.NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_lineASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
      Origin = 'TSDBMAIN.GOODS_RECORD_CARDS.ASSORTMENT_ID'
    end
    object m_Q_lineASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
    object m_Q_lineMU_NAME: TStringField
      FieldName = 'MU_NAME'
      Origin = 'TSDBMAIN.MEASURE_UNITS.NAME'
      Size = 30
    end
    object m_Q_lineITEMS_QNT: TFloatField
      FieldName = 'ITEMS_QNT'
      Origin = 'TSDBMAIN.RTL_TTN_OUT_ITEMS.ITEMS_QNT'
    end
    object m_Q_lineIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      Origin = 'TSDBMAIN.RTL_TTN_OUT_ITEMS.IN_PRICE'
    end
    object m_Q_lineOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
      Origin = 'TSDBMAIN.RTL_TTN_OUT_ITEMS.OUT_PRICE'
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_TTN_OUT_ITEMS.NOTE'
      Size = 250
    end
    object m_Q_lineCUR_REST: TFloatField
      FieldName = 'CUR_REST'
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_ttn_out_items '
      'set '
      ' card_id = :CARD_ID,'
      ' items_qnt = :ITEMS_QNT,'
      ' in_price = :IN_PRICE,'
      ' out_price = :OUT_PRICE,'
      ' note = :NOTE'
      'where doc_id = :ID and line_id = :LINE_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_ttn_out_items (doc_id, line_id, card_id, items_q' +
        'nt, in_price, out_price, note)'
      
        'values (:ID, :LINE_ID, :CARD_ID, :ITEMS_QNT, :IN_PRICE, :OUT_PRI' +
        'CE, :NOTE)')
    DeleteSQL.Strings = (
      'delete from rtl_ttn_out_items'
      'where doc_id = :OLD_ID and line_id = :OLD_LINE_ID')
  end
  object m_Q_orgs: TMLQuery
    BeforeOpen = m_Q_orgsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      ' SELECT o.id AS id,'
      '       o.name AS name'
      'FROM organizations o'
      'WHERE o.id <> :HOME_ORG_ID'
      '  AND o.enable = '#39'Y'#39
      '  and (o.org_type_id = 2 or o.id = 3 or o.id = 34)'
      '%WHERE_CLAUSE'
      'ORDER BY o.name ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 40
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptUnknown
      end>
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'o.id'
    end
    object m_Q_orgsNAME: TStringField
      DisplayLabel = '��������'
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = 'o.name'
      Size = 250
    end
  end
  object m_Q_get_in_price: TQuery
    BeforeOpen = m_Q_get_in_priceBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select pkg_rtl_prices.fnc_cur_in_price(:NMCL_ID,:ASSORTMENT_ID) ' +
        'val from dual')
    Left = 136
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end>
    object m_Q_get_in_priceVAL: TFloatField
      FieldName = 'VAL'
    end
  end
  object m_Q_barcode_info: TMLQuery
    BeforeOpen = m_Q_barcode_infoBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '
      '  bci.nmcl_id,'
      '  bci.assort_id,'
      '  bci.items_qnt,'
      '  bci.error,'
      '  bci.card_id,'
      
        '  PKG_RTL_PRICES.fnc_cur_out_price(bci.nmcl_id, bci.assort_id, :' +
        'ORG_ID) as out_price,'
      
        '  PKG_RTL_PRICES.fnc_cur_in_price(bci.nmcl_id, bci.assort_id, :O' +
        'RG_ID) as in_price'
      
        'FROM table(pkg_bar_code.fnc_tbl_nmcl_for_bar_code_rtl(:BARCODE))' +
        ' bci')
    Macros = <>
    Left = 240
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'BARCODE'
        ParamType = ptInput
      end>
    object m_Q_barcode_infoNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_barcode_infoASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
    end
    object m_Q_barcode_infoITEMS_QNT: TFloatField
      FieldName = 'ITEMS_QNT'
    end
    object m_Q_barcode_infoERROR: TStringField
      FieldName = 'ERROR'
      Size = 255
    end
    object m_Q_barcode_infoCARD_ID: TFloatField
      FieldName = 'CARD_ID'
    end
    object m_Q_barcode_infoOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
    end
    object m_Q_barcode_infoIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
    end
  end
  object m_Q_upins_line: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '    MERGE INTO'
      '      rtl_ttn_out_items dst'
      '    using'
      '      ('
      '        SELECT'
      '          :ID as id,'
      '          :LINE_ID as line_id,'
      '          :CARD_ID as card_id,'
      '          :ITEMS_QNT as items_qnt,'
      '          :IN_PRICE as in_price,'
      '          :OUT_PRICE as out_price,'
      '          :NOTE as note'
      '        FROM dual'
      '      ) src'
      '    on ('
      '         (dst.doc_id, dst.line_id) '
      '             = ( SELECT doc_id, min(line_id) '
      '                 FROM rtl_ttn_out_items ii'
      '                 WHERE ii.doc_id = src.ID '
      '                   AND  ii.card_id = src.CARD_ID'
      '                 GROUP BY ii.doc_id'
      '               )'
      '    )'
      '    when matched then'
      '      UPDATE SET '
      '        dst.items_qnt = NVL(dst.items_qnt,0) + src.ITEMS_QNT'
      '    when not matched then'
      
        '      INSERT (dst.DOC_ID, dst.LINE_ID, dst.ITEMS_QNT, dst.IN_PRI' +
        'CE, dst.OUT_PRICE, dst.card_id)'
      
        '      VALUES (src.ID, src.LINE_ID, src.ITEMS_QNT, src.IN_PRICE, ' +
        'src.OUT_PRICE, src.card_id);'
      'END;')
    Macros = <>
    Left = 344
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CARD_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'ITEMS_QNT'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'IN_PRICE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'OUT_PRICE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NOTE'
        ParamType = ptInput
      end>
  end
  object m_Q_orgs2: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 552
    Top = 240
    object m_Q_orgs2ID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgs2NAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
end
