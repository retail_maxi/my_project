//---------------------------------------------------------------------------

#ifndef FmMessageToRKUListH
#define FmMessageToRKUListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENTLIST.h"
#include "DmMessageToRKU.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
//---------------------------------------------------------------------------
class TFMessageToRKUList : public TTSFDocumentList
{
__published:	// IDE-managed Components
private:	// User declarations
    TDMessageToRKU *m_dm;
public:		// User declarations
    __fastcall TFMessageToRKUList(TComponent* p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------
#endif
