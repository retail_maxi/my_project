//---------------------------------------------------------------------------

#ifndef FmReplaceDocNumH
#define FmReplaceDocNumH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmBill.h"
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
#include <ToolWin.hpp>
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
//---------------------------------------------------------------------------
class TFReplaseDocNum : public TTSFDialog
{
__published:	// IDE-managed Components
        TLabel *m_L_buh_doc_number;
        TDBEdit *m_DBE_TTN_NUMBER;
        TLabel *Label1;
        TDataSource *m_DS_item;
        TDBDateEdit *m_DBDE_TTN_DATE;
private:	// User declarations
  TDBill *m_dm;
public:		// User declarations
        __fastcall TFReplaseDocNum::TFReplaseDocNum(TComponent* p_owner, TDBill *p_dm);
};
//---------------------------------------------------------------------------
//extern PACKAGE TFReplaseDocNum *FReplaseDocNum;
//---------------------------------------------------------------------------
#endif
