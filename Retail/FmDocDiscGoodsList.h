//---------------------------------------------------------------------------

#ifndef FmDocDiscGoodsListH
#define FmDocDiscGoodsListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmDocDiscGoods.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTLIST.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include "XLSXConverterFrm.h"
#include "XLSXConverter.h"
#include "FileLogon.h"
#include "MLLov.h"
#include "MLLovView.h"
#include "RXCtrls.hpp"
//---------------------------------------------------------------------------
class TFDocDiscGoodsList : public TTSFDocumentList
{
__published:	// IDE-managed Components
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TToolButton *ToolButton1;
  TSpeedButton *m_SBTN_set_date;
        TAction *m_ACT_upload_file;
        TAction *m_ACT_upload_hst;
        TAction *m_ACT_prod;
        TAction *m_ACT_rev;
        TAction *m_ACT_rtt;
        TAction *m_ACT_change_start;
        TAction *m_ACT_type_act;
        TAction *m_ACT_rep_dtn;
	TAction *m_ACT_hst;
        TAction *m_ACT_del_rtt;
        TAction *m_ACT_replace_prices;
        TPanel *m_P_1;
        TSpeedButton *m_SB_group_process;
        TAction *m_ACT_group_process;
        TSpeedButton *m_SB_group_process1;
        TAction *m_ACT_copy;
        TPanel *m_P_orgs_list;
        TMLDBGrid *m_DBG_orgs_list;
        TDataSource *m_DS_orgs_list;
        TSpeedButton *m_SB_view_nmcl_sales;
        TAction *m_ACT_view_nmcl_sales;
        TSplitter *m_SP_1;
        TToolBar *m_TB_next;
        TRxSpeedButton *m_RSB_fldr_next;
        TToolBar *m_TB_prev;
        TRxSpeedButton *m_RSB_fldr_prev;
        TPopupMenu *m_PM_fldr_prev;
        TPopupMenu *m_PM_fldr_next;
        TPanel *m_P_is_action;
        TToolButton *ToolButton2;
        TPanel *m_P_aqua;
        TLabel *m_L_aqua;
        TToolButton *ToolButton3;
        TPanel *m_P_del;
        TLabel *m_L_red;
        TPanel *m_P_red;
        TToolButton *ToolButton4;
        TPanel *m_P_return;
        TLabel *m_L_yellow;
        TPanel *m_P_yellow;
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_group_processExecute(TObject *Sender);
        void __fastcall m_ACT_group_processUpdate(TObject *Sender);
        void __fastcall m_ACT_copyUpdate(TObject *Sender);
        void __fastcall m_ACT_copyExecute(TObject *Sender);
        void __fastcall m_DBG_listGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
        void __fastcall m_ACT_view_nmcl_salesExecute(TObject *Sender);
        void __fastcall m_ACT_view_nmcl_salesUpdate(TObject *Sender);
        void __fastcall m_ACT_deleteExecute(TObject *Sender);
        void __fastcall m_PM_fldr_prevPopup(TObject *Sender);
        void __fastcall m_PM_fldr_nextPopup(TObject *Sender);

private:	// User declarations
  TDDocDiscGoods *m_dm;
  void __fastcall ShowFldrMenu();
  void __fastcall ShowFldrMenuPrev();
  void __fastcall PMFldrNextClick(TObject *Sender);
  int __fastcall getRouteId(int p_src_fldr_id, int p_dest_fldr_id);
public:
     TTSOperationControl m_view_nmcl_sales; 
public:		// User declarations
  __fastcall TFDocDiscGoodsList(TComponent* p_owner,
                               TTSDDocument *p_dm_document): TTSFDocumentList(p_owner,
                                                                            p_dm_document),
                               m_view_nmcl_sales("TSRtlOperations.ViewNmclSales"),
                               m_dm(static_cast<TDDocDiscGoods*>(p_dm_document)) {};
};
//---------------------------------------------------------------------------
#endif
