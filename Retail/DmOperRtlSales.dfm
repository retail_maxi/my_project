inherited DOperRtlSales: TDOperRtlSales
  OldCreateOrder = False
  Left = 587
  Top = 305
  Height = 599
  Width = 1082
  inherited m_DB_main: TDatabase
    AliasName = 'TOPT1'
    Params.Strings = (
      'USER NAME=ROBOT'
      'PASSWORD=RBT')
  end
  inherited m_Q_main: TMLQuery
    BeforeOpen = m_Q_mainBeforeOpen
    SQL.Strings = (
      'select * from ('
      'SELECT '
      '       s.doc_id AS id,'
      '       s.org_id,'
      '       o.name AS org_name,'
      '       s.complete AS complete,'
      '       s.out_sale_sum AS sale_sum,'
      '       s.buyer_sum AS buyer_sum,'
      '       s.complete_user_id AS complete_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(s.complete_user_id), ' +
        '250) AS complete_user_name,'
      '       s.host AS host,'
      
        '       (select max(decode(cash, '#39'Y'#39', '#39'���'#39', '#39'������'#39')) from rtl_' +
        'checks where sale_doc_id = s.doc_id) as cash'
      ' FROM organizations o,'
      '      rtl_sales s'
      'WHERE o.id = s.org_id'
      '  AND s.complete BETWEEN :BEGIN_DATE AND :END_DATE'
      '  AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND s.org_id = :ORG_ID))'
      '%WHERE_TABAC)'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      '')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_TABAC'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 32
    Top = 184
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_mainID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_mainORG_ID: TFloatField
      DisplayLabel = 'ID ���'
      FieldName = 'ORG_ID'
      Origin = 'ORG_ID'
    end
    object m_Q_mainORG_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'ORG_NAME'
      Origin = 'ORG_NAME'
      Size = 250
    end
    object m_Q_mainCOMPLETE: TDateTimeField
      DisplayLabel = '���� �������� �� ���'
      FieldName = 'COMPLETE'
      Origin = 'COMPLETE'
    end
    object m_Q_mainSALE_SUM: TFloatField
      DisplayLabel = '�����'
      FieldName = 'SALE_SUM'
      Origin = 'SALE_SUM'
      currency = True
    end
    object m_Q_mainBUYER_SUM: TFloatField
      DisplayLabel = '������� �����������'
      FieldName = 'BUYER_SUM'
      Origin = 'BUYER_SUM'
      currency = True
    end
    object m_Q_mainCOMPLETE_USER_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'COMPLETE_USER_ID'
      Origin = 'COMPLETE_USER_ID'
    end
    object m_Q_mainCOMPLETE_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'COMPLETE_USER_NAME'
      Origin = 'COMPLETE_USER_NAME'
      Size = 250
    end
    object m_Q_mainHOST: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
    object m_Q_mainCASH: TStringField
      DisplayLabel = '��� �������'
      FieldName = 'CASH'
      Origin = 'CASH'
      Size = 6
    end
  end
  object m_Q_checks: TMLQuery
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_main
    SQL.Strings = (
      'SELECT check_num AS check_num,'
      '       check_date AS check_date,'
      '       check_sum AS check_sum,'
      '       print_date AS print_date,'
      '       print_user_id AS print_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250) ' +
        'AS print_user_name,'
      '       host AS host'
      'FROM rtl_checks'
      'WHERE sale_doc_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 32
    Top = 249
    ParamData = <
      item
        DataType = ftFloat
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_checksCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'CHECK_NUM'
    end
    object m_Q_checksCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'CHECK_DATE'
    end
    object m_Q_checksCHECK_SUM: TFloatField
      DisplayLabel = '�����'
      FieldName = 'CHECK_SUM'
      Origin = 'CHECK_SUM'
      currency = True
    end
    object m_Q_checksPRINT_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'PRINT_DATE'
      Origin = 'PRINT_DATE'
    end
    object m_Q_checksPRINT_USER_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'PRINT_USER_ID'
      Origin = 'PRINT_USER_ID'
    end
    object m_Q_checksPRINT_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'PRINT_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250)'
      Size = 250
    end
    object m_Q_checksHOST: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 256
    Top = 128
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Size = 250
    end
  end
  object m_DS_main: TDataSource
    DataSet = m_Q_main
    Left = 368
    Top = 128
  end
  object m_Q_item: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT doc_id AS id,'
      '       complete AS complete,'
      '       out_sale_sum AS sale_sum,'
      '       buyer_sum AS buyer_sum,'
      '       complete_user_id AS complete_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(complete_user_id), 25' +
        '0) AS complete_user_name,'
      '       host AS host'
      'FROM rtl_sales s'
      'WHERE doc_id = :ID'
      ' '
      ' ')
    Macros = <>
    Left = 112
    Top = 185
    ParamData = <
      item
        DataType = ftFloat
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_itemCOMPLETE: TDateTimeField
      FieldName = 'COMPLETE'
    end
    object m_Q_itemSALE_SUM: TFloatField
      FieldName = 'SALE_SUM'
      currency = True
    end
    object m_Q_itemBUYER_SUM: TFloatField
      FieldName = 'BUYER_SUM'
      currency = True
    end
    object m_Q_itemCOMPLETE_USER_ID: TFloatField
      FieldName = 'COMPLETE_USER_ID'
    end
    object m_Q_itemCOMPLETE_USER_NAME: TStringField
      FieldName = 'COMPLETE_USER_NAME'
      Size = 250
    end
    object m_Q_itemHOST: TStringField
      FieldName = 'HOST'
      Size = 64
    end
    object m_Q_itemID: TFloatField
      FieldName = 'ID'
    end
  end
  object m_Q_item_checks: TMLQuery
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_item
    SQL.Strings = (
      'SELECT report_doc_id AS report_doc_id,'
      '       check_num AS check_num,'
      '       check_date AS check_date,'
      '       check_sum AS check_sum,'
      '       print_date AS print_date,'
      '       print_user_id AS print_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250) ' +
        'AS print_user_name,'
      '       host AS host'
      'FROM rtl_checks'
      'WHERE sale_doc_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 212
    Top = 192
    ParamData = <
      item
        DataType = ftFloat
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_item_checksREPORT_DOC_ID: TFloatField
      FieldName = 'REPORT_DOC_ID'
    end
    object m_Q_item_checksCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'CHECK_NUM'
    end
    object m_Q_item_checksCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'CHECK_DATE'
    end
    object m_Q_item_checksCHECK_SUM: TFloatField
      DisplayLabel = '�����'
      FieldName = 'CHECK_SUM'
      Origin = 'CHECK_SUM'
      currency = True
    end
    object m_Q_item_checksPRINT_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'PRINT_DATE'
      Origin = 'PRINT_DATE'
    end
    object m_Q_item_checksPRINT_USER_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'PRINT_USER_ID'
      Origin = 'PRINT_USER_ID'
    end
    object m_Q_item_checksPRINT_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'PRINT_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250)'
      Size = 250
    end
    object m_Q_item_checksHOST: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
  end
  object m_Q_lines: TMLQuery
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_item
    SQL.Strings = (
      'SELECT rsl.line_id AS line_id,'
      '       rsl.nmcl_id AS nmcl_id,'
      '       ni.name AS nmcl_name,'
      '       rsl.assort_id AS assort_id,'
      '       a.name AS assort_name,'
      '       rsl.weight_code AS weight_code,'
      '       rsl.bar_code AS bar_code,'
      '       rsl.out_price AS out_price,'
      '       rsl.quantity AS quantity,'
      '       sd.dep_number AS dep_number,'
      '       rsl.nds AS nds,'
      '       rsl.check_num AS check_number,'
      '       rsl.card_id AS card_id'
      'FROM rtl_sale_lines rsl,'
      '     nomenclature_items ni,'
      '     assortment a,'
      '     shop_departments sd'
      'WHERE rsl.doc_id = :ID'
      '  AND ni.id = rsl.nmcl_id'
      '  AND a.id = rsl.assort_id'
      '  AND sd.id = rsl.shop_dep_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 120
    Top = 240
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_linesLINE_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'LINE_ID'
      Origin = 'rsl.line_id'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'NMCL_ID'
      Origin = 'rsl.nmcl_id'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_linesASSORT_ID: TFloatField
      DisplayLabel = 'ID ������.'
      FieldName = 'ASSORT_ID'
      Origin = 'rsl.assort_id'
      Visible = False
    end
    object m_Q_linesASSORT_NAME: TStringField
      DisplayLabel = '������.'
      FieldName = 'ASSORT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_linesWEIGHT_CODE: TFloatField
      DisplayLabel = '������ ����'
      FieldName = 'WEIGHT_CODE'
      Origin = 'rsl.weight_code'
    end
    object m_Q_linesBAR_CODE: TStringField
      DisplayLabel = '�����-���'
      FieldName = 'BAR_CODE'
      Origin = 'rsl.bar_code'
      Size = 30
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'rsl.out_price'
      currency = True
    end
    object m_Q_linesQUANTITY: TFloatField
      DisplayLabel = '���.'
      FieldName = 'QUANTITY'
      Origin = 'rsl.quantity'
    end
    object m_Q_linesDEP_NUMBER: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'DEP_NUMBER'
      Origin = 'sd.dep_number'
      Visible = False
    end
    object m_Q_linesNDS: TFloatField
      DisplayLabel = '���'
      FieldName = 'NDS'
      Origin = 'rsl.nds'
      Visible = False
    end
    object m_Q_linesCHECK_NUMBER: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUMBER'
      Origin = 'rsl.check_number'
      Visible = False
    end
    object m_Q_linesCARD_ID: TFloatField
      DisplayLabel = 'ID ��������'
      FieldName = 'CARD_ID'
      Origin = 'rsl.card_id'
      Visible = False
    end
  end
  object m_DS_item: TDataSource
    DataSet = m_Q_item
    Left = 440
    Top = 128
  end
end
