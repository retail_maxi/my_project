//---------------------------------------------------------------------------

#ifndef FmMessageFromRKUListH
#define FmMessageFromRKUListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENTLIST.h"
#include "DmMessageFromRKU.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
//---------------------------------------------------------------------------
class TFMessageFromRKUList : public TTSFDocumentList
{
__published:	// IDE-managed Components
private:	// User declarations
    TDMessageFromRKU *m_dm;
public:		// User declarations
    __fastcall TFMessageFromRKUList(TComponent* p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------
#endif
