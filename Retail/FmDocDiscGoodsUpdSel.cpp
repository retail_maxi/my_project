//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocDiscGoodsUpdSel.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFDocDiscGoodsUpdSel::TFDocDiscGoodsUpdSel(TComponent* p_owner,
                                            TDDocDiscGoods *p_dm_doc_with_ln,
                                            bool ch):
                                TTSFSubItem(p_owner,p_dm_doc_with_ln),
                                            m_dm(p_dm_doc_with_ln)
{
  m_dm->ReDefineDataSources(this);

  m_start_win_control = ActiveControl;

  FPressOK = false;
  if (ch) m_dm->m_Q_sel_org->ParamByName("ch")->AsInteger = 2;
  m_ACT_sel_all->Visible = ch;
}
//---------------------------------------------------------------------------

bool __fastcall TFDocDiscGoodsUpdSel::GetPressOK()
{
  return FPressOK;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsUpdSel::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  if(m_SBTN_save->CanFocus()) m_SBTN_save->SetFocus();
  BeforeApply(Sender);
  m_dm->m_Q_fikt_upd->Post();
  FPressOK = true;
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsUpdSel::m_DBG_listKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (Key == 32)
  {
    FillNodesCheck(m_dm->m_Q_sel_org->FieldByName("ID")->AsInteger, m_dm->m_Q_sel_org->FieldByName("CHECKED")->AsInteger == 2);
  }
}
//---------------------------------------------------------------------------

bool __fastcall TFDocDiscGoodsUpdSel::FillNodesCheck(int node_id, bool m_check)
{
  m_dm->m_Q_sel_org->DisableControls();
  try
  {
    if (m_check)
    {
      m_dm->m_Q_sel_org->Edit();
      m_dm->m_Q_sel_org->FieldByName("CHECKED")->AsInteger = 1;
      m_dm->m_Q_sel_org->Post();
      m_dm->m_Q_sel_org->Next();
    }
    else
    {
      m_dm->m_Q_sel_org->Edit();
      m_dm->m_Q_sel_org->FieldByName("CHECKED")->AsInteger = 2;
      m_dm->m_Q_sel_org->Post();
      m_dm->m_Q_sel_org->Next();
    }
      m_dm->m_Q_sel_org->Locate(m_dm->m_Q_sel_org->FieldByName("ID")->FieldName,
                               node_id, TLocateOptions());

  }
  __finally
  {
    m_dm->m_Q_sel_org->EnableControls();
  }
  return true;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsUpdSel::m_ACT_sel_allExecute(
      TObject *Sender)
{
    m_dm->m_Q_sel_org->Close();
    m_dm->m_Q_sel_org->ParamByName("ch")->AsInteger = m_dm->m_Q_sel_org->ParamByName("ch")->AsInteger == 2 ? 1:2;
    m_dm->m_Q_sel_org->Open();
}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsUpdSel::BeforeApply(TObject *Sender)
{
   TWinControl *focused = NULL;
   AnsiString msg = BeforeUpdSelApply(&focused);
   if( !msg.IsEmpty() )
   {
     if( focused ) focused->SetFocus();
     throw ETSError(msg);
   }
}
//---------------------------------------------------------------------------

AnsiString __fastcall TFDocDiscGoodsUpdSel::BeforeUpdSelApply(TWinControl **p_focused)
{
 
   if ( m_dm->m_Q_fikt_updACT_PRICE->IsNull)
  {
    m_DBE_act_price->SetFocus();
    return "������� ���� �� �����!";
  }

  if ( m_dm->m_Q_fikt_updPRICE_BEF_ACT->IsNull)
  {
    m_DBE_price_bef_act->SetFocus();
    return "������� ���� �� �����!";
  }

  if ( m_dm->m_Q_fikt_updBEGIN_DATE->IsNull || m_dm->m_Q_fikt_updEND_DATE->IsNull)
  {
    m_DBDE_begin->SetFocus();
    return "������� ������ �������� �����!";
  }


  if( !Warning.IsEmpty() ) TSExecMessageDlg(this,tmWarning,Warning);

  return "";
}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsUpdSel::FormShow(TObject *Sender)
{
    //FormShow(Sender);
    m_DBCB_urgent_reprice->Checked = 0;
}
//---------------------------------------------------------------------------

