//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvSheetItem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTWITHLINES"
#pragma link "MLDBPanel"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

AnsiString __fastcall TFInvSheetItem::BeforeItemApply(TWinControl **p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete) return "";

  return "";
}
//---------------------------------------------------------------------------


void __fastcall TFInvSheetItem::m_BCS_itemBegin(TObject *Sender)
{
  m_temp_grid_options = m_DBG_lines->OptionsML;
  m_DBG_lines->OptionsML = TMLGridOptions() << goWithoutFind;
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetItem::m_BCS_itemEnd(TObject *Sender)
{
  try
  {
    m_dm->m_Q_barcode_info->Close();
    m_dm->m_Q_barcode_info->ParamByName("BARCODE")->AsString = m_BCS_item->GetResult();
    m_dm->m_Q_barcode_info->Open();
    m_ACT_append->Execute();
  }
  __finally
  {
    m_DBG_lines->OptionsML = m_temp_grid_options;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetItem::FormShow(TObject *Sender)
{
  TTSFDocumentWithLines::FormShow(Sender);

  if (m_dm->Params->Values["LINE_ID"]) PostMessage(Handle,WM_CLOSE,NULL,NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetItem::m_ACT_fill_linesExecute(TObject *Sender)
{
   if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();

   if (MessageDlg(
        "��������� ������ ��������� �� ���� �" + m_dm->m_Q_itemACT_NUMBER->AsString +"?",
        mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0
      ) == mrYes)
   {
   m_dm->m_Q_fill_lines->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
   m_dm->m_Q_fill_lines->ExecSQL();
   m_dm->RefreshItemDataSets();
   }
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetItem::m_ACT_fill_linesUpdate(TObject *Sender)
{
   m_ACT_fill_lines->Enabled = m_dm->UpdateRegimeItem != urView &&
                               m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr &&
                               !m_dm->m_Q_itemINV_DOC_ID->IsNull;
}
//---------------------------------------------------------------------------


void __fastcall TFInvSheetItem::m_ACT_del_dublExecute(TObject *Sender)
{
  //
  if (MessageDlg(
        "������� ����� �������� �����?",
        mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0
      ) == mrYes)
  {
     m_dm->m_Q_del_dubl_bar_code->ParamByName("inv_sheet_doc_id")->Value = m_dm->m_Q_itemID->Value;
     m_dm->m_Q_del_dubl_bar_code->ExecSQL();
     m_dm->RefreshItemDataSets();
     ShowMessage("����� �������� ����� �������.");
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvSheetItem::m_ACT_del_dublUpdate(TObject *Sender)
{
  //
  m_ACT_del_dubl->Enabled = m_dm->UpdateRegimeItem != urView &&
                               m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr &&
                               !m_dm->m_Q_itemINV_DOC_ID->IsNull;
}
//---------------------------------------------------------------------------

