//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmAlcoJournal.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDAlcoJournal::TDAlcoJournal(TComponent* p_owner, AnsiString p_prog_id):
    TTSDOperation(p_owner, p_prog_id),
    m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
  m_gen = 0;
  m_Q_taxpayer_fict->Open();
  SetBeginEndDate(int(GetSysDate()) - 14, int(GetSysDate()));
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDAlcoJournal::GetBeginDate()
{
  return m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDAlcoJournal::GetEndDate()
{
  return m_Q_main->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDAlcoJournal::SetBeginEndDate(const TDateTime &p_begin_date,
                                                const TDateTime &p_end_date)
{
    if ((int(m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
            (int(m_Q_main->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)))
        return;

    m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
    m_Q_main->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

    if (m_Q_main->Active)
        RefreshDataSets();
}
//---------------------------------------------------------------------------

__fastcall TDAlcoJournal::~TDAlcoJournal()
{
  if(m_Q_taxpayer_fict->Active) m_Q_taxpayer_fict->Close();
}
//---------------------------------------------------------------------------
void __fastcall TDAlcoJournal::m_Q_mainBeforeOpen(TDataSet *DataSet)
{
  /*заполнение сессионной таблицы*/
  m_SP_fill_tmp_alco_class_item_v->ParamByName("P_BEGIN_DATE")->AsDateTime = m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime;
  m_SP_fill_tmp_alco_class_item_v->ParamByName("P_END_DATE")->AsDateTime = m_Q_main->ParamByName("END_DATE")->AsDateTime;
  m_SP_fill_tmp_alco_class_item_v->ExecProc();
  m_Q_main->ParamByName("taxpayer_id")->AsInteger = m_Q_taxpayer_fictID->AsInteger;
  m_Q_main->ParamByName("org_id")->AsInteger = m_home_org_id;
  m_Q_main->ParamByName("gen")->AsInteger = m_gen;
  m_gen = 0;
}
//---------------------------------------------------------------------------

