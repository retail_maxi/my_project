//---------------------------------------------------------------------------

#ifndef FmInvActItemH
#define FmInvActItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmInvAct.h"
#include "FmInvActLine.h"
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "RXCtrls.hpp"
#include <Dialogs.hpp>
#include "MLCustomContainer.h"
#include "TSFormContainer.h"
//---------------------------------------------------------------------------
class TFInvActItem : public TTSFDocumentWithLines
{
__published:
  TLabel *m_L_doc_number;
  TMLDBPanel *m_P_doc_number;
  TLabel *m_L_doc_date;
  TMLDBPanel *m_P_doc_date;
  TLabel *m_L_start_date;
  TMLDBPanel *m_P_start_date;
  TSpeedButton *m_SBTN_freeze;
  TLabel *m_L_note;
  TDBEdit *m_DBE_note;
  TAction *m_ACT_freeze;
  TDataSource *m_DS_gr_deps;
  TMLLov *m_LOV_gr_dep;
  TLabel *m_L_gr_dep;
  TMLLovListView *m_LV_gr_dep;
        TCheckBox *m_CB_show_nonzero;
  TToolButton *ToolButton1;
  TSpeedButton *m_SBTN_load_sheet;
  TAction *m_ACT_load_sheets;
  TAction *m_ACT_zero_fact;
  TSpeedButton *m_SBTN_zero_fact;
        TAction *m_ACT_mark_to_recalc;
        TToolBar *m_TB_dop_fldr;
        TRxSpeedButton *m_RSB_fldr_next;
        TPopupMenu *m_PM_fldr_next;
        TLabel *Label2;
        TDBEdit *m_DBE_max_err;
        TSpeedButton *m_SBTN_counters;
        TAction *m_ACT_cnt_link;
        TRxSpeedButton *RxSpeedButton1;
        TRxSpeedButton *m_RSB_mark_to_recalc;
        TPopupMenu *m_PM_mark_to_recalc;
        TMenuItem *N1;
        TMenuItem *N2;
        TAction *m_ACT_clear_recalc;
        TSpeedButton *m_SBT_recalc;
        TAction *m_ACT_recalc;
        TDBEdit *m_DBE_NOTE_NON_PREPARE;
        TLabel *m_L_1;
        TPopupMenu *m_PM_change_prepare;
        TAction *m_ACT_change_prepare;
        TMenuItem *N3;
        TSpeedButton *SpeedButton1;
        TAction *m_ACT_non_prepare;
        TAction *m_ACT_hst;
        TTSFormContainer *m_FC_card_history;
        TSpeedButton *m_SBTN_hst;
        TSpeedButton *SpeedButton2;
        TAction *m_ACT_anal;
        TLabel *m_L1;
        TDBEdit *m_DBE_MAX_ERR_A;
        TSpeedButton *SpeedButton3;
        TAction *m_ACT_auditor_report;
        TCheckBox *m_CB_show_nulls;
        TMLDBPanel *m_DBPL_recalc_percent;
        TLabel *Label3;
        TAction *m_ACT_freeze_with_sales;
        TSpeedButton *m_SBTN_freeze_with_sales;
        TLabel *Label1;
        TMLLovListView *m_LV_reason;
        TDataSource *m_DS_reason;
        TMLLov *m_LOV_reason;
        TLabel *m_L_doc_num_recalc;
        TMLLovListView *m_LV_num_acts;
        TDataSource *m_DS_num_acts;
        TMLLov *m_LOV_num_acts;
  void __fastcall m_ACT_freezeUpdate(TObject *Sender);
  void __fastcall m_ACT_freezeExecute(TObject *Sender);
  void __fastcall m_ACT_appendUpdate(TObject *Sender);
  void __fastcall m_CB_show_nullsClick(TObject *Sender);
  void __fastcall m_ACT_load_sheetsUpdate(TObject *Sender);
  void __fastcall m_ACT_load_sheetsExecute(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_ACT_zero_factUpdate(TObject *Sender);
  void __fastcall m_ACT_zero_factExecute(TObject *Sender);
        void __fastcall m_ACT_mark_to_recalcUpdate(TObject *Sender);
        void __fastcall m_ACT_mark_to_recalcExecute(TObject *Sender);
        void __fastcall m_ACT_cnt_linkExecute(TObject *Sender);
        void __fastcall m_ACT_clear_recalcExecute(TObject *Sender);
        void __fastcall m_ACT_clear_recalcUpdate(TObject *Sender);
        void __fastcall m_DBG_linesGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
        void __fastcall m_ACT_recalcExecute(TObject *Sender);
        void __fastcall m_ACT_recalcUpdate(TObject *Sender);
        void __fastcall m_ACT_change_prepareExecute(TObject *Sender);
        void __fastcall m_DBG_linesMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall m_ACT_non_prepareUpdate(TObject *Sender);
        void __fastcall m_ACT_non_prepareExecute(TObject *Sender);
        void __fastcall m_ACT_hstUpdate(TObject *Sender);
        void __fastcall m_ACT_hstExecute(TObject *Sender);
        void __fastcall m_ACT_analUpdate(TObject *Sender);
        void __fastcall m_ACT_analExecute(TObject *Sender);
        void __fastcall m_ACT_auditor_reportExecute(TObject *Sender);
        void __fastcall m_ACT_auditor_reportUpdate(TObject *Sender);
        void __fastcall m_CB_show_nonzeroClick(TObject *Sender);
        void __fastcall m_ACT_deleteExecute(TObject *Sender);
        void __fastcall m_ACT_freeze_with_salesExecute(TObject *Sender);
        void __fastcall m_ACT_freeze_with_salesUpdate(TObject *Sender);
        void __fastcall m_LOV_gr_depBeforeApply(TObject *Sender);
        void __fastcall m_LOV_reasonAfterApply(TObject *Sender);
        void __fastcall m_LOV_reasonAfterClear(TObject *Sender);
private:
  void __fastcall PMFldrNextClick(TObject *Sender);
  TDInvAct* m_dm;
  TTSOperationControl m_view_rep_act_ctrl;
  void __fastcall MarkToRecalc(int p_value);
  AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
  TS_FORM_LINE_SHOW(TFInvActLine)
public:
  __fastcall TFInvActItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm);
};
//---------------------------------------------------------------------------
#endif
