//---------------------------------------------------------------------------

#ifndef FmOperRtlSalesPeriodH
#define FmOperRtlSalesPeriodH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmOperRtlSales.h"
#include "TSFMDIALOG.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
#include <ToolWin.hpp>
#include "TSFmDBDialog.h"
//---------------------------------------------------------------------------
class TFOperRtlSalesPeriod : public TTSFDialog
{
__published:	// IDE-managed Components
        TDateTimePicker *m_DTP_DateFrom;
        TDateTimePicker *m_DTP_DateTo;
        TDateTimePicker *m_DTP_TimeFrom;
        TDateTimePicker *m_DTP_TimeTo;
        TLabel *Label1;
        TLabel *Label2;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
private:	// User declarations
    TDOperRtlSales* m_dm;
public:		// User declarations
    __fastcall TFOperRtlSalesPeriod(TComponent* p_owner, TDOperRtlSales* p_dm);
};
#endif
