//---------------------------------------------------------------------------

#ifndef DmUnprocessDocumentH
#define DmUnprocessDocumentH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMOPERATION.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include <Registry.hpp>
#include "MLFuncs.h"
//---------------------------------------------------------------------------
class TDUnprocessDocument : public TTSDOperation
{
__published:
        TMLQuery *m_Q_org;
        TMLQuery *m_Q_org_fict;
        TMLUpdateSQL *m_U_org_fict;
        TFloatField *m_Q_org_fictID;
        TStringField *m_Q_org_fictNAME;
        TFloatField *m_Q_orgID;
        TStringField *m_Q_orgNAME;
	TStringField *m_Q_mainORG_NAME;
	TFloatField *m_Q_mainDOC_ID;
	TStringField *m_Q_mainDOC_NAME;
	TDateTimeField *m_Q_mainCREATE_DATE;
	TStringField *m_Q_mainAUTHOR;
	TStringField *m_Q_mainMODIFY_USER;
	TStringField *m_Q_mainFLDR_NAME;
	TDateTimeField *m_Q_mainMODIFY_DATE;
	TStringField *m_Q_mainSENDER_NAME;
	TStringField *m_Q_mainRECEIVER_NAME;
	TStringField *m_Q_mainSUPPLIER_NAME;
        TFloatField *m_Q_mainCNT_ID;
        TStringField *m_Q_mainPOSITION;
        TStringField *m_Q_mainDOP_INFO;
private:
    void SetFictFilterORGValues();
    TDateTime __fastcall GetBeginDate();
    TDateTime __fastcall GetEndDate();
public:
    __fastcall TDUnprocessDocument(TComponent* p_owner, AnsiString p_prog_id);
    void SaveToReestrFilterValue(char *FilterName, int FilterValue);
    int GetFromReestrFilterValue(char *FilterName);
    int m_home_org_id;
    void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};

};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
 