inherited DIncome: TDIncome
  OldCreateOrder = False
  Left = 744
  Top = 264
  Height = 767
  Width = 1155
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_list: TMLQuery
    AfterOpen = m_Q_listAfterOpen
    BeforeClose = m_Q_listBeforeClose
    SQL.Strings = (
      'WITH fldrs AS'
      ' (SELECT /*+ materialize */ '
      '         ff.id,'
      '         ff.name,'
      '         ff.doc_type_id,'
      
        '         rpad(fnc_mask_folder_right(ff.id, :ACCESS_MASK), 250) A' +
        'S doc_fldr_right'
      '    FROM folders ff'
      '   WHERE ff.doc_type_id = :DOC_TYPE_ID'
      '     AND fnc_mask_folder_right(ff.id, '#39'Select'#39') = 1)'
      'select /*+ INDEX (D XPK_DOCUMENTS)*/'
      '  d.id,'
      '  d.src_org_id doc_src_org_id,'
      '  o.name doc_src_org_name,'
      '  d.comp_name doc_create_comp_name,'
      '  d.create_date doc_create_date,'
      '  d.author doc_create_user,'
      '  d.status doc_status,'
      
        '  decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �� ���' +
        '���'#39','#39'������'#39') doc_status_name,'
      '  d.status_change_date doc_status_change_date,'
      '  d.fldr_id doc_fldr_id,'
      '  f.name doc_fldr_name,'
      '  f.doc_fldr_right,'
      '  d.modify_comp_name doc_modify_comp_name,'
      '  d.modify_user doc_modify_user,'
      '  d.modify_date doc_modify_date,'
      '  i.doc_number,'
      '  i.doc_date,'
      '  gdp.name gdp_name,'
      '  i.receiver_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(sm.cnt_id),1,250) r' +
        'eceiver_name,'
      '  i.inc_started,'
      '  i.inc_finished,'
      '  i.amount_in,'
      '  i.amount_out,'
      '  i.corr_amt,'
      '  i.note,'
      '  i.acc_doc_id,'
      
        '  substr('#39'�'#39' || ppd.doc_number || '#39' �� '#39' || TO_CHAR(ppd.issued_d' +
        'ate, '#39'dd.mm.yyyy'#39') || '#39' ('#39' || ppd.note || '#39')'#39',1,250) acc_note,'
      '  ppd.sender_id cnt_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,25' +
        '0) cnt_name,'
      '  ppd.buyer_id mngr_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(ppd.buyer_id),1,250' +
        ') mngr_name,'
      
        '  trim(rpad(cc.doc_number || decode(nvl(cc.note,'#39#39'),'#39#39','#39#39','#39' ('#39' |' +
        '| cc.note  || '#39')'#39'),250)) as cnt_doc_number,'
      '  i.amount_non_stored,'
      '  rs.sign,'
      '  rs.sign_date,'
      '  i.inc_hour,'
      '  i.inc_minute,'
      '  i.with_recount,'
      
        '  (select max(gd.name) from groups_of_depositories gd, rtl_bills' +
        ' rb'
      '   where rb.acc_doc_id = i.acc_doc_id'
      '   and  gd.id = rb.gr_dep_id) as gr_dep_name'
      'from '
      '  documents d,'
      '  fldrs f,'
      '  organizations o,'
      '  rtl_incomes i,'
      '  prepay_documents ppd,'
      '  client_contracts cc,  '
      '  groups_of_depositories gdp,'
      '  staffmen sm,'
      '  rtl_income_sign rs'
      'where f.id = d.fldr_id'
      '  and o.id = d.src_org_id'
      '  and i.doc_id = d.id'
      
        '  and i.doc_date between to_date(:BEGIN_DATE) and to_date(:END_D' +
        'ATE) + 1 - 1/24/60/60'
      '  and ppd.doc_id = i.acc_doc_id'
      '  AND ppd.cnt_doc_id = cc.doc_id(+)'
      '  and (d.src_org_id = :ORG_ID OR -1 = :ORG_ID)'
      '  and gdp.id = i.gr_dep_id'
      '  and sm.id (+) = i.receiver_id'
      '  AND rs.doc_id(+) = i.doc_id'
      '  and 1 = case '
      
        '            when :NMCL_ID_FILTER is null and :NMCL_NAME_FILTER i' +
        's null then 1'
      '            when :NMCL_ID_FILTER is not null then ('
      
        '              select 1 from rtl_income_items ii where doc_id = i' +
        '.doc_id and ii.nmcl_id = :NMCL_ID_FILTER'
      '            )'
      '            when :NMCL_NAME_FILTER is not null then ('
      
        '              select 1 from rtl_income_items ii, nomenclature_it' +
        'ems ni'
      
        '              where ii.doc_id = i.doc_id and ni.id = ii.nmcl_id ' +
        'and ni.name like '#39'%'#39' || :NMCL_NAME_FILTER || '#39'%'#39
      '            )'
      '          end'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NMCL_NAME_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NMCL_NAME_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NMCL_NAME_FILTER'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 'd.id'
    end
    inherited m_Q_listDOC_SRC_ORG_NAME: TStringField
      Origin = 'o.name'
    end
    inherited m_Q_listDOC_CREATE_DATE: TDateTimeField
      Origin = 'd.create_date'
    end
    inherited m_Q_listDOC_CREATE_COMP_NAME: TStringField
      Origin = 'd.comp_name'
    end
    inherited m_Q_listDOC_CREATE_USER: TStringField
      Origin = 'd.author'
    end
    inherited m_Q_listDOC_MODIFY_DATE: TDateTimeField
      Origin = 'd.modify_date'
    end
    inherited m_Q_listDOC_MODIFY_COMP_NAME: TStringField
      Origin = 'd.modify_comp_name'
    end
    inherited m_Q_listDOC_MODIFY_USER: TStringField
      Origin = 'd.modify_user'
    end
    inherited m_Q_listDOC_STATUS_NAME: TStringField
      Origin = 
        'decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �� �����' +
        '�'#39','#39'������'#39')'
    end
    inherited m_Q_listDOC_STATUS_CHANGE_DATE: TDateTimeField
      Origin = 'd.status_change_date'
    end
    inherited m_Q_listDOC_FLDR_NAME: TStringField
      Origin = 'f.name'
    end
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'i.doc_number'
      Size = 10
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'DOC_DATE'
      Origin = 'i.doc_date'
    end
    object m_Q_listGDP_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'GDP_NAME'
      Origin = 'gdp.name'
    end
    object m_Q_listRECEIVER_ID: TFloatField
      DisplayLabel = 'ID ����'
      FieldName = 'RECEIVER_ID'
      Origin = 'i.receiver_id'
      Visible = False
    end
    object m_Q_listRECEIVER_NAME: TStringField
      DisplayLabel = '����'
      FieldName = 'RECEIVER_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(sm.cnt_id),1,250)'
    end
    object m_Q_listINC_STARTED: TDateTimeField
      DisplayLabel = '������ ������������'
      FieldName = 'INC_STARTED'
      Origin = 'i.inc_started'
    end
    object m_Q_listINC_FINISHED: TDateTimeField
      DisplayLabel = '��������� ������������'
      FieldName = 'INC_FINISHED'
      Origin = 'i.inc_finished'
    end
    object m_Q_listAMOUNT_IN: TFloatField
      DisplayLabel = '����� � ������� �����'
      FieldName = 'AMOUNT_IN'
      Origin = 'i.amount_in'
      currency = True
    end
    object m_Q_listAMOUNT_OUT: TFloatField
      DisplayLabel = '����� � ����� ����������'
      FieldName = 'AMOUNT_OUT'
      Origin = 'i.amount_out'
      currency = True
    end
    object m_Q_listCORR_AMT: TFloatField
      DisplayLabel = '���������������� �����'
      FieldName = 'CORR_AMT'
      Origin = 'i.corr_amt'
      currency = True
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'i.note'
      Size = 30
    end
    object m_Q_listACC_DOC_ID: TFloatField
      DisplayLabel = 'ID ���.���.'
      FieldName = 'ACC_DOC_ID'
      Origin = 'i.acc_doc_id'
    end
    object m_Q_listACC_NOTE: TStringField
      DisplayLabel = '������������� ��������'
      FieldName = 'ACC_NOTE'
      Origin = 
        'substr('#39'�'#39' || ppd.doc_number || '#39' �� '#39' || TO_CHAR(ppd.issued_dat' +
        'e, '#39'dd.mm.yyyy'#39') || '#39' ('#39' || ppd.note || '#39')'#39',1,250)'
      Size = 30
    end
    object m_Q_listCNT_ID: TFloatField
      DisplayLabel = 'ID ����������'
      FieldName = 'CNT_ID'
      Origin = 'ppd.sender_id'
      Visible = False
    end
    object m_Q_listCNT_NAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'CNT_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,250)'
    end
    object m_Q_listMNGR_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'MNGR_ID'
      Origin = 'ppd.buyer_id'
      Visible = False
    end
    object m_Q_listMNGR_NAME: TStringField
      DisplayLabel = '��������'
      FieldName = 'MNGR_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(ppd.buyer_id),1,250)'
    end
    object m_Q_listCNT_DOC_NUMBER: TStringField
      DisplayLabel = '�������'
      DisplayWidth = 70
      FieldName = 'CNT_DOC_NUMBER'
      Origin = 
        'trim(rpad(cc.doc_number || decode(nvl(cc.note,'#39#39'),'#39#39','#39#39','#39' ('#39' || ' +
        'cc.note  || '#39')'#39'),250))'
      Size = 250
    end
    object m_Q_listAMOUNT_NON_STORED: TFloatField
      DisplayLabel = '����� �� � �����'
      FieldName = 'AMOUNT_NON_STORED'
      Origin = 'i.amount_non_stored'
      currency = True
    end
    object m_Q_listSIGN: TStringField
      DisplayLabel = '��� �����'
      FieldName = 'SIGN'
      Origin = 'rs.sign'
    end
    object m_Q_listSIGN_DATE: TDateTimeField
      DisplayLabel = '��� ����� (����)'
      FieldName = 'SIGN_DATE'
      Origin = 'rs.sign_date'
    end
    object m_Q_listINC_HOUR: TFloatField
      DisplayLabel = '����� ��������|�.'
      FieldName = 'INC_HOUR'
      Origin = 'i.inc_hour'
    end
    object m_Q_listINC_MINUTE: TFloatField
      DisplayLabel = '����� ��������|���.'
      FieldName = 'INC_MINUTE'
      Origin = 'i.inc_minute'
    end
    object m_Q_listWITH_RECOUNT: TStringField
      DisplayLabel = '� ���������� ���-��'
      FieldName = 'WITH_RECOUNT'
      Origin = 'i.with_recount'
      FixedChar = True
      Size = 1
    end
    object m_Q_listGR_DEP_NAME: TStringField
      DisplayLabel = '����� ��'
      FieldName = 'GR_DEP_NAME'
      Origin = 
        '(select max(gd.name) from groups_of_depositories gd, rtl_bills r' +
        'b where rb.acc_doc_id = i.acc_doc_id and  gd.id = rb.gr_dep_id)'
      Size = 100
    end
  end
  inherited m_Q_item: TMLQuery
    BeforeClose = m_Q_itemBeforeClose
    SQL.Strings = (
      'select'
      '  i.doc_id id,'
      '  d.fldr_id,'
      '  d.src_org_id,'
      '  i.doc_number,'
      '  i.doc_date,'
      '  i.gr_dep_id,'
      '  gdp.name gdp_name,'
      '  i.receiver_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(sm.cnt_id),1,250) r' +
        'eceiver_name,'
      '  i.inc_started,'
      '  i.inc_finished,'
      '  i.amount_in,'
      '  i.amount_out,'
      '  i.corr_amt,'
      '  i.note,'
      '  ppd.doc_id acc_doc_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,25' +
        '0) cnt_name,'
      '  ppd.sender_id,'
      '  ppd.note acc_note,'
      '  ppd.doc_number acc_doc_number,'
      '  ppd.issued_date acc_doc_date,'
      '  ppd.amount acc_doc_amount,'
      '  m.note mngr_note,'
      '  ppd.cnt_doc_id contract_id,'
      
        '  substr(cc.doc_number || nvl2(cc.note,'#39' ('#39' || cc.note || '#39')'#39','#39#39 +
        '),1,250) contract_note,'
      '       nvl(i.amount_in,0) + nvl(i.corr_amt,0) AS sum_with_corr,'
      ' i.arch_bill_doc_id,'
      ' scht.doc_id as scht_doc_id, '
      ' scht.doc_number as scht_doc_number, '
      ' scht.doc_date as scht_doc_date, '
      ' scht.src_name as scht_src_oname, '
      ' scht.dst_name as scht_dst_oname,'
      ' did.doc_id as diff_doc_id,'
      ' i.amount_non_stored,'
      ' ris.sign,'
      ' ris.sign_date,'
      ' ppd.ttn_number,'
      ' ppd.ttn_date,'
      
        ' (select NVL(MAX(param_value),0) from org_params where org_par_i' +
        'd = 44 AND org_id = d.src_org_id) as no_torg2,'
      ' i.inc_hour,'
      ' i.inc_minute,'
      ' i.with_recount,'
      ' scht.gr_dep_name'
      'from '
      '  rtl_incomes i, '
      '  documents d,'
      '  groups_of_depositories gdp,'
      '  prepay_documents ppd,'
      '  managers m,'
      '  staffmen sm,'
      '  client_contracts cc,'
      '  ('
      '    select '
      '    b.doc_id, b.acc_doc_id, b.doc_number, b.doc_date, '
      '    b.src_org_id src_id, o_src.name src_name, '
      
        '    d.src_org_id dst_id, o_dest.name dst_name, gd.name as gr_dep' +
        '_name'
      
        '    from rtl_bills b, documents d, organizations o_src, organiza' +
        'tions o_dest, rtl_incomes ri, groups_of_depositories gd'
      '    WHERE d.id = b.doc_id'
      '    AND o_src.id(+) = b.src_org_id'
      '    AND gd.id(+) = b.gr_dep_id'
      '    AND o_dest.id = d.src_org_id'
      '    AND b.acc_doc_id = ri.acc_doc_id'
      '    AND ri.doc_id = :ID'
      
        '    AND b.doc_number = (SELECT MAX(doc_number) FROM rtl_bills WH' +
        'ERE acc_doc_id = ri.acc_doc_id)'
      '  ) scht,'
      '  doc_income_diff did,'
      '  rtl_income_sign ris'
      'where '
      '  i.doc_id = :ID'
      '  and d.id = i.doc_id'
      '  and d.fldr_id = :FLDR_ID'
      '  and gdp.id = i.gr_dep_id'
      '  and sm.id = i.receiver_id'
      '  and ppd.doc_id (+) = i.acc_doc_id'
      '  and m.cnt_id (+) = ppd.buyer_id'
      '  and cc.doc_id (+) = ppd.cnt_doc_id'
      '  and scht.acc_doc_id(+) = i.acc_doc_id'
      '  and did.acc_doc_id(+) = i.acc_doc_id'
      '  and ris.doc_id(+) = i.doc_id')
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_ID'
        ParamType = ptInput
      end>
    object m_Q_itemFLDR_ID: TFloatField
      FieldName = 'FLDR_ID'
    end
    object m_Q_itemSRC_ORG_ID: TFloatField
      FieldName = 'SRC_ORG_ID'
    end
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 50
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemGR_DEP_ID: TFloatField
      FieldName = 'GR_DEP_ID'
    end
    object m_Q_itemGDP_NAME: TStringField
      FieldName = 'GDP_NAME'
      Size = 100
    end
    object m_Q_itemRECEIVER_ID: TFloatField
      FieldName = 'RECEIVER_ID'
    end
    object m_Q_itemRECEIVER_NAME: TStringField
      FieldName = 'RECEIVER_NAME'
      Size = 250
    end
    object m_Q_itemINC_STARTED: TDateTimeField
      FieldName = 'INC_STARTED'
    end
    object m_Q_itemINC_FINISHED: TDateTimeField
      FieldName = 'INC_FINISHED'
    end
    object m_Q_itemAMOUNT_IN: TFloatField
      FieldName = 'AMOUNT_IN'
      currency = True
    end
    object m_Q_itemAMOUNT_OUT: TFloatField
      FieldName = 'AMOUNT_OUT'
      currency = True
    end
    object m_Q_itemCORR_AMT: TFloatField
      FieldName = 'CORR_AMT'
      currency = True
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemACC_DOC_ID: TFloatField
      FieldName = 'ACC_DOC_ID'
    end
    object m_Q_itemCNT_NAME: TStringField
      FieldName = 'CNT_NAME'
      Size = 250
    end
    object m_Q_itemACC_NOTE: TStringField
      FieldName = 'ACC_NOTE'
      Size = 250
    end
    object m_Q_itemACC_DOC_NUMBER: TStringField
      FieldName = 'ACC_DOC_NUMBER'
      Size = 50
    end
    object m_Q_itemACC_DOC_DATE: TDateTimeField
      FieldName = 'ACC_DOC_DATE'
    end
    object m_Q_itemACC_DOC_AMOUNT: TFloatField
      FieldName = 'ACC_DOC_AMOUNT'
    end
    object m_Q_itemMNGR_NOTE: TStringField
      FieldName = 'MNGR_NOTE'
      Size = 250
    end
    object m_Q_itemCONTRACT_ID: TFloatField
      FieldName = 'CONTRACT_ID'
    end
    object m_Q_itemCONTRACT_NOTE: TStringField
      FieldName = 'CONTRACT_NOTE'
      Size = 250
    end
    object m_Q_itemSUM_WITH_CORR: TFloatField
      FieldName = 'SUM_WITH_CORR'
      currency = True
    end
    object m_Q_itemARCH_BILL_DOC_ID: TFloatField
      FieldName = 'ARCH_BILL_DOC_ID'
    end
    object m_Q_itemSENDER_ID: TFloatField
      FieldName = 'SENDER_ID'
      OnChange = m_Q_itemSENDER_IDChange
    end
    object m_Q_itemSCHT_DOC_ID: TFloatField
      FieldName = 'SCHT_DOC_ID'
    end
    object m_Q_itemSCHT_DOC_NUMBER: TStringField
      FieldName = 'SCHT_DOC_NUMBER'
      Size = 100
    end
    object m_Q_itemSCHT_DOC_DATE: TDateTimeField
      FieldName = 'SCHT_DOC_DATE'
    end
    object m_Q_itemSCHT_SRC_ONAME: TStringField
      FieldName = 'SCHT_SRC_ONAME'
      Size = 250
    end
    object m_Q_itemSCHT_DST_ONAME: TStringField
      FieldName = 'SCHT_DST_ONAME'
      Size = 250
    end
    object m_Q_itemDIFF_DOC_ID: TFloatField
      FieldName = 'DIFF_DOC_ID'
    end
    object m_Q_itemAMOUNT_NON_STORED: TFloatField
      FieldName = 'AMOUNT_NON_STORED'
      currency = True
    end
    object m_Q_itemSIGN: TStringField
      FieldName = 'SIGN'
    end
    object m_Q_itemSIGN_DATE: TDateTimeField
      FieldName = 'SIGN_DATE'
    end
    object m_Q_itemTTN_NUMBER: TStringField
      FieldName = 'TTN_NUMBER'
      Size = 50
    end
    object m_Q_itemTTN_DATE: TDateTimeField
      FieldName = 'TTN_DATE'
    end
    object m_Q_itemNO_TORG2: TStringField
      FieldName = 'NO_TORG2'
      Size = 100
    end
    object m_Q_itemINC_HOUR: TFloatField
      FieldName = 'INC_HOUR'
    end
    object m_Q_itemINC_MINUTE: TFloatField
      FieldName = 'INC_MINUTE'
    end
    object m_Q_itemWITH_RECOUNT: TStringField
      FieldName = 'WITH_RECOUNT'
      FixedChar = True
      Size = 1
    end
    object m_Q_itemGR_DEP_NAME: TStringField
      FieldName = 'GR_DEP_NAME'
      Size = 100
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_incomes'
      'set'
      '  gr_dep_id = :GR_DEP_ID,'
      '  receiver_id = :RECEIVER_ID,'
      '  doc_number = :DOC_NUMBER,'
      '  acc_doc_id = :ACC_DOC_ID,'
      '  inc_started = :INC_STARTED,'
      '  inc_finished = :INC_FINISHED,'
      '  corr_amt = :CORR_AMT,'
      '  note = :NOTE,'
      '  inc_hour = :inc_hour,'
      '  inc_minute = :inc_minute'
      'where'
      '  doc_id = :ID')
    InsertSQL.Strings = (
      'insert into rtl_incomes'
      
        '  (doc_id, acc_doc_id, gr_dep_id, receiver_id, doc_date, doc_num' +
        'ber, inc_started, inc_finished, corr_amt, note, inc_hour, inc_mi' +
        'nute)'
      'values'
      
        '  (:ID, :ACC_DOC_ID, :GR_DEP_ID, :RECEIVER_ID, :DOC_DATE, :DOC_N' +
        'UMBER, :INC_STARTED, :INC_FINISHED, :CORR_AMT, :NOTE, :inc_hour,' +
        ' :inc_minute)')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      '  prc_delete_doc_inc_link(:OLD_ID);'
      'END; '
      ' ')
  end
  inherited m_Q_acc_serv: TQuery
    Left = 808
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select * from ('
      'select'
      '  ii.doc_id id,'
      '  ii.line_id line_id,'
      '  ii.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  ii.assortment_id,'
      '  a.name assortment_name,'
      '  ii.in_price,'
      '  t.tax_rate,'
      '  ii.out_price,'
      '  ii.qnt,'
      '  ii.out_price*ii.qnt out_amount,'
      '  ii.src_amount,'
      '  ii.country_id,'
      '  c.name country_name,'
      '  ii.scd_num,'
      '  nvl('
      '   (select to_number(par_value) from doc_item_good_params'
      '    where par_id = :PARAM_NOT_INCLUDE_ACT_VERIF'
      '      and doc_id = ii.doc_id'
      '      and line_id = ii.line_id),'
      '   0'
      '  ) not_include_act_verif,'
      '  rni.name reason_name,'
      '  ii.note,'
      '  decode(ii.stored,1,'#39'� �����'#39',0,'#39'�� � �����'#39') stored_desc,'
      '  ii.stored,'
      
        '  DECODE(ii.in_price,NULL,0,0,0,((ii.out_price / ii.in_price) * ' +
        '100) - 100) as percent,'
      '  ii.make_date,'
      
        '  decode(ii.stored,0,'#39'��'#39',(select max('#39'��'#39') from rtl_income_item' +
        's i'
      '                           where ii.doc_id = i.doc_id'
      '                           and ii.nmcl_id = i.nmcl_id'
      
        '                           and ii.assortment_id = i.assortment_i' +
        'd'
      '                           and i.stored = 0)) as torg2,'
      '   ni.prod_type_id,'
      '   pt.name as prod_name,'
      '   ii.pull_date,'
      '   ( SELECT SUBSTR( listagg(ris.pref_alccode, '#39', '#39') '
      
        '              WITHIN GROUP (order by ris.pref_alccode),1,255) as' +
        ' pref_alccode_list '
      
        '     FROM rtl_income_subitems ris WHERE ris.doc_id = ii.doc_id A' +
        'ND ris.line_id = ii.line_id '
      '   ) pref_alccode_list '
      'from'
      '  rtl_income_items ii,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  reason_not_income rni,'
      '  assortment a,'
      '  countries c,'
      '  nmcl_tax_rates t,'
      '  products_types pt'
      'where ii.doc_id = :ID'
      '  and ni.id = ii.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and ii.stored = nvl(:STORED,ii.stored)'
      '  and rni.id (+) = ii.non_storing_reason_id'
      '  and c.id = ii.country_id'
      '  and a.id = ii.assortment_id'
      '  and t.nmcl_id (+) = ii.nmcl_id'
      '  and t.tax_id (+) = 62'
      '  and pt.id = ni.prod_type_id)'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      ''
      ''
      ' '
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PARAM_NOT_INCLUDE_ACT_VERIF'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'STORED'
        ParamType = ptUnknown
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'ID'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      FieldName = 'NMCL_NAME'
      Origin = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_linesMEAS_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MEAS_NAME'
      Origin = 'MEAS_NAME'
      Size = 30
    end
    object m_Q_linesASSORTMENT_ID: TFloatField
      DisplayLabel = '��� ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'ASSORTMENT_ID'
    end
    object m_Q_linesASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_linesIN_PRICE: TFloatField
      DisplayLabel = '����*'
      FieldName = 'IN_PRICE'
      Origin = 'IN_PRICE'
      currency = True
    end
    object m_Q_linesTAX_RATE: TFloatField
      DisplayLabel = '������ ���'
      FieldName = 'TAX_RATE'
      Origin = 'TAX_RATE'
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'OUT_PRICE'
      currency = True
    end
    object m_Q_linesPERCENT: TFloatField
      DisplayLabel = '%'
      FieldName = 'PERCENT'
      DisplayFormat = '0.00'
    end
    object m_Q_linesQNT: TFloatField
      DisplayLabel = '���-��'
      FieldName = 'QNT'
      Origin = 'QNT'
    end
    object m_Q_linesOUT_AMOUNT: TFloatField
      DisplayLabel = '�����'
      FieldName = 'OUT_AMOUNT'
      Origin = 'OUT_AMOUNT'
      currency = True
    end
    object m_Q_linesSRC_AMOUNT: TFloatField
      DisplayLabel = '����� ���.'
      FieldName = 'SRC_AMOUNT'
      currency = True
    end
    object m_Q_linesCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Origin = 'COUNTRY_ID'
    end
    object m_Q_linesCOUNTRY_NAME: TStringField
      DisplayLabel = '������ �������������'
      FieldName = 'COUNTRY_NAME'
      Origin = 'COUNTRY_NAME'
      Size = 50
    end
    object m_Q_linesSCD_NUM: TStringField
      DisplayLabel = '� ���'
      FieldName = 'SCD_NUM'
      Origin = 'SCD_NUM'
      Size = 30
    end
    object m_Q_linesNOT_INCLUDE_ACT_VERIF: TFloatField
      DisplayLabel = '�� ��������� � ������'
      FieldName = 'NOT_INCLUDE_ACT_VERIF'
    end
    object m_Q_linesREASON_NAME: TStringField
      DisplayLabel = '���������� "�� � �����"'
      DisplayWidth = 50
      FieldName = 'REASON_NAME'
      Origin = 'REASON_NAME'
      Size = 250
    end
    object m_Q_linesNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 250
    end
    object m_Q_linesSTORED_DESC: TStringField
      DisplayLabel = '���'
      FieldName = 'STORED_DESC'
      Origin = 'STORED_DESC'
      Size = 10
    end
    object m_Q_linesSTORED: TFloatField
      FieldName = 'STORED'
      Origin = 'STORED'
    end
    object m_Q_linesMAKE_DATE: TDateTimeField
      DisplayLabel = '���� ������������'
      FieldName = 'MAKE_DATE'
      Origin = 'MAKE_DATE'
    end
    object m_Q_linesTORG2: TStringField
      DisplayLabel = '����-2'
      FieldName = 'TORG2'
      Origin = 'TORG2'
      Size = 2
    end
    object m_Q_linesPROD_TYPE_ID: TFloatField
      DisplayLabel = 'ID ������ ������'
      FieldName = 'PROD_TYPE_ID'
      Origin = 'PROD_TYPE_ID'
    end
    object m_Q_linesPROD_NAME: TStringField
      DisplayLabel = '������ ������'
      FieldName = 'PROD_NAME'
      Origin = 'PROD_NAME'
      Size = 100
    end
    object m_Q_linesPULL_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'PULL_DATE'
      Origin = 'PULL_DATE'
    end
    object m_Q_linesPREF_ALCCODE_LIST: TStringField
      DisplayLabel = '������. �������'
      DisplayWidth = 25
      FieldName = 'PREF_ALCCODE_LIST'
      Origin = 'PREF_ALCCODE_LIST'
      Size = 255
    end
  end
  inherited m_Q_line: TMLQuery
    AfterClose = m_Q_lineAfterClose
    SQL.Strings = (
      'select'
      '  ii.doc_id id,'
      '  ii.line_id,'
      '  ii.nmcl_id,'
      '  ii.card_id,'
      '  ni.name nmcl_name,'
      '  ii.assortment_id,'
      '  a.name assortment_name,'
      '  mu.name mu_name,'
      '  ii.in_price,'
      '  ii.out_price,'
      '  ii.qnt,'
      '  ii.out_price*ii.qnt out_amount,'
      
        '  round((ii.out_price / nullif(ii.in_price,0) - 1) * 100,2) incr' +
        'ease_prc,'
      '  ii.src_amount,'
      '  ii.stored,'
      '  ii.note,'
      '  ii.non_storing_reason_id,'
      '  rni.name reason_name,'
      '  ii.country_id,'
      '  c.name country_name,'
      '  ii.scd_num,'
      '  inf.bar_code_note,'
      
        '  PKG_GOODS_CARDS.fnc_get_nmcl_rest(ii.nmcl_id,ii.assortment_id,' +
        'sysdate,0) as cur_rest,'
      '  ii.make_date,'
      '  TRIM(RPAD(inf.bar_code_note||'#39' '#39'||(SELECT MAX(nbc.note_bc)'
      '   FROM nmcl_bar_codes nbc'
      
        '   WHERE nbc.nmcl_id = ii.nmcl_id AND nbc.assortment_id = ii.ass' +
        'ortment_id AND nbc.enable = '#39'Y'#39'),250))  AS note_bc'
      'from'
      '  rtl_income_items ii,'
      '  reason_not_income rni,'
      '  nomenclature_items ni, countries c,'
      '  measure_units mu, assortment a, nmcl_info inf'
      'where'
      '  ii.doc_id = :ID'
      '  and ii.line_id = :LINE_ID'
      '  and ni.id = ii.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and c.id = ii.country_id'
      '  and rni.id (+) = ii.non_storing_reason_id'
      '  and a.id (+) = ii.assortment_id'
      '  AND ii.nmcl_id = inf.nmcl_id(+)'
      ''
      ' ')
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_lineCARD_ID: TFloatField
      FieldName = 'CARD_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_lineASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
    end
    object m_Q_lineASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_lineMU_NAME: TStringField
      FieldName = 'MU_NAME'
      Size = 30
    end
    object m_Q_lineIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      currency = True
    end
    object m_Q_lineOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
      currency = True
    end
    object m_Q_lineINCREASE_PRC: TFloatField
      FieldName = 'INCREASE_PRC'
    end
    object m_Q_lineQNT: TFloatField
      FieldName = 'QNT'
    end
    object m_Q_lineOUT_AMOUNT: TFloatField
      FieldName = 'OUT_AMOUNT'
      currency = True
    end
    object m_Q_lineSRC_AMOUNT: TFloatField
      FieldName = 'SRC_AMOUNT'
      currency = True
    end
    object m_Q_lineSTORED: TFloatField
      FieldName = 'STORED'
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_lineNON_STORING_REASON_ID: TFloatField
      FieldName = 'NON_STORING_REASON_ID'
    end
    object m_Q_lineREASON_NAME: TStringField
      FieldName = 'REASON_NAME'
      Size = 250
    end
    object m_Q_lineCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
    end
    object m_Q_lineCOUNTRY_NAME: TStringField
      FieldName = 'COUNTRY_NAME'
      Size = 50
    end
    object m_Q_lineSCD_NUM: TStringField
      FieldName = 'SCD_NUM'
      Size = 30
    end
    object m_Q_lineBAR_CODE_NOTE: TStringField
      FieldName = 'BAR_CODE_NOTE'
      Size = 100
    end
    object m_Q_lineCUR_REST: TFloatField
      FieldName = 'CUR_REST'
    end
    object m_Q_lineMAKE_DATE: TDateTimeField
      FieldName = 'MAKE_DATE'
    end
    object m_Q_lineNOTE_BC: TStringField
      FieldName = 'NOTE_BC'
      Size = 250
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      '  update rtl_income_items'
      '  set'
      '    nmcl_id = :NMCL_ID,'
      '    assortment_id = nvl(:ASSORTMENT_ID,7),'
      '    qnt = :QNT,'
      '    src_amount = :SRC_AMOUNT,'
      '    in_price = :IN_PRICE,'
      '    out_price = :OUT_PRICE,'
      '    stored = :STORED,'
      '    non_storing_reason_id = :NON_STORING_REASON_ID,'
      '    card_id = :CARD_ID,'
      '    country_id = :COUNTRY_ID,'
      '    scd_num = :SCD_NUM,'
      '    note = :NOTE,'
      '    make_date = :make_date'
      '  where'
      '    doc_id = :ID and line_id = :LINE_ID')
    InsertSQL.Strings = (
      'insert into rtl_income_items'
      
        '  (doc_id,line_id,nmcl_id,assortment_id,qnt,src_amount,in_price,' +
        'out_price,'
      
        '   stored,non_storing_reason_id,card_id,country_id,scd_num,make_' +
        'date)'
      'values'
      
        '  (:ID,:LINE_ID,:NMCL_ID,nvl(:ASSORTMENT_ID,7),:QNT,:SRC_AMOUNT,' +
        ':IN_PRICE,:OUT_PRICE,'
      
        '   :STORED,:NON_STORING_REASON_ID,:CARD_ID,:COUNTRY_ID,:SCD_NUM,' +
        ' :make_date)')
    DeleteSQL.Strings = (
      'begin'
      
        'delete from rtl_income_subitems where doc_id = :OLD_ID and line_' +
        'id = :OLD_LINE_ID;'
      
        'delete from rtl_income_items where doc_id = :OLD_ID and line_id ' +
        '= :OLD_LINE_ID;'
      'end;')
    IgnoreRowsAffected = True
  end
  object m_Q_nmcls: TMLQuery
    BeforeOpen = m_Q_nmclsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select'
      '  ii.doc_id id,'
      '  ii.line_id,'
      '  ii.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name mu_name,'
      '  ii.qnt,'
      '  ii.in_price,'
      '  ii.out_price,'
      '  decode(ii.stored,1,'#39'� �����'#39',0,'#39'�� � �����'#39') stored,'
      '  ii.pull_date'
      'from'
      '  documents d,'
      '  folders f,'
      '  organizations o,'
      '  rtl_incomes i,'
      '  rtl_income_items ii,'
      '  nomenclature_items ni,'
      '  prepay_documents ppd,'
      '  measure_units mu,'
      '  groups_of_depositories gdp,'
      '  staffmen sm'
      'where f.doc_type_id = :DOC_TYPE_ID'
      '  and f.id = d.fldr_id'
      '  and o.id = d.src_org_id'
      '  and i.doc_id = d.id'
      
        '  and i.doc_date between trunc(to_date(:BEGIN_DATE)) AND trunc(t' +
        'o_date(:END_DATE)) + 1 - 1/24/60/60'
      '  and instr(fnc_mask_folder_right(f.id,:ACCESS_MASK),'#39'1'#39') != 0'
      '  and (d.src_org_id = :ORG_ID OR -1 = :ORG_ID)'
      '  and ii.doc_id = i.doc_id'
      '  and ni.id = ii.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and ppd.doc_id = i.acc_doc_id'
      '  and gdp.id = i.gr_dep_id'
      '  and sm.id = i.receiver_id'
      '  and 1 = case '
      
        '            when :NMCL_ID_FILTER is null and :NMCL_NAME_FILTER i' +
        's null then 1'
      
        '            when :NMCL_ID_FILTER is not null then decode(:NMCL_I' +
        'D_FILTER,ii.nmcl_id,1)'
      
        '            when :NMCL_NAME_FILTER is not null then sign(instr(n' +
        'i.name,:NMCL_NAME_FILTER))'
      '          end'
      '%LIST_WHERE_CLAUSE'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'LIST_WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 224
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NMCL_NAME_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NMCL_NAME_FILTER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NMCL_NAME_FILTER'
        ParamType = ptInput
      end>
    object m_Q_nmclsID: TFloatField
      FieldName = 'ID'
      Origin = 'ii.doc_id'
    end
    object m_Q_nmclsLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'ii.line_id'
    end
    object m_Q_nmclsNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'ii.nmcl_id'
    end
    object m_Q_nmclsNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      DisplayWidth = 50
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_nmclsMU_NAME: TStringField
      DisplayLabel = '��. ���.'
      DisplayWidth = 20
      FieldName = 'MU_NAME'
      Origin = 'mu.name'
      Size = 30
    end
    object m_Q_nmclsQNT: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QNT'
      Origin = 'ii.qnt'
    end
    object m_Q_nmclsIN_PRICE: TFloatField
      DisplayLabel = '������� ���� '
      FieldName = 'IN_PRICE'
      Origin = 'ii.in_price'
    end
    object m_Q_nmclsOUT_PRICE: TFloatField
      DisplayLabel = '���� ����������'
      FieldName = 'OUT_PRICE'
      Origin = 'ii.out_price'
    end
    object m_Q_nmclsSTORED: TStringField
      DisplayLabel = '���'
      FieldName = 'STORED'
      Origin = 'decode(ii.stored,1,'#39'� �����'#39',0,'#39'�� � �����'#39')'
      Size = 10
    end
    object m_Q_nmclsPULL_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'PULL_DATE'
      Origin = 'ii.pull_date'
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name from organizations'
      'where enable = '#39'Y'#39' '
      'UNION ALL'
      'SELECT -1, '#39' ���'#39' FROM DUAL'
      'order by name')
    Left = 16
    Top = 296
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_U_fict_org: TMLUpdateSQL
    IgnoreRowsAffected = True
    IgnoreApply = True
    Left = 152
    Top = 296
  end
  object m_Q_fict_org: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id from organizations where 1 = 0')
    UpdateObject = m_U_fict_org
    Macros = <>
    Left = 80
    Top = 296
    object m_Q_fict_orgID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
  end
  object m_Q_gdp_names: TMLQuery
    BeforeOpen = m_Q_gdp_namesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct gd.id gd_id, gd.name gd_name'
      'from depositories d, groups_of_depositories gd'
      
        'where d.org_id = :HOME_ORG_ID and d.enable = '#39'Y'#39' and gd.id = d.g' +
        'roup_id'
      'and exists (select 1 from org_params'
      
        '                where org_par_id = 24 and org_id = :HOME_ORG_ID ' +
        'and end_date is null'
      '                and to_number(param_value) = gd.id)'
      '%WHERE_CLAUSE'
      'order by gd.id')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    MLOptions = [qoOrderDisable, qoWithoutWriteReg]
    Left = 320
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_gdp_namesGD_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'GD_ID'
      Origin = 'GD.ID'
    end
    object m_Q_gdp_namesGD_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'GD_NAME'
      Origin = 'GD.NAME'
      Size = 100
    end
  end
  object m_Q_dup_acc_doc: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT MAX(acc_doc_id)  as acc_doc_id, doc_number, doc_date '
      'FROM '
      '('
      '    select acc_doc_id, doc_number, doc_date from fk_incomes '
      '    where doc_id != :ID and acc_doc_id = :ACC_DOC_ID'
      '        UNION ALL'
      '    select acc_doc_id, doc_number, doc_date from rtl_incomes '
      '    where doc_id != :ID and acc_doc_id = :ACC_DOC_ID'
      ') v'
      'group by doc_number, doc_date')
    Macros = <>
    Left = 416
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ACC_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ACC_DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_dup_acc_docACC_DOC_ID: TFloatField
      FieldName = 'ACC_DOC_ID'
      Origin = 'TSDBMAIN.RTL_INCOMES.ACC_DOC_ID'
    end
    object m_Q_dup_acc_docDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 50
    end
    object m_Q_dup_acc_docDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
  end
  object m_Q_receivers: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'with pos_templ as (SELECT const_string as val FROM constants WHE' +
        'RE const_name = '#39'ROZN_SPEC_FILTER'#39')'
      'select'
      '  sm.id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(sm.cnt_id),1,200) |' +
        '| '#39' '#39' || ec.name name,'
      '  d.name department'
      'from '
      '  staffmen sm,'
      '  employes_class ec,'
      '  staff_table st,'
      '  departments d,'
      '  pos_templ'
      'where '
      '  sm.hire_date <= trunc(sysdate)'
      '  and (sm.fire_date is null or sm.fire_date > trunc(sysdate))'
      
        '  and ec.id (+) = staff.staff_methods.get_employes_class(sm.id,s' +
        'ysdate)'
      '  and st.id = sm.staff_table_id'
      '  and st.disable_date is null'
      '  and ('
      '    regexp_like(upper(st.position),pos_templ.val)'
      '    or'
      '    sm.id in ('
      
        '      select r.sm_id from replace_reserve r, staff_table st1, po' +
        's_templ'
      '      where r.active = '#39'N'#39'  -- ���� ����� �� ���������'
      '        and r.start_date <= trunc(sysdate)'
      '        and r.replace_position_id = st1.id'
      '        and st1.disable_date is null'
      '        and regexp_like(upper(st1.position),pos_templ.val)'
      '    )'
      '  )'
      '  and d.id = st.dep_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 504
    Top = 296
    object m_Q_receiversID: TFloatField
      FieldName = 'ID'
      Origin = 'sm.id'
    end
    object m_Q_receiversNAME: TStringField
      DisplayLabel = '���, ���������'
      DisplayWidth = 30
      FieldName = 'NAME'
      Origin = 
        'substr(pkg_contractors.fnc_contr_full_name(sm.cnt_id),1,200) || ' +
        #39' '#39' || ec.name'
      Size = 250
    end
    object m_Q_receiversDEPARTMENT: TStringField
      DisplayLabel = '�������������'
      DisplayWidth = 15
      FieldName = 'DEPARTMENT'
      Origin = 'd.name'
      Size = 100
    end
  end
  object m_Q_nmcl_list: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      '  ni.id as nmcl_id,'
      '  ni.name as nmcl_name,'
      '  mu.name as mu_name'
      'from '
      '  nomenclature_items ni,'
      '  measure_units mu'
      'where '
      '  mu.id = ni.meas_unit_id'
      '%WHERE_CLAUSE'
      'order by ni.name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 24
    Top = 356
    object m_Q_nmcl_listNMCL_ID: TFloatField
      DisplayLabel = '���'
      DisplayWidth = 15
      FieldName = 'NMCL_ID'
      Origin = 'NI.ID'
    end
    object m_Q_nmcl_listNMCL_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'NMCL_NAME'
      Origin = 'NI.NAME'
      Size = 150
    end
    object m_Q_nmcl_listMU_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MU_NAME'
      Origin = 'MU.NAME'
      Size = 30
    end
  end
  object m_Q_assort: TMLQuery
    BeforeOpen = m_Q_assortBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name'
      'from assortment'
      'where '
      '  id in ('
      '    select assortment_id from nmcl_bar_codes'
      '    where nmcl_id = :NMCL_ID '
      '    and assortment_id != 7 and enable = '#39'Y'#39
      '  )'
      '%WHERE_CLAUSE'
      'order by name'
      ''
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 92
    Top = 356
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_assortID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_assortNAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 100
    end
  end
  object m_Q_tax_rate: TQuery
    BeforeOpen = m_Q_tax_rateBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select to_number(tax_rate) value'
      'from nmcl_tax_rates'
      'where tax_id = 62 and nmcl_id = :NMCL_ID ')
    Left = 24
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_tax_rateVALUE: TFloatField
      FieldName = 'VALUE'
    end
  end
  object m_Q_countries: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name FROM countries where id > 0'
      '%WHERE_CLAUSE'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    MLOptions = [qoOrderDisable, qoWithoutWriteReg]
    Left = 168
    Top = 356
    object m_Q_countriesID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_countriesNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 50
    end
  end
  object m_Q_reasons: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name from reason_not_income'
      '%WHERE_EXPRESSION'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 244
    Top = 356
    object m_Q_reasonsID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_reasonsNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 50
    end
  end
  object m_Q_nmcl_desc: TMLQuery
    BeforeOpen = m_Q_nmcl_descBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      '  c.assortment_id,'
      '  a.name assortment_name'
      'from '
      '  goods_record_cards c, '
      '  assortment a,'
      '  retail_goods_cards rc'
      'where'
      '  c.gr_dep_id = :GR_DEP_ID'
      '  and c.nmcl_id = :NMCL_ID'
      '  and a.id = c.assortment_id'
      '  and rc.card_id = c.id'
      'order by'
      '  rc.store_qnt desc')
    Macros = <>
    MLOptions = [qoOrderDisable, qoWithoutWriteReg]
    Left = 104
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'GR_DEP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_nmcl_descASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
      Origin = 'TSDBMAIN.GOODS_RECORD_CARDS.ASSORTMENT_ID'
    end
    object m_Q_nmcl_descASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
  end
  object m_Q_get_gtd_list: TMLQuery
    BeforeOpen = m_Q_get_gtd_listBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select gl.country_id, c.name country_name, gl.num_gtd'
      'from gtd_list gl, countries c'
      'where gl.nmcl_id = :NMCL_ID and gl.country_id = c.id'
      ' '
      ' ')
    Macros = <>
    Left = 320
    Top = 356
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_get_gtd_listCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Origin = 'TSDBMAIN.GTD_LIST.COUNTRY_ID'
    end
    object m_Q_get_gtd_listNUM_GTD: TStringField
      FieldName = 'NUM_GTD'
      Origin = 'TSDBMAIN.GTD_LIST.NUM_GTD'
      Size = 30
    end
    object m_Q_get_gtd_listCOUNTRY_NAME: TStringField
      FieldName = 'COUNTRY_NAME'
      Origin = 'TSDBMAIN.COUNTRIES.NAME'
      Size = 50
    end
  end
  object m_Q_act_tcd: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_act_tcdBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      '  di.doc_id,'
      '  di.doc_number,'
      '  di.doc_date,'
      '  di.inc_started,'
      '  di.inc_finished,'
      '  di.receiver_id,'
      
        '  substr(pkg_persons.fnc_person_name(di.receiver_id,1),1,250) re' +
        'ceiver_name,'
      '  di.note,'
      '  '#39'N'#39' ok'
      'from '
      '  documents d,'
      '  dct_incomes di'
      'where '
      '  d.fldr_id = :FLDR_ID'
      '  and d.status = '#39'F'#39
      '  and di.doc_id = d.id'
      '  and d.src_org_id = session_globals.get_home_org_id'
      '  and di.dest_doc_id is null'
      ' AND di.inc_started > sysdate - 10')
    UpdateObject = m_U_act_tcd
    Macros = <>
    Left = 24
    Top = 480
    ParamData = <
      item
        DataType = ftInteger
        Name = 'FLDR_ID'
        ParamType = ptInput
      end>
    object m_Q_act_tcdDOC_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'DOC_ID'
      Origin = 'di.doc_id'
    end
    object m_Q_act_tcdDOC_NUMBER: TStringField
      DisplayLabel = '�'
      FieldName = 'DOC_NUMBER'
      Origin = 'di.doc_number'
      Size = 50
    end
    object m_Q_act_tcdDOC_DATE: TDateTimeField
      DisplayLabel = '����'
      FieldName = 'DOC_DATE'
      Origin = 'di.doc_date'
    end
    object m_Q_act_tcdINC_STARTED: TDateTimeField
      DisplayLabel = '�������| ������'
      FieldName = 'INC_STARTED'
      Origin = 'di.inc_started'
    end
    object m_Q_act_tcdINC_FINISHED: TDateTimeField
      DisplayLabel = '�������| ���������'
      FieldName = 'INC_FINISHED'
      Origin = 'di.inc_finished'
    end
    object m_Q_act_tcdRECEIVER_ID: TFloatField
      FieldName = 'RECEIVER_ID'
      Origin = 'di.RECEIVER_ID'
    end
    object m_Q_act_tcdRECEIVER_NAME: TStringField
      DisplayLabel = '����'
      DisplayWidth = 100
      FieldName = 'RECEIVER_NAME'
      Origin = 'substr(pkg_persons.fnc_person_name(di.receiver_id,1),1,250)'
      Size = 250
    end
    object m_Q_act_tcdNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 100
      FieldName = 'NOTE'
      Origin = 'di.note'
      Size = 250
    end
    object m_Q_act_tcdOK: TStringField
      FieldName = 'OK'
      FixedChar = True
      Size = 1
    end
  end
  object m_U_act_tcd: TMLUpdateSQL
    Left = 104
    Top = 480
  end
  object m_Q_load_act_tcd: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '     pkg_act_tcd.prc_load_act_tcd(:P_DOC_ID, :P_SQ_ID);'
      'END;')
    Left = 248
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P_SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_clear_act_tcd: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  PKG_ACT_TCD.prc_clear_act_tcd(:P_DOC_ID);'
      'END;')
    Left = 280
    Top = 480
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P_DOC_ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_prices: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select pkg_rtl_prices.fnc_cur_in_price(:NMCL_ID,nvl(:ASSORTMENT_' +
        'ID,7)) in_price,'
      
        '       pkg_rtl_prices.fnc_cur_out_price(:NMCL_ID,nvl(:ASSORTMENT' +
        '_ID,7)) out_price,'
      
        '       PKG_GOODS_CARDS.fnc_get_nmcl_rest(:NMCL_ID,nvl(:ASSORTMEN' +
        'T_ID,7),sysdate,0) as cur_rest'
      'from dual')
    Left = 184
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end>
    object m_Q_pricesIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
    end
    object m_Q_pricesOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
    end
    object m_Q_pricesCUR_REST: TFloatField
      FieldName = 'CUR_REST'
    end
  end
  object m_Q_check_rtl_inc_nmcl: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT trim(rpad(fnc_check_rtl_inc_nmcl(:NMCL_ID, :ASSORTMENT_ID' +
        '),250)) as res FROM dual')
    Macros = <>
    Left = 424
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptUnknown
      end>
    object m_Q_check_rtl_inc_nmclRES: TStringField
      FieldName = 'RES'
      Size = 250
    end
  end
  object m_Q_nmcl_info: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_nmcl_infoBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT TRIM(RPAD(i.bar_code_note||'#39' '#39'||(SELECT MAX(nbc.note_bc)'
      '   FROM nmcl_bar_codes nbc'
      
        '   WHERE nbc.nmcl_id = i.nmcl_id AND nbc.assortment_id = :ASSORT' +
        '_ID AND nbc.enable = '#39'Y'#39'),250))  AS bar_code_note'
      'FROM nmcl_info i'
      'WHERE i.nmcl_id = :NMCL_ID  ')
    Macros = <>
    Left = 408
    Top = 356
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_nmcl_infoBAR_CODE_NOTE: TStringField
      DisplayWidth = 250
      FieldName = 'BAR_CODE_NOTE'
      Size = 250
    end
  end
  object m_Q_prc_fill_data_4_print_labels: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  PKG_RTL_PRINT_LABELS.prc_fill_data_4_print_labels(:ID,:SQ_ID);'
      'END;')
    Macros = <>
    Left = 520
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SQ_ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_bar_codes: TMLQuery
    BeforeOpen = m_Q_bar_codesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT bc.id, bc.bar_code, bc.nmcl_id, bc.assortment_id, bc.in_m' +
        'ain_meas, bc.note, bc.note_bc'
      '  FROM nmcl_bar_codes bc'
      '  WHERE bc.nmcl_id = :NMCL_ID'
      '    AND bc.assortment_id = :ASSORTMENT_ID'
      '    AND bc.enable = '#39'Y'#39)
    Macros = <>
    Left = 616
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptUnknown
      end>
    object m_Q_bar_codesID: TFloatField
      DisplayLabel = 'ID ��'
      FieldName = 'ID'
    end
    object m_Q_bar_codesBAR_CODE: TStringField
      DisplayLabel = '��'
      FieldName = 'BAR_CODE'
      Size = 30
    end
    object m_Q_bar_codesNMCL_ID: TFloatField
      DisplayLabel = 'ID ���.'
      FieldName = 'NMCL_ID'
    end
    object m_Q_bar_codesASSORTMENT_ID: TFloatField
      DisplayLabel = 'ID ������������.'
      FieldName = 'ASSORTMENT_ID'
    end
    object m_Q_bar_codesIN_MAIN_MEAS: TFloatField
      DisplayLabel = '��������'
      FieldName = 'IN_MAIN_MEAS'
    end
    object m_Q_bar_codesNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 80
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_bar_codesNOTE_BC: TStringField
      DisplayLabel = '���������� � ��'
      FieldName = 'NOTE_BC'
      Size = 250
    end
  end
  object m_Q_make_date: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_make_dateBeforeOpen
    AfterInsert = m_Q_make_dateAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT NVL(make_date, cast(null as date)) make_date, note, nmcl_' +
        'id, id, assort_id,'
      '               :ID AS inc_doc_id'
      'FROM rtl_shelf_lifes'
      'WHERE nmcl_id = :NMCL_ID'
      '      and assort_id = :ASSORT_ID')
    UpdateObject = m_U_make_date
    Macros = <>
    Left = 376
    Top = 488
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end>
    object m_Q_make_dateMAKE_DATE: TDateTimeField
      FieldName = 'MAKE_DATE'
      Origin = 'TSDBMAIN.RTL_SHELF_LIFES.MAKE_DATE'
    end
    object m_Q_make_dateNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_SHELF_LIFES.NOTE'
      Size = 250
    end
    object m_Q_make_dateNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.RTL_SHELF_LIFES.NMCL_ID'
    end
    object m_Q_make_dateID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_make_dateASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Origin = 'TSDBMAIN.RTL_SHELF_LIFES.ASSORT_ID'
    end
    object m_Q_make_dateINC_DOC_ID: TFloatField
      FieldName = 'INC_DOC_ID'
    end
  end
  object m_U_make_date: TMLUpdateSQL
    ModifySQL.Strings = (
      'BEGIN'
      '  UPDATE  rtl_shelf_lifes'
      '  SET make_date = :MAKE_DATE,'
      '          inc_doc_id = :INC_DOC_ID'
      '  WHERE nmcl_id = :NMCL_ID '
      '    AND assort_id = :ASSORT_ID;'
      ''
      ' IF sql%rowcount = 0 THEN'
      
        '  INSERT INTO  rtl_shelf_lifes(id, nmcl_id, make_date, assort_id' +
        ', inc_doc_id)'
      
        '  VALUES (sq_rtl_shelf_lifes.NEXTVAL, :NMCL_ID, :MAKE_DATE, :ASS' +
        'ORT_ID, :INC_DOC_ID);'
      ' END IF;'
      ' '
      'END;')
    InsertSQL.Strings = (
      'BEGIN'
      '  UPDATE  rtl_shelf_lifes'
      '  SET make_date = :MAKE_DATE,'
      '  INC_DOC_ID = :INC_DOC_ID'
      '  WHERE nmcl_id = :NMCL_ID '
      '    AND assort_id = :ASSORT_ID'
      ';'
      ''
      ' IF sql%rowcount = 0 THEN'
      
        '  INSERT INTO  rtl_shelf_lifes(id, nmcl_id, make_date, assort_id' +
        ', inc_doc_id)'
      
        '  VALUES (sq_rtl_shelf_lifes.NEXTVAL, :NMCL_ID, :MAKE_DATE, :ASS' +
        'ORT_ID, :INC_DOC_ID) ;'
      ' END IF;'
      ' '
      'END;')
    IgnoreRowsAffected = True
    Left = 376
    Top = 536
  end
  object m_Q_chk_hours: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT MAX(shelf_life) shelf_life, nvl(sum(is_weight),0)  AS is_' +
        'weight'
      'FROM ('
      'SELECt MAX(s.shelf_life) shelf_life, 0 AS is_weight'
      'FROM nmcl_weight_shelf_life s'
      'WHERE s.nmcl_id = :NMCL_ID'
      '  AND s.assort_id = :ASSORT_ID'
      '  AND s.shl_meas_unit_id = 4 '
      
        'and not EXISTS (select nmcl_id from rtl_make_date_exception wher' +
        'e nmcl_id = :nmcl_id)'
      'UNION '
      'SELECT null AS shelf_life, nvl(MAX(n.id),0) AS is_weight'
      'FROM nmcl_weight_list n'
      'WHERE n.nmcl_id = :NMCL_ID'
      '  AND n.assort_id = :ASSORT_ID'
      
        'and not EXISTS (select nmcl_id from rtl_make_date_exception wher' +
        'e nmcl_id = :nmcl_id)'
      ')')
    Macros = <>
    Left = 464
    Top = 504
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'nmcl_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'nmcl_id'
        ParamType = ptInput
      end>
    object m_Q_chk_hoursSHELF_LIFE: TFloatField
      FieldName = 'SHELF_LIFE'
      Origin = 'TSDBMAIN.NMCL_WEIGHT_SHELF_LIFE.SHELF_LIFE'
    end
    object m_Q_chk_hoursIS_WEIGHT: TFloatField
      FieldName = 'IS_WEIGHT'
    end
  end
  object m_Q_rtl_bill_list: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT b.doc_id ,'
      '       b.doc_number ,'
      '       b.doc_date ,'
      '       b.on_date ,'
      '       b.amount ,'
      '       ppd.sender_id cnt_id,'
      
        '       substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id)' +
        ',1,250) cnt_name,'
      '       ppd.buyer_id mngr_id,'
      
        '       substr(pkg_contractors.fnc_contr_full_name(ppd.buyer_id),' +
        '1,250) mngr_name,'
      '       b.src_org_id,'
      '       o_src.name as src_org_name ,'
      '       '#39'N'#39' ok'
      'FROM rtl_bills b,'
      '  documents d,'
      '  organizations o,'
      '  organizations o_src,  '
      '  prepay_documents ppd'
      'WHERE d.doc_type_id = 188945  '
      '  AND b.doc_id = d.id'
      '  AND b.on_date > sysdate - 10   '
      '  AND o.id  = d.src_org_id'
      '  AND b.src_org_id = 34 '
      '  AND d.fldr_id = 2480           -- ����� "�������"'
      '  AND ppd.doc_id (+) = b.acc_doc_id'
      '  AND b.src_org_id = o_src.id(+)'
      '  AND o.id = SESSION_GLOBALS.get_home_org_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_rtl_bill
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 289
    Top = 560
    object m_Q_rtl_bill_listDOC_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'DOC_ID'
      Origin = 'b.doc_id'
    end
    object m_Q_rtl_bill_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���.'
      FieldName = 'DOC_NUMBER'
      Origin = 'b.doc_number'
      Size = 100
    end
    object m_Q_rtl_bill_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���.'
      FieldName = 'DOC_DATE'
      Origin = 'b.doc_date'
    end
    object m_Q_rtl_bill_listON_DATE: TDateTimeField
      DisplayLabel = '�� ����'
      FieldName = 'ON_DATE'
      Origin = 'b.on_date'
    end
    object m_Q_rtl_bill_listAMOUNT: TFloatField
      DisplayLabel = '�����'
      FieldName = 'AMOUNT'
      Origin = 'b.amount'
    end
    object m_Q_rtl_bill_listCNT_ID: TFloatField
      DisplayLabel = 'ID ����.'
      FieldName = 'CNT_ID'
      Origin = 'ppd.sender_id'
    end
    object m_Q_rtl_bill_listCNT_NAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'CNT_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,250)'
      Size = 250
    end
    object m_Q_rtl_bill_listMNGR_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'MNGR_ID'
      Origin = 'ppd.buyer_id'
    end
    object m_Q_rtl_bill_listMNGR_NAME: TStringField
      DisplayLabel = '��������'
      FieldName = 'MNGR_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(ppd.buyer_id),1,250)'
      Size = 250
    end
    object m_Q_rtl_bill_listSRC_ORG_ID: TFloatField
      DisplayLabel = 'ID ����� �����������'
      FieldName = 'SRC_ORG_ID'
      Origin = 'b.src_org_id'
    end
    object m_Q_rtl_bill_listSRC_ORG_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'SRC_ORG_NAME'
      Origin = 'o_src.name'
      Size = 250
    end
    object m_Q_rtl_bill_listOK: TStringField
      FieldName = 'OK'
      FixedChar = True
      Size = 1
    end
  end
  object m_U_rtl_bill: TMLUpdateSQL
    Left = 209
    Top = 560
  end
  object m_Q_add_items_by_bill: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '   prc_add_income_items_by_bill(:DOC_ID,:BILL_DOC_ID);'
      'END;')
    Macros = <>
    Left = 584
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'BILL_DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_nmcl_addr_list: TMLQuery
    BeforeOpen = m_Q_nmcl_addr_listBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT r.line_id,'
      '       r.nmcl_id,'
      '       ni.name         nmcl_name,'
      '       r.assortment_id,'
      '       a.name          assort_name,'
      '       st.address,'
      '       s.cnt_addr'
      '  FROM rtl_income_items r,'
      '       (SELECT p.nmcl_id, p.assort_id, COUNT(*) cnt_addr'
      '          FROM nmcl_weight_stickers_add_prod p'
      '         GROUP BY p.nmcl_id, p.assort_id) s,'
      '       nomenclature_items ni,'
      '       assortment a,'
      '       nmcl_weight_stickers st'
      ' WHERE r.doc_id = :ID'
      '   AND r.nmcl_id = s.nmcl_id'
      '   AND r.assortment_id = s.assort_id'
      '   AND r.nmcl_id = ni.id'
      '   AND r.assortment_id = a.id'
      '   AND s.cnt_addr > 1'
      '   AND r.nmcl_id = st.nmcl_id'
      '   AND r.assortment_id = st.assort_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 580
    Top = 255
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_nmcl_addr_listLINE_ID: TFloatField
      FieldName = 'LINE_ID'
    end
    object m_Q_nmcl_addr_listNMCL_ID: TFloatField
      DisplayLabel = '�����| ID'
      FieldName = 'NMCL_ID'
    end
    object m_Q_nmcl_addr_listNMCL_NAME: TStringField
      DisplayLabel = '�����| ������������'
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_nmcl_addr_listASSORTMENT_ID: TFloatField
      DisplayLabel = '�����������| ID'
      FieldName = 'ASSORTMENT_ID'
    end
    object m_Q_nmcl_addr_listASSORT_NAME: TStringField
      DisplayLabel = '�����������| ������������'
      FieldName = 'ASSORT_NAME'
      Size = 100
    end
    object m_Q_nmcl_addr_listADDRESS: TStringField
      DisplayLabel = '������� ��. �����'
      FieldName = 'ADDRESS'
      Size = 92
    end
    object m_Q_nmcl_addr_listCNT_ADDR: TFloatField
      DisplayLabel = '���������� �������'
      FieldName = 'CNT_ADDR'
    end
  end
  object m_Q_address_list: TMLQuery
    BeforeOpen = m_Q_address_listBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT t.line_id,'
      '       t.nmcl_id,'
      '       t.assort_id,'
      '       t.shl_meas_unit_id,'
      '       s.name AS shelf_name,'
      '       t.shelf_life,'
      '       t.address'
      '  FROM nmcl_weight_stickers_add_prod t, shelf_life_meas_units s'
      ' WHERE t.shl_meas_unit_id = s.id'
      '   AND t.nmcl_id = :NMCL_ID'
      '   AND t.assort_id = :ASSORT_ID'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 680
    Top = 255
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end>
    object m_Q_address_listLINE_ID: TFloatField
      DisplayLabel = 'LID'
      FieldName = 'LINE_ID'
      Origin = 't.LINE_ID'
    end
    object m_Q_address_listNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.NMCL_WEIGHT_STICKERS_ADD_PROD.NMCL_ID'
    end
    object m_Q_address_listASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Origin = 'TSDBMAIN.NMCL_WEIGHT_STICKERS_ADD_PROD.ASSORT_ID'
    end
    object m_Q_address_listSHL_MEAS_UNIT_ID: TFloatField
      FieldName = 'SHL_MEAS_UNIT_ID'
      Origin = 'TSDBMAIN.NMCL_WEIGHT_STICKERS_ADD_PROD.SHL_MEAS_UNIT_ID'
    end
    object m_Q_address_listSHELF_NAME: TStringField
      DisplayLabel = '���� ��������| ��. ���������'
      FieldName = 'SHELF_NAME'
      Origin = 'S.NAME'
      Size = 50
    end
    object m_Q_address_listSHELF_LIFE: TFloatField
      DisplayLabel = '���� ��������| ����������'
      FieldName = 'SHELF_LIFE'
      Origin = 't.SHELF_LIFE'
    end
    object m_Q_address_listADDRESS: TStringField
      DisplayLabel = '��. �����'
      FieldName = 'ADDRESS'
      Origin = 't.ADDRESS'
      Size = 92
    end
  end
  object m_Q_chg_address: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      ''
      '  UPDATE nmcl_weight_stickers'
      '     SET address = :ADDRESS'
      '   WHERE nmcl_id = :NMCL_ID'
      '     AND assort_id = :ASSORT_ID;'
      ''
      '  UPDATE nmcl_weight_shelf_life'
      
        '     SET shl_meas_unit_id = :SHL_MEAS_UNIT_ID, shelf_life = :SHE' +
        'LF_LIFE'
      '   WHERE nmcl_id = :NMCL_ID'
      '     AND assort_id = :ASSORT_ID;'
      ''
      'END;')
    Left = 685
    Top = 315
    ParamData = <
      item
        DataType = ftString
        Name = 'ADDRESS'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SHL_MEAS_UNIT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SHELF_LIFE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_load_schet: TMLQuery
    BeforeOpen = m_Q_load_schetBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      'b.doc_id, b.acc_doc_id, b.doc_number, b.doc_date, '
      'b.src_org_id src_id, o_src.name src_name, '
      'd.src_org_id dst_id, o_dest.name dst_name'
      
        'from rtl_bills b, documents d, organizations o_src, organization' +
        's o_dest'
      'WHERE d.id = b.doc_id'
      'AND o_src.id = b.src_org_id'
      'AND o_dest.id = d.src_org_id'
      'AND b.acc_doc_id = :ID')
    Macros = <>
    Left = 36
    Top = 548
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_load_schetDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Origin = 'TSDBMAIN.RTL_BILLS.DOC_ID'
    end
    object m_Q_load_schetACC_DOC_ID: TFloatField
      FieldName = 'ACC_DOC_ID'
      Origin = 'TSDBMAIN.RTL_BILLS.ACC_DOC_ID'
    end
    object m_Q_load_schetDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Origin = 'TSDBMAIN.RTL_BILLS.DOC_NUMBER'
      Size = 100
    end
    object m_Q_load_schetDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
      Origin = 'TSDBMAIN.RTL_BILLS.DOC_DATE'
    end
    object m_Q_load_schetSRC_ID: TFloatField
      FieldName = 'SRC_ID'
      Origin = 'TSDBMAIN.RTL_BILLS.SRC_ORG_ID'
    end
    object m_Q_load_schetSRC_NAME: TStringField
      FieldName = 'SRC_NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
    object m_Q_load_schetDST_ID: TFloatField
      FieldName = 'DST_ID'
      Origin = 'TSDBMAIN.DOCUMENTS.SRC_ORG_ID'
    end
    object m_Q_load_schetDST_NAME: TStringField
      FieldName = 'DST_NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_Q_set_schet_acc: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'UPDATE rtl_bills SET acc_doc_id = :ACC WHERE doc_id = :ID AND ac' +
        'c_doc_id IS NULL')
    Macros = <>
    Left = 116
    Top = 548
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ACC'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
  end
  object m_Q_lines_diff: TMLQuery
    BeforeOpen = m_Q_lines_diffBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT id,line_id,nmcl_id,nmcl_name,meas_name,assortment_id,asso' +
        'rtment_name,in_price,out_price,arp,schet,diff,out_amount,src_amo' +
        'unt'
      'FROM ('
      '    SELECT '
      '    MAX(v.id) as id,'
      '    MAX(v.line_id) as line_id,'
      '    v.nmcl_id,'
      '    v.nmcl_name,'
      '    v.meas_name,'
      '    v.assortment_id,'
      '    v.assortment_name,'
      '    MAX(v.in_price) as in_price,'
      '    MAX(v.out_price) as out_price,'
      '    SUM(CASE WHEN v.type = 1 THEN v.qnt END) as arp,'
      '    SUM(CASE WHEN v.type = 2 THEN v.qnt END) as schet,'
      
        '    round(NVL(SUM(CASE WHEN v.type = 1 THEN v.qnt END),0) - NVL(' +
        'SUM(CASE WHEN v.type = 2 THEN v.qnt END),0),2) diff,'
      '    MAX(v.out_amount) as out_amount,'
      '    MAX(v.src_amount) as src_amount'
      '    FROM ('
      '        select'
      '          ii.doc_id id,'
      '          ii.line_id line_id,'
      '          ii.nmcl_id,'
      '          ni.name nmcl_name,'
      '          mu.name meas_name,'
      '          ii.assortment_id,'
      '          a.name assortment_name,'
      '          ii.in_price,'
      '          ii.out_price,'
      '          ii.qnt,'
      '          ii.out_price*ii.qnt out_amount,'
      '          ii.src_amount,'
      '          1 type'
      '        from'
      '          rtl_income_items ii,'
      '          nomenclature_items ni,'
      '          measure_units mu,'
      '          assortment a'
      '        where ii.doc_id = :ID'
      '          and ni.id = ii.nmcl_id'
      '          and mu.id = ni.meas_unit_id'
      '          and ii.stored = 1'
      '          and a.id = ii.assortment_id'
      '        UNION ALL'
      '         SELECT '
      '         TO_NUMBER(NULL) id,'
      '         TO_NUMBER(NULL) line_id,'
      '         i.nmcl_id, '
      '         ni.name nmcl_name,'
      '          mu.name meas_name,'
      '          i.assortment_id,'
      '          a.name assortment_name,'
      '          TO_NUMBER(NULL) in_price,'
      '          TO_NUMBER(NULL) out_price,'
      '          i.qnt,'
      '          TO_NUMBER(NULL) out_amount,'
      '          TO_NUMBER(NULL) src_amount,'
      '          2 type'
      '         FROM rtl_bills b, '
      '          rtl_bill_items i, '
      '          rtl_incomes r, '
      '          nomenclature_items ni,'
      '          measure_units mu,'
      '          assortment a'
      '         WHERE i.doc_id = b.doc_id'
      '         AND r.acc_doc_id = b.acc_doc_id'
      '         AND r.doc_id = :ID'
      '         AND ni.id = i.nmcl_id'
      '         AND mu.id = ni.meas_unit_id'
      '         AND a.id = i.assortment_id'
      '    ) v'
      '    GROUP BY '
      '    v.nmcl_id,'
      '    v.nmcl_name,'
      '    v.meas_name,'
      '    v.assortment_id,'
      '    v.assortment_name'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 120
    Top = 600
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_lines_diffID: TFloatField
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_lines_diffLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'line_id'
    end
    object m_Q_lines_diffNMCL_ID: TFloatField
      DisplayLabel = '���'
      FieldName = 'NMCL_ID'
      Origin = 'nmcl_id'
    end
    object m_Q_lines_diffNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'nmcl_name'
      Size = 100
    end
    object m_Q_lines_diffMEAS_NAME: TStringField
      DisplayLabel = '��. ���������'
      FieldName = 'MEAS_NAME'
      Origin = 'meas_name'
      Size = 30
    end
    object m_Q_lines_diffASSORTMENT_ID: TFloatField
      DisplayLabel = '��� ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'assortment_id'
    end
    object m_Q_lines_diffASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'assortment_name'
      Size = 100
    end
    object m_Q_lines_diffIN_PRICE: TFloatField
      DisplayLabel = '����*'
      FieldName = 'IN_PRICE'
      Origin = 'in_price'
    end
    object m_Q_lines_diffOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'out_price'
    end
    object m_Q_lines_diffARP: TFloatField
      DisplayLabel = '���-��|���'
      FieldName = 'ARP'
      Origin = 'arp'
    end
    object m_Q_lines_diffSCHET: TFloatField
      DisplayLabel = '���-��|����'
      FieldName = 'SCHET'
      Origin = 'schet'
    end
    object m_Q_lines_diffDIFF: TFloatField
      DisplayLabel = '���-��|�����������'
      FieldName = 'DIFF'
      Origin = 'diff'
    end
    object m_Q_lines_diffOUT_AMOUNT: TFloatField
      DisplayLabel = '�����'
      FieldName = 'OUT_AMOUNT'
      Origin = 'out_amount'
    end
    object m_Q_lines_diffSRC_AMOUNT: TFloatField
      DisplayLabel = '����� ���'
      FieldName = 'SRC_AMOUNT'
      Origin = 'src_amount'
    end
  end
  object m_SP_check_schet: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'RETAIL.FNC_CHECK_INCOME_SCHET'
    Left = 40
    Top = 600
    ParamData = <
      item
        DataType = ftString
        Name = 'Result'
        ParamType = ptResult
      end
      item
        DataType = ftFloat
        Name = 'V_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'V_TYPE_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_our_cnt: TMLQuery
    BeforeOpen = m_Q_our_cntBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select r.cnt_id from rtl_bill_require r WHERE r.cnt_id = :CNT_ID')
    Macros = <>
    Left = 208
    Top = 608
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CNT_ID'
        ParamType = ptInput
      end>
    object m_Q_our_cntCNT_ID: TFloatField
      FieldName = 'CNT_ID'
    end
  end
  object m_Q_sign: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'INSERT INTO rtl_income_sign(DOC_ID)'
      'VALUES(:DOC_ID)')
    Macros = <>
    Left = 288
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DOC_ID'
        ParamType = ptUnknown
      end>
    object FloatField1: TFloatField
      FieldName = 'CNT_ID'
    end
  end
  object m_Q_fict: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select to_number(null) as par from dual')
    UpdateObject = m_U_fict
    Macros = <>
    Left = 464
    Top = 568
    object m_Q_fictPAR: TFloatField
      FieldName = 'PAR'
    end
  end
  object m_U_fict: TMLUpdateSQL
    IgnoreRowsAffected = True
    IgnoreApply = True
    Left = 544
    Top = 568
  end
  object m_Q_upd_amt: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'update rtl_income_items'
      'set src_amount = src_amount + :amount,'
      '    in_price = null'
      'where doc_id = :doc_id'
      
        'and line_id = (select max(line_id) keep (dense_rank last ordr by' +
        ' qnt) from rtl_income_items'
      '                     where doc_id = :doc_id)')
    Macros = <>
    Left = 624
    Top = 568
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'amount'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DOC_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'doc_id'
        ParamType = ptUnknown
      end>
  end
  object m_Q_sublines: TMLQuery
    BeforeOpen = m_Q_sublinesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '
      '  doc_id as id,'
      '  line_id,'
      '  sub_line_id,'
      '  pref_alccode,'
      '  pdf_alccode'
      'FROM rtl_income_subitems ris'
      '  where ris.doc_id = :doc_id'
      '    AND ris.line_id = :line_id')
    Macros = <>
    Left = 584
    Top = 128
    ParamData = <
      item
        DataType = ftInteger
        Name = 'doc_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'line_id'
        ParamType = ptInput
      end>
    object m_Q_sublinesID: TFloatField
      FieldName = 'ID'
      Origin = 'doc_id'
    end
    object m_Q_sublinesLINE_ID: TFloatField
      DisplayLabel = 'LID'
      FieldName = 'LINE_ID'
      Origin = 'LINE_ID'
    end
    object m_Q_sublinesSUB_LINE_ID: TFloatField
      DisplayLabel = 'SLID'
      FieldName = 'SUB_LINE_ID'
      Origin = 'SUB_LINE_ID'
    end
    object m_Q_sublinesPREF_ALCCODE: TStringField
      DisplayLabel = '�������'
      DisplayWidth = 30
      FieldName = 'PREF_ALCCODE'
      Origin = 'PREF_ALCCODE'
      Size = 50
    end
    object m_Q_sublinesPDF_ALCCODE: TStringField
      DisplayLabel = '�������� �����'
      DisplayWidth = 50
      FieldName = 'PDF_ALCCODE'
      Origin = 'PDF_ALCCODE'
      Size = 255
    end
  end
  object m_Q_sub_del: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_sublinesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'DELETE FROM rtl_income_subitems ris'
      '  WHERE ris.doc_id = :doc_id'
      '    AND ris.line_id = :line_id'
      '    AND sub_line_id = :sub_line_id')
    Macros = <>
    Left = 664
    Top = 128
    ParamData = <
      item
        DataType = ftInteger
        Name = 'doc_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'line_id'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'sub_line_id'
        ParamType = ptInput
      end>
  end
  object m_Q_sub_ins: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      '  INSERT INTO rtl_income_subitems(  '
      '    doc_id,'
      '    line_id,'
      '    sub_line_id,'
      '    pref_alccode,'
      '    pdf_alccode'
      '  )'
      '  VALUES('
      '    :doc_id,'
      '    :line_id,'
      '    sq_rtl_income_subitems.nextval,'
      '    pkg_bar_code.get_egais_alccode( :src_pdf_alccode ),'
      '    pkg_bar_code.fnc_get_PDF_CODE( :src_pdf_alccode )'
      '  )')
    Macros = <>
    Left = 736
    Top = 128
    ParamData = <
      item
        DataType = ftInteger
        Name = 'doc_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'line_id'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'src_pdf_alccode'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'src_pdf_alccode'
        ParamType = ptInput
      end>
  end
  object m_SP_CREATE_INC: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'RETAIL.PRC_ADD_INCOME_BY_INCOME'
    Left = 384
    Top = 592
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P_BASE_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_NEW_DOC_ID'
        ParamType = ptOutput
      end>
  end
  object m_SP_CREATE_ED_INC: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'RETAIL.PRC_ADD_EDIT_INC_RTL_BY_INCOME'
    Left = 696
    Top = 592
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P_BASE_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_NEW_DOC_ID'
        ParamType = ptOutput
      end>
  end
end
