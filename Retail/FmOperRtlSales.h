//---------------------------------------------------------------------------

#ifndef FmOperRtlSalesH
#define FmOperRtlSalesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmOperRtlSales.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFmOperation.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include <Dialogs.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "autotree.hpp"
#include "dbtree.hpp"
#include "MLCustomContainer.h"
#include "TSFormContainer.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TFOperRtlSales : public TTSFOperation
{
__published:	// IDE-managed Components
    TToolButton *ToolButton1;
    TMLDBGrid *m_DBG_main;
        TAction *m_ACT_view;
        TDataSource *m_DS_main;
        TSplitter *Splitter1;
        TToolBar *m_TB_main_control_buttons_list;
        TSpeedButton *m_SBTN_view;
        TMLDBGrid *MLDBGrid1;
        TSplitter *m_SP_bottom;
        TDataSource *m_DS_checks;
        TDBLookupComboBox *m_DBLCB_organizations;
        TToolButton *m_TB_1;
        TSpeedButton *m_SBTN_period;
        TToolButton *m_TB_2;
        TCheckBox *m_CB_tabac;
        TAction *m_ACT_period;
        TDataSource *m_DS_orgs;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_periodExecute(TObject *Sender);
        void __fastcall m_CB_tabacClick(TObject *Sender);
        void __fastcall m_DBLCB_organizationsClick(TObject *Sender);
        void __fastcall m_ACT_viewExecute(TObject *Sender);
        void __fastcall m_DBG_mainDblClick(TObject *Sender);
        
private:	// User declarations
  TDOperRtlSales* m_dm;

public:		// User declarations
    __fastcall TFOperRtlSales(TComponent* p_owner, TTSDOperation *p_dm);
    __fastcall ~TFOperRtlSales(){};
};
//---------------------------------------------------------------------------
#endif
