//---------------------------------------------------------------------------

#ifndef FmAlcoJournalNewH
#define FmAlcoJournalNewH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMOPERATION.h"
#include "DmAlcoJournalNew.h"
#include "TSFmOperation.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Db.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "TSFormControls.h"
#include <Dialogs.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "MLDBPanel.h"
#include "TSReportControls.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TFAlcoJournalNew : public TTSFOperation
{
__published:	// IDE-managed Components
    TDataSource *m_DS_main;
    TAction *m_ACT_complete;
    TAction *m_ACT_uncomplete;
    TAction *m_ACT_save;
    TToolButton *ToolButton1;
    TDataSource *m_DS_taxpayer;
    TMLLov *m_LOV_taxpayer;
    TToolButton *ToolButton2;
    TDataSource *m_DS_taxpayer_fict;
    TAction *m_ACT_save_dll;
    TMLSetBeginEndDate *m_ACT_set_begin_end_date;
    TDataSource *m_DS_org;
    TAction *m_ACT_generate;
    TAction *m_ACT_reload_list;
    TPanel *m_P_3;
    TToolBar *m_TB_reload_list;
    TBitBtn *m_SBTN_reload_list;
    TToolButton *btn1;
    TSpeedButton *m_SB__SBTN_set_date;
    TMLDBGrid *m_DBG_main1;
	TMLDBPanel *MLDBPanel1;
	TLabel *m_L_2;
	TMLLovListView *m_LV_taxpayer;
	TLabel *m_L_taxpayer;
	TLabel *m_L_address;
	TDBEdit *m_DBE_inn;
	TDBEdit *m_DBE_kpp;
	TLabel *m_L_inn;
	TLabel *m_L_kpp;
	TDataSource *m_DS_address;
	TDBMemo *m_DBM_address;
	TAction *m_ACT_print_form;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
    void __fastcall m_DBG_main1GetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State);
    void __fastcall m_LOV_taxpayerAfterApply(TObject *Sender);
	void __fastcall m_ACT_print_formExecute(TObject *Sender);

public:
  __fastcall TFAlcoJournalNew(TComponent* p_owner, TTSDOperation *p_dm_operation);

private:
    TDAlcoJournalNew *m_dm;
    TTSReportControl alcoReportControl_;
};
//---------------------------------------------------------------------------
#endif
