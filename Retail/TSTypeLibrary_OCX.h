// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// C++ TLBWRTR : $Revision:   1.134.1.41  $
// File generated on 15.07.2017 13:47:27 from Type Library described below.

// ************************************************************************ //
// Type Lib: L:\TSTypeLibrary\TSTypeLibrary.tlb (1)
// IID\LCID: {92D1487E-A494-401D-B723-2667DA9E97C5}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Parent TypeLibrary:
//   (0) v1.0 TSRetail, (P:\Modules\Retail\TSRetail.tlb)
// ************************************************************************ //
#ifndef   __TSTypeLibrary_OCX_h__
#define   __TSTypeLibrary_OCX_h__

#pragma option push -b -w-inl

#include <utilcls.h>
#if !defined(__UTILCLS_H_VERSION) || (__UTILCLS_H_VERSION < 0x0500)
//
// The code generated by the TLIBIMP utility or the Import|TypeLibrary 
// and Import|ActiveX feature of C++Builder rely on specific versions of
// the header file UTILCLS.H found in the INCLUDE\VCL directory. If an 
// older version of the file is detected, you probably need an update/patch.
//
#error "This file requires a newer version of the header UTILCLS.H" \
       "You need to apply an update/patch to your copy of C++Builder"
#endif
#include <olectl.h>
#include <ocidl.h>
#if !defined(_NO_VCL)
#include <stdvcl.hpp>
#endif  //   _NO_VCL
#include <ocxproxy.h>

#include "TSTypeLibrary_TLB.h"
namespace Tstypelibrary_tlb
{

// *********************************************************************//
// HelpString: TSTypeLibrary
// Version:    1.4
// *********************************************************************//

};     // namespace Tstypelibrary_tlb

#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using  namespace Tstypelibrary_tlb;
#endif

#pragma option pop

#endif // __TSTypeLibrary_OCX_h__
