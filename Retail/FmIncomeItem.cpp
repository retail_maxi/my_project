//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <cmath>
#include "FmIncomeItem.h"
#include "FmGetBuhDoc.h"
#include "Barcode.h"
#include "FmActTCDSet.h"
#include "FmViewBuhDoc.h"
#include "MLFuncs.h"
#include "FmAccountSet.h"
#include "TSErrors.h"
#include "FmGetShetDoc.h"
#include "FmTotalAmt.h"
#include "FmCheckSum.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "TSReportContainer"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_get_acc_docUpdate(TObject *Sender)
{
  m_ACT_get_acc_doc->Enabled = m_dm->UpdateRegimeItem != urView && m_dm->m_Q_linesID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_get_acc_docExecute(TObject *Sender)
{
  TFGetBuhDoc* f = new TFGetBuhDoc(this,m_dm->m_DB_main->Handle,m_dm);
  try
  {
    f->ShowModal();
    if (f->PressOK)
    {
      if (m_dm->m_Q_dup_acc_doc->Active) m_dm->m_Q_dup_acc_doc->Close();
      m_dm->m_Q_dup_acc_doc->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_dup_acc_doc->ParamByName("ACC_DOC_ID")->AsInteger = f->m_Q_listID->AsInteger;
      m_dm->m_Q_dup_acc_doc->Open();
      if (m_dm->m_Q_dup_acc_doc->RecordCount > 0) {
        MessageDlg("������������� �������� ��� ������������ � ���� ������� �" + m_dm->m_Q_dup_acc_docDOC_NUMBER->AsString + " �� " + m_dm->m_Q_dup_acc_docDOC_DATE->AsString,mtWarning,TMsgDlgButtons() << mbOK,0);
        m_dm->m_Q_dup_acc_doc->Close();
        return;
      }
      m_dm->m_Q_dup_acc_doc->Close();
      if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();

      //���. ��������
      m_dm->m_Q_itemACC_DOC_ID->AsInteger = f->m_Q_listID->AsInteger;
      m_dm->m_Q_itemACC_DOC_NUMBER->AsString = f->m_Q_listPPD_DOC_NUMBER->AsString;
      m_dm->m_Q_itemCNT_NAME->AsString = f->m_Q_listPPD_SENDER->AsString;
      m_dm->m_Q_itemSENDER_ID->AsInteger = f->m_Q_listSENDER_ID->AsInteger;
      m_dm->m_Q_itemACC_DOC_DATE->AsDateTime = f->m_Q_listPPD_ISSUED_DATE->AsDateTime;
      m_dm->m_Q_itemACC_DOC_AMOUNT->AsFloat = f->m_Q_listPPD_AMOUNT->AsFloat;
      m_dm->m_Q_itemACC_NOTE->AsString = f->m_Q_listPPD_NOTE->AsString;
      m_dm->m_Q_itemMNGR_NOTE->AsString = f->m_Q_listM_NOTE->AsString;
      m_dm->m_Q_itemCONTRACT_NOTE->AsString = f->m_Q_listCONTRACT_NOTE->AsString;

      //����
      //������� ���� ���, ����� �� ����������� ����� ������
      if(m_dm->m_Q_load_schet->Active) m_dm->m_Q_load_schet->Close();
      m_dm->m_Q_load_schet->Open();
      if(m_dm->m_Q_load_schetDOC_ID->IsNull)
      {
        m_dm->m_Q_itemSCHT_DOC_ID->Clear();
      }
      else
      {
        m_dm->m_Q_itemSCHT_DOC_ID->AsInteger = m_dm->m_Q_load_schetDOC_ID->AsInteger;
      }

      m_dm->m_Q_itemSCHT_DOC_NUMBER->AsString = m_dm->m_Q_load_schetDOC_NUMBER->AsString;
      m_dm->m_Q_itemSCHT_DOC_DATE->AsDateTime = m_dm->m_Q_load_schetDOC_DATE->AsDateTime;
      m_dm->m_Q_itemSCHT_SRC_ONAME->AsString = m_dm->m_Q_load_schetSRC_NAME->AsString;
      m_dm->m_Q_itemSCHT_DST_ONAME->AsString = m_dm->m_Q_load_schetDST_NAME->AsString;

      m_dm->m_Q_item->Post();

      TFIncomeItem::SchetUpd();
    }
  }
  __finally
  {
    if (f) delete f;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_DTP_inc_started_timeChange(TObject *Sender)
{
  if (!m_dm->m_Q_itemINC_STARTED->IsNull)
  {
    if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();
    m_dm->m_Q_itemINC_STARTED->AsDateTime = int(m_dm->m_Q_itemINC_STARTED->AsDateTime) +
                                            m_DTP_inc_started_time->DateTime - int(m_DTP_inc_started_time->DateTime);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_DTP_inc_finished_timeChange(TObject *Sender)
{
  if (!m_dm->m_Q_itemINC_FINISHED->IsNull)
  {
    if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();
    m_dm->m_Q_itemINC_FINISHED->AsDateTime = int(m_dm->m_Q_itemINC_FINISHED->AsDateTime) +
                                             m_DTP_inc_finished_time->DateTime - int(m_DTP_inc_finished_time->DateTime);
  }
}
//---------------------------------------------------------------------------

AnsiString __fastcall TFIncomeItem::BeforeItemApply(TWinControl **p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete) return "";

  if (m_dm->m_Q_itemGR_DEP_ID->IsNull)
  {
    *p_focused = m_LLV_gdp;
    return "�������� �����!";
  }

  if (m_dm->m_Q_itemACC_DOC_ID->IsNull)
  {
    *p_focused = m_BBTN_get_acc_doc;
    return "������� ������������� ��������!";
  }

  if (m_dm->m_Q_itemINC_STARTED->IsNull)
  {
    *p_focused = m_DBDE_inc_started_date;
    return "������� ���� ������ ������������!";
  }

  if (m_dm->m_Q_itemINC_FINISHED->IsNull)
  {
    *p_focused = m_DBDE_inc_finished_date;
    return "������� ���� ��������� ������������!";
  }

  if (!m_dm->m_Q_itemINC_HOUR->IsNull && m_dm->m_Q_itemINC_HOUR->AsInteger > 24)
  {
    *p_focused = m_DBE_inc_hour;
    return "����� �������� �� ������ ��������� 24 �.!";
  }

  if (!m_dm->m_Q_itemINC_MINUTE->IsNull && m_dm->m_Q_itemINC_MINUTE->AsInteger > 60)
  {
    *p_focused = m_DBE_inc_minute;
    return "����� �������� �� ������ ��������� 60 ���.!";
  }

  if (m_dm->m_Q_itemRECEIVER_ID->IsNull)
  {
    *p_focused = m_LLV_receiver;
    return "�������� �����������!";
  }

  if (m_DTP_inc_started_time->OnChange) m_DTP_inc_started_time->OnChange(this);
  if (m_DTP_inc_finished_time->OnChange) m_DTP_inc_finished_time->OnChange(this);

  //�������� ������
  if (m_dm->UpdateRegimeItem != urView)
  {
    m_dm->m_Q_set_schet_acc->Prepare();
    m_dm->m_Q_set_schet_acc->ParamByName("ACC")->AsInteger = m_dm->m_Q_itemACC_DOC_ID->AsInteger;
    m_dm->m_Q_set_schet_acc->ParamByName("ID")->AsInteger = m_dm->m_Q_itemSCHT_DOC_ID->AsInteger;
    m_dm->m_Q_set_schet_acc->ExecSQL();
    m_dm->m_Q_set_schet_acc->UnPrepare();
  }

  return "";
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::FormShow(TObject *Sender)
{
  TTSFDocumentWithLines::FormShow(Sender);

  m_DTP_inc_started_time->Time = m_dm->m_Q_itemINC_STARTED->AsFloat - int(m_dm->m_Q_itemINC_STARTED->AsDateTime);
  m_DTP_inc_finished_time->Time = m_dm->m_Q_itemINC_FINISHED->AsFloat - int(m_dm->m_Q_itemINC_FINISHED->AsDateTime);
 	m_DTP_inc_started_time->Enabled = m_dm->UpdateRegimeItem != urView;
 	m_DTP_inc_finished_time->Enabled = m_dm->UpdateRegimeItem != urView;

  m_CB_type->ItemIndex = m_dm->CurrLinesKind;

  //��������� ����������� ��������� ������ � ������, �������������� "����������", ��. ��� �������� ���������
  m_LLV_receiver->Enabled = m_dm->UpdateRegimeItem == urInsert ||
                            m_dm->UpdateRegimeItem == urEdit &&
                            m_dm->m_Q_folderFLDR_NEXT_ID->AsInteger == m_dm->FldrCnf;

  m_SBTN_insert->Visible = true;
  m_BBTN_save->Visible = true;

  m_P_spec->ActivePageIndex = 0;

  if(m_dm->m_Q_our_cnt->Active) m_dm->m_Q_our_cnt->Close();
  m_dm->m_Q_our_cnt->Open();

  TFIncomeItem::SchetUpd();

}

void __fastcall TFIncomeItem::SchetUpd()
{
  m_P_schet->Visible = !m_dm->m_Q_our_cntCNT_ID->IsNull || !m_dm->m_Q_itemSCHT_DOC_ID->IsNull;
  m_TS_diff->TabVisible = m_P_schet->Visible;
  if(m_P_schet->Visible)
  {
    m_P_header->Height = 311;
  }
  else
  {
    m_P_header->Height = 279;
  }
  if(m_dm->m_Q_lines_diff->Active) m_dm->m_Q_lines_diff->Close();
  TFIncomeItem::m_P_specChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_CB_typeClick(TObject *Sender)
{
  m_dm->CurrLinesKind = (TDIncome::LineKind)m_CB_type->ItemIndex;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_set_not_include_act_verificationUpdate(TObject *Sender)
{
  m_ACT_set_not_include_act_verification->Enabled =
    !m_dm->m_Q_linesID->IsNull &&
    //�������� ������ � ����� "��������"
    m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrChk && !m_dm->m_Q_linesSTORED->AsInteger &&
    m_can_choose_ver_option && !m_dm->m_Q_linesNOT_INCLUDE_ACT_VERIF->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_set_not_include_act_verificationExecute(TObject *Sender)
{
 //  m_dm->SetVerOption(true);

  SetParamLines(true);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_set_include_act_verificationUpdate(TObject *Sender)
{
  m_ACT_set_include_act_verification->Enabled =
    !m_dm->m_Q_linesID->IsNull &&
    //�������� ������ � ����� "��������"
    m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrChk && !m_dm->m_Q_linesSTORED->AsInteger &&
    m_can_choose_ver_option && m_dm->m_Q_linesNOT_INCLUDE_ACT_VERIF->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_set_include_act_verificationExecute(TObject *Sender)
{
 // m_dm->SetVerOption(false);
 SetParamLines(false);
}
//---------------------------------------------------------------------------
void __fastcall TFIncomeItem::SetParamLines(bool p_value)
{

 if (m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->FldrChk) throw ETSError("�������� ������� ����� ������ � ����� \"��������\"");
  if (m_DBG_liness->SelectedRows->Count == 0 && !m_dm->m_Q_linesSTORED->AsInteger &&
              m_dm->m_Q_linesNOT_INCLUDE_ACT_VERIF->AsInteger != p_value )
      m_dm->SetDocParam(m_DBG_liness->DataSource->DataSet->FieldByName("ID")->AsInteger,
                    m_DBG_liness->DataSource->DataSet->FieldByName("LINE_ID")->AsInteger,
                    m_dm->ParamNotIncludeActVerif,int(p_value));
  else
  {
   for (int i = 0; i < m_DBG_liness->SelectedRows->Count; i++ )
   {
   m_DBG_liness->DataSource->DataSet->GotoBookmark((void *)m_DBG_liness->SelectedRows->Items[i].c_str());

    if (!m_dm->m_Q_linesSTORED->AsInteger && m_dm->m_Q_linesNOT_INCLUDE_ACT_VERIF->AsInteger != p_value )
    {
     m_dm->SetDocParam(m_DBG_liness->DataSource->DataSet->FieldByName("ID")->AsInteger,
                    m_DBG_liness->DataSource->DataSet->FieldByName("LINE_ID")->AsInteger,
                    m_dm->ParamNotIncludeActVerif,int(p_value));
     }
   }
  }
  m_dm->m_Q_lines->Close();
  m_dm->m_Q_lines->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_appendUpdate(TObject *Sender)
{
  m_ACT_append->Enabled =  m_dm->UpdateRegimeItem != urView &&
                          m_dm->CurrLinesKind != TDIncome::lkAny &&
                          m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrNew;   
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_BCS_itemBegin(TObject *Sender)
{
  m_old_lines_grid_options = m_DBG_liness->OptionsML;
  m_DBG_liness->OptionsML = TMLGridOptions() << goWithoutFind;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_BCS_itemEnd(TObject *Sender)
{
  try
  {
    m_dm->LastBarCode = m_BCS_item->GetResult();
    m_ACT_append->Execute();
  }
  __finally
  {
    m_DBG_liness->OptionsML = m_old_lines_grid_options;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_load_act_tcdUpdate(TObject *Sender)
{
  m_ACT_load_act_tcd->Enabled = m_dm->UpdateRegimeItem != urView &&
                                m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrNew;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_load_act_tcdExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  ExecActTCDSetDlg(this,m_dm);

  m_DTP_inc_started_time->Time = m_dm->m_Q_itemINC_STARTED->AsFloat - int(m_dm->m_Q_itemINC_STARTED->AsDateTime);
 	m_DTP_inc_finished_time->Time = m_dm->m_Q_itemINC_FINISHED->AsFloat - int(m_dm->m_Q_itemINC_FINISHED->AsDateTime);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_clear_act_tcdUpdate(TObject *Sender)
{
  m_ACT_clear_act_tcd->Enabled = m_dm->UpdateRegimeItem != urView &&
                                 m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrNew;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_clear_act_tcdExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  if (MessageDlg(
        "��� ������� ������ ����� ������� ������������ ���������. ����������?",
        mtWarning,TMsgDlgButtons() << mbYes << mbNo,0
      ) == mrYes)
  {
    m_dm->m_Q_clear_act_tcd->ParamByName("P_DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_clear_act_tcd->ExecSQL();
    m_dm->RefreshItemDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_DBG_linesEnter(TObject *Sender)
{
  m_BCS_item->Active = true;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_DBG_linesExit(TObject *Sender)
{
  m_BCS_item->Active = false;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_set_corr_amtUpdate(TObject *Sender)
{
  m_ACT_set_corr_amt->Enabled = m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrChk && m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_set_corr_amtExecute(TObject *Sender)
{
  AnsiString InputString = FloatToStr(m_dm->m_Q_itemCORR_AMT->AsFloat);
  double res;
  bool ok = false;
  while (!ok)
  {
    InputString = InputBox("������� �����", "", InputString);
    try
    {
      res = MLDRound(StrToFloat(InputString),2);
      ok = true;
    }
    catch(...) {};
  }

  if (m_dm->m_Q_itemCORR_AMT->AsFloat != res)
  {
    if (m_dm->m_Q_item->State != dsEdit) m_dm->m_Q_item->Edit();
    m_dm->m_Q_itemCORR_AMT->AsFloat = res;
    m_dm->m_Q_item->Post();
    m_dm->ApplyUpdatesItem();
    m_dm->RefreshItemDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_view_sourceUpdate(TObject *Sender)
{
    m_ACT_view_source->Enabled = (!m_dm->m_Q_itemACC_DOC_ID->IsNull);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_view_sourceExecute(TObject *Sender)
{
    TFViewBuhDoc *f = new TFViewBuhDoc(
        this,
        m_dm->m_DB_main->Handle,
        m_dm->m_Q_itemACC_DOC_ID->AsInteger
    );
    try
    {
        f->ShowModal();
    }
    __finally
    {
        if (f) delete f;
    }

}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_rtl_bill_linkExecute(TObject *Sender)
{
   if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  ExecAccountSetDlg(this,m_dm);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_rtl_bill_linkUpdate(TObject *Sender)
{
   m_ACT_rtl_bill_link->Enabled = m_dm->UpdateRegimeItem != urView &&
                                  m_can_load_rtl_bill ;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_next_fldrExecute(TObject *Sender)
{
    if (m_dm->FldrNew == m_dm->m_Q_itemFLDR_ID->AsInteger) // �������� ������ �������
    {
        //�������� �����������
        m_dm->m_SP_check_schet->ParamByName("V_DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
        m_dm->m_SP_check_schet->ParamByName("V_TYPE_ID")->AsInteger = 1;
        m_dm->m_SP_check_schet->ExecProc();
        AnsiString res = m_dm->m_SP_check_schet->ParamByName("Result")->AsString.Trim();
        if(res.Length() > 1)
        {
          if (Application->MessageBox(res.c_str(),
                                  Application->Title.c_str(),
                                  MB_ICONQUESTION | MB_YESNO) == IDNO )
          {
            return;
          }
        }

        m_dm->m_Q_nmcl_addr_list->Open();
        if ( m_dm->m_Q_nmcl_addr_list->RecordCount > 0 )
        {
            std::auto_ptr<TFNmclAddressList> f (new TFNmclAddressList(this, m_dm));
            f->ShowModal();
        }
        m_dm->m_Q_nmcl_addr_list->Close();

        if(m_dm->m_Q_itemAMOUNT_NON_STORED->AsFloat > 0 &&
           m_dm->m_Q_itemNO_TORG2->AsInteger == 0)
        {
           if (Application->MessageBox("����������� ����� � ��������� � ����-2?",
                                   Application->Title.c_str(),
                                   MB_ICONQUESTION | MB_YESNO) == IDNO )
           {
             return;
           }
           else
           {
             TPrinter* prn = Printer();
             m_TSRC_blt_rep->CreateObject();
             try
             {     
                prn->Copies = 1;
                m_TSRC_blt_rep->ReportImpl->Load("REP_TORG2_RS_ARP", stDatabase);
                m_TSRC_blt_rep->ReportImpl->CanPrepare = true;
                m_TSRC_blt_rep->ReportImpl->CanPrint = true;
                m_TSRC_blt_rep->ReportImpl->CanSave = true;
                m_TSRC_blt_rep->ReportImpl->CanDesign = false;
                m_TSRC_blt_rep->ParamsImpl->SetParam("ID", m_dm->m_Q_itemID->AsInteger);
                m_TSRC_blt_rep->ReportImpl->Printer = prn->Printers->Strings[prn->PrinterIndex].c_str();
                m_TSRC_blt_rep->ReportImpl->Copyes = 3;
                m_TSRC_blt_rep->ReportImpl->Prepare();
                m_TSRC_blt_rep->ReportImpl->PrintPrepared();
             }
              __finally
             {
                    m_TSRC_blt_rep->DestroyObject();
             }
           }
        }
      if (m_dm->m_Q_itemSENDER_ID->AsInteger != 2069191 && m_dm->m_Q_itemSENDER_ID->AsInteger != 2000906){
        TFmTotalAmt *f = new TFmTotalAmt(this,m_dm);
        if (f->ShowModal() == IDOK){
          double par = f->Par;
          if (m_dm->m_Q_itemAMOUNT_IN->AsFloat - par != 0)
            if (fabs(m_dm->m_Q_itemAMOUNT_IN->AsFloat - par) < 5){
              m_dm->m_Q_upd_amt->ParamByName("doc_id")->AsInteger = m_dm->m_Q_itemID->AsInteger;
              m_dm->m_Q_upd_amt->ParamByName("amount")->AsFloat = par - m_dm->m_Q_itemAMOUNT_IN->AsFloat;
              m_dm->m_Q_upd_amt->ExecSQL();
            } else {
              TFmCheckSum *fm = new TFmCheckSum(this, m_dm, AnsiString(MLDRound(fabs(m_dm->m_Q_itemAMOUNT_IN->AsFloat - par),2)));
              if (fm->ShowModal() == IDOK) return;
              else{
                Close();
                return;
              }
            }
        } else return;
      }
    }
    TTSFDocumentWithLines::m_ACT_next_fldrExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_get_schet_docExecute(TObject *Sender)
{
  TFGetSchetDoc* f = new TFGetSchetDoc(this,m_dm->m_DB_main->Handle,m_dm);
  try
  {
    f->ShowModal();
    if (f->PressOK)
    {
      //����� �� ��������, ���� ��������� ���� ��� ��������?

      if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();

      m_dm->m_Q_itemSCHT_DOC_ID->AsInteger = f->m_Q_listID->AsInteger;
      m_dm->m_Q_itemSCHT_DOC_NUMBER->AsString = f->m_Q_listDOC_NUMBER->AsString;
      m_dm->m_Q_itemSCHT_DOC_DATE->AsDateTime = f->m_Q_listDOC_DATE->AsDateTime;
      m_dm->m_Q_itemSCHT_SRC_ONAME->AsString = f->m_Q_listSRC_ORG_NAME->AsString;
      m_dm->m_Q_itemSCHT_DST_ONAME->AsString = f->m_Q_listDOC_SRC_ORG_NAME->AsString;

      m_dm->m_Q_item->Post();
    }
  }
  __finally
  {
    if (f) delete f;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_get_schet_docUpdate(TObject *Sender)
{
  m_ACT_get_schet_doc->Enabled = m_dm->UpdateRegimeItem != urView;// && m_dm->m_Q_linesID->IsNull;
  if(m_P_schet->Visible)
  {
    if(m_dm->m_Q_itemSCHT_DOC_ID->IsNull)
    {
      m_P_schet->Color = clCream;
    }
    else
    {
      m_P_schet->Color = clBtnFace;
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_DBG_diffGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
  if (!State.Contains(gdSelected) &&
    (Column->FieldName == "ARP" || Column->FieldName == "SCHET" || Column->FieldName == "DIFF") &&
    m_dm->m_Q_lines_diffDIFF->AsFloat != 0
  )
  {
    Background = clYellow;
    AFont->Color = clWindowText;
  }
}
//---------------------------------------------------------------------------


void __fastcall TFIncomeItem::m_P_specChange(TObject *Sender)
{
  m_P_lines_control->Enabled = m_P_spec->ActivePage == m_TS_spec;
  if(m_P_spec->ActivePage == m_TS_diff && !m_dm->m_Q_lines_diff->Active)
  {
    m_dm->m_Q_lines_diff->Open();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  if(m_dm->m_Q_lines_diff->Active) m_dm->m_Q_lines_diff->Close();
  TTSFDocumentWithLines::FormClose(Sender, Action);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = m_dm->UpdateRegimeItem != urView &&
                          !m_dm->m_Q_linesID->IsNull &&
                          m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->FldrNew;
}
//---------------------------------------------------------------------------



void __fastcall TFIncomeItem::m_ACT_income_diffExecute(TObject *Sender)
{
  if (m_income_diff_ctrl.CanExecuteEdit)
     m_income_diff_ctrl.ExecuteEdit(m_dm->m_Q_itemDIFF_DOC_ID->AsInteger);
  else  if (m_income_diff_ctrl.CanExecuteView)
     m_income_diff_ctrl.ExecuteView(m_dm->m_Q_itemDIFF_DOC_ID->AsInteger);
  else
    Application->MessageBox(AnsiString("� ��� ��� ���� �� �������� ���������").c_str(),
                                    Application->Title.c_str(),
                                    MB_ICONERROR | MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::m_ACT_income_diffUpdate(TObject *Sender)
{
   m_ACT_income_diff->Enabled =  !m_dm->m_Q_itemDIFF_DOC_ID->IsNull;
   m_ACT_sign->Visible = (!m_dm->m_Q_our_cntCNT_ID->IsNull && m_dm->m_Q_itemSCHT_DOC_ID->IsNull) || !m_dm->m_Q_itemSIGN->IsNull;
   m_DBE_sign->Visible = !m_dm->m_Q_itemSIGN->IsNull;
   m_DBE_sign_date->Visible = !m_dm->m_Q_itemSIGN->IsNull;
}
//---------------------------------------------------------------------------


void __fastcall TFIncomeItem::m_ACT_signUpdate(TObject *Sender)
{
  m_ACT_sign->Enabled = m_ACT_sign->Visible
                       && m_can_sign
                       && m_dm->UpdateRegimeItem != urView
                       && m_dm->m_Q_itemSIGN->IsNull
                       && (m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->FldrNew || m_dm->UpdateRegimeItem == urInsert);
}
//---------------------------------------------------------------------------


void __fastcall TFIncomeItem::m_ACT_signExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  m_dm->m_Q_sign->Prepare();
  m_dm->m_Q_sign->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
  m_dm->m_Q_sign->ExecSQL();
  m_dm->m_Q_sign->UnPrepare();
  m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------





void __fastcall TFIncomeItem::btnCreateIncClick(TObject *Sender)
{
 m_dm->m_SP_CREATE_INC->ParamByName("P_BASE_DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
         m_dm->m_SP_CREATE_INC->ExecProc();

          if(m_dm->m_Q_list->Active) m_dm->m_Q_list->Close();
           m_dm->m_Q_list->Open();

         char str[100];
         sprintf(str,"��� ������, id = %s", m_dm->m_SP_CREATE_INC->ParamByName("P_NEW_DOC_ID")->AsString.Trim() );
                    MessageBox(0,str ,"�������", MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeItem::btnCreateEdIncClick(TObject *Sender)
{
  m_dm->m_SP_CREATE_ED_INC->ParamByName("P_BASE_DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
         m_dm->m_SP_CREATE_ED_INC->ExecProc();

          if(m_dm->m_Q_list->Active) m_dm->m_Q_list->Close();
           m_dm->m_Q_list->Open();

         char str[100];
         sprintf(str,"������������� ��� �������, id = %s", m_dm->m_SP_CREATE_ED_INC->ParamByName("P_NEW_DOC_ID")->AsString.Trim() );
                    MessageBox(0,str ,"�������", MB_OK);
}
//---------------------------------------------------------------------------

