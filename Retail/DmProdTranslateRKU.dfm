inherited DProdTranslateRKU: TDProdTranslateRKU
  Left = 285
  Top = 161
  Width = 741
  inherited m_Q_list: TMLQuery
    AfterScroll = m_Q_listAfterScroll
    SQL.Strings = (
      'SELECT'
      't.id, t.org_id, o.name as org_name,'
      't.nmcl_id, ni.name as nmcl_name,'
      't.assort_id, a.name as assort_name,'
      't.to_gr_dep_id, g_to.name as to_gr_dep_name,'
      't.to_nmcl_id, ni_to.name as to_nmcl_name,'
      't.to_assort_id, a_to.name as to_assort_name,'
      't.coef'
      
        'FROM rtl_prod_translate_rku t, organizations o, nomenclature_ite' +
        'ms ni, assortment a, '
      
        '     groups_of_depositories g_to, nomenclature_items ni_to, asso' +
        'rtment a_to'
      'WHERE'
      't.org_id = o.id'
      'AND t.nmcl_id = ni.id'
      'AND t.assort_id = a.id'
      'AND t.to_gr_dep_id = g_to.id'
      'AND t.to_nmcl_id = ni_to.id'
      'AND t.to_assort_id = a_to.id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 't.id'
    end
    object m_Q_listORG_ID: TFloatField
      DisplayLabel = '��� �����������'
      FieldName = 'ORG_ID'
      Origin = 't.org_id'
    end
    object m_Q_listORG_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ORG_NAME'
      Origin = 'o.name'
      Size = 250
    end
    object m_Q_listNMCL_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'NMCL_ID'
      Origin = 't.nmcl_id'
    end
    object m_Q_listNMCL_NAME: TStringField
      DisplayLabel = '������������ �������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_listASSORT_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'ASSORT_ID'
      Origin = 't.assort_id'
    end
    object m_Q_listASSORT_NAME: TStringField
      DisplayLabel = '����������� �������'
      FieldName = 'ASSORT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_listTO_GR_DEP_ID: TFloatField
      DisplayLabel = '��� ������ �������'
      FieldName = 'TO_GR_DEP_ID'
      Origin = 't.to_gr_dep_id'
    end
    object m_Q_listTO_GR_DEP_NAME: TStringField
      DisplayLabel = '����� �������'
      FieldName = 'TO_GR_DEP_NAME'
      Origin = 'g_to.name'
      Size = 100
    end
    object m_Q_listTO_NMCL_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'TO_NMCL_ID'
      Origin = 't.to_nmcl_id'
    end
    object m_Q_listTO_NMCL_NAME: TStringField
      DisplayLabel = '������������ �������'
      FieldName = 'TO_NMCL_NAME'
      Origin = 'ni_to.name'
      Size = 100
    end
    object m_Q_listTO_ASSORT_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'TO_ASSORT_ID'
      Origin = 't.to_assort_id'
    end
    object m_Q_listTO_ASSORT_NAME: TStringField
      DisplayLabel = '����������� �������'
      FieldName = 'TO_ASSORT_NAME'
      Origin = 'a_to.name'
      Size = 100
    end
    object m_Q_listCOEF: TFloatField
      DisplayLabel = '�����������'
      FieldName = 'COEF'
      Origin = 't.coef'
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'SELECT'
      't.id, t.org_id, o.name as org_name,'
      't.nmcl_id, ni.name as nmcl_name,'
      't.assort_id, a.name as assort_name,'
      't.to_gr_dep_id, g_to.name as to_gr_dep_name,'
      't.to_nmcl_id, ni_to.name as to_nmcl_name,'
      't.to_assort_id, a_to.name as to_assort_name,'
      't.coef'
      
        'FROM rtl_prod_translate_rku t, organizations o, nomenclature_ite' +
        'ms ni, assortment a, '
      
        '     groups_of_depositories g_to, nomenclature_items ni_to, asso' +
        'rtment a_to'
      'WHERE'
      't.org_id = o.id'
      'AND t.nmcl_id = ni.id'
      'AND t.assort_id = a.id'
      'AND t.to_gr_dep_id = g_to.id'
      'AND t.to_nmcl_id = ni_to.id'
      'AND t.to_assort_id = a_to.id'
      'AND t.id = :ID')
    object m_Q_itemORG_ID: TFloatField
      FieldName = 'ORG_ID'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.ORG_ID'
    end
    object m_Q_itemORG_NAME: TStringField
      FieldName = 'ORG_NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
    object m_Q_itemNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.NMCL_ID'
    end
    object m_Q_itemNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_itemASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.ASSORT_ID'
    end
    object m_Q_itemASSORT_NAME: TStringField
      FieldName = 'ASSORT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
    object m_Q_itemTO_GR_DEP_ID: TFloatField
      FieldName = 'TO_GR_DEP_ID'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.TO_GR_DEP_ID'
    end
    object m_Q_itemTO_GR_DEP_NAME: TStringField
      FieldName = 'TO_GR_DEP_NAME'
      Origin = 'TSDBMAIN.GROUPS_OF_DEPOSITORIES.NAME'
      Size = 100
    end
    object m_Q_itemTO_NMCL_ID: TFloatField
      FieldName = 'TO_NMCL_ID'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.TO_NMCL_ID'
    end
    object m_Q_itemTO_NMCL_NAME: TStringField
      FieldName = 'TO_NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_itemTO_ASSORT_ID: TFloatField
      FieldName = 'TO_ASSORT_ID'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.TO_ASSORT_ID'
    end
    object m_Q_itemTO_ASSORT_NAME: TStringField
      FieldName = 'TO_ASSORT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
    object m_Q_itemCOEF: TFloatField
      FieldName = 'COEF'
      Origin = 'TSDBMAIN.RTL_PROD_TRANSLATE_RKU.COEF'
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'UPDATE retail.rtl_prod_translate_rku '
      'SET '
      'org_id = :org_id, '
      'nmcl_id = :nmcl_id, '
      'assort_id = :assort_id, '
      'to_nmcl_id = :to_nmcl_id,'
      'to_assort_id = :to_assort_id, '
      'to_gr_dep_id = :to_gr_dep_id, '
      'coef = :coef'
      'WHERE id = :OLD_ID')
    InsertSQL.Strings = (
      'INSERT INTO retail.rtl_prod_translate_rku '
      '(id, org_id, nmcl_id, assort_id, to_nmcl_id,'
      '       to_assort_id, to_gr_dep_id, coef)'
      'VALUES(:id, :org_id, :nmcl_id, :assort_id, :to_nmcl_id,'
      '       :to_assort_id, :to_gr_dep_id, :coef)')
    DeleteSQL.Strings = (
      'DELETE FROM rtl_prod_translate_rku WHERE id = :OLD_ID')
    IgnoreRowsAffected = True
  end
  object m_Q_hist: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_histBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT'
      't.id, t.rtl_prod_rku_id,'
      't.org_id, o.name as org_name,'
      't.nmcl_id, ni.name as nmcl_name,'
      't.assort_id, a.name as assort_name,'
      't.to_gr_dep_id, g_to.name as to_gr_dep_name,'
      't.to_nmcl_id, ni_to.name as to_nmcl_name,'
      't.to_assort_id, a_to.name as to_assort_name,'
      't.coef,'
      't.oper_type,'
      
        'DECODE(t.oper_type, '#39'I'#39', '#39'�������'#39', '#39'U'#39', '#39'���������'#39', '#39'��������'#39 +
        ') as oper_type_name,'
      't.oper_date,'
      't.user_name,'
      't.comp_name'
      
        'FROM rtl_prod_translate_rku_hist t, organizations o, nomenclatur' +
        'e_items ni, assortment a, '
      
        '     groups_of_depositories g_to, nomenclature_items ni_to, asso' +
        'rtment a_to'
      'WHERE'
      't.org_id = o.id'
      'AND t.nmcl_id = ni.id'
      'AND t.assort_id = a.id'
      'AND t.to_gr_dep_id = g_to.id'
      'AND t.to_nmcl_id = ni_to.id'
      'AND t.to_assort_id = a_to.id'
      'AND t.rtl_prod_rku_id = :ID'
      '%WHERE_CLAUSE'
      'order by t.id DESC')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 24
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_histID: TFloatField
      FieldName = 'ID'
      Origin = 't.id'
    end
    object m_Q_histRTL_PROD_RKU_ID: TFloatField
      DisplayLabel = 'ID �����������'
      FieldName = 'RTL_PROD_RKU_ID'
      Origin = 't.RTL_PROD_RKU_ID'
    end
    object m_Q_histORG_ID: TFloatField
      DisplayLabel = '��� �����������'
      FieldName = 'ORG_ID'
      Origin = 't.ORG_ID'
    end
    object m_Q_histORG_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ORG_NAME'
      Origin = 'o.name'
      Size = 250
    end
    object m_Q_histNMCL_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'NMCL_ID'
      Origin = 't.NMCL_ID'
    end
    object m_Q_histNMCL_NAME: TStringField
      DisplayLabel = '������������ �������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_histASSORT_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'ASSORT_ID'
      Origin = 't.ASSORT_ID'
    end
    object m_Q_histASSORT_NAME: TStringField
      DisplayLabel = '����������� �������'
      FieldName = 'ASSORT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_histTO_GR_DEP_ID: TFloatField
      DisplayLabel = '��� ������ �������'
      FieldName = 'TO_GR_DEP_ID'
      Origin = 't.TO_GR_DEP_ID'
    end
    object m_Q_histTO_GR_DEP_NAME: TStringField
      DisplayLabel = '����� �������'
      FieldName = 'TO_GR_DEP_NAME'
      Origin = 'g_to.name'
      Size = 100
    end
    object m_Q_histTO_NMCL_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'TO_NMCL_ID'
      Origin = 't.to_nmcl_id'
    end
    object m_Q_histTO_NMCL_NAME: TStringField
      DisplayLabel = '������������ �������'
      FieldName = 'TO_NMCL_NAME'
      Origin = 'ni_to.name'
      Size = 100
    end
    object m_Q_histTO_ASSORT_ID: TFloatField
      DisplayLabel = '��� ������������ �������'
      FieldName = 'TO_ASSORT_ID'
      Origin = 't.to_assort_id'
    end
    object m_Q_histTO_ASSORT_NAME: TStringField
      DisplayLabel = '����������� �������'
      FieldName = 'TO_ASSORT_NAME'
      Origin = 'a_to.name'
      Size = 100
    end
    object m_Q_histCOEF: TFloatField
      DisplayLabel = '�����������'
      FieldName = 'COEF'
      Origin = 't.coef'
    end
    object m_Q_histOPER_TYPE: TStringField
      DisplayLabel = '��� ���� ��������'
      FieldName = 'OPER_TYPE'
      Origin = 't.OPER_TYPE'
      FixedChar = True
      Size = 1
    end
    object m_Q_histOPER_TYPE_NAME: TStringField
      DisplayLabel = '��� � �������'
      FieldName = 'OPER_TYPE_NAME'
      Origin = 
        'DECODE(t.oper_type, '#39'I'#39', '#39'�������'#39', '#39'U'#39', '#39'���������'#39', '#39'��������'#39 +
        ')'
      Size = 9
    end
    object m_Q_histOPER_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'OPER_DATE'
      Origin = 't.OPER_DATE'
    end
    object m_Q_histUSER_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'USER_NAME'
      Origin = 't.USER_NAME'
    end
    object m_Q_histCOMP_NAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'COMP_NAME'
      Origin = 't.COMP_NAME'
      Size = 55
    end
  end
  object m_Q_org: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select o.id, o.name'
      'from organizations o'
      'WHERE o.enable = '#39'Y'#39
      'AND o.org_type_id = 2'
      '%WHERE_CLAUSE'
      'order by o.id')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 24
    Top = 232
    object m_Q_orgID: TFloatField
      FieldName = 'ID'
      Origin = 'o.id'
    end
    object m_Q_orgNAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'NAME'
      Origin = 'o.name'
      Size = 250
    end
  end
  object m_Q_nmcl: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select n.id, n.name'
      'from nomenclature_items n'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 96
    Top = 232
    object m_Q_nmclID: TFloatField
      FieldName = 'ID'
      Origin = 'n.id'
    end
    object m_Q_nmclNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'n.name'
      Size = 100
    end
  end
  object m_Q_nmcl_to: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select n.id, n.name'
      'from nomenclature_items n'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 24
    Top = 296
    object FloatField1: TFloatField
      FieldName = 'ID'
      Origin = 'n.id'
    end
    object StringField1: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'n.name'
      Size = 100
    end
  end
  object m_Q_assort: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_assortBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name'
      'FROM '
      '('
      'select a.id, a.name'
      'from assortment a, nmcl_bar_codes b'
      'WHERE b.assortment_id = a.id'
      'AND b.nmcl_id = :NMCL_ID AND b.enable = '#39'Y'#39
      'UNION'
      'SELECT id, name FROM assortment where id = 7'
      ')'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 168
    Top = 232
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_assortID: TFloatField
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_assortNAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'NAME'
      Origin = 'name'
      Size = 100
    end
  end
  object m_Q_assort_to: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_assort_toBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name'
      'FROM '
      '('
      'select a.id, a.name'
      'from assortment a, nmcl_bar_codes b'
      'WHERE b.assortment_id = a.id'
      'AND b.nmcl_id = :NMCL_ID AND b.enable = '#39'Y'#39
      'UNION'
      'SELECT id, name FROM assortment where id = 7'
      ')'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 96
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object FloatField2: TFloatField
      FieldName = 'ID'
      Origin = 'id'
    end
    object StringField2: TStringField
      DisplayLabel = '�����������'
      FieldName = 'NAME'
      Origin = 'name'
      Size = 100
    end
  end
  object m_Q_gr_dep_to: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_gr_dep_toBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT g.id, g.name'
      'FROM groups_of_depositories g'
      'WHERE'
      'g.org_id = :ORG_ID'
      '%WHERE_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 168
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_gr_dep_toID: TFloatField
      FieldName = 'ID'
      Origin = 'g.id'
    end
    object m_Q_gr_dep_toNAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'NAME'
      Origin = 'g.name'
      Size = 100
    end
  end
end
