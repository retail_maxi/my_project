//---------------------------------------------------------------------------

#ifndef DmAssortExceptionH
#define DmAssortExceptionH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMREFBOOK.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDAssortException : public TTSDRefbook
{
__published:	// IDE-managed Components
        TMLQuery *m_Q_nmcls;
        TFloatField *m_Q_nmclsID;
	TFloatField *m_Q_listGR_ID;
	TStringField *m_Q_listGR_NAME;
	TFloatField *m_Q_listCAT_ID;
	TStringField *m_Q_listCAT_NAME;
	TFloatField *m_Q_itemGR_ID;
	TStringField *m_Q_itemGR_NAME;
	TFloatField *m_Q_itemCAT_ID;
	TStringField *m_Q_itemCAT_NAME;
	TStringField *m_Q_nmclsCAT_NAME;
	TStringField *m_Q_nmclsGR_NAME;
        TMLQuery *m_Q_subcats;
        TMLQuery *m_Q_subsubcats;
        TFloatField *m_Q_listSUBCAT_ID;
        TStringField *m_Q_listSUBCAT_NAME;
        TFloatField *m_Q_listSUBSUBCAT_ID;
        TStringField *m_Q_listSUBSUBCAT_NAME;
        TFloatField *m_Q_itemSUBCAT_ID;
        TStringField *m_Q_itemSUBCAT_NAME;
        TFloatField *m_Q_itemSUBSUBCAT_ID;
        TStringField *m_Q_itemSUBSUBCAT_NAME;
        TFloatField *m_Q_subcatsID;
        TStringField *m_Q_subcatsNAME;
        TFloatField *m_Q_subsubcatsID;
        TStringField *m_Q_subsubcatsNAME;
        TFloatField *m_Q_subcatsCAT_ID;
        TStringField *m_Q_subcatsCAT_NAME;
        TFloatField *m_Q_subsubcatsSUBCAT_ID;
        TStringField *m_Q_subsubcatsSUBCAT_NAME;
        TFloatField *m_Q_subsubcatsCAT_ID;
        TStringField *m_Q_subsubcatsCAT_NAME;
        TFloatField *m_Q_subcatsSUBSUBCAT_ID;
        TStringField *m_Q_subcatsSUBSUBCAT_NAME;
        TFloatField *m_Q_nmclsSUBCAT_ID;
        TStringField *m_Q_nmclsSUBCAT_NAME;
        TFloatField *m_Q_nmclsSUBSUBCAT_ID;
        TStringField *m_Q_nmclsSUBSUBCAT_NAME;
        TMLQuery *m_Q_sku;
        TMLQuery *m_Q_orgs;
        TFloatField *m_Q_itemORG_GROUP_ID;
        TStringField *m_Q_itemORG_GROUP_NAME;
        TMLQuery *m_Q_org_groups;
        TFloatField *m_Q_org_groupsID;
        TStringField *m_Q_org_groupsNAME;
        TFloatField *m_Q_skuNMCL_ID;
        TStringField *m_Q_skuNMCL_NAME;
        TFloatField *m_Q_orgsORG_ID;
        TStringField *m_Q_orgsORG_NAME;
        TFloatField *m_Q_listORG_GROUP_ID;
        TStringField *m_Q_listORG_GROUP_NAME;
        TStringField *m_Q_listORGS;
        TStringField *m_Q_listNMCLS;
        TQuery *m_Q_delete_orgs;
        void __fastcall m_Q_orgsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_skuBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
        void __fastcall m_Q_subcatsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_subsubcatsBeforeOpen(TDataSet *DataSet);
private:	// User declarations
protected:
    TS_REFB_SQ_NAME("retail.sq_rtl_assort_exception")
public:		// User declarations
        __fastcall TDAssortException(TComponent* p_owner, AnsiString p_prog_id);
ML_BEGIN_DATA_SETS
  ML_BASE_DATA_SETS(TTSDRefbook)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_orgs)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_sku)
ML_END_DATA_SETS

};
//---------------------------------------------------------------------------

#endif
