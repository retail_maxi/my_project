//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocRtlCouponDiscOrgs.h"
#include "MLFuncs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
bool ExecDocRtlCouponDiscOrgsDlg(TComponent *p_owner,
                      hDBIDb p_db_handle,
                      long p_doc_id
                      )
{
  TFDocRtlCouponDiscOrgs *f_gl = new TFDocRtlCouponDiscOrgs(p_owner, p_db_handle, p_doc_id);
  bool res = false;
  try
  {
    f_gl->ShowModal();
    res = f_gl->PressOK;
  }
  __finally
  {
    delete f_gl;
  }
  return res;
}
//---------------------------------------------------------------------------

__fastcall TFDocRtlCouponDiscOrgs::TFDocRtlCouponDiscOrgs(TComponent* p_owner,
                                    hDBIDb p_db_handle, long p_doc_id): TForm(p_owner),
                                    m_db_handle(p_db_handle), m_doc_id(p_doc_id)
{
  FPressOK = false;
  m_DB_gl->Handle = m_db_handle;
}
//---------------------------------------------------------------------------

__fastcall TFDocRtlCouponDiscOrgs::~TFDocRtlCouponDiscOrgs()
{
  m_DB_gl->Handle = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscOrgs::m_ACT_closeExecute(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscOrgs::m_ACT_apply_updatesExecute(TObject *Sender)
{
   m_Q_sel_orgs->First();
   while (!m_Q_sel_orgs->Eof)
   {
     if (m_Q_sel_orgsCHECKED->AsInteger == 1)
     {
       try {
         m_Q_ins_orgs->ParamByName("DOC_ID")->AsInteger = m_doc_id;
         m_Q_ins_orgs->ParamByName("ORG_ID")->AsInteger = m_Q_sel_orgsID->AsInteger;
         m_Q_ins_orgs->ExecSQL();
       } catch (Exception& e){
         MessageDlg(e.Message, mtError, TMsgDlgButtons()<<mbOK,0);
       }
     }
     else
     {
       try {
         m_Q_del_orgs->ParamByName("DOC_ID")->AsInteger = m_doc_id;
         m_Q_del_orgs->ParamByName("ORG_ID")->AsInteger = m_Q_sel_orgsID->AsInteger;
         m_Q_del_orgs->ExecSQL();
       } catch (Exception& e){
         MessageDlg(e.Message, mtError, TMsgDlgButtons()<<mbOK,0);
       }
     }
     m_Q_sel_orgs->Next();
   }

  FPressOK = true;
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscOrgs::FormShow(TObject *Sender)
{
  if(m_Q_sel_orgs->Active) m_Q_sel_orgs->Close();
  m_Q_sel_orgs->Open();
}
//---------------------------------------------------------------------------
void __fastcall TFDocRtlCouponDiscOrgs::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  m_DB_gl->Close();
}
//---------------------------------------------------------------------------

bool __fastcall TFDocRtlCouponDiscOrgs::FillNodesCheck(int node_id, bool m_check)
{
  m_Q_sel_orgs->DisableControls();
  try
  {
    if (m_check)
    {
      m_Q_sel_orgs->Edit();
      m_Q_sel_orgsCHECKED->AsInteger = 1;
      m_Q_sel_orgs->Post();
      m_Q_sel_orgs->Next();
    }
    else
    {
      m_Q_sel_orgs->Edit();
      m_Q_sel_orgsCHECKED->AsInteger = 2;
      m_Q_sel_orgs->Post();
      m_Q_sel_orgs->Next();
    }
      m_Q_sel_orgs->Locate(m_Q_sel_orgsID->FieldName, node_id, TLocateOptions());
  }
  __finally
  {
    m_Q_sel_orgs->EnableControls();
  }
  return true;
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscOrgs::m_DBG_listKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == 32)
  {
    FillNodesCheck(m_Q_sel_orgsID->AsInteger, m_Q_sel_orgsCHECKED->AsInteger == 2);
  }
}
//---------------------------------------------------------------------------

bool __fastcall TFDocRtlCouponDiscOrgs::GetPressOK()
{
  return FPressOK;
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscOrgs::m_Q_sel_orgsBeforeOpen(
      TDataSet *DataSet)
{
  m_Q_sel_orgs->ParamByName("DOC_ID")->AsInteger = m_doc_id;
}
//---------------------------------------------------------------------------


