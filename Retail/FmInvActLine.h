//---------------------------------------------------------------------------

#ifndef FmInvActLineH
#define FmInvActLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmInvAct.h"
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "TSFormControls.h"
#include "FmInvActHst.h"
#include <Dialogs.hpp>
#include "RXCtrls.hpp"
#include "DLBCScaner.h"
//---------------------------------------------------------------------------
class TFInvActLine : public TTSFDocumentLine
{
__published:
  TLabel *m_L_card_id;
  TMLDBPanel *m_P_card_id;
  TLabel *m_L_nmcl;
  TMLDBPanel *m_P_nmcl_id;
  TMLDBPanel *m_P_nmcl_name;
  TLabel *m_L_mu_name;
  TMLDBPanel *m_P_mu_name;
  TLabel *m_L_price;
  TMLDBPanel *m_P_out_price;
  TLabel *m_L_fact_items;
  TDBEdit *m_DBE_fact_items;
  TLabel *m_L_start_date;
  TDBDateEdit *m_DE_start_date;
  TDateTimePicker *m_TP_start_date;
  TSpeedButton *m_SBTN_clear_start_date;
  TLabel *m_L_note;
  TDBEdit *m_DBE_note;
  TAction *m_ACT_clear_start_date;
  TMLDBGrid *m_DBG_sheet_lines;
  TDataSource *m_DS_sheet_lines;
  TLabel *m_L_sheet_link;
  TAction *m_ACT_view_sheet;
  TSpeedButton *m_SBTN_view_sheet;
  TMLDBPanel *m_P_assortment_name;
  TLabel *m_L_assortment;
  TMLDBPanel *m_P_start_items;
  TLabel *m_L_start_items;
        TLabel *Label1;
        TDBEdit *m_DBE_recalc_items;
        TDBCheckBox *m_DBCHB_for_recalc;
        TDBCheckBox *m_DBCB_NON_PREPARE;
  TDBEdit *m_DBE_from;
  TLabel *Label2;
  TLabel *Label3;
  TDBEdit *m_DBE_to;
  TLabel *Label4;
  TDBEdit *m_DBE_coef;
        TBitBtn *m_BBTN_show_hst;
        TToolButton *ToolButton1;
        TAction *m_ACT_show_hst;
        TMLDBGrid *m_DBG_alco;
        TDataSource *m_DS_alc;
        TPanel *m_P_lines_control;
        TRxSpeedButton *RxSpeedButton1;
        TToolBar *m_TB_lines_control_button;
        TSpeedButton *m_SBTN_append;
        TSpeedButton *m_SBTN_edit;
        TSpeedButton *m_SBTN_delete;
        TAction *m_ACT_ins;
        TAction *m_ACT_edit;
        TAction *m_ACT_del;
        TDLBCScaner *m_BCS_item;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_TP_start_dateChange(TObject *Sender);
  void __fastcall m_ACT_clear_start_dateUpdate(TObject *Sender);
  void __fastcall m_ACT_clear_start_dateExecute(TObject *Sender);
  void __fastcall m_DBE_fact_itemsChange(TObject *Sender);
  void __fastcall m_ACT_view_sheetUpdate(TObject *Sender);
  void __fastcall m_ACT_view_sheetExecute(TObject *Sender);
  void __fastcall m_DS_lineDataChange(TObject *Sender, TField *Field);
  void __fastcall m_DBE_fromChange(TObject *Sender);
  void __fastcall m_DBE_fromExit(TObject *Sender);
        void __fastcall m_ACT_show_hstExecute(TObject *Sender);
        void __fastcall m_ACT_insExecute(TObject *Sender);
        void __fastcall m_ACT_editExecute(TObject *Sender);
        void __fastcall m_ACT_delExecute(TObject *Sender);
        void __fastcall m_ACT_insUpdate(TObject *Sender);
        void __fastcall m_ACT_editUpdate(TObject *Sender);
        void __fastcall m_ACT_delUpdate(TObject *Sender);
        void __fastcall m_BCS_itemBegin(TObject *Sender);
        void __fastcall m_BCS_itemEnd(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:
  TDInvAct* m_dm;
  TTSDocumentControl doc_sheet_ctrl;
  bool formLoaded;
  TMLGridOptions  m_temp_grid_options;
public:
  __fastcall TFInvActLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
};
//---------------------------------------------------------------------------
#endif
