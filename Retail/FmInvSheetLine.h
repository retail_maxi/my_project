//---------------------------------------------------------------------------

#ifndef FmInvSheetLineH
#define FmInvSheetLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmInvSheet.h"
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "FrmRtlIncomeLine.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include "RXCtrls.hpp"
#include <Dialogs.hpp>
#include <Grids.hpp>
#include "DLBCScaner.h"
//---------------------------------------------------------------------------
class TFInvSheetLine : public TTSFDocumentLine
{
__published:
  TLabel *m_L_mu_name;
  TMLDBPanel *m_P_mu_name;
  TLabel *m_L_items;
  TDBEdit *m_DBE_items;
  TLabel *m_L_start_date;
  TDBDateEdit *m_DE_start_date;
  TDateTimePicker *m_TP_start_date;
  TSpeedButton *m_SBTN_clear_start_date;
  TLabel *m_L_note;
  TDBEdit *m_DBE_note;
  TAction *m_ACT_clear_start_date;
  TFRtlIncomeLine *m_FRM_rtl_line;
        TMLDBGrid *m_DBG_alco;
        TPanel *m_P_lines_control;
        TRxSpeedButton *RxSpeedButton1;
        TToolBar *m_TB_lines_control_button;
        TSpeedButton *m_SBTN_append;
        TSpeedButton *m_SBTN_edit;
        TSpeedButton *m_SBTN_delete;
        TDataSource *m_DS_alc;
        TAction *m_ACT_ins;
        TAction *m_ACT_edit;
        TAction *m_ACT_del;
        TDLBCScaner *m_BCS_line;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_TP_start_dateChange(TObject *Sender);
  void __fastcall m_ACT_clear_start_dateUpdate(TObject *Sender);
  void __fastcall m_ACT_clear_start_dateExecute(TObject *Sender);
  void __fastcall m_ACT_apply_updatesUpdate(TObject *Sender);
  void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall m_ACT_insExecute(TObject *Sender);
        void __fastcall m_ACT_insUpdate(TObject *Sender);
        void __fastcall m_ACT_editExecute(TObject *Sender);
        void __fastcall m_ACT_editUpdate(TObject *Sender);
        void __fastcall m_ACT_delExecute(TObject *Sender);
        void __fastcall m_ACT_delUpdate(TObject *Sender);
        void __fastcall m_BCS_lineBegin(TObject *Sender);
        void __fastcall m_BCS_lineEnd(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:
  TDInvSheet* m_dm;
  AnsiString __fastcall BeforeLineApply(TWinControl *p_focused);
  TMLGridOptions  m_temp_grid_options;
public:
  __fastcall TFInvSheetLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
  __fastcall ~TFInvSheetLine();
};
//---------------------------------------------------------------------------
#endif
