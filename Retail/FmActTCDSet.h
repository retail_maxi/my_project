//---------------------------------------------------------------------------

#ifndef FmActTCDSetH
#define FmActTCDSetH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Db.hpp>
#include <DBTables.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DmIncome.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
//---------------------------------------------------------------------------

class TFActTCDSet : public TForm
{
__published:
  TDataSource *m_DS_tp_outcomes;
  TPanel *m_P_main_control;
  TPanel *m_P_main_control_buttons;
  TBitBtn *m_BBTN_close;
  TBitBtn *m_BBTN_save;
  TMLDBGrid *m_DBG_tp_outcomes;
private:
  TDIncome *m_dm;
public:
  __fastcall TFActTCDSet(TComponent* p_owner, TDIncome *p_dm)
             :TForm(p_owner),
             m_dm(p_dm)
             {};
};
//---------------------------------------------------------------------------

bool ExecActTCDSetDlg(TComponent *p_owner, TDIncome *p_dm);
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
