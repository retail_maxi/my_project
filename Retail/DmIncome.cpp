//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmIncome.h"
#include "TSErrors.h"   //
#include "Barcode.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//--------------------------------------------------------------------------

__fastcall TDIncome::TDIncome(TComponent* p_owner, AnsiString p_prog_id)
  : TTSDDocumentWithLines(p_owner,p_prog_id),
  m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID"))),
  m_fldr_new(StrToInt64(GetConstValue("FLDR_RETAIL_INCOME_NEW"))),
  m_fldr_cnf(StrToInt64(GetConstValue("FLDR_RETAIL_INCOME_CNF"))),
  m_fldr_chk(StrToInt64(GetConstValue("FLDR_RETAIL_INCOME_CHK"))),
  m_fldr_arc(StrToInt64(GetConstValue("FLDR_RETAIL_INCOME_ARC"))),
  m_param_not_include_act_verif(StrToInt64(GetConstValue("PARAM_NOT_INCLUDE_ACT_VERIF"))),
  m_show_nmcls(false),
  m_last_bar_code(""),
  m_dct_act_fldr_filled_id(StrToInt64(GetConstValue("FLDR_DCT_INCOME_FLD"))),
  m_sq_id(0),
  p_sq_doc(1),
  m_only_open(true)
{
  //m_home_org_id = 7; //debug
  m_Q_orgs->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
  m_Q_orgs->Locate(m_Q_orgsID->FieldName,m_home_org_id,TLocateOptions());
  m_Q_fict_org->Open();
  m_Q_fict_org->Insert();
  m_Q_fict_orgID->AsInteger = m_home_org_id;

  SetBeginEndDate(int(GetSysDate()),int(GetSysDate()));

}
//---------------------------------------------------------------------------

__fastcall TDIncome::~TDIncome()
{
  m_Q_fict_org->Cancel();
  m_Q_fict_org->Close();
  m_Q_orgs->Close();
  if(m_Q_our_cnt->Active) m_Q_our_cnt->Close();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_nmclsBeforeOpen(TDataSet *DataSet)
{
  m_Q_nmcls->ParamByName("DOC_TYPE_ID")->Value = m_Q_list->ParamByName("DOC_TYPE_ID")->Value;
  m_Q_nmcls->ParamByName("BEGIN_DATE")->Value = m_Q_list->ParamByName("BEGIN_DATE")->Value;
  m_Q_nmcls->ParamByName("END_DATE")->Value = m_Q_list->ParamByName("END_DATE")->Value;
  m_Q_nmcls->ParamByName("ACCESS_MASK")->Value = m_Q_list->ParamByName("ACCESS_MASK")->Value;
  m_Q_nmcls->ParamByName("ORG_ID")->Value = m_Q_list->ParamByName("ORG_ID")->Value;
  m_Q_nmcls->MacroByName("LIST_WHERE_CLAUSE")->Value = m_Q_list->MacroByName("WHERE_CLAUSE")->Value;
}
//---------------------------------------------------------------------------

int __fastcall TDIncome::GetOrgId()
{
  return m_Q_list->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::SetOrgId(int p_id)
{
  if (GetOrgId() == p_id) return;
  m_Q_list->ParamByName("ORG_ID")->AsInteger = p_id;
  RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_listAfterOpen(TDataSet *DataSet)
{
  //��� ������ �� TTSDDocument::TTSDDocument() ���������������, ��� m_show_nmcls == false �� �������������
  if (m_show_nmcls) m_Q_nmcls->Open();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_listBeforeClose(TDataSet *DataSet)
{
  m_Q_nmcls->Close();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::SetShowNmcls(bool p_value)
{
  if (p_value == m_show_nmcls) return;

  (m_show_nmcls = p_value) ? m_Q_nmcls->Open() : m_Q_nmcls->Close();
}
//---------------------------------------------------------------------------
void __fastcall TDIncome::SetOnlyOpen(bool p_value)
{
  if (p_value == m_only_open) return;

  m_only_open = p_value;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date)
{
  if ((int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date))) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_gdp_namesBeforeOpen(TDataSet *DataSet)
{
  m_Q_gdp_names->ParamByName("HOME_ORG_ID")->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_itemAfterInsert(DataSet);

  m_Q_itemDOC_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemDOC_NUMBER->AsInteger = StrToIntDef(GetDocNumber(DocTypeId),0);
  m_Q_itemINC_STARTED->AsDateTime = GetSysDate();
  m_Q_itemINC_FINISHED->AsDateTime = GetSysDate();

  m_Q_gdp_names->Close();
  m_Q_gdp_names->Open();
  if (m_Q_gdp_names->RecordCount)
  {
    m_Q_itemGR_DEP_ID->AsInteger = m_Q_gdp_namesGD_ID->AsInteger;
    m_Q_itemGDP_NAME->AsString = m_Q_gdp_namesGD_NAME->AsString;
  }
}
//---------------------------------------------------------------------------

TDIncome::LineKind __fastcall TDIncome::GetCurrLinesKind()
{
  return m_Q_lines->ParamByName("STORED")->IsNull ? lkAny : (LineKind)m_Q_lines->ParamByName("STORED")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::SetCurrLinesKind(TDIncome::LineKind k)
{
  if (k == lkAny) m_Q_lines->ParamByName("STORED")->Clear();
  else m_Q_lines->ParamByName("STORED")->AsInteger = k;

  m_Q_lines->Close();
  m_Q_lines->Open();
}
//---------------------------------------------------------------------------

void TDIncome::SetVerOption(bool val)
{
  if (m_Q_folderFLDR_ID->AsInteger != FldrChk) throw ETSError("�������� ������� ����� ������ � ����� \"��������\"");

  SetDocParam(m_Q_linesID->AsInteger,m_Q_linesLINE_ID->AsInteger,ParamNotIncludeActVerif,int(val));
  m_Q_lines->Close();
  m_Q_lines->Open();
  m_Q_lines->Locate(m_Q_linesLINE_ID->FieldName,m_Q_linesLINE_ID->AsInteger,TLocateOptions());
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_linesBeforeOpen(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_linesBeforeOpen(DataSet);
  m_Q_lines->ParamByName("PARAM_NOT_INCLUDE_ACT_VERIF")->AsInteger = m_param_not_include_act_verif;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_tax_rateBeforeOpen(TDataSet *DataSet)
{
  if (m_Q_lineNMCL_ID->IsNull)
    m_Q_tax_rate->ParamByName("NMCL_ID")->Clear();
  else
    m_Q_tax_rate->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_assortBeforeOpen(TDataSet *DataSet)
{
  m_Q_assort->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_nmcl_descBeforeOpen(TDataSet *DataSet)
{
  m_Q_nmcl_desc->ParamByName("GR_DEP_ID")->AsInteger = m_Q_itemGR_DEP_ID->AsInteger;
  m_Q_nmcl_desc->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------


void __fastcall TDIncome::m_Q_get_gtd_listBeforeOpen(TDataSet *DataSet)
{
  m_Q_get_gtd_list->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_lineAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_lineAfterInsert(DataSet);

  if (!m_last_bar_code.IsEmpty())
  {
    TBarCodeEx bc = FindBarCodeEx(m_DB_main->DatabaseName,m_last_bar_code);
    if (bc.Exist)
    {
      m_Q_lineNMCL_ID->AsInteger = bc.NmclId;
      m_Q_lineNMCL_NAME->AsString = bc.NmclName;
      m_Q_lineASSORTMENT_ID->AsInteger = bc.AssortmentId;
      m_Q_lineASSORTMENT_NAME->AsString = bc.AssortmentName;
    }
  }

  LineKind lk = GetCurrLinesKind();
  m_Q_lineSTORED->AsInteger = lk == lkAny ? lkStored : lk;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_lineAfterClose(TDataSet *DataSet)
{
  m_last_bar_code = "";
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_act_tcdBeforeOpen(TDataSet *DataSet)
{
  m_Q_act_tcd->ParamByName("FLDR_ID")->AsInteger = m_dct_act_fldr_filled_id;
}
//---------------------------------------------------------------------------

long __fastcall TDIncome::FillTmpReportParamValues2(long p_sq)
{
  long sq = p_sq;
  if (sq == 0) sq = GetSqNextVal("SQ_TMP_REPORT_PARAM_VALUES");
  m_Q_fill_tmp_rep_param_val->ParamByName("ID")->AsInteger = sq;
  m_Q_fill_tmp_rep_param_val->Open();
  try
  {
      m_Q_act_tcd->First();
      while( !m_Q_act_tcd->Eof )
      {
        if( m_Q_act_tcdOK->AsString == "Y" )
        {
          m_Q_fill_tmp_rep_param_val->Insert();
          m_Q_fill_tmp_rep_param_valID->AsInteger = sq;
          m_Q_fill_tmp_rep_param_valVALUE->AsString = m_Q_act_tcdDOC_ID->AsString;
          m_Q_fill_tmp_rep_param_val->Post();
        }

        m_Q_act_tcd->Next();
      }

    m_DB_main->ApplyUpdates(OPENARRAY(TDBDataSet*,((TDBDataSet*)m_Q_fill_tmp_rep_param_val)));
  }
  __finally
  {
    m_Q_fill_tmp_rep_param_val->Close();
  }
  return sq;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::ClearTmpReportParamValues2(long p_sq)
{
  m_Q_clear_tmp_rep_param_val->ParamByName("ID")->AsInteger = p_sq;
  m_Q_clear_tmp_rep_param_val->ExecSQL();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_itemBeforeClose(TDataSet *DataSet)
{
  if (UpdateRegimeItem == urInsert && !ChangingItem) FreeDocNumber(DocTypeId,m_Q_itemDOC_NUMBER->AsString);
}
//---------------------------------------------------------------------------

AnsiString __fastcall TDIncome::CheckRtlIncNmcl(int p_nmcl_id,int p_assortment_id)
{
  AnsiString m_res = "";

  if (m_Q_check_rtl_inc_nmcl->Active) m_Q_check_rtl_inc_nmcl->Close();
  m_Q_check_rtl_inc_nmcl->ParamByName("NMCL_ID")->AsInteger = p_nmcl_id;
  m_Q_check_rtl_inc_nmcl->ParamByName("ASSORTMENT_ID")->AsInteger = p_assortment_id;
  m_Q_check_rtl_inc_nmcl->Open();

  m_res = m_Q_check_rtl_inc_nmclRES->AsString;
  m_Q_check_rtl_inc_nmcl->Close();
  return m_res;
}

//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_nmcl_infoBeforeOpen(TDataSet *DataSet)
{
  m_Q_nmcl_info->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_nmcl_info->ParamByName("ASSORT_ID")->AsInteger = m_Q_lineASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::FillData4PrintLabel(int p_doc_id)
{
  m_Q_prc_fill_data_4_print_labels->ParamByName("ID")->AsInteger = p_doc_id;
  m_Q_prc_fill_data_4_print_labels->ParamByName("SQ_ID")->AsInteger = p_sq_doc;
  m_Q_prc_fill_data_4_print_labels->ExecSQL();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_bar_codesBeforeOpen(TDataSet *DataSet)
{
  m_Q_bar_codes->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_bar_codes->ParamByName("ASSORTMENT_ID")->AsInteger = m_Q_lineASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDIncome::GetMakeDate()
{
  return m_Q_lineMAKE_DATE->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::SetMakeDate(TDateTime p_dValue)
{
  if (m_Q_lineMAKE_DATE->AsDateTime != p_dValue && !m_Q_lineMAKE_DATE->IsNull)
  {
    if (m_Q_line->State != dsEdit) m_Q_line->Edit();
    m_Q_lineMAKE_DATE->AsDateTime = p_dValue;
  }
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_make_dateAfterInsert(TDataSet *DataSet)
{ 
  m_Q_make_dateNMCL_ID->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_make_dateASSORT_ID->AsInteger = m_Q_lineASSORTMENT_ID->AsInteger;
  m_Q_make_dateINC_DOC_ID->AsInteger = m_Q_lineID->AsInteger;
 // if (UpdateRegimeItem == urInsert) m_Q_make_dateMAKE_DATE->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TDIncome::m_Q_make_dateBeforeOpen(TDataSet *DataSet)
{  
  m_Q_make_date->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_make_date->ParamByName("ASSORT_ID")->AsInteger = m_Q_lineASSORTMENT_ID->AsInteger;
  m_Q_make_date->ParamByName("ID")->AsInteger = m_Q_lineID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_nmcl_addr_listBeforeOpen(TDataSet *DataSet)
{
    m_Q_nmcl_addr_list->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_address_listBeforeOpen(TDataSet *DataSet)
{
    m_Q_address_list->ParamByName("NMCL_ID")->AsInteger = m_Q_nmcl_addr_listNMCL_ID->AsInteger;
    m_Q_address_list->ParamByName("ASSORT_ID")->AsInteger = m_Q_nmcl_addr_listASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::ChangeAddr()
{
    m_Q_chg_address->ParamByName("ADDRESS")->AsString = m_Q_address_listADDRESS->AsString;
    m_Q_chg_address->ParamByName("NMCL_ID")->AsInteger = m_Q_address_listNMCL_ID->AsInteger;
    m_Q_chg_address->ParamByName("ASSORT_ID")->AsInteger = m_Q_address_listASSORT_ID->AsInteger;
    m_Q_chg_address->ParamByName("SHL_MEAS_UNIT_ID")->AsInteger = m_Q_address_listSHL_MEAS_UNIT_ID->AsInteger;
    m_Q_chg_address->ParamByName("SHELF_LIFE")->AsInteger = m_Q_address_listSHELF_LIFE->AsInteger;
    m_Q_chg_address->ExecSQL();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_load_schetBeforeOpen(TDataSet *DataSet)
{
  m_Q_load_schet->ParamByName("ID")->AsInteger = m_Q_itemACC_DOC_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_lines_diffBeforeOpen(TDataSet *DataSet)
{
  m_Q_lines_diff->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------


void __fastcall TDIncome::m_Q_itemSENDER_IDChange(TField *Sender)
{
  if(m_Q_our_cnt->Active) m_Q_our_cnt->Close();
  m_Q_our_cnt->Open();
}
//---------------------------------------------------------------------------

void __fastcall TDIncome::m_Q_our_cntBeforeOpen(TDataSet *DataSet)
{
  m_Q_our_cnt->ParamByName("CNT_ID")->AsInteger = m_Q_itemSENDER_ID->AsInteger;
}
//---------------------------------------------------------------------------



void __fastcall TDIncome::m_Q_sublinesBeforeOpen(TDataSet *DataSet)
{
  m_Q_sublines->ParamByName("DOC_ID")->Value = m_Q_lineID->Value;
  m_Q_sublines->ParamByName("LINE_ID")->Value = m_Q_lineLINE_ID->Value;
}
//---------------------------------------------------------------------------

