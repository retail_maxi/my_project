//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvActList.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTLIST"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFInvActList::TFInvActList(TComponent* p_owner, TTSDDocument *p_dm)
                                     :TTSFDocumentList(p_owner, p_dm),
                                     m_dm(static_cast<TDInvAct*>(p_dm))
{
}
//---------------------------------------------------------------------------
void __fastcall TFInvActList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate, m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);

  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
  m_DBLCB_organizations->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_organizations->Enabled = false;
  else
    m_DBLCB_organizations->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActList::m_ACT_create_act_for_groupUpdate(TObject *Sender)
{
  m_ACT_create_act_for_group->Enabled = m_dm->CanInsertItem;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActList::m_ACT_create_act_for_groupExecute(TObject *Sender)
{
  if (int doc_id = m_dm->CreateActForGroup())
  {
    m_dm->EditItem(doc_id);
    m_dm->RefreshListDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActList::m_DBG_listGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
  if( m_dm->m_Q_listDOC_FLDR_ID->AsInteger != m_dm->CnfFldr )
  {
    Background = clYellow;
    AFont->Color = clWindowText;
  }

  TTSFDocumentList::m_DBG_listGetCellParams(Sender,Column,AFont,Background,State);

}
//---------------------------------------------------------------------------
void __fastcall TFInvActList::m_DBLCB_organizationsClick(TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;
}
//---------------------------------------------------------------------------


