//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmInvSheet.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "MLLogin"
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "RxQuery"
#pragma link "TSConnectionContainer"
#pragma link "TSDMDOCUMENTWITHLINES"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDInvSheet::TDInvSheet(TComponent* p_owner,AnsiString p_prog_id)
                                 : TTSDDocumentWithLines(p_owner,p_prog_id),
                                 m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID"))),
                                 m_new_fldr(StrToInt64(GetConstValue("FLDR_RTL_INV_SHEET_NEW"))),
                                 m_max_qnt(StrToInt64(GetConstValue("MAX_QNT_INV_ACT")))
{
  Params->InitParam("LINE_ID",0);
  SetBeginEndDate(int(GetSysDate())-7,int(GetSysDate()));
  m_Q_orgs->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
}
//---------------------------------------------------------------------------

__fastcall TDInvSheet::~TDInvSheet()
{
  if( m_Q_orgs->Active ) m_Q_orgs->Close();
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDInvSheet::GetBeginDate()
{
  return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDInvSheet::GetEndDate()
{
  return m_Q_list->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::SetBeginEndDate(const TDateTime &p_begin_date, const TDateTime &p_end_date)
{
  if ((int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  RefreshListDataSets();
}
//---------------------------------------------------------------------------


void __fastcall TDInvSheet::m_Q_assortmentsBeforeOpen(TDataSet *DataSet)
{
  m_Q_assortments->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_itemAfterInsert(DataSet);
  m_Q_itemDOC_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemDOC_NUMBER->AsInteger = StrToIntDef(GetDocNumber(DocTypeId),0);
  m_Q_itemFLDR_ID->AsInteger = m_new_fldr;
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_itemBeforeClose(TDataSet *DataSet)
{
  if (UpdateRegimeItem == urInsert && !ChangingItem) FreeDocNumber(DocTypeId,m_Q_itemDOC_NUMBER->AsString);
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_barcode_infoAfterClose(TDataSet *DataSet)
{
  m_Q_barcode_info->ParamByName("BARCODE")->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_lineAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_lineAfterInsert(DataSet);

  if (m_Q_barcode_info->Active && m_Q_barcode_info->RecordCount)
  {
    m_Q_lineNMCL_ID->AsInteger = m_Q_barcode_infoNMCL_ID->AsInteger;
    m_Q_lineNMCL_NAME->AsString = m_Q_barcode_infoNMCL_NAME->AsString;
    m_Q_lineASSORTMENT_ID->AsInteger = m_Q_barcode_infoASSORTMENT_ID->AsInteger;
    m_Q_lineASSORTMENT_NAME->AsString = m_Q_barcode_infoASSORTMENT_NAME->AsString;
  }
  m_Q_barcode_info->Close();

  m_Q_lineSTART_DATE->AsDateTime = GetSysDate();
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_linesAfterOpen(TDataSet *DataSet)
{
  if (int line_id = Params->Values["LINE_ID"])
  {
    Params->Values["LINE_ID"] = 0;
    try
    {
      if (m_Q_lines->Locate("LINE_ID",line_id,TLocateOptions()))
        switch (UpdateRegimeItem)
        {
          case urEdit: EditLine(line_id); break;
          case urView: ViewLine(line_id); break;
        }
    }
    __finally
    {
      Params->Values["LINE_ID"] = line_id;
    }
  }
}
//---------------------------------------------------------------------------

int __fastcall TDInvSheet::GetOrgId()
{
  return m_Q_list->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::SetOrgId(int p_value)
{
  if( GetOrgId() == p_value ) return;

  m_Q_list->ParamByName("ORG_ID")->AsInteger = p_value;

  if( m_Q_list->Active )
  {
    CloseListDataSets();
    OpenListDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_alccodeBeforeOpen(TDataSet *DataSet)
{
  m_Q_alccode->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_alccode->ParamByName("ASSORTMENT_ID")->AsInteger = m_Q_lineASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_alcoBeforeOpen(TDataSet *DataSet)
{
  m_Q_alco->ParamByName("DOC_ID")->AsInteger = m_Q_lineID->AsInteger;
  m_Q_alco->ParamByName("LINE_ID")->AsInteger = m_Q_lineLINE_ID->AsInteger;
  m_Q_alco->ParamByName("SUB_LINE_ID")->AsInteger = m_Q_alcSUB_LINE_ID->AsInteger;        
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_alcoAfterInsert(TDataSet *DataSet)
{
  m_Q_alcoID->AsInteger = m_Q_lineID->AsInteger;
  m_Q_alcoLINE_ID->AsInteger = m_Q_lineLINE_ID->AsInteger;
  m_Q_alcoSUB_LINE_ID->AsInteger = 100;        
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_alcBeforeOpen(TDataSet *DataSet)
{
  m_Q_alc->ParamByName("DOC_ID")->AsInteger = m_Q_lineID->AsInteger;
  m_Q_alc->ParamByName("LINE_ID")->AsInteger = m_Q_lineLINE_ID->AsInteger;        
}
//---------------------------------------------------------------------------

void __fastcall TDInvSheet::m_Q_alcAfterInsert(TDataSet *DataSet)
{
  m_Q_alcID->AsInteger = m_Q_lineID->AsInteger;
  m_Q_alcLINE_ID->AsInteger = m_Q_lineLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------


