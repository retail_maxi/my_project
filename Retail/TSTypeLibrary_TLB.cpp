// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// C++ TLBWRTR : $Revision:   1.134.1.41  $
// File generated on 15.07.2017 13:47:27 from Type Library described below.

// ************************************************************************ //
// Type Lib: L:\TSTypeLibrary\TSTypeLibrary.tlb (1)
// IID\LCID: {92D1487E-A494-401D-B723-2667DA9E97C5}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Parent TypeLibrary:
//   (0) v1.0 TSRetail, (P:\Modules\Retail\TSRetail.tlb)
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "TSTypeLibrary_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Tstypelibrary_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary                                      
// *********************************************************************//
const GUID LIBID_TSTypeLibrary = {0x92D1487E, 0xA494, 0x401D,{ 0xB7, 0x23, 0x26, 0x67, 0xDA, 0x9E, 0x97, 0xC5} };
const GUID IID_ITSForm = {0xD1FD8FFB, 0xA77E, 0x4158,{ 0x97, 0x64, 0x92, 0x46, 0x90, 0x66, 0x48, 0xBB} };
const GUID DIID_ITSFormEvents = {0xD370C277, 0xBEDD, 0x4DF8,{ 0xB3, 0xB0, 0x4A, 0x2A, 0xEC, 0x14, 0x55, 0x97} };
const GUID CLSID_CTSFormPattern = {0x4E91DF4A, 0x2C68, 0x4EBB,{ 0xB1, 0xD2, 0x71, 0xEA, 0x3F, 0x51, 0x25, 0x4C} };
const GUID IID_ITSItem = {0xDE4A116E, 0xA112, 0x490F,{ 0x81, 0x68, 0xC7, 0x8A, 0x5B, 0xF2, 0x0D, 0x6D} };
const GUID IID_ITSDocument = {0xB45F6BA0, 0x42D1, 0x4533,{ 0x8B, 0xF5, 0x2B, 0x64, 0x1D, 0x06, 0xB0, 0x92} };
const GUID IID_ITSRefbook = {0x0C56D9CB, 0xF0B5, 0x4242,{ 0xB5, 0x7F, 0x11, 0x03, 0xFF, 0x37, 0xA9, 0xFC} };
const GUID IID_ITSOperation = {0x8328023B, 0xD05F, 0x4162,{ 0x8C, 0xA0, 0xF4, 0x26, 0x47, 0x2A, 0x72, 0x90} };
const GUID IID_ITSReport = {0x32976479, 0x63A5, 0x4F9F,{ 0x99, 0xD6, 0x7A, 0x38, 0xD2, 0xC6, 0xE3, 0xED} };
const GUID IID_ITSParams = {0x4C9BAF7A, 0x32F0, 0x4452,{ 0xA8, 0x71, 0x4F, 0xB6, 0x12, 0xDD, 0xAC, 0xC3} };
const GUID IID_ITSProcess = {0x66077F88, 0xF036, 0x4331,{ 0xB4, 0xE1, 0x87, 0x07, 0x71, 0x5A, 0x61, 0x0E} };

};     // namespace Tstypelibrary_tlb
