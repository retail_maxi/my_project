inherited DFindKass: TDFindKass
  OldCreateOrder = False
  Left = 329
  Top = 276
  Height = 640
  Width = 870
  inherited m_DB_main: TDatabase
    AliasName = 'shop7'
  end
  inherited m_Q_main: TMLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM dual'
      ' '
      ' ')
    UpdateObject = nil
  end
  object m_Q_list: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT DISTINCT c.sale_doc_id AS sale_doc_id,'
      '                c.print_user_id AS print_user_id,'
      
        '                TRIM(RPAD(pkg_contractors.fnc_contr_name(c.print' +
        '_user_id), 250)) print_user_name,'
      '                c.check_sum AS check_sum,'
      '                c.host AS host'
      'FROM rtl_checks c,'
      '     rtl_zreports z'
      
        'WHERE c.check_date BETWEEN TO_DATE(:CHECK_DATE) AND TO_DATE(:CHE' +
        'CK_DATE) + 1'
      '  AND c.check_num = :CHECK_NUM'
      '  AND z.fiscal_num = :FISCAL_NUM'
      '  AND z.doc_id = c.report_doc_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' '
      ' '
      ' ')
    UpdateObject = m_U_main
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 24
    Top = 184
    ParamData = <
      item
        DataType = ftDate
        Name = 'CHECK_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'CHECK_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FISCAL_NUM'
        ParamType = ptInput
      end>
    object m_Q_listSALE_DOC_ID: TFloatField
      DisplayLabel = '�������'
      FieldName = 'SALE_DOC_ID'
      Origin = 'c.sale_doc_id'
    end
    object m_Q_listPRINT_USER_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'PRINT_USER_ID'
      Origin = 'c.print_user_id'
    end
    object m_Q_listPRINT_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'PRINT_USER_NAME'
      Origin = 'TRIM(RPAD(pkg_contractors.fnc_contr_name(c.print_user_id), 250))'
      Size = 250
    end
    object m_Q_listCHECK_SUM: TFloatField
      DisplayLabel = '�����'
      FieldName = 'CHECK_SUM'
      Origin = 'c.check_sum'
    end
    object m_Q_listHOST: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'c.host'
      Size = 64
    end
  end
  object m_Q_tmp_check_list: TMLQuery
    AutoCalcFields = False
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'WITH zc AS ('
      '    SELECT c.check_num,'
      '           z.fiscal_num,'
      '           c.sale_doc_id AS sale_doc_id,'
      '           c.print_user_id AS print_user_id,'
      
        '           TRIM(RPAD(pkg_contractors.fnc_contr_name(c.print_user' +
        '_id), 250)) print_user_name,'
      '           c.check_date as check_date'
      '    FROM rtl_checks c,'
      '         rtl_zreports z,'
      '         tmp_checks tmc'
      
        '    WHERE z.doc_id = c.report_doc_id AND tmc.fiscal_num = z.fisc' +
        'al_num AND tmc.check_num = c.check_num'
      ')'
      'SELECT * from ('
      'SELECT t.*,'
      
        '       (SELECT TRIM(RPAD(csl(pkg_contractors.fnc_contr_name(s.cn' +
        't_id)), 250))'
      '       FROM time_book tb, staffmen s, staff_table st'
      '       WHERE tb.symb_id in (14, 18)'
      '       AND tb.tb_date  = t.check_date'
      '       AND tb.staffman_id = s.id'
      '       AND s.staff_table_id = st.id'
      '       AND st.dep_id in (select id from departments'
      '                         start with id = t.dep_id'
      '                         connect by prior id = master_id)'
      
        '       AND (upper(st.position) like upper('#39'�������������%'#39') or s' +
        't.id = 2437)) as artt,'
      ''
      
        '       (SELECT TRIM(RPAD(csl(pkg_contractors.fnc_contr_name(s.cn' +
        't_id)), 250))'
      '       FROM time_book tb, staffmen s, staff_table st'
      '       WHERE tb.symb_id in (14, 18)'
      '       AND tb.tb_date  = t.check_date'
      '       AND tb.staffman_id = s.id'
      '       AND s.staff_table_id = st.id'
      '       AND st.dep_id in (select id from departments'
      '                         start with id = t.dep_id'
      '                         connect by prior id = master_id)'
      
        '       AND upper(st.position) = upper('#39'���������� �������� �����' +
        '��'#39')) as zks,'
      ''
      
        '       (SELECT TRIM(RPAD(csl(pkg_contractors.fnc_contr_name(s.cn' +
        't_id)), 250))'
      '       FROM time_book tb, staffmen s, staff_table st'
      '       WHERE tb.symb_id in (14, 18)'
      '       AND tb.tb_date  = t.check_date'
      '       AND tb.staffman_id = s.id'
      '       AND s.staff_table_id = st.id'
      '       AND st.dep_id in (select id from departments'
      '                         start with id = t.dep_id'
      '                         connect by prior id = master_id)'
      '       AND upper(st.position) = upper('#39'������� ������'#39')) as sk'
      'FROM '
      '('
      '    SELECT'
      '           tc.ID, '
      '           tc.fiscal_num,'
      '           tc.check_num,'
      '           tc.check_date,'
      '           tc.error_qnt,'
      '           tc.t_customer,'
      '           t_zc.print_user_name,'
      '           ('
      
        '             SELECT  max(st.position) FROM staffmen s, staff_tab' +
        'le st, departments d'
      '             WHERE s.cnt_id = t_zc.print_user_id'
      
        '                   AND s.id = (SELECT max(id) FROM staffmen WHER' +
        'E cnt_id = s.cnt_id AND TRUNC(tc.check_date) BETWEEN TRUNC(hire_' +
        'date) AND TRUNC(NVL(fire_date,tc.check_date)))'
      '                   AND s.staff_table_id = st.id'
      '                   AND st.dep_id = d.id'
      '           ) as post_name,'
      '           ('
      
        '             SELECT  max(d.name) FROM staffmen s, staff_table st' +
        ', departments d'
      '             WHERE s.cnt_id = t_zc.print_user_id'
      
        '                   AND s.id = (SELECT max(id) FROM staffmen WHER' +
        'E cnt_id = s.cnt_id AND TRUNC(tc.check_date) BETWEEN TRUNC(hire_' +
        'date) AND TRUNC(NVL(fire_date,tc.check_date)))'
      '                   AND s.staff_table_id = st.id'
      '                   AND st.dep_id = d.id'
      '           ) as dep_name,'
      '           ('
      
        '             SELECT  decode(max(d.id), 2004, 2004, max(d.master_' +
        'id)) FROM staffmen s, staff_table st, departments d'
      '             WHERE s.cnt_id = t_zc.print_user_id'
      
        '                   AND s.id = (SELECT max(id) FROM staffmen WHER' +
        'E cnt_id = s.cnt_id AND TRUNC(tc.check_date) BETWEEN TRUNC(hire_' +
        'date) AND TRUNC(NVL(fire_date,tc.check_date)))'
      '                   AND s.staff_table_id = st.id'
      '                   AND st.dep_id = d.id'
      '           ) as dep_id'
      '    FROM tmp_checks tc,'
      '         zc t_zc'
      '    WHERE t_zc.check_num (+) = tc.check_num'
      '          and TRUNC(t_zc.check_date(+)) = TRUNC(tc.check_date)'
      '          and t_zc.fiscal_num(+) = tc.fiscal_num'
      '    ORDER BY ID'
      ') t) tt'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION '
      ' '
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 40
    Top = 240
    object m_Q_tmp_check_listID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_tmp_check_listFISCAL_NUM: TFloatField
      DisplayLabel = '� ��'
      FieldName = 'FISCAL_NUM'
      Origin = 'fiscal_num'
    end
    object m_Q_tmp_check_listCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'check_num'
    end
    object m_Q_tmp_check_listCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'check_date'
    end
    object m_Q_tmp_check_listERROR_QNT: TFloatField
      DisplayLabel = '���-�� ������'
      DisplayWidth = 3
      FieldName = 'ERROR_QNT'
      Origin = 'error_qnt'
    end
    object m_Q_tmp_check_listT_CUSTOMER: TStringField
      DisplayLabel = '�. ����������'
      DisplayWidth = 80
      FieldName = 'T_CUSTOMER'
      Origin = 't_customer'
      Size = 120
    end
    object m_Q_tmp_check_listPRINT_USER_NAME: TStringField
      DisplayLabel = '���'
      DisplayWidth = 120
      FieldName = 'PRINT_USER_NAME'
      Origin = 'print_user_name '
      Size = 250
    end
    object m_Q_tmp_check_listDEP_NAME: TStringField
      DisplayLabel = '�������������'
      DisplayWidth = 36
      FieldName = 'DEP_NAME'
      Origin = 'DEP_NAME'
      FixedChar = True
      Size = 4
    end
    object m_Q_tmp_check_listPOST_NAME: TStringField
      DisplayLabel = '���������'
      DisplayWidth = 36
      FieldName = 'POST_NAME'
      Origin = 'POST_NAME'
      FixedChar = True
      Size = 4
    end
    object m_Q_tmp_check_listARTT: TStringField
      DisplayLabel = '����'
      FieldName = 'ARTT'
      Origin = 'ARTT'
      Size = 250
    end
    object m_Q_tmp_check_listZKS: TStringField
      DisplayLabel = '���'
      FieldName = 'ZKS'
      Origin = 'ZKS'
      Size = 250
    end
    object m_Q_tmp_check_listSK: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'SK'
      Origin = 'SK'
      Size = 250
    end
  end
  object m_Q_tmp_check_ins: TMLQuery
    AutoCalcFields = False
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        '  INSERT INTO tmp_checks (ID,  check_num,  fiscal_num,  check_da' +
        'te,  error_qnt,  t_customer )'
      
        '  VALUES ( :id, :check_num,  :fiscal_num,  TO_DATE(:check_date, ' +
        #39'DD.MM.YYYY'#39'),  :error_qnt,  :t_customer )')
    Macros = <>
    Left = 152
    Top = 240
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'check_num'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'fiscal_num'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'check_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'error_qnt'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 't_customer'
        ParamType = ptInput
        Value = ''
      end>
  end
  object m_Q_tmp_check_del: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'DELETE FROM tmp_checks')
    Macros = <>
    Left = 280
    Top = 240
  end
end
