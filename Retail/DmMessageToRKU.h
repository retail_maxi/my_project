//---------------------------------------------------------------------------

#ifndef DmMessageToRKUH
#define DmMessageToRKUH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMDocument.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENT.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDMessageToRKU : public TTSDDocument
{
__published:	// IDE-managed Components
    TFloatField *m_Q_listCREATE_USER_ID;
    TStringField *m_Q_itemMESSAGE_TYPE;
    TStringField *m_Q_itemHOST;
    TDateTimeField *m_Q_itemSEND_DATE;
    TFloatField *m_Q_itemCREATE_USER_ID;
    TStringField *m_Q_listMESSAGE_TYPE;
    TStringField *m_Q_listHOST;
    TFloatField *m_Q_listRKU;
    TDateTimeField *m_Q_listSEND_DATE;
    TMLQuery *m_Q_type_mess_list;
    TFloatField *m_Q_type_mess_listID;
    TStringField *m_Q_type_mess_listNAME;
    TFloatField *m_Q_itemTYPE_MESSAGE_ID;
    TMLQuery *m_Q_user_list;
    TFloatField *m_Q_user_listCNT_ID;
    TStringField *m_Q_user_listCNT_NAME;
    TStringField *m_Q_itemCREATE_USER_NAME;
    TStringField *m_Q_listCREATE_USER_NAME;
    TQuery *m_Q_get_cnt_id;
    TFloatField *m_Q_get_cnt_idCNT_ID;
    TFloatField *m_Q_itemDOC_NUMBER;
    TFloatField *m_Q_listDOC_NUMBER;
    TDateTimeField *m_Q_itemDOC_DATE;
    TStringField *m_Q_itemNOTE;
    TStringField *m_Q_itemRKU;
    TQuery *m_Q_rku_list;
    TFloatField *m_Q_rku_listRKU;
    TStringField *m_Q_rku_listIS_READ;
    TQuery *m_Q_items_clear;
    TQuery *m_Q_items_add;
    TMLQuery *m_Q_not_read;
    TFloatField *m_Q_not_readCNT_ID;
    TStringField *m_Q_not_readCNT_NAME;
    TFloatField *m_Q_not_readRKU;
    TStringField *m_Q_not_readIS_READ;
    TDateTimeField *m_Q_not_readREAD_DATE;
   TMLQuery *m_Q_resend;
   TQuery *m_Q_resend_doc;
    void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
    void __fastcall m_Q_itemBeforeClose(TDataSet *DataSet);
    void __fastcall m_Q_rku_listBeforeOpen(TDataSet *DataSet);
    void __fastcall m_Q_not_readBeforeOpen(TDataSet *DataSet);
private:	// User declarations
    int __fastcall GetCntId();
    int is_read;
    int cnt_RKU;
public:
    __property int IsRead = {read = is_read, write = is_read};
    __property int CntRKU = {read = cnt_RKU};
public:		// User declarations
    __fastcall TDMessageToRKU(TComponent* p_owner, AnsiString p_prog_id);
    void __fastcall ItemsClear();
    void __fastcall ItemsAdd(int p_rku);

ML_BEGIN_DATA_SETS
	ML_BASE_DATA_SETS(TTSDDocument)
	ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_not_read)
ML_END_DATA_SETS
};
//---------------------------------------------------------------------------
#endif
