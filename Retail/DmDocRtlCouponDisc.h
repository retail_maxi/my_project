//---------------------------------------------------------------------------

#ifndef DmDocRtlCouponDiscH
#define DmDocRtlCouponDiscH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDDocRtlCouponDisc : public TTSDDocumentWithLines
{
__published:
  TMLQuery *m_Q_nmcl_list;
  TFloatField *m_Q_nmcl_listNMCL_ID;
  TStringField *m_Q_nmcl_listNMCL_NAME;
  TMLQuery *m_Q_assort;
  TFloatField *m_Q_assortID;
  TStringField *m_Q_assortNAME;
        TFloatField *m_Q_listDOC_NUMBER;
        TDateTimeField *m_Q_listDOC_DATE;
        TDateTimeField *m_Q_listBEGIN_DATE;
        TDateTimeField *m_Q_listEND_DATE;
        TStringField *m_Q_listNOTE;
        TFloatField *m_Q_itemDOC_NUMBER;
        TDateTimeField *m_Q_itemDOC_DATE;
        TDateTimeField *m_Q_itemBEGIN_DATE;
        TDateTimeField *m_Q_itemEND_DATE;
        TFloatField *m_Q_lineASSORT_ID;
        TFloatField *m_Q_lineNMCL_ID;
        TFloatField *m_Q_linePERCENT_DISC;
        TFloatField *m_Q_linesASSORT_ID;
        TFloatField *m_Q_linesNMCL_ID;
        TFloatField *m_Q_linesCOUPON_NUM;
        TStringField *m_Q_linesBAR_CODE;
        TFloatField *m_Q_linesPERCENT_DISC;
        TFloatField *m_Q_lineCOUPON_NUM;
        TStringField *m_Q_lineBAR_CODE;
        TStringField *m_Q_linesASSORT_NAME;
        TStringField *m_Q_linesNMCL_NAME;
        TStringField *m_Q_lineASSORT_NAME;
        TStringField *m_Q_lineNMCL_NAME;
        TMLQuery *m_Q_orgs;
        TFloatField *m_Q_orgsORG_ID;
        TStringField *m_Q_orgsORG_NAME;
        TFloatField *m_Q_lineLIMIT_QNT;
        TFloatField *m_Q_linesLIMIT_QNT;
        TMLQuery *m_Q_upd_date;
        TMLQuery *m_Q_set_date;
        TMLUpdateSQL *m_U_set_date;
        TDateTimeField *m_Q_set_dateEND_DATE;
        TFloatField *m_Q_linesPRICE_DISC;
        TFloatField *m_Q_linePRICE_DISC;
   TMLQuery *m_Q_orgs_name;
   TStringField *m_Q_orgs_nameNAME_ORG;
   TStringField *m_Q_listDEST_ORG_ID;
   TStringField *m_Q_listDEST_ORG_NAME;
   TStringField *m_Q_itemNOTE;
  void __fastcall m_Q_nmclsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_assortBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_lineAfterInsert(TDataSet *DataSet);
   void __fastcall m_Q_orgs_nameBeforeOpen(TDataSet *DataSet);

private:
  TS_DOC_LINE_SQ_NAME("SQ_DOC_RTL_COUPON_DISC_ITEMS")
  TDateTime __fastcall GetBeginDate() const {return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;};
  TDateTime __fastcall GetEndDate() const {return m_Q_list->ParamByName("END_DATE")->AsDateTime;};

public:
  __property TDateTime BeginDate = { read = GetBeginDate };
  __property TDateTime EndDate = { read = GetEndDate };

public:
  __fastcall TDDocRtlCouponDisc(TComponent* p_owner, AnsiString p_prog_id);
  __fastcall ~TDDocRtlCouponDisc();
  void __fastcall SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date);


};
//---------------------------------------------------------------------------
#endif
