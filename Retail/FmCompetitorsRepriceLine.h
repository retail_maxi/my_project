//---------------------------------------------------------------------------

#ifndef FmCompetitorsRepriceLineH
#define FmCompetitorsRepriceLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmCompetitorsReprice.h"
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFCompetitorsRepriceLine : public TTSFDocumentLine
{
  __published:	// IDE-managed Components
   TLabel *m_L_nmcl;
   TLabel *m_L_competitor;
   TLabel *m_L_comp_price;
   TMLDBPanel *m_DBE_nmcl;
   TMLDBPanel *m_DBE_NMCL_NAME;
   TDBEdit *m_DBE_COMP_PRICE;
   TLabel *m_L_rub;
   TDataSource *m_DS_comp;
   void __fastcall FormShow(TObject *Sender);
private:	// User declarations
   TDCompetitorsReprice *m_dm;
protected:
  virtual AnsiString __fastcall BeforeLineApply(TWinControl *p_focused);
public:		// User declarations
  __fastcall TFCompetitorsRepriceLine(TComponent* p_owner,
                                 TTSDDocumentWithLines *p_dm_doc_with_ln):
            TTSFDocumentLine(p_owner,p_dm_doc_with_ln),
            m_dm(static_cast<TDCompetitorsReprice*>(DMDocumentWithLines)){};
};
//---------------------------------------------------------------------------
#endif
