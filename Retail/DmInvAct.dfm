inherited DInvAct: TDInvAct
  OldCreateOrder = False
  Left = 529
  Top = 282
  Height = 657
  Width = 1408
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=ROBOT'
      'PASSWORD=RBT')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'select * from ('
      'select id,'
      '       doc_src_org_id,'
      '       doc_src_org_name,'
      '       doc_create_date,'
      '       doc_create_comp_name,'
      '       doc_create_user,'
      '       doc_modify_date,'
      '       doc_modify_comp_name,'
      '       doc_modify_user,'
      '       doc_status,'
      '       doc_status_name,'
      '       doc_status_change_date,'
      '       doc_fldr_id,'
      '       doc_fldr_name,'
      '       doc_fldr_right,'
      '       doc_number,'
      '       doc_date,'
      '       def_start_date,'
      '       note note,'
      '       sumin,'
      '       sumout,'
      '       note_non_prepare,'
      '       gr_dep_name,'
      '       max_err_a, '
      '       max_err_b,'
      '       note_auditor,'
      '       reason_id,'
      '       reason_name,'
      '       doc_num_recalc,'
      '       case '
      '          WHEN (recalc_items = 0) then 0'
      
        '          WHEN (t<>0) and (nvl((err_items_A/nullif(recalc_items_' +
        'A,0)*100),0) < 25 ) then round(700*A/t) '
      '          ELSE 0'
      '       end --����� ��������� �� ������� ������ �'
      '       +'
      '       case '
      '          WHEN (recalc_items = 0) then 0'
      
        '          WHEN (t<>0) and (nvl((err_items_B/nullif(recalc_items_' +
        'B,0)*100),0) <= 6) then round(700*B/t) '
      
        '          WHEN (t<>0) and (nvl((err_items_B/nullif(recalc_items_' +
        'B,0)*100),0) > 6 and '
      
        '                             nvl((err_items_B/nullif(recalc_item' +
        's_B,0)*100),0) <= 7) then round(490*B/t) '
      
        '          WHEN (t<>0) and (nvl((err_items_B/nullif(recalc_items_' +
        'B,0)*100),0) > 7 and '
      
        '                             nvl((err_items_B/nullif(recalc_item' +
        's_B,0)*100),0) <= 9) then round(210*B/t) '
      '          ELSE 0'
      '       end --����� ��������� �� ������� ������ � '
      '       as bonus_counter_AB, '
      '       '
      '       case '
      '          WHEN (recalc_items = 0) then 0'
      
        '          WHEN (t<>0) and (nvl((err_items_A/nullif(recalc_items_' +
        'A,0)*100),0) >= 25) then round(700*A/t)'
      '          ELSE 0'
      '       end -- ����� �������� �� ������� ������ �      '
      '       +'
      '       case '
      '          WHEN (recalc_items = 0) then 0'
      
        '          WHEN (t<>0) and (nvl((err_items_B/nullif(recalc_items_' +
        'B,0)*100),0) > 9) then round(700*B/t)'
      
        '          WHEN (t<>0) and (nvl((err_items_B/nullif(recalc_items_' +
        'B,0)*100),0) > 7 and '
      
        '                             nvl((err_items_B/nullif(recalc_item' +
        's_B,0)*100),0) <= 9)then round(490*B/t) '
      
        '          WHEN (t<>0) and (nvl((err_items_B/nullif(recalc_items_' +
        'B,0)*100),0) > 6 and '
      
        '                             nvl((err_items_B/nullif(recalc_item' +
        's_B,0)*100),0) <= 7)then round(210*B/t) '
      '          ELSE 0'
      '       end -- ����� �������� �� ������� ������ �'
      '       as bonus_auditor_AB '
      'from('
      'select '
      '  d.id,'
      '  d.src_org_id doc_src_org_id,'
      '  o.name doc_src_org_name,'
      '  d.create_date doc_create_date,'
      '  d.comp_name doc_create_comp_name,'
      '  d.author doc_create_user,'
      '  d.modify_date doc_modify_date,'
      '  d.modify_comp_name doc_modify_comp_name,'
      '  d.modify_user doc_modify_user,'
      '  d.status doc_status,'
      
        '  decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �� ���' +
        '���'#39','#39'������'#39') doc_status_name,'
      '  d.status_change_date doc_status_change_date,'
      '  d.fldr_id doc_fldr_id,'
      '  f.name doc_fldr_name,'
      
        '  substr(fnc_mask_folder_right(f.id,:ACCESS_MASK),1,250) doc_fld' +
        'r_right,'
      '  a.doc_number doc_number,'
      '  a.doc_date doc_date,'
      '  a.def_start_date,'
      '  a.note note,'
      '  a.sumin,'
      '  a.sumout,'
      '  a.note_non_prepare,'
      '  gd.name gr_dep_name,'
      '  a.max_err_a, '
      '  a.max_err_b,'
      '  decode(a.note_auditor, null, null, '#39'��������'#39') note_auditor,'
      '  a.reason_id,'
      '  r.name as reason_name,'
      '  a.doc_num_recalc,'
      '  '
      '  (SELECT count(*)'
      '              FROM rtl_inv_act_items i'
      
        '              WHERE i.doc_id = a.doc_id AND i.recalc_items IS NO' +
        'T NULL) AS recalc_items,'
      '              (SELECT count(*) '
      '              FROM rtl_inv_act_items i,'
      '                   nomenclature_items ni,'
      '                   measure_units mu,'
      '                   rtl_subcategories sct,'
      '                   rtl_subsubcategories ssct,'
      '                   rtl_classifier_rs rcr'
      '              WHERE i.doc_id = a.doc_id'
      '              AND i.recalc_items IS NOT NULL'
      '              and mu.id = ni.meas_unit_id'
      '              AND ssct.subcat_id = sct.id'
      '              AND rcr.subsubcat_id = ssct.id'
      '              AND rcr.nmcl_id = i.nmcl_id'
      '              AND rcr.assort_id = i.assortment_id'
      '              AND ni.id = i.nmcl_id'
      
        '              AND (sct.id in (2070,2072,2073,2077,2081,2145,2146' +
        ',3387,3388,3389,3390,3391,3392) or ((sct.id = 2080) and (mu.id =' +
        ' 6)) or ((sct.id in (2083,2088)) and (mu.id != 1)))) as recalc_i' +
        'tems_A, --���������� ����������� ������� ������ �'
      '              (SELECT count(*) '
      '              FROM rtl_inv_act_items i,'
      '                   nomenclature_items ni,'
      '                   measure_units mu,'
      '                   rtl_subcategories sct,'
      '                   rtl_subsubcategories ssct,'
      '                   rtl_classifier_rs rcr'
      '              WHERE i.doc_id = a.doc_id'
      '              AND i.recalc_items IS NOT NULL'
      '              and mu.id = ni.meas_unit_id'
      '              AND ssct.subcat_id = sct.id'
      '              AND rcr.subsubcat_id = ssct.id'
      '              AND rcr.nmcl_id = i.nmcl_id'
      '              AND rcr.assort_id = i.assortment_id'
      '              AND ni.id = i.nmcl_id'
      
        '              AND not (sct.id in (2070,2072,2073,2077,2081,2145,' +
        '2146,3387,3388,3389,3390,3391,3392) or ((sct.id = 2080) and (mu.' +
        'id = 6)) or ((sct.id in (2083,2088)) and (mu.id != 1)))) as reca' +
        'lc_items_B, --���������� ����������� ������� ������ �'
      '              (SELECT count(*) '
      '              FROM rtl_inv_act_items i,'
      '                   nomenclature_items ni,'
      '                   measure_units mu,'
      '                   rtl_subcategories sct,'
      '                   rtl_subsubcategories ssct,'
      '                   rtl_classifier_rs rcr'
      '              WHERE i.doc_id = a.doc_id '
      '              AND i.recalc_items IS NOT NULL'
      '              AND i.recalc_items <> nvl(i.fact_items,0)'
      '              and mu.id = ni.meas_unit_id'
      '              AND ssct.subcat_id = sct.id'
      '              AND rcr.subsubcat_id = ssct.id'
      '              AND rcr.nmcl_id = i.nmcl_id'
      '              AND rcr.assort_id = i.assortment_id'
      '              AND ni.id = i.nmcl_id'
      
        '              AND (sct.id in (2070,2072,2073,2077,2081,2145,2146' +
        ',3387,3388,3389,3390,3391,3392) or ((sct.id = 2080) and (mu.id =' +
        ' 6)) or ((sct.id in (2083,2088)) and (mu.id != 1)))) as err_item' +
        's_A, --���������� ������ �� ������� �� ������ �'
      '              (SELECT count(*)  '
      '              FROM rtl_inv_act_items i,'
      '                   nomenclature_items ni,'
      '                   measure_units mu,'
      '                   rtl_subcategories sct,'
      '                   rtl_subsubcategories ssct,'
      '                   rtl_classifier_rs rcr'
      '              WHERE i.doc_id = a.doc_id '
      '              AND i.recalc_items IS NOT NULL'
      '              AND i.recalc_items <> nvl(i.fact_items,0)'
      '              and mu.id = ni.meas_unit_id'
      '              AND ssct.subcat_id = sct.id'
      '              AND rcr.subsubcat_id = ssct.id'
      '              AND rcr.nmcl_id = i.nmcl_id'
      '              AND rcr.assort_id = i.assortment_id'
      '              AND ni.id = i.nmcl_id'
      
        '              AND not (sct.id in (2070,2072,2073,2077,2081,2145,' +
        '2146,3387,3388,3389,3390,3391,3392) or ((sct.id = 2080) and (mu.' +
        'id = 6)) or ((sct.id in (2083,2088)) and (mu.id != 1)))) as err_' +
        'items_B, --���������� ������ �� ������� �� ������ �'
      '              (SELECT count(*) '
      '              FROM rtl_inv_act_items i'
      
        '              WHERE i.doc_id = a.doc_id) as t, -- ���������� ���' +
        '���� ���������� � ��� ��������������'
      '              (SELECT count(*) '
      '              FROM rtl_inv_act_items i,'
      '                   nomenclature_items ni,'
      '                   measure_units mu,'
      '                   rtl_subcategories sct,'
      '                   rtl_subsubcategories ssct,'
      '                   rtl_classifier_rs rcr'
      '              WHERE i.doc_id = a.doc_id'
      '              and mu.id = ni.meas_unit_id'
      '              AND ssct.subcat_id = sct.id'
      '              AND rcr.subsubcat_id = ssct.id'
      '              AND rcr.nmcl_id = i.nmcl_id'
      '              AND rcr.assort_id = i.assortment_id'
      '              AND ni.id = i.nmcl_id'
      
        '              AND (sct.id in (2070,2072,2073,2077,2081,2145,2146' +
        ',3387,3388,3389,3390,3391,3392) or ((sct.id = 2080) and (mu.id =' +
        ' 6)) or ((sct.id in (2083,2088)) and (mu.id != 1)))) as A, -- ��' +
        '�������� ������� ���������� � ��� �������������� (�� ������ �)'
      '              (SELECT count(*) '
      '              FROM rtl_inv_act_items i,'
      '                   nomenclature_items ni,'
      '                   measure_units mu,'
      '                   rtl_subcategories sct,'
      '                   rtl_subsubcategories ssct,'
      '                   rtl_classifier_rs rcr'
      '              WHERE i.doc_id = a.doc_id'
      '              and mu.id = ni.meas_unit_id'
      '              AND ssct.subcat_id = sct.id'
      '              AND rcr.subsubcat_id = ssct.id'
      '              AND rcr.nmcl_id = i.nmcl_id'
      '              AND rcr.assort_id = i.assortment_id'
      '              AND ni.id = i.nmcl_id'
      
        '              AND not (sct.id in (2070,2072,2073,2077,2081,2145,' +
        '2146,3387,3388,3389,3390,3391,3392) or ((sct.id = 2080) and (mu.' +
        'id = 6)) or ((sct.id in (2083,2088)) and (mu.id != 1)))) as B --' +
        ' ���������� ������� ���������� � ��� �������������� (�� ������ �' +
        ')          '
      'from '
      '  documents d,'
      '  folders f,'
      '  organizations o,'
      '  rtl_inv_acts a,'
      '  groups_of_depositories gd,'
      '  rtl_inv_act_reason r'
      'where '
      '  f.doc_type_id = :DOC_TYPE_ID'
      '  and f.id = d.fldr_id'
      '  and o.id = d.src_org_id'
      '  and fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '  and a.doc_id = d.id'
      '  and a.doc_date between :BEGIN_DATE and :END_DATE'
      '  and gd.id = a.gr_dep_id'
      
        '  AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_id = :ORG_' +
        'ID))'
      '  and a.reason_id = r.reason_id(+)))'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 32
    Top = 136
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 'ID'
    end
    inherited m_Q_listDOC_SRC_ORG_ID: TFloatField
      Origin = 'DOC_SRC_ORG_ID'
    end
    inherited m_Q_listDOC_SRC_ORG_NAME: TStringField
      Origin = 'DOC_SRC_ORG_NAME'
    end
    inherited m_Q_listDOC_CREATE_DATE: TDateTimeField
      Origin = 'DOC_CREATE_DATE'
    end
    inherited m_Q_listDOC_CREATE_COMP_NAME: TStringField
      Origin = 'DOC_CREATE_COMP_NAME'
    end
    inherited m_Q_listDOC_CREATE_USER: TStringField
      Origin = 'DOC_CREATE_USER'
    end
    inherited m_Q_listDOC_MODIFY_DATE: TDateTimeField
      Origin = 'DOC_MODIFY_DATE'
    end
    inherited m_Q_listDOC_MODIFY_COMP_NAME: TStringField
      Origin = 'DOC_MODIFY_COMP_NAME'
    end
    inherited m_Q_listDOC_MODIFY_USER: TStringField
      Origin = 'DOC_MODIFY_USER'
    end
    inherited m_Q_listDOC_STATUS: TStringField
      Origin = 'DOC_STATUS'
    end
    inherited m_Q_listDOC_STATUS_NAME: TStringField
      Origin = 'DOC_STATUS_NAME'
    end
    inherited m_Q_listDOC_STATUS_CHANGE_DATE: TDateTimeField
      Origin = 'DOC_STATUS_CHANGE_DATE'
    end
    inherited m_Q_listDOC_FLDR_ID: TFloatField
      Origin = 'DOC_FLDR_ID'
    end
    inherited m_Q_listDOC_FLDR_NAME: TStringField
      Origin = 'DOC_FLDR_NAME'
    end
    inherited m_Q_listDOC_FLDR_RIGHT: TStringField
      Origin = 'DOC_FLDR_RIGHT'
    end
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '� ����'
      DisplayWidth = 10
      FieldName = 'DOC_NUMBER'
      Origin = 'DOC_NUMBER'
      Size = 100
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'DOC_DATE'
      Origin = 'DOC_DATE'
    end
    object m_Q_listDEF_START_DATE: TDateTimeField
      DisplayLabel = '���� ��������������'
      FieldName = 'DEF_START_DATE'
      Origin = 'DEF_START_DATE'
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 30
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 250
    end
    object m_Q_listGR_DEP_NAME: TStringField
      DisplayLabel = '�����'
      DisplayWidth = 30
      FieldName = 'GR_DEP_NAME'
      Origin = 'GR_DEP_NAME'
      Size = 100
    end
    object m_Q_listSUMIN: TFloatField
      DisplayLabel = '�����*'
      FieldName = 'SUMIN'
      Origin = 'SUMIN'
      currency = True
    end
    object m_Q_listSUMOUT: TFloatField
      DisplayLabel = '�����'
      FieldName = 'SUMOUT'
      Origin = 'SUMOUT'
      currency = True
    end
    object m_Q_listNOTE_NON_PREPARE: TStringField
      DisplayLabel = '���������� �� ������������'
      FieldName = 'NOTE_NON_PREPARE'
      Origin = 'NOTE_NON_PREPARE'
      Size = 250
    end
    object m_Q_listMAX_ERR_A: TFloatField
      DisplayLabel = ' ����.��. � %'
      FieldName = 'MAX_ERR_A'
      Origin = 'MAX_ERR_A'
    end
    object m_Q_listMAX_ERR_B: TFloatField
      DisplayLabel = '����.��. � %'
      FieldName = 'MAX_ERR_B'
      Origin = 'MAX_ERR_B'
    end
    object m_Q_listNOTE_AUDITOR: TStringField
      DisplayLabel = '����� ��������'
      FieldName = 'NOTE_AUDITOR'
      Origin = 'NOTE_AUDITOR'
      Size = 8
    end
    object m_Q_listREASON_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'REASON_ID'
      Origin = 'REASON_ID'
    end
    object m_Q_listREASON_NAME: TStringField
      DisplayLabel = '�������'
      FieldName = 'REASON_NAME'
      Origin = 'REASON_NAME'
      Size = 50
    end
    object m_Q_listDOC_NUM_RECALC: TStringField
      DisplayLabel = '� ���� ���������'
      FieldName = 'DOC_NUM_RECALC'
      Origin = 'DOC_NUM_RECALC'
      Size = 100
    end
    object m_Q_listBONUS_COUNTER_AB: TFloatField
      DisplayLabel = '����� ����� ���������'
      FieldName = 'BONUS_COUNTER_AB'
      Origin = 'BONUS_COUNTER_AB'
    end
    object m_Q_listBONUS_AUDITOR_AB: TFloatField
      DisplayLabel = '����� ����� ��������'
      FieldName = 'BONUS_AUDITOR_AB'
      Origin = 'BONUS_AUDITOR_AB'
    end
  end
  inherited m_Q_item: TMLQuery
    BeforeClose = m_Q_itemBeforeClose
    SQL.Strings = (
      'select id,'
      '       doc_number,'
      '       doc_date,'
      '       def_start_date,'
      '       note,'
      '       note_non_prepare,'
      '       diff_amt_out,'
      '       gr_dep_id,'
      '       gr_dep_name,'
      '       doc_status,'
      
        '       round(recalc_qnt/nullif(start_qnt,0)*100,2)  AS recalc_pe' +
        'rcent,'
      '       max_err_a,'
      '       max_err_b,'
      '       note_auditor,'
      '       t.fldr_id,'
      '       reason_id,'
      '       reason_name,'
      '       doc_num_recalc'
      'from ('
      'select '
      '  a.doc_id id,'
      '  a.doc_number,'
      '  a.doc_date,'
      '  a.def_start_date,'
      '  a.note,'
      '  a.note_non_prepare,'
      
        '  (select sum((fact_items-start_items)*out_price) from rtl_inv_a' +
        'ct_items where doc_id = a.doc_id) diff_amt_out,'
      '  a.gr_dep_id,'
      '  gd.name gr_dep_name,'
      '  d.status AS doc_status,'
      '   (select count(*)'
      '  from  rtl_inv_act_items '
      '  where doc_id = a.doc_id'
      
        '  and ((start_items) != 0 or (start_items = 0 and fact_items != ' +
        '0))) AS start_qnt,'
      '   (select count(*)'
      '  from  rtl_inv_act_items '
      '  where doc_id = a.doc_id'
      '  and nvl(was_recalc,0) = 1'
      
        '  and ((start_items) != 0 or (start_items = 0 and fact_items != ' +
        '0))) AS recalc_qnt , '
      '  d.fldr_id,'
      '  a.max_err_a,'
      '  a.max_err_b,'
      '  a.note_auditor,'
      '  a.reason_id,'
      '  r.name as reason_name,'
      '  a.doc_num_recalc'
      'from '
      
        '  rtl_inv_acts a, documents d, groups_of_depositories gd, rtl_in' +
        'v_act_reason r'
      'where '
      '  a.doc_id = :ID'
      '  and d.id = a.doc_id'
      
        '  and :ID = (select id from documents where id = :ID and fldr_id' +
        ' = :FLDR_ID)'
      '  and gd.id = a.gr_dep_id'
      '  and a.reason_id = r.reason_id(+)'
      ') t')
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 100
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemDEF_START_DATE: TDateTimeField
      FieldName = 'DEF_START_DATE'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemDIFF_AMT_OUT: TFloatField
      FieldName = 'DIFF_AMT_OUT'
      currency = True
    end
    object m_Q_itemGR_DEP_ID: TFloatField
      FieldName = 'GR_DEP_ID'
    end
    object m_Q_itemGR_DEP_NAME: TStringField
      FieldName = 'GR_DEP_NAME'
      Size = 100
    end
    object m_Q_itemDOC_STATUS: TStringField
      FieldName = 'DOC_STATUS'
      FixedChar = True
      Size = 1
    end
    object m_Q_itemRECALC_PERCENT: TFloatField
      FieldName = 'RECALC_PERCENT'
    end
    object m_Q_itemNOTE_NON_PREPARE: TStringField
      FieldName = 'NOTE_NON_PREPARE'
      Size = 250
    end
    object m_Q_itemMAX_ERR_A: TFloatField
      FieldName = 'MAX_ERR_A'
    end
    object m_Q_itemMAX_ERR_B: TFloatField
      FieldName = 'MAX_ERR_B'
    end
    object m_Q_itemNOTE_AUDITOR: TMemoField
      FieldName = 'NOTE_AUDITOR'
      BlobType = ftMemo
      Size = 3000
    end
    object m_Q_itemFLDR_ID: TFloatField
      FieldName = 'FLDR_ID'
    end
    object m_Q_itemREASON_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'REASON_ID'
      Origin = 'reason_id'
    end
    object m_Q_itemREASON_NAME: TStringField
      DisplayLabel = '�������'
      FieldName = 'REASON_NAME'
      Origin = 'reason_id'
      Size = 50
    end
    object m_Q_itemDOC_NUM_RECALC: TStringField
      DisplayLabel = '� ���� ���������'
      FieldName = 'DOC_NUM_RECALC'
      Origin = 'DOC_NUM_RECALC'
      Size = 100
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_inv_acts'
      'set def_start_date = :DEF_START_DATE, '
      '      note = :NOTE, '
      '      gr_dep_id = :GR_DEP_ID,'
      '      note_non_prepare = :NOTE_NON_PREPARE,'
      '      note_auditor = :NOTE_AUDITOR,'
      '      reason_id = :REASON_ID,'
      '      doc_num_recalc = :DOC_NUM_RECALC'
      'where doc_id = :OLD_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_inv_acts (doc_id,doc_number,doc_date,def_start_d' +
        'ate,note,gr_dep_id,cat_based, note_non_prepare, note_auditor, re' +
        'ason_id, doc_num_recalc)'
      
        'values (:ID,:DOC_NUMBER,:DOC_DATE,:DEF_START_DATE,:NOTE,:GR_DEP_' +
        'ID,0, :NOTE_NON_PREPARE, :NOTE_AUDITOR, :REASON_ID, :DOC_NUM_REC' +
        'ALC)')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      'END;')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select * from ('
      'select '
      '  i.doc_id id,'
      '  i.line_id line_id,'
      '  i.start_date,  '
      '  i.nmcl_id nmcl_id,'
      '  ni.name nmcl_name,'
      '  a.name assortment_name,'
      '  mu.short_name mu_name,'
      '  i.in_price,'
      '  i.out_price,'
      '  i.start_items,'
      '  i.fact_items,'
      '  nvl(i.recalc_items,i.fact_items) - i.start_items diff_items,'
      
        '  (nvl(i.recalc_items,i.fact_items) - i.start_items)*i.in_price ' +
        'diff_amt_in,'
      
        '  (nvl(i.recalc_items,i.fact_items) - i.start_items)*i.out_price' +
        ' diff_amt_out,'
      '  i.card_id,'
      '  i.note,'
      '  i.recalc_items,'
      '  i.non_prepare,'
      '  nvl(i.is_recalc,0) is_recalc,'
      '  i.fact_from,'
      '  i.fact_to,'
      '  ('
      '    select MAX(r.coef)'
      '    from rtl_prod_translate_rku r'
      '    WHERE r.to_nmcl_id = i.nmcl_id'
      '    AND r.to_assort_id = i.assortment_id'
      '    AND r.to_gr_dep_id = ri.gr_dep_id'
      '   ) as trans_rku_coef,'
      '  i.prodpp,'
      '  ct.group_id,'
      '  g.name as gname,'
      '  sct.cat_id,'
      '  ct.name as ctname,'
      '  ssct.subcat_id,'
      '  sct.name as sctname,'
      
        '  case when sct.id in (2070, 2072, 2073, 2077, 2081, 2145, 2146,' +
        ' 3387,3388, 3389, 3390, 3391, 3392) or ((sct.id = 2080) and (mu.' +
        'id = 6)) or ((sct.id in (2083, 2088)) and (mu.id != 1)) then '#39'��' +
        '� �'#39
      '       else '#39'��� �'#39
      '  end as type_AB,'
      
        ' (select  max((nvl(ii.recalc_items, ii.fact_items) - ii.start_it' +
        'ems)) keep (dense_rank last order by aa.doc_date, aa.doc_id) '
      '  from rtl_inv_acts aa, rtl_inv_act_items ii, documents dd'
      
        '  where aa.doc_date BETWEEN ADD_MONTHS(sysdate,-7) AND aa.doc_da' +
        'te'
      '  and aa.doc_id = ii.doc_id'
      '  and ii.nmcl_id = i.nmcl_id'
      '  and ii.assortment_id = i.assortment_id'
      '  and aa.doc_date < ri.doc_date'
      '  and aa.doc_id != ri.doc_id'
      '  and aa.gr_dep_id = ri.gr_dep_id'
      '  and aa.doc_id = dd.id'
      '  AND dd.fldr_id IN (2465,2466,1607337)'
      '  AND dd.status <> '#39'D'#39') as items_pred'
      ' '
      'from '
      '  rtl_inv_act_items i,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a,'
      '  products_types pt,'
      '  rtl_inv_acts ri,'
      '  rtl_groups g,'
      '  rtl_categories ct,'
      '  rtl_subcategories sct,'
      '  rtl_subsubcategories ssct,'
      '  rtl_classifier_rs rcr'
      'where '
      '  i.doc_id = :ID'
      '  and ni.id = i.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and a.id = i.assortment_id'
      '  and pt.id = ni.prod_type_id'
      '  and ri.doc_id = i.doc_id'
      
        '  and nvl(nullif(:SHOW_NULLS,0), i.fact_items + i.out_price + i.' +
        'start_items) is not null'
      '  AND ct.group_id = g.id (+)'
      '  AND sct.cat_id = ct.id (+)'
      '  AND ssct.subcat_id = sct.id (+)'
      '  AND rcr.subsubcat_id = ssct.id (+)'
      '  AND rcr.nmcl_id (+) = i.nmcl_id'
      '  AND rcr.assort_id (+) = i.assortment_id'
      '%WHERE_NON_ZERO'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_NON_ZERO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SHOW_NULLS'
        ParamType = ptInput
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'ID'
    end
    object m_Q_linesSTART_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'START_DATE'
      Origin = 'START_DATE'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      DisplayWidth = 20
      FieldName = 'NMCL_NAME'
      Origin = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_linesASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 20
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_linesMU_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MU_NAME'
      Origin = 'MU_NAME'
      Size = 10
    end
    object m_Q_linesIN_PRICE: TFloatField
      DisplayLabel = '����*'
      FieldName = 'IN_PRICE'
      Origin = 'IN_PRICE'
      currency = True
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'OUT_PRICE'
      currency = True
    end
    object m_Q_linesSTART_ITEMS: TFloatField
      DisplayLabel = '������� �� �����'
      FieldName = 'START_ITEMS'
      Origin = 'START_ITEMS'
    end
    object m_Q_linesFACT_ITEMS: TFloatField
      DisplayLabel = '����'
      FieldName = 'FACT_ITEMS'
      Origin = 'FACT_ITEMS'
    end
    object m_Q_linesDIFF_ITEMS: TFloatField
      DisplayLabel = '�������|� ���. ��.'
      FieldName = 'DIFF_ITEMS'
      Origin = 'DIFF_ITEMS'
    end
    object m_Q_linesDIFF_AMT_IN: TFloatField
      DisplayLabel = '�������|�����*'
      FieldName = 'DIFF_AMT_IN'
      Origin = 'DIFF_AMT_IN'
      currency = True
    end
    object m_Q_linesDIFF_AMT_OUT: TFloatField
      DisplayLabel = '�������|�����'
      FieldName = 'DIFF_AMT_OUT'
      Origin = 'DIFF_AMT_OUT'
      currency = True
    end
    object m_Q_linesCARD_ID: TFloatField
      DisplayLabel = 'ID ��������'
      FieldName = 'CARD_ID'
      Origin = 'CARD_ID'
    end
    object m_Q_linesRECALC_ITEMS: TFloatField
      DisplayLabel = '���� �� ���������'
      FieldName = 'RECALC_ITEMS'
      Origin = 'RECALC_ITEMS'
    end
    object m_Q_linesNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 30
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 250
    end
    object m_Q_linesIS_RECALC: TFloatField
      DisplayLabel = '������� ���������'
      FieldName = 'IS_RECALC'
      Origin = 'IS_RECALC'
      OnChange = m_Q_linesIS_RECALCChange
    end
    object m_Q_linesNON_PREPARE: TStringField
      DisplayLabel = '������������'
      FieldName = 'NON_PREPARE'
      Origin = 'NON_PREPARE'
      FixedChar = True
      Size = 1
    end
    object m_Q_linesFACT_FROM: TFloatField
      DisplayLabel = '���� ��� �����.'
      FieldName = 'FACT_FROM'
      Origin = 'FACT_FROM'
    end
    object m_Q_linesFACT_TO: TFloatField
      DisplayLabel = '���� � �����.'
      FieldName = 'FACT_TO'
      Origin = 'FACT_TO'
    end
    object m_Q_linesTRANS_RKU_COEF: TFloatField
      DisplayLabel = '����������� ��������'
      FieldName = 'TRANS_RKU_COEF'
      Origin = 'TRANS_RKU_COEF'
    end
    object m_Q_linesPRODPP: TFloatField
      FieldName = 'PRODPP'
      Origin = 'PRODPP'
    end
    object m_Q_linesGROUP_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'GROUP_ID'
      Origin = 'GROUP_ID'
    end
    object m_Q_linesGNAME: TStringField
      DisplayLabel = '������'
      FieldName = 'GNAME'
      Origin = 'GNAME'
      Size = 100
    end
    object m_Q_linesCAT_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'CAT_ID'
      Origin = 'CAT_ID'
    end
    object m_Q_linesCTNAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'CTNAME'
      Origin = 'CTNAME'
      Size = 100
    end
    object m_Q_linesSCTNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'SCTNAME'
      Origin = 'SCTNAME'
      Size = 100
    end
    object m_Q_linesSUBCAT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'SUBCAT_ID'
      Origin = 'SUBCAT_ID'
    end
    object m_Q_linesTYPE_AB: TStringField
      DisplayLabel = '��� ������'
      FieldName = 'TYPE_AB'
      Origin = 'TYPE_AB'
      FixedChar = True
      Size = 5
    end
    object m_Q_linesITEMS_PRED: TFloatField
      DisplayLabel = '�������|����. � ���. ��.'
      FieldName = 'ITEMS_PRED'
      Origin = 'ITEMS_PRED'
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'select '
      '  i.doc_id id,'
      '  i.line_id line_id,'
      '  i.start_date,'
      '  i.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.short_name mu_name,'
      '  i.assortment_id,'
      '  a.name assortment_name,'
      '  i.in_price,'
      '  i.out_price,'
      '  i.start_items,'
      '  i.fact_items,'
      '  i.note note,'
      '  i.card_id,'
      '  i.non_prepare,'
      '  i.recalc_items,'
      '  nvl(i.is_recalc,0) is_recalc,'
      '  i.fact_from,'
      '  i.fact_to,'
      '('
      '    select MAX(r.coef)'
      '    from rtl_prod_translate_rku r'
      '    WHERE r.to_nmcl_id = i.nmcl_id'
      '    AND r.to_assort_id = i.assortment_id'
      '    AND r.to_gr_dep_id = ri.gr_dep_id'
      '   ) as trans_rku_coef'
      'from '
      '  rtl_inv_act_items i,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a,'
      '  documents d,'
      '  rtl_inv_acts ri'
      'where '
      '  i.doc_id = :ID'
      '  and i.line_id = :LINE_ID'
      '  and ni.id = i.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and a.id = i.assortment_id'
      '  and ri.doc_id = i.doc_id')
    object m_Q_lineSTART_DATE: TDateTimeField
      FieldName = 'START_DATE'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.START_DATE'
    end
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_lineMU_NAME: TStringField
      FieldName = 'MU_NAME'
      Origin = 'TSDBMAIN.MEASURE_UNITS.SHORT_NAME'
      Size = 10
    end
    object m_Q_lineASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
    end
    object m_Q_lineASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
    object m_Q_lineIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.IN_PRICE'
      currency = True
    end
    object m_Q_lineOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.OUT_PRICE'
      currency = True
    end
    object m_Q_lineSTART_ITEMS: TFloatField
      FieldName = 'START_ITEMS'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.START_ITEMS'
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.NOTE'
      Size = 250
    end
    object m_Q_lineCARD_ID: TFloatField
      FieldName = 'CARD_ID'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.CARD_ID'
    end
    object m_Q_lineRECALC_ITEMS: TFloatField
      FieldName = 'RECALC_ITEMS'
    end
    object m_Q_lineIS_RECALC: TFloatField
      FieldName = 'IS_RECALC'
    end
    object m_Q_lineNON_PREPARE: TStringField
      FieldName = 'NON_PREPARE'
      FixedChar = True
      Size = 1
    end
    object m_Q_lineFACT_ITEMS: TFloatField
      FieldName = 'FACT_ITEMS'
      Origin = 'TSDBMAIN.RTL_INV_ACT_ITEMS.FACT_ITEMS'
    end
    object m_Q_lineFACT_FROM: TFloatField
      FieldName = 'FACT_FROM'
    end
    object m_Q_lineFACT_TO: TFloatField
      FieldName = 'FACT_TO'
    end
    object m_Q_lineTRANS_RKU_COEF: TFloatField
      FieldName = 'TRANS_RKU_COEF'
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      'begin'
      '  update rtl_inv_act_items'
      
        '  set start_date = :START_DATE, fact_items = :FACT_ITEMS, note =' +
        ' :NOTE, recalc_items = :RECALC_ITEMS,'
      '  fact_from = :FACT_FROM, fact_to = :FACT_TO, '
      '  is_recalc = nvl(:IS_RECALC,0),'
      '  was_recalc = nvl(:IS_RECALC,0),'
      '  non_prepare = :NON_PREPARE'
      '  where doc_id = :OLD_ID and line_id = :OLD_LINE_ID;'
      '  pkg_rtl_inv.prc_fill_line(:OLD_ID,:OLD_LINE_ID);'
      'end;')
    DeleteSQL.Strings = (
      'begin'
      
        'delete from rtl_inv_act_subitems where doc_id = :OLD_ID and line' +
        '_id = :OLD_LINE_ID;'
      
        'delete from rtl_inv_act_items where doc_id = :OLD_ID and line_id' +
        ' = :OLD_LINE_ID;'
      'end;')
    IgnoreRowsAffected = True
  end
  object m_Q_sheet_lines: TMLQuery
    BeforeOpen = m_Q_sheet_linesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select si.doc_id, si.line_id, s.doc_number, s.doc_date, si.start' +
        '_date, si.items_qnt, si.note'
      'from rtl_inv_sheet_items si, rtl_inv_sheets s'
      
        'where si.act_doc_id = :ID and si.act_line_id = :LINE_ID and s.do' +
        'c_id = si.doc_id '
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 32
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_sheet_linesDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Origin = 'TSDBMAIN.RTL_INV_SHEET_ITEMS.DOC_ID'
    end
    object m_Q_sheet_linesLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'TSDBMAIN.RTL_INV_SHEET_ITEMS.LINE_ID'
    end
    object m_Q_sheet_linesDOC_NUMBER: TStringField
      DisplayLabel = '�'
      DisplayWidth = 10
      FieldName = 'DOC_NUMBER'
      Origin = 's.doc_number'
      Size = 100
    end
    object m_Q_sheet_linesDOC_DATE: TDateTimeField
      DisplayLabel = '����'
      DisplayWidth = 10
      FieldName = 'DOC_DATE'
      Origin = 's.doc_date'
    end
    object m_Q_sheet_linesSTART_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'START_DATE'
      Origin = 'si.start_date'
    end
    object m_Q_sheet_linesITEMS_QNT: TFloatField
      DisplayLabel = '����'
      FieldName = 'ITEMS_QNT'
      Origin = 'si.items_qnt'
    end
    object m_Q_sheet_linesNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 40
      FieldName = 'NOTE'
      Origin = 'si.note'
      Size = 250
    end
  end
  object m_Q_unbind_lines: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'update rtl_inv_sheet_items'
      'set act_doc_id = null, act_line_id = null'
      'where act_doc_id = :ID and act_line_id = :LINE_ID')
    Left = 136
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_freeze: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin pkg_rtl_inv.prc_freeze(:DOC_ID); end;')
    Left = 216
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_class_groups: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select 1 as class_id, '#39'1 ��������� (1 �����)'#39' as name from dual'
      'union'
      
        'select 2 as class_id, '#39'2 ��������� (6 �������)'#39' as name from dua' +
        'l')
    Left = 568
    Top = 304
    object m_Q_class_groupsCLASS_ID: TFloatField
      FieldName = 'CLASS_ID'
      Origin = 'TSDBMAIN.NMCL_CLASSIFICATIONS.CLASS_ID'
    end
    object m_Q_class_groupsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.NMCL_CLASSIFICATIONS.NAME'
      Size = 100
    end
  end
  object m_SP_create_act_for_group: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'RETAIL.PRC_CREATE_ACT_FOR_GROUP'
    Left = 648
    Top = 304
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P_CLASS_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_GR_DEP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'P_START_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_NONZERO'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_DOC_ID'
        ParamType = ptInputOutput
      end>
  end
  object m_Q_gr_deps: TMLQuery
    BeforeOpen = m_Q_gr_depsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name'
      'from groups_of_depositories g'
      'where g.org_id = :HOME_ORG_ID'
      'and exists (select 1 from org_params'
      
        '                where org_par_id = 24 and org_id = :HOME_ORG_ID ' +
        'and end_date is null'
      '                and to_number(param_value) = g.id)'
      '%WHERE_CLAUSE'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 700
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_gr_depsID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_gr_depsNAME: TStringField
      DisplayLabel = '��������'
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 100
    end
  end
  object m_Q_load_nmcls: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin pkg_rtl_inv.prc_load_nmcls(:DOC_ID,:SQ_ID); end;')
    Left = 32
    Top = 368
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_nmcls: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_nmclsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct'
      '  ni.id nmcl_id,'
      '  ni.name nmcl_name,'
      '  a.id assortment_id,'
      '  a.name assortment_name,'
      '  mu.short_name meas_name,'
      '  trim(to_char(ni.id) || '#39','#39' || to_char(a.id)) save_str,'
      '  :YES_NO AS ok,'
      '  ct.group_id,'
      '  g.name as gname,'
      '  sct.cat_id,'
      '  ct.name as ctname,'
      '  ssct.subcat_id,'
      '  sct.name subcat_name'
      'from '
      '  goods_record_cards c,'
      '  retail_goods_cards rc,'
      '  nomenclature_items ni,'
      '  assortment a,'
      '  measure_units mu,'
      '  products_types pt,'
      '  rtl_groups g,'
      '  rtl_categories ct,'
      '  rtl_subcategories sct,'
      '  rtl_subsubcategories ssct,'
      '  rtl_classifier_rs rcr'
      'where '
      '  c.gr_dep_id = :GR_DEP_ID'
      '  and rc.card_id = c.id'
      '  and decode(:NONZERO,1,rc.store_qnt,1) != 0'
      '  and ni.id = c.nmcl_id'
      '  and a.id = c.assortment_id'
      '  and mu.id = ni.meas_unit_id'
      '  and pt.id = ni.prod_type_id'
      '  AND c.enabled = 1'
      '  AND ct.group_id = g.id (+)'
      '  AND sct.cat_id = ct.id (+)'
      '  AND ssct.subcat_id = sct.id (+)'
      '  AND rcr.subsubcat_id = ssct.id (+)'
      '  AND rcr.nmcl_id (+) = c.nmcl_id'
      '  AND rcr.assort_id (+) = c.assortment_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_nmcls
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 128
    Top = 368
    ParamData = <
      item
        DataType = ftString
        Name = 'YES_NO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'GR_DEP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NONZERO'
        ParamType = ptInput
      end>
    object m_Q_nmclsGROUP_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'GROUP_ID'
      Origin = 'ct.group_id'
    end
    object m_Q_nmclsGNAME: TStringField
      DisplayLabel = '������'
      FieldName = 'GNAME'
      Origin = 'g.name'
      Size = 100
    end
    object m_Q_nmclsCAT_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'CAT_ID'
      Origin = 'sct.cat_id'
    end
    object m_Q_nmclsCTNAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'CTNAME'
      Origin = 'ct.name'
      Size = 100
    end
    object m_Q_nmclsNMCL_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'NMCL_ID'
      Origin = 'ni.id'
    end
    object m_Q_nmclsNMCL_NAME: TStringField
      DisplayLabel = '������������'
      DisplayWidth = 30
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_nmclsASSORTMENT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'a.id'
    end
    object m_Q_nmclsASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 30
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_nmclsMEAS_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MEAS_NAME'
      Origin = 'mu.short_name'
      Size = 30
    end
    object m_Q_nmclsSAVE_STR: TStringField
      FieldName = 'SAVE_STR'
      Size = 33
    end
    object m_Q_nmclsOK: TStringField
      DisplayLabel = '�������'
      FieldName = 'OK'
      FixedChar = True
      Size = 1
    end
    object m_Q_nmclsSUBCAT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'SUBCAT_ID'
      Origin = 'ssct.subcat_id'
    end
    object m_Q_nmclsSUBCAT_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'SUBCAT_NAME'
      Origin = 'sct.name'
      Size = 100
    end
  end
  object m_U_nmcls: TUpdateSQL
    Left = 200
    Top = 368
  end
  object m_Q_load_sheets: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '  pkg_rtl_inv.prc_load_sheets(:DOC_ID);'
      'end;')
    Left = 288
    Top = 368
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_zero_fact: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  PKG_RTL_INV.prc_set_act_zero_fact(:DOC_ID);'
      'END;')
    Left = 376
    Top = 368
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_nmcls_i: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_nmcls_iBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct'
      '  nc.class_id AS pt_id,'
      '  nc.name AS pt_name,'
      '  ni.id nmcl_id,'
      '  ni.name nmcl_name,'
      '  a.id assortment_id,'
      '  a.name assortment_name,'
      '  mu.short_name meas_name,'
      '  trim(to_char(ni.id) || '#39','#39' || to_char(a.id)) save_str,'
      '  :YES_NO as ok'
      'from '
      '  goods_record_cards c,'
      '  retail_goods_cards rc,'
      '  nomenclature_items ni,'
      '  assortment a,'
      '  measure_units mu,'
      '  nmcl_classifications nc,'
      '  nmcl_class_relations ncr'
      'where '
      '  c.gr_dep_id = :GR_DEP_ID'
      '  and rc.card_id = c.id'
      '  and decode(:NONZERO,1,rc.store_qnt,1) != 0'
      '  and ni.id = c.nmcl_id'
      '  and a.id = c.assortment_id'
      '  and mu.id = ni.meas_unit_id'
      '  AND ni.id = ncr.nmcl_id'
      '  AND ncr.class_type_id = 3'
      '  AND ncr.class_id = nc.class_id'
      '  AND nc.class_type_id = 3'
      '  AND c.enabled = 1'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_nmcls_i
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 120
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'YES_NO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'GR_DEP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NONZERO'
        ParamType = ptInput
      end>
    object m_Q_nmcls_iPT_ID: TFloatField
      DisplayLabel = '���'
      FieldName = 'PT_ID'
      Origin = 'nc.class_id'
    end
    object m_Q_nmcls_iPT_NAME: TStringField
      DisplayLabel = '������'
      DisplayWidth = 30
      FieldName = 'PT_NAME'
      Origin = 'nc.name'
      Size = 100
    end
    object m_Q_nmcls_iNMCL_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'NMCL_ID'
      Origin = 'ni.id'
    end
    object m_Q_nmcls_iNMCL_NAME: TStringField
      DisplayLabel = '������������'
      DisplayWidth = 30
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_nmcls_iASSORTMENT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'a.id'
    end
    object m_Q_nmcls_iASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 30
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_nmcls_iMEAS_NAME: TStringField
      DisplayLabel = '��. ���.'
      FieldName = 'MEAS_NAME'
      Origin = 'mu.short_name'
      Size = 30
    end
    object m_Q_nmcls_iSAVE_STR: TStringField
      FieldName = 'SAVE_STR'
      Size = 33
    end
    object m_Q_nmcls_iOK: TStringField
      DisplayLabel = '�������'
      FieldName = 'OK'
      FixedChar = True
      Size = 1
    end
  end
  object m_U_nmcls_i: TUpdateSQL
    Left = 200
    Top = 416
  end
  object m_Q_mark_recalc: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '  pkg_rtl_inv.Prc_mark_lines_to_recalc(:SQ_ID, :DOC_ID, :IS_PR);'
      'end;')
    Left = 288
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IS_PR'
        ParamType = ptInput
      end>
  end
  object m_U_fict_lines: TMLUpdateSQL
    Left = 464
    Top = 424
  end
  object m_Q_folders_next: TQuery
    BeforeOpen = m_Q_folders_nextBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select'
      '    v.src_fldr_id,'
      '    v.id,'
      '    v.dest_fldr_id,'
      '    v.name,'
      '    v.route_next_right,'
      '    :ROUTE_ACCESS_MASK as route_access_mask,'
      '    count(*) over() AS fldr_count'
      'from'
      '    ('
      '        select'
      '            fr.src_fldr_id,'
      '            fr.id,'
      '            fr.dest_fldr_id,'
      '            fr.name,'
      '            fr.sort_id,'
      
        '            rpad(fnc_mask_route_right(fr.id,:ROUTE_ACCESS_MASK, ' +
        ':ID),250) AS route_next_right'
      '        from'
      '            fldr_routes fr,'
      '            folders f_s,'
      '            folders f_e'
      '        where'
      '            fr.src_fldr_id = ('
      '                select'
      '                    fldr_id'
      '                from'
      '                    documents'
      '                where'
      '                    id = :ID'
      '            )'
      '            and f_s.id = fr.src_fldr_id'
      '            and f_e.id = fr.dest_fldr_id'
      '            and f_e.sort_id > f_s.sort_id'
      '    ) v'
      'where'
      '    v.route_next_right = 1'
      'order by'
      '    v.sort_id'
      ' ')
    Left = 40
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'ROUTE_ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ROUTE_ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_folders_nextSRC_FLDR_ID: TFloatField
      FieldName = 'SRC_FLDR_ID'
    end
    object m_Q_folders_nextID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_folders_nextDEST_FLDR_ID: TFloatField
      FieldName = 'DEST_FLDR_ID'
    end
    object m_Q_folders_nextNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object m_Q_folders_nextROUTE_NEXT_RIGHT: TStringField
      FieldName = 'ROUTE_NEXT_RIGHT'
      Size = 250
    end
    object m_Q_folders_nextROUTE_ACCESS_MASK: TStringField
      FieldName = 'ROUTE_ACCESS_MASK'
      Size = 32
    end
    object m_Q_folders_nextFLDR_COUNT: TFloatField
      FieldName = 'FLDR_COUNT'
    end
  end
  object m_Q_cnt_list: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT cnt_id,'
      '               cnt_name,'
      '               position'
      'FROM ('
      'SELECT pc.cnt_id,'
      
        '       TRIM(RPAD(pc.first_name||'#39' '#39'||pc.last_name||'#39' '#39'||pc.middl' +
        'e_name,250))  AS cnt_name,'
      '       st.position '
      'FROM persons_contractors pc,'
      '     staffmen s,'
      '     staff_table st'
      'WHERE pc.cnt_id = s.cnt_id'
      '  AND s.fire_date IS NULL     '
      '  AND s.staff_table_id = st.id'
      '   AND NOT EXISTS (SELECT 1 FROM rtl_inv_act_cnt r'
      '                  WHERE r.doc_id = :DOC_ID'
      '                    AND r.cnt_id = pc.cnt_id) '
      ')'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 568
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_cnt_listCNT_ID: TFloatField
      DisplayLabel = 'ID ����.'
      FieldName = 'CNT_ID'
      Origin = 'cnt_id'
    end
    object m_Q_cnt_listCNT_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'CNT_NAME'
      Origin = 'cnt_name'
      Size = 250
    end
    object m_Q_cnt_listPOSITION: TStringField
      DisplayLabel = '���������'
      FieldName = 'POSITION'
      Size = 100
    end
  end
  object m_Q_recalc: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin pkg_rtl_inv.Prc_recalc_act(:DOC_ID); end;')
    Left = 32
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_check_order: TQuery
    BeforeOpen = m_Q_check_orderBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      'i.doc_id, si.inv_date as next_date'
      
        'from rtl_order_inv_items i, rtl_order_inv_subitems si, documents' +
        ' d, rtl_order_inv r'
      'WHERE i.act_id = :DOC_ID'
      'AND i.doc_id = si.doc_id AND i.line_id = si.line_id'
      '--AND si.end_date IS NULL'
      'AND si.inv_date = TRUNC(:INV_DATE)'
      'AND d.id = i.doc_id AND d.status <> '#39'D'#39
      'AND r.doc_id = i.doc_id')
    Left = 112
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'INV_DATE'
        ParamType = ptInput
      end>
    object m_Q_check_orderDOC_ID: TFloatField
      FieldName = 'DOC_ID'
    end
    object m_Q_check_orderNEXT_DATE: TDateTimeField
      FieldName = 'NEXT_DATE'
    end
  end
  object m_Q_edit_inv_item: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '   pkg_rtl_inv.prc_edit_inv_item(:ID, :NEW_DATE, :NOTE);'
      'END;')
    Macros = <>
    Left = 224
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'NEW_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NOTE'
        ParamType = ptInput
      end>
  end
  object m_Q_edit_inv_prepare: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      'update rtl_inv_act_items set'
      '       non_prepare = :NEW_PR'
      'where doc_id = :ID and line_id = :LINE_ID;'
      'END;')
    Macros = <>
    Left = 344
    Top = 496
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_PR'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_upd_prepare: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'update rtl_inv_act_items'
      'set non_prepare = decode(non_prepare, '#39'N'#39', '#39'Y'#39', '#39'N'#39')'
      'where doc_id = :doc_id'
      
        'and line_id in (select to_number(value) FROM tmp_report_param_va' +
        'lues where id = :sq_id)')
    Macros = <>
    Left = 464
    Top = 491
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'doc_id'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'sq_id'
        ParamType = ptUnknown
      end>
  end
  object m_Q_fact_hst: TMLQuery
    BeforeOpen = m_Q_fact_hstBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '
      'pt.id as pt_id,'
      'pt.name as pt_name,'
      'nmcl_id, '
      'ni.name as nmcl_name,'
      'modify_date, '
      'modify_user,'
      
        'trim(rpad(PKG_PERSONS.fnc_user_full_name(modify_user, sysdate),2' +
        '50)) as modify_fio,'
      'fact_items_old,'
      'fact_items_new'
      'from rtl_inv_act_hst r, nomenclature_items ni, products_types pt'
      'where doc_id = :doc_id'
      '  and line_id = :line_id'
      'and r.nmcl_id = ni.id'
      'and ni.prod_type_id = pt.id'
      '%WHERE_CLAUSE'
      'order by modify_date desc')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 568
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'line_id'
        ParamType = ptInput
      end>
    object m_Q_fact_hstPT_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'PT_ID'
      Origin = 'pt.id'
    end
    object m_Q_fact_hstPT_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'PT_NAME'
      Origin = 'pt.NAME'
      Size = 100
    end
    object m_Q_fact_hstNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_fact_hstNMCL_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.NAME'
      Size = 100
    end
    object m_Q_fact_hstMODIFY_DATE: TDateTimeField
      DisplayLabel = '���� �����������'
      FieldName = 'MODIFY_DATE'
      Origin = 'MODIFY_DATE'
    end
    object m_Q_fact_hstMODIFY_USER: TStringField
      DisplayLabel = '���������������� ������������'
      FieldName = 'MODIFY_USER'
      Origin = 'MODIFY_USER'
      Size = 50
    end
    object m_Q_fact_hstMODIFY_FIO: TStringField
      DisplayLabel = '���'
      FieldName = 'MODIFY_FIO'
      Origin = 
        'trim(rpad(PKG_PERSONS.fnc_user_full_name(modify_user, sysdate),2' +
        '50))'
      Size = 250
    end
    object m_Q_fact_hstFACT_ITEMS_OLD: TFloatField
      DisplayLabel = '���� - ������ ��������'
      FieldName = 'FACT_ITEMS_OLD'
      Origin = 'FACT_ITEMS_OLD'
    end
    object m_Q_fact_hstFACT_ITEMS_NEW: TFloatField
      DisplayLabel = '���� - ����� ��������'
      FieldName = 'FACT_ITEMS_NEW'
      Origin = 'FACT_ITEMS_NEW'
    end
  end
  object m_Q_ch_cur_rest: TMLQuery
    BeforeOpen = m_Q_ch_cur_restBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select max(i.nmcl_id) as nmcl_id from rtl_inv_act_items i'
      'where i.doc_id = :doc_id'
      
        'and i.start_items != pkg_goods_cards.FNC_GET_CARD_REST_CUR(i.car' +
        'd_id)'
      
        'and nvl(i.start_items,0) != nvl(i.recalc_items,nvl(i.fact_items,' +
        '0))')
    Macros = <>
    Left = 764
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'doc_id'
        ParamType = ptInput
      end>
    object m_Q_ch_cur_restNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 560
    Top = 240
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_Q_alco: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcoBeforeOpen
    AfterInsert = m_Q_alcoAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select s.doc_id id,'
      '       s.line_id,'
      '       s.sub_line_id,'
      '       s.pdf_bar_code,'
      
        '       DECODE(s.alccode,'#39'XXXXXXXXXXXXXXXXXXX'#39','#39'-1'#39',s.alccode) as' +
        ' alccode,'
      '       s.start_items,'
      '       s.fact_items,'
      '       s.recalc_items,'
      '       ep.fullname'
      'from rtl_inv_act_subitems s,'
      '        egais_pref ep'
      'where doc_id = :DOC_ID'
      'and line_id = :LINE_ID'
      'and sub_line_id = :SUB_LINE_ID'
      'and ep.alccode(+) = s.alccode')
    UpdateObject = m_U_alco
    Macros = <>
    Left = 576
    Top = 376
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SUB_LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_alcoID: TFloatField
      DisplayLabel = 'DOC_ID'
      FieldName = 'ID'
      Origin = 'S.DOC_ID'
    end
    object m_Q_alcoLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'S.LINE_ID'
    end
    object m_Q_alcoSUB_LINE_ID: TFloatField
      FieldName = 'SUB_LINE_ID'
      Origin = 'S.SUB_LINE_ID'
    end
    object m_Q_alcoPDF_BAR_CODE: TStringField
      DisplayLabel = '��� �������� �����'
      FieldName = 'PDF_BAR_CODE'
      Origin = 'S.PDF_BAR_CODE'
      Size = 100
    end
    object m_Q_alcoALCCODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'ALCCODE'
      Origin = 'DECODE(s.alccode,'#39'XXXXXXXXXXXXXXXXXXX'#39','#39'-1'#39',s.alccode)'
      Size = 50
    end
    object m_Q_alcoFULLNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'FULLNAME'
      Origin = 'EP.FULLNAME'
      Size = 255
    end
    object m_Q_alcoSTART_ITEMS: TFloatField
      DisplayLabel = '�� �����'
      FieldName = 'START_ITEMS'
      Origin = 's.START_ITEMS'
    end
    object m_Q_alcoFACT_ITEMS: TFloatField
      DisplayLabel = '����'
      FieldName = 'FACT_ITEMS'
      Origin = 's.START_ITEMS'
    end
    object m_Q_alcoRECALC_ITEMS: TFloatField
      DisplayLabel = '���� �� ���������'
      FieldName = 'RECALC_ITEMS'
      Origin = 's.START_ITEMS'
    end
  end
  object m_Q_alccode: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alccodeBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '
      '  t.pref_alccode,'
      '  t.name'
      'FROM '
      '('
      'SELECT '
      
        '      DECODE(ep.alccode,'#39'XXXXXXXXXXXXXXXXXXX'#39','#39'-1'#39',ep.alccode) a' +
        's pref_alccode,'
      '      ep.fullname as name'
      '    FROM egais_pref ep'
      '    WHERE exists('
      
        '      SELECT null FROM egais_nmcl ne WHERE ne.nmcl_id = :NMCL_ID' +
        ' AND ne.assortment_id = :ASSORTMENT_ID AND ne.pref_alccode = ep.' +
        'alccode'
      '    ) '
      '    OR ep.alccode = '#39'XXXXXXXXXXXXXXXXXXX'#39' '
      ') t'
      '%WHERE_EXPRESSION'
      'ORDER BY name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 456
    Top = 360
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end>
    object m_Q_alccodePREF_ALCCODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'PREF_ALCCODE'
      Origin = 'PREF_ALCCODE'
      Size = 100
    end
    object m_Q_alccodeNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 255
    end
  end
  object m_U_alco: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_inv_act_subitems'
      
        'set alccode =  DECODE( :ALCCODE, '#39'-1'#39','#39'XXXXXXXXXXXXXXXXXXX'#39', :AL' +
        'CCODE ), '
      '      start_items = :START_ITEMS,'
      '      fact_items = :FACT_ITEMS,'
      '      recalc_items = :RECALC_ITEMS'
      'where doc_id = :OLD_ID'
      'and line_id = :OLD_LINE_ID'
      'and sub_line_id = :OLD_SUB_LINE_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_inv_act_subitems (doc_id, line_id, sub_line_id, ' +
        'pdf_bar_code, alccode, '
      's.start_items,  s.fact_items,  s.recalc_items)'
      
        'values (:ID, :LINE_ID, sq_rtl_inv_act_subitems.nextval, :PDF_BAR' +
        '_CODE,  DECODE( '
      ':ALCCODE, '#39'-1'#39','#39'XXXXXXXXXXXXXXXXXXX'#39', :ALCCODE ), :START_ITEMS, '
      ':FACT_ITEMS, :RECALC_ITEMS)')
    DeleteSQL.Strings = (
      
        'delete from rtl_inv_act_subitems where doc_id = :OLD_ID and line' +
        '_id = :OLD_LINE_ID and sub_line_id = :OLD_SUB_LINE_ID')
    IgnoreRowsAffected = True
    Left = 648
    Top = 376
  end
  object m_Q_alc: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcBeforeOpen
    AfterInsert = m_Q_alcAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select s.doc_id id,'
      '       s.line_id,'
      '       s.sub_line_id,'
      '       s.pdf_bar_code,'
      '       s.alccode,'
      '       s.start_items,'
      '       s.fact_items,'
      '       s.recalc_items,'
      '       ep.fullname'
      'from rtl_inv_act_subitems s,'
      '        egais_pref ep'
      'where doc_id = :DOC_ID'
      'and line_id = :LINE_ID'
      'and ep.alccode(+) = s.alccode')
    Macros = <>
    Left = 720
    Top = 376
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_alcID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_alcLINE_ID: TFloatField
      FieldName = 'LINE_ID'
    end
    object m_Q_alcSUB_LINE_ID: TFloatField
      FieldName = 'SUB_LINE_ID'
    end
    object m_Q_alcPDF_BAR_CODE: TStringField
      DisplayLabel = '��� �������� �����'
      FieldName = 'PDF_BAR_CODE'
      Origin = 's.pdf_bar_code'
      Size = 100
    end
    object m_Q_alcALCCODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'ALCCODE'
      Origin = 's.alccode'
      Size = 50
    end
    object m_Q_alcFULLNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'FULLNAME'
      Origin = 'ep.fullname'
      Size = 255
    end
    object m_Q_alcSTART_ITEMS: TFloatField
      DisplayLabel = '�� �����'
      FieldName = 'START_ITEMS'
      Origin = 's.START_ITEMS'
    end
    object m_Q_alcFACT_ITEMS: TFloatField
      DisplayLabel = '����'
      FieldName = 'FACT_ITEMS'
      Origin = 's.FACT_ITEMS'
    end
    object m_Q_alcRECALC_ITEMS: TFloatField
      DisplayLabel = '���� �� ���������'
      FieldName = 'RECALC_ITEMS'
      Origin = 's.RECALC_ITEMS'
    end
  end
  object m_Q_pdf_bar_code: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcBeforeOpen
    AfterInsert = m_Q_alcAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '  update rtl_inv_act_subitems'
      
        '  set fact_items = decode(:FLDR,2458,nvl(fact_items,0) + 1,fact_' +
        'items),'
      
        '      recalc_items = decode(:FLDR,2458,recalc_items,nvl(recalc_i' +
        'tems,0) + 1)'
      
        '  where doc_id = :DOC_ID and line_id = :LINE_ID and alccode = pk' +
        'g_bar_code.get_egais_alccode(:PDF_BAR_CODE);'
      ''
      '  if (sql%rowcount = 0) then'
      
        '  insert into rtl_inv_act_subitems(doc_id, line_id, sub_line_id,' +
        ' pdf_bar_code, alccode,start_items, fact_items, recalc_items)'
      
        '  values (:DOC_ID, :LINE_ID, sq_rtl_inv_act_subitems.nextval, :P' +
        'DF_BAR_CODE, pkg_bar_code.get_egais_alccode(:PDF_BAR_CODE),0, de' +
        'code(:FLDR,2458, 1,0),decode(:FLDR,2458,0, 1));'
      '  end if;'
      'end;')
    MacroChar = '&'
    Macros = <>
    Left = 656
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'FLDR'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR'
        ParamType = ptInput
      end>
  end
  object m_Q_del_items: TMLQuery
    BeforeOpen = m_Q_linesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '  delete from rtl_inv_sheet_items'
      '      where act_doc_id = :DOC_ID'
      
        '      and act_line_id in (select to_number(value) FROM tmp_repor' +
        't_param_values where id = :sq_id);'
      ' delete from rtl_inv_act_items'
      ' where doc_id = :DOC_ID'
      
        ' and line_id in (select to_number(value) FROM tmp_report_param_v' +
        'alues where id = :sq_id);'
      'end;'
      '')
    Macros = <>
    Left = 704
    Top = 248
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'sq_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'sq_id'
        ParamType = ptInput
      end>
  end
  object m_Q_freeze_group: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin pkg_rtl_inv.prc_freeze_group(:DOC_ID, :SQ_ID); end;')
    Left = 416
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_freeze_with_sales: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin pkg_rtl_inv.prc_freeze_with_sales(:DOC_ID); end;')
    Left = 304
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_freeze_group_with_sales: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'begin pkg_rtl_inv.prc_freeze_group_with_sales(:DOC_ID, :SQ_ID); ' +
        'end;')
    Left = 496
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_reason: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select reason_id, name from rtl_inv_act_reason'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 748
    Top = 440
    object m_Q_reasonREASON_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'REASON_ID'
      Origin = 'reason_id'
    end
    object m_Q_reasonNAME: TStringField
      DisplayLabel = '�������'
      FieldName = 'NAME'
      Origin = 'name'
      Size = 50
    end
  end
  object m_Q_num_acts_list: TMLQuery
    BeforeOpen = m_Q_num_acts_listBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select doc_number from rtl_inv_acts a, documents d'
      'where a.doc_date BETWEEN sysdate-365 and sysdate'
      'and d.id = a.doc_id'
      'and d.status <> '#39'D'#39
      'and d.src_org_id = :ORG_ID'
      '%WHERE_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 748
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_num_acts_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���� ��������������'
      FieldName = 'DOC_NUMBER'
      Origin = 'doc_number'
      Size = 100
    end
  end
end
