inherited FOperRtlSalesItem: TFOperRtlSalesItem
  Left = 741
  Top = 252
  Caption = '��������� ������� (��)'
  ClientHeight = 582
  ClientWidth = 788
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 532
    Width = 788
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 594
      inherited m_SBTN_save: TBitBtn
        Visible = False
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          0000FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          00000000000000000000000000000084840000000000FF00FF00FF00FF000000
          0000008484000084840000848400008484000084840000848400008484000084
          84000084840000848400008484000084840000000000FF00FF00FF00FF000000
          0000008484000084840000000000000000000000000000000000000000000000
          00000000000000000000008484000084840000000000FF00FF00FF00FF000000
          00000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000084840000000000FF00FF00FF00FF000000
          00000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF000000
          00000084840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 497
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 563
    Width = 788
    Panels = <
      item
        Style = psOwnerDraw
        Width = 18
      end
      item
        Width = 80
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 788
    inherited m_P_tb_main_add: TPanel
      Left = 741
    end
    inherited m_P_tb_main: TPanel
      Width = 741
      inherited m_TB_main: TToolBar
        Width = 741
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 26
    Width = 788
    Height = 135
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object m_L_COMPLETE: TLabel
      Left = 8
      Top = 12
      Width = 118
      Height = 13
      Caption = '���� �������� �� ���'
      FocusControl = m_MLDBP_COMPLETE
    end
    object m_L_SALE_SUM: TLabel
      Left = 92
      Top = 44
      Width = 34
      Height = 13
      Caption = '�����'
      FocusControl = m_MLDBP_SALE_SUM
    end
    object m_L_BUYER_SUM: TLabel
      Left = 256
      Top = 44
      Width = 112
      Height = 13
      Caption = '������� �����������'
      FocusControl = m_MLDBP_BUYER_SUM
    end
    object m_L_COMPLETE_USER_ID: TLabel
      Left = 70
      Top = 76
      Width = 56
      Height = 13
      Caption = 'ID �������'
      FocusControl = m_MLDBP_COMPLETE_USER_ID
    end
    object m_L_COMPLETE_USER_NAME: TLabel
      Left = 280
      Top = 76
      Width = 37
      Height = 13
      Caption = '������'
      FocusControl = m_MLDBP_COMPLETE_USER_NAME
    end
    object m_L_HOST: TLabel
      Left = 68
      Top = 108
      Width = 58
      Height = 13
      Caption = '���������'
      FocusControl = m_MLDBP_HOST
    end
    object m_MLDBP_COMPLETE: TMLDBPanel
      Left = 136
      Top = 8
      Width = 185
      Height = 21
      DataField = 'COMPLETE'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 0
    end
    object m_MLDBP_SALE_SUM: TMLDBPanel
      Left = 136
      Top = 40
      Width = 100
      Height = 21
      DataField = 'SALE_SUM'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 1
    end
    object m_MLDBP_BUYER_SUM: TMLDBPanel
      Left = 376
      Top = 40
      Width = 100
      Height = 21
      DataField = 'BUYER_SUM'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 2
    end
    object m_MLDBP_COMPLETE_USER_ID: TMLDBPanel
      Left = 136
      Top = 72
      Width = 121
      Height = 21
      DataField = 'COMPLETE_USER_ID'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 3
    end
    object m_MLDBP_COMPLETE_USER_NAME: TMLDBPanel
      Left = 328
      Top = 72
      Width = 321
      Height = 21
      DataField = 'COMPLETE_USER_NAME'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 4
    end
    object m_MLDBP_HOST: TMLDBPanel
      Left = 136
      Top = 104
      Width = 513
      Height = 21
      DataField = 'HOST'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 5
    end
  end
  object MLDBGrid1: TMLDBGrid [4]
    Left = 0
    Top = 161
    Width = 788
    Height = 80
    Align = alTop
    DataSource = m_DS_checks
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    KeyColumnFind = -1
    ValueColumnFind = -1
    Columns = <
      item
        FieldName = 'CHECK_NUM'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'CHECK_DATE'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'CHECK_SUM'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'PRINT_DATE'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'PRINT_USER_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'PRINT_USER_NAME'
        Title.TitleButton = True
        Width = 200
        Footers = <>
      end
      item
        FieldName = 'HOST'
        Title.TitleButton = True
        Width = 200
        Footers = <>
      end>
  end
  object Panel2: TPanel [5]
    Left = 0
    Top = 241
    Width = 788
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object m_SBTN_print_check: TSpeedButton
      Left = 2
      Top = 2
      Width = 135
      Height = 25
      Action = m_ACT_print_check
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF0000000000FF00FF0000000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000FFFF0000FFFF0000FF
        FF00FF00FF00FF00FF00000000000000000000000000FF00FF0000000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084848400848484008484
        8400FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF000000000000000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000FF00FF0000000000FF00FF000000000000000000FF00FF00FF00
        FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000FF00FF0000000000FF00FF0000000000FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00000000000000000000000000000000000000
        0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF000000000000000000000000000000
        000000000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
  end
  object MLDBGrid2: TMLDBGrid [6]
    Left = 0
    Top = 273
    Width = 788
    Height = 259
    Align = alClient
    DataSource = m_DS_lines
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    UseMultiTitle = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    KeyColumnFind = -1
    ValueColumnFind = -1
    Columns = <
      item
        FieldName = 'LINE_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        Title.TitleButton = True
        Width = 200
        Footers = <>
      end
      item
        FieldName = 'ASSORT_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'ASSORT_NAME'
        Title.TitleButton = True
        Width = 200
        Footers = <>
      end
      item
        FieldName = 'WEIGHT_CODE'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'BAR_CODE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'OUT_PRICE'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'QUANTITY'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'DEP_NUMBER'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'NDS'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'CHECK_NUMBER'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'CARD_ID'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_apply_updates: TAction
      Hint = '��������� �������'
    end
    inherited m_ACT_insert: TAction
      Visible = False
    end
    object m_ACT_print_check: TAction
      Category = 'SubItem'
      Caption = '�������� ���...'
      Hint = '������ ��������� ����'
      OnExecute = m_ACT_print_checkExecute
      OnUpdate = m_ACT_print_checkUpdate
    end
  end
  object m_DS_item: TDataSource
    DataSet = DOperRtlSales.m_Q_item
    Top = 280
  end
  object m_DS_checks: TDataSource
    DataSet = DOperRtlSales.m_Q_item_checks
    Top = 312
  end
  object m_DS_lines: TDataSource
    DataSet = DOperRtlSales.m_Q_lines
    Top = 344
  end
end
