//---------------------------------------------------------------------------

#ifndef FmRtlReturnsListH
#define FmRtlReturnsListH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENTLIST.h"
#include "DmRtlReturns.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include <DBCtrls.hpp>
//---------------------------------------------------------------------------

class TFRtlReturnsList : public TTSFDocumentList
{
__published:
  TToolButton *m_TBTN_sep6;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TSpeedButton *m_SBTN_set_date;
        TDBLookupComboBox *m_DBLCB_organizations;
        TToolButton *ToolButton1;
        TDataSource *m_DS_orgs;
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_DBLCB_organizationsClick(TObject *Sender);
        void __fastcall m_ACT_deleteUpdate(TObject *Sender);
private:
  TDRtlReturns *m_dm;
public:
  __fastcall TFRtlReturnsList(TComponent* p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

