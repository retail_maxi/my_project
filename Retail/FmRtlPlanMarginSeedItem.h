//---------------------------------------------------------------------------

#ifndef FmRtlPlanMarginSeedItemH
#define FmRtlPlanMarginSeedItemH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFMREFBOOKITEM.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DmRtlPlanMarginSeed.h"
#include <Menus.hpp>
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include "MLLov.h"
#include "MLLovView.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFRtlPlanMarginSeedItem : public TTSFRefbookItem
{
__published:
        TLabel *m_L_nmcl;
        TLabel *lbl1;
        TDBEdit *m_DBE_purchase;
        TLabel *Label1;
        TLabel *Label2;
        TDBEdit *m_DBE_transfer;
        TDBEdit *m_DBE_price_gm;
        TDBEdit *m_DBE_price_sm;
protected:
  virtual AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
public:
  __fastcall TFRtlPlanMarginSeedItem(TComponent* p_owner,
                                TTSDRefbook *p_dm_refbook): TTSFRefbookItem(p_owner,
                                                                            p_dm_refbook) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
