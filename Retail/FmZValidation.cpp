//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmZValidation.h"
#include "FmSetDateTimePeriod.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmOperation"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFZValidation::FormShow(TObject *Sender)
{
    TTSFOperation::FormShow(Sender);
      SetPeriodCaption(m_dm->m_begin_date,m_dm->m_end_date);
}
//---------------------------------------------------------------------------

void __fastcall TFZValidation::m_ACT_fill_taxExecute(TObject *Sender)
{
      if( m_dm->SqTax == 0 ) m_dm->SqTax = m_dm->GetSqNextVal("SQ_PARAM_VALUES");
      m_dm->m_Q_ins_all_tax->ParamByName("SQ_ID")->AsInteger = m_dm->SqTax;
      m_dm->m_Q_ins_all_tax->ExecSQL();
      m_dm->m_Q_tax->Close();    m_dm->m_Q_tax->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFZValidation::m_ACT_clear_taxExecute(TObject *Sender)
{
      m_dm->ClearParamValues(m_dm->SqTax);
      m_dm->m_Q_tax->Close();    m_dm->m_Q_tax->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFZValidation::m_CB_act_taxClick(TObject *Sender)
{
   m_dm->m_act_tax =  (m_CB_act_tax->State == cbChecked);
   m_dm->m_Q_tax->Close();    m_dm->m_Q_tax->Open();
}


//---------------------------------------------------------------------------
void __fastcall TFZValidation::SetPeriodCaption(TDateTime p_begin_date, TDateTime p_end_date)
{
      if (p_begin_date.DateTimeString().Length()>11)
        m_SBTN_period->Caption = "� " + p_begin_date.DateTimeString().SubString(1, p_begin_date.DateTimeString().Length()-3);
      else
        m_SBTN_period->Caption = "� " + p_begin_date.DateTimeString();
      if (p_end_date.DateTimeString().Length()>11)
        m_SBTN_period->Caption =m_SBTN_period->Caption  + " �� " + p_end_date.DateTimeString().SubString(1, p_end_date.DateTimeString().Length()-3);
      else
        m_SBTN_period->Caption =m_SBTN_period->Caption + " �� " + p_end_date.DateTimeString();

}
//---------------------------------------------------------------------------

void __fastcall TFZValidation::m_ACT_set_periodExecute(TObject *Sender)
{
  TFSetDateTimePeriod *f = new TFSetDateTimePeriod(this,m_dm->m_begin_date, m_dm->m_end_date);
  try
  {
    if (f->ShowModal() == IDOK)
    {
      SetPeriodCaption(f->BeginDate, f->EndDate);
      m_dm->SetBeginEndDate( f->BeginDate,f->EndDate);

    }
  }
  __finally
  {
    delete f;
  }
}
//---------------------------------------------------------------------------



