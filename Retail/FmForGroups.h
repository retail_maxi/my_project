//---------------------------------------------------------------------------

#ifndef FmForGroupsH
#define FmForGroupsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmInvAct.h"
#include <Db.hpp>
#include <DBCtrls.hpp>
//---------------------------------------------------------------------------

class TFForGroups : public TTSFDialog
{
__published:
  TDataSource *m_DS_class_groups;
  TDBLookupComboBox *m_DBCB_class_groups;
  TLabel *m_L_class_groups;
  TDateTimePicker *m_DTP_doc_date;
  TLabel *m_L_doc_date;
  TDataSource *m_DS_gr_deps;
  TLabel *m_L_dep;
  TDBLookupComboBox *m_DBCB_gr_dep;
        TCheckBox *m_CB_nonzero;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_DTP_doc_dateChange(TObject *Sender);
        void __fastcall m_CB_nonzeroClick(TObject *Sender);
private:
  TDInvAct *m_dm;
public:
  __fastcall TFForGroups(TComponent* p_owner, TTSDCustom *p_dm)
                        :TTSFDialog(p_owner),
                        m_dm(static_cast<TDInvAct*>(p_dm)) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
