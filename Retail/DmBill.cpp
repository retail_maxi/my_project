//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmBill.h"  
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "MLLogin"
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "RxQuery"
#pragma link "TSConnectionContainer"
#pragma link "TSDMDOCUMENTWITHLINES"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDBill::TDBill(TComponent* p_owner, AnsiString p_prog_id)
  : TTSDDocumentWithLines(p_owner,p_prog_id),
  m_fldr_rdy(StrToInt64(GetConstValue("FLDR_RETAIL_BILL_RDY"))),
  m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
  SetBeginEndDate(int(GetSysDate()-3),int(GetSysDate()+1));
  m_view_only_vld_arp = true;

  m_Q_orgs->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
}
//---------------------------------------------------------------------------

__fastcall TDBill::~TDBill()
{
  if( m_Q_orgs->Active ) m_Q_orgs->Close();
}
//---------------------------------------------------------------------------

int __fastcall TDBill::GetOrgId()
{
  return m_Q_list->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDBill::SetOrgId(int p_value)
{
  if( GetOrgId() == p_value ) return;

  m_Q_list->ParamByName("ORG_ID")->AsInteger = p_value;

  if( m_Q_list->Active )
  {
    CloseListDataSets();
    OpenListDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TDBill::SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date)
{
  if ((int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date))) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TDBill::m_Q_linesBeforeOpen(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_linesBeforeOpen(DataSet);
  if( m_view_only_vld_arp )
  {
    m_Q_lines->MacroByName("VIEW_ONLY_VLD_ARP")->AsString = m_Q_only_vld_arp->SQL->Text.c_str();
  }
  else
  {
    m_Q_lines->MacroByName("VIEW_ONLY_VLD_ARP")->Clear();
  }

}
//---------------------------------------------------------------------------
void __fastcall TDBill::SetViewOnlyVldArp(bool p_value)
{
  if (p_value != m_view_only_vld_arp)
  {
    m_view_only_vld_arp = p_value;
    RefreshItemDataSets();
  }
}
//---------------------------------------------------------------------------


void __fastcall TDBill::m_Q_reasonBeforeOpen(TDataSet *DataSet)
{
   m_Q_reason->ParamByName("ID")->AsInteger = m_Q_linesID->AsInteger;
   m_Q_reason->ParamByName("LINE_ID")->AsInteger = m_Q_linesLINE_ID->AsInteger;
}
//---------------------------------------------------------------------------



