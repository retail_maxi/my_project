//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmTTNOut.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "MLLogin"
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "RxQuery"
#pragma link "TSConnectionContainer"
#pragma link "TSDMDOCUMENTWITHLINES"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDTTNOut::TDTTNOut(TComponent* p_owner, AnsiString p_prog_id)
  : TTSDDocumentWithLines(p_owner,p_prog_id),
  m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID"))),
  m_fldr_new(StrToInt64(GetConstValue("FLDR_RTL_TTN_OUT_NEW")))
{
  SetBeginEndDate(int(GetSysDate()-7),int(GetSysDate()));
  m_Q_orgs2->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
}
//---------------------------------------------------------------------------

__fastcall TDTTNOut::~TDTTNOut()
{
  if( m_Q_orgs2->Active ) m_Q_orgs2->Close();
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date)
{
  if ((int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date))) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  RefreshListDataSets();
}
//---------------------------------------------------------------------------

int __fastcall TDTTNOut::GetOrgId()
{
  return m_Q_list->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::SetOrgId(int p_value)
{
  if( GetOrgId() == p_value ) return;

  m_Q_list->ParamByName("ORG_ID")->AsInteger = p_value;

  if( m_Q_list->Active )
  {
    CloseListDataSets();
    OpenListDataSets();
  }
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::m_Q_get_in_priceBeforeOpen(TDataSet *DataSet)
{
  m_Q_get_in_price->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
  m_Q_get_in_price->ParamByName("ASSORTMENT_ID")->AsInteger = m_Q_lineASSORTMENT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_itemAfterInsert(DataSet);

  m_Q_itemDOC_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemDOC_NUMBER->AsInteger = StrToIntDef(GetDocNumber(DocTypeId),0);
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::m_Q_orgsBeforeOpen(TDataSet *DataSet)
{
   m_Q_orgs->ParamByName("HOME_ORG_ID")->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::m_Q_barcode_infoBeforeOpen(TDataSet *DataSet)
{
  m_Q_barcode_info->ParamByName("ORG_ID")->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

void __fastcall TDTTNOut::UpdateCard(const TCardOut &p_card_out)
{
    m_Q_upins_line->ParamByName("CARD_ID")->AsInteger = p_card_out.CardId;
    m_Q_upins_line->ParamByName("ITEMS_QNT")->AsFloat  = p_card_out.ItemsQnt;
    m_Q_upins_line->ParamByName("IN_PRICE")->AsFloat  = p_card_out.InPrice;
    m_Q_upins_line->ParamByName("OUT_PRICE")->AsFloat  = p_card_out.OutPrice;
    m_Q_upins_line->ParamByName("ID")->AsInteger  = m_Q_itemID->AsInteger;
    m_Q_upins_line->ParamByName("LINE_ID")->AsInteger  = GetSqNextVal(GetDocumentLineSequenceName());
    if ( p_card_out.Note.IsEmpty() )
      m_Q_upins_line->ParamByName("NOTE")->Clear();
    else m_Q_upins_line->ParamByName("NOTE")->AsString  = p_card_out.Note;
    m_Q_upins_line->ExecSQL();
}

