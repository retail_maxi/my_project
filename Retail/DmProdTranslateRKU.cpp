//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmProdTranslateRKU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------



void __fastcall TDProdTranslateRKU::m_Q_histBeforeOpen(TDataSet *DataSet)
{
  m_Q_hist->ParamByName("ID")->AsInteger = m_Q_listID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDProdTranslateRKU::m_Q_listAfterScroll(TDataSet *DataSet)
{
  if(m_Q_hist->Active) m_Q_hist->Close();
  m_Q_hist->Open();
}
//---------------------------------------------------------------------------

void __fastcall TDProdTranslateRKU::m_Q_assortBeforeOpen(TDataSet *DataSet)
{
  m_Q_assort->ParamByName("NMCL_ID")->AsInteger = m_Q_itemNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDProdTranslateRKU::m_Q_assort_toBeforeOpen(
      TDataSet *DataSet)
{
  m_Q_assort_to->ParamByName("NMCL_ID")->AsInteger = m_Q_itemTO_NMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDProdTranslateRKU::m_Q_gr_dep_toBeforeOpen(
      TDataSet *DataSet)
{
  m_Q_gr_dep_to->ParamByName("ORG_ID")->AsInteger = m_Q_itemORG_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDProdTranslateRKU::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDRefbook::m_Q_itemAfterInsert(DataSet);
  m_Q_itemCOEF->AsInteger = 1;
  //m_Q_itemORG_ID->AsInteger = m_home_org_id;
}
//---------------------------------------------------------------------------

