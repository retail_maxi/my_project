//---------------------------------------------------------------------------

#ifndef FmMessagesRKUH
#define FmMessagesRKUH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmMessagesRKU.h"
#include "TSFmOperation.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Db.hpp>
#include <Grids.hpp>
#include "MLCustomContainer.h"
#include "TSFormContainer.h"
#include <Dialogs.hpp>
#include <DBCtrls.hpp>
//---------------------------------------------------------------------------
class TFMessagesRKU : public TTSFOperation
{
__published:	// IDE-managed Components
    TToolButton *m_TBTN_sep5;
    TMLSetBeginEndDate *m_ACT_set_begin_end_date;
    TSpeedButton *m_SB_begin_end_date;
    TToolBar *m_TB_main_control_buttons_list;
    TSpeedButton *m_SBTN_insert;
    TSpeedButton *m_SBTN_edit;
    TSpeedButton *m_SBTN_delete;
    TSpeedButton *m_SBTN_view;
    TMLDBGrid *m_DBG_main;
    TDataSource *m_DS_main;
    TTSFormContainer *m_TSFC_from_rku;
    TTSFormContainer *m_TSFC_to_rku;
    TAction *m_ACT_insert;
    TAction *m_ACT_edit;
    TAction *m_ACT_delete;
    TAction *m_ACT_view;
    TAction *m_ACT_doc_prop;
    TToolButton *m_TBTN_sep6;
    TToolButton *m_TBTN_doc_prop;
    TPanel *m_P_timer_control;
    TCheckBox *m_CB_timer;
    TEdit *m_E_time;
    TLabel *m_L_timer;
    TLabel *m_L_sec;
    TUpDown *m_UD_time;
    TAction *m_ACT_auto_refresh;
        TDBLookupComboBox *m_DBLCB_organizations;
        TToolButton *ToolButton1;
        TDataSource *m_DS_orgs;
    void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
    void __fastcall m_ACT_insertExecute(TObject *Sender);
    void __fastcall m_ACT_insertUpdate(TObject *Sender);
    void __fastcall m_ACT_editExecute(TObject *Sender);
    void __fastcall m_ACT_editUpdate(TObject *Sender);
    void __fastcall m_ACT_deleteExecute(TObject *Sender);
    void __fastcall m_ACT_deleteUpdate(TObject *Sender);
    void __fastcall m_ACT_viewExecute(TObject *Sender);
    void __fastcall m_ACT_viewUpdate(TObject *Sender);
    void __fastcall m_DBG_mainGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
    void __fastcall m_DBG_mainDblClick(TObject *Sender);
    void __fastcall m_DBG_mainKeyPress(TObject *Sender, char &Key);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall m_ACT_doc_propExecute(TObject *Sender);
    void __fastcall m_ACT_doc_propUpdate(TObject *Sender);
    void __fastcall m_ACT_auto_refreshExecute(TObject *Sender);
    void __fastcall m_T_sbTimer(TObject *Sender);
    void __fastcall m_E_timeChange(TObject *Sender);
        void __fastcall m_DBLCB_organizationsClick(TObject *Sender);
private:	// User declarations
    TDMessagesRKU *m_dm;
    int m_doc_type_id;
    bool m_can_execute_item;
public:		// User declarations
    __fastcall TFMessagesRKU(TComponent* p_owner, TTSDOperation *p_dm_operation);
    __fastcall ~TFMessagesRKU();
};
//---------------------------------------------------------------------------
#endif
