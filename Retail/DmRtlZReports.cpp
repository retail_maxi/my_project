//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmRtlZReports.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TDRtlZReports::TDRtlZReports(TComponent* p_owner,
                                        AnsiString p_prog_id): TTSDDocument(p_owner,
                                                                            p_prog_id),
                                                               m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
  SetBeginEndDate(int(GetSysDate()),GetSysDate());
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDRtlZReports::GetBeginDate()
{
  return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDRtlZReports::GetEndDate()
{
  return m_Q_list->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDRtlZReports::SetBeginEndDate(const TDateTime &p_begin_date,
                                               const TDateTime &p_end_date)
{
  if( ((m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == p_begin_date) &&
      ((m_Q_list->ParamByName("END_DATE")->AsDateTime) == p_end_date) ) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = p_begin_date;
  m_Q_list->ParamByName("END_DATE")->AsDateTime = p_end_date;

  if( m_Q_list->Active ) RefreshListDataSets();
}
//---------------------------------------------------------------------------
void __fastcall TDRtlZReports::m_Q_operBeforeOpen(TDataSet *DataSet)
{
    m_Q_oper->ParamByName("Z_ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------
void __fastcall TDRtlZReports::m_Q_sprmBeforeOpen(TDataSet *DataSet)
{
    m_Q_sprm->ParamByName("Z_ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------
void __fastcall TDRtlZReports::m_Q_retBeforeOpen(TDataSet *DataSet)
{
     m_Q_ret->ParamByName("Z_ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

