//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmNmclAddressList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFNmclAddressList::TFNmclAddressList(TComponent* Owner,
                                                TTSDDocumentWithLines *p_dm)
    : TForm(Owner),
    m_dm(static_cast<TDIncome*>(p_dm))
{
}
//---------------------------------------------------------------------------

void __fastcall TFNmclAddressList::m_ACT_saveExecute(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TFNmclAddressList::m_ACT_show_addrExecute(TObject *Sender)
{
    std::auto_ptr<TFShowAddressList> f (new TFShowAddressList(this, m_dm));
    if (f->ShowModal() == mrOk)
    {
        long m_line = m_dm->m_Q_nmcl_addr_listLINE_ID->AsInteger;
        m_dm->m_Q_nmcl_addr_list->Close();
        m_dm->m_Q_nmcl_addr_list->Open();
        m_dm->m_Q_nmcl_addr_list->Locate(m_dm->m_Q_nmcl_addr_listLINE_ID->FieldName, m_line, TLocateOptions());
    }
}
//---------------------------------------------------------------------------

