inherited FTTNOutLine: TFTTNOutLine
  Left = 528
  Top = 272
  Caption = 'FTTNOutLine'
  ClientHeight = 172
  ClientWidth = 512
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_nmcl: TLabel [0]
    Left = 16
    Top = 36
    Width = 74
    Height = 13
    Caption = '������������'
  end
  object m_L_qnt: TLabel [1]
    Left = 16
    Top = 86
    Width = 59
    Height = 13
    Caption = '����������'
  end
  object m_L_assort: TLabel [2]
    Left = 16
    Top = 60
    Width = 67
    Height = 13
    Caption = '�����������'
  end
  inherited m_P_main_control: TPanel
    Top = 122
    Width = 512
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 318
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 221
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 153
    Width = 512
  end
  inherited m_P_main_top: TPanel
    Width = 512
    TabOrder = 5
    inherited m_P_tb_main_add: TPanel
      Left = 465
    end
    inherited m_P_tb_main: TPanel
      Width = 465
      inherited m_TB_main: TToolBar
        Width = 465
      end
    end
  end
  object m_DBP_nmcl: TMLDBPanel [6]
    Left = 96
    Top = 32
    Width = 400
    Height = 21
    DataField = 'NMCL_NAME'
    DataSource = m_DS_line
    Alignment = taLeftJustify
    BevelWidth = 0
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 1
  end
  object m_DBP_assort: TMLDBPanel [7]
    Left = 96
    Top = 56
    Width = 400
    Height = 21
    DataField = 'ASSORTMENT_NAME'
    DataSource = m_DS_line
    Alignment = taLeftJustify
    BevelWidth = 0
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
  end
  object m_DBE_qnt: TDBEdit [8]
    Left = 96
    Top = 82
    Width = 102
    Height = 21
    DataField = 'ITEMS_QNT'
    DataSource = m_DS_line
    TabOrder = 3
  end
  inherited m_DS_line: TDataSource
    DataSet = DTTNOut.m_Q_line
    Left = 256
    Top = 0
  end
end
