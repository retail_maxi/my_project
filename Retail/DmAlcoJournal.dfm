inherited DAlcoJournal: TDAlcoJournal
  OldCreateOrder = False
  Left = 880
  Top = 123
  Width = 741
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_main: TMLQuery
    BeforeOpen = m_Q_mainBeforeOpen
    SQL.Strings = (
      
        '/*���������� ��������, �� ������� ����� ����������� ������ �����' +
        '� �� �������*/'
      'with quart_no_decl as'
      '(select decl_quart.date_q, count(ad.quart_date)as count_decl'
      
        '  from (SELECT Add_months(trunc(:begin_date, '#39'q'#39'), 3*(level-1)) ' +
        'as date_q '
      '          FROM DUAL'
      
        '        where (MONTHS_BETWEEN(least(ADD_MONTHS(trunc(:end_date, ' +
        #39'q'#39'), 3), trunc(sysdate, '#39'q'#39')), trunc(:begin_date, '#39'q'#39'))/3) >0'
      
        '        CONNECT BY level <= (MONTHS_BETWEEN(least(ADD_MONTHS(tru' +
        'nc(:end_date, '#39'q'#39'), 3), trunc(sysdate, '#39'q'#39')), trunc(:begin_date,' +
        ' '#39'q'#39'))/3)) decl_quart,'
      '       buf_alco_decl_org_taxp ad, documents d'
      ' where ad.quart_date(+) = decl_quart.date_q'
      '   and ad.tax_payer_id(+) =  :taxpayer_id'
      '   and d.id(+) = ad.alco_decl_id'
      '   and d.status(+) !='#39'D'#39
      ' group by decl_quart.date_q'
      ' having count(ad.quart_date) = 0)'
      ''
      'select rownum as num, '
      '          cod_name_inp,'
      '          nmcl_id_inp,'
      '          nmcl_name_inp,'
      '          code_inp,'
      '          sender_name,'
      '          sender_inn,'
      '          ttn_date,'
      '          ttn_number,'
      '          cap_inp,'
      '          qnt_inp,'
      '          sum_inp,'
      '          mark_ch,'
      '          date_ch,'
      '          code_ch,'
      '          cod_name_ch,'
      '          nmcl_id_ch,'
      '          nmcl_name_ch,'
      '          cap_ch,'
      '          qnt_ch,'
      '          sum_ch,'
      '          doc_id_inc,'
      '          doc_id_ch'
      ' from'
      '(select cod_name_inp,'
      '       nmcl_id_inp,'
      '       nmcl_name_inp,'
      '       code_inp,'
      '       sender_name,'
      '       sender_inn,'
      '       ttn_date,'
      '       ttn_number,'
      '       cap_inp,'
      '       qnt_inp,'
      '       sum_inp,'
      '       mark_ch,'
      '       date_ch,'
      '       code_ch,'
      '       cod_name_ch,'
      '       nmcl_id_ch,'
      '       nmcl_name_ch,'
      '       cap_ch,'
      '       qnt_ch,'
      '       sum_ch,'
      '       doc_id_inc,'
      '       doc_id_ch'
      'from       '
      '(select * from '
      ' (/*������� �������*/'
      '   select aci.name as cod_name_inp,'
      '        ni.id as nmcl_id_inp,'
      '        ni.name as nmcl_name_inp,'
      '        aci.code as code_inp,'
      '        cc.name ||'#39' ���('#39' || cc.kpp ||'#39')'#39' as sender_name,'
      '        c.inn as sender_inn,'
      '        buf.oper_date as ttn_date,'
      '        buf.ttn_number,'
      '        nomi.capacity as cap_inp,'
      '        buf.qnt as qnt_inp,'
      '        null as sum_inp,'
      '        null as mark_ch,'
      '        null as date_ch,'
      '        null as code_ch,'
      '        null as cod_name_ch,'
      '        null as nmcl_id_ch,'
      '        null as nmcl_name_ch,'
      '        null as cap_ch,'
      '        null as qnt_ch,'
      '        null as sum_ch,'
      '        buf.doc_id as doc_id_inc,'
      '        null as doc_id_ch,'
      '        buf.oper_date as sort_date,'
      '        1 as s'
      '   from buf_alco_journal buf, nomenclature_items ni,'
      '        companies_contractors cc,'
      '        contractors c,'
      '        tmp_alco_class_item_vers aciv,'
      '         alco_class_items aci,'
      '        nmcl_info nomi'
      
        '  where buf.oper_date between :begin_date and :end_date + 1 -1/2' +
        '4/60/60'
      '    and buf.taxpayer_id = :taxpayer_id  '
      '    and buf.org_id =  :org_id  '
      '    and ni.id  = buf.nmcl_id'
      '    and cc.cnt_id = buf.sender_id'
      '    and c.id = buf.sender_id'
      '    and aciv.nmcl_id = ni.id'
      '    and aci.id = aciv.alco_class_id'
      '    and nomi.nmcl_id = ni.id'
      '    and buf.inpaym = 1'
      '  union all'
      ' /*������� �������*/'
      ' select null,'
      '        null as nmcl_id,'
      '        null as nmcl_name,'
      '        null,'
      '        null as sender_name,'
      '        null as sender_inn,'
      '        null,'
      '        null,'
      '        null,'
      '        null,'
      '        null as sum_inp,'
      '        '#39'�������'#39','
      '        buf.oper_date,'
      '        aci.code,'
      '        aci.name,'
      '        ni.id as nmcl_id_r,'
      '        ni.name as nmcl_name_r,'
      '        nomi.capacity,'
      '        buf.qnt,'
      '        null as sum_r,'
      '        null as doc_id_inc,'
      '        buf.doc_id as doc_id_ch,'
      '        buf.oper_date as sort_date,'
      '        1 as s'
      '   from buf_alco_journal buf, nomenclature_items ni,'
      '        tmp_alco_class_item_vers aciv,'
      '        alco_class_items aci,'
      '        nmcl_info nomi'
      
        '  where buf.oper_date between :begin_date  and :end_date  + 1 -1' +
        '/24/60/60'
      '    and buf.taxpayer_id = :taxpayer_id '
      '    and buf.org_id =  :org_id  '
      '    and ni.id  = buf.nmcl_id'
      '    and aciv.nmcl_id = ni.id'
      '    and aci.id = aciv.alco_class_id'
      '    and nomi.nmcl_id = ni.id'
      '    and buf.inpaym = 0)   '
      '    %WHERE_EXPRESSION'
      ' union all'
      ' /*����� �� ��������� �� ��������� ������*/'
      ' /*�� �����������*/'
      
        ' select '#39'�����'#39' as cod_name_inp, null, null, null, null, null, n' +
        'ull, null, null, null, sum(decode(balco.inpaym, 1, balco.qnt, nu' +
        'll)), '
      
        '        null, null, null, null, null,  null, null, null, sum(dec' +
        'ode(balco.inpaym, 0, balco.qnt, null)), null, null, ADD_MONTHS(b' +
        'alco.quart_date, 3)-1/24/60/60 as sort_date, 0 as s'
      '   from buf_alco_decl_org_taxp balco'
      '  where balco.quart_date <= :end_date  '
      '    and ADD_MONTHS(balco.quart_date, 3)-1 >= :begin_date '
      '    and balco.tax_payer_id = :taxpayer_id  '
      '    and balco.org_id = :org_id  '
      '  group by balco.quart_date'
      'union all'
      '/*�� �������, ���� ������ ������ ������������ �����*/'
      
        ' select '#39'�����'#39' as cod_name_inp, null, null, null, null, null, n' +
        'ull, null, null, null, sum(decode(b.inpaym, 1, (nomi.capacity*b.' +
        'qnt)/10, null)), '
      
        '        null, null, null, null, null, null, null, null, sum(deco' +
        'de(b.inpaym, 0, (nomi.capacity*b.qnt)/10, null)), null, null, AD' +
        'D_MONTHS(qnd.date_q, 3)-1/24/60/60 as sort_date, 0 as s'
      
        '  from buf_alco_journal b, quart_no_decl qnd, nomenclature_items' +
        ' ni, nmcl_info nomi, shop_departments sd'
      
        ' where b.oper_date between qnd.date_q and ADD_MONTHS(qnd.date_q,' +
        ' 3)-1/24/60/60'
      '   and sd.id(+) = b.shop_dep_id'
      '   and nvl(b.taxpayer_id, sd.taxpayer_id) = :taxpayer_id'
      '   and b.org_id = :org_id'
      '   and ni.id  = b.nmcl_id'
      '   and nomi.nmcl_id = ni.id'
      '   and 1 = :gen'
      'Group by  qnd.date_q)'
      'Order by  sort_date, s  desc)')
    UpdateObject = nil
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'taxpayer_id'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'taxpayer_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'org_id'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'taxpayer_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'org_id'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'taxpayer_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'org_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'taxpayer_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'org_id'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'gen'
        ParamType = ptUnknown
      end>
    object m_Q_mainNUM: TFloatField
      DisplayLabel = '� �/�'
      FieldName = 'NUM'
    end
    object m_Q_mainCOD_NAME_INP: TStringField
      DisplayLabel = '�����������||��� � ������������ ���������'
      FieldName = 'COD_NAME_INP'
      Origin = 'COD_NAME_INP'
      Size = 250
    end
    object m_Q_mainNMCL_ID_INP: TFloatField
      DisplayLabel = '�����������||NMCL_ID'
      FieldName = 'NMCL_ID_INP'
      Origin = 'NMCL_ID_INP'
    end
    object m_Q_mainNMCL_NAME_INP: TStringField
      DisplayLabel = '�����������||������������'
      FieldName = 'NMCL_NAME_INP'
      Origin = 'NMCL_NAME_INP'
      Size = 100
    end
    object m_Q_mainCODE_INP: TStringField
      DisplayLabel = '�����������||��� ���� ���������'
      FieldName = 'CODE_INP'
      Origin = 'CODE_INP'
      Size = 10
    end
    object m_Q_mainSENDER_NAME: TStringField
      DisplayLabel = '�����������||��������� ���������||������������ �����������'
      FieldName = 'SENDER_NAME'
      Origin = 'SENDER_NAME'
      Size = 115
    end
    object m_Q_mainSENDER_INN: TStringField
      DisplayLabel = '�����������||��������� ���������||���'
      FieldName = 'SENDER_INN'
      Origin = 'SENDER_INN'
      Size = 12
    end
    object m_Q_mainTTN_DATE: TDateTimeField
      DisplayLabel = '�����������||���||����'
      FieldName = 'TTN_DATE'
      Origin = 'TTN_DATE'
    end
    object m_Q_mainTTN_NUMBER: TStringField
      DisplayLabel = '�����������||���||�����'
      FieldName = 'TTN_NUMBER'
      Origin = 'TTN_NUMBER'
      Size = 50
    end
    object m_Q_mainCAP_INP: TFloatField
      DisplayLabel = '�����������||���||������� ����(��������) (�)'
      FieldName = 'CAP_INP'
      Origin = 'CAP_INP'
    end
    object m_Q_mainQNT_INP: TFloatField
      DisplayLabel = '�����������||���||���������� ����(��������) '
      FieldName = 'QNT_INP'
      Origin = 'QNT_INP'
    end
    object m_Q_mainSUM_INP: TFloatField
      DisplayLabel = '�����������||����� ��������� �� �������� ������, ���'
      FieldName = 'SUM_INP'
    end
    object m_Q_mainMARK_CH: TStringField
      DisplayLabel = '������||���������� ������'
      FieldName = 'MARK_CH'
      Origin = 'MARK_CH'
      Size = 7
    end
    object m_Q_mainDATE_CH: TDateTimeField
      DisplayLabel = '������||���� ��������'
      FieldName = 'DATE_CH'
      Origin = 'DATE_CH'
    end
    object m_Q_mainCODE_CH: TStringField
      DisplayLabel = '������||��� � ������������ ���������||��� ���� ��'
      FieldName = 'CODE_CH'
      Origin = 'CODE_CH'
      Size = 10
    end
    object m_Q_mainCOD_NAME_CH: TStringField
      DisplayLabel = '������||��� � ������������ ���������||��� ��'
      FieldName = 'COD_NAME_CH'
      Origin = 'COD_NAME_CH'
      Size = 250
    end
    object m_Q_mainNMCL_ID_CH: TFloatField
      DisplayLabel = '������||��� � ������������ ���������||NMCL_ID'
      FieldName = 'NMCL_ID_CH'
      Origin = 'NMCL_ID_CH'
    end
    object m_Q_mainNMCL_NAME_CH: TStringField
      DisplayLabel = '������||��� � ������������ ���������||������������'
      FieldName = 'NMCL_NAME_CH'
      Origin = 'NMCL_NAME_CH'
      Size = 100
    end
    object m_Q_mainCAP_CH: TFloatField
      DisplayLabel = '������||������� ���� (��������)(�)'
      FieldName = 'CAP_CH'
      Origin = 'CAP_CH'
    end
    object m_Q_mainQNT_CH: TFloatField
      DisplayLabel = '������||���������� ���� (��������)'
      FieldName = 'QNT_CH'
      Origin = 'QNT_CH'
    end
    object m_Q_mainSUM_CH: TFloatField
      DisplayLabel = '������||����� ������ �� �������� ������, ���'
      FieldName = 'SUM_CH'
    end
    object m_Q_mainDOC_ID_INC: TFloatField
      DisplayLabel = 'ID ���'
      FieldName = 'DOC_ID_INC'
      Origin = 'DOC_ID_INC'
    end
    object m_Q_mainDOC_ID_CH: TFloatField
      DisplayLabel = 'ID ����. �������(��)'
      FieldName = 'DOC_ID_CH'
      Origin = 'DOC_ID_CH'
    end
  end
  object m_Q_taxpayer: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name'
      'from taxpayer'
      'where id in (2000906,150,91,152,142,145) '
      '%WHERE_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 32
    Top = 200
    object m_Q_taxpayerID: TFloatField
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_taxpayerNAME: TStringField
      DisplayLabel = '������������'
      DisplayWidth = 100
      FieldName = 'NAME'
      Origin = 'name'
      Size = 100
    end
  end
  object m_Q_taxpayer_fict: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select 1 as id, rpad('#39' '#39',250) as name'
      'from dual')
    UpdateObject = m_U_taxpayer_fict
    Macros = <>
    Left = 120
    Top = 200
    object m_Q_taxpayer_fictID: TFloatField
      DisplayWidth = 30
      FieldName = 'ID'
    end
    object m_Q_taxpayer_fictNAME: TStringField
      FieldName = 'NAME'
      Size = 250
    end
  end
  object m_U_taxpayer_fict: TMLUpdateSQL
    Left = 216
    Top = 200
  end
  object m_Q_org: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select name from organizations '
      'where id = :ID')
    Macros = <>
    Left = 32
    Top = 264
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_orgNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_SP_fill_tmp_alco_class_item_v: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'GLOBALREF.PRC_FILL_TMP_ALCO_CLASS_ITEM_V'
    Left = 320
    Top = 136
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'P_BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'P_END_DATE'
        ParamType = ptInput
      end>
  end
  object m_SP_fill_buf_alco_jour: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'GLOBALREF.PRC_FILL_BUF_ALCO_JOUR'
    Left = 112
    Top = 264
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'P_DATE'
        ParamType = ptInput
      end>
  end
end
