//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmSelectOrg.h"
#include "MLFuncs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLQuery"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
bool ExecSelectOrgDlg(TComponent *p_owner,
                      hDBIDb p_db_handle)
{
  TFSelectOrg *f_gl = new TFSelectOrg(p_owner, p_db_handle);
  bool res = false;
  try
  {
    f_gl->ShowModal();
    res = f_gl->PressOK;
  }
  __finally
  {
    delete f_gl;
  }
  return res;
}
//---------------------------------------------------------------------------

__fastcall TFSelectOrg::TFSelectOrg(TComponent* p_owner,
                                    hDBIDb p_db_handle): TForm(p_owner),
                                    m_db_handle(p_db_handle),
                                    m_sq_id(-1)
{
  FPressOK = false;
  load_data = false;
  m_DB_gl->Handle = m_db_handle;
}
//---------------------------------------------------------------------------

__fastcall TFSelectOrg::~TFSelectOrg()
{
  if( m_Q_organizations_list->Active) m_Q_organizations_list->Close();
  m_DB_gl->Handle = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFSelectOrg::m_ACT_closeExecute(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSelectOrg::m_ACT_apply_updatesExecute(TObject *Sender)
{
  TStrings *StringList = new TStringList();
  for( int i = 0; i < m_CLB_org->Items->Count; i++)
  {
    if (m_CLB_org->Checked[i])
      StringList->Add(AnsiString((int)m_CLB_org->Items->Objects[i]));
  }
  m_sq_id = FillTmpReportParamValues(StringList, m_sq_id);

  m_SP_create_doc->ParamByName("v_tmp_id")->AsInteger = m_sq_id;
  m_SP_create_doc->ParamByName("v_file_name")->AsString = ExtractFileName(m_OD_import_Excel->FileName);
  m_SP_create_doc->ExecProc();

  FPressOK = true;
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSelectOrg::FormShow(TObject *Sender)
{
  m_Q_organizations_list->Open();
  m_Q_organizations_list->First();
  m_CLB_org->Items->Clear();
  int i = 0;
  while (!m_Q_organizations_list->Eof)
  {
    m_CLB_org->Items->AddObject(m_Q_organizations_listNAME->AsString,(TObject *)m_Q_organizations_listID->AsInteger);
    if (m_Q_organizations_listCHECKED->AsInteger == 1)
      m_CLB_org->Checked[i] = true;
    else
      m_CLB_org->Checked[i] = false;
    m_Q_organizations_list->Next();
    i = i + 1;
  }

  if( m_Q_organizations_list->Active ) m_Q_organizations_list->Close();
}
//---------------------------------------------------------------------------

bool __fastcall TFSelectOrg::GetPressOK()
{
  return FPressOK;
}
//---------------------------------------------------------------------------


void __fastcall TFSelectOrg::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  m_DB_gl->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSelectOrg::m_Q_organizations_listBeforeOpen(
      TDataSet *DataSet)
{
  m_Q_organizations_list->ParamByName("SQ_ID")->AsInteger = m_sq_id;
}
//---------------------------------------------------------------------------

long __fastcall TFSelectOrg::FillTmpReportParamValues(TStrings *p_str,
                                                     long p_sq)
{
  long sq = p_sq;
  if( sq == -1 ) sq = GetSqNextVal("SQ_TMP_REPORT_PARAM_VALUES");

  m_Q_kill_tmp_rep_param_val->ParamByName("ID")->AsInteger = sq;
  m_Q_kill_tmp_rep_param_val->ExecSQL();

  m_Q_fill_tmp_rep_param_val->ParamByName("ID")->AsInteger = sq;
  m_Q_fill_tmp_rep_param_val->Open();
  try
  {
    for( int i = 0; i < p_str->Count; i++ )
    {
      m_Q_fill_tmp_rep_param_val->Insert();
      m_Q_fill_tmp_rep_param_valID->AsInteger = sq;
      m_Q_fill_tmp_rep_param_valVALUE->AsString = p_str->Strings[i];
      m_Q_fill_tmp_rep_param_val->Post();
    }

    m_DB_gl->ApplyUpdates(OPENARRAY(TDBDataSet*,((TDBDataSet*)m_Q_fill_tmp_rep_param_val)));
  }
  __finally
  {
    m_Q_fill_tmp_rep_param_val->Close();
  }
  return sq;
}
//---------------------------------------------------------------------------

int __fastcall TFSelectOrg::GetSqNextVal(const AnsiString &p_sq_name)
{
  int res = -1;

  m_Q_sq_next_val->SQL->Clear();
  m_Q_sq_next_val->SQL->Add("SELECT " + p_sq_name + ".NEXTVAL AS sq_next FROM dual");
  m_Q_sq_next_val->Open();
  try
  {
    res = m_Q_sq_next_val->FieldByName("SQ_NEXT")->AsInteger;
  }
  __finally
  {
    m_Q_sq_next_val->Close();
  }

  return res;
}
//---------------------------------------------------------------------------

void __fastcall TFSelectOrg::m_ACT_sel_fileExecute(TObject *Sender)
{
   if (m_OD_import_Excel->Execute())
  {
    Variant Excel;
    try
    {
      Excel = CreateOleObject("EXCEL.Application");
    }
    catch (...)
    {
      MessageBox(0, "������ ��� �������� ������� excel","������", MB_OK);
      return;
    }
    Excel.OlePropertyGet("Workbooks").OlePropertyGet("Open",m_OD_import_Excel->FileName.c_str());

    m_Q_del_data->ExecSQL();
    load_data = false;
    
    int nmcl_id  = 0;
    int assort_id  = 0;
    AnsiString note = "";
    int rs_type_id = 0;
    int rec_count = 0;

    for (int i = 2; i < 65000; i++)
    {
      try
      {
        nmcl_id = Excel.OlePropertyGet("Cells",i,1);
        assort_id = Excel.OlePropertyGet("Cells",i,3);

        note = Excel.OlePropertyGet("Cells",i,5);
        rs_type_id = Excel.OlePropertyGet("Cells",i,6);
      }
      catch (...)
      {
        ShowMessage("������ ���� ������. ���������� "+IntToStr(rec_count)+" �������.");
        Excel.OleProcedure("Quit");
        return;
      }

      if (nmcl_id == 0)
      {
        Excel.OleProcedure("Quit");
        ShowMessage("���������� " + IntToStr(rec_count) + " �������.");
        if (rec_count > 0) load_data = true;
        return;
      }
      else
      {
        try
        {
          //������� ������

          if (nmcl_id != 0 && assort_id != 0)
          {
            m_Q_insert_data->ParamByName("nmcl_id")->AsInteger = nmcl_id;
            m_Q_insert_data->ParamByName("assort_id")->AsInteger = assort_id;
            m_Q_insert_data->ParamByName("note")->AsString = note;
            m_Q_insert_data->ParamByName("rs_type_id")->AsInteger = rs_type_id;
            m_Q_insert_data->ExecSQL();
          }
          rec_count++;
        }
        catch (...)
        {
           ShowMessage("������ ��� ��������. ���������� "+IntToStr(rec_count)+" �������.");
           Excel.OleProcedure("Quit");
           return;
        }
      }
    }
    Excel.OleProcedure("Quit");
   }
   else ShowMessage("���� �� ��� ������.");
}
//---------------------------------------------------------------------------

void __fastcall TFSelectOrg::m_ACT_apply_updatesUpdate(TObject *Sender)
{
   m_ACT_apply_updates->Enabled = load_data;
}
//---------------------------------------------------------------------------

