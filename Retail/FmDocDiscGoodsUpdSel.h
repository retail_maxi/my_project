//---------------------------------------------------------------------------

#ifndef FmDocDiscGoodsUpdSelH
#define FmDocDiscGoodsUpdSelH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFMSUBITEM.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmDocDiscGoods.h"
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TFDocDiscGoodsUpdSel : public TTSFSubItem
{
__published:	// IDE-managed Components
        TDataSource *m_DS_upd_param;
        TPanel *m_P_top;
        TLabel *m_L_act_price;
        TLabel *m_L_price_bef_act;
        TLabel *m_L_period;
        TLabel *m_L_end;
        TDBDateEdit *m_DBDE_begin;
        TDBDateEdit *m_DBDE_END_DATE;
        TDBEdit *m_DBE_act_price;
        TDBEdit *m_DBE_price_bef_act;
        TGroupBox *m_GR_rtt;
        TMLDBGrid *m_DBG_list;
        TBitBtn *m_BB__BTN_close;
        TDataSource *m_DS_orgs;
        TAction *m_ACT_sel_all;
        TDBCheckBox *m_DBCB_urgent_reprice;
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall m_DBG_listKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall m_ACT_sel_allExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
  ETSUpdateRegime m_regime;
  TDDocDiscGoods *m_dm;
  TWinControl *m_start_win_control;
  bool __fastcall FillNodesCheck(int node_id, bool m_check);
  bool FPressOK;
  bool __fastcall GetPressOK();
  void __fastcall BeforeApply(TObject *Sender);
protected:
    virtual AnsiString __fastcall BeforeUpdSelApply(TWinControl **p_focused);
public:		// User declarations
        __fastcall TFDocDiscGoodsUpdSel(TComponent* p_owner,
                             TDDocDiscGoods *p_dm_doc_with_ln,
                             bool ch = false);
         __fastcall ~TFDocDiscGoodsUpdSel(){};
          __property bool PressOK  = { read=GetPressOK };
};
//---------------------------------------------------------------------------
#endif
 