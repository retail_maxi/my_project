inherited FRtlPlanMarginSeedList: TFRtlPlanMarginSeedList
  Left = 722
  Top = 122
  Width = 965
  Caption = 'FRtlPlanMarginSeedList'
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Width = 949
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 852
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 464
    end
  end
  inherited m_SB_main: TStatusBar
    Width = 949
  end
  inherited m_DBG_list: TMLDBGrid
    Top = 57
    Width = 949
    Height = 380
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'PURCHASE'
        PickList.Strings = ()
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TRANSFER'
        PickList.Strings = ()
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'PRICE_GM'
        PickList.Strings = ()
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'PRICE_SM'
        PickList.Strings = ()
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 949
    inherited m_P_tb_main: TPanel
      Width = 902
      inherited m_TB_main: TToolBar
        Width = 902
        Height = 24
        ButtonHeight = 22
        inherited m_SB_ods: TSpeedButton
          Height = 22
        end
        inherited m_SB_xlsx: TSpeedButton
          Height = 22
        end
        object ToolButton1: TToolButton
          Left = 270
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 16
          Style = tbsSeparator
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 902
    end
  end
  object m_P_filters: TPanel [4]
    Left = 0
    Top = 26
    Width = 949
    Height = 31
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object m_L_nmcl: TLabel
      Left = 17
      Top = 8
      Width = 61
      Height = 13
      Caption = '����������'
    end
    object m_DBE_note: TDBEdit
      Left = 87
      Top = 5
      Width = 391
      Height = 21
      DataField = 'PURCHASE'
      TabOrder = 0
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_actived: TAction
      Category = 'Custom'
      Caption = '����������� ��'
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DRtlPlanMarginSeed.m_Q_list
  end
end
