inherited FRtlZReportsItem: TFRtlZReportsItem
  Left = 309
  Top = 177
  Width = 839
  Height = 635
  BorderStyle = bsSizeable
  Caption = 'FRtlZReportsItem'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object m_S_oper_sprm: TSplitter [0]
    Left = 0
    Top = 321
    Width = 823
    Height = 5
    Cursor = crVSplit
    Align = alTop
    Beveled = True
  end
  inherited m_P_main_control: TPanel
    Top = 547
    Width = 823
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 726
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 435
      inherited m_SBTN_insert: TSpeedButton
        Visible = False
      end
      inherited m_BBTN_save: TBitBtn
        Visible = False
      end
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 241
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 578
    Width = 823
  end
  inherited m_P_main_top: TPanel
    Width = 823
    inherited m_P_tb_main: TPanel
      Width = 776
      inherited m_TB_main: TToolBar
        Width = 776
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 776
    end
  end
  object m_P_header: TPanel [4]
    Left = 0
    Top = 26
    Width = 823
    Height = 119
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object m_L_num_fr: TLabel
      Left = 16
      Top = 8
      Width = 29
      Height = 13
      Caption = '���'
      FocusControl = m_DBE_FISCAL_NUM
    end
    object m_L_num_rep: TLabel
      Left = 152
      Top = 8
      Width = 44
      Height = 13
      Caption = '�������'
      FocusControl = m_DBE_REPORT_NUM
    end
    object m_L_taxpayer: TLabel
      Left = 309
      Top = 8
      Width = 99
      Height = 13
      Caption = '����������������'
      FocusControl = m_DBE_TAXPAYER_NAME
    end
    object m_L_print_date: TLabel
      Left = 629
      Top = 8
      Width = 63
      Height = 13
      Caption = '���� ������'
      FocusControl = m_DBE_PRINT_DATE
    end
    object m_L_rep_sum: TLabel
      Left = 16
      Top = 36
      Width = 70
      Height = 13
      Caption = '����� ������'
      FocusControl = m_DBE_REPORT_SUM
    end
    object m_L_sale_sum: TLabel
      Left = 629
      Top = 36
      Width = 75
      Height = 13
      Caption = '����� ������'
      FocusControl = m_DBE_SALES_SUM
    end
    object m_L_tax10: TLabel
      Left = 16
      Top = 68
      Width = 76
      Height = 13
      Caption = '����� ��� 10'
    end
    object m_L_tax18: TLabel
      Left = 227
      Top = 68
      Width = 76
      Height = 13
      Caption = '����� ��� 18'
    end
    object m_L_sprm_sum: TLabel
      Left = 235
      Top = 36
      Width = 68
      Height = 13
      Caption = '����� ����'
    end
    object m_L_stock_name: TLabel
      Left = 424
      Top = 68
      Width = 80
      Height = 13
      Caption = '�������������'
      FocusControl = m_DBP_stock_name
    end
    object Label1: TLabel
      Left = 424
      Top = 36
      Width = 78
      Height = 13
      Caption = '����� �������'
    end
    object m_L_ret_sum: TLabel
      Left = 16
      Top = 100
      Width = 90
      Height = 13
      Caption = '����� ���������'
    end
    object m_DBE_FISCAL_NUM: TMLDBPanel
      Left = 53
      Top = 5
      Width = 80
      Height = 21
      DataField = 'FISCAL_NUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 0
    end
    object m_DBE_REPORT_NUM: TMLDBPanel
      Left = 210
      Top = 5
      Width = 80
      Height = 21
      DataField = 'REPORT_NUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 1
    end
    object m_DBE_TAXPAYER_NAME: TMLDBPanel
      Left = 414
      Top = 5
      Width = 202
      Height = 21
      DataField = 'TAXPAYER_NAME'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 2
    end
    object m_DBE_PRINT_DATE: TMLDBPanel
      Left = 699
      Top = 5
      Width = 112
      Height = 21
      DataField = 'PRINT_DATE'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 3
    end
    object m_DBE_REPORT_SUM: TMLDBPanel
      Left = 109
      Top = 33
      Width = 100
      Height = 21
      DataField = 'REPORT_SUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 4
    end
    object m_DBE_SALES_SUM: TMLDBPanel
      Left = 711
      Top = 33
      Width = 100
      Height = 21
      DataField = 'SALES_SUM_DISC'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 6
    end
    object m_DBE_SRPM_SUM: TMLDBPanel
      Left = 310
      Top = 33
      Width = 100
      Height = 21
      DataField = 'SRPM_SUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 5
    end
    object m_DBP_REPORT_SUM: TMLDBPanel
      Left = 109
      Top = 64
      Width = 100
      Height = 21
      DataField = 'REPORT_10_SUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 7
    end
    object m_DBP_SRPM_SUM: TMLDBPanel
      Left = 311
      Top = 63
      Width = 100
      Height = 21
      DataField = 'REPORT_18_SUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 8
    end
    object m_DBP_stock_name: TMLDBPanel
      Left = 514
      Top = 64
      Width = 297
      Height = 21
      DataField = 'STOCK_NAME'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 9
    end
    object m_DBE_BONUS_SUM: TMLDBPanel
      Left = 510
      Top = 33
      Width = 100
      Height = 21
      DataField = 'BONUS_POINT_SUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 10
    end
    object m_DBP_RET_SUM: TMLDBPanel
      Left = 109
      Top = 96
      Width = 100
      Height = 21
      DataField = 'RET_SUM'
      DataSource = m_DS_item
      BevelOuter = bvLowered
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 11
    end
  end
  object m_P_oper: TPanel [5]
    Left = 0
    Top = 145
    Width = 823
    Height = 176
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object m_DBG_oper: TMLDBGrid
      Left = 0
      Top = 20
      Width = 823
      Height = 156
      Align = alClient
      DataSource = m_DS_oper
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterColor = clWindow
      AutoFitColWidths = True
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
      Columns = <
        item
          FieldName = 'CNT_NAME'
          Title.TitleButton = True
          Width = 250
          Footers = <>
        end
        item
          FieldName = 'CNT_SUM'
          Title.TitleButton = True
          Footers = <>
        end
        item
          FieldName = 'RET_SUM'
          Title.TitleButton = True
          Footers = <>
        end>
    end
    object m_P_oper_header: TPanel
      Left = 0
      Top = 0
      Width = 823
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object m_L_oper_header: TLabel
        Left = 16
        Top = 2
        Width = 122
        Height = 13
        Caption = '� ������� ��������'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object m_P_sprm: TPanel [6]
    Left = 0
    Top = 326
    Width = 823
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 5
    object m_PC_spec: TPageControl
      Left = 0
      Top = 0
      Width = 823
      Height = 221
      ActivePage = m_TS_srpm
      Align = alClient
      TabOrder = 0
      object m_TS_srpm: TTabSheet
        Caption = '����'
        object m_DBG_srpm: TMLDBGrid
          Left = 0
          Top = 0
          Width = 815
          Height = 193
          Align = alClient
          DataSource = m_DS_sprm
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterColor = clWindow
          AutoFitColWidths = True
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
          Columns = <
            item
              FieldName = 'DEP_NUMBER'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'NMCL_ID'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'NMCL_NAME'
              Title.TitleButton = True
              Width = 150
              Footers = <>
            end
            item
              FieldName = 'NDS'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'OUT_PRICE'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'QUANTITY'
              Title.TitleButton = True
              Footers = <>
            end>
        end
      end
      object m_TS_ret: TTabSheet
        Caption = '��������'
        ImageIndex = 1
        object m_DBG_ret: TMLDBGrid
          Left = 0
          Top = 0
          Width = 815
          Height = 193
          Align = alClient
          DataSource = m_DS_ret
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterColor = clWindow
          AutoFitColWidths = True
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
          Columns = <
            item
              FieldName = 'DEP_NUMBER'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'NMCL_ID'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'NMCL_NAME'
              Title.TitleButton = True
              Width = 150
              Footers = <>
            end
            item
              FieldName = 'NDS'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'OUT_PRICE'
              Title.TitleButton = True
              Footers = <>
            end
            item
              FieldName = 'QUANTITY'
              Title.TitleButton = True
              Footers = <>
            end>
        end
      end
    end
  end
  inherited m_DS_item: TDataSource
    DataSet = DRtlZReports.m_Q_item
    Left = 496
    Top = 352
  end
  object m_DS_oper: TDataSource
    DataSet = DRtlZReports.m_Q_oper
    Left = 32
    Top = 113
  end
  object m_DS_sprm: TDataSource
    DataSet = DRtlZReports.m_Q_sprm
    Left = 24
    Top = 190
  end
  object m_DS_ret: TDataSource
    DataSet = DRtlZReports.m_Q_ret
    Left = 64
    Top = 190
  end
end
