//---------------------------------------------------------------------------

#ifndef FmInvActAlcoH
#define FmInvActAlcoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <Db.hpp>
#include "MLQuery.h"
#include "RxQuery.hpp"
#include <DBTables.hpp>
#include "DmInvAct.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Mask.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include "TSFMDOCUMENTWITHLINES.h"
#include "MLDBPanel.h"
//---------------------------------------------------------------------------
class TFInvActAlco : public TTSFDialog
{
__published:	// IDE-managed Components
        TDBEdit *m_DBE_alccode;
        TMLLovListView *m_LV_alccode;
        TLabel *Label10;
        TDataSource *m_DS_alccode;
        TMLLov *m_LOV_alccode;
        TLabel *Label3;
        TDBEdit *m_DBE_recalc_items;
        TDataSource *m_DS_alco;
        TDBEdit *m_DBE_fact_items;
        TLabel *Label1;
        TMLDBPanel *m_P_start_items;
        TLabel *Label2;
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
  TDInvAct *m_dm;
public:		// User declarations
        __fastcall TFInvActAlco(TComponent* p_owner,
                                  TTSDDocumentWithLines *p_dm);
};
//---------------------------------------------------------------------------
bool ExecInvActAlcoDlg(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
//---------------------------------------------------------------------------
#endif
