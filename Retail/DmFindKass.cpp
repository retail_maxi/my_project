//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmFindKass.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TDFindKass::RefreshListDataSets()
{
  if (m_Q_tmp_check_list->Active) m_Q_tmp_check_list->Close();
  m_Q_tmp_check_list->Open(); 
}
