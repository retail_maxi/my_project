inherited FCompetitorsRepriceLine: TFCompetitorsRepriceLine
  Left = 386
  Top = 311
  ActiveControl = m_DBE_COMP_PRICE
  ClientHeight = 187
  ClientWidth = 517
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_nmcl: TLabel [0]
    Left = 32
    Top = 49
    Width = 30
    Height = 13
    Caption = '�����'
  end
  object m_L_competitor: TLabel [1]
    Left = 7
    Top = 80
    Width = 55
    Height = 13
    Caption = '���������'
  end
  object m_L_comp_price: TLabel [2]
    Left = 35
    Top = 109
    Width = 26
    Height = 13
    Caption = '����'
  end
  object m_L_rub: TLabel [3]
    Left = 200
    Top = 108
    Width = 22
    Height = 13
    Caption = '���.'
  end
  inherited m_P_main_control: TPanel
    Top = 137
    Width = 517
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 323
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 226
      inherited m_SBTN_insert: TSpeedButton
        Visible = False
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 168
    Width = 517
  end
  inherited m_P_main_top: TPanel
    Width = 517
    inherited m_P_tb_main_add: TPanel
      Left = 470
    end
    inherited m_P_tb_main: TPanel
      Width = 470
      inherited m_TB_main: TToolBar
        Width = 470
      end
    end
  end
  object m_DBE_nmcl: TMLDBPanel [7]
    Left = 72
    Top = 45
    Width = 417
    Height = 21
    DataField = 'NMCL_NAME'
    DataSource = m_DS_line
    Alignment = taRightJustify
    BevelWidth = 0
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 3
  end
  object m_DBE_NMCL_NAME: TMLDBPanel [8]
    Left = 72
    Top = 77
    Width = 417
    Height = 21
    DataField = 'COMP_NAME'
    DataSource = m_DS_comp
    Alignment = taRightJustify
    BevelWidth = 0
    BorderWidth = 1
    BorderStyle = bsSingle
    Color = clBtnFace
    UseDockManager = True
    ParentColor = False
    TabOrder = 4
  end
  object m_DBE_COMP_PRICE: TDBEdit [9]
    Left = 72
    Top = 104
    Width = 121
    Height = 21
    DataField = 'COMP_PRICE'
    DataSource = m_DS_line
    TabOrder = 5
  end
  inherited m_DS_line: TDataSource
    DataSet = DCompetitorsReprice.m_Q_line
  end
  object m_DS_comp: TDataSource
    DataSet = DCompetitorsReprice.m_Q_comp_one
    Left = 16
    Top = 80
  end
end
