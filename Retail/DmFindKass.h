//---------------------------------------------------------------------------

#ifndef DmFindKassH
#define DmFindKassH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMOPERATION.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------

class TDFindKass : public TTSDOperation
{
__published:
  TMLQuery *m_Q_list;
  TFloatField *m_Q_listSALE_DOC_ID;
  TFloatField *m_Q_listPRINT_USER_ID;
  TStringField *m_Q_listPRINT_USER_NAME;
  TFloatField *m_Q_listCHECK_SUM;
  TStringField *m_Q_listHOST;
        TMLQuery *m_Q_tmp_check_list;
        TFloatField *m_Q_tmp_check_listID;
        TFloatField *m_Q_tmp_check_listFISCAL_NUM;
        TFloatField *m_Q_tmp_check_listCHECK_NUM;
        TDateTimeField *m_Q_tmp_check_listCHECK_DATE;
        TFloatField *m_Q_tmp_check_listERROR_QNT;
        TStringField *m_Q_tmp_check_listT_CUSTOMER;
        TStringField *m_Q_tmp_check_listPRINT_USER_NAME;
        TStringField *m_Q_tmp_check_listDEP_NAME;
        TStringField *m_Q_tmp_check_listPOST_NAME;
        TMLQuery *m_Q_tmp_check_ins;
        TMLQuery *m_Q_tmp_check_del;
        TStringField *m_Q_tmp_check_listARTT;
        TStringField *m_Q_tmp_check_listZKS;
        TStringField *m_Q_tmp_check_listSK;
public:
  __fastcall TDFindKass(TComponent* p_owner,
                        AnsiString p_prog_id): TTSDOperation(p_owner,
                                                             p_prog_id) {};
  void __fastcall RefreshListDataSets();
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

