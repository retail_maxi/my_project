//---------------------------------------------------------------------------

#ifndef FmUnprocessDocumentH
#define FmUnprocessDocumentH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMOPERATION.h"
#include "DmUnprocessDocument.h"
#include "TSFmOperation.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Db.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "TSFormControls.h"
#include <Dialogs.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "VCLUnZip.hpp"
#include "VCLZip.hpp"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TFUnprocessDocument : public TTSFOperation
{
__published:	// IDE-managed Components
        TDataSource *m_DS_main;
        TMLDBGrid *m_DBG_main;
        TAction *m_ACT_complete;
        TAction *m_ACT_uncomplete;
        TAction *m_ACT_save;
        TToolButton *ToolButton1;
        TDataSource *m_DS_org;
        TMLLov *m_LOV_org;
        TMLLovListView *m_LV_org;
        TToolButton *ToolButton2;
        TLabel *m_L_org;
        TDataSource *m_DS_org_fict;
        TAction *m_ACT_save_dll;
        TMLSetBeginEndDate *m_ACT_set_begin_end_date;
        TSpeedButton *btn_SBTN_set_date;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
private:	// User declarations
    TDUnprocessDocument *m_dm;
    TVCLZip *m_VCLZip;

public:		// User declarations
  __fastcall TFUnprocessDocument(TComponent* p_owner, TTSDOperation *p_dm_operation);
};
//---------------------------------------------------------------------------
extern PACKAGE TFUnprocessDocument *FUnprocessDocument;
//---------------------------------------------------------------------------
#endif
