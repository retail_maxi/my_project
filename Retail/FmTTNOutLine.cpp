//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmTTNOutLine.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmDocumentLine"
#pragma link "MLDBPanel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
AnsiString __fastcall TFTTNOutLine::BeforeLineApply(TWinControl *p_focused)
{
  if (m_dm->m_Q_lineITEMS_QNT->AsFloat <= 0)
  {
    return "������� ����������";
  }
  if (m_dm->m_Q_lineITEMS_QNT->AsFloat > m_dm->m_Q_lineCUR_REST->AsFloat)
  {
    return "������������ ���������� ������ ������� �� ������";
  }
  if( !Warning.IsEmpty() ) TSExecMessageDlg(this,tmWarning,Warning);

  return "";
}

