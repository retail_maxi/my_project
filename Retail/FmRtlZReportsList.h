//---------------------------------------------------------------------------

#ifndef FmRtlZReportsListH
#define FmRtlZReportsListH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENTLIST.h"
#include "DmRtlZReports.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFRtlZReportsList : public TTSFDocumentList
{
__published:
  TToolButton *m_TBTN_sep6;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
   TSpeedButton *m_SBTN_period;
   TAction *m_ACT_set_period;
  void __fastcall FormShow(TObject *Sender);
   void __fastcall m_ACT_set_periodExecute(TObject *Sender);
private:
  TDRtlZReports *m_dm;
public:
  __fastcall TFRtlZReportsList(TComponent* p_owner, TTSDDocument *p_dm_document);
   void __fastcall SetPeriodCaption(TDateTime p_begin_date, TDateTime p_end_date);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

