inherited DAssortException: TDAssortException
  OldCreateOrder = False
  Left = 603
  Top = 221
  Width = 741
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=ROBOT'
      'PASSWORD=RBT')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'select v.id,'
      '       v.gr_id,'
      '       g.name as gr_name,'
      '       v.cat_id,'
      '       v.cat_name,'
      '       v.subcat_id as subcat_id,'
      
        '       decode(nmcls, null , nvl(sc.name,'#39'���'#39'), '#39#39') as subcat_na' +
        'me,'
      '       v.subsubcat_id,'
      
        '       decode(nmcls, null, nvl(ssc.name, '#39'���'#39'), '#39#39') as subsubca' +
        't_name,'
      '       v.org_group_id,'
      
        '       decode(orgs, null, nvl(og.name, '#39'���'#39'), '#39#39') as org_group_' +
        'name,'
      '       v.orgs,'
      '       v.nmcls'
      '  from ('
      '    select ae.id,'
      '           c.group_id as gr_id,'
      '           ae.cat_id,'
      '           c.name as cat_name,'
      '           nvl(ae.subcat_id,-1) as subcat_id,'
      '           nvl(ae.subsubcat_id,-1) as subsubcat_id,'
      '           nvl(ae.org_group_id,-1) as org_group_id,'
      
        '           (select trim(rpad(csl(o.name),250)) from rtl_assort_e' +
        'xception_orgs ro, organizations o where ro.assort_excep_id = ae.' +
        'id and ro.org_id = o.id) as orgs,'
      
        '           (select trim(rpad(csl(ni.name),250)) from rtl_assort_' +
        'exception_nmcls rn, nomenclature_items ni where rn.assort_excep_' +
        'id = ae.id and rn.nmcl_id = ni.id) as nmcls'
      '    from rtl_assort_exception ae, '
      '         rtl_categories c'
      '    where ae.cat_id = c.id'
      '       ) v,'
      '        rtl_groups g,'
      '        rtl_subsubcategories ssc,'
      '        rtl_subcategories sc,'
      '        org_groups og'
      ' where v.gr_id = g.id'
      '   and ssc.id(+) = v.subsubcat_id'
      '   and sc.id(+) = v.subcat_id'
      '   and og.id(+) = v.org_group_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 'v.ID'
    end
    object m_Q_listGR_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'GR_ID'
      Origin = 'v.gr_id'
    end
    object m_Q_listGR_NAME: TStringField
      DisplayLabel = '������������ ������ ������'
      FieldName = 'GR_NAME'
      Origin = 'g.name'
      Size = 100
    end
    object m_Q_listCAT_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'CAT_ID'
      Origin = 'v.cat_id'
    end
    object m_Q_listCAT_NAME: TStringField
      DisplayLabel = '������������ ��������� ������'
      FieldName = 'CAT_NAME'
      Origin = 'v.cat_name'
      Size = 100
    end
    object m_Q_listSUBCAT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'SUBCAT_ID'
      Origin = 'v.subcat_id'
    end
    object m_Q_listSUBCAT_NAME: TStringField
      DisplayLabel = '������������ ������������'
      DisplayWidth = 60
      FieldName = 'SUBCAT_NAME'
      Origin = 'decode(nmcls, null , nvl(sc.name,'#39'���'#39'), '#39#39')'
      Size = 100
    end
    object m_Q_listSUBSUBCAT_ID: TFloatField
      DisplayLabel = 'ID ���������������'
      FieldName = 'SUBSUBCAT_ID'
      Origin = 'v.subsubcat_id'
    end
    object m_Q_listSUBSUBCAT_NAME: TStringField
      DisplayLabel = '������������ ���������������'
      DisplayWidth = 60
      FieldName = 'SUBSUBCAT_NAME'
      Origin = 'decode(nmcls, null, nvl(ssc.name, '#39'���'#39'), '#39#39')'
      Size = 200
    end
    object m_Q_listORG_GROUP_ID: TFloatField
      DisplayLabel = 'ID �����'
      FieldName = 'ORG_GROUP_ID'
      Origin = 'v.org_group_id'
    end
    object m_Q_listORG_GROUP_NAME: TStringField
      DisplayLabel = '����'
      FieldName = 'ORG_GROUP_NAME'
      Origin = 'decode(orgs, null, nvl(og.name, '#39'���'#39'), '#39#39')'
      Size = 250
    end
    object m_Q_listORGS: TStringField
      DisplayLabel = '���'
      FieldName = 'ORGS'
      Origin = 'v.orgs'
      Size = 250
    end
    object m_Q_listNMCLS: TStringField
      DisplayLabel = 'SKU'
      FieldName = 'NMCLS'
      Origin = 'v.nmcls'
      Size = 250
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'select v.id,'
      '       v.gr_id,'
      '       g.name as gr_name,'
      '       v.cat_id,'
      '       v.cat_name,'
      '       v.subcat_id as subcat_id,'
      
        '       decode(qnt_nmcl, 0, nvl(sc.name,'#39'���'#39'), '#39#39') as subcat_nam' +
        'e,'
      '       v.subsubcat_id,'
      
        '       decode(qnt_nmcl, 0, nvl(ssc.name, '#39'���'#39'), '#39#39') as subsubca' +
        't_name,'
      '       v.org_group_id,'
      
        '       decode(qnt_org, 0, nvl(og.name, '#39'���'#39'), '#39#39') as org_group_' +
        'name'
      '  from ('
      '    select ae.id,'
      '           c.group_id as gr_id,'
      '           ae.cat_id,'
      '           c.name as cat_name,'
      '           nvl(ae.subcat_id,-1) as subcat_id,'
      '           nvl(ae.subsubcat_id,-1) as subsubcat_id,'
      '           nvl(ae.org_group_id,-1) as org_group_id,'
      
        '           (select count(*) from rtl_assort_exception_nmcls n wh' +
        'ere n.assort_excep_id = ae.id) as qnt_nmcl,'
      
        '           (select count(*) from rtl_assort_exception_orgs o whe' +
        're o.assort_excep_id = ae.id) as qnt_org'
      '    from rtl_assort_exception ae, '
      '         rtl_categories c'
      '    where ae.cat_id = c.id'
      '      and ae.id = :ID'
      '       ) v,'
      '        rtl_groups g,'
      '        rtl_subsubcategories ssc,'
      '        rtl_subcategories sc,'
      '        org_groups og'
      ' where v.gr_id = g.id'
      '   and ssc.id(+) = v.subsubcat_id'
      '   and sc.id(+) = v.subcat_id'
      '   and og.id(+) = v.org_group_id')
    Left = 104
    inherited m_Q_itemID: TFloatField
      Origin = 'ID'
      Visible = False
    end
    object m_Q_itemGR_ID: TFloatField
      FieldName = 'GR_ID'
      Origin = 'TSDBMAIN.RTL_GROUPS.ID'
    end
    object m_Q_itemGR_NAME: TStringField
      FieldName = 'GR_NAME'
      Origin = 'TSDBMAIN.RTL_GROUPS.NAME'
      Size = 100
    end
    object m_Q_itemCAT_ID: TFloatField
      FieldName = 'CAT_ID'
      Origin = 'TSDBMAIN.RTL_CATEGORIES.ID'
    end
    object m_Q_itemCAT_NAME: TStringField
      FieldName = 'CAT_NAME'
      Origin = 'TSDBMAIN.RTL_CATEGORIES.NAME'
      Size = 100
    end
    object m_Q_itemSUBCAT_ID: TFloatField
      FieldName = 'SUBCAT_ID'
    end
    object m_Q_itemSUBCAT_NAME: TStringField
      FieldName = 'SUBCAT_NAME'
      Size = 100
    end
    object m_Q_itemSUBSUBCAT_ID: TFloatField
      FieldName = 'SUBSUBCAT_ID'
    end
    object m_Q_itemSUBSUBCAT_NAME: TStringField
      FieldName = 'SUBSUBCAT_NAME'
      Size = 200
    end
    object m_Q_itemORG_GROUP_ID: TFloatField
      FieldName = 'ORG_GROUP_ID'
    end
    object m_Q_itemORG_GROUP_NAME: TStringField
      FieldName = 'ORG_GROUP_NAME'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update retail.rtl_assort_exception'
      'SET '
      '  cat_id = :cat_id,'
      '  subcat_id = NULLIF( :subcat_id , -1),'
      '  subsubcat_id = NULLIF(:subsubcat_id , -1),'
      '  org_group_id = NULLIF(:org_group_id,-1)'
      'where id = :OLD_ID')
    InsertSQL.Strings = (
      
        'insert into retail.rtl_assort_exception(id, cat_id, subcat_id, s' +
        'ubsubcat_id, org_group_id)'
      
        'values (:id, :cat_id, NULLIF(:subcat_id,-1), NULLIF(:subsubcat_i' +
        'd,-1), NULLIF(:org_group_id, -1))')
    DeleteSQL.Strings = (
      'begin'
      
        'delete from retail.rtl_assort_exception_nmcls where assort_excep' +
        '_id = :OLD_ID;'
      
        'delete from retail.rtl_assort_exception_orgs where assort_excep_' +
        'id = :OLD_ID;'
      'delete from retail.rtl_assort_exception where id = :OLD_ID;'
      'end;')
    IgnoreRowsAffected = True
  end
  object m_Q_nmcls: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select c.id,'
      '        c.name as cat_name,'
      '       g.name as gr_name,'
      '       -1 as  SUBCAT_ID, '#39'���'#39' SUBCAT_NAME,'
      '      -1 as  SUBSUBCAT_ID, '#39'���'#39' SUBSUBCAT_NAME'
      'from rtl_categories c, '
      '     rtl_groups g'
      'where c.group_id = g.id'
      
        'and not exists(select 1 from rtl_assort_exception ae  where c.id' +
        ' = ae.cat_id and ae.subcat_id is null and ae.subsubcat_id is nul' +
        'l )'
      '%WHERE_CLAUSE'
      'ORDER BY cat_name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 24
    Top = 184
    object m_Q_nmclsID: TFloatField
      DisplayLabel = 'ID ���������'
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'c.id'
    end
    object m_Q_nmclsCAT_NAME: TStringField
      DisplayLabel = '������������ ��������� ������'
      FieldName = 'CAT_NAME'
      Origin = 'c.name'
      Size = 100
    end
    object m_Q_nmclsGR_NAME: TStringField
      DisplayLabel = '������������ ������ ������'
      FieldName = 'GR_NAME'
      Size = 100
    end
    object m_Q_nmclsSUBCAT_ID: TFloatField
      FieldName = 'SUBCAT_ID'
      Visible = False
    end
    object m_Q_nmclsSUBCAT_NAME: TStringField
      FieldName = 'SUBCAT_NAME'
      Visible = False
      FixedChar = True
      Size = 3
    end
    object m_Q_nmclsSUBSUBCAT_ID: TFloatField
      FieldName = 'SUBSUBCAT_ID'
      Visible = False
    end
    object m_Q_nmclsSUBSUBCAT_NAME: TStringField
      FieldName = 'SUBSUBCAT_NAME'
      Visible = False
      FixedChar = True
      Size = 3
    end
  end
  object m_Q_subcats: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_subcatsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT id, name, cat_id, cat_name, -1 as  SUBSUBCAT_ID, '#39'���'#39' SU' +
        'BSUBCAT_NAME'
      'FROM '
      '('
      '   select sc.id,'
      '        sc.name as name,'
      '        c.id as cat_id,'
      '        c.name as cat_name  '
      '   from rtl_subcategories sc, rtl_categories c'
      '   where '
      
        '   not exists(select 1 from rtl_assort_exception ae  where ae.su' +
        'bcat_id = sc.id and ae.subsubcat_id is null)'
      '   and c.id = sc.cat_id'
      '   and c.id = :CAT_ID'
      '   union all'
      '   SELECT -1 as id, '#39'���'#39' as name, '
      '        null as cat_id,'
      '        null as cat_name  '
      '   FROM dual'
      ')'
      '%WHERE_EXPRESSION '
      'ORDER BY name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 120
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CAT_ID'
        ParamType = ptInput
      end>
    object m_Q_subcatsID: TFloatField
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_subcatsNAME: TStringField
      DisplayLabel = '������������'
      DisplayWidth = 60
      FieldName = 'NAME'
      Origin = 'name'
      Size = 100
    end
    object m_Q_subcatsCAT_ID: TFloatField
      FieldName = 'CAT_ID'
      Visible = False
    end
    object m_Q_subcatsCAT_NAME: TStringField
      DisplayLabel = '���������'
      DisplayWidth = 50
      FieldName = 'CAT_NAME'
      Size = 100
    end
    object m_Q_subcatsSUBSUBCAT_ID: TFloatField
      FieldName = 'SUBSUBCAT_ID'
      Visible = False
    end
    object m_Q_subcatsSUBSUBCAT_NAME: TStringField
      FieldName = 'SUBSUBCAT_NAME'
      Visible = False
      FixedChar = True
      Size = 21
    end
  end
  object m_Q_subsubcats: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_subsubcatsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '
      '  id, name, subcat_id, subcat_name, cat_id, cat_name'
      'FROM '
      '('
      '    select ssc.id,'
      '        ssc.name as name, '
      '        sc.id as subcat_id,'
      '        sc.name as subcat_name,'
      '        c.id as cat_id,'
      '        c.name as cat_name  '
      
        '    from rtl_subsubcategories ssc, rtl_subcategories sc, rtl_cat' +
        'egories c'
      '    where '
      
        '       not exists(select 1 from rtl_assort_exception ae  where a' +
        'e.subsubcat_id = ssc.id)'
      '       and sc.id = ssc.subcat_id'
      '       and c.id = sc.cat_id'
      '       and sc.id = :SUBCAT_ID'
      '    union all'
      '    SELECT -1 as id, '#39'���'#39' as name,'
      '        null as subcat_id,'
      '        null as subcat_name,'
      '        null as cat_id,'
      '        null as cat_name  '
      '    FROM dual'
      ')'
      '%WHERE_EXPRESSION '
      'ORDER BY name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 240
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SUBCAT_ID'
        ParamType = ptInput
      end>
    object m_Q_subsubcatsID: TFloatField
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_subsubcatsNAME: TStringField
      DisplayLabel = '���������������'
      DisplayWidth = 80
      FieldName = 'NAME'
      Origin = 'name'
      Size = 200
    end
    object m_Q_subsubcatsSUBCAT_ID: TFloatField
      FieldName = 'SUBCAT_ID'
      Visible = False
    end
    object m_Q_subsubcatsSUBCAT_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'SUBCAT_NAME'
      Size = 100
    end
    object m_Q_subsubcatsCAT_ID: TFloatField
      FieldName = 'CAT_ID'
      Visible = False
    end
    object m_Q_subsubcatsCAT_NAME: TStringField
      FieldName = 'CAT_NAME'
      Visible = False
      Size = 100
    end
  end
  object m_Q_sku: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_skuBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT assort_excep_id, nmcl_id,trim(rpad(ni.name, 250)) as nmcl' +
        '_name'
      '  FROM rtl_assort_exception_nmcls rn,'
      '             nomenclature_items ni'
      ' WHERE rn.nmcl_id = ni.id'
      '       AND rn.assort_excep_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 128
    Top = 248
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_skuNMCL_ID: TFloatField
      DisplayLabel = 'ID SKU'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_skuNMCL_NAME: TStringField
      DisplayLabel = '������������ SKU'
      FieldName = 'NMCL_NAME'
      Origin = 'trim(rpad(ni.name, 250))'
      Size = 250
    end
  end
  object m_Q_orgs: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_orgsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT assort_excep_id, org_id, trim(rpad(o.name, 250)) as org_n' +
        'ame'
      '  FROM rtl_assort_exception_orgs ro,'
      '       organizations o'
      '  WHERE ro.org_id = o.id'
      '    AND ro.assort_excep_id = :ID'
      '%WHERE_CLAUSE'
      'ORDER BY o.name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 208
    Top = 248
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_orgsORG_ID: TFloatField
      DisplayLabel = 'ID ���'
      FieldName = 'ORG_ID'
      Origin = 'org_id'
    end
    object m_Q_orgsORG_NAME: TStringField
      DisplayLabel = '������������ ���'
      FieldName = 'ORG_NAME'
      Origin = 'trim(rpad(o.name, 250))'
      Size = 250
    end
  end
  object m_Q_org_groups: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select * from ('
      'SELECT g.id,'
      '             g.name'
      '  FROM org_groups g'
      'WHERE g.type_id = 10'
      ' union all'
      '   SELECT -1 as id, '#39'���'#39' as name '
      '   FROM dual'
      ')'
      '%WHERE_EXPRESSION'
      'order by name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 344
    Top = 190
    object m_Q_org_groupsID: TFloatField
      DisplayLabel = 'ID �����'
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_org_groupsNAME: TStringField
      DisplayLabel = '����'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
  end
  object m_Q_delete_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'BEGIN'
      '  DELETE FROM  rtl_assort_exception_orgs r'
      '  WHERE r.assort_excep_id = :ID; '
      'END;')
    Left = 296
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
  end
end
