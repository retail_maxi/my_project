//---------------------------------------------------------------------------

#ifndef FmZValidationH
#define FmZValidationH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFmOperation.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Db.hpp>
#include <Grids.hpp>
#include "DmZValidation.h"
//---------------------------------------------------------------------------
class TFZValidation : public TTSFOperation
{
__published:	// IDE-managed Components
   TPanel *m_P_params;
   TMLDBGrid *m_DBG_main;
   TDataSource *m_DS_main;
   TGroupBox *m_G_taxpayer;
   TPanel *m_P_prm_ctrl;
   TPanel *m_P_tax_ctrl;
   TMLDBGrid *m_DBG_tax;
   TSpeedButton *m_SBTN_period;
   TBitBtn *m_BBTN_refresh;
   TDataSource *m_DS_tax;
   TMLSetBeginEndDate *m_ACT_set_begin_end_date;
   TSplitter *m_S_prm_main;
   TAction *m_ACT_fill_tax;
   TAction *m_ACT_clear_tax;
   TToolBar *m_TB_tax;
   TBitBtn *m_BBTN_fill_tax;
   TBitBtn *m_BBTN_clear_tax;
   TCheckBox *m_CB_act_tax;
   TAction *m_ACT_set_period;
   void __fastcall FormShow(TObject *Sender);
   void __fastcall m_ACT_fill_taxExecute(TObject *Sender);
   void __fastcall m_ACT_clear_taxExecute(TObject *Sender);
   void __fastcall m_CB_act_taxClick(TObject *Sender);
   void __fastcall m_ACT_set_periodExecute(TObject *Sender);
private:	// User declarations
   TDZValidation *m_dm;
public:		// User declarations
   __fastcall TFZValidation(TComponent *p_owner, TTSDOperation *p_dm_operation)
           : TTSFOperation(p_owner, p_dm_operation),
            m_dm(static_cast<TDZValidation*>(DMOperation)) {};
     void __fastcall SetPeriodCaption(TDateTime p_begin_date, TDateTime p_end_date);

};
//---------------------------------------------------------------------------
#endif
