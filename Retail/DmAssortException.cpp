//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop         

#include "DmAssortException.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
__fastcall TDAssortException::TDAssortException(TComponent* p_owner, AnsiString p_prog_id):
    TTSDRefbook(p_owner, p_prog_id)
{
}
//---------------------------------------------------------------------------

void __fastcall TDAssortException::m_Q_orgsBeforeOpen(TDataSet *DataSet)
{
   m_Q_orgs->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDAssortException::m_Q_skuBeforeOpen(TDataSet *DataSet)
{
   m_Q_sku->ParamByName("ID")->AsInteger = m_Q_itemID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDAssortException::m_Q_itemAfterInsert(TDataSet *DataSet)
{
   TTSDRefbook::m_Q_itemAfterInsert(DataSet);
   m_Q_itemORG_GROUP_ID->AsInteger = -1;
   m_Q_itemORG_GROUP_NAME->AsString = "���";
}
//---------------------------------------------------------------------------

void __fastcall TDAssortException::m_Q_subcatsBeforeOpen(TDataSet *DataSet)
{
   m_Q_subcats->ParamByName("CAT_ID")->AsInteger = m_Q_itemCAT_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDAssortException::m_Q_subsubcatsBeforeOpen(
      TDataSet *DataSet)
{
   m_Q_subsubcats->ParamByName("SUBCAT_ID")->AsInteger = m_Q_itemSUBCAT_ID->AsInteger;
}
//---------------------------------------------------------------------------

