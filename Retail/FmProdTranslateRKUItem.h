//---------------------------------------------------------------------------

#ifndef FmProdTranslateRKUItemH
#define FmProdTranslateRKUItemH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFMREFBOOKITEM.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DmProdTranslateRKU.h"
#include <Menus.hpp>
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include "MLLovView.h"
#include "MLLov.h"
#include "RxLookup.hpp"
//---------------------------------------------------------------------------

class TFProdTranslateRKUItem : public TTSFRefbookItem
{
__published:
  TMLLovListView *m_LV_org;
  TLabel *Label2;
  TDataSource *m_DS_org;
  TMLLov *m_LOV_org;
  TMLLovListView *m_LV_nmcl;
  TMLLovListView *m_LV_assort;
  TMLLovListView *m_LV_gr_dep;
  TMLLovListView *m_LV_nmcl_to;
  TMLLovListView *m_LV_assort_to;
  TDBEdit *m_DBE_coef;
  TLabel *Label1;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TDataSource *m_DS_nmcl;
  TMLLov *m_LOV_nmcl;
  TDataSource *m_DS_assort;
  TMLLov *m_LOV_assort;
  TDataSource *m_DS_gr_dep;
  TMLLov *m_LOV_gr_dep;
  TDataSource *m_DS_nmcl_to;
  TMLLov *m_LOV_nmcl_to;
  TDataSource *m_DS_assort_to;
  TMLLov *m_LOV_assort_to;
  void __fastcall m_LOV_orgAfterApply(TObject *Sender);
  void __fastcall m_LOV_nmclAfterApply(TObject *Sender);
  void __fastcall m_LOV_nmcl_toAfterApply(TObject *Sender);
protected:
  virtual AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
private:
  TDProdTranslateRKU *m_dm;
public:
  __fastcall TFProdTranslateRKUItem(TComponent* p_owner,
                                TTSDRefbook *p_dm_refbook): TTSFRefbookItem(p_owner,
                                                                            p_dm_refbook),
                                  m_dm(static_cast<TDProdTranslateRKU*>(DMRefbook)) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
