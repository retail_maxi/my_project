//---------------------------------------------------------------------------

#ifndef FmInvSheetAlcoH
#define FmInvSheetAlcoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <Db.hpp>
#include "MLQuery.h"
#include "RxQuery.hpp"
#include <DBTables.hpp>
#include "DmInvSheet.h"
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
#include <Mask.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include "TSFMDOCUMENTWITHLINES.h"
//---------------------------------------------------------------------------
class TFInvSheetAlco : public TTSFDialog
{
__published:	// IDE-managed Components
        TDataSource *m_DS_alccode;
        TMLLov *m_LOV_alccode;
        TDataSource *m_DS_alco;
        TPanel *m_panel_SN;
        TDataSource *m_DS_fsm;
        TLabel *d2;
        TLabel *d1;
        TLabel *d3;
        TEdit *edt_rank;
        TEdit *edt_num;
        TPanel *m_panel_alc;
        TLabel *d5;
        TLabel *d6;
        TDBEdit *m_DBE_alccode;
        TMLLovListView *m_LV_alccode;
        TDBEdit *m_DBE_qnt;
        TLabel *d4;
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall mACT_apply_updatesClick(TObject *Sender);
        void __fastcall m_ACT_apply_updatesExecuteAll(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall edt_rankKeyPress(TObject *Sender, char &Key);
        void __fastcall edt_numKeyPress(TObject *Sender, char &Key);
        void __fastcall m_ACT_closeExecute(TObject *Sender);
private:	// User declarations
  TDInvSheet *m_dm;
  
public:		// User declarations
        __fastcall TFInvSheetAlco(TComponent* p_owner,
                                  TTSDDocumentWithLines *p_dm);
};
//---------------------------------------------------------------------------
bool ExecInvSheetAlcoDlg(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
//---------------------------------------------------------------------------
#endif
