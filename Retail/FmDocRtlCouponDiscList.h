//---------------------------------------------------------------------------

#ifndef FmDocRtlCouponDiscListH
#define FmDocRtlCouponDiscListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmDocRtlCouponDisc.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTLIST.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include "DLBCScaner.h"
#include <Dialogs.hpp>
#include "TSAccessControl.h"
//---------------------------------------------------------------------------
class TFDocRtlCouponDiscList : public TTSFDocumentList
{
__published:
  TDataSource *m_DS_nmcls;
  TToolButton *ToolButton1;
  TDataSource *m_DS_orgs;
  TDataSource *m_DS_fict_org;
  TAction *m_ACT_goto_doc;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TToolButton *ToolButton2;
  TSpeedButton *m_SBTN_set_date;
  TToolButton *ToolButton3;
  TToolButton *ToolButton4;
  TAction *m_ACT_nmcl_filter;
  TToolButton *m_TBTN_nmcl_filter;
  TAction *m_ACT_select_all;
  TAction *m_ACT_check_arrival;
  TToolBar *ToolBar1;
  TAction *m_ACT_print_label;
        TSpeedButton *SpeedButton2;
        TAction *m_ACT_end_date;
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_editUpdate(TObject *Sender);
        void __fastcall m_ACT_deleteUpdate(TObject *Sender);
        void __fastcall m_ACT_end_dateUpdate(TObject *Sender);
        void __fastcall m_ACT_end_dateExecute(TObject *Sender);
private:
  TDDocRtlCouponDisc* m_dm;
  bool m_can_change_date;
public:
  __fastcall TFDocRtlCouponDiscList(TComponent* p_owner, TTSDDocument *p_dm)
    : TTSFDocumentList(p_owner,p_dm),
    m_dm(static_cast<TDDocRtlCouponDisc*>(p_dm))
    {
    m_can_change_date = TTSOperationAccess(m_dm->DRight->GetMaskOperRight(StrToInt64(m_dm->GetConstValue("OPER_RTL_COUPON_DIST_CHG_END_DAT")),TTSOperationAccess().AccessMask)).CanRun;
    };
};
//---------------------------------------------------------------------------
#endif
