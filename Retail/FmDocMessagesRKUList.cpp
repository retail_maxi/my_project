//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocMessagesRKUList.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFDocMessagesRKUList::TFDocMessagesRKUList(TComponent *p_owner)
    : TTSFDialog(p_owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFDocMessagesRKUList::m_ACT_clr_allExecute(TObject *Sender)
{
    for (int i = 0; i < m_CLB_rku_list->Items->Count; i++)
        m_CLB_rku_list->Checked[i] = false;
}
//---------------------------------------------------------------------------

void __fastcall TFDocMessagesRKUList::m_ACT_sel_allExecute(TObject *Sender)
{
    for (int i = 0; i < m_CLB_rku_list->Items->Count; i++)
        m_CLB_rku_list->Checked[i] = true;
}
//---------------------------------------------------------------------------

