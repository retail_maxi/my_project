//---------------------------------------------------------------------------

#ifndef IncomeImplH
#define IncomeImplH
//---------------------------------------------------------------------------

#include "TSModuleTemplate.h"
#include "DmIncome.h"
#include "FmIncomeList.h"
#include "FmIncomeItem.h"
#include "TSRetail_TLB.h"
//---------------------------------------------------------------------------

#undef TS_ADDITION_INTERFACE_DECLARE
#define TS_ADDITION_INTERFACE_DECLARE TS_DOCUMENT_INTERFACE_DECLARE
#undef TS_ADDITION_INTERFACE_ENTRY
#define TS_ADDITION_INTERFACE_ENTRY TS_DOCUMENT_INTERFACE_ENTRY
#undef TS_ADDITION_INTERFACE_IMPL
#define TS_ADDITION_INTERFACE_IMPL TS_DOCUMENT_INTERFACE_IMPL

TS_FORM_LIST_COCLASS_IMPL(Income)
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
