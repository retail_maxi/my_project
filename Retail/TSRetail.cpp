//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <atl\atlvcl.h>

#include "IncomeImpl.h"
#include "InvSheetImpl.h"
#include "InvActImpl.h"
#include "RtlZReportsImpl.h"
#include "RtlSalesImpl.h"
#include "RtlReturnsImpl.h"
#include "BillImpl.h"
#include "TTNOutImpl.h"
#include "CompetitorsRepriceImpl.h"
#include "FindKassImpl.h"
#include "MessageToRKUImpl.h"
#include "MessageFromRKUImpl.h"
#include "MessagesRKUImpl.h"
#include "ProdTranslateRKUImpl.h"
#include "BuhZreportsImpl.h"
#include "ZValidationImpl.h"
#include "NoCashSalesImpl.h"
#include "CompetitorsRepriceSummImpl.h"
#include "ZreportsUnloadingImpl.h"
#include "DocRtlCouponDiscImpl.h"
#include "AssortExceptionImpl.h"
#include "UnprocessDocumentImpl.h"
#include "AlcoJournalImpl.h"
#include "AlcoJournalNewImpl.h"
#include "DocDiscGoodsImpl.h"
#include "RtlPlanMarginSeedImpl.h"
#include "OperRtlSalesImpl.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

USEUNIT("TSRetail_ATL.cpp");
USERES("TSRetail.res");
USETLB("TSRetail.tlb");
USEUNIT("TSRetail_TLB.cpp");
USEFORM("DmIncome.cpp", DIncome);
USEFORM("FmIncomeList.cpp", FIncomeList);
USEFORM("FmIncomeItem.cpp", FIncomeItem);
USEFORM("FmIncomeLine.cpp", FIncomeLine);
USEUNIT("IncomeImpl.cpp");
USEFORM("..\Common\FrmRtlIncomeLine.cpp", FRtlIncomeLine);
USEFORM("..\Common\FrmShelfLifeLists.cpp", FRShelfLifeLists); /* TFrame: File Type */
USEUNIT("..\Common\BarCode.cpp");
USEFORM("FmGetBuhDoc.cpp", FGetBuhDoc);
USEFORM("FmActTCDSet.cpp", FActTCDSet);
USEFORM("DmInvSheet.cpp", DInvSheet);
USEFORM("FmInvSheetList.cpp", FInvSheetList);
USEFORM("FmInvSheetItem.cpp", FInvSheetItem);
USEFORM("FmInvSheetLine.cpp", FInvSheetLine);
USEUNIT("InvSheetImpl.cpp");
USEFORM("DmInvAct.cpp", DInvAct);
USEFORM("FmInvActList.cpp", FInvActList);
USEFORM("FmInvActItem.cpp", FInvActItem);
USEFORM("FmInvActLine.cpp", FInvActLine);
USEUNIT("InvActImpl.cpp");
USEFORM("FmForGroups.cpp", FForGroups);
USEFORM("FmNmcls.cpp", FNmcls);
USEFORM("DmRtlZReports.cpp", DRtlZReports);
USEFORM("DmRtlSales.cpp", DRtlSales);
USEFORM("DmRtlReturns.cpp", DRtlReturns);
USEFORM("FmRtlZReportsList.cpp", FRtlZReportsList);
USEFORM("FmRtlSalesList.cpp", FRtlSalesList);
USEFORM("FmRtlReturnsList.cpp", FRtlReturnsList);
USEFORM("FmRtlZReportsItem.cpp", FRtlZReportsItem);
USEFORM("FmRtlSalesItem.cpp", FRtlSalesItem);
USEFORM("FmRtlReturnsItem.cpp", FRtlReturnsItem);
USEUNIT("RtlZReportsImpl.cpp");
USEUNIT("RtlSalesImpl.cpp");
USEUNIT("RtlReturnsImpl.cpp");
USEFORM("FmRtlReturnLine.cpp", FRtlReturnLine);
USEFORM("DmBill.cpp", DBill); /* TDataModule: File Type */
USEFORM("FmBillItem.cpp", FBillItem);
USEFORM("FmBillList.cpp", FBillList);
USEFORM("FmBillLine.cpp", FBillLine);
USEUNIT("BillImpl.cpp");
USEFORM("DmTTNOut.cpp", DTTNOut); /* TDataModule: File Type */
USEFORM("FmTTNOutList.cpp", FTTNOutList);
USEFORM("FmTTNOutItem.cpp", FTTNOutItem);
USEFORM("FmTTNOutLine.cpp", FTTNOutLine);
USEUNIT("TTNOutImpl.cpp");
USEFORM("..\Common\FmRtlCardOut.cpp", FRtlCardOut);
USEFORM("..\Common\FmRtlCardOutCnt.cpp", FRtlCardOutCnt);
USEUNIT("L:\TSBF\TSBF.cpp");
USEFORM("..\Waybills\FmViewBuhDoc.cpp", FViewBuhDoc);
USEFORM("DmCompetitorsReprice.cpp", DCompetitorsReprice);
USEFORM("FmCompetitorsRepriceList.cpp", FCompetitorsRepriceList);
USEFORM("FmCompetitorsRepriceItem.cpp", FCompetitorsRepriceItem);
USEFORM("FmCompetitorsRepriceLine.cpp", FCompetitorsRepriceLine);
USEUNIT("CompetitorsRepriceImpl.cpp");
USEFORM("FmSetCnt.cpp", FSetCnt);
USEUNIT("FindKassImpl.cpp");
USEFORM("DmFindKass.cpp", DFindKass);
USEFORM("FmFindKass.cpp", FFindKass);
USEFORM("FmAccountSet.cpp", FAccountSet);
USEFORM("DmMessageToRKU.cpp", DMessageToRKU); /* TDataModule: File Type */
USEFORM("FmMessageToRKUList.cpp", FMessageToRKUList);
USEFORM("FmMessageToRKUItem.cpp", FMessageToRKUItem);
USEUNIT("MessageToRKUImpl.cpp");
USEFORM("DmMessageFromRKU.cpp", DMessageFromRKU); /* TDataModule: File Type */
USEFORM("FmMessageFromRKUList.cpp", FMessageFromRKUList);
USEFORM("FmMessageFromRKUItem.cpp", FMessageFromRKUItem);
USEUNIT("MessageFromRKUImpl.cpp");
USEFORM("DmMessagesRKU.cpp", DMessagesRKU); /* TDataModule: File Type */
USEFORM("FmMessagesRKU.cpp", FMessagesRKU);
USEUNIT("MessagesRKUImpl.cpp");
USEFORM("FmDocMessagesRKUList.cpp", FDocMessagesRKUList);
USEFORM("FmNmclAddressList.cpp", FNmclAddressList);
USEFORM("FmShowAddressList.cpp", Form3);
USEFORM("FmGetShetDoc.cpp", FGetSchetDoc);
USEFORM("..\Retail3\FmPutInv.cpp", FPutInv);
USEFORM("DmProdTranslateRKU.cpp", DProdTranslateRKU); /* TDataModule: File Type */
USEFORM("FmProdTranslateRKUList.cpp", FProdTranslateRKUList);
USEFORM("FmProdTranslateRKUItem.cpp", FProdTranslateRKUItem);
USEUNIT("ProdTranslateRKUImpl.cpp");
USEFORM("FmBuhZreportsList.cpp", FBuhZreportsList);
USEUNIT("BuhZreportsImpl.cpp");
USEFORM("DmBuhZreports.cpp", DBuhZreports); /* TDataModule: File Type */
USEFORM("FmBuhZreportsItem.cpp", FBuhZreportsItem);
USEFORM("DmZValidation.cpp", DZValidation); /* TDataModule: File Type */
USEUNIT("ZValidationImpl.cpp");
USEFORM("FmZValidation.cpp", FZValidation);
USEFORM("DmNoCashSales.cpp", DNoCashSales); /* TDataModule: File Type */
USEFORM("FmNoCashSales.cpp", FNoCashSales);
USEUNIT("NoCashSalesImpl.cpp");
USEUNIT("..\Common\XLSXConverter.cpp");
USEFORM("..\Common\FmSetDateTimePeriod.cpp", FSetDateTimePeriod);
USEFORM("FmCompetitorsRepriceSummItem.cpp", FCompetitorsRepriceSummItem);
USEFORM("FmCompetitorsRepriceSummLine.cpp", FCompetitorsRepriceSummLine);
USEFORM("FmCompetitorsRepriceSummList.cpp", FCompetitorsRepriceSummList);
USEUNIT("CompetitorsRepriceSummImpl.cpp");
USEFORM("DmCompetitorsRepriceSumm.cpp", DCompetitorsRepriceSumm); /* TDataModule: File Type */
USEFORM("FmCompRepriceSummOsv.cpp", FCompRepriceSummOsv);
USEFORM("DmZreportsUnloading.cpp", DZreportsUnloading); /* TDataModule: File Type */
USEFORM("FmZreportsUnloading.cpp", FZreportsUnloading);
USEUNIT("ZreportsUnloadingImpl.cpp");
USEFORM("FmRtlSalesPeriod.cpp", FRtlSalesPeriod);
USEFORM("FmDocRtlCouponDiscList.cpp", FDocRtlCouponDiscList);
USEFORM("DmDocRtlCouponDisc.cpp", DDocRtlCouponDisc); /* TDataModule: File Type */
USEUNIT("DocRtlCouponDiscImpl.cpp");
USEFORM("FmDocRtlCouponDiscItem.cpp", FDocRtlCouponDiscItem);
USEFORM("FmDocRtlCouponDiscLine.cpp", FDocRtlCouponDiscLine);
USEFORM("FmTotalAmt.cpp", FmTotalAmt);
USEFORM("FmCheckSum.cpp", FmCheckSum);
USEFORM("DmAssortException.cpp", DAssortException); /* TDataModule: File Type */
USEFORM("FmAssortExceptionItem.cpp", FAssortExceptionItem);
USEFORM("FmAssortExceptionList.cpp", FAssortExceptionList);
USEUNIT("AssortExceptionImpl.cpp");
USEFORM("DmUnprocessDocument.cpp", DUnprocessDocument); /* TDataModule: File Type */
USEFORM("FmUnprocessDocument.cpp", FUnprocessDocument);
USEUNIT("UnprocessDocumentImpl.cpp");
USEUNIT("AlcoJournalImpl.cpp");
USEFORM("DmAlcoJournal.cpp", DAlcoJournal); /* TDataModule: File Type */
USEFORM("FmAlcoJournal.cpp", FAlcoJournal);
USEFORM("FmSelectOrg.cpp", FSelectOrg);
USEUNIT("AlcoJournalNewImpl.cpp");
USEFORM("DmAlcoJournalNew.cpp", DAlcoJournalNew); /* TDataModule: File Type */
USEFORM("FmAlcoJournalNew.cpp", FAlcoJournalNew);
USELIB("C:\Program Files\Borland\CBuilder5\Projects\Lib\TSBF.lib");
USEFORM("FmReplaceDocNum.cpp", FReplaseDocNum);
USEFORM("FmReplaceReason.cpp", FReplaseReason);
USEFORM("FmRtlReturnsRecond.cpp", FRtlReturnsRecond);
USEFORM("FmDocRtlCouponDiscOrgs.cpp", FDocRtlCouponDiscOrgs);
USEFORM("DmDocDiscGoods.cpp", DDocDiscGoods); /* TDataModule: File Type */
USEUNIT("DocDiscGoodsImpl.cpp");
USEFORM("FmDocDiscGoodsItem.cpp", FDocDiscGoodsItem);
USEFORM("FmDocDiscGoodsLine.cpp", FDocDiscGoodsLine);
USEFORM("FmDocDiscGoodsList.cpp", FDocDiscGoodsList);
USEFORM("FmDocDiscGoodsUpdSel.cpp", FDocDiscGoodsUpdSel);
USEFORM("FmInvActAlco.cpp", FInvActAlco);
USEFORM("FmInvSheetAlco.cpp", FInvSheetAlco);
USEUNIT("..\Common\DragNDropFiles.cpp");
USEUNIT("..\Common\DragFiles.cpp");
USEFORM("..\Common\FrmAttachContrs.cpp", FrAttachContrs); /* TFrame: File Type */
USEFORM("..\Common\FrmAttachFileTS.cpp", FrAttachFileTS); /* TFrame: File Type */
USEFORM("DmRtlPlanMarginSeed.cpp", DRtlPlanMarginSeed); /* TDataModule: File Type */
USEFORM("FmRtlPlanMarginSeedItem.cpp", FRtlPlanMarginSeedItem);
USEFORM("FmRtlPlanMarginSeedList.cpp", FRtlPlanMarginSeedList);
USEUNIT("RtlPlanMarginSeedImpl.cpp");
USEFORM("FmRTLAssortExcepNmcls.cpp", FRTLAssortExcepNmcls);
USEFORM("FmRTLAssortExcepOrgs.cpp", FRTLAssortExcepOrgs);
USEFORM("DmOperRtlSales.cpp", DOperRtlSales); /* TDataModule: File Type */
USEFORM("FmOperRtlSales.cpp", FOperRtlSales);
USEUNIT("OperRtlSalesImpl.cpp");
USEFORM("FmOperRtlSalesPeriod.cpp", FOperRtlSalesPeriod);
USEFORM("FmOperRtlSalesItem.cpp", FOperRtlSalesItem);
//---------------------------------------------------------------------------
TComModule  ProjectModule;
TComModule &_Module = ProjectModule;
//---------------------------------------------------------------------------

BEGIN_OBJECT_MAP(ObjectMap)
  OBJECT_ENTRY(CLSID_Income, TIncomeImpl)
  OBJECT_ENTRY(CLSID_InvSheet, TInvSheetImpl)
  OBJECT_ENTRY(CLSID_InvAct, TInvActImpl)
  OBJECT_ENTRY(CLSID_RtlZReports, TRtlZReportsImpl)
  OBJECT_ENTRY(CLSID_RtlSales, TRtlSalesImpl)
  OBJECT_ENTRY(CLSID_RtlReturns, TRtlReturnsImpl)
  OBJECT_ENTRY(CLSID_Bill, TBillImpl)
  OBJECT_ENTRY(CLSID_TTNOut, TTTNOutImpl)
  OBJECT_ENTRY(CLSID_CompetitorsReprice, TCompetitorsRepriceImpl)
  OBJECT_ENTRY(CLSID_FindKass, TFindKassImpl)
  OBJECT_ENTRY(CLSID_MessageToRKU, TMessageToRKUImpl)
  OBJECT_ENTRY(CLSID_MessageFromRKU, TMessageFromRKUImpl)
  OBJECT_ENTRY(CLSID_MessagesRKU, TMessagesRKUImpl)
  OBJECT_ENTRY(CLSID_ProdTranslateRKU, TProdTranslateRKUImpl)
  OBJECT_ENTRY(CLSID_BuhZreports, TBuhZreportsImpl)
  OBJECT_ENTRY(CLSID_ZValidation, TZValidationImpl)
  OBJECT_ENTRY(CLSID_NoCashSales, TNoCashSalesImpl)
  OBJECT_ENTRY(CLSID_CompetitorsRepriceSumm, TCompetitorsRepriceSummImpl)
  OBJECT_ENTRY(CLSID_ZreportsUnloading, TZreportsUnloadingImpl)
  OBJECT_ENTRY(CLSID_DocRtlCouponDisc, TDocRtlCouponDiscImpl)
  OBJECT_ENTRY(CLSID_AssortException, TAssortExceptionImpl)
  OBJECT_ENTRY(CLSID_UnprocessDocument, TUnprocessDocumentImpl)
  OBJECT_ENTRY(CLSID_AlcoJournal, TAlcoJournalImpl)
  OBJECT_ENTRY(CLSID_AlcoJournalNew, TAlcoJournalNewImpl)
  OBJECT_ENTRY(CLSID_DocDiscGoods, TDocDiscGoodsImpl)
  OBJECT_ENTRY(CLSID_RtlPlanMarginSeed, TRtlPlanMarginSeedImpl)
  OBJECT_ENTRY(CLSID_OperRtlSales, TOperRtlSalesImpl)
END_OBJECT_MAP()
//---------------------------------------------------------------------------

int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*)
{
    if (reason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hinst);
        DisableThreadLibraryCalls(hinst);
    }
    return TRUE;
}
//---------------------------------------------------------------------------

void ModuleTerm(void)
{
    _Module.Term();
}
#pragma exit ModuleTerm 63
//---------------------------------------------------------------------------

STDAPI __export DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}
//---------------------------------------------------------------------------

STDAPI __export DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}
//---------------------------------------------------------------------------

STDAPI __export DllRegisterServer(void)
{
    return _Module.RegisterServer(TRUE);
}
//---------------------------------------------------------------------------

STDAPI __export DllUnregisterServer(void)
{
    return _Module.UnregisterServer();
}
//---------------------------------------------------------------------------
