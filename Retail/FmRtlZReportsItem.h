//---------------------------------------------------------------------------

#ifndef FmRtlZReportsItemH
#define FmRtlZReportsItemH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENT.h"
#include "DmRtlZReports.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFRtlZReportsItem : public TTSFDocument
{
__published:
   TPanel *m_P_header;
   TPanel *m_P_oper;
   TPanel *m_P_sprm;
   TSplitter *m_S_oper_sprm;
   TMLDBGrid *m_DBG_oper;
   TPanel *m_P_oper_header;
   TLabel *m_L_oper_header;
   TDataSource *m_DS_oper;
   TDataSource *m_DS_sprm;
   TLabel *m_L_num_fr;
   TLabel *m_L_num_rep;
   TLabel *m_L_taxpayer;
   TLabel *m_L_print_date;
   TLabel *m_L_rep_sum;
   TLabel *m_L_sale_sum;
   TLabel *m_L_tax10;
   TLabel *m_L_tax18;
   TMLDBPanel *m_DBE_FISCAL_NUM;
   TMLDBPanel *m_DBE_REPORT_NUM;
   TMLDBPanel *m_DBE_TAXPAYER_NAME;
   TMLDBPanel *m_DBE_PRINT_DATE;
   TMLDBPanel *m_DBE_REPORT_SUM;
   TMLDBPanel *m_DBE_SALES_SUM;
   TLabel *m_L_sprm_sum;
   TMLDBPanel *m_DBE_SRPM_SUM;
   TMLDBPanel *m_DBP_REPORT_SUM;
   TMLDBPanel *m_DBP_SRPM_SUM;
    TMLDBPanel *m_DBP_stock_name;
    TLabel *m_L_stock_name;
        TLabel *Label1;
        TMLDBPanel *m_DBE_BONUS_SUM;
        TPageControl *m_PC_spec;
        TTabSheet *m_TS_srpm;
        TTabSheet *m_TS_ret;
        TMLDBGrid *m_DBG_srpm;
        TMLDBGrid *m_DBG_ret;
        TDataSource *m_DS_ret;
        TMLDBPanel *m_DBP_RET_SUM;
        TLabel *m_L_ret_sum;
   void __fastcall FormShow(TObject *Sender);
private:
  TDRtlZReports *m_dm;
public:
  __fastcall TFRtlZReportsItem(TComponent *p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

