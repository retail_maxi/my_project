//---------------------------------------------------------------------------

#ifndef DmMessageFromRKUH
#define DmMessageFromRKUH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMDocument.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENT.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDMessageFromRKU : public TTSDDocument
{
__published:	// IDE-managed Components
    TFloatField *m_Q_listDOC_NUMBER;
    TStringField *m_Q_listMESSAGE_TYPE;
    TStringField *m_Q_listHOST;
    TFloatField *m_Q_listRKU;
    TDateTimeField *m_Q_listREAD_DATE;
    TFloatField *m_Q_listREAD_USER_ID;
    TStringField *m_Q_listREAD_USER_NAME;
    TFloatField *m_Q_itemDOC_NUMBER;
    TFloatField *m_Q_itemTYPE_MESSAGE_ID;
    TStringField *m_Q_itemMESSAGE_TYPE;
    TStringField *m_Q_itemHOST;
    TFloatField *m_Q_itemRKU;
    TDateTimeField *m_Q_itemREAD_DATE;
    TFloatField *m_Q_itemREAD_USER_ID;
    TStringField *m_Q_itemREAD_USER_NAME;
    TQuery *m_Q_get_cnt_id;
    TFloatField *m_Q_get_cnt_idCNT_ID;
    TDateTimeField *m_Q_itemDOC_DATE;
    TMLQuery *m_Q_type_mess_list;
    TFloatField *m_Q_type_mess_listID;
    TStringField *m_Q_type_mess_listNAME;
    TMLQuery *m_Q_user_list;
    TFloatField *m_Q_user_listCNT_ID;
    TStringField *m_Q_user_listCNT_NAME;
    TStringField *m_Q_itemNOTE;
    void __fastcall m_Q_itemBeforeClose(TDataSet *DataSet);
    void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
private:	// User declarations
    int __fastcall GetCntId();
public:		// User declarations
    __fastcall TDMessageFromRKU(TComponent* p_owner, AnsiString p_prog_id);
};
//---------------------------------------------------------------------------
#endif
