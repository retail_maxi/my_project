//---------------------------------------------------------------------------

#ifndef FmInvActListH
#define FmInvActListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmInvAct.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTLIST.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include <DBCtrls.hpp>
//---------------------------------------------------------------------------
class TFInvActList : public TTSFDocumentList
{
__published:
  TToolButton *ToolButton1;
  TSpeedButton *m_SBTN_set_date;
  TMLSetBeginEndDate *m_ACT_set_begin_end_date;
  TAction *m_ACT_create_act_for_group;
  TSpeedButton *m_SBTN_create_for_group;
        TDBLookupComboBox *m_DBLCB_organizations;
        TToolButton *ToolButton2;
        TDataSource *m_DS_orgs;
  void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_ACT_create_act_for_groupUpdate(TObject *Sender);
  void __fastcall m_ACT_create_act_for_groupExecute(TObject *Sender);
  void __fastcall m_DBG_listGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
        void __fastcall m_DBLCB_organizationsClick(TObject *Sender);
private:
  TDInvAct* m_dm;
public:
  __fastcall TFInvActList(TComponent* p_owner, TTSDDocument *p_dm);
};
//---------------------------------------------------------------------------
#endif
