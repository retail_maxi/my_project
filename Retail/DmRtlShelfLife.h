//---------------------------------------------------------------------------

#ifndef DmRtlShelfLifeH
#define DmRtlShelfLifeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMREFBOOK.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDRtlShelfLife : public TTSDRefbook
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
        __fastcall TDRtlShelfLife(TComponent* p_owner,
                                    AnsiString p_prog_id);
};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
