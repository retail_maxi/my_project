//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmMessageToRKUItem.h"
#include "FmDocMessagesRKUList.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFMessageToRKUItem::TFMessageToRKUItem(TComponent* p_owner,
                                                  TTSDDocument *p_dm_document)
    : TTSFDocument(p_owner, p_dm_document),
    m_dm(dynamic_cast<TDMessageToRKU*>(p_dm_document))
{
}
//---------------------------------------------------------------------------

void __fastcall TFMessageToRKUItem::m_ACT_show_RKUUpdate(TObject *Sender)
{
    m_ACT_show_RKU->Enabled = m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFMessageToRKUItem::m_ACT_show_RKUExecute(TObject *Sender)
{
    m_dm->ApplyUpdatesItem();
    std::auto_ptr<TFDocMessagesRKUList> f (new TFDocMessagesRKUList(this));
    f->Caption = "������ ���";
    for (int i = 0; i< m_dm->CntRKU; i++)
      f->m_CLB_rku_list->Items->Add(IntToStr(i+1));

    m_dm->m_Q_rku_list->Open();
    int idx;
    while (!m_dm->m_Q_rku_list->Eof)
    {
        idx = m_dm->m_Q_rku_listRKU->AsInteger - 1;
        if (idx > m_dm->CntRKU-1) continue;
        f->m_CLB_rku_list->Checked[idx] = true;
        m_dm->m_Q_rku_list->Next();
    }
    m_dm->m_Q_rku_list->Close();

    if (f->ShowModal() == IDOK)
    {
        m_dm->ItemsClear();
        for (int i = 0; i < f->m_CLB_rku_list->Items->Count; i++)
        {
            if (f->m_CLB_rku_list->Checked[i]) m_dm->ItemsAdd(i+1);
        }
        m_dm->RefreshItemDataSets();
    }
}
//---------------------------------------------------------------------------


void __fastcall TFMessageToRKUItem::FormShow(TObject *Sender)
{
   TTSFDocument::FormShow(Sender);
   m_CB_is_read->ItemIndex = 0;
   m_dm->IsRead = m_CB_is_read->ItemIndex;
   m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFMessageToRKUItem::m_CB_is_readChange(TObject *Sender)
{
    m_dm->IsRead = m_CB_is_read->ItemIndex;
    m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFMessageToRKUItem::m_ACT_next_fldrExecute(TObject *Sender)
{
  AnsiString m_str_msg = "";
  if (m_DBE_rku->Text == "")
  {
    m_str_msg = m_str_msg + "� ���\n";
  }
  if (m_LV_type_message->Text == "")
  {
    m_str_msg = m_str_msg + "��� ���������\n";
  }
  if (m_str_msg != "")
  {
    Application->MessageBox(("�� ��������� ��������� ����:\n" + m_str_msg).c_str(),
                            Application->Title.c_str(),
                            MB_ICONERROR | MB_YESNO);
  }
  else
  {
    TTSFDocument::m_ACT_next_fldrExecute(Sender);
  }
}
//---------------------------------------------------------------------------




void __fastcall TFMessageToRKUItem::m_ACT_resendExecute(TObject *Sender)
{
  //int item_id = m_dm->m_Q_itemID->AsInteger;
//  if (m_dm->UpdateRegimeItem != urView)
//  {
    m_dm->m_Q_resend->MacroByName("DOC_ID")->AsString = m_dm->m_Q_itemID->AsString;
    m_dm->m_Q_resend->MacroByName("DOC_NUMBER")->AsString = m_dm->GetDocNumber(m_dm->DocTypeId);
    m_dm->m_Q_resend->ExecSQL();
    Close();
//  }
}
//---------------------------------------------------------------------------

void __fastcall TFMessageToRKUItem::m_ACT_resendUpdate(TObject *Sender)
{
   //m_ACT_resend->Enabled = m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

