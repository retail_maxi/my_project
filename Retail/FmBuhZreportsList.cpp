//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmBuhZreportsList.h"
#include "FmSetDateTimePeriod.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFBuhZreportsList::TFBuhZreportsList(TComponent* p_owner,
                                                TTSDDocument *p_dm_document): TTSFDocumentList(p_owner,
                                                                                               p_dm_document),
                                                                              m_dm(static_cast<TDBuhZreports*>(DMDocument))
{
}
//---------------------------------------------------------------------------


void __fastcall TFBuhZreportsList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);
  SetPeriodCaption(m_dm->BeginDate,m_dm->EndDate);
}
//---------------------------------------------------------------------------
void __fastcall TFBuhZreportsList::SetPeriodCaption(TDateTime p_begin_date, TDateTime p_end_date)
{
      if (p_begin_date.DateTimeString().Length()>11)
        m_SBTN_period->Caption = "� " + p_begin_date.DateTimeString().SubString(1, p_begin_date.DateTimeString().Length()-3);
      else
        m_SBTN_period->Caption = "� " + p_begin_date.DateTimeString();
      if (p_end_date.DateTimeString().Length()>11)
        m_SBTN_period->Caption =m_SBTN_period->Caption  + " �� " + p_end_date.DateTimeString().SubString(1, p_end_date.DateTimeString().Length()-3);
      else
        m_SBTN_period->Caption =m_SBTN_period->Caption + " �� " + p_end_date.DateTimeString();

}

//---------------------------------------------------------------------------

void __fastcall TFBuhZreportsList::m_ACT_set_periodExecute(TObject *Sender)
{
  TFSetDateTimePeriod *f = new TFSetDateTimePeriod(this,m_dm->BeginDate, m_dm->EndDate);
  try
  {
    if (f->ShowModal() == IDOK)
    {
      SetPeriodCaption(f->BeginDate, f->EndDate);
      m_dm->SetBeginEndDate( f->BeginDate,f->EndDate);

    }
  }
  __finally
  {
    delete f;
  }
}
//---------------------------------------------------------------------------

