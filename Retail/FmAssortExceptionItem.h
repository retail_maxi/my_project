//---------------------------------------------------------------------------

#ifndef FmAssortExceptionItemH
#define FmAssortExceptionItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMREFBOOKITEM.h"
#include "DmAssortException.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "TSFormControls.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TFAssortExceptionItem : public TTSFRefbookItem
{
__published:	// IDE-managed Components
        TPanel *m_P_nmcl;
        TDataSource *m_DS_nmcl;
        TLabel *m_L_nmcl;
        TMLLovListView *m_LV_nmcl;
        TMLLov *m_LOV_nmcl;
        TMLLov *m_LOV_subcat;
        TMLLov *m_LOV_subsubcat;
        TMLLovListView *m_LV_subcat;
        TMLLovListView *m_LV_subsubcat;
        TLabel *m_L_subcat;
        TLabel *Label2;
        TDataSource *m_DS_subcat;
        TDataSource *m_DS_subsubcat;
        TLabel *m_L_1;
        TMLLovListView *m_LV_org_group;
        TDataSource *m_DS_org_groups;
        TMLLov *m_LOV_org_group;
        TPanel *m_P_1;
        TAction *m_ACT_edit_org;
        TPageControl *m_PC_main;
        TTabSheet *m_TS_nmcl;
        TTabSheet *m_TS_org;
        TPanel *m_P_ctrl_nmcl;
        TSpeedButton *m_SB_add_module;
        TPanel *m_P_ctrl_org;
        TSpeedButton *m_SB_add_org;
        TMLDBGrid *m_DBG_nmcl;
        TMLDBGrid *m_DBG_org;
        TDataSource *m_DS_sku;
        TDataSource *m_DS_orgs;
        TAction *m_ACT_edit_nmcl;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_LOV_nmclAfterClear(TObject *Sender);
        void __fastcall m_LOV_subcatAfterClear(TObject *Sender);
        void __fastcall m_LOV_subcatAfterApply(TObject *Sender);
        void __fastcall m_LOV_s(TObject *Sender);
        void __fastcall m_LOV_nmclAfterApply(TObject *Sender);
        void __fastcall m_ACT_edit_orgExecute(TObject *Sender);
        void __fastcall m_ACT_edit_nmclExecute(TObject *Sender);
        void __fastcall m_ACT_edit_orgUpdate(TObject *Sender);
        void __fastcall m_ACT_edit_nmclUpdate(TObject *Sender);
private:	// User declarations
        TDAssortException *m_dm;
public:		// User declarations
        __fastcall TFAssortExceptionItem(TComponent* p_owner, TTSDRefbook *p_dm_refbook);
protected:
        AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
};
//---------------------------------------------------------------------------
#endif
