//---------------------------------------------------------------------------

#ifndef FmCompetitorsRepriceSummItemH
#define FmCompetitorsRepriceSummItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmCompetitorsRepriceSumm.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include "FmCompetitorsRepriceSummLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFCompetitorsRepriceSummItem : public TTSFDocumentWithLines
{
__published:	// IDE-managed Components
   TLabel *m_L_doc_number;
   TMLDBPanel *m_DBE_DOC_NUMBER;
        TMLDBPanel *m_P_DOC_DATE;
   TLabel *m_L_org;
   TLabel *m_L_note;
   TDBEdit *m_DBE_note;
   TLabel *m_L_doc_date;
        TMLLov *m_LOV_org_grps;
        TMLLovListView *m_LV_org_grps;
        TDataSource *m_DS_org_grps;
        TToolBar *m_TB_1;
        TSpeedButton *m_SBTN_edit1;
        TAction *m_ACT_add_lines;
        TSpeedButton *m_SBTN_view1;
        TAction *m_ACT_osnov;
        TBitBtn *m_BBTN_view_info_nmcl;
        TAction *m_ACT_veiw_info_nmcl;
   void __fastcall m_ACT_editExecute(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall m_DBG_linesColEnter(TObject *Sender);
   void __fastcall m_ACT_editUpdate(TObject *Sender);
   void __fastcall m_ACT_deleteUpdate(TObject *Sender);
   void __fastcall m_ACT_viewUpdate(TObject *Sender);
   void __fastcall m_DBG_linesDblClick(TObject *Sender);
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall m_ACT_add_linesExecute(TObject *Sender);
        void __fastcall m_ACT_add_linesUpdate(TObject *Sender);
        void __fastcall m_LOV_org_grpsAfterApply(TObject *Sender);
        void __fastcall m_DBG_linesGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
        void __fastcall m_ACT_osnovUpdate(TObject *Sender);
        void __fastcall m_ACT_osnovExecute(TObject *Sender);
        void __fastcall m_ACT_veiw_info_nmclUpdate(TObject *Sender);
        void __fastcall m_ACT_veiw_info_nmclExecute(TObject *Sender);
protected:
  TS_FORM_LINE_SHOW(TFCompetitorsRepriceSummLine)
 // AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
private:	// User declarations
  TDCompetitorsRepriceSumm *m_dm;
  TTSOperationControl m_view_nmcl_data_ctrl;
public:		// User declarations
  __fastcall TFCompetitorsRepriceSummItem(TComponent *p_owner,TTSDDocumentWithLines *p_dm_doc_with_ln):
                  TTSFDocumentWithLines(p_owner,p_dm_doc_with_ln),
                  m_dm(static_cast<TDCompetitorsRepriceSumm*>(DMDocumentWithLines)),
                  m_view_nmcl_data_ctrl("TSRtlOperations.ViewNmclSales") {};

  __fastcall ~TFCompetitorsRepriceSummItem();


  void __fastcall RefreshItem();
  void __fastcall ResetItem();
};
//---------------------------------------------------------------------------
#endif
