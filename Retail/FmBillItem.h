//---------------------------------------------------------------------------

#ifndef FmBillItemH
#define FmBillItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmBill.h"
#include "FmBillLine.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFBillItem : public TTSFDocumentWithLines
{
__published:
  TMLDBPanel *m_DBP_doc_number;
  TLabel *m_L_doc_number;
  TMLDBPanel *m_DBP_doc_date;
  TLabel *m_L_doc_date;
  TMLDBPanel *m_DBP_on_date;
  TLabel *m_L_on_date;
  TMLDBPanel *m_DBP_src_org;
  TLabel *m_L_src_org;
  TLabel *m_L_buh_doc_number;
  TMLDBPanel *m_DBP_buh_doc_number;
  TLabel *m_L_buh_doc_date;
  TMLDBPanel *m_DBP_buh_doc_date;
  TLabel *m_L_buh_doc_id;
  TMLDBPanel *m_DBP_buh_doc_id;
  TLabel *m_L_buh_doc_amount;
  TMLDBPanel *m_DBP_buh_doc_amount;
  TLabel *m_L_contract;
  TMLDBPanel *m_DBP_contract;
  TLabel *m_L_buh_doc_cnt;
  TMLDBPanel *m_DBP_buh_doc_cnt;
  TLabel *m_L_mngr_note;
  TMLDBPanel *m_DBP_mngr_note;
  TLabel *m_L_buh_doc_note;
  TMLDBPanel *m_DBP_buh_doc_note;
  TMLDBPanel *m_DBP_amount;
  TLabel *m_L_amount;
  TDBEdit *m_DBE_note;
  TLabel *m_L_note;
  TAction *m_ACT_get_acc_doc;
  TBitBtn *m_BBTN_get_acc_doc;
  TLabel *Label1;
  TMLDBPanel *MLDBPanel1;
  TToolButton *ToolButton1;
  TCheckBox *m_CB_view_only_vld_arp;
        TLabel *m_L_info;
        TMLDBPanel *m_DBP_info;
        TAction *m_ACT_replace;
        TButton *m_BBTN_replace;
        TButton *m_BBTN_reason;
        TAction *m_ACT_reason;
  void __fastcall m_ACT_get_acc_docExecute(TObject *Sender);
  void __fastcall m_ACT_get_acc_docUpdate(TObject *Sender);
  void __fastcall m_ACT_appendUpdate(TObject *Sender);
  void __fastcall m_DBG_linesGetCellParams(TObject *Sender,
          TColumnEh *Column, TFont *AFont, TColor &Background,
          TGridDrawState State);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_CB_view_only_vld_arpClick(TObject *Sender);
        void __fastcall m_ACT_replaceExecute(TObject *Sender);
        void __fastcall m_ACT_replaceUpdate(TObject *Sender);
        void __fastcall m_ACT_reasonExecute(TObject *Sender);
        void __fastcall m_ACT_reasonUpdate(TObject *Sender);
        void __fastcall m_ACT_prev_fldrExecute(TObject *Sender);
private:
  TS_FORM_LINE_SHOW(TFBillLine)
  TDBill* m_dm;
public:
  __fastcall TFBillItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm)
    : TTSFDocumentWithLines(p_owner,p_dm),
    m_dm(static_cast<TDBill*>(p_dm))
    {};
};
//---------------------------------------------------------------------------
#endif
