inherited FProdTranslateRKUItem: TFProdTranslateRKUItem
  Left = 408
  Top = 176
  ActiveControl = m_LV_org
  Caption = 'FProdTranslateRKUItem'
  ClientHeight = 295
  ClientWidth = 468
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 16
    Top = 41
    Width = 66
    Height = 13
    Caption = '�����������'
    FocusControl = m_LV_org
  end
  object Label1: TLabel [1]
    Left = 16
    Top = 80
    Width = 119
    Height = 13
    Caption = '������������ �������'
    FocusControl = m_LV_nmcl
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 104
    Width = 111
    Height = 13
    Caption = '����������� �������'
    FocusControl = m_LV_assort
  end
  object Label4: TLabel [3]
    Left = 16
    Top = 144
    Width = 81
    Height = 13
    Caption = '����� ��������'
    FocusControl = m_LV_gr_dep
  end
  object Label5: TLabel [4]
    Left = 16
    Top = 168
    Width = 122
    Height = 13
    Caption = '������������ ��������'
    FocusControl = m_LV_nmcl_to
  end
  object Label6: TLabel [5]
    Left = 16
    Top = 192
    Width = 114
    Height = 13
    Caption = '����������� ��������'
    FocusControl = m_LV_assort_to
  end
  object Label7: TLabel [6]
    Left = 16
    Top = 216
    Width = 70
    Height = 13
    Caption = '�����������'
    FocusControl = m_DBE_coef
  end
  inherited m_P_main_control: TPanel
    Top = 245
    Width = 468
    TabOrder = 7
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 371
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 80
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 276
    Width = 468
  end
  inherited m_P_main_top: TPanel
    Width = 468
    TabOrder = 9
    inherited m_P_tb_main: TPanel
      Width = 421
      inherited m_TB_main: TToolBar
        Width = 421
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 421
    end
  end
  object m_LV_org: TMLLovListView [10]
    Left = 144
    Top = 36
    Width = 313
    Height = 21
    Lov = m_LOV_org
    UpDownWidth = 601
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 0
    ParentColor = False
  end
  object m_LV_nmcl: TMLLovListView [11]
    Left = 144
    Top = 76
    Width = 313
    Height = 21
    Lov = m_LOV_nmcl
    UpDownWidth = 601
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 1
    ParentColor = False
  end
  object m_LV_assort: TMLLovListView [12]
    Left = 144
    Top = 100
    Width = 313
    Height = 21
    Lov = m_LOV_assort
    UpDownWidth = 601
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 2
    ParentColor = False
  end
  object m_LV_gr_dep: TMLLovListView [13]
    Left = 144
    Top = 140
    Width = 313
    Height = 21
    Lov = m_LOV_gr_dep
    UpDownWidth = 601
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 3
    ParentColor = False
  end
  object m_LV_nmcl_to: TMLLovListView [14]
    Left = 144
    Top = 164
    Width = 313
    Height = 21
    Lov = m_LOV_nmcl_to
    UpDownWidth = 601
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 4
    ParentColor = False
  end
  object m_LV_assort_to: TMLLovListView [15]
    Left = 144
    Top = 188
    Width = 313
    Height = 21
    Lov = m_LOV_assort_to
    UpDownWidth = 601
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 5
    ParentColor = False
  end
  object m_DBE_coef: TDBEdit [16]
    Left = 144
    Top = 216
    Width = 121
    Height = 21
    DataField = 'COEF'
    DataSource = m_DS_item
    TabOrder = 6
  end
  inherited m_DS_item: TDataSource
    DataSet = DProdTranslateRKU.m_Q_item
    Left = 256
    Top = 0
  end
  object m_DS_org: TDataSource
    DataSet = DProdTranslateRKU.m_Q_org
    Left = 264
    Top = 32
  end
  object m_LOV_org: TMLLov
    DataFieldKey = 'ORG_ID'
    DataFieldValue = 'ORG_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_org
    AutoOpenList = True
    OnAfterApply = m_LOV_orgAfterApply
    OnAfterClear = m_LOV_orgAfterApply
    Left = 296
    Top = 32
  end
  object m_DS_nmcl: TDataSource
    DataSet = DProdTranslateRKU.m_Q_nmcl
    Left = 264
    Top = 72
  end
  object m_LOV_nmcl: TMLLov
    DataFieldKey = 'NMCL_ID'
    DataFieldValue = 'NMCL_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_nmcl
    AutoOpenList = True
    OnAfterApply = m_LOV_nmclAfterApply
    OnAfterClear = m_LOV_nmclAfterApply
    Left = 296
    Top = 72
  end
  object m_DS_assort: TDataSource
    DataSet = DProdTranslateRKU.m_Q_assort
    Left = 264
    Top = 96
  end
  object m_LOV_assort: TMLLov
    DataFieldKey = 'ASSORT_ID'
    DataFieldValue = 'ASSORT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_assort
    AutoOpenList = True
    Left = 296
    Top = 96
  end
  object m_DS_gr_dep: TDataSource
    DataSet = DProdTranslateRKU.m_Q_gr_dep_to
    Left = 264
    Top = 136
  end
  object m_LOV_gr_dep: TMLLov
    DataFieldKey = 'TO_GR_DEP_ID'
    DataFieldValue = 'TO_GR_DEP_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_gr_dep
    AutoOpenList = True
    Left = 296
    Top = 136
  end
  object m_DS_nmcl_to: TDataSource
    DataSet = DProdTranslateRKU.m_Q_nmcl_to
    Left = 264
    Top = 160
  end
  object m_LOV_nmcl_to: TMLLov
    DataFieldKey = 'TO_NMCL_ID'
    DataFieldValue = 'TO_NMCL_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_nmcl_to
    AutoOpenList = True
    OnAfterApply = m_LOV_nmcl_toAfterApply
    OnAfterClear = m_LOV_nmcl_toAfterApply
    Left = 296
    Top = 160
  end
  object m_DS_assort_to: TDataSource
    DataSet = DProdTranslateRKU.m_Q_assort_to
    Left = 264
    Top = 184
  end
  object m_LOV_assort_to: TMLLov
    DataFieldKey = 'TO_ASSORT_ID'
    DataFieldValue = 'TO_ASSORT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_assort_to
    AutoOpenList = True
    Left = 296
    Top = 184
  end
end
