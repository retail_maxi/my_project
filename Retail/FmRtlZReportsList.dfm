inherited FRtlZReportsList: TFRtlZReportsList
  Left = 345
  Top = 185
  Width = 867
  Height = 546
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 458
    Width = 851
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 754
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 366
      inherited m_SBTN_insert: TSpeedButton
        Visible = False
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 489
    Width = 851
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 851
    Height = 432
    FooterRowCount = 1
    SumList.Active = True
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'FISCAL_NUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'REPORT_NUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'REPORT_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'TAXPAYER_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'PRINT_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'REPORT_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'REPORT_10_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'REPORT_18_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SRPM_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SALES_SUM_DISC'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'BONUS_POINT_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'RET_SUM'
        PickList.Strings = ()
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 851
    inherited m_P_tb_main: TPanel
      Width = 804
      inherited m_TB_main: TToolBar
        Width = 804
        object m_TBTN_sep6: TToolButton
          Left = 302
          Top = 2
          Width = 8
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_SBTN_period: TSpeedButton
          Left = 310
          Top = 2
          Width = 250
          Height = 22
          Action = m_ACT_set_period
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 804
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'ZReports'
      Caption = 'm_ACT_set_begin_end_date'
      BeginDate = 40071
      EndDate = 40071
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
    object m_ACT_set_period: TAction
      OnExecute = m_ACT_set_periodExecute
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DRtlZReports.m_Q_list
  end
end
