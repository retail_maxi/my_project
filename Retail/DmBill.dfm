inherited DBill: TDBill
  OldCreateOrder = False
  Left = 750
  Top = 124
  Height = 615
  Width = 870
  inherited m_DB_main: TDatabase
    AliasName = 'GG'
    Connected = False
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'select'
      '  d.id,'
      '  d.src_org_id doc_src_org_id,'
      '  o.name doc_src_org_name,'
      '  d.comp_name doc_create_comp_name,'
      '  d.create_date doc_create_date,'
      '  d.author doc_create_user,'
      '  d.status doc_status,'
      
        '  decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �� ���' +
        '���'#39','#39'������'#39') doc_status_name,'
      '  d.status_change_date doc_status_change_date,'
      '  d.fldr_id doc_fldr_id,'
      '  f.name doc_fldr_name,'
      
        '  rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) doc_fldr_ri' +
        'ght,'
      '  d.modify_comp_name doc_modify_comp_name,'
      '  d.modify_user doc_modify_user,'
      '  d.modify_date doc_modify_date,'
      '  b.doc_number,'
      '  b.doc_date,'
      '  b.on_date,'
      '  b.amount,'
      '  b.note,'
      
        '  nvl2(ppd.doc_id,substr('#39'�'#39' || ppd.doc_number || '#39' �� '#39' || to_c' +
        'har(ppd.issued_date, '#39'dd.mm.yyyy'#39') || '#39' ('#39' || ppd.note || '#39')'#39',1,' +
        '250),'#39#39') acc_note,'
      '  ppd.sender_id cnt_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,25' +
        '0) cnt_name,'
      '  ppd.buyer_id mngr_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(ppd.buyer_id),1,250' +
        ') mngr_name,'
      '  b.src_org_id,'
      '  o_src.name as src_org_name,'
      '  b.acc_doc_id,'
      
        ' substr(nvl2(b.cnt_id,'#39'���������: '#39'||pkg_contractors.fnc_contr_n' +
        'ame(b.cnt_id)||'#39';'#39','#39#39')||nvl2(tp.cnt_id,'#39' ��������: '#39'||pkg_contra' +
        'ctors.fnc_contr_name(tp.cnt_id),'#39#39'),1,250) dop_info'
      'from '
      '  documents d,'
      '  folders f,'
      '  rtl_bills b,'
      '  organizations o,'
      '  organizations o_src,  '
      '  prepay_documents ppd,'
      '  taxpayer tp'
      'where '
      '  f.doc_type_id = :DOC_TYPE_ID'
      '  and f.id = d.fldr_id'
      '  and b.doc_id = d.id'
      
        '  and b.on_date between to_date(:BEGIN_DATE) and to_date(:END_DA' +
        'TE) + 1 - 1/24/60/60'
      '  and o.id  = d.src_org_id'
      '  and fnc_mask_folder_right(f.id,'#39'Select'#39') = '#39'1'#39
      '  and ppd.doc_id (+) = b.acc_doc_id'
      '  AND b.src_org_id = o_src.id(+)'
      '  and tp.id(+) = b.taxpayer_id'
      '  AND ((:ORG_ID = -1) '
      '   OR (:ORG_ID != -1 AND d.src_org_id = :ORG_ID))'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    inherited m_Q_listDOC_SRC_ORG_NAME: TStringField
      DisplayWidth = 20
    end
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���.'
      DisplayWidth = 10
      FieldName = 'DOC_NUMBER'
      Origin = 'b.doc_number'
      Size = 100
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���.'
      FieldName = 'DOC_DATE'
      Origin = 'b.doc_date'
    end
    object m_Q_listON_DATE: TDateTimeField
      DisplayLabel = '�� ����'
      FieldName = 'ON_DATE'
      Origin = 'b.on_date'
    end
    object m_Q_listAMOUNT: TFloatField
      DisplayLabel = '�����'
      FieldName = 'AMOUNT'
      Origin = 'b.amount'
      currency = True
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 40
      FieldName = 'NOTE'
      Origin = 'b.note'
      Size = 250
    end
    object m_Q_listACC_NOTE: TStringField
      DisplayLabel = '���������� � ���. ���.'
      DisplayWidth = 50
      FieldName = 'ACC_NOTE'
      Origin = 
        'nvl2(ppd.doc_id,substr('#39'�'#39' || ppd.doc_number || '#39' �� '#39' || to_cha' +
        'r(ppd.issued_date, '#39'dd.mm.yyyy'#39') || '#39' ('#39' || ppd.note || '#39')'#39',1,25' +
        '0),'#39#39')'
      Size = 250
    end
    object m_Q_listCNT_ID: TFloatField
      FieldName = 'CNT_ID'
      Visible = False
    end
    object m_Q_listCNT_NAME: TStringField
      DisplayLabel = '���������'
      DisplayWidth = 30
      FieldName = 'CNT_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,250)'
      Size = 250
    end
    object m_Q_listMNGR_ID: TFloatField
      FieldName = 'MNGR_ID'
      Visible = False
    end
    object m_Q_listMNGR_NAME: TStringField
      DisplayLabel = '��������'
      DisplayWidth = 30
      FieldName = 'MNGR_NAME'
      Origin = 'substr(pkg_contractors.fnc_contr_full_name(ppd.buyer_id),1,250)'
      Size = 250
    end
    object m_Q_listSRC_ORG_ID: TFloatField
      DisplayLabel = 'ID ����� �����������'
      FieldName = 'SRC_ORG_ID'
      Origin = 'b.src_org_id'
    end
    object m_Q_listSRC_ORG_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 100
      FieldName = 'SRC_ORG_NAME'
      Origin = 'o_src.name'
      Size = 250
    end
    object m_Q_listACC_DOC_ID: TFloatField
      DisplayLabel = 'ID ���. ���.'
      FieldName = 'ACC_DOC_ID'
      Origin = 'b.acc_doc_id'
    end
    object m_Q_listDOP_INFO: TStringField
      DisplayLabel = '���.����������'
      FieldName = 'DOP_INFO'
      Origin = 
        'substr(nvl2(b.cnt_id,'#39'���������: '#39'||pkg_contractors.fnc_contr_na' +
        'me(b.cnt_id)||'#39';'#39','#39#39')||nvl2(tp.cnt_id,'#39' ��������: '#39'||pkg_contrac' +
        'tors.fnc_contr_name(tp.cnt_id),'#39#39'),1,250)'
      Size = 250
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'select'
      '  b.doc_id id,'
      '  d.fldr_id,'
      '  b.src_org_id,'
      '  o.name dest_org_name,'
      '  b.doc_number,'
      '  b.doc_date,'
      '  b.on_date,'
      '  b.amount,'
      '  b.note,'
      '  ppd.doc_id acc_doc_id,'
      '  b.cnt_id,'
      
        '  substr(pkg_contractors.fnc_contr_full_name(ppd.sender_id),1,25' +
        '0) cnt_name,'
      '  ppd.note acc_note,'
      '  ppd.doc_number acc_doc_number,'
      '  ppd.issued_date acc_doc_date,'
      '  ppd.amount acc_doc_amount,'
      '  m.note mngr_note,'
      '  ppd.cnt_doc_id contract_id,'
      
        '  substr(cc.doc_number || nvl2(cc.note,'#39' ('#39' || cc.note || '#39')'#39','#39#39 +
        '),1,250) contract_note,'
      '  o_src.name as src_org_name,'
      
        ' substr(nvl2(b.cnt_id,'#39'���������: '#39'||pkg_contractors.fnc_contr_n' +
        'ame(b.cnt_id)||'#39';'#39','#39#39')||nvl2(tp.cnt_id,'#39' ��������: '#39'||pkg_contra' +
        'ctors.fnc_contr_name(tp.cnt_id),'#39#39'),1,250) dop_info,'
      'ppd.ttn_number,'
      'ppd.ttn_date,'
      ' (select max(doc_number) from rtl_bills'
      '  where acc_doc_id =  :ACC_DOC_ID'
      '  and doc_id != b.doc_id) doc_num'
      'from '
      '  rtl_bills b, '
      '  documents d,'
      '  organizations o,'
      '  organizations o_src,  '
      '  prepay_documents ppd,'
      '  managers m,'
      '  view_cnt_contracts cc,'
      '  taxpayer tp'
      'where '
      '  b.doc_id = :ID'
      '  and d.id = b.doc_id'
      '  and d.fldr_id = :FLDR_ID'
      '  and o.id = d.src_org_id'
      '  and ppd.doc_id (+) = b.acc_doc_id'
      '  and m.cnt_id (+) = ppd.buyer_id'
      '  and cc.doc_id (+) = ppd.cnt_doc_id'
      '  AND b.src_org_id = o_src.id(+)'
      '  and tp.id(+) = b.taxpayer_id'
      '  ')
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ACC_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_ID'
        ParamType = ptInput
      end>
    object m_Q_itemFLDR_ID: TFloatField
      FieldName = 'FLDR_ID'
    end
    object m_Q_itemSRC_ORG_ID: TFloatField
      FieldName = 'SRC_ORG_ID'
    end
    object m_Q_itemSRC_ORG_NAME: TStringField
      FieldName = 'SRC_ORG_NAME'
      Size = 250
    end
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 100
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemON_DATE: TDateTimeField
      FieldName = 'ON_DATE'
    end
    object m_Q_itemAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      currency = True
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemACC_DOC_ID: TFloatField
      FieldName = 'ACC_DOC_ID'
    end
    object m_Q_itemCNT_NAME: TStringField
      FieldName = 'CNT_NAME'
      Size = 250
    end
    object m_Q_itemACC_NOTE: TStringField
      FieldName = 'ACC_NOTE'
      Size = 250
    end
    object m_Q_itemACC_DOC_NUMBER: TStringField
      FieldName = 'ACC_DOC_NUMBER'
      Size = 50
    end
    object m_Q_itemACC_DOC_DATE: TDateTimeField
      FieldName = 'ACC_DOC_DATE'
    end
    object m_Q_itemACC_DOC_AMOUNT: TFloatField
      FieldName = 'ACC_DOC_AMOUNT'
    end
    object m_Q_itemMNGR_NOTE: TStringField
      FieldName = 'MNGR_NOTE'
      Size = 250
    end
    object m_Q_itemCONTRACT_ID: TFloatField
      FieldName = 'CONTRACT_ID'
    end
    object m_Q_itemCONTRACT_NOTE: TStringField
      FieldName = 'CONTRACT_NOTE'
      Size = 250
    end
    object m_Q_itemDEST_ORG_NAME: TStringField
      FieldName = 'DEST_ORG_NAME'
      Size = 250
    end
    object m_Q_itemDOP_INFO: TStringField
      FieldName = 'DOP_INFO'
      Size = 250
    end
    object m_Q_itemTTN_NUMBER: TStringField
      FieldName = 'TTN_NUMBER'
      Size = 50
    end
    object m_Q_itemTTN_DATE: TDateTimeField
      FieldName = 'TTN_DATE'
    end
    object m_Q_itemCNT_ID: TFloatField
      FieldName = 'CNT_ID'
    end
    object m_Q_itemDOC_NUM: TStringField
      FieldName = 'DOC_NUM'
      Size = 100
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'begin'
      ''
      '   update rtl_bills '
      '   set '
      '     acc_doc_id = :ACC_DOC_ID, '
      '     note = :NOTE  '
      '   where doc_id = :ID;'
      ''
      '  if :TTN_NUMBER IS NOT NULL then'
      '    update prepay_documents'
      '    set '
      '      ttn_number = :TTN_NUMBER,'
      '      ttn_date = :TTN_DATE'
      '    where doc_id = :ACC_DOC_ID;'
      '  end if;'
      ''
      'end;')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'SELECT'
      '  id,'
      '  line_id,'
      '  nmcl_id,'
      '  nmcl_name,'
      '  meas_name,'
      '  assortment_id,'
      '  assortment_name,'
      '  qnt,'
      '  stored_qnt,'
      '  non_stored_qnt,'
      '  in_price,'
      '  amount_in,'
      '  in_price_arp,'
      '  in_amount_arp,'
      '  note,'
      '  delta_qnt,'
      '  note_reason'
      'FROM'
      '(SELECT id,'
      '  max(line_id) as line_id,'
      '  nmcl_id,'
      '  nmcl_name,'
      '  meas_name,'
      '  assortment_id,'
      '  assortment_name,'
      '  sum(qnt) as qnt,'
      '  sum(stored_qnt) as stored_qnt,'
      '  sum(non_stored_qnt) as non_stored_qnt,'
      '  max(in_price) as in_price,'
      '  max(amount_in) as amount_in,'
      '  max(in_price_arp) as in_price_arp,'
      '  max(in_amount_arp) as in_amount_arp,'
      '  max(note) as note,'
      '  nvl(sum(qnt),0) - nvl(sum(stored_qnt),0) as delta_qnt,'
      '  max(note_reason) as note_reason'
      'FROM  '
      ''
      '('
      'select'
      '  bi.doc_id as id,'
      '  bi.line_id as line_id ,'
      '  bi.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  bi.assortment_id,'
      '  a.name assortment_name,'
      '  bi.qnt,'
      '  bi.in_price,'
      '  bi.amount_in,'
      '  to_number(null) as in_price_arp,'
      '  to_number(null) as in_amount_arp,    '
      '  to_number(null) as stored_qnt, '
      '    to_number(null) as non_stored_qnt,'
      '  bi.note,'
      '  bi.note_reason'
      'from'
      '  rtl_bill_items bi,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where bi.doc_id = :ID'
      '  and ni.id = bi.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and a.id = bi.assortment_id'
      'UNION ALL'
      
        '-- ���������� �� ��������������� ����� ����� ����� ������� �����' +
        '��'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  ii.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  ii.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(ii.src_amount)/nullif(sum(ii.qnt * sign(ii.stored)),0) as ' +
        'in_price_arp,'
      '  sum(ii.src_amount) as in_amount_arp,  '
      '  sum(ii.qnt * sign(ii.stored)) stored_qnt, '
      '    sum(ii.qnt * (1-sign(ii.stored))) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      
        'from rtl_bills b, rtl_incomes i, rtl_income_items ii, documents ' +
        'd,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.acc_doc_id = b.acc_doc_id and ii.doc_id = i.doc_id'
      '    AND i.doc_id = d.id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP'
      '    AND ii.nmcl_id = ni.id'
      '    AND ni.meas_unit_id = mu.id'
      '    AND ii.assortment_id = a.id'
      'group by ii.nmcl_id, ni.name, mu.name, ii.assortment_id,a.name'
      'UNION ALL'
      
        '-- ���������� �� ��������������� ����� ����� ����� ������� �����' +
        '��'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  ii.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  ii.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(ii.items_qnt*ii.in_price)/nullif(sum(ii.items_qnt * sign(i' +
        'i.in_store)),0) as in_price_arp,'
      '  sum(ii.items_qnt*ii.in_price) as in_amount_arp,  '
      '  sum(ii.items_qnt * sign(ii.in_store)) stored_qnt, '
      '    sum(ii.items_qnt * (1-sign(ii.in_store))) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      
        'from rtl_bills b, income_goods i, income_goods_items ii, documen' +
        'ts d,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.buh_doc_id = b.acc_doc_id and ii.doc_id = i.doc_id'
      '    AND i.doc_id = d.id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP'
      '    AND ii.nmcl_id = ni.id'
      '    AND ni.meas_unit_id = mu.id'
      '    AND ii.assortment_id = a.id'
      'group by ii.nmcl_id, ni.name, mu.name, ii.assortment_id,a.name'
      'UNION ALL'
      
        '-- ���������� �� ��������������� ����� ����� ����� ������� �����' +
        '�� (��)'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  ii.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  ii.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(ii.src_amount)/nullif(sum(ii.qnt * sign(ii.stored)),0) as ' +
        'in_price_arp,'
      '  sum(ii.src_amount) as in_amount_arp,  '
      '  sum(ii.qnt * sign(ii.stored)) stored_qnt, '
      '    sum(ii.qnt * (1-sign(ii.stored))) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      'from rtl_bills b, fk_incomes i, fk_income_items ii, documents d,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.acc_doc_id = b.acc_doc_id and ii.doc_id = i.doc_id'
      '    AND i.doc_id = d.id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP'
      '    AND ii.nmcl_id = ni.id'
      '    AND ni.meas_unit_id = mu.id'
      '    AND ii.assortment_id = a.id'
      'group by ii.nmcl_id, ni.name, mu.name, ii.assortment_id,a.name'
      'union all'
      '--������������� �� ��������������� ��� - ������'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  eqi.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  eqi.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(eqi.items_qnt*eqi.in_price)/nullif(sum(eqi.items_qnt),0) a' +
        's in_price_arp,'
      '  sum(eqi.items_qnt*eqi.in_price) as in_amount_arp,  '
      '  sum(eqi.items_qnt) stored_qnt, '
      '  to_number(null) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      
        'from rtl_bills b, rtl_incomes i, rtl_edit_qnt eq, rtl_edit_qnt_i' +
        'n_items eqi, '
      '  documents d, documents d2,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.acc_doc_id = b.acc_doc_id '
      '    AND d.id = i.doc_id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP    '
      '    and eq.base_doc_id = i.doc_id'
      '    and d2.id = eq.id'
      '    and d2.status != '#39'D'#39
      '    and eqi.doc_id = eq.id'
      '    AND eqi.nmcl_id = ni.id'
      '    AND ni.meas_unit_id = mu.id'
      '    AND eqi.assortment_id = a.id'
      'group by eqi.nmcl_id, ni.name, mu.name, eqi.assortment_id,a.name'
      'union all'
      '--������������� �� ��������������� ���  - ��������'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  grc.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  grc.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(eqi.items_qnt*eqi.in_price)/nullif(sum(eqi.items_qnt),0) a' +
        's in_price_arp,'
      '  -1*sum(eqi.items_qnt*eqi.in_price) as in_amount_arp,  '
      '  -1*sum(eqi.items_qnt) stored_qnt, '
      '  to_number(null) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      
        'from rtl_bills b, rtl_incomes i, rtl_edit_qnt eq, rtl_edit_qnt_o' +
        'ut_items eqi, '
      '  documents d, documents d2,'
      '   goods_record_cards grc,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.acc_doc_id = b.acc_doc_id '
      '    AND d.id = i.doc_id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP    '
      '    and eq.base_doc_id = i.doc_id'
      '    and d2.id = eq.id'
      '    and d2.status != '#39'D'#39
      '    and eqi.doc_id = eq.id'
      '    AND grc.id = eqi.card_id      '
      '    AND ni.id = grc.nmcl_id'
      '    AND ni.meas_unit_id = mu.id'
      '    and a.id = grc.assortment_id'
      'group by grc.nmcl_id, ni.name, mu.name, grc.assortment_id,a.name'
      'union all'
      '--������������� �� ��������������� ��� �� - ������'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  eqi.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  eqi.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(eqi.items_qnt*eqi.in_price)/nullif(sum(eqi.items_qnt),0) a' +
        's in_price_arp,'
      '  sum(eqi.items_qnt*eqi.in_price) as in_amount_arp,  '
      '  sum(eqi.items_qnt) stored_qnt, '
      '  to_number(null) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      
        'from rtl_bills b, fk_incomes i, rtl_edit_qnt eq, rtl_edit_qnt_in' +
        '_items eqi, '
      '  documents d, documents d2,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.acc_doc_id = b.acc_doc_id '
      '    AND d.id = i.doc_id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP    '
      '    and eq.base_doc_id = i.doc_id'
      '    and d2.id = eq.id'
      '    and d2.status != '#39'D'#39
      '    and eqi.doc_id = eq.id'
      '    AND eqi.nmcl_id = ni.id'
      '    AND ni.meas_unit_id = mu.id'
      '    AND eqi.assortment_id = a.id'
      'group by eqi.nmcl_id, ni.name, mu.name, eqi.assortment_id,a.name'
      'union all'
      '--������������� �� ��������������� ��� ��  - ��������'
      'select '
      '  :ID as id,'
      '  to_number(null) as line_id ,'
      '  grc.nmcl_id,'
      '  ni.name nmcl_name,'
      '  mu.name meas_name,'
      '  grc.assortment_id,'
      '  a.name assortment_name,'
      '  to_number(null) as qnt,'
      '  to_number(null) as in_price,'
      '  to_number(null) as amount_in,  '
      
        '  sum(eqi.items_qnt*eqi.in_price)/nullif(sum(eqi.items_qnt),0) a' +
        's in_price_arp,'
      '  -1*sum(eqi.items_qnt*eqi.in_price) as in_amount_arp,  '
      '  -1*sum(eqi.items_qnt) stored_qnt, '
      '  to_number(null) non_stored_qnt,'
      '    null as note,'
      '    null as note_reason'
      
        'from rtl_bills b, fk_incomes i, rtl_edit_qnt eq, rtl_edit_qnt_ou' +
        't_items eqi, '
      '  documents d, documents d2,'
      '   goods_record_cards grc,'
      '  nomenclature_items ni,'
      '  measure_units mu,'
      '  assortment a'
      'where b.doc_id = :ID '
      '    and i.acc_doc_id = b.acc_doc_id '
      '    AND d.id = i.doc_id'
      '    AND d.status != '#39'D'#39
      '%VIEW_ONLY_VLD_ARP    '
      '    and eq.base_doc_id = i.doc_id'
      '    and d2.id = eq.id'
      '    and d2.status != '#39'D'#39
      '    and eqi.doc_id = eq.id'
      '    AND grc.id = eqi.card_id      '
      '    AND ni.id = grc.nmcl_id'
      '    AND ni.meas_unit_id = mu.id'
      '    and a.id = grc.assortment_id'
      'group by grc.nmcl_id, ni.name, mu.name, grc.assortment_id,a.name'
      ''
      ')'
      
        'GROUP BY id, nmcl_id, nmcl_name, meas_name, assortment_id, assor' +
        'tment_name'
      ')'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'VIEW_ONLY_VLD_ARP'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'bi.id'
    end
    inherited m_Q_linesLINE_ID: TFloatField
      Visible = False
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      DisplayWidth = 30
      FieldName = 'NMCL_NAME'
      Origin = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_linesMEAS_NAME: TStringField
      DisplayLabel = '��. ���.'
      DisplayWidth = 10
      FieldName = 'MEAS_NAME'
      Origin = 'MEAS_NAME'
      Size = 30
    end
    object m_Q_linesASSORTMENT_ID: TFloatField
      DisplayLabel = '��� ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'ASSORTMENT_ID'
    end
    object m_Q_linesASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      DisplayWidth = 30
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_linesQNT: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QNT'
      Origin = 'QNT'
    end
    object m_Q_linesSTORED_QNT: TFloatField
      DisplayLabel = '���-�� �� ���|� �����'
      FieldName = 'STORED_QNT'
      Origin = 'STORED_QNT'
    end
    object m_Q_linesNON_STORED_QNT: TFloatField
      DisplayLabel = '���-�� �� ���|�� � �����'
      FieldName = 'NON_STORED_QNT'
      Origin = 'NON_STORED_QNT'
    end
    object m_Q_linesIN_PRICE: TFloatField
      DisplayLabel = '����*'
      FieldName = 'IN_PRICE'
      Origin = 'IN_PRICE'
      currency = True
    end
    object m_Q_linesAMOUNT_IN: TFloatField
      DisplayLabel = '�����*'
      FieldName = 'AMOUNT_IN'
      Origin = 'AMOUNT_IN'
      currency = True
    end
    object m_Q_linesNOTE: TStringField
      DisplayLabel = '����������'
      DisplayWidth = 40
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 250
    end
    object m_Q_linesIN_PRICE_ARP: TFloatField
      FieldName = 'IN_PRICE_ARP'
    end
    object m_Q_linesIN_AMOUNT_ARP: TFloatField
      FieldName = 'IN_AMOUNT_ARP'
    end
    object m_Q_linesDELTA_QNT: TFloatField
      DisplayLabel = '���-�� �� ���|�����������'
      FieldName = 'DELTA_QNT'
      Origin = 'delta_qnt'
    end
    object m_Q_linesNOTE_REASON: TStringField
      DisplayLabel = '�������'
      DisplayWidth = 40
      FieldName = 'NOTE_REASON'
      Origin = 'NOTE_REASON'
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'select'
      '  bi.doc_id id,'
      '  bi.line_id,'
      '  bi.nmcl_id,'
      '  ni.name nmcl_name,'
      '  bi.assortment_id,'
      '  a.name assortment_name,'
      '  mu.name mu_name,'
      '  bi.qnt,'
      '  bi.in_price,'
      '  bi.amount_in,'
      '  bi.bar_code,'
      '  bi.note'
      'from'
      '  rtl_bill_items bi,'
      '  nomenclature_items ni, '
      '  measure_units mu, '
      '  assortment a'
      'where'
      '  bi.doc_id = :ID'
      '  and bi.line_id = :LINE_ID'
      '  and ni.id = bi.nmcl_id'
      '  and mu.id = ni.meas_unit_id'
      '  and a.id = bi.assortment_id')
    Left = 488
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'TSDBMAIN.NOMENCLATURE_ITEMS.NAME'
      Size = 100
    end
    object m_Q_lineASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.ASSORTMENT_ID'
    end
    object m_Q_lineASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'TSDBMAIN.ASSORTMENT.NAME'
      Size = 100
    end
    object m_Q_lineMU_NAME: TStringField
      FieldName = 'MU_NAME'
      Origin = 'TSDBMAIN.MEASURE_UNITS.NAME'
      Size = 30
    end
    object m_Q_lineQNT: TFloatField
      FieldName = 'QNT'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.QNT'
    end
    object m_Q_lineIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.IN_PRICE'
      currency = True
    end
    object m_Q_lineAMOUNT_IN: TFloatField
      FieldName = 'AMOUNT_IN'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.AMOUNT_IN'
      currency = True
    end
    object m_Q_lineBAR_CODE: TStringField
      FieldName = 'BAR_CODE'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.BAR_CODE'
      Size = 30
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.NOTE'
      Size = 250
    end
  end
  inherited m_U_line: TMLUpdateSQL
    Left = 544
  end
  object m_Q_only_vld_arp: TMLQuery
    SQL.Strings = (
      'AND d.fldr_id NOT IN (21332,2432)')
    Macros = <>
    Left = 200
    Top = 312
  end
  object m_U_reason: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_bill_items '
      '   set '
      '     note_reason = :NOTE_REASON'
      'where doc_id = :DOC_ID'
      'and line_id = :LINE_ID')
    InsertSQL.Strings = (
      
        'insert into rtl_bill_items(doc_id, line_id, nmcl_id, assortment_' +
        'id,  note_reason)'
      
        'values (:DOC_ID, :LINE_ID, :NMCL_ID, :ASSORTMENT_ID, :NOTE_REASO' +
        'N)')
    IgnoreRowsAffected = True
    Left = 424
    Top = 320
  end
  object m_Q_reason: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_reasonBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select doc_id,'
      '       line_id,'
      '       nmcl_id,'
      '       assortment_id,'
      '       note_reason'
      'from rtl_bill_items'
      'where doc_id = :ID'
      'and line_id = :LINE_ID')
    UpdateObject = m_U_reason
    Macros = <>
    Left = 344
    Top = 320
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_reasonDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.DOC_ID'
    end
    object m_Q_reasonLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.LINE_ID'
    end
    object m_Q_reasonNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.NMCL_ID'
    end
    object m_Q_reasonASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.ASSORTMENT_ID'
    end
    object m_Q_reasonNOTE_REASON: TStringField
      FieldName = 'NOTE_REASON'
      Origin = 'TSDBMAIN.RTL_BILL_ITEMS.NOTE_REASON'
      Size = 250
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 305
    Top = 240
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
end
