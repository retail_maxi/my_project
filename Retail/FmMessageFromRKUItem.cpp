//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmMessageFromRKUItem.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFMessageFromRKUItem::TFMessageFromRKUItem(TComponent* p_owner,
                                                      TTSDDocument *p_dm_document)
    : TTSFDocument(p_owner, p_dm_document),
    m_dm(dynamic_cast<TDMessageFromRKU*>(p_dm_document))
{
}
//---------------------------------------------------------------------------
 