inherited FAssortExceptionList: TFAssortExceptionList
  Left = 749
  Top = 185
  Width = 815
  Height = 600
  Caption = 'FAssortExceptionList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 512
    Width = 799
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 702
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 314
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 543
    Width = 799
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 799
    Height = 486
    TitleFont.Name = 'MS Sans Serif'
    OnDblClick = nil
    OnKeyPress = nil
    FooterFont.Name = 'MS Sans Serif'
    AutoFitColWidths = False
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'GR_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'GR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 300
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CAT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CAT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 300
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SUBCAT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SUBCAT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SUBSUBCAT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SUBSUBCAT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_GROUP_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_GROUP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 150
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORGS'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 250
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCLS'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 250
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 799
    inherited m_P_tb_main: TPanel
      Width = 752
      inherited m_TB_main: TToolBar
        Width = 752
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 752
    end
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_edit: TAction
      ShortCut = 0
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DAssortException.m_Q_list
  end
end
