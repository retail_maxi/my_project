//---------------------------------------------------------------------------

#ifndef FmDocRtlCouponDiscOrgsH
#define FmDocRtlCouponDiscOrgsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "RxQuery.hpp"
#include <Db.hpp>
#include <DBTables.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
//---------------------------------------------------------------------------

class TFDocRtlCouponDiscOrgs : public TForm
{
__published:
  TPanel *m_P_main_control;
  TToolBar *m_TB_main_control_buttons_dialog;
  TBitBtn *m_SBTN_save;
  TBitBtn *m_BBTN_close;
  TImageList *m_IL_main;
  TActionList *m_ACTL_main;
  TAction *m_ACT_close;
  TAction *m_ACT_apply_updates;
  TDatabase *m_DB_gl;
   TMLQuery *m_Q_sel_orgs;
   TMLUpdateSQL *m_U_sel_orgs;
   TDataSource *m_DS_orgs;
   TFloatField *m_Q_sel_orgsID;
   TStringField *m_Q_sel_orgsNAME;
   TFloatField *m_Q_sel_orgsCHECKED;
   TMLDBGrid *m_DBG_list;
   TMLQuery *m_Q_ins_orgs;
   TFloatField *m1;
   TStringField *m2;
   TFloatField *m3;
   TMLQuery *m_Q_del_orgs;
   TFloatField *m4;
   TStringField *m5;
   TFloatField *m6;
  void __fastcall m_ACT_closeExecute(TObject *Sender);
  void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall m_DBG_listKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
   void __fastcall m_Q_sel_orgsBeforeOpen(TDataSet *DataSet);
private:
  bool FPressOK;
  long m_doc_id;
  bool __fastcall GetPressOK();
  hDBIDb m_db_handle;
 bool __fastcall FillNodesCheck(int node_id, bool m_check);
public:
  __fastcall TFDocRtlCouponDiscOrgs(TComponent* p_owner,
                         hDBIDb p_db_handle,
                         long p_doc_id);
  __fastcall ~TFDocRtlCouponDiscOrgs();
  __property bool PressOK  = { read=GetPressOK };
};
//---------------------------------------------------------------------------

bool ExecDocRtlCouponDiscOrgsDlg(TComponent *p_owner,
                      hDBIDb p_db_handle,
                      long p_doc_id);
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

