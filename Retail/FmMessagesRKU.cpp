//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "FmMessagesRKU.h"
#include "TSFmDocumentProperty.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFMessagesRKU::TFMessagesRKU(TComponent* p_owner,
                                        TTSDOperation *p_dm_operation)
    : TTSFOperation(p_owner, p_dm_operation),
     m_doc_type_id(m_dm->DocFromRKU),
     m_dm(static_cast<TDMessagesRKU*>(DMOperation)),
     m_can_execute_item(false)
{
    m_TSFC_from_rku->CreateObject();
    m_TSFC_to_rku->CreateObject();
}
//---------------------------------------------------------------------------

__fastcall TFMessagesRKU::~TFMessagesRKU()
{
    m_TSFC_from_rku->DestroyObject();
    m_TSFC_to_rku->DestroyObject();
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
    m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                          m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_insertExecute(TObject *Sender)
{
  long id = -1;

  m_can_execute_item = true;
  id = m_TSFC_to_rku->ItemImpl->Insert();
  m_can_execute_item = false;

  if( id > 0 )
  {
    m_dm->RefreshDataSets();
    m_dm->m_Q_main->Locate(m_dm->m_Q_mainID->FieldName, id, TLocateOptions());
    m_dm->m_last_id = id;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_insertUpdate(TObject *Sender)
{
    m_ACT_insert->Enabled = m_dm->CanInsertItem;
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_editExecute(TObject *Sender)
{
  bool res = false;
  int id = m_dm->m_Q_mainID->AsInteger;
  m_can_execute_item = true;
  if (m_dm->m_Q_mainDOC_TYPE_ID->AsInteger == m_dm->DocFromRKU) res = m_TSFC_from_rku->ItemImpl->Edit(id);
  else if (m_dm->m_Q_mainDOC_TYPE_ID->AsInteger == m_dm->DocToRKU) res = m_TSFC_to_rku->ItemImpl->Edit(id);
  else
  {
     m_can_execute_item = false;
     throw ETSError("��� ��������� �� ��������������");
  }
  m_can_execute_item = false;
  if ((res))
  {
    m_dm->RefreshDataSets();
    m_dm->m_Q_main->Locate(m_dm->m_Q_mainID->FieldName, id, TLocateOptions());
  }
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_editUpdate(TObject *Sender)
{
    m_ACT_edit->Enabled = m_dm->CanEditItem;    
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_deleteExecute(TObject *Sender)
{
  if( Application->MessageBox("������� ��������?",
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES )
  {
    int id = m_dm->m_Q_mainID->AsInteger;
    m_can_execute_item = true;
    if (m_dm->m_Q_mainDOC_TYPE_ID->AsInteger == m_dm->DocFromRKU) m_TSFC_from_rku->ItemImpl->Delete(id);
    else if (m_dm->m_Q_mainDOC_TYPE_ID->AsInteger == m_dm->DocToRKU) m_TSFC_to_rku->ItemImpl->Delete(id);
    else
    {
        m_can_execute_item = false;
        throw ETSError("��� ��������� �� ��������������");
    }
    m_dm->RefreshDataSets();
    m_dm->m_Q_main->Locate(m_dm->m_Q_mainID->FieldName, id, TLocateOptions());
    m_can_execute_item = false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_deleteUpdate(TObject *Sender)
{
    m_ACT_delete->Enabled = m_dm->CanDeleteItem;    
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_viewExecute(TObject *Sender)
{
  int id = m_dm->m_Q_mainID->AsInteger;
  m_can_execute_item = true;
  if (m_dm->m_Q_mainDOC_TYPE_ID->AsInteger == m_dm->DocFromRKU) m_TSFC_from_rku->ItemImpl->Interface->View(id);
  else if (m_dm->m_Q_mainDOC_TYPE_ID->AsInteger == m_dm->DocToRKU) m_TSFC_to_rku->ItemImpl->Interface->View(id);
  else
  {
      m_can_execute_item = false;
      throw ETSError("��� ��������� �� ��������������");
  }
  m_dm->RefreshDataSets();
  m_dm->m_Q_main->Locate(m_dm->m_Q_mainID->FieldName, id, TLocateOptions());
  m_can_execute_item = false;
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_viewUpdate(TObject *Sender)
{
    m_ACT_view->Enabled = m_dm->CanViewItem;    
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_DBG_mainGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
    if( (m_dm->m_Q_mainDOC_STATUS->AsString == 'D') &&
        !State.Contains(gdSelected) )
    {
        Background = clRed;
        AFont->Color = clWindow;
        return;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_DBG_mainDblClick(TObject *Sender)
{
    m_ACT_view->Execute();    
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_DBG_mainKeyPress(TObject *Sender,
      char &Key)
{
    if( int(Key) == VK_RETURN ) m_ACT_view->Execute();     
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::FormShow(TObject *Sender)
{
    TTSFOperation::FormShow(Sender);
    m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
    m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
    if (m_dm->m_Q_doc_ids->Active)
    {m_dm->m_Q_doc_ids->Close();
    }
    m_dm->m_Q_doc_ids->Open();
    m_dm->m_last_id = m_dm->m_Q_doc_idsLAST_ID->AsInteger;

    m_DBLCB_organizations->KeyValue = m_dm->OrgId;
    if (m_dm->HomeOrgId != 3)
      m_DBLCB_organizations->Enabled = false;
    else
      m_DBLCB_organizations->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_doc_propExecute(TObject *Sender)
{
    TSExecDocumentPropertyDlg(this, m_dm->m_DB_main->Handle, m_dm->m_Q_mainID->AsInteger);    
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_doc_propUpdate(TObject *Sender)
{
    m_ACT_doc_prop->Enabled = m_ACT_view->Enabled;    
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_ACT_auto_refreshExecute(TObject *Sender)
{
    if (m_CB_timer->Checked)
    {
        int sec = m_E_time->Text.ToIntDef(0);
        if (sec == 0)
        {
            sec = 5;
            m_E_time->Text = IntToStr(sec);
        }
        m_T_sb->Interval = 1000 * sec;
    }
    m_T_sb->Enabled = m_CB_timer->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_T_sbTimer(TObject *Sender)
{
    // �������� ������
    m_dm->RefreshDataSets();
    //���� ���� �� ���� ��� ������� - �� ������������ ������ ���������
    if (m_can_execute_item)
    {   
        return;
    }
    //���������� ������
    m_T_sb->Enabled = false;

    //���������� ������ ����������
    if (m_dm->m_Q_doc_ids->Active)
    {
      m_dm->m_Q_doc_ids->Close();
    }
    m_dm->m_Q_doc_ids->Open();

    //�������� �� ������ �������
   // if (m_dm->m_last_id != m_dm->m_Q_doc_idsLAST_ID->AsInteger)
   // {
      //�������� ����� ���������
      if (m_dm->m_Q_new_docs->Active)
      {
        m_dm->m_Q_new_docs->Close();
      }

      m_dm->m_Q_new_docs->ParamByName("LAST_ID")->AsInteger = m_dm->m_last_id;
      m_dm->m_Q_new_docs->Open();

      //������� �� �������������� ��-�������
      m_dm->m_Q_new_docs->First();
      while (!m_dm->m_Q_new_docs->Eof)
      {
        bool res = false;
        int id = m_dm->m_Q_new_docsID->AsInteger;

        if (m_dm->m_Q_new_docsDOC_TYPE_ID->AsInteger == m_dm->DocFromRKU)
           res = m_TSFC_from_rku->ItemImpl->Edit(id);
        else if (m_dm->m_Q_new_docsDOC_TYPE_ID->AsInteger == m_dm->DocToRKU)
           res = m_TSFC_to_rku->ItemImpl->Edit(id);
        else throw ETSError("��� ��������� �� ��������������");
        if ((res))
        {
          m_dm->RefreshDataSets();
          m_dm->m_Q_main->Locate(m_dm->m_Q_mainID->FieldName, id, TLocateOptions());
        }
        m_dm->m_Q_new_docs->Next();
      }
   // }
    //�������� ������ ����������
    m_dm->m_last_id = m_dm->m_Q_doc_idsLAST_ID->AsInteger;

    // ��������� ������
    m_T_sb->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFMessagesRKU::m_E_timeChange(TObject *Sender)
{
    int sec = m_E_time->Text.ToIntDef(0);
    m_E_time->Text = IntToStr(sec);
    if (sec == 0)
    {
        m_CB_timer->Checked = false;
        m_T_sb->Enabled = false;
    }
    else
        m_T_sb->Interval = 1000 * sec;
}
//---------------------------------------------------------------------------





void __fastcall TFMessagesRKU::m_DBLCB_organizationsClick(TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;
}
//---------------------------------------------------------------------------

