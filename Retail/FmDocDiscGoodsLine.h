//---------------------------------------------------------------------------

#ifndef FmDocDiscGoodsLineH
#define FmDocDiscGoodsLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmDocDiscGoods.h"
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "RxLookup.hpp"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFDocDiscGoodsLine : public TTSFDocumentLine
{
__published:	// IDE-managed Components
        TPageControl *m_PC_main;
        TTabSheet *m_TS_new;
        TTabSheet *m_TS_old;
        TLabel *Label1;
        TRxDBLookupCombo *m_DBLC_grp;
        TDataSource *m_DS_org_groups;
        TLabel *Label2;
        TMLLovListView *m_LV_orgs;
        TDataSource *m_DS_orgs;
        TMLLov *m_LOV_orgs;
        TLabel *Label4;
        TDBEdit *m_DBE_note;
        TMLDBGrid *m_DBG_list;
        TDataSource *m_DS_all;
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
        void __fastcall m_ACT_insertUpdate(TObject *Sender);
        void __fastcall m_DBG_listKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
  TDDocDiscGoods *m_dm;
  bool __fastcall FillNodesCheck(int node_id, bool m_check);
protected:
  virtual AnsiString __fastcall BeforeLineApply(TWinControl *p_focused);
public:		// User declarations
  __fastcall TFDocDiscGoodsLine(TComponent* p_owner,
                             TTSDDocumentWithLines *p_dm_doc_with_ln): TTSFDocumentLine(p_owner,
                                                                                        p_dm_doc_with_ln),
                                                                       m_dm(static_cast<TDDocDiscGoods*>(DMDocumentWithLines)){};
};
//---------------------------------------------------------------------------
#endif
