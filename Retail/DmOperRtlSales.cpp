//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmOperRtlSales.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TDOperRtlSales::TDOperRtlSales(TComponent* p_owner, AnsiString p_prog_id):
                           TTSDOperation(p_owner,p_prog_id),
                           m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
  SetBeginEndDate(int(GetSysDate()), GetSysDate());
  is_tabac = false;
  m_Q_orgs->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDOperRtlSales::GetBeginDate()
{
  return m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDOperRtlSales::GetEndDate()
{
  return m_Q_main->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDOperRtlSales::SetBeginEndDate(const TDateTime &p_begin_date,
                                               const TDateTime &p_end_date)
{
  if( (m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime == p_begin_date) &&
      (m_Q_main->ParamByName("END_DATE")->AsDateTime == p_end_date) ) return;

  m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = p_begin_date;
  m_Q_main->ParamByName("END_DATE")->AsDateTime = p_end_date;

  if( m_Q_main->Active ) RefreshDataSets();
}
//---------------------------------------------------------------------------
__fastcall TDOperRtlSales::~TDOperRtlSales()
{
  if( m_Q_orgs->Active ) m_Q_orgs->Close();
}
//---------------------------------------------------------------------

int __fastcall TDOperRtlSales::GetOrgId()
{
  return m_Q_main->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDOperRtlSales::SetOrgId(int p_value)
{
  if( GetOrgId() == p_value ) return;

  m_Q_main->ParamByName("ORG_ID")->AsInteger = p_value;

  if( m_Q_main->Active )
  {
    m_Q_main->Close();
    m_Q_main->Open();
  }
}
//---------------------------------------------------------------------------
void __fastcall TDOperRtlSales::m_Q_mainBeforeOpen(TDataSet *DataSet)
{
   m_Q_main->MacroByName("WHERE_TABAC")->AsString = is_tabac ? "and exists (select 1 from rtl_sale_lines l, nomenclature_items ni where l.doc_id = s.doc_id and l.nmcl_id = ni.id and ni.prod_type_id = 555 and abs(PKG_RTL_PRICES.fnc_cur_out_price(l.nmcl_id, l.assort_id, o.id)/nullif(l.out_price, 0) -1) > 0.1)":"";
}
//---------------------------------------------------------------------------


