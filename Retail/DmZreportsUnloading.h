//---------------------------------------------------------------------------

#ifndef DmZreportsUnloadingH
#define DmZreportsUnloadingH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMOPERATION.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDZreportsUnloading : public TTSDOperation
{
__published:	// IDE-managed Components
   TMLQuery *m_Q_tax;
   TFloatField *m_Q_taxID;
   TStringField *m_Q_taxNAME;
   TMLQuery *m_Q_param;
   TMLUpdateSQL *m_U_param;
   TFloatField *m_Q_paramID;
   TStringField *m_Q_paramNAME;
   TFloatField *m_Q_mainDOC_ID;
   TFloatField *m_Q_mainORG_ID;
   TStringField *m_Q_mainORG_NAME;
   TFloatField *m_Q_mainFISCAL_NUM;
   TFloatField *m_Q_mainREPORT_NUM;
   TDateTimeField *m_Q_mainREPORT_DATE;
   TFloatField *m_Q_mainREPORT_SUM;
   void __fastcall m_Q_mainBeforeOpen(TDataSet *DataSet);
private:	// User declarations
   int m_taxpayer;
public:		// User declarations
   TDateTime m_begin_date;
   TDateTime m_end_date;

   void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                             const TDateTime &p_end_date) ;
   void __fastcall SetTaxpayer(int p_taxpayer)   ;

   __fastcall TDZreportsUnloading(TComponent* p_owner, AnsiString p_prog_id);
   __fastcall TDZreportsUnloading::~TDZreportsUnloading();
};
//---------------------------------------------------------------------------
#endif
