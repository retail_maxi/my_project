inherited FMessageFromRKUList: TFMessageFromRKUList
  Left = 256
  Top = 114
  Width = 859
  Height = 640
  Caption = 'FMessageFromRKUList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 556
    Width = 851
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 754
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 366
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 587
    Width = 851
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 851
    Height = 530
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 53
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 71
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 84
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 106
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 138
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 104
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 151
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 183
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 37
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 133
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 39
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MESSAGE_TYPE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'HOST'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 150
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'RKU'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'READ_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'READ_USER_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'READ_USER_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 851
    inherited m_P_tb_main: TPanel
      Width = 804
      inherited m_TB_main: TToolBar
        Width = 804
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 804
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DMessageFromRKU.m_Q_list
  end
end
