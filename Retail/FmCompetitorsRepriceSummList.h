//---------------------------------------------------------------------------

#ifndef FmCompetitorsRepriceSummListH
#define FmCompetitorsRepriceSummListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTLIST.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmCompetitorsRepriceSumm.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFCompetitorsRepriceSummList : public TTSFDocumentList
{
__published:	// IDE-managed Components
   TToolButton *m_TBTN_sep;
   TSpeedButton *m_SBTN_set_begin_end_date;
   TMLSetBeginEndDate *m_ACT_set_begin_end_date;
   void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
private:	// User declarations
   TDCompetitorsRepriceSumm *m_dm;
public:		// User declarations
 __fastcall TFCompetitorsRepriceSummList(TComponent* p_owner,
                             TTSDDocument *p_dm_document): TTSFDocumentList(p_owner,
                              p_dm_document),
                              m_dm(static_cast<TDCompetitorsRepriceSumm*>(DMDocument)) {};

};
//---------------------------------------------------------------------------
#endif
