inherited DZreportsUnloading: TDZreportsUnloading
  Left = 263
  Top = 107
  Height = 640
  Width = 870
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
  end
  inherited m_Q_fill_param_val: TQuery
    UpdateObject = nil
  end
  inherited m_Q_main: TMLQuery
    BeforeOpen = m_Q_mainBeforeOpen
    SQL.Strings = (
      'SELECT d.id AS doc_id,'
      '       d.src_org_id AS org_id,'
      '       o.name AS org_name,'
      '       zr.fiscal_num,'
      '       zr.report_num,'
      '       zr.report_date,'
      '       zr.report_sum'
      'FROM documents d,'
      '     organizations o,'
      '     rtl_zreports zr'
      'WHERE o.id = d.src_org_id'
      '  AND zr.doc_id = d.id'
      '  AND zr.report_date BETWEEN :BEGIN_DATE AND :END_DATE '
      '  AND zr.fiscal_taxpayer_id = :TAXPAYER_ID'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end>
    Top = 136
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'TAXPAYER_ID'
        ParamType = ptInput
      end>
    object m_Q_mainDOC_ID: TFloatField
      DisplayLabel = 'ID ���������'
      FieldName = 'DOC_ID'
      Origin = 'd.id'
    end
    object m_Q_mainORG_ID: TFloatField
      DisplayLabel = 'ID ���'
      FieldName = 'ORG_ID'
      Origin = 'd.src_org_id'
    end
    object m_Q_mainORG_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'ORG_NAME'
      Origin = 'o.NAME'
      Size = 250
    end
    object m_Q_mainFISCAL_NUM: TFloatField
      DisplayLabel = '� ��'
      FieldName = 'FISCAL_NUM'
      Origin = 'zr.FISCAL_NUM'
    end
    object m_Q_mainREPORT_NUM: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'REPORT_NUM'
      Origin = 'zr.REPORT_NUM'
    end
    object m_Q_mainREPORT_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'REPORT_DATE'
      Origin = 'zr.REPORT_DATE'
    end
    object m_Q_mainREPORT_SUM: TFloatField
      DisplayLabel = '����� ������'
      FieldName = 'REPORT_SUM'
      Origin = 'zr.REPORT_SUM'
    end
  end
  object m_Q_tax: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name from taxpayer where id =150')
    Macros = <>
    Left = 40
    Top = 200
    object m_Q_taxID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.TAXPAYER.ID'
    end
    object m_Q_taxNAME: TStringField
      DisplayLabel = '��������'
      FieldName = 'NAME'
      Origin = 'name'
      Size = 100
    end
  end
  object m_Q_param: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name from taxpayer where id =150')
    UpdateObject = m_U_param
    Macros = <>
    Left = 104
    Top = 200
    object m_Q_paramID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.TAXPAYER.ID'
    end
    object m_Q_paramNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.TAXPAYER.NAME'
      Size = 100
    end
  end
  object m_U_param: TMLUpdateSQL
    IgnoreRowsAffected = True
    Left = 168
    Top = 200
  end
end
