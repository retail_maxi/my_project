//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmDocRtlCouponDisc.h"
#include "TSErrors.h"
#include "Barcode.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//--------------------------------------------------------------------------

__fastcall TDDocRtlCouponDisc::TDDocRtlCouponDisc(TComponent* p_owner, AnsiString p_prog_id)
  : TTSDDocumentWithLines(p_owner,p_prog_id)
{
  SetBeginEndDate(int(GetSysDate()),int(GetSysDate()));
}
//---------------------------------------------------------------------------

__fastcall TDDocRtlCouponDisc::~TDDocRtlCouponDisc()
{

  m_Q_orgs->Close();
}
//---------------------------------------------------------------------------


void __fastcall TDDocRtlCouponDisc::m_Q_nmclsBeforeOpen(TDataSet *DataSet)
{
//
}

//---------------------------------------------------------------------------

void __fastcall TDDocRtlCouponDisc::SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date)
{
  if ((int(m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_list->ParamByName("END_DATE")->AsDateTime) == int(p_end_date))) return;

  m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_list->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  RefreshListDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TDDocRtlCouponDisc::m_Q_itemAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_itemAfterInsert(DataSet);

  m_Q_itemDOC_DATE->AsDateTime = int(GetSysDate());
  m_Q_itemDOC_NUMBER->AsInteger = StrToIntDef(GetDocNumber(DocTypeId),0);
}

//---------------------------------------------------------------------------

void __fastcall TDDocRtlCouponDisc::m_Q_linesBeforeOpen(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_linesBeforeOpen(DataSet);
}
//---------------------------------------------------------------------------

void __fastcall TDDocRtlCouponDisc::m_Q_assortBeforeOpen(TDataSet *DataSet)
{
  m_Q_assort->ParamByName("NMCL_ID")->AsInteger = m_Q_lineNMCL_ID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDDocRtlCouponDisc::m_Q_lineAfterInsert(TDataSet *DataSet)
{
  TTSDDocumentWithLines::m_Q_lineAfterInsert(DataSet);
}
//---------------------------------------------------------------------------
void __fastcall TDDocRtlCouponDisc::m_Q_orgs_nameBeforeOpen(
      TDataSet *DataSet)
{
   m_Q_orgs_name->ParamByName("DOC_ID")->AsInteger = m_Q_itemID->AsInteger;   
}
//---------------------------------------------------------------------------

