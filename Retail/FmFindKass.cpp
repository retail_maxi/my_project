//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmFindKass.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFFindKass::TFFindKass(TComponent* p_owner,
                                  TTSDOperation *p_dm_operation): TTSFOperation(p_owner,
                                                                                p_dm_operation),
                                                                  m_dm(static_cast<TDFindKass*>(DMOperation))
{
  m_DTP_check_date->Date = int(m_dm->GetSysDate());
}
//---------------------------------------------------------------------------

void __fastcall TFFindKass::m_ACT_find_checkExecute(TObject *Sender)
{
  if(m_dm->m_Q_list->Active) m_dm->m_Q_list->Close();

  m_dm->m_Q_list->ParamByName("CHECK_DATE")->AsDateTime = (int)m_DTP_check_date->Date;
  m_dm->m_Q_list->ParamByName("CHECK_NUM")->AsString = m_E_check_num->Text;
  m_dm->m_Q_list->ParamByName("FISCAL_NUM")->AsString = m_E_fiscal_num->Text;

  m_dm->m_Q_list->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFFindKass::m_ACT_find_checkUpdate(TObject *Sender)
{
  m_ACT_find_check->Enabled = !m_E_check_num->Text.IsEmpty() &&
                              !m_E_fiscal_num->Text.IsEmpty();
}
//---------------------------------------------------------------------------

void __fastcall TFFindKass::m_ACT_open_saleExecute(TObject *Sender)
{
  TTSDocumentControl *m_doc = new  TTSDocumentControl("TSRetail.RtlSales");
  try
  {
    if( m_doc->CanExecuteView ) m_doc->ExecuteView(m_dm->m_Q_listSALE_DOC_ID->AsInteger);
  }
  __finally
  {
    if( m_doc ) delete m_doc;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFFindKass::m_ACT_open_saleUpdate(TObject *Sender)
{
  m_ACT_open_sale->Enabled = !m_dm->m_Q_listSALE_DOC_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFFindKass::m_DBG_listDblClick(TObject *Sender)
{
  TFFindKass::m_ACT_open_saleExecute(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TFFindKass::m_ACT_load_excelExecute(TObject *Sender)
{
  //
  if (m_OP_file->Execute()) {
    String path = m_OP_file->FileName;
    Variant cnn;
    try {
      m_dm->m_Q_tmp_check_del->ExecSQL();
      cnn = CreateOleObject("EXCEL.Application");
    }
    catch (...) {
      MessageBox(0, "������ ��� �������� ������� excel","������", MB_OK);
      return;
    }
    cnn.OlePropertyGet("Workbooks").OlePropertyGet("Open",path.c_str());

    AnsiString cell_data;
    int rec_count = 0;
    int check_num  = 0;
    int fiscal_num = 0;
    AnsiString  check_date = "";
    int error_qnt = 0;
    AnsiString t_customer = "";

    for (int i=2; i<65000; i++) {
      try {
        fiscal_num = cnn.OlePropertyGet("Cells",i,1);
        check_num  = cnn.OlePropertyGet("Cells",i,2);
        check_date = cnn.OlePropertyGet("Cells",i,3);
        error_qnt  = cnn.OlePropertyGet("Cells",i,4);
        t_customer = cnn.OlePropertyGet("Cells",i,5);

//        if (m_dm->m_Q_check_org->Active) m_dm->m_Q_check_org->Close();
//        m_dm->m_Q_check_org->ParamByName("ORG_ID")->AsInteger = org_id;
//        m_dm->m_Q_check_org->Open();
      }
      catch (...) {
        ShowMessage("������ ���� ������. ���������� "+IntToStr(rec_count)+" �������.");
        cnn.OleProcedure("Quit");
        return;
      }
      if (fiscal_num == 0 && check_num == 0) {
        cnn.OleProcedure("Quit");
        ShowMessage("���������� "+IntToStr(rec_count)+" �������.");
        m_dm->RefreshListDataSets();
        return;
      }
      else {
          m_dm->m_Q_tmp_check_ins->ParamByName("id")->AsInteger = i-1;
          m_dm->m_Q_tmp_check_ins->ParamByName("fiscal_num")->AsInteger = fiscal_num;
          m_dm->m_Q_tmp_check_ins->ParamByName("check_num")->AsInteger = check_num;
          m_dm->m_Q_tmp_check_ins->ParamByName("check_date")->AsString = check_date;
          m_dm->m_Q_tmp_check_ins->ParamByName("error_qnt")->AsInteger = error_qnt;
          m_dm->m_Q_tmp_check_ins->ParamByName("t_customer")->AsString = t_customer;
          try {
            m_dm->m_Q_tmp_check_ins->ExecSQL();
            rec_count++;
          }
          catch (...) {
            ShowMessage("������ ��� ��������. ���������� "+IntToStr(rec_count)+" �������.");
            cnn.OleProcedure("Quit");
            return;
          }
      }
    }
    cnn.OleProcedure("Quit");
  }
  else ShowMessage("���� �� ��� ������.");
}
//---------------------------------------------------------------------------


