//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmShowAddressList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFShowAddressList::TFShowAddressList(TComponent* Owner,
                                                TTSDDocumentWithLines *p_dm)
    : TForm(Owner),
    m_dm(static_cast<TDIncome*>(p_dm))
{
}
//---------------------------------------------------------------------------

void __fastcall TFShowAddressList::m_BBTN_closeClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TFShowAddressList::FormShow(TObject *Sender)
{
    if (m_dm->m_Q_address_list->Active ) m_dm->m_Q_address_list->Close();
    m_dm->m_Q_address_list->Open();    
}
//---------------------------------------------------------------------------

void __fastcall TFShowAddressList::m_ACT_saveExecute(TObject *Sender)
{
    m_dm->ChangeAddr();
}
//---------------------------------------------------------------------------

