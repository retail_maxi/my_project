//---------------------------------------------------------------------------

#ifndef DmAlcoJournalH
#define DmAlcoJournalH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMOPERATION.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
#include <Registry.hpp>
#include "MLFuncs.h"
//---------------------------------------------------------------------------
class TDAlcoJournal : public TTSDOperation
{
__published:
        TMLQuery *m_Q_taxpayer;
        TMLQuery *m_Q_taxpayer_fict;
        TMLUpdateSQL *m_U_taxpayer_fict;
        TFloatField *m_Q_taxpayerID;
        TStringField *m_Q_taxpayerNAME;
        TFloatField *m_Q_taxpayer_fictID;
        TStringField *m_Q_taxpayer_fictNAME;
        TMLQuery *m_Q_org;
        TStringField *m_Q_orgNAME;
        TStoredProc *m_SP_fill_tmp_alco_class_item_v;
        TFloatField *m_Q_mainNUM;
        TStringField *m_Q_mainCOD_NAME_INP;
        TFloatField *m_Q_mainNMCL_ID_INP;
        TStringField *m_Q_mainNMCL_NAME_INP;
        TStringField *m_Q_mainCODE_INP;
        TStringField *m_Q_mainSENDER_NAME;
        TStringField *m_Q_mainSENDER_INN;
        TDateTimeField *m_Q_mainTTN_DATE;
        TStringField *m_Q_mainTTN_NUMBER;
        TFloatField *m_Q_mainCAP_INP;
        TFloatField *m_Q_mainQNT_INP;
        TFloatField *m_Q_mainSUM_INP;
        TStringField *m_Q_mainMARK_CH;
        TDateTimeField *m_Q_mainDATE_CH;
        TStringField *m_Q_mainCODE_CH;
        TStringField *m_Q_mainCOD_NAME_CH;
        TFloatField *m_Q_mainNMCL_ID_CH;
        TStringField *m_Q_mainNMCL_NAME_CH;
        TFloatField *m_Q_mainCAP_CH;
        TFloatField *m_Q_mainQNT_CH;
        TFloatField *m_Q_mainSUM_CH;
        TStoredProc *m_SP_fill_buf_alco_jour;
        TFloatField *m_Q_mainDOC_ID_INC;
        TFloatField *m_Q_mainDOC_ID_CH;
        void __fastcall m_Q_mainBeforeOpen(TDataSet *DataSet);
private:
    TDateTime __fastcall GetBeginDate();
    TDateTime __fastcall GetEndDate();
public:
    int m_gen;
    __fastcall TDAlcoJournal(TComponent* p_owner, AnsiString p_prog_id);
    __fastcall ~TDAlcoJournal();
    int m_home_org_id;
    void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);
  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};

};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
 