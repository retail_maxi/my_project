//---------------------------------------------------------------------------

#ifndef FmBillLineH
#define FmBillLineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFmDocumentLine.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include "DmBill.h"
//---------------------------------------------------------------------------
class TFBillLine : public TTSFDocumentLine
{
__published:
private:
  TDBill* m_dm;
public:
  __fastcall TFBillLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm)
                       : TTSFDocumentLine(p_owner,p_dm),
                       m_dm(static_cast<TDBill*>(p_dm))
                       {}
};
//---------------------------------------------------------------------------
#endif
