//---------------------------------------------------------------------------

#ifndef FmRtlReturnsRecondH
#define FmRtlReturnsRecondH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "TSFMSUBITEM.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DmRtlReturns.h"
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFRtlReturnsRecond : public TTSFSubItem
{
__published:	// IDE-managed Components
        TLabel *m_L_price;
        TLabel *m_L_qnt;
   TDataSource *m_DS_item;
   TLabel *Label1;
   TComboBox *m_CB_cash_error;
   TComboBox *m_CB_decision_return;
   TComboBox *m_CB_is_check;
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
   void __fastcall m_CB_cash_errorChange(TObject *Sender);
   void __fastcall m_CB_decision_returnChange(TObject *Sender);
   void __fastcall m_CB_is_checkChange(TObject *Sender);
private:	// User declarations
  ETSUpdateRegime m_regime;
  TDRtlReturns *m_dm;
  TWinControl *m_start_win_control;
  void __fastcall BeforeApply(TObject *Sender);
  AnsiString m_is_check;
  AnsiString m_cash_error;
  AnsiString m_decision_return;
public:		// User declarations
        __fastcall TFRtlReturnsRecond(TComponent* p_owner,
                             TDRtlReturns *p_dm_doc_with_ln,
                             ETSUpdateRegime p_regime);
         __fastcall ~TFRtlReturnsRecond(){};
};
//---------------------------------------------------------------------------
#endif
 