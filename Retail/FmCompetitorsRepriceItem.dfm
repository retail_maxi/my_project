inherited FCompetitorsRepriceItem: TFCompetitorsRepriceItem
  Left = 285
  Top = 275
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_header: TPanel
    Height = 79
    object m_L_doc_number: TLabel
      Left = 15
      Top = 17
      Width = 66
      Height = 13
      Caption = '�������� �'
      FocusControl = m_DBE_DOC_NUMBER
    end
    object m_L_org: TLabel
      Left = 347
      Top = 17
      Width = 52
      Height = 13
      Caption = '��� �����'
    end
    object m_L_note: TLabel
      Left = 18
      Top = 45
      Width = 61
      Height = 13
      Caption = '����������'
      FocusControl = m_DBE_note
    end
    object m_L_doc_date: TLabel
      Left = 178
      Top = 16
      Width = 12
      Height = 13
      Caption = '��'
    end
    object m_DBE_DOC_NUMBER: TMLDBPanel
      Left = 87
      Top = 13
      Width = 77
      Height = 21
      DataField = 'DOC_NUMBER'
      DataSource = m_DS_item
      Alignment = taRightJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 0
    end
    object m_DBE_ISSUED_DATE: TMLDBPanel
      Left = 207
      Top = 13
      Width = 113
      Height = 21
      DataField = 'ISSUED_DATE'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 1
    end
    object m_DBE_note: TDBEdit
      Left = 86
      Top = 41
      Width = 626
      Height = 21
      DataField = 'NOTE'
      DataSource = m_DS_item
      TabOrder = 3
    end
    object m_DBE_org: TMLDBPanel
      Left = 405
      Top = 13
      Width = 306
      Height = 21
      DataField = 'ORG_NAME'
      DataSource = m_DS_item
      Alignment = taRightJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 2
    end
  end
  inherited m_DBG_lines: TMLDBGrid
    Top = 105
    Height = 311
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    OnColEnter = m_DBG_linesColEnter
    OnDblClick = m_DBG_linesDblClick
    UseMultiTitle = True
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'LINE_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MU_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'IN_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'OUT_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NEW_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_lines_control: TPanel
    inherited m_TB_lines_control_button: TToolBar
      ButtonWidth = 97
      inherited m_SBTN_append: TSpeedButton
        Visible = False
      end
    end
  end
  inherited m_DS_lines: TDataSource
    DataSet = DCompetitorsReprice.m_Q_lines
  end
  inherited m_DS_item: TDataSource
    DataSet = DCompetitorsReprice.m_Q_item
  end
end
