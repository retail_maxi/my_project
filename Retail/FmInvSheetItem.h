//---------------------------------------------------------------------------

#ifndef FmInvSheetItemH
#define FmInvSheetItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmInvSheet.h"
#include "FmInvSheetLine.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DLBCScaner.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFInvSheetItem : public TTSFDocumentWithLines
{
__published:
  TLabel *m_L_doc_number;
  TMLDBPanel *m_DBP_doc_number;
  TLabel *m_L_doc_date;
  TMLDBPanel *m_DBP_doc_date;
  TLabel *m_L_act;
  TMLLovListView *m_LLV_acts;
  TDataSource *m_DS_acts;
  TMLLov *m_LOV_acts;
  TLabel *m_L_note;
  TDBEdit *m_DBE_note;
  TDLBCScaner *m_BCS_item;
        TToolBar *m_TB_dop;
        TSpeedButton *m_SBTN_fill_lines;
        TAction *m_ACT_fill_lines;
        TSpeedButton *m_SBTN_del_dubl;
        TAction *m_ACT_del_dubl;
  void __fastcall m_BCS_itemBegin(TObject *Sender);
  void __fastcall m_BCS_itemEnd(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_fill_linesExecute(TObject *Sender);
        void __fastcall m_ACT_fill_linesUpdate(TObject *Sender);
        void __fastcall m_ACT_del_dublExecute(TObject *Sender);
        void __fastcall m_ACT_del_dublUpdate(TObject *Sender);
private:
  TDInvSheet* m_dm;
  TMLGridOptions m_temp_grid_options;
  AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
  TS_FORM_LINE_SHOW(TFInvSheetLine)
public:
  __fastcall TFInvSheetItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm)
                           : TTSFDocumentWithLines(p_owner,p_dm),
                           m_dm(static_cast<TDInvSheet*>(p_dm))
                           {};
};
//---------------------------------------------------------------------------
#endif
