//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmUnprocessDocument.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFUnprocessDocument::TFUnprocessDocument(TComponent* p_owner, TTSDOperation *p_dm_operation)
        :TTSFOperation(p_owner,p_dm_operation),
         m_dm(static_cast<TDUnprocessDocument*>(DMOperation))
{
}
//---------------------------------------------------------------------------
void __fastcall TFUnprocessDocument::FormShow(TObject *Sender)
{
   TTSFOperation::FormShow(Sender);
   m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
   m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
}
//---------------------------------------------------------------------------



void __fastcall TFUnprocessDocument::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                        m_ACT_set_begin_end_date->EndDate);        
}
//---------------------------------------------------------------------------




