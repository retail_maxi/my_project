inherited FBillItem: TFBillItem
  Left = 564
  Top = 240
  Width = 1281
  Height = 710
  Caption = 'FBillItem'
  Constraints.MinWidth = 800
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 580
    Width = 1265
    Height = 40
    AutoSize = False
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 1168
      Height = 40
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 622
      Width = 546
      Height = 40
      object m_BBTN_reason: TButton
        Left = 291
        Top = 2
        Width = 100
        Height = 25
        Action = m_ACT_reason
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        TabOrder = 2
      end
      object m_BBTN_replace: TButton
        Left = 391
        Top = 2
        Width = 155
        Height = 25
        Action = m_ACT_replace
        TabOrder = 1
      end
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 428
      Height = 40
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 653
    Width = 1265
  end
  inherited m_P_main_top: TPanel
    Width = 1265
    inherited m_P_tb_main: TPanel
      Width = 1218
      inherited m_TB_main: TToolBar
        Width = 1218
        object ToolButton1: TToolButton
          Left = 335
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 18
          Style = tbsSeparator
        end
        object m_CB_view_only_vld_arp: TCheckBox
          Left = 343
          Top = 2
          Width = 201
          Height = 24
          Caption = '�������� � ������ ���. ���'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = m_CB_view_only_vld_arpClick
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 1218
    end
  end
  inherited m_P_header: TPanel
    Width = 1265
    Height = 213
    object m_L_doc_number: TLabel
      Left = 71
      Top = 12
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = '���� �'
    end
    object m_L_doc_date: TLabel
      Left = 200
      Top = 12
      Width = 11
      Height = 13
      Caption = '��'
    end
    object m_L_on_date: TLabel
      Left = 304
      Top = 12
      Width = 37
      Height = 13
      Caption = '�� ����'
    end
    object m_L_src_org: TLabel
      Left = 464
      Top = 12
      Width = 11
      Height = 13
      Caption = '��'
    end
    object m_L_buh_doc_number: TLabel
      Left = 37
      Top = 69
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = '���. ��������'
      FocusControl = m_DBP_buh_doc_number
    end
    object m_L_buh_doc_date: TLabel
      Left = 200
      Top = 69
      Width = 11
      Height = 13
      Caption = '��'
      FocusControl = m_DBP_buh_doc_date
    end
    object m_L_buh_doc_id: TLabel
      Left = 300
      Top = 69
      Width = 11
      Height = 13
      Caption = 'ID'
      FocusControl = m_DBP_buh_doc_id
    end
    object m_L_buh_doc_amount: TLabel
      Left = 408
      Top = 69
      Width = 49
      Height = 13
      Caption = '�� �����'
      FocusControl = m_DBP_buh_doc_amount
    end
    object m_L_contract: TLabel
      Left = 568
      Top = 69
      Width = 44
      Height = 13
      Caption = '�������'
      FocusControl = m_DBP_buh_doc_amount
    end
    object m_L_buh_doc_cnt: TLabel
      Left = 50
      Top = 97
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = '���������'
      FocusControl = m_DBP_buh_doc_cnt
    end
    object m_L_mngr_note: TLabel
      Left = 55
      Top = 125
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = '��������'
      FocusControl = m_DBP_mngr_note
    end
    object m_L_buh_doc_note: TLabel
      Left = 14
      Top = 153
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = '���������� � ���.'
      FocusControl = m_DBP_buh_doc_note
    end
    object m_L_amount: TLabel
      Left = 74
      Top = 181
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = '�����'
    end
    object m_L_note: TLabel
      Left = 251
      Top = 181
      Width = 63
      Height = 13
      Caption = '����������'
    end
    object Label1: TLabel
      Left = 604
      Top = 12
      Width = 9
      Height = 13
      Caption = '� '
    end
    object m_L_info: TLabel
      Left = 17
      Top = 40
      Width = 91
      Height = 13
      Alignment = taRightJustify
      Caption = '���. ����������'
      FocusControl = m_DBP_info
    end
    object m_DBP_doc_number: TMLDBPanel
      Left = 112
      Top = 8
      Width = 81
      Height = 21
      DataField = 'DOC_NUMBER'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
    end
    object m_DBP_doc_date: TMLDBPanel
      Left = 216
      Top = 8
      Width = 81
      Height = 21
      DataField = 'DOC_DATE'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object m_DBP_on_date: TMLDBPanel
      Left = 352
      Top = 8
      Width = 81
      Height = 21
      DataField = 'ON_DATE'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 2
    end
    object m_DBP_src_org: TMLDBPanel
      Left = 480
      Top = 8
      Width = 121
      Height = 21
      DataField = 'SRC_ORG_NAME'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 3
    end
    object m_DBP_buh_doc_number: TMLDBPanel
      Left = 112
      Top = 65
      Width = 81
      Height = 21
      DataField = 'ACC_DOC_NUMBER'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 4
    end
    object m_DBP_buh_doc_date: TMLDBPanel
      Left = 216
      Top = 65
      Width = 81
      Height = 21
      DataField = 'ACC_DOC_DATE'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 5
    end
    object m_DBP_buh_doc_id: TMLDBPanel
      Left = 312
      Top = 65
      Width = 89
      Height = 21
      DataField = 'ACC_DOC_ID'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 6
    end
    object m_DBP_buh_doc_amount: TMLDBPanel
      Left = 464
      Top = 65
      Width = 89
      Height = 21
      DataField = 'ACC_DOC_AMOUNT'
      DataSource = m_DS_item
      Alignment = taRightJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 7
    end
    object m_DBP_contract: TMLDBPanel
      Left = 616
      Top = 65
      Width = 164
      Height = 21
      DataField = 'CONTRACT_NOTE'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 8
    end
    object m_DBP_buh_doc_cnt: TMLDBPanel
      Left = 112
      Top = 93
      Width = 669
      Height = 21
      DataField = 'CNT_NAME'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 9
    end
    object m_DBP_mngr_note: TMLDBPanel
      Left = 112
      Top = 121
      Width = 669
      Height = 21
      DataField = 'MNGR_NOTE'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 10
    end
    object m_DBP_buh_doc_note: TMLDBPanel
      Left = 112
      Top = 149
      Width = 669
      Height = 21
      DataField = 'ACC_NOTE'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 11
    end
    object m_DBP_amount: TMLDBPanel
      Left = 112
      Top = 177
      Width = 97
      Height = 21
      DataField = 'AMOUNT'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 12
    end
    object m_DBE_note: TDBEdit
      Left = 320
      Top = 177
      Width = 461
      Height = 21
      DataField = 'NOTE'
      DataSource = m_DS_item
      TabOrder = 13
    end
    object m_BBTN_get_acc_doc: TBitBtn
      Left = 796
      Top = 64
      Width = 55
      Height = 33
      Action = m_ACT_get_acc_doc
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 14
    end
    object MLDBPanel1: TMLDBPanel
      Left = 616
      Top = 8
      Width = 161
      Height = 21
      DataField = 'DEST_ORG_NAME'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 15
    end
    object m_DBP_info: TMLDBPanel
      Left = 112
      Top = 36
      Width = 667
      Height = 21
      DataField = 'DOP_INFO'
      DataSource = m_DS_item
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      ParentColor = False
      TabOrder = 16
    end
  end
  inherited m_DBG_lines: TMLDBGrid
    Top = 239
    Width = 1265
    Height = 341
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    UseMultiTitle = True
    OnGetCellParams = m_DBG_linesGetCellParams
    Columns = <
      item
        FieldName = 'LINE_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MEAS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ASSORTMENT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ASSORTMENT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 150
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'QNT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'STORED_QNT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NON_STORED_QNT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DELTA_QNT'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 77
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'IN_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'AMOUNT_IN'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE_REASON'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_lines_control: TPanel
    Top = 620
    Width = 1265
    Visible = False
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_edit: TAction
      OnUpdate = m_ACT_appendUpdate
    end
    inherited m_ACT_delete: TAction
      OnUpdate = m_ACT_appendUpdate
    end
    inherited m_ACT_view: TAction
      OnUpdate = m_ACT_appendUpdate
    end
    object m_ACT_get_acc_doc: TAction
      Category = 'Bill'
      Caption = '...'
      Hint = '������� ������������� ��������'
      OnExecute = m_ACT_get_acc_docExecute
      OnUpdate = m_ACT_get_acc_docUpdate
    end
    object m_ACT_replace: TAction
      Category = 'Bill'
      Caption = '�������� � � ����  ���'
      ImageIndex = 19
      OnExecute = m_ACT_replaceExecute
      OnUpdate = m_ACT_replaceUpdate
    end
    object m_ACT_reason: TAction
      Category = 'Bill'
      Caption = '������� �������'
      OnExecute = m_ACT_reasonExecute
      OnUpdate = m_ACT_reasonUpdate
    end
  end
  inherited m_DS_lines: TDataSource
    DataSet = DBill.m_Q_lines
    Left = 32
    Top = 320
  end
  inherited m_DS_item: TDataSource
    DataSet = DBill.m_Q_item
    Left = 344
    Top = 16
  end
end
