inherited DDocRtlCouponDisc: TDDocRtlCouponDisc
  OldCreateOrder = False
  Left = 330
  Top = 166
  Height = 760
  Width = 875
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Connected = False
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      '      select'
      '          d.id as id,'
      '          d.src_org_id as doc_src_org_id,'
      '          o.name as doc_src_org_name,'
      '          d.create_date as doc_create_date,'
      '          d.comp_name as doc_create_comp_name,'
      '          d.author as doc_create_user,'
      '          d.modify_date as doc_modify_date,'
      '          d.modify_comp_name as doc_modify_comp_name,'
      '          d.modify_user as doc_modify_user,'
      '          d.status as doc_status,      '
      
        '          decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'�����' +
        '���������'#39','#39'������'#39') as doc_status_name,'
      '          d.status_change_date as doc_status_change_date,'
      '          d.fldr_id as doc_fldr_id,'
      '          f.name as doc_fldr_name,     '
      
        '          rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) as ' +
        'doc_fldr_right,'
      '          dcd.doc_number,'
      '          dcd.doc_date,'
      '          dcd.begin_date,'
      '          dcd.end_date,'
      
        '          trim(rpad(listagg(o2.id,'#39'; '#39') within group (order by d' +
        '.id),255)) as dest_org_id,'
      
        '          trim(rpad(listagg(o2.name,'#39'; '#39') within group (order by' +
        ' d.id),255)) as dest_org_name,'
      '          dcd.note'
      '      from'
      '          documents d,'
      '          folders f,'
      '          organizations o,'
      '          doc_rtl_coupon_disc dcd,'
      '          organizations o2,'
      '          doc_rtl_coupon_disc_orgs dcdo'
      '      '
      '      where'
      '         f.doc_type_id = :DOC_TYPE_ID'
      '         and f.id = d.fldr_id'
      '         and o.id = d.src_org_id'
      '         and fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '         and dcd.doc_id = d.id'
      '         and dcdo.org_id = o2.id'
      '         and dcdo.doc_id = dcd.doc_id'
      
        '         and dcd.doc_date between :BEGIN_DATE and :END_DATE + 1 ' +
        '- 1/2 + 4/3600'
      '%WHERE_CLAUSE'
      
        '      group by d.id, d.src_org_id,o.name, d.create_date,d.comp_n' +
        'ame,d.author,d.modify_date,d.modify_comp_name,'
      
        '          d.modify_user,d.status,d.status_change_date,d.fldr_id,' +
        'f.name,f.id,dcd.doc_number,dcd.doc_date,dcd.begin_date,dcd.end_d' +
        'ate,dcd.note'
      '%ORDER_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_CLAUSE'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ACCESS_MASK'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
        Value = 0d
      end>
    inherited m_Q_listID: TFloatField
      Origin = ''
    end
    inherited m_Q_listDOC_SRC_ORG_ID: TFloatField
      DisplayLabel = 'ORG_ID'
      Visible = True
    end
    inherited m_Q_listDOC_STATUS: TStringField
      Visible = True
    end
    inherited m_Q_listDOC_FLDR_ID: TFloatField
      Visible = True
    end
    inherited m_Q_listDOC_FLDR_RIGHT: TStringField
      Visible = True
    end
    object m_Q_listDOC_NUMBER: TFloatField
      DisplayLabel = '����� ���������'
      FieldName = 'DOC_NUMBER'
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'DOC_DATE'
    end
    object m_Q_listBEGIN_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'BEGIN_DATE'
    end
    object m_Q_listEND_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'END_DATE'
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_listDEST_ORG_ID: TStringField
      DisplayLabel = 'ID �����'
      FieldName = 'DEST_ORG_ID'
      Size = 255
    end
    object m_Q_listDEST_ORG_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'DEST_ORG_NAME'
      Size = 255
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'select'
      '    dcd.doc_id as id,'
      '    dcd.doc_number,'
      '    dcd.doc_date,'
      '    dcd.begin_date,'
      '    dcd.end_date, '
      '    dcd.note'
      'from'
      '    doc_rtl_coupon_disc dcd'
      'where'
      '    dcd.doc_id = :ID'
      '    and :ID = ('
      '        select'
      '            id'
      '        from'
      '            documents '
      '        where'
      '            id = :ID'
      '            and fldr_id = :FLDR_ID'
      '    )')
    object m_Q_itemDOC_NUMBER: TFloatField
      FieldName = 'DOC_NUMBER'
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemBEGIN_DATE: TDateTimeField
      FieldName = 'BEGIN_DATE'
    end
    object m_Q_itemEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_rtl_coupon_disc'
      'set'
      '  doc_date=:DOC_DATE,'
      '  doc_number=:DOC_NUMBER,'
      '  begin_date=:BEGIN_DATE,'
      '  end_date=:END_DATE,'
      '  note=:NOTE'
      'where'
      '  doc_id = :ID')
    InsertSQL.Strings = (
      'insert into doc_rtl_coupon_disc'
      '  (doc_id, doc_date, doc_number, begin_date, end_date, note)'
      'values'
      '  (:ID, :DOC_DATE, :DOC_NUMBER, :BEGIN_DATE,:END_DATE, :NOTE)'
      ' '
      ' ')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      '  prc_delete_doc_inc_link(:OLD_ID);'
      'END; '
      ' ')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select dcdi.doc_id as id,'
      '       dcdi.line_id,'
      '       dcdi.assort_id,'
      '       a.name as assort_name,'
      '       dcdi.nmcl_id, '
      '       ni.name as nmcl_name,'
      '       dcdi.coupon_num,'
      '       dcdi.bar_code,'
      '       dcdi.percent_disc,'
      '       dcdi.price_disc, '
      '       to_NUMBER(dcdi.limit_qnt) as limit_qnt'
      'from doc_rtl_coupon_disc_items dcdi,'
      '     nomenclature_items ni,'
      '     assortment a'
      'where dcdi.doc_id = :ID'
      'and ni.id=dcdi.nmcl_id'
      'and a.id=dcdi.assort_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
        Value = 0
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'dcdi.doc_id'
    end
    inherited m_Q_linesLINE_ID: TFloatField
      Origin = 'dcdi.line_id'
    end
    object m_Q_linesASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Origin = 'TSDBMAIN.DOC_RTL_COUPON_DISC_ITEMS.ASSORT_ID'
    end
    object m_Q_linesNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'TSDBMAIN.DOC_RTL_COUPON_DISC_ITEMS.NMCL_ID'
    end
    object m_Q_linesCOUPON_NUM: TFloatField
      DisplayLabel = '����� ������'
      FieldName = 'COUPON_NUM'
      Origin = 'TSDBMAIN.DOC_RTL_COUPON_DISC_ITEMS.COUPON_NUM'
    end
    object m_Q_linesBAR_CODE: TStringField
      DisplayLabel = '�����-���'
      FieldName = 'BAR_CODE'
      Origin = 'TSDBMAIN.DOC_RTL_COUPON_DISC_ITEMS.BAR_CODE'
      Size = 30
    end
    object m_Q_linesPERCENT_DISC: TFloatField
      DisplayLabel = '������� ������'
      FieldName = 'PERCENT_DISC'
      Origin = 'TSDBMAIN.DOC_RTL_COUPON_DISC_ITEMS.PERCENT_DISC'
    end
    object m_Q_linesASSORT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_linesLIMIT_QNT: TFloatField
      DisplayLabel = '����������� ����������'
      FieldName = 'LIMIT_QNT'
      Origin = 'dcdi.limit_qnt'
    end
    object m_Q_linesPRICE_DISC: TFloatField
      DisplayLabel = '���� �� ������'
      FieldName = 'PRICE_DISC'
      Origin = 'dcdi.price_disc'
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'select    dcdi.doc_id as id,'
      '             dcdi.line_id,'
      '             dcdi.assort_id,'
      '             a.name as assort_name,'
      '             dcdi.nmcl_id, '
      '             ni.name as nmcl_name,'
      '             dcdi.percent_disc, '
      '             dcdi.price_disc,'
      '             dcdi.coupon_num,'
      '             dcdi.bar_code,'
      '             TO_NUMBER(dcdi.limit_qnt) as limit_qnt'
      '      from doc_rtl_coupon_disc_items dcdi,'
      '     nomenclature_items ni,'
      '     assortment a'
      '      where dcdi.doc_id = :ID'
      '      and dcdi.line_id=:LINE_ID'
      '      and ni.id=dcdi.nmcl_id'
      '  and a.id=dcdi.assort_id')
    Left = 448
    inherited m_Q_lineID: TFloatField
      Origin = 'dcdi.doc_id'
    end
    inherited m_Q_lineLINE_ID: TFloatField
      Origin = 'dcdi.line_id'
    end
    object m_Q_lineASSORT_ID: TFloatField
      DisplayLabel = '�����������'
      FieldName = 'ASSORT_ID'
      Origin = 'dcdi.assort'
    end
    object m_Q_lineNMCL_ID: TFloatField
      DisplayLabel = '������������'
      FieldName = 'NMCL_ID'
      Origin = 'dcdi.nmcl_id'
    end
    object m_Q_linePERCENT_DISC: TFloatField
      DisplayLabel = '������� ������'
      FieldName = 'PERCENT_DISC'
      Origin = 'dcdi.percent_disc'
    end
    object m_Q_lineCOUPON_NUM: TFloatField
      DisplayLabel = '����� ������'
      FieldName = 'COUPON_NUM'
      Origin = 'dcdi.coupon_num'
    end
    object m_Q_lineBAR_CODE: TStringField
      DisplayLabel = '�����-���'
      FieldName = 'BAR_CODE'
      Origin = 'dcdi.bar_code'
      Size = 30
    end
    object m_Q_lineASSORT_NAME: TStringField
      FieldName = 'ASSORT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_lineLIMIT_QNT: TFloatField
      FieldName = 'LIMIT_QNT'
    end
    object m_Q_linePRICE_DISC: TFloatField
      DisplayLabel = '���� �� ������'
      FieldName = 'PRICE_DISC'
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      '  update doc_rtl_coupon_disc_items'
      '  set'
      '    nmcl_id = :NMCL_ID,'
      '    assort_id = nvl(:ASSORT_ID,7),'
      '    percent_disc=:PERCENT_DISC,'
      '    limit_qnt=:LIMIT_QNT, '
      '    bar_code=:BAR_CODE,'
      '    coupon_num=:COUPON_NUM,'
      '    price_disc = :PRICE_DISC'
      '  where'
      '    doc_id = :ID and line_id = :LINE_ID')
    InsertSQL.Strings = (
      'insert into doc_rtl_coupon_disc_items'
      
        '  (doc_id,line_id,nmcl_id,assort_id,coupon_num,bar_code, percent' +
        '_disc,limit_qnt,price_disc)'
      'values'
      
        '  (:ID,:LINE_ID,:NMCL_ID,nvl(:ASSORT_ID,7),:COUPON_NUM,:BAR_CODE' +
        ',:PERCENT_DISC,:LIMIT_QNT,:PRICE_DISC)')
    DeleteSQL.Strings = (
      
        'delete from doc_rtl_coupon_disc_items where doc_id = :OLD_ID and' +
        ' line_id = :OLD_LINE_ID')
    IgnoreRowsAffected = True
  end
  object m_Q_nmcl_list: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      '  ni.id as nmcl_id,'
      '  ni.name as nmcl_name'
      'from '
      '  nomenclature_items ni'
      '%WHERE_EXPRESSION'
      'order by ni.name'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 144
    Top = 340
    object m_Q_nmcl_listNMCL_ID: TFloatField
      DisplayLabel = '���'
      DisplayWidth = 15
      FieldName = 'NMCL_ID'
      Origin = 'NI.ID'
    end
    object m_Q_nmcl_listNMCL_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'NMCL_NAME'
      Origin = 'NI.NAME'
      Size = 150
    end
  end
  object m_Q_assort: TMLQuery
    BeforeOpen = m_Q_assortBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name from('
      'SELECT DISTINCT a.id AS id,'
      '       a.name AS name'
      'FROM nmcl_bar_codes nbc,'
      '     assortment a'
      'WHERE nbc.nmcl_id = :NMCL_ID'
      '  AND a.id = nbc.assortment_id'
      'UNION ALL'
      'SELECT id,'
      '             name'
      'FROM assortment'
      'WHERE id = 7)'
      '%WHERE_EXPRESSION'
      'ORDER BY name'
      ''
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 236
    Top = 340
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_assortID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_assortNAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 100
    end
  end
  object m_Q_orgs: TMLQuery
    BeforeOpen = m_Q_assortBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select o.id as org_id,'
      '       o.name as org_name'
      'from organizations o'
      'where o.enable = '#39'Y'#39
      '%WHERE_CLAUSE'
      'order by o.name'
      ''
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 40
    Top = 348
    object m_Q_orgsORG_ID: TFloatField
      FieldName = 'ORG_ID'
      Origin = 'o.id'
    end
    object m_Q_orgsORG_NAME: TStringField
      FieldName = 'ORG_NAME'
      Origin = 'o.name'
      Size = 250
    end
  end
  object m_Q_upd_date: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '   update doc_rtl_coupon_disc'
      '   set end_date = :end_date'
      '   where doc_id = :doc_id;'
      '   pkg_work.upd_doc_history(:doc_id, '#39'��������� ���� "��"'#39');'
      'end;'
      ' ')
    Macros = <>
    Left = 336
    Top = 339
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'end_date'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'doc_id'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'doc_id'
        ParamType = ptUnknown
      end>
  end
  object m_Q_set_date: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select end_date from doc_rtl_coupon_disc'
      'where doc_id = :doc_id')
    UpdateObject = m_U_set_date
    Macros = <>
    Left = 416
    Top = 299
    ParamData = <
      item
        DataType = ftInteger
        Name = 'doc_id'
        ParamType = ptInput
      end>
    object m_Q_set_dateEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
      Origin = 'TSDBMAIN.DOC_RTL_COUPON_DISC.END_DATE'
    end
  end
  object m_U_set_date: TMLUpdateSQL
    IgnoreRowsAffected = True
    Left = 416
    Top = 355
  end
  object m_Q_orgs_name: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_orgs_nameBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select trim(rpad(listagg(o.name,'#39'; '#39') within group (order by o.n' +
        'ame),255)) as name_org'
      'from doc_rtl_coupon_disc_orgs dcco,'
      '     organizations o'
      'where o.id = dcco.org_id'
      '  and dcco.doc_id = :DOC_ID')
    Macros = <>
    Left = 40
    Top = 411
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_orgs_nameNAME_ORG: TStringField
      FieldName = 'NAME_ORG'
      Size = 255
    end
  end
end
