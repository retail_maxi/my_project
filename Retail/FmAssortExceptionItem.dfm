inherited FAssortExceptionItem: TFAssortExceptionItem
  Left = 746
  Top = 225
  Caption = 'FAssortExceptionItem'
  ClientHeight = 500
  ClientWidth = 609
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 450
    Width = 609
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 512
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 221
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 481
    Width = 609
  end
  inherited m_P_main_top: TPanel
    Width = 609
    inherited m_P_tb_main: TPanel
      Width = 562
      inherited m_TB_main: TToolBar
        Width = 562
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 562
    end
  end
  object m_P_nmcl: TPanel [3]
    Left = 0
    Top = 26
    Width = 609
    Height = 424
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object m_L_nmcl: TLabel
      Left = 40
      Top = 20
      Width = 60
      Height = 13
      Caption = '���������:*'
      FocusControl = m_LV_nmcl
    end
    object m_L_subcat: TLabel
      Left = 25
      Top = 56
      Width = 75
      Height = 13
      Caption = '������������:'
      FocusControl = m_LV_subcat
    end
    object Label2: TLabel
      Left = 7
      Top = 88
      Width = 93
      Height = 13
      Caption = '���������������:'
      FocusControl = m_LV_subsubcat
    end
    object m_L_1: TLabel
      Left = 73
      Top = 117
      Width = 26
      Height = 13
      Caption = '����:'
      FocusControl = m_LV_org_group
    end
    object m_LV_nmcl: TMLLovListView
      Left = 104
      Top = 16
      Width = 457
      Height = 21
      Lov = m_LOV_nmcl
      UpDownWidth = 457
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 0
      ParentColor = False
    end
    object m_LV_subcat: TMLLovListView
      Left = 104
      Top = 52
      Width = 457
      Height = 21
      Lov = m_LOV_subcat
      UpDownWidth = 457
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 1
      ParentColor = False
    end
    object m_LV_subsubcat: TMLLovListView
      Left = 104
      Top = 84
      Width = 457
      Height = 21
      Lov = m_LOV_subsubcat
      UpDownWidth = 457
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 2
      ParentColor = False
    end
    object m_LV_org_group: TMLLovListView
      Left = 104
      Top = 115
      Width = 457
      Height = 21
      Lov = m_LOV_org_group
      UpDownWidth = 457
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 3
      ParentColor = False
    end
    object m_P_1: TPanel
      Left = 0
      Top = 144
      Width = 609
      Height = 280
      Align = alBottom
      Caption = 'm_P_1'
      TabOrder = 4
      object m_PC_main: TPageControl
        Left = 1
        Top = 1
        Width = 607
        Height = 278
        ActivePage = m_TS_nmcl
        Align = alClient
        TabOrder = 0
        object m_TS_nmcl: TTabSheet
          Caption = 'SKU'
          object m_P_ctrl_nmcl: TPanel
            Left = 0
            Top = 223
            Width = 599
            Height = 27
            Align = alBottom
            AutoSize = True
            BevelOuter = bvNone
            TabOrder = 0
            object m_SB_add_module: TSpeedButton
              Left = 0
              Top = 0
              Width = 121
              Height = 27
              Action = m_ACT_edit_nmcl
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                1800000000000003000000000000000000000000000000000000FF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000FF00FFFF00FF000000C6C6C600848400848400848400848400
                8484008484008484008484008484008484008484000000FF00FFFF00FFFF00FF
                000000C6C6C600FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0084
                84000000FF00FFFF00FFFF00FFFF00FFFF00FF000000C6C6C600FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF008484000000FF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FF000000C6C6C600FFFF00FFFF00FFFF00FFFF008484000000FF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C6C6C600
                FFFF00FFFF008484000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FF000000C6C6C6008484000000FF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00
                0000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
            end
          end
          object m_DBG_nmcl: TMLDBGrid
            Left = 0
            Top = 0
            Width = 599
            Height = 223
            Align = alClient
            DataSource = m_DS_sku
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterColor = clWindow
            AutoFitColWidths = True
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
            Columns = <
              item
                FieldName = 'NMCL_ID'
                Title.TitleButton = True
                Footers = <>
              end
              item
                FieldName = 'NMCL_NAME'
                Title.TitleButton = True
                Width = 250
                Footers = <>
              end>
          end
        end
        object m_TS_org: TTabSheet
          Caption = '���'
          ImageIndex = 1
          object m_P_ctrl_org: TPanel
            Left = 0
            Top = 223
            Width = 599
            Height = 27
            Align = alBottom
            AutoSize = True
            BevelOuter = bvNone
            TabOrder = 0
            object m_SB_add_org: TSpeedButton
              Left = 0
              Top = 0
              Width = 121
              Height = 27
              Action = m_ACT_edit_org
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                1800000000000003000000000000000000000000000000000000FF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000FF00FFFF00FF000000C6C6C600848400848400848400848400
                8484008484008484008484008484008484008484000000FF00FFFF00FFFF00FF
                000000C6C6C600FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0084
                84000000FF00FFFF00FFFF00FFFF00FFFF00FF000000C6C6C600FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF008484000000FF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FF000000C6C6C600FFFF00FFFF00FFFF00FFFF008484000000FF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C6C6C600
                FFFF00FFFF008484000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FF000000C6C6C6008484000000FF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00
                0000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
            end
          end
          object m_DBG_org: TMLDBGrid
            Left = 0
            Top = 0
            Width = 599
            Height = 223
            Align = alClient
            DataSource = m_DS_orgs
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterColor = clWindow
            AutoFitColWidths = True
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
            Columns = <
              item
                FieldName = 'ORG_ID'
                Title.TitleButton = True
                Footers = <>
              end
              item
                FieldName = 'ORG_NAME'
                Title.TitleButton = True
                Width = 250
                Footers = <>
              end>
          end
        end
      end
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_edit_org: TAction
      Category = 'Item'
      Caption = '��������'
      ImageIndex = 12
      OnExecute = m_ACT_edit_orgExecute
      OnUpdate = m_ACT_edit_orgUpdate
    end
    object m_ACT_edit_nmcl: TAction
      Category = 'Item'
      Caption = '��������'
      OnExecute = m_ACT_edit_nmclExecute
      OnUpdate = m_ACT_edit_nmclUpdate
    end
  end
  inherited m_DS_item: TDataSource
    DataSet = DAssortException.m_Q_item
  end
  object m_DS_nmcl: TDataSource
    DataSet = DAssortException.m_Q_nmcls
    Left = 168
    Top = 34
  end
  object m_LOV_nmcl: TMLLov
    DataFieldKey = 'CAT_ID'
    DataFieldValue = 'CAT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'CAT_NAME'
    DataSourceList = m_DS_nmcl
    Mappings.Strings = (
      'SUBCAT_ID=SUBCAT_ID'
      'SUBCAT_NAME=SUBCAT_NAME'
      'SUBSUBCAT_ID=SUBSUBCAT_ID'
      'SUBSUBCAT_NAME=SUBSUBCAT_NAME')
    AutoOpenList = True
    OnAfterApply = m_LOV_nmclAfterApply
    OnAfterClear = m_LOV_nmclAfterClear
    Left = 200
    Top = 34
  end
  object m_LOV_subcat: TMLLov
    DataFieldKey = 'SUBCAT_ID'
    DataFieldValue = 'SUBCAT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_subcat
    Mappings.Strings = (
      'CAT_ID=CAT_ID'
      'CAT_NAME=CAT_NAME'
      'SUBSUBCAT_ID=SUBSUBCAT_ID'
      'SUBSUBCAT_NAME=SUBSUBCAT_NAME')
    AutoOpenList = True
    OnAfterApply = m_LOV_subcatAfterApply
    OnAfterClear = m_LOV_subcatAfterClear
    Left = 200
    Top = 74
  end
  object m_LOV_subsubcat: TMLLov
    DataFieldKey = 'SUBSUBCAT_ID'
    DataFieldValue = 'SUBSUBCAT_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_subsubcat
    Mappings.Strings = (
      'CAT_ID=CAT_ID'
      'CAT_NAME=CAT_NAME'
      'SUBCAT_ID=SUBCAT_ID'
      'SUBCAT_NAME=SUBCAT_NAME')
    AutoOpenList = True
    OnAfterApply = m_LOV_s
    Left = 200
    Top = 114
  end
  object m_DS_subcat: TDataSource
    DataSet = DAssortException.m_Q_subcats
    Left = 168
    Top = 74
  end
  object m_DS_subsubcat: TDataSource
    DataSet = DAssortException.m_Q_subsubcats
    Left = 168
    Top = 114
  end
  object m_DS_org_groups: TDataSource
    DataSet = DAssortException.m_Q_org_groups
    Left = 168
    Top = 146
  end
  object m_LOV_org_group: TMLLov
    DataFieldKey = 'ORG_GROUP_ID'
    DataFieldValue = 'ORG_GROUP_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_org_groups
    AutoOpenList = True
    Left = 200
    Top = 144
  end
  object m_DS_sku: TDataSource
    DataSet = DAssortException.m_Q_sku
    Left = 168
    Top = 242
  end
  object m_DS_orgs: TDataSource
    DataSet = DAssortException.m_Q_orgs
    Left = 240
    Top = 242
  end
end
