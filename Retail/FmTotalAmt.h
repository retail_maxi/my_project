//---------------------------------------------------------------------------

#ifndef FmTotalAmtH
#define FmTotalAmtH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLLovView.h"
#include "DmIncome.h"
#include "MLLov.h"
#include <Db.hpp>
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFmTotalAmt : public TTSFDialog
{
__published:	// IDE-managed Components
        TDataSource *m_DS_fict;
        TDBEdit *m_DBE_par;
        TLabel *Label1;
        void __fastcall m_ACT_apply_updatesExecute(TObject *Sender);
private:	// User declarations
       TDIncome* m_dm;
       double m_par;
public:		// User declarations
    __property double Par = {read=m_par};
    __fastcall TFmTotalAmt(TComponent* p_owner, TDIncome* p_dm);
    __fastcall ~TFmTotalAmt();
};
//---------------------------------------------------------------------------
#endif
