//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocDiscGoodsItem.h"
#include "TSFmDBEditDialog.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
AnsiString __fastcall TFDocDiscGoodsItem::BeforeItemApply(TWinControl **p_focused)
{
  if( m_dm->UpdateRegimeItem == urDelete ) return "";

  if (m_dm->m_Q_itemREASON_ID->AsString == "1" && m_dm->m_Q_itemPULL_DATE->IsNull)
  {
    *p_focused = m_DBDE_PULL_DATE;
    return "C �������� ������ '���������� �����' ���� '���� ��������' ����������� ��� ����������!";
  }

  if (m_dm->m_Q_itemBEGIN_DATE->AsDateTime > m_dm->m_Q_itemEND_DATE->AsDateTime)
  {
     *p_focused = m_DBDE_end;
     return "������ ������������ ������ �������� ������!";
  }

  if (m_DBCB_supp_comp->Checked==true && m_dm->m_Q_itemCNT_METHOD_COMP->IsNull)
  {
    *p_focused = m_DBE_cnt_method_comp;
     return "���� '�����/������ �����������' ����������� ��� ����������!";
  }

  return "";
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_editUpdate(TObject *Sender)
{
  m_ACT_edit->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_LOV_nmclsAfterApply(TObject *Sender)
{
  if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();

  m_dm->m_Q_itemASSORTMENT_ID->Clear();
  m_dm->m_Q_itemASSORTMENT_NAME->Clear();
  m_dm->m_Q_assorts->Close();
  m_dm->m_Q_assorts->Open();

  if (m_dm->m_Q_item->State != dsEdit)
     m_dm->m_Q_item->Edit();
   m_dm->m_Q_itemCNT_ID->Clear();
   m_dm->m_Q_itemCNT_NAME->Clear();
   m_dm->m_Q_cnt->Close();
   m_dm->m_Q_cnt->Open();
  if (m_dm->m_Q_cnt->RecordCount==1)
  {
    m_dm->m_Q_itemCNT_ID->AsInteger=m_dm->m_Q_cntCNT_ID->AsInteger;
    m_dm->m_Q_itemCNT_NAME->AsString=m_dm->m_Q_cntCNT_NAME->AsString;
    m_LV_cnt->Refresh();
  }
  m_dm->m_Q_cnt->Close();

  /*m_dm->m_Q_is_assorted->Close();
  m_dm->m_Q_is_assorted->Open();
  if (!m_dm->m_Q_is_assortedVAL->AsInteger)
  {
    m_dm->m_Q_itemASSORTMENT_ID->AsInteger = 7;
    m_dm->m_Q_itemASSORTMENT_NAME->Clear();
    m_LV_assorts->Enabled = false;
  }
  else
    m_LV_assorts->Enabled = true;
  m_dm->m_Q_is_assorted->Close(); */
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_LOV_nmclsAfterClear(TObject *Sender)
{
  if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();

  m_dm->m_Q_itemASSORTMENT_ID->Clear();
  m_dm->m_Q_itemASSORTMENT_NAME->Clear();
  //m_LV_assorts->Enabled = false;

}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::FormShow(TObject *Sender)
{
  TTSFDocumentWithLines::FormShow(Sender);
  /*m_dm->m_Q_is_assorted->Close();
  m_dm->m_Q_is_assorted->Open();
  if (!m_dm->m_Q_is_assortedVAL->AsInteger)
    m_LV_assorts->Enabled = false;
  else
    m_LV_assorts->Enabled = true;
  m_dm->m_Q_is_assorted->Close(); */

  m_CB_urgent_reprice->Checked = m_dm->m_Q_itemURGENT_REPRICE->AsInteger != 0;
  m_CB_urgent_reprice->Enabled = m_dm->UpdateRegimeItem != urView;

  m_DBCB_supp_comp->Enabled = m_dm->UpdateRegimeItem != urView;
  m_DBCB_supp_comp->Checked = m_dm->m_Q_itemSUPP_COMP->AsInteger != 0;

  ShowFldrMenu();
  ShowFldrMenuPrev();
  
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_LOV_actsAfterClear(TObject *Sender)
{
  if (m_dm->m_Q_item->State == dsBrowse) m_dm->m_Q_item->Edit();

}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_insertUpdate(TObject *Sender)
{
 m_ACT_insert->Enabled = m_dm->UpdateRegimeItem == urInsert;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_deleteExecute(TObject *Sender)
{
  if( Application->MessageBox("�������?",
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDNO ) return;

  if( m_DBG_rtt->SelectedRows->Count == 0 )
  {
    m_dm->m_Q_del_rtt->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_dm->m_Q_del_rtt->ParamByName("LINE_ID")->AsInteger = m_DBG_rtt->DataSource->DataSet->FieldByName("LINE_ID")->AsInteger;
    m_dm->m_Q_del_rtt->ExecSQL();

  }
  else
  for( int i = 0; i < m_DBG_rtt->SelectedRows->Count; i++ )
  {
      m_DBG_rtt->DataSource->DataSet->GotoBookmark((void *)m_DBG_rtt->SelectedRows->Items[i].c_str());
      m_dm->m_Q_del_rtt->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
      m_dm->m_Q_del_rtt->ParamByName("LINE_ID")->AsInteger = m_DBG_rtt->DataSource->DataSet->FieldByName("LINE_ID")->AsInteger;
      m_dm->m_Q_del_rtt->ExecSQL();
  }
  m_dm->ApplyUpdatesItem();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_add_all_rttExecute(
      TObject *Sender)
{
  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  m_dm->m_Q_ins_all_rtt->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
  m_dm->m_Q_ins_all_rtt->ExecSQL();
  m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_add_all_rttUpdate(TObject *Sender)
{
  m_ACT_append->Enabled = m_dm->UpdateRegimeItem != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_CB_urgent_repriceClick(
      TObject *Sender)
{
    if (m_dm->UpdateRegimeItem != urView)
    {
       m_dm->m_Q_item->Edit();
       m_dm->m_Q_itemURGENT_REPRICE->AsInteger = (int)m_CB_urgent_reprice->Checked;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_PM_fldr_prevPopup(TObject *Sender)
{
   ShowFldrMenuPrev();     
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_PM_fldr_nextPopup(TObject *Sender)
{
    ShowFldrMenu();
}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsItem::ShowFldrMenu()
{
    //���������� ������ �������� ����� �� ����������
    if (m_dm->m_Q_folders_next->Active)
        m_dm->m_Q_folders_next->Close();
    m_dm->m_Q_folders_next->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_PM_fldr_next->Items->Clear();
    m_dm->m_Q_folders_next->Open();

    while (!m_dm->m_Q_folders_next->Eof)
    {
       TMenuItem *NewItem = new TMenuItem(m_PM_fldr_next);
       NewItem->Caption = m_dm->m_Q_folders_nextNAME->AsString;
       NewItem->Tag = m_dm->m_Q_folders_nextID->AsInteger;
       NewItem->ImageIndex = 16;
       NewItem->OnClick = PMFldrNextClick;
       m_PM_fldr_next->Items->Add(NewItem);
       m_dm->m_Q_folders_next->Next();
    }
    m_RSB_fldr_next->Visible = ((m_dm->m_Q_folders_next->RecordCount > 0) &&
        (m_dm->m_Q_documentSTATUS->AsString != "D"));

   //��������� ���������� �������
   m_TB_main_control_buttons_custom->Left = 4;
   m_TB_next->Left = 3;
   m_TB_prev->Left = 2;
   m_TB_main_control_buttons_document->Left = 1;

    m_TB_next->AutoSize = false;
    m_TB_next->AutoSize = true;
    m_dm->m_Q_folders_next->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsItem::BeforeMoveRoute(long p_item_id,
                                              long fldr_id,
                                              const AnsiString &fldr_name,
                                              const AnsiString &fldr_right,
                                              long p_route_id,
                                              const AnsiString &p_route_name,
                                              const AnsiString &p_route_right,
                                              long p_dest_fldr_id,
                                              const AnsiString &p_dest_fldr_name,
                                              const AnsiString &p_dest_fldr_right,
                                              AnsiString *p_params)
{
  *p_params =  IntToStr(p_route_id);
}

//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::PMFldrNextClick(TObject *Sender)
{
  TMenuItem *ClickedItem = dynamic_cast<TMenuItem *>(Sender);
    if (ClickedItem)
    {
        if( m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();

        long item_id = m_dm->m_Q_itemID->AsInteger;
        long fldr_id = m_dm->m_Q_folderFLDR_ID->AsInteger;
        AnsiString fldr_name = m_dm->m_Q_folderFLDR_NAME->AsString;
        AnsiString fldr_right = m_dm->m_Q_folderFLDR_RIGHT->AsString;
        long route_id = ClickedItem->Tag;

        if ((route_id == getRouteId(m_dm->FldrNew, m_dm->FldrAct)) && ((m_dm->m_Q_linesMIN_MARGIN_AF_ACT->AsInteger <= -5) ||
                                  (m_dm->m_Q_linesMAX_TOTAL_PROFIT->AsInteger >= 10000))) route_id = getRouteId(m_dm->FldrNew, m_dm->FldrUtv);
        if ((route_id == getRouteId(m_dm->FldrSogl, m_dm->FldrAct)) && ((m_dm->m_Q_linesMIN_MARGIN_AF_ACT->AsInteger <= -5) ||
                                  (m_dm->m_Q_linesMAX_TOTAL_PROFIT->AsInteger >= 10000))) route_id = getRouteId(m_dm->FldrSogl, m_dm->FldrUtv);
        ClickedItem->Tag = route_id;
        AnsiString route_name = ClickedItem->Caption;
        AnsiString route_right = m_dm->m_Q_folderROUTE_NEXT_RIGHT->AsString;
        long dest_fldr_id = m_dm->m_Q_folderFLDR_NEXT_ID->AsInteger;
        AnsiString dest_fldr_name = m_dm->m_Q_folderFLDR_NEXT_NAME->AsString;
        AnsiString dest_fldr_right = m_dm->m_Q_folderFLDR_NEXT_RIGHT->AsString;
        AnsiString params = "";

        /*if (m_dm->m_Q_route->Active) m_dm->m_Q_route->Close();
        m_dm->m_Q_route->ParamByName("SRC_FLDR_ID")->AsInteger = m_dm->FldrNew;
        m_dm->m_Q_route->ParamByName("DEST_FLDR_ID")->AsInteger = m_dm->FldrCansel;
        m_dm->m_Q_route->Open();  */
        int route_cansel = getRouteId(m_dm->FldrNew, m_dm->FldrCansel);
        int route_sogl = getRouteId(m_dm->FldrNew, m_dm->FldrSogl);

        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_itemBEGIN_DATE->IsNull || m_dm->m_Q_itemEND_DATE->IsNull))
        {
            m_DBDE_begin->SetFocus();
            throw ETSError("������� ������ �������� ������!");
        }
        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && m_dm->m_Q_itemACTION_TYPE_ID->IsNull)
        {
           m_LV_acts->SetFocus();
           throw ETSError("������� ��� �����!");
        }
        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && m_dm->m_Q_itemNMCL_ID->IsNull)
        {
           m_LV_nmcls->SetFocus();
           throw ETSError("�������� �����!");
        }
        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && m_dm->m_Q_itemASSORTMENT_ID->IsNull)
        {
           m_LV_assorts->SetFocus();
           throw ETSError("�� ������ �����������!");
        }
        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && m_dm->m_Q_itemREASON_ID->IsNull)
        {
           m_LV_disc_reasons->SetFocus();
           throw ETSError("������� ������� ������!");
        }
        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_linesLINE_ID->IsNull || m_dm->m_Q_linesMAX_LINE_ID->AsInteger == 3))
        {
           throw ETSError("�� ������� ���, �� ������� ��������� ��������� ������!");
        }

        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && ClickedItem->Tag != route_sogl && m_dm->m_Q_itemACT_PRICE->IsNull)
        {
            m_DBE_act_price->SetFocus();
            throw ETSError("������� ���� �� �����!");
        }

         if (ClickedItem->Tag != route_cansel && ClickedItem->Tag != route_sogl  && !(m_dm->m_Q_itemPRICE_BEF_ACT->IsNull)/*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_itemACT_PRICE->AsFloat >= m_dm->m_Q_itemPRICE_BEF_ACT->AsFloat))
        {
            m_DBE_act_price->SetFocus();
            throw ETSError("���� �� ����� ������ ���� ������ ���� ��� �����!");
        }

         if (ClickedItem->Tag == route_sogl && !(m_dm->m_Q_itemPRICE_BEF_ACT->IsNull)/*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_itemACT_PRICE->AsFloat >= m_dm->m_Q_itemPRICE_BEF_ACT->AsFloat))
        {
            m_DBE_act_price->SetFocus();
            throw ETSError("���� �� ����� ������ ���� ������ ���� ��� �����!");
        }

        if (ClickedItem->Tag != route_cansel && ClickedItem->Tag != route_sogl  && !(m_dm->m_Q_itemPRICE_BEF_ACT_ON_CARD->IsNull)/*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_itemACT_PRICE->AsFloat >= m_dm->m_Q_itemPRICE_BEF_ACT_ON_CARD->AsFloat))
        {
            m_DBE_act_price->SetFocus();
            throw ETSError("���� �� ����� ������ ���� ������ ���� �� ����� �� �����!");
        }

         if (ClickedItem->Tag == route_sogl && !(m_dm->m_Q_itemPRICE_BEF_ACT_ON_CARD->IsNull)/*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_itemACT_PRICE->AsFloat >= m_dm->m_Q_itemPRICE_BEF_ACT_ON_CARD->AsFloat))
        {
            m_DBE_act_price->SetFocus();
            throw ETSError("���� �� ����� ������ ���� ������ ���� �� ����� �� �����!");
        }

        if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel /*m_dm->m_Q_routeROUTE_ID->AsInteger*/ && (m_dm->m_Q_itemBEGIN_DATE->AsDateTime > m_dm->m_Q_itemEND_DATE->AsDateTime))
        {
            m_DBDE_begin->SetFocus();
            throw ETSError("���� ��������� ����� ������ ���� ������ ���� ������!");
        }

        // ������ ����������� ���� ���������� � �����
        int route_new_act = getRouteId(m_dm->FldrNew, m_dm->FldrAct);
        int route_sogl_act = getRouteId(m_dm->FldrSogl, m_dm->FldrAct);
        int route_utv_act = getRouteId(m_dm->FldrUtv, m_dm->FldrAct);
        int route_new_sogl = getRouteId(m_dm->FldrNew, m_dm->FldrSogl);
        int route_new_utv = getRouteId(m_dm->FldrNew, m_dm->FldrUtv);
        if (route_id == route_new_act || route_id == route_sogl_act || route_id == route_utv_act || route_id == route_new_sogl || route_id == route_new_utv)
        {
           if (m_dm->m_Q_itemACT_PRICE->IsNull && (route_id != route_new_sogl || route_id == route_new_utv))
           {
              m_DBE_act_price->SetFocus();
              throw ETSError("�� ������� ���� �� �����!");
           }
           if (m_dm->m_Q_chk_docs->Active)  m_dm->m_Q_chk_docs->Close();
           m_dm->m_Q_chk_docs->Open();
           m_dm->m_Q_chk_docs->First();
           if (m_dm->m_Q_chk_docsC->AsInteger > 0)
           {
              if( Application->MessageBox(m_dm->m_Q_chk_docsNOTE->AsString.c_str(),
                                            Application->Title.c_str(),
                                            MB_ICONQUESTION | MB_YESNO) == IDNO )
              return;
           }
        }
        BeforeMoveRoute(item_id,
                    fldr_id,
                    fldr_name,
                    fldr_right,
                    route_id,
                    route_name,
                    route_right,
                    dest_fldr_id,
                    dest_fldr_name,
                    dest_fldr_right,
                    &params);

        // ������ ����������� ���� ���������� � �����
        int route_new_cans = getRouteId(m_dm->FldrNew, m_dm->FldrCansel);
        int route_sogl_cans = getRouteId(m_dm->FldrSogl, m_dm->FldrCansel);
        int route_utv_cans = getRouteId(m_dm->FldrUtv, m_dm->FldrCansel);
        if (route_id == route_new_cans || route_id == route_sogl_cans || route_id == route_utv_cans )
        {
          if (m_dm->m_Q_reason_rejection->Active) m_dm->m_Q_reason_rejection->Close();
          m_dm->m_Q_reason_rejection->Open();
          m_dm->m_Q_reason_rejection->Edit();

          if (TSExecDBEditDlg(this,"������� ������",m_dm->m_Q_reason_rejection,"NOTE",500))
          {
            if (m_dm->m_Q_reason_rejectionNOTE->AsString.Length() > 0)
            {
              m_dm->m_Q_edit_reason_rejection->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
              m_dm->m_Q_edit_reason_rejection->ParamByName("NOTE")->AsString = m_dm->m_Q_reason_rejectionNOTE->AsString;
              m_dm->m_Q_edit_reason_rejection->ExecSQL();
              m_dm->RefreshItemDataSets();
              //m_dm->ChangeFldr(ClickedItem->Tag,item_id,params);
            }
            else return;
          }
          else return;
        }
        // ������ ����������� ���� ���������� ������� �� ������������
        int route_utv_sogl = getRouteId(m_dm->FldrUtv, m_dm->FldrSogl);
        if (route_id == route_utv_sogl)
        {
          if (m_dm->m_Q_reject_zam_kd->Active) m_dm->m_Q_reject_zam_kd->Close();
          m_dm->m_Q_reject_zam_kd->Open();
          m_dm->m_Q_reject_zam_kd->Edit();

          if (TSExecDBEditDlg(this,"������� ����������",m_dm->m_Q_reject_zam_kd,"NOTE",500))
          {
            if (m_dm->m_Q_reject_zam_kdNOTE->AsString.Length() > 0)
            {
              m_dm->m_Q_edit_reject_zam_ld->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
              m_dm->m_Q_edit_reject_zam_ld->ParamByName("NOTE")->AsString = m_dm->m_Q_reject_zam_kdNOTE->AsString;
              m_dm->m_Q_edit_reject_zam_ld->ExecSQL();
              m_dm->RefreshItemDataSets();
              //m_dm->ChangeFldr(ClickedItem->Tag,item_id,params);
            }
            else return;
          }
          else return;
        }
        //�������� ������� ��� ��� �������� ��, ��, ����
        if (m_dm->m_Q_lines->Active)  m_dm->m_Q_lines->Close();
           m_dm->m_Q_lines->Open();
        int gm=0,sm_mini=0;
        while (!m_dm->m_Q_lines->Eof) {
          if (m_dm->m_Q_linesGM->AsInteger==1) gm=gm+1;
          if (m_dm->m_Q_linesSM_MINI->AsInteger==1) sm_mini=sm_mini+1;
          m_dm->m_Q_lines->Next();
          }
          //�������� ��� ��
           if (gm!=0 && ClickedItem->Tag != route_cansel) {

                 if (m_dm->m_Q_itemPRICE_BEF_ACT->IsNull)
                     {
                       m_DBE_out_price_nd->SetFocus();
                       throw ETSError("����� ��� ���� ��. ������� ���� ��� �����!");
                     }

                       }
           //�������� ��� ��, ����
           if (sm_mini!=0 && ClickedItem->Tag != route_cansel) {

                 if (m_dm->m_Q_itemPRICE_BEF_ACT_ON_CARD->IsNull)
                     {
                       m_DBE_out_price->SetFocus();
                       throw ETSError("����� ��� ���� �� ��� ����. ������� ���� �� ����� �� �����!");
                     }

                       }

        m_dm->ChangeFldr(ClickedItem->Tag,item_id,params);

        AfterMoveRoute(item_id,
                   fldr_id,
                   fldr_name,
                   fldr_right,
                   route_id,
                   route_name,
                   route_right,
                   dest_fldr_id,
                   dest_fldr_name,
                   dest_fldr_right);
        Close();
        m_dm->RefreshListDataSets(true);
    }   
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::ShowFldrMenuPrev()
{
    //���������� ������ �������� ����� �� ����������
    if (m_dm->m_Q_folders_prev->Active)
        m_dm->m_Q_folders_prev->Close();
    m_dm->m_Q_folders_prev->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_PM_fldr_prev->Items->Clear();
    m_dm->m_Q_folders_prev->Open();
   // if (!m_dm->m_Q_folders_prev->Eof)
   //     m_RSB_fldr_prev->Caption = m_dm->m_Q_folders_prevNAME->AsString;
    while (!m_dm->m_Q_folders_prev->Eof)
    {
       TMenuItem *NewItem = new TMenuItem(m_PM_fldr_prev);
       NewItem->Caption = m_dm->m_Q_folders_prevNAME->AsString;
       NewItem->Tag = m_dm->m_Q_folders_prevID->AsInteger;
       NewItem->ImageIndex = 15;
       NewItem->OnClick = PMFldrNextClick;
       m_PM_fldr_prev->Items->Add(NewItem);
       m_dm->m_Q_folders_prev->Next();
    }
    m_RSB_fldr_prev->Visible = ((m_dm->m_Q_folders_prev->RecordCount > 0) &&
        (m_dm->m_Q_documentSTATUS->AsString != "D"));

   /* if(m_dm->m_Q_folders_prev->RecordCount != m_PM_fldr_prev->Items->Count
       && m_PM_fldr_prev->Items->Count > 0)
    {
      m_RSB_fldr_prev->Caption = m_PM_fldr_prev->Items->Items[0]->Caption;
    }  */


   //��������� ���������� �������
   m_TB_main_control_buttons_custom->Left = 4;
   m_TB_next->Left = 3;
   m_TB_prev->Left = 2;
   m_TB_main_control_buttons_document->Left = 1;
   
    m_TB_prev->AutoSize = false;
    m_TB_prev->AutoSize = true;
    m_dm->m_Q_folders_prev->Close();
}
//---------------------------------------------------------------------------


void __fastcall TFDocDiscGoodsItem::m_ACT_view_nmcl_salesExecute(
      TObject *Sender)
{
    TMLParams p_params;
    p_params.InitParam("NMCL_ID",m_dm->m_Q_itemNMCL_ID->AsInteger);
    p_params.InitParam("ASSORT_ID",m_dm->m_Q_itemASSORTMENT_ID->AsInteger);
    p_params.InitParam("AGENT_ID",m_dm->m_Q_itemAGENT_ID->AsInteger);


    if (!m_view_nmcl_sales.CanExecute)
    {
       MessageDlg("��� ���� �� ��������!",
       mtConfirmation, TMsgDlgButtons() << mbOK, 0);
       return;
    }
    else m_view_nmcl_sales.Execute(true, p_params);
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_view_nmcl_salesUpdate(
      TObject *Sender)
{
      m_ACT_view_nmcl_sales->Enabled = m_view_nmcl_sales.CanExecute &&
                                     !m_dm->m_Q_itemNMCL_ID->IsNull;
}
//---------------------------------------------------------------------------
int __fastcall TFDocDiscGoodsItem::getRouteId(int p_src_fldr_id, int p_dest_fldr_id)

{
  if (m_dm->m_Q_route->Active) m_dm->m_Q_route->Close();
  m_dm->m_Q_route->ParamByName("SRC_FLDR_ID")->AsInteger = p_src_fldr_id;
  m_dm->m_Q_route->ParamByName("DEST_FLDR_ID")->AsInteger = p_dest_fldr_id;
  m_dm->m_Q_route->Open();
  return m_dm->m_Q_routeROUTE_ID->AsInteger;
}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsItem::m_ACT_deleteUpdate(TObject *Sender)
{
   m_ACT_delete->Enabled = m_dm->UpdateRegimeItem != urView && !m_dm->m_Q_linesLINE_ID->IsNull && m_dm->m_Q_linesLINE_ID->AsInteger != 3;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsItem::m_ACT_appendExecute(TObject *Sender)
{
   m_dm->ApplyUpdatesItem();
   TTSFDocumentWithLines::m_ACT_appendExecute(Sender);
}
//---------------------------------------------------------------------------



void __fastcall TFDocDiscGoodsItem::m_DBCB_supp_compClick(TObject *Sender)
{
 if (m_DBCB_supp_comp->Checked==true) {
   m_L_cnt->Visible=true;
   m_L_cnt_sale->Visible=true;
   m_L_cnt_price_comp->Visible=true;
   m_LV_cnt->Visible=true;
   m_L_cnt_comp_sale->Visible=true;
   m_L_cnt_method_comp->Visible=true;
   m_DBE_cnt_sale->Visible=true;
   m_DBE_cnt_price_comp->Visible=true;
   m_DBE_cnt_comp_sale->Visible=true;
   m_DBE_cnt_method_comp->Visible=true;
 }
  else  {
   m_L_cnt->Visible=false;
   m_L_cnt_sale->Visible=false;
   m_L_cnt_price_comp->Visible=false;
   m_LV_cnt->Visible=false;
   m_L_cnt_comp_sale->Visible=false;
   m_L_cnt_method_comp->Visible=false;
   m_DBE_cnt_sale->Visible=false;
   m_DBE_cnt_price_comp->Visible=false;
   m_DBE_cnt_comp_sale->Visible=false;
   m_DBE_cnt_method_comp->Visible=false;
  }
}
//---------------------------------------------------------------------------





void __fastcall TFDocDiscGoodsItem::m_DBG_rttGetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
     if ((m_dm->m_Q_linesIS_CURRENT_ACTION_ORG->AsInteger == 1) &&
      !State.Contains(gdSelected))
  {
    Background = clAqua;
    AFont->Color = clBlack;
  }
}
//---------------------------------------------------------------------------

