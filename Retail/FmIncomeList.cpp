//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmIncomeList.h"
#include "TSErrors.h"
#include "BarCode.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTLIST"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);

  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;

  m_CB_show_nmcls->Checked = m_dm->ShowNmcls;
  m_DBLCB_org->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_org->Enabled = false;
  else
    m_DBLCB_org->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_DS_fict_orgDataChange(TObject *Sender, TField *Field)
{
  //���� ��� ����� ���� ������ �� TTSFDocumentList, �� ������������� m_dm
  if (m_dm) m_dm->OrgId = m_dm->m_Q_orgsID->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_CB_show_nmclsClick(TObject *Sender)
{
  m_dm->ShowNmcls = m_CB_show_nmcls->Checked;
  m_DBG_nmcls->Visible = m_dm->ShowNmcls;
  m_S_h->Visible = m_dm->ShowNmcls;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_goto_docUpdate(TObject *Sender)
{
  m_ACT_goto_doc->Enabled = m_dm->ShowNmcls && !m_dm->m_Q_nmclsID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_goto_docExecute(TObject *Sender)
{
  if (!m_dm->m_Q_list->Locate(m_dm->m_Q_listID->FieldName,m_dm->m_Q_nmclsID->Value,TLocateOptions()))
    throw ETSError("�������� � ������ �� ������");
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_DBG_nmclsDblClick(TObject *Sender)
{
  if (m_ACT_goto_doc->Enabled) m_ACT_goto_doc->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_E_nmcl_idChange(TObject *Sender)
{
  //��� ����� ������ ������� ������ ���������
  if (!m_E_nmcl_name->Text.IsEmpty())
  {
    m_E_nmcl_name->OnChange = NULL;
    m_E_nmcl_name->Text = "";
    m_E_nmcl_name->OnChange = m_E_nmcl_nameChange;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_E_nmcl_nameChange(TObject *Sender)
{
  //��� ����� ������ ������� ������ ���������
  if (!m_E_nmcl_id->Text.IsEmpty())
  {
    m_E_nmcl_id->OnChange = NULL;
    m_E_nmcl_id->Text = "";
    m_E_nmcl_id->OnChange = m_E_nmcl_idChange;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_nmcl_filterExecute(TObject *Sender)
{
  //��������� ����������� ���������� � ��������
  int nmcl_id = 0;

  if (!m_E_nmcl_id->Text.IsEmpty())
  {
    nmcl_id = StrToIntDef(m_E_nmcl_id->Text.c_str(),0);
    if (nmcl_id <= 0) throw ETSError("�������� �������� ���� ������");
  }

  if (!nmcl_id)
  {
    m_dm->m_Q_list->ParamByName("NMCL_ID_FILTER")->Clear();
    m_dm->m_Q_nmcls->ParamByName("NMCL_ID_FILTER")->Clear();
  }
  else
  {
    m_dm->m_Q_list->ParamByName("NMCL_ID_FILTER")->AsInteger = nmcl_id;
    m_dm->m_Q_nmcls->ParamByName("NMCL_ID_FILTER")->AsInteger = nmcl_id;
  }

  if (m_E_nmcl_name->Text.IsEmpty())
  {
    m_dm->m_Q_list->ParamByName("NMCL_NAME_FILTER")->Clear();
    m_dm->m_Q_nmcls->ParamByName("NMCL_NAME_FILTER")->Clear();
  }
  else
  {
    m_dm->m_Q_list->ParamByName("NMCL_NAME_FILTER")->AsString = m_E_nmcl_name->Text;
    m_dm->m_Q_nmcls->ParamByName("NMCL_NAME_FILTER")->AsString = m_E_nmcl_name->Text;
  }

  //���������� ������
  m_dm->RefreshListDataSets();

  if (!m_dm->m_Q_list->RecordCount)
  {
    MessageDlg("��� �� ����� ������, ��������������� �������!",mtWarning,TMsgDlgButtons() << mbOK,0);
    m_dm->m_Q_list->ParamByName("NMCL_NAME_FILTER")->Clear();
    m_dm->m_Q_nmcls->ParamByName("NMCL_NAME_FILTER")->Clear();
    m_dm->RefreshListDataSets();
  }

  //����� �������� ��� ������
  m_ACT_nmcl_filter->ImageIndex =
    m_dm->m_Q_list->ParamByName("NMCL_ID_FILTER")->IsNull && m_dm->m_Q_list->ParamByName("NMCL_NAME_FILTER")->IsNull ?
    NmclFilterOffImgIdx :
    NmclFilterOnImgIdx;

  m_DBG_list->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_insertUpdate(TObject *Sender)
{
  if (m_dm->OrgId == m_dm->HomeOrgId)
     TTSFDocumentList::m_ACT_insertUpdate(Sender);
  else
     m_ACT_insert->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_editUpdate(TObject *Sender)
{
  if ((m_dm->m_Q_listDOC_SRC_ORG_ID->AsInteger == m_dm->HomeOrgId) ||
      (m_dm->m_Q_listDOC_FLDR_ID->AsInteger == m_dm->FldrChk))
     TTSFDocumentList::m_ACT_editUpdate(Sender);
  else
      m_ACT_edit->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_deleteUpdate(TObject *Sender)
{
  if (m_dm->m_Q_listDOC_SRC_ORG_ID->AsInteger == m_dm->HomeOrgId)
     TTSFDocumentList::m_ACT_deleteUpdate(Sender);
  else
     m_ACT_delete->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_select_allUpdate(TObject *Sender)
{
  m_ACT_select_all->Enabled = !m_dm->m_Q_listID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_select_allExecute(TObject *Sender)
{
  m_DBG_list->SelectedRows->SelectAll();
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_status_barUpdate(TObject *Sender)
{
  TTSFDocumentList::m_ACT_status_barUpdate(Sender);
  m_SB_main->Panels->Items[2]->Text = m_BCS_search->Active ? "������ �����-����" : "";
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_BCS_searchBegin(TObject *Sender)
{
  m_tmp_list_grid_opts = m_DBG_list->OptionsML;
  m_tmp_nmcls_grid_opts = m_DBG_nmcls->OptionsML;
  m_DBG_list->OptionsML = TMLGridOptions() << goWithoutFind;
  m_DBG_nmcls->OptionsML = TMLGridOptions() << goWithoutFind;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_BCS_searchEnd(TObject *Sender)
{
  try
  {
    if (!m_dm->ShowNmcls) throw ETSError("�� ������� �������� �������");

    TBarCode bar_code = FindBarCode(m_dm->m_DB_main->DatabaseName,m_BCS_search->GetResult());
    if (bar_code.Exist)
    {
      if (!m_dm->m_Q_nmcls->Locate(m_dm->m_Q_nmclsNMCL_ID->FieldName,bar_code.NmclId,TLocateOptions()))
        throw ETSError(
          "����� ��� " + bar_code.BarCode + " ��� ������ " + IntToStr(bar_code.NmclId) + "  \"" +
          bar_code.NmclName + "\" � ������ ����������� �� ������"
        );
    }
    else
      throw ETSError("����� ��� " + bar_code.BarCode + " �� ������");
  }
  __finally
  {
    m_DBG_list->OptionsML = m_tmp_list_grid_opts;
    m_DBG_nmcls->OptionsML = m_tmp_nmcls_grid_opts;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_DBG_listEnter(TObject *Sender)
{
  m_BCS_search->Active = true;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_DBG_listExit(TObject *Sender)
{
  m_BCS_search->Active = false;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_check_arrivalExecute(TObject *Sender)
{
  TMLParams p_params;
  long sq = 0;

  sq = m_dm->FillTmpReportParamValues(m_DBG_list,"ID", sq);
  p_params.InitParam("SQ_ID", sq);

  m_rep_ch_arrival_ctrl.Execute(FormState.Contains(fsModal), p_params);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_check_arrivalUpdate(TObject *Sender)
{
  m_ACT_check_arrival->Enabled =
    !m_dm->m_Q_listID->IsNull &&
    m_rep_ch_arrival_ctrl.CanExecute;
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_print_labelExecute(TObject *Sender)
{
  m_dm->FillData4PrintLabel(m_dm->m_Q_listID->AsInteger);
  TMLParams params;
 	params.InitParam("SQ_ID", m_dm->SqDoc);
  m_rtl_print_labels.Execute(true,params);
}
//---------------------------------------------------------------------------

void __fastcall TFIncomeList::m_ACT_print_labelUpdate(TObject *Sender)
{
  m_ACT_print_label->Enabled = !m_dm->m_Q_listID->IsNull && m_rtl_print_labels.CanExecute;
}
//---------------------------------------------------------------------------


