//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmAlcoJournalNew.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDAlcoJournalNew::TDAlcoJournalNew(TComponent* p_owner, AnsiString p_prog_id):
    TTSDOperation(p_owner, p_prog_id),
    m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID")))
{
  m_gen = 0;
  m_Q_taxpayer_fict->Open();
  SetBeginEndDate(int(GetSysDate()) - 1, int(GetSysDate()));
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDAlcoJournalNew::GetBeginDate()
{
  return m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDAlcoJournalNew::GetEndDate()
{
  return m_Q_main->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDAlcoJournalNew::SetBeginEndDate(const TDateTime &p_begin_date,
                                                const TDateTime &p_end_date)
{
    if ((int(m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
            (int(m_Q_main->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)))
        return;

    m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
    m_Q_main->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

    if (m_Q_main->Active)
        RefreshDataSets();
}
//---------------------------------------------------------------------------

__fastcall TDAlcoJournalNew::~TDAlcoJournalNew()
{
  if(m_Q_taxpayer_fict->Active) m_Q_taxpayer_fict->Close();
}
//---------------------------------------------------------------------------
void __fastcall TDAlcoJournalNew::m_Q_mainBeforeOpen(TDataSet *DataSet)
{
	m_Q_main->ParamByName( "TAX_PAYER" )->AsInteger = m_Q_taxpayer_fictID->AsInteger;
	m_Q_main->ParamByName( "BEGIN_DATE" )->AsDateTime = BeginDate;
	m_Q_main->ParamByName( "END_DATE" )->AsDateTime = EndDate;
}
//---------------------------------------------------------------------------



void __fastcall
TDAlcoJournalNew::m_Q_orgBeforeOpen(TDataSet *DataSet)
{
	m_Q_org->ParamByName( "ID" )->AsInteger = m_home_org_id;
}



void __fastcall
TDAlcoJournalNew::m_Q_addressBeforeOpen(TDataSet *DataSet)
{
	m_Q_address->ParamByName( "ID" )->AsInteger = m_home_org_id;
}

