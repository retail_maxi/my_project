//---------------------------------------------------------------------------

#ifndef FmReplaceReasonH
#define FmReplaceReasonH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmBill.h"
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
#include <ToolWin.hpp>
#include "RXDBCtrl.hpp"
#include "ToolEdit.hpp"
//---------------------------------------------------------------------------
class TFReplaseReason : public TTSFDialog
{
__published:	// IDE-managed Components
        TLabel *m_L_reason;
        TDBEdit *m_DBE_reason;
        TDataSource *m_DS_reason;
private:	// User declarations
  TDBill *m_dm;
public:		// User declarations
        __fastcall TFReplaseReason::TFReplaseReason(TComponent* p_owner, TDBill *p_dm);
};
//---------------------------------------------------------------------------
//extern PACKAGE TFReplaseReason *FReplaseReason;
//---------------------------------------------------------------------------
#endif
