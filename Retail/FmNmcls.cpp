//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmNmcls.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFNmcls::m_ACT_select_allExecute(TObject *Sender)
{

  switch( m_dm->TypeNmclLoad )
  {
    case tnlGoods:
      m_dm->m_Q_nmcls->Close();
      m_dm->m_Q_nmcls->ParamByName("YES_NO")->AsString = "Y";
      m_dm->m_Q_nmcls->Open();
      break;
    case tnlInvs:
      m_dm->m_Q_nmcls_i->Close();
      m_dm->m_Q_nmcls_i->ParamByName("YES_NO")->AsString = "Y";
      m_dm->m_Q_nmcls_i->Open();
      break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFNmcls::m_ACT_select_allUpdate(TObject *Sender)
{
  switch( m_dm->TypeNmclLoad )
  {
    case tnlGoods:
      m_ACT_select_all->Enabled = !m_dm->m_Q_nmclsNMCL_ID->IsNull;
      break;
    case tnlInvs:
      m_ACT_select_all->Enabled = !m_dm->m_Q_nmcls_iNMCL_ID->IsNull;
      break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFNmcls::m_CB_nonzeroClick(TObject *Sender)
{
  switch( m_dm->TypeNmclLoad )
  {
    case tnlGoods:
      if (m_CB_nonzero->Checked != m_dm->m_Q_nmcls->ParamByName("NONZERO")->AsInteger)
      {
        m_dm->m_Q_nmcls->Close();
        m_dm->m_Q_nmcls->ParamByName("NONZERO")->AsInteger = m_CB_nonzero->Checked;
        m_dm->m_Q_nmcls_i->ParamByName("NONZERO")->AsInteger = m_CB_nonzero->Checked;
        m_dm->m_Q_nmcls->Open();
      }
      break;
    case tnlInvs:
      if (m_CB_nonzero->Checked != m_dm->m_Q_nmcls_i->ParamByName("NONZERO")->AsInteger)
      {
        m_dm->m_Q_nmcls_i->Close();
        m_dm->m_Q_nmcls->ParamByName("NONZERO")->AsInteger = m_CB_nonzero->Checked;        
        m_dm->m_Q_nmcls_i->ParamByName("NONZERO")->AsInteger = m_CB_nonzero->Checked;
        m_dm->m_Q_nmcls_i->Open();
      }
      break;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFNmcls::FormShow(TObject *Sender)
{
  m_CB_nonzero->Checked = m_dm->m_Q_nmcls->ParamByName("NONZERO")->AsInteger;
  m_PC_nmcl->ActivePageIndex = m_dm->TypeNmclLoad;
}
//---------------------------------------------------------------------------

void __fastcall TFNmcls::m_PC_nmclChange(TObject *Sender)
{
  m_dm->TypeNmclLoad = (TTypeNmclLoad)m_PC_nmcl->ActivePageIndex;
}
//---------------------------------------------------------------------------

