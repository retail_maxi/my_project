//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocDiscGoodsLine.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------


AnsiString __fastcall TFDocDiscGoodsLine::BeforeLineApply(TWinControl *p_focused)
{
/*
  if (m_dm->m_Q_lineLINE_ID->AsInteger <= 0)
  {
    return "�������� �����!";
  }
  */
  return "";
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsLine::FormShow(TObject *Sender)
{
  TTSFDocumentLine::FormShow(Sender);
  m_DBG_list->ReadOnly = (m_dm->UpdateRegimeLine != urInsert);
  m_DBLC_grp->ReadOnly = (m_dm->UpdateRegimeLine != urInsert);
  if(m_dm->m_Q_sel_orgs->Active) m_dm->m_Q_sel_orgs->Close();
  m_dm->m_Q_sel_orgs->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsLine::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  if (m_PC_main->ActivePage == m_TS_new)
  {
    m_dm->m_Q_sel_orgs->First();
    while (!m_dm->m_Q_sel_orgs->Eof)
    {
      if (m_dm->m_Q_sel_orgsCHECKED->AsInteger == 1)
      {
        try {
          m_dm->m_Q_ins_orgs->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
          m_dm->m_Q_ins_orgs->ParamByName("ORG_ID")->AsInteger = m_dm->m_Q_sel_orgsID->AsInteger;
          m_dm->m_Q_ins_orgs->ExecSQL();
        } catch (Exception& e){
          MessageDlg(e.Message, mtError, TMsgDlgButtons()<<mbOK,0);
        }
      }
      m_dm->m_Q_sel_orgs->Next();
    }
    TTSFDialog::m_ACT_apply_updatesExecute(Sender);
  }
  else TTSFDocumentLine::m_ACT_apply_updatesExecute(Sender);
  m_dm->RefreshItemDataSets();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsLine::m_ACT_insertUpdate(TObject *Sender)
{
  if (m_PC_main->ActivePage == m_TS_new)
  {
    m_ACT_insert->Enabled = false;
  }
  else TTSFDocumentLine::m_ACT_insertUpdate(Sender);
}
//---------------------------------------------------------------------------

bool __fastcall TFDocDiscGoodsLine::FillNodesCheck(int node_id, bool m_check)
{
  m_dm->m_Q_sel_orgs->DisableControls();
  try
  {
    if (m_check)
    {
      m_dm->m_Q_sel_orgs->Edit();
      m_dm->m_Q_sel_orgsCHECKED->AsInteger = 1;
      m_dm->m_Q_sel_orgs->Post();
      m_dm->m_Q_sel_orgs->Next();
    }
    else
    {
      m_dm->m_Q_sel_orgs->Edit();
      m_dm->m_Q_sel_orgsCHECKED->AsInteger = 2;
      m_dm->m_Q_sel_orgs->Post();
      m_dm->m_Q_sel_orgs->Next();
    }
      m_dm->m_Q_sel_orgs->Locate(m_dm->m_Q_sel_orgsID->FieldName, node_id, TLocateOptions());
  }
  __finally
  {
    m_dm->m_Q_sel_orgs->EnableControls();
  }
  return true;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsLine::m_DBG_listKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == 32)
  {
    FillNodesCheck(m_dm->m_Q_sel_orgsID->AsInteger, m_dm->m_Q_sel_orgsCHECKED->AsInteger == 2);
  }
}
//---------------------------------------------------------------------------

