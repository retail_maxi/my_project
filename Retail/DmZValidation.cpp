//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DmZValidation.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLCustomContainer"
#pragma link "MLLogin"
#pragma link "MLQuery"
#pragma link "MLUpdateSQL"
#pragma link "RxQuery"
#pragma link "TSConnectionContainer"
#pragma link "TSDMOPERATION"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TDZValidation::TDZValidation(TComponent* p_owner, AnsiString p_prog_id)
          : TTSDOperation(p_owner, p_prog_id)
 {
    m_sq_tax = 0;
    m_act_tax = true;
    SetBeginEndDate(int(GetSysDate()), GetSysDate());
    m_Q_tax->Open();
 };
__fastcall TDZValidation::~TDZValidation()
 {
    m_Q_tax->Close();
    ClearParamValues(m_sq_tax);
 };

void __fastcall TDZValidation::SetBeginEndDate(const TDateTime &p_begin_date,
                                             const TDateTime &p_end_date)
{
    m_begin_date = p_begin_date;
    m_end_date = p_end_date;
}
//---------------------------------------------------------------------------

void __fastcall TDZValidation::m_Q_taxCHECKEDChange(TField *Sender)
{
    if (m_Q_taxCHECKED->AsInteger == 1)
   {
       if( m_sq_tax == 0 ) m_sq_tax = GetSqNextVal("SQ_PARAM_VALUES");
       m_Q_ins_param->ParamByName("SQ_ID")->AsInteger =  m_sq_tax;
       m_Q_ins_param->ParamByName("VALUE")->AsInteger =  m_Q_taxID->AsInteger;
       m_Q_ins_param->ExecSQL();
   }
   else
   {
       m_Q_del_param->ParamByName("SQ_ID")->AsInteger =  m_sq_tax;
       m_Q_del_param->ParamByName("VALUE")->AsInteger =  m_Q_taxID->AsInteger;
       m_Q_del_param->ExecSQL();
   }
}
//---------------------------------------------------------------------------

void __fastcall TDZValidation::m_Q_taxBeforeOpen(TDataSet *DataSet)
{
   m_Q_tax->ParamByName("SQ_ID")->AsInteger = m_sq_tax;
   if (m_act_tax)
     m_Q_tax->MacroByName("WHERE_ACT_TAX")->AsString =
     AnsiString(" and tp.id in (select distinct sd.taxpayer_id from shop_departments sd") +
     AnsiString(" where sd.disabled is null and sd.taxpayer_id is not null) ");
   else
     m_Q_tax->MacroByName("WHERE_ACT_TAX")->AsString = "";

}
//---------------------------------------------------------------------------

void __fastcall TDZValidation::m_Q_mainBeforeOpen(TDataSet *DataSet)
{
  m_Q_main->ParamByName("SQ_TAX_ID")->AsInteger = m_sq_tax;
  m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = m_begin_date;
  m_Q_main->ParamByName("END_DATE")->AsDateTime = m_end_date;
}
//---------------------------------------------------------------------------

