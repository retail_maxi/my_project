inherited FRtlReturnsRecond: TFRtlReturnsRecond
  Left = 646
  Top = 372
  Caption = 'FRtlReturnsRecond'
  ClientHeight = 205
  ClientWidth = 335
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_price: TLabel [0]
    Left = 14
    Top = 56
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = '������ �������'
  end
  object m_L_qnt: TLabel [1]
    Left = 14
    Top = 92
    Width = 158
    Height = 13
    Alignment = taRightJustify
    Caption = '��������� ������� �� �������'
  end
  object Label1: TLabel [2]
    Left = 14
    Top = 124
    Width = 125
    Height = 13
    Alignment = taRightJustify
    Caption = '������� ��������� ����'
  end
  inherited m_P_main_control: TPanel
    Top = 155
    Width = 335
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 141
      inherited m_SBTN_save: TBitBtn
        Caption = '����������'
      end
      inherited m_BBTN_close: TBitBtn
        Visible = False
      end
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 44
      inherited m_SBTN_insert: TSpeedButton
        Visible = False
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 186
    Width = 335
    Panels = <
      item
        Style = psOwnerDraw
        Width = 18
      end
      item
        Width = 80
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 335
    inherited m_P_tb_main_add: TPanel
      Left = 288
    end
    inherited m_P_tb_main: TPanel
      Width = 288
      inherited m_TB_main: TToolBar
        Width = 288
      end
    end
  end
  object m_CB_cash_error: TComboBox [6]
    Left = 192
    Top = 56
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    OnChange = m_CB_cash_errorChange
    Items.Strings = (
      '��'
      '���'
      ' ')
  end
  object m_CB_decision_return: TComboBox [7]
    Left = 192
    Top = 88
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    OnChange = m_CB_decision_returnChange
    Items.Strings = (
      '��'
      '���'
      ' ')
  end
  object m_CB_is_check: TComboBox [8]
    Left = 192
    Top = 120
    Width = 121
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    OnChange = m_CB_is_checkChange
    Items.Strings = (
      '����'
      '���'
      ' ')
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_apply_updates: TAction
      Caption = '����������'
      Hint = '����������'
    end
  end
  object m_DS_item: TDataSource
    DataSet = DRtlReturns.m_Q_item
    Left = 40
    Top = 56
  end
end
