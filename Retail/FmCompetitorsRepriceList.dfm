inherited FCompetitorsRepriceList: TFCompetitorsRepriceList
  Left = 445
  Top = 288
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_DBG_list: TMLDBGrid
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_RIGHT'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ISSUED_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    inherited m_P_tb_main: TPanel
      inherited m_TB_main: TToolBar
        object m_TBTN_sep: TToolButton
          Left = 256
          Top = 2
          Width = 8
          Caption = 'm_TBTN_sep'
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_SBTN_set_begin_end_date: TSpeedButton
          Left = 264
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'ML'
      Caption = 'c 09.06.2010 �� 09.06.2010'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 40338
      EndDate = 40338
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DCompetitorsReprice.m_Q_list
  end
end
