//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmForGroups.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFForGroups::FormShow(TObject *Sender)
{
  m_DBCB_class_groups->KeyValue = m_dm->m_Q_class_groupsCLASS_ID->AsInteger;
  m_DTP_doc_date->DateTime = int(m_dm->GetSysDate());
  m_DBCB_gr_dep->KeyValue = m_dm->m_Q_gr_depsID->AsInteger;
  m_dm->m_SP_create_act_for_group->ParamByName("P_START_DATE")->AsDateTime = int(m_DTP_doc_date->DateTime);
  m_CB_nonzeroClick(m_CB_nonzero);
  m_DBCB_class_groups->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFForGroups::m_DTP_doc_dateChange(TObject *Sender)
{
  m_dm->m_SP_create_act_for_group->ParamByName("P_START_DATE")->AsDateTime = int(m_DTP_doc_date->DateTime);
}
//---------------------------------------------------------------------------


void __fastcall TFForGroups::m_CB_nonzeroClick(TObject *Sender)
{
 m_dm->m_SP_create_act_for_group->ParamByName("P_NONZERO")->AsInteger = m_CB_nonzero->Checked; 
}
//---------------------------------------------------------------------------

