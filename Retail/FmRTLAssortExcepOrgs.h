//---------------------------------------------------------------------------

#ifndef FmRTLAssortExcepOrgsH
#define FmRTLAssortExcepOrgsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmInvAct.h"
#include "MLQuery.h"
#include "RxQuery.hpp"
#include <DBTables.hpp>
#include "dbtree_l.hpp"
#include "MLUpdateSQL.h"
//---------------------------------------------------------------------------

class TFRTLAssortExcepOrgs : public TTSFDialog
{
__published:
  TPanel *m_P_main;
  TPanel *m_P_left;
  TPanel *m_P_all;
  TSplitter *Splitter1;
  TPanel *m_P_btns;
  TSpeedButton *m_SBTN_add;
  TPageControl *m_PC_all_cnt;
        TTabSheet *m_TS_org;
  TSpeedButton *m_SBTN_delete;
        TDataSource *m_DS_org;
        TMLDBGrid *m_DBG_all_org;
  TAction *m_ACT_add;
  TAction *m_ACT_delete;
        TDataSource *m_DS_all_org;
        TQuery *m_Q_add_org;
        TQuery *m_Q_delete_org;
        TMLQuery *m_Q_all_org;
        TMLUpdateSQL *m_U_org;
        TMLDBGrid *m_DBG_org;
        TMLQuery *m_Q_org;
        TFloatField *m_Q_orgORG_ID;
        TStringField *m_Q_orgORG_NAME;
        TFloatField *m_Q_all_orgID;
        TStringField *m_Q_all_orgNAME;
  void __fastcall m_ACT_addExecute(TObject *Sender);
  void __fastcall m_ACT_addUpdate(TObject *Sender);
  void __fastcall m_ACT_deleteExecute(TObject *Sender);
  void __fastcall m_ACT_deleteUpdate(TObject *Sender);
private:
  bool is_edit;
public:
  __fastcall TFRTLAssortExcepOrgs(TComponent* p_owner,
                     TTSDCustom* p_dm_custom, bool p_edit): TTSFDialog(p_owner,
                                                          p_dm_custom),
                                                          is_edit(p_edit) {};
};
//---------------------------------------------------------------------------

void RTLAssortExcepOrgsDlg(TComponent* p_owner, int p_id, int p_org_group_id, TTSDCustom* p_dm, bool p_edit = false);
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

