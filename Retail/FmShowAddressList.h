//---------------------------------------------------------------------------

#ifndef FmShowAddressListH
#define FmShowAddressListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLActionsControls.h"
#include "MLQuery.h"
#include "RxQuery.hpp"
#include <ActnList.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <DBTables.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include "DmIncome.h"
#include "TSDMDOCUMENTWITHLINES.h"
//---------------------------------------------------------------------------
class TFShowAddressList : public TForm
{
__published:	// IDE-managed Components
    TDataSource *m_DS_address_list;
    TActionList *m_AL_main;
    TMLSetBeginEndDate *m_ACT_set_begin_end_date;
    TMLDSFilter *m_ACT_filter;
    TMLDSOrder *m_ACT_order;
    TMLDBGridFind *m_ACT_find;
    TMLDBGridFindAgain *m_ACT_find_again;
    TMLDBGridSetColumns *m_ACT_set_columns;
    TAction *m_ACT_save;
    TImageList *m_IL_main;
    TToolBar *m_TB_buh_doc;
    TToolButton *m_TBTN_filter;
    TToolButton *m_TBTN_order;
    TToolButton *m_TBTN_find;
    TToolButton *m_TBTN_find_again;
    TToolButton *m_TBTN_sep1;
    TToolButton *m_TBTN_set_columns;
    TToolButton *m_TBTN_sep2;
    TPanel *m_P_main_control;
    TToolBar *m_TB_main_control_buttons_dialog;
    TBitBtn *m_SBTN_save;
    TBitBtn *m_BBTN_close;
    TMLDBGrid *m_DBG_list;
    void __fastcall m_BBTN_closeClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall m_ACT_saveExecute(TObject *Sender);
private:	// User declarations
    TDIncome* m_dm;
public:		// User declarations
    __fastcall TFShowAddressList(TComponent* p_owner, TTSDDocumentWithLines *p_dm);
};
//---------------------------------------------------------------------------
#endif 
