//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmTotalAmt.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLLovView"
#pragma link "MLLov"
#pragma link "TSFMDIALOG"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFmTotalAmt::TFmTotalAmt(TComponent* p_owner, TDIncome* p_dm)
    :TTSFDialog(p_owner,p_dm),
    m_dm(p_dm)
{
    if (m_dm->m_Q_fict->Active) m_dm->m_Q_fict->Close();
    m_dm->m_Q_fict->Open();  m_dm->m_Q_fict->Edit();
}


__fastcall TFmTotalAmt::~TFmTotalAmt()
{
 if (m_dm->m_Q_fict->Active)
        m_dm->m_Q_fict->Close();
}
//---------------------------------------------------------------------------


void __fastcall TFmTotalAmt::m_ACT_apply_updatesExecute(TObject *Sender)
{
  m_par = m_dm->m_Q_fictPAR->AsFloat;
  TTSFDialog::m_ACT_apply_updatesExecute(Sender);
}
//---------------------------------------------------------------------------



