inherited FForGroups: TFForGroups
  Left = 403
  Top = 523
  ActiveControl = m_DBCB_class_groups
  Caption = '��������� ��� ��������'
  ClientHeight = 130
  ClientWidth = 451
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_class_groups: TLabel [0]
    Left = 7
    Top = 12
    Width = 53
    Height = 13
    Caption = '���������'
  end
  object m_L_doc_date: TLabel [1]
    Left = 320
    Top = 76
    Width = 26
    Height = 13
    Caption = '����'
    FocusControl = m_DTP_doc_date
  end
  object m_L_dep: TLabel [2]
    Left = 11
    Top = 44
    Width = 31
    Height = 13
    Caption = '�����'
  end
  inherited m_P_main_control: TPanel
    Top = 99
    Width = 451
    TabOrder = 2
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 257
      inherited m_SBTN_save: TBitBtn
        Caption = '�������'
      end
    end
  end
  object m_DBCB_class_groups: TDBLookupComboBox [4]
    Left = 64
    Top = 8
    Width = 384
    Height = 21
    KeyField = 'CLASS_ID'
    ListField = 'NAME'
    ListSource = m_DS_class_groups
    TabOrder = 0
  end
  object m_DTP_doc_date: TDateTimePicker [5]
    Left = 352
    Top = 72
    Width = 97
    Height = 21
    Hint = 
      '����, � ������� ��� ������ ������ ��������� ������ ���� ��������' +
      '�� '
    CalAlignment = dtaLeft
    Date = 38534
    Time = 38534
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 1
    OnChange = m_DTP_doc_dateChange
  end
  object m_DBCB_gr_dep: TDBLookupComboBox [6]
    Left = 64
    Top = 40
    Width = 384
    Height = 21
    KeyField = 'ID'
    ListField = 'NAME'
    ListSource = m_DS_gr_deps
    TabOrder = 3
  end
  object m_CB_nonzero: TCheckBox [7]
    Left = 64
    Top = 66
    Width = 169
    Height = 24
    Caption = '��� ������� ��������'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    State = cbChecked
    TabOrder = 4
    OnClick = m_CB_nonzeroClick
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_apply_updates: TAction
      Caption = '�������'
      Hint = '������� ���'
    end
  end
  object m_DS_class_groups: TDataSource
    DataSet = DInvAct.m_Q_class_groups
    Left = 64
    Top = 16
  end
  object m_DS_gr_deps: TDataSource
    DataSet = DInvAct.m_Q_gr_deps
    Left = 64
    Top = 48
  end
end
