inherited DDocDiscGoods: TDDocDiscGoods
  OldCreateOrder = False
  Left = 393
  Top = 140
  Height = 851
  Width = 1229
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=ROBOT'
      'PASSWORD=RBT')
    Left = 24
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'select *'
      '  from ('
      'SELECT d.id AS id,'
      '       d.src_org_id AS doc_src_org_id,'
      '       o.name AS doc_src_org_name,'
      '       d.create_date AS doc_create_date,'
      '       d.comp_name AS doc_create_comp_name,'
      '       d.author AS doc_create_user,'
      
        '       TRIM(rpad(pkg_persons.Fnc_User_Name_Full(d.author,sysdate' +
        ') ,250)) as name_user,'
      '       d.modify_date AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user AS doc_modify_user,'
      '       d.status AS doc_status,'
      
        '       decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �' +
        '� ������'#39','#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) AS doc' +
        '_fldr_right,'
      '       g.doc_number, '
      '       g.doc_date, '
      '       g.begin_date, '
      '       g.end_date,'
      '       g.action_type_id,'
      '       act.name AS action_type_name,'
      '       g.nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       g.assortment_id, '
      '       s.name AS assortment_name,'
      '       g.reason_id, '
      '       rd.name AS reason_name,'
      '       g.note,'
      '       g.act_price,'
      '       g.price_bef_act,'
      '       g.pull_date,'
      '       nvl(g.urgent_reprice,0) as urgent_reprice,'
      '       rs.cat_men_id,'
      
        '         (select  max(trim(rpad(pkg_persons.fnc_person_name(rcm.' +
        'cnt_id,1),250))) as cat_men_name'
      '          from rtl_classifier_rs rs,'
      '               rtl_subsubcategories ss,'
      '               rtl_subcategories s,'
      '               rtl_categories c,'
      '               rtl_groups gr,'
      '               rtl_directions d,'
      '               rtl_cat_mens rcm'
      '          where rs.subsubcat_id = ss.id'
      '            and ss.subcat_id = s.id'
      '            and s.cat_id = c.id'
      '            and c.group_id = gr.id'
      '            and gr.direct_id = d.id'
      '            and nvl(gr.cat_men_id,-1) = rcm.id'
      '            and rs.nmcl_id = g.nmcl_id'
      
        '            and (rs.assort_id = g.assortment_id or g.assortment_' +
        'id = 7)) cat_men_name,'
      '       rs.group_id,'
      '       rs.group_name,'
      '       g.reason_rejection,'
      '       g.reject_zam_kd,'
      '       nvl(g.is_returned,0) as is_returned,'
      '       (select max(agent_id) '
      '          from purchasing_agent_nmcl'
      '        where nmcl_id = g.nmcl_id'
      '           and end_date is null) agent_id,'
      '      nvl(supp_comp,0) as supp_comp,'
      '       (SELECT sum(buf.store_items)'
      '        FROM buf_nmcl_data buf'
      '       WHERE  buf.nmcl_id = g.nmcl_id'
      '         AND buf.assortment_id = g.assortment_id'
      '         AND buf.rep_date = TRUNC(SYSDATE)'
      '         AND buf.org_id = 3) as rest_rc,'
      
        '        TRIM(RPAD(pkg_contractors.fnc_contr_full_name( g.cnt_id)' +
        ' ,250)) as cnt_name,'
      '        g.cnt_sale,'
      '        g.cnt_price_comp,'
      
        '      TRIM(rpad((select csl(a) from (select o.name as a from doc' +
        '_disc_goods_items di, organizations o, groups_organizations og, ' +
        'org_groups gr '
      
        '                                             where doc_id=d.id a' +
        'nd di.org_id=o.id and o.id = og.org_id and gr.id = og.grp_id and' +
        ' gr.type_id = 2518 and gr.id=71)),250))  as action_org_vol,'
      
        '      TRIM(rpad((select csl(a) from (select o.name as a from doc' +
        '_disc_goods_items di, organizations o, groups_organizations og, ' +
        'org_groups gr '
      
        '                                             where doc_id=d.id a' +
        'nd di.org_id=o.id and o.id = og.org_id and gr.id = og.grp_id and' +
        ' gr.type_id = 2518 and gr.id=72)),250))  as action_org_cher,'
      
        '      TRIM(rpad((select csl(a) from (select o.name as a from doc' +
        '_disc_goods_items di, organizations o, groups_organizations og, ' +
        'org_groups gr '
      
        '                                             where doc_id=d.id a' +
        'nd di.org_id=o.id and o.id = og.org_id and gr.id = og.grp_id and' +
        ' gr.type_id = 2518 and gr.id=88)),250))  as action_org_arch,'
      
        '      case when exists(select 1 from rtl_act_price_list ap where' +
        ' ap.nmcl_id=g.nmcl_id and nvl(nullif(ap.assortment_id,7),g.assor' +
        'tment_id)= g.assortment_id) then 1 else 0 end is_current_action,'
      '      g.cnt_comp_sale,'
      '      g.cnt_method_comp,'
      
        '      TRIM(rpad(pkg_persons.Fnc_User_Name_Full(d.modify_user,sys' +
        'date), 250)) as mod_user_name'
      'FROM documents d,'
      '     folders f,'
      '     organizations o,'
      '     doc_disc_goods g,'
      '     nomenclature_items ni,'
      '     rtl_action_types act,'
      '     assortment s,'
      '     rtl_reason_disc rd,     '
      
        '     (select nmcl_id, assort_id, gr.id as group_id, gr.name as g' +
        'roup_name, gr.cat_men_id as cat_men_id, d.name as dir_name'
      '        from rtl_classifier_rs rs,'
      '             rtl_subsubcategories ss,'
      '             rtl_subcategories s,'
      '             rtl_categories c,'
      '             rtl_groups gr,'
      '             rtl_directions d             '
      '        where rs.subsubcat_id = ss.id'
      '          and ss.subcat_id = s.id'
      '          and s.cat_id = c.id'
      '          and c.group_id = gr.id'
      '          and gr.direct_id = d.id) rs'
      'WHERE f.doc_type_id = :DOC_TYPE_ID'
      '  AND f.id = d.fldr_id'
      '  AND o.id = d.src_org_id'
      '  AND fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      
        '  AND g.doc_date BETWEEN :BEGIN_DATE AND :END_DATE + 1 - 1/24/60' +
        '/60'
      '  AND g.doc_id = d.id'
      ' AND ni.id(+) = g.nmcl_id'
      '  AND act.id(+) = g.action_type_id'
      '  AND s.id(+) = g.assortment_id'
      '  AND g.reason_id = rd.id(+)'
      '  AND rs.nmcl_id(+) = g.nmcl_id'
      '  AND rs.assort_id(+) = g.assortment_id'
      ') v'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 'v.ID'
    end
    inherited m_Q_listDOC_SRC_ORG_ID: TFloatField
      Origin = 'v.DOC_SRC_ORG_ID'
    end
    inherited m_Q_listDOC_SRC_ORG_NAME: TStringField
      Origin = 'v.DOC_SRC_ORG_NAME'
    end
    inherited m_Q_listDOC_CREATE_DATE: TDateTimeField
      Origin = 'v.DOC_CREATE_DATE'
    end
    inherited m_Q_listDOC_CREATE_COMP_NAME: TStringField
      Origin = 'v.DOC_CREATE_COMP_NAME'
    end
    inherited m_Q_listDOC_CREATE_USER: TStringField
      Origin = 'v.DOC_CREATE_USER'
    end
    inherited m_Q_listDOC_MODIFY_DATE: TDateTimeField
      Origin = 'v.DOC_MODIFY_DATE'
    end
    inherited m_Q_listDOC_MODIFY_COMP_NAME: TStringField
      Origin = 'v.DOC_MODIFY_COMP_NAME'
    end
    inherited m_Q_listDOC_MODIFY_USER: TStringField
      Origin = 'v.DOC_MODIFY_USER'
    end
    inherited m_Q_listDOC_STATUS: TStringField
      Origin = 'v.DOC_STATUS'
    end
    inherited m_Q_listDOC_STATUS_NAME: TStringField
      Origin = 'v.DOC_STATUS_NAME'
    end
    inherited m_Q_listDOC_STATUS_CHANGE_DATE: TDateTimeField
      Origin = 'v.DOC_STATUS_CHANGE_DATE'
    end
    inherited m_Q_listDOC_FLDR_ID: TFloatField
      Origin = 'v.DOC_FLDR_ID'
    end
    inherited m_Q_listDOC_FLDR_NAME: TStringField
      Origin = 'v.DOC_FLDR_NAME'
    end
    inherited m_Q_listDOC_FLDR_RIGHT: TStringField
      Origin = 'v.DOC_FLDR_RIGHT'
    end
    object m_Q_listDOC_NUMBER: TStringField
      DisplayLabel = '� ���������'
      FieldName = 'DOC_NUMBER'
      Origin = 'v.DOC_NUMBER'
      Size = 50
    end
    object m_Q_listDOC_DATE: TDateTimeField
      DisplayLabel = '���� ���������'
      FieldName = 'DOC_DATE'
      Origin = 'v.DOC_DATE'
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'v.NOTE'
      Size = 250
    end
    object m_Q_listNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'NMCL_ID'
      Origin = 'v.NMCL_ID'
    end
    object m_Q_listNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      FieldName = 'NMCL_NAME'
      Origin = 'v.NMCL_NAME'
      Size = 100
    end
    object m_Q_listASSORTMENT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      FieldName = 'ASSORTMENT_ID'
      Origin = 'v.ASSORTMENT_ID'
    end
    object m_Q_listASSORTMENT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORTMENT_NAME'
      Origin = 'v.ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_listACTION_TYPE_ID: TFloatField
      FieldName = 'ACTION_TYPE_ID'
      Origin = 'v.ACTION_TYPE_ID'
    end
    object m_Q_listACTION_TYPE_NAME: TStringField
      DisplayLabel = '��� �����'
      FieldName = 'ACTION_TYPE_NAME'
      Origin = 'v.ACTION_TYPE_NAME'
      Size = 250
    end
    object m_Q_listBEGIN_DATE: TDateTimeField
      DisplayLabel = '������ ������ | ���� ������'
      FieldName = 'BEGIN_DATE'
      Origin = 'v.BEGIN_DATE'
    end
    object m_Q_listEND_DATE: TDateTimeField
      DisplayLabel = '������ ������ | ���� ���������'
      FieldName = 'END_DATE'
      Origin = 'v.END_DATE'
    end
    object m_Q_listACT_PRICE: TFloatField
      DisplayLabel = '������������� ���� �� �����'
      FieldName = 'ACT_PRICE'
      Origin = 'v.ACT_PRICE'
      currency = True
    end
    object m_Q_listGROUP_ID: TFloatField
      DisplayLabel = 'ID ������ ������'
      FieldName = 'GROUP_ID'
      Origin = 'v.GROUP_ID'
    end
    object m_Q_listGROUP_NAME: TStringField
      DisplayLabel = '������ ������'
      FieldName = 'GROUP_NAME'
      Origin = 'v.group_name'
      Size = 100
    end
    object m_Q_listCAT_MEN_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'CAT_MEN_ID'
      Origin = 'v.CAT_MEN_ID'
    end
    object m_Q_listCAT_MEN_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'CAT_MEN_NAME'
      Origin = 'v.cat_men_name'
      Size = 250
    end
    object m_Q_listREASON_ID: TStringField
      FieldName = 'REASON_ID'
      Origin = 'v.REASON_ID'
      Visible = False
      Size = 250
    end
    object m_Q_listREASON_NAME: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'REASON_NAME'
      Origin = 'v.REASON_NAME'
      Size = 250
    end
    object m_Q_listPRICE_BEF_ACT: TFloatField
      DisplayLabel = '���� �� �����'
      FieldName = 'PRICE_BEF_ACT'
      Origin = 'v.PRICE_BEF_ACT'
    end
    object m_Q_listREASON_REJECTION: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'REASON_REJECTION'
      Origin = 'v.REASON_REJECTION'
      Size = 250
    end
    object m_Q_listIS_RETURNED: TFloatField
      FieldName = 'IS_RETURNED'
      Origin = 'v.IS_RETURNED'
    end
    object m_Q_listAGENT_ID: TFloatField
      FieldName = 'AGENT_ID'
    end
    object m_Q_listURGENT_REPRICE: TFloatField
      DisplayLabel = '������� ����������'
      FieldName = 'URGENT_REPRICE'
      Origin = 'v.URGENT_REPRICE'
    end
    object m_Q_listSUPP_COMP: TFloatField
      DisplayLabel = '����������� �����������'
      FieldName = 'SUPP_COMP'
      Origin = 'v.SUPP_COMP'
    end
    object m_Q_listREST_RC: TFloatField
      DisplayLabel = '������� �� ��'
      FieldName = 'REST_RC'
      Origin = 'v.REST_RC'
    end
    object m_Q_listREJECT_ZAM_KD: TStringField
      DisplayLabel = '������� ���������� ��� �� �� ��'
      FieldName = 'REJECT_ZAM_KD'
      Origin = 'v.REJECT_ZAM_KD'
      Size = 250
    end
    object m_Q_listCNT_NAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'CNT_NAME'
      Origin = 'v.CNT_NAME'
      Size = 250
    end
    object m_Q_listCNT_SALE: TFloatField
      DisplayLabel = '������ ������'
      FieldName = 'CNT_SALE'
      Origin = 'v.CNT_SALE'
    end
    object m_Q_listCNT_PRICE_COMP: TFloatField
      DisplayLabel = '���� ��� ������� �����������'
      FieldName = 'CNT_PRICE_COMP'
      Origin = 'v.CNT_PRICE_COMP'
    end
    object m_Q_listACTION_ORG_VOL: TStringField
      DisplayLabel = '����� �� ��� | �������'
      FieldName = 'ACTION_ORG_VOL'
      Origin = 'v.ACTION_ORG_VOL'
      Size = 250
    end
    object m_Q_listACTION_ORG_CHER: TStringField
      DisplayLabel = '����� �� ��� | ���������'
      FieldName = 'ACTION_ORG_CHER'
      Origin = 'v.ACTION_ORG_CHER'
      Size = 250
    end
    object m_Q_listACTION_ORG_ARCH: TStringField
      DisplayLabel = '����� �� ��� | �����������'
      FieldName = 'ACTION_ORG_ARCH'
      Origin = 'v.ACTION_ORG_ARCH'
      Size = 250
    end
    object m_Q_listIS_CURRENT_ACTION: TFloatField
      DisplayLabel = '���� ����������� �����'
      FieldName = 'IS_CURRENT_ACTION'
      Origin = 'v.IS_CURRENT_ACTION'
      Visible = False
    end
    object m_Q_listNAME_USER: TStringField
      DisplayLabel = '��� ������������, ���������� ��������'
      FieldName = 'NAME_USER'
      Origin = 'v.NAME_USER'
      Size = 250
    end
    object m_Q_listCNT_COMP_SALE: TStringField
      DisplayLabel = '����������� � ��. ���������� ������'
      FieldName = 'CNT_COMP_SALE'
      Origin = 'v.CNT_COMP_SALE'
      Size = 250
    end
    object m_Q_listCNT_METHOD_COMP: TStringField
      DisplayLabel = '�����/������ �����������'
      FieldName = 'CNT_METHOD_COMP'
      Origin = 'v.CNT_METHOD_COMP'
      Size = 250
    end
    object m_Q_listMOD_USER_NAME: TStringField
      DisplayLabel = '��� ������������, ������������� ��������'
      FieldName = 'MOD_USER_NAME'
      Origin = 'v.MOD_USER_NAME'
      Size = 250
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'SELECT g.doc_id id,'
      '       g.doc_number, '
      '       g.doc_date, '
      '       g.begin_date, '
      '       g.end_date,'
      '       g.action_type_id, '
      '       act.name AS action_type_name,'
      '       g.nmcl_id, '
      '       ni.name AS nmcl_name,'
      '       g.assortment_id, '
      '       s.name AS assortment_name,'
      '       g.reason_id, '
      '       rd.name AS reason_name,'
      '       g.note,'
      '       g.act_price,'
      '       g.price_bef_act,'
      '       g.cnt_id,'
      
        '      TRIM(RPAD(pkg_contractors.fnc_contr_full_name( g.cnt_id) ,' +
        '250)) as cnt_name,'
      '       g.price_bef_act_on_card,'
      '       g.cnt_sale,'
      '       g.cnt_price_comp,'
      '       g.cnt_comp_sale,'
      '       g.cnt_method_comp,'
      '       g.pull_date,'
      '       g.urgent_reprice,'
      
        '       (select  max(trim(rpad(pkg_persons.fnc_person_name(rcm.cn' +
        't_id,1),250))) as cat_men_name'
      '          from rtl_classifier_rs rs,'
      '               rtl_subsubcategories ss,'
      '               rtl_subcategories s,'
      '               rtl_categories c,'
      '               rtl_groups gr,'
      '               rtl_directions d,'
      '               rtl_cat_mens rcm'
      '          where rs.subsubcat_id = ss.id'
      '            and ss.subcat_id = s.id'
      '            and s.cat_id = c.id'
      '            and c.group_id = gr.id'
      '            and gr.direct_id = d.id'
      '            and nvl(gr.cat_men_id,-1) = rcm.id'
      '            and rs.nmcl_id = g.nmcl_id'
      
        '            and (rs.assort_id = g.assortment_id or g.assortment_' +
        'id = 7)) cat_men_name,'
      '       g.reason_rejection,'
      '       (select max(agent_id) '
      '          from purchasing_agent_nmcl'
      '        where nmcl_id = g.nmcl_id'
      '           and end_date is null) agent_id,'
      '      supp_comp'
      'FROM doc_disc_goods g,'
      '     nomenclature_items ni,'
      '     rtl_action_types act,'
      '     assortment s,'
      '     rtl_reason_disc rd'
      'WHERE g.doc_id = :ID'
      
        '  AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_id' +
        ' = :FLDR_ID)'
      '   AND ni.id(+) = g.nmcl_id'
      '  AND act.id(+) = g.action_type_id'
      '  AND s.id(+) = g.assortment_id'
      '  AND g.reason_id = rd.id(+)')
    object m_Q_itemDOC_NUMBER: TStringField
      FieldName = 'DOC_NUMBER'
      Size = 50
    end
    object m_Q_itemDOC_DATE: TDateTimeField
      FieldName = 'DOC_DATE'
    end
    object m_Q_itemBEGIN_DATE: TDateTimeField
      FieldName = 'BEGIN_DATE'
    end
    object m_Q_itemEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object m_Q_itemACTION_TYPE_ID: TFloatField
      FieldName = 'ACTION_TYPE_ID'
    end
    object m_Q_itemACTION_TYPE_NAME: TStringField
      FieldName = 'ACTION_TYPE_NAME'
      Size = 250
    end
    object m_Q_itemNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_itemNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Size = 100
    end
    object m_Q_itemASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
    end
    object m_Q_itemASSORTMENT_NAME: TStringField
      FieldName = 'ASSORTMENT_NAME'
      Size = 100
    end
    object m_Q_itemREASON_ID: TStringField
      FieldName = 'REASON_ID'
      Size = 250
    end
    object m_Q_itemREASON_NAME: TStringField
      FieldName = 'REASON_NAME'
      Size = 250
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemACT_PRICE: TFloatField
      FieldName = 'ACT_PRICE'
      currency = True
    end
    object m_Q_itemPRICE_BEF_ACT: TFloatField
      FieldName = 'PRICE_BEF_ACT'
      currency = True
    end
    object m_Q_itemPULL_DATE: TDateTimeField
      FieldName = 'PULL_DATE'
    end
    object m_Q_itemURGENT_REPRICE: TFloatField
      FieldName = 'URGENT_REPRICE'
    end
    object m_Q_itemCAT_MEN_NAME: TStringField
      FieldName = 'CAT_MEN_NAME'
      Size = 250
    end
    object m_Q_itemREASON_REJECTION: TStringField
      FieldName = 'REASON_REJECTION'
      Size = 250
    end
    object m_Q_itemAGENT_ID: TFloatField
      FieldName = 'AGENT_ID'
    end
    object m_Q_itemSUPP_COMP: TFloatField
      FieldName = 'SUPP_COMP'
    end
    object m_Q_itemCNT_ID: TFloatField
      DisplayLabel = 'ID ����������'
      FieldName = 'CNT_ID'
      Origin = 'g.cnt_id'
    end
    object m_Q_itemPRICE_BEF_ACT_ON_CARD: TFloatField
      DisplayLabel = '���� �� ����� �� �����'
      FieldName = 'PRICE_BEF_ACT_ON_CARD'
      Origin = 'g.price_bef_act_on_card'
    end
    object m_Q_itemCNT_SALE: TFloatField
      DisplayLabel = '������ ������'
      FieldName = 'CNT_SALE'
      Origin = 'g.cnt_sale'
    end
    object m_Q_itemCNT_PRICE_COMP: TFloatField
      DisplayLabel = '���� ��� ������� �����������'
      FieldName = 'CNT_PRICE_COMP'
      Origin = 'g.cnt_price_comp'
    end
    object m_Q_itemCNT_NAME: TStringField
      FieldName = 'CNT_NAME'
      Origin = 'cnt_name'
      Size = 250
    end
    object m_Q_itemCNT_COMP_SALE: TStringField
      DisplayLabel = '����������� � ��. ���������� ������'
      FieldName = 'CNT_COMP_SALE'
      Size = 250
    end
    object m_Q_itemCNT_METHOD_COMP: TStringField
      DisplayLabel = '�����/������ �����������'
      FieldName = 'CNT_METHOD_COMP'
      Size = 250
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_disc_goods'
      'set'
      '  NMCL_ID = :NMCL_ID,'
      '  ASSORTMENT_ID = :ASSORTMENT_ID,'
      '  ACTION_TYPE_ID = :ACTION_TYPE_ID,'
      '  DOC_NUMBER = :DOC_NUMBER,'
      '  DOC_DATE = :DOC_DATE,'
      '  BEGIN_DATE = :BEGIN_DATE,'
      '  END_DATE = :END_DATE,'
      '  NOTE = :NOTE,'
      '  REASON_ID = :REASON_ID,'
      '  ACT_PRICE = :ACT_PRICE,'
      '  PRICE_BEF_ACT = :PRICE_BEF_ACT,'
      '  PULL_DATE = :PULL_DATE,'
      '  URGENT_REPRICE = :URGENT_REPRICE,'
      '  REASON_REJECTION= :REASON_REJECTION,'
      '  SUPP_COMP = :SUPP_COMP,'
      '  CNT_ID= :CNT_ID,'
      '  PRICE_BEF_ACT_ON_CARD = :PRICE_BEF_ACT_ON_CARD,'
      '  CNT_SALE = :CNT_SALE,'
      '  CNT_PRICE_COMP = :CNT_PRICE_COMP,'
      '  CNT_COMP_SALE = :CNT_COMP_SALE,'
      '  CNT_METHOD_COMP = :CNT_METHOD_COMP'
      'where'
      '  DOC_ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into doc_disc_goods'
      
        '  (DOC_ID, DOC_NUMBER, DOC_DATE, BEGIN_DATE, END_DATE,  ACTION_T' +
        'YPE_ID, NMCL_ID, ASSORTMENT_ID,'
      
        '   REASON_ID, NOTE, ACT_PRICE, PRICE_BEF_ACT, PULL_DATE, URGENT_' +
        'REPRICE, SUPP_COMP, CNT_ID, PRICE_BEF_ACT_ON_CARD, CNT_SALE,CNT_' +
        'PRICE_COMP,CNT_COMP_SALE,CNT_METHOD_COMP)'
      'values'
      
        '  (:ID, :DOC_NUMBER,:DOC_DATE, :BEGIN_DATE, :END_DATE, :ACTION_T' +
        'YPE_ID, :NMCL_ID, :ASSORTMENT_ID, '
      
        '   :REASON_ID, :NOTE, :ACT_PRICE, :PRICE_BEF_ACT, :PULL_DATE, :U' +
        'RGENT_REPRICE, nvl(:SUPP_COMP,0),  :CNT_ID, :PRICE_BEF_ACT_ON_CA' +
        'RD, :CNT_SALE, :CNT_PRICE_COMP, :CNT_COMP_SALE, :CNT_METHOD_COMP' +
        ')')
  end
  inherited m_Q_lines: TMLQuery
    SQL.Strings = (
      'select v.*, '
      
        '       min(margin_af_act) over (partition BY id) as min_margin_a' +
        'f_act,'
      
        '       max(total_profit) over (partition BY id) as max_total_pro' +
        'fit,'
      '       max(line_id) over () as max_line_id'
      'from ('
      'SELECT id,'
      '       line_id,'
      '       grp_name,'
      '       org_name,'
      '       model_type,'
      '       model_type_id,'
      '       note,'
      
        '       round(((out_price_nd - cur_price_in)/ nullif(cur_price_in' +
        ',0))*100,2) as fact_margin,'
      '       (out_price_nd - cur_price_in) as fact_margin_sum,'
      
        '       round(((act_price - cur_price_in) / nullif(cur_price_in,0' +
        '))*100,2) as margin_af_act,'
      
        '       case when ((act_price - cur_price_in) / nullif(cur_price_' +
        'in,0)) < 0 '
      
        '                 then round(((act_price - cur_price_in)* nvl(sto' +
        're_items,0)),2)  '
      '            else null'
      '       end total_profit,'
      '       day_store,'
      '       store_items,'
      '       items_week_1,'
      '       items_week_2,'
      '       items_week_3,'
      '       cur_price_in,'
      '       out_price,'
      '       out_price_nd,'
      '       inc_date_rc,'
      '       inc_date_rtt,'
      '       action_type_name,'
      '       action_begin_date,'
      '       action_end_date,  '
      '       action_out_price, '
      '       pull_date,'
      '       act_price,'
      '       gm,'
      '       sm_mini, '
      '       is_current_action_org,'
      '       capacity'
      '  FROM ('
      '        SELECT a.doc_id AS id,'
      '               a.org_id AS line_id,'
      '               og.name AS grp_name,'
      '               o.name AS org_name,'
      
        '               pkg_rtl_rs_nmcl_model.fnc_get_nmcl_model_org(:NMC' +
        'L_ID, :ASSORT_ID, og.id, o.id) as model_type_id,'
      
        '               trim(rpad(pkg_rtl_rs_nmcl_model.fnc_get_nmcl_mode' +
        'l_org_name(:NMCL_ID, :ASSORT_ID, og.id, o.id),250)) as model_typ' +
        'e,'
      '               a.note,'
      
        '               round((nvl(store_items,0) - nvl(pkg_rtl_purch_org' +
        '_par.fnc_get_window_par(:NMCL_ID, :ASSORT_ID,o.id),0))/NULLIF((i' +
        'tems_week_1+items_week_2+items_week_3)/21, 0),2) as day_store,'
      '               store_items,'
      '               items_week_1,'
      '               items_week_2,'
      '               items_week_3,'
      
        '               nvl(a.cur_price_in, pkg_rtl_prices.fnc_cur_in_pri' +
        'ce(:NMCL_ID, 7, o.id)) as cur_price_in,'
      
        '               nvl(a.out_price, pkg_rtl_prices.fnc_cur_out_price' +
        '(:NMCL_ID,:ASSORT_ID ,o.id)) AS out_price,'
      
        '               nvl(a.out_price_nd, nvl(PKG_RTL_PRICES.fnc_cur_ou' +
        't_price_nd(:NMCL_ID,:ASSORT_ID ,o.id),pkg_rtl_prices.fnc_cur_out' +
        '_price(:NMCL_ID,:ASSORT_ID ,o.id))) as out_price_nd,'
      '               trunc((SELECT max(bin.last_inc_date)'
      '                        FROM cnt_nmcl_last_inc bin'
      '                        WHERE bin.nmcl_id = :NMCL_ID'
      '                        AND bin.assortment_id = :ASSORT_ID'
      '                        AND bin.org_id = 3)) AS inc_date_rc, '
      '               trunc((SELECT max(bin.last_inc_date)'
      '                   FROM cnt_nmcl_last_inc bin'
      '                   WHERE bin.nmcl_id = :NMCL_ID'
      '                   AND bin.assortment_id = :ASSORT_ID'
      '                   AND bin.org_id = o.id)) AS inc_date_rtt, '
      '               act.action_type_name,'
      '               act.begin_date action_begin_date,'
      '               act.end_date action_end_date,  '
      '               act.out_price action_out_price, '
      '               d.pull_date,'
      '               d.act_price,'
      '               ac.id as assort_class_id,'
      
        '                   (select nvl(max(1),0) from groups_organizatio' +
        'ns og, organizations o'
      '                  where  o.id = og.org_id '
      '                   and o.id= a.org_id'
      '                   and og.grp_id = 74) as gm,'
      
        '               (select nvl(max(1),0) from groups_organizations o' +
        'g, organizations o'
      '                  where  o.id = og.org_id '
      '                   and o.id= a.org_id'
      '                   and og.grp_id = 75) as sm_mini,'
      
        '                case when exists(select 1 from rtl_act_price_lis' +
        't ap where ap.nmcl_id=d.nmcl_id and nvl(nullif(ap.assortment_id,' +
        '7),d.assortment_id)= d.assortment_id and org_id=a.org_id) then 1' +
        ' else 0 end is_current_action_org,'
      '                 sc.capacity'
      '          FROM doc_disc_goods_items a,'
      '               doc_disc_goods d,'
      '               groups_organizations g,'
      '               organizations o, '
      '               org_groups og,'
      '               rtl_shelf_capacity sc,'
      '               assort_class_nmcl acn,'
      '               assort_class ac,'
      '               (SELECT sum(store_items) AS store_items,'
      '                       sum(items_week_1) AS items_week_1,'
      '                       sum(items_week_2) AS items_week_2,'
      '                       sum(items_week_3) AS items_week_3,'
      '                       org_id '
      '                     FROM (           '
      '                      SELECT buf.store_items ,'
      '                        cast(NULL AS NUMBER) AS items_week_1,'
      '                        cast(NULL AS NUMBER) AS items_week_2,'
      '                        cast(NULL AS NUMBER) AS items_week_3,'
      '                        buf.org_id'
      '                      FROM buf_nmcl_data buf,'
      '                           mv_buf_nmcl_data mv'
      '                      WHERE  buf.nmcl_id = :NMCL_ID'
      '                       AND buf.assortment_id = :ASSORT_ID'
      '                       AND buf.rep_date = TRUNC(SYSDATE)'
      '                       AND mv.nmcl_id(+) = buf.nmcl_id'
      '                       AND mv.org_id(+) = buf.org_id'
      
        '                       AND mv.assortment_id(+) = buf.assortment_' +
        'id'
      '                     UNION ALL'
      
        '                      SELECT cast(NULL AS NUMBER) AS store_items' +
        ','
      '                        mv.items_week_1,'
      '                        mv.items_week_2,'
      '                        mv.items_week_3,'
      '                        mv.org_id'
      '                     FROM   mv_buf_nmcl_data mv'
      '                       WHERE  mv.nmcl_id = :NMCL_ID'
      '                       AND mv.assortment_id = :ASSORT_ID'
      '                     )'
      '                     GROUP BY org_id ) buf,'
      '               (select act.name as action_type_name, '
      '                       da.begin_date, da.end_date,  '
      '                       da.doc_id , l.out_price, l.org_id'
      '                  from rtl_act_price_list l,'
      '                       rtl_action_types act,'
      '                       doc_action_price da'
      '                 where l.nmcl_id = :NMCL_ID'
      
        '                   and nvl(nullif(l.assortment_id,7), :ASSORT_ID' +
        ') = :ASSORT_ID'
      '                   and l.action_type_id = act.id'
      '                   and da.doc_id = l.doc_id) act'
      '         WHERE a.doc_id = :ID'
      '          AND a.doc_id = d.doc_id'
      '          AND g.grp_id = og.id'
      '          AND o.id = g.org_id'
      '          AND og.type_id = 10'
      '          AND o.id = a.org_id'
      '          AND act.org_id(+) = a.org_id'
      '          AND acn.nmcl_id(+) = d.nmcl_id  '
      '          AND acn.assortment_id(+) = d.assortment_id'
      '          AND acn.org_id(+) is null'
      '          AND ac.id(+) = acn.class_id'
      '           AND buf.org_id(+) = o.id '
      '           AND sc.nmcl_id(+) =  d.nmcl_id  '
      '           AND sc.assortment_id(+) = d.assortment_id'
      '           AND sc.org_id(+) =o.id )'
      '  union all'
      '  select id,'
      '       line_id,'
      '       grp_name,'
      '       org_name,'
      '       model_type,'
      '       null as model_type_id,'
      '       null as note,'
      '       null as fact_margin,'
      '       null as fact_margin_sum,'
      '       null as margin_af_act,'
      '       null as total_profit,'
      '        null as day_store,'
      '        store_items,'
      '        null as items_week_1,'
      '        null as items_week_2,'
      '        null as items_week_3,'
      '        cur_price_in as cur_price_in,'
      '        null as out_price,'
      '        null as out_price_nd,'
      '        null as inc_date_rc,'
      '        null as inc_date_rtt,'
      '        null as action_type_name,'
      '        null as action_begin_date,'
      '        null as action_end_date,  '
      '        null as  action_out_price, '
      '        null as pull_date,'
      '        null as act_price,'
      '       null as gm,'
      '       null as sm_mini,'
      '       null as is_current_action_org,'
      '       null as capasity'
      'from ('
      'select a.doc_id as id,'
      '       3 as line_id,'
      '       '#39'��'#39' as grp_name,'
      '       '#39'�������������'#39' as org_name,'
      '        ac.name as model_type,'
      '        nvl((SELECT buf.store_items '
      '          FROM buf_nmcl_data buf'
      '         WHERE buf.org_id = 3 '
      '           AND buf.nmcl_id = a.nmcl_id'
      '           AND buf.assortment_id = a.assortment_id'
      '           AND buf.rep_date = TRUNC(SYSDATE)),0) as store_items,'
      '        a.act_price as act_price,'
      
        '        nvl(pkg_goods_prices.fnc_cur_price_in(a.nmcl_id, 3, a.as' +
        'sortment_id),0) as cur_price_in'
      '  from doc_disc_goods a,'
      '        assort_class_nmcl acn,'
      '        assort_class ac '
      ' where a.doc_id = :ID'
      '   and acn.nmcl_id = a.nmcl_id'
      '   and acn.assortment_id = a.assortment_id'
      '   and acn.org_id is null '
      '   and acn.class_id = ac.id'
      '   and acn.class_id = 10'
      '   ) '
      '  )v'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'nmcl_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'assort_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    inherited m_Q_linesID: TFloatField
      Origin = 'v.ID'
    end
    inherited m_Q_linesLINE_ID: TFloatField
      Origin = 'v.LINE_ID'
    end
    object m_Q_linesGRP_NAME: TStringField
      DisplayLabel = '����'
      FieldName = 'GRP_NAME'
      Origin = 'v.GRP_NAME'
      Size = 250
    end
    object m_Q_linesORG_NAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'ORG_NAME'
      Origin = 'v.ORG_NAME'
      Size = 250
    end
    object m_Q_linesMODEL_TYPE: TStringField
      DisplayLabel = '�������'
      FieldName = 'MODEL_TYPE'
      Origin = 'v.MODEL_TYPE'
      Size = 2
    end
    object m_Q_linesMODEL_TYPE_ID: TFloatField
      FieldName = 'MODEL_TYPE_ID'
      Origin = 'v.MODEL_TYPE_ID'
    end
    object m_Q_linesNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'v.NOTE'
      Size = 250
    end
    object m_Q_linesFACT_MARGIN_SUM: TFloatField
      DisplayLabel = '����������� ������� �� �����������. ���� | ���.'
      FieldName = 'FACT_MARGIN_SUM'
      Origin = 'v.FACT_MARGIN_SUM'
      currency = True
    end
    object m_Q_linesFACT_MARGIN: TFloatField
      DisplayLabel = '����������� ������� �� �����������. ���� | %'
      FieldName = 'FACT_MARGIN'
      Origin = 'v.FACT_MARGIN'
    end
    object m_Q_linesDAY_STORE: TFloatField
      DisplayLabel = '����� ������ � ����'
      FieldName = 'DAY_STORE'
      Origin = 'v.DAY_STORE'
    end
    object m_Q_linesSTORE_ITEMS: TFloatField
      DisplayLabel = '�������'
      FieldName = 'STORE_ITEMS'
      Origin = 'v.STORE_ITEMS'
    end
    object m_Q_linesITEMS_WEEK_1: TFloatField
      DisplayLabel = '�������|1 ���.'
      FieldName = 'ITEMS_WEEK_1'
      Origin = 'v.ITEMS_WEEK_1'
    end
    object m_Q_linesITEMS_WEEK_2: TFloatField
      DisplayLabel = '�������|2 ���.'
      FieldName = 'ITEMS_WEEK_2'
      Origin = 'v.ITEMS_WEEK_2'
    end
    object m_Q_linesITEMS_WEEK_3: TFloatField
      DisplayLabel = '�������|3 ���.'
      FieldName = 'ITEMS_WEEK_3'
      Origin = 'v.ITEMS_WEEK_3'
    end
    object m_Q_linesCUR_PRICE_IN: TFloatField
      DisplayLabel = '����*'
      FieldName = 'CUR_PRICE_IN'
      Origin = 'v.CUR_PRICE_IN'
      currency = True
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '���� ���������� �� �����'
      FieldName = 'OUT_PRICE'
      Origin = 'v.OUT_PRICE'
      currency = True
    end
    object m_Q_linesOUT_PRICE_ND: TFloatField
      DisplayLabel = '���� ���������� "��� �����"'
      FieldName = 'OUT_PRICE_ND'
      Origin = 'v.OUT_PRICE_ND'
      currency = True
    end
    object m_Q_linesINC_DATE_RTT: TDateTimeField
      DisplayLabel = '���� ���������� ������� �� | ���'
      FieldName = 'INC_DATE_RTT'
      Origin = 'v.INC_DATE_RTT'
    end
    object m_Q_linesINC_DATE_RC: TDateTimeField
      DisplayLabel = '���� ���������� ������� �� | ��'
      FieldName = 'INC_DATE_RC'
      Origin = 'v.INC_DATE_RC'
    end
    object m_Q_linesACTION_TYPE_NAME: TStringField
      DisplayLabel = '����������� ����� | ���'
      FieldName = 'ACTION_TYPE_NAME'
      Origin = 'v.ACTION_TYPE_NAME'
      Size = 250
    end
    object m_Q_linesACTION_BEGIN_DATE: TDateTimeField
      DisplayLabel = '����������� ����� | ������ �������� | c'
      FieldName = 'ACTION_BEGIN_DATE'
      Origin = 'v.ACTION_BEGIN_DATE'
    end
    object m_Q_linesACTION_END_DATE: TDateTimeField
      DisplayLabel = '����������� ����� | ������ �������� | ��'
      FieldName = 'ACTION_END_DATE'
      Origin = 'v.ACTION_END_DATE'
    end
    object m_Q_linesACTION_OUT_PRICE: TFloatField
      DisplayLabel = '����������� ����� | ���� �������'
      FieldName = 'ACTION_OUT_PRICE'
      Origin = 'v.ACTION_OUT_PRICE'
      currency = True
    end
    object m_Q_linesPULL_DATE: TDateTimeField
      DisplayLabel = '���� �������� ��'
      FieldName = 'PULL_DATE'
      Origin = 'v.PULL_DATE'
    end
    object m_Q_linesMARGIN_AF_ACT: TFloatField
      DisplayLabel = '% ������� ����� ������� ������'
      FieldName = 'MARGIN_AF_ACT'
      Origin = 'v.MARGIN_AF_ACT'
    end
    object m_Q_linesTOTAL_PROFIT: TFloatField
      DisplayLabel = '�������� ������� �������'
      FieldName = 'TOTAL_PROFIT'
      Origin = 'v.TOTAL_PROFIT'
      currency = True
    end
    object m_Q_linesACT_PRICE: TFloatField
      FieldName = 'ACT_PRICE'
      Origin = 'v.ACT_PRICE'
    end
    object m_Q_linesMIN_MARGIN_AF_ACT: TFloatField
      FieldName = 'MIN_MARGIN_AF_ACT'
      Origin = 'min(margin_af_act) over (partition BY id)'
    end
    object m_Q_linesMAX_TOTAL_PROFIT: TFloatField
      FieldName = 'MAX_TOTAL_PROFIT'
      Origin = 'max(total_profit) over (partition BY id)'
    end
    object m_Q_linesIS_CURRENT_ACTION_ORG: TFloatField
      DisplayLabel = '��� � ����������� ������ �� �������'
      FieldName = 'IS_CURRENT_ACTION_ORG'
      Visible = False
    end
    object m_Q_linesGM: TFloatField
      DisplayLabel = '�����������'
      FieldName = 'GM'
    end
    object m_Q_linesSM_MINI: TFloatField
      DisplayLabel = '�� ��� ����'
      FieldName = 'SM_MINI'
    end
    object m_Q_linesCAPACITY: TFloatField
      DisplayLabel = '����������� �����, ��.'
      FieldName = 'CAPACITY'
    end
    object m_Q_linesMAX_LINE_ID: TFloatField
      FieldName = 'MAX_LINE_ID'
    end
  end
  inherited m_Q_line: TMLQuery
    SQL.Strings = (
      'SELECT a.doc_id AS id,'
      '       a.org_id AS line_id,'
      '       o.name AS org_name,'
      '       a.note,'
      '       g.grp_id'
      'FROM doc_disc_goods_items a, groups_organizations g,'
      '     organizations o, org_groups og'
      'WHERE a.doc_id = :ID'
      '      AND g.grp_id = og.id'
      '      AND o.id = g.org_id'
      '      AND og.type_id = 10'
      '      AND o.id = a.org_id'
      '      AND a.org_id = :LINE_ID'
      ' '
      ' '
      ' ')
    object m_Q_lineORG_NAME: TStringField
      FieldName = 'ORG_NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
    object m_Q_lineNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.DOC_ACTION_PRICE_ITEMS.NOTE'
      Size = 250
    end
    object m_Q_lineGRP_ID: TFloatField
      FieldName = 'GRP_ID'
      Origin = 'TSDBMAIN.GROUPS_ORGANIZATIONS.GRP_ID'
    end
  end
  inherited m_U_line: TMLUpdateSQL
    ModifySQL.Strings = (
      'update doc_disc_goods_items'
      'set'
      '  NOTE = :NOTE'
      'where'
      '  DOC_ID = :OLD_ID and'
      '  ORG_ID = :OLD_LINE_ID')
    InsertSQL.Strings = (
      'BEGIN'
      '  prc_add_doc_disc_goods_items(:GRP_ID, :LINE_ID, :ID, :NOTE);'
      'END;')
    DeleteSQL.Strings = (
      'delete from doc_disc_goods_items'
      'where'
      '  DOC_ID = :OLD_ID and'
      '  ORG_ID = :OLD_LINE_ID')
    IgnoreRowsAffected = True
  end
  object m_Q_nmcls: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT ni.id, ni.name'
      'FROM nomenclature_items ni'
      'WHERE EXISTS (SELECT a.nmcl_id FROM rtl_price_list a'
      '          WHERE a.nmcl_id = ni.id)'
      '%WHERE_CLAUSE'
      'ORDER BY ni.name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 44
    Top = 304
    object m_Q_nmclsID: TFloatField
      DisplayWidth = 20
      FieldName = 'ID'
      Origin = 'ni.id'
    end
    object m_Q_nmclsNAME: TStringField
      DisplayLabel = '�����'
      FieldName = 'NAME'
      Origin = 'ni.name'
      Size = 100
    end
  end
  object m_Q_acts: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT a.id, a.name, a.note'
      'FROM rtl_action_types a'
      'WHERE enabled = '#39'Y'#39
      'and id in (7, 26, 27)'
      '%WHERE_CLAUSE'
      'order by 2')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 124
    Top = 303
    object m_Q_actsID: TFloatField
      DisplayWidth = 35
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_actsNAME: TStringField
      DisplayLabel = '���'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
    object m_Q_actsNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Size = 250
    end
  end
  object m_Q_assorts: TMLQuery
    BeforeOpen = m_Q_assortsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select id, name'
      ' from('
      'SELECT DISTINCT a.id AS id,'
      '       a.name AS name'
      'FROM nmcl_bar_codes nbc,'
      '     assortment a'
      'WHERE nbc.nmcl_id = :NMCL_ID'
      '  AND a.id = nbc.assortment_id'
      '  AND a.id <> 7'
      'UNION ALL'
      'SELECT id,'
      '             name'
      'FROM assortment'
      'WHERE id = 7)'
      '%WHERE_EXPRESSION'
      'ORDER BY name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 43
    Top = 368
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_assortsID: TFloatField
      DisplayWidth = 20
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_assortsNAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 100
    end
  end
  object m_Q_orgs: TMLQuery
    BeforeOpen = m_Q_orgsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name FROM ('
      'SELECT -1 AS id,'
      '       '#39'���'#39' AS name'
      'FROM dual'
      'UNION ALL'
      'SELECT o.id AS id,'
      '       o.name AS name'
      'FROM groups_organizations g,'
      '     organizations o, org_groups og'
      'WHERE g.grp_id = og.id'
      '  AND o.id = g.org_id'
      '  AND o.enable = '#39'Y'#39
      '  AND og.type_id = 10'
      '  AND g.grp_id = :GRP_ID'
      
        '  AND NOT EXISTS (SELECT a.org_id FROM doc_disc_goods_items a WH' +
        'ERE a.doc_id = :DOC_ID AND a.org_id = o.id)'
      ')'
      '%WHERE_EXPRESSION'
      'ORDER BY id'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 319
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'GRP_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_orgsID: TFloatField
      DisplayLabel = 'Id'
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_orgsNAME: TStringField
      DisplayLabel = '��'
      FieldName = 'NAME'
      Origin = 'name'
      Size = 250
    end
  end
  object m_Q_org_groups: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT og.id,'
      '       og.name'
      'FROM org_groups og'
      'WHERE og.type_id = 10'
      'AND exists'
      '(SELECT 1'
      'FROM groups_organizations g,'
      '     organizations o'
      'WHERE g.grp_id = og.id'
      '  AND o.id = g.org_id'
      '  AND o.enable = '#39'Y'#39')'
      'ORDER BY og.name')
    Left = 410
    Top = 344
    object m_Q_org_groupsID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_org_groupsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
  end
  object m_Q_is_assorted: TMLQuery
    BeforeOpen = m_Q_is_assortedBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select sign(count(*)) val from rtl_nmcl_assort_rel a'
      'where a.nmcl_id = :NMCL_ID and a.end_date is null'
      ' ')
    Macros = <>
    Left = 592
    Top = 400
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_is_assortedVAL: TFloatField
      FieldName = 'VAL'
    end
  end
  object m_Q_sel_orgs: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_sel_orgsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT o.id AS id,'
      '       o.name AS name,'
      '       2 as checked'
      'FROM groups_organizations g,'
      '     organizations o, org_groups og'
      'WHERE g.grp_id = og.id'
      '  AND o.id = g.org_id'
      '  AND o.enable = '#39'Y'#39
      '  AND og.type_id = 10'
      
        '  AND NOT EXISTS (SELECT a.org_id FROM doc_disc_goods_items a WH' +
        'ERE a.doc_id = :DOC_ID AND a.org_id = o.id)'
      '%WHERE_CLAUSE')
    UpdateObject = m_U_sel_orgs
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 213
    Top = 424
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_sel_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'o.id'
    end
    object m_Q_sel_orgsNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'o.name'
      Size = 100
    end
    object m_Q_sel_orgsCHECKED: TFloatField
      DisplayLabel = '�������'
      FieldName = 'CHECKED'
    end
  end
  object m_U_sel_orgs: TMLUpdateSQL
    Left = 320
    Top = 400
  end
  object m_Q_ins_orgs: TMLQuery
    BeforeOpen = m_Q_is_assortedBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'INSERT INTO doc_disc_goods_items(doc_id, org_id)'
      'VALUES(:DOC_ID, :ORG_ID)')
    Macros = <>
    Left = 304
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DOC_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'ORG_ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_sel_org: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select * from('
      '  SELECT o.id,'
      '       o.name,'
      '       1 AS im_index,'
      '       :ch AS checked,'
      '       to_number(1) as master_id'
      '  FROM organizations o'
      '  WHERE o.enable = '#39'Y'#39' AND org_type_id = 2'
      
        '  AND (:ALL_RTT = 1 or EXISTS (SELECT 1 FROM doc_disc_goods_item' +
        's a WHERE a.doc_id = :DOC_ID AND a.org_id = o.id)))'
      '%WHERE_EXPRESSION'
      'ORDER BY name'
      ' '
      ' '
      ' ')
    UpdateObject = m_U_sel_org
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 411
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ch'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ALL_RTT'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_sel_orgID: TFloatField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object m_Q_sel_orgNAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 250
    end
    object m_Q_sel_orgIM_INDEX: TFloatField
      FieldName = 'IM_INDEX'
    end
    object m_Q_sel_orgCHECKED: TFloatField
      DisplayLabel = '�������'
      FieldName = 'CHECKED'
    end
    object m_Q_sel_orgMASTER_ID: TFloatField
      FieldName = 'MASTER_ID'
    end
  end
  object m_U_sel_org: TMLUpdateSQL
    Left = 501
    Top = 512
  end
  object m_Q_folders_next: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT v.src_fldr_id,'
      '       v.id, '
      '       v.dest_fldr_id, '
      '       v.name,'
      '       v.route_next_right'
      'FROM ('
      '      SELECT fr.src_fldr_id, '
      '             fr.id, '
      '             fr.dest_fldr_id, '
      '             fr.name,'
      '             fr.sort_id,'
      
        '             rpad(fnc_mask_route_right(fr.id,:ROUTE_ACCESS_MASK,' +
        ':ID),250) AS route_next_right'
      '      FROM fldr_routes fr,'
      '           folders f_s,'
      '           folders f_e'
      
        '      WHERE fr.src_fldr_id = (SELECT fldr_id FROM documents WHER' +
        'E id = :ID) AND'
      '            f_s.id = fr.src_fldr_id AND'
      '            f_e.id = fr.dest_fldr_id AND'
      '            f_e.sort_id > f_s.sort_id'
      '     ) v'
      'WHERE v.route_next_right = 1 '
      'ORDER BY v.sort_id'
      ' ')
    Macros = <>
    Left = 699
    Top = 244
    ParamData = <
      item
        DataType = ftString
        Name = 'ROUTE_ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_folders_nextSRC_FLDR_ID: TFloatField
      FieldName = 'SRC_FLDR_ID'
    end
    object m_Q_folders_nextID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_folders_nextDEST_FLDR_ID: TFloatField
      FieldName = 'DEST_FLDR_ID'
    end
    object m_Q_folders_nextNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object m_Q_folders_nextROUTE_NEXT_RIGHT: TStringField
      FieldName = 'ROUTE_NEXT_RIGHT'
      Size = 250
    end
  end
  object m_Q_folders_prev: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT v.src_fldr_id,'
      '       v.id, '
      '       v.dest_fldr_id, '
      '       v.name,'
      '       v.route_next_right'
      'FROM ('
      '      SELECT fr.src_fldr_id, '
      '             fr.id, '
      '             fr.dest_fldr_id,'
      '             fr.name,'
      '             fr.sort_id,'
      
        '             rpad(fnc_mask_route_right(fr.id,:ROUTE_ACCESS_MASK,' +
        ':ID),250) AS route_next_right'
      '      FROM fldr_routes fr,'
      '           folders f_s,'
      '           folders f_e'
      
        '      WHERE fr.src_fldr_id = (SELECT fldr_id FROM documents WHER' +
        'E id = :ID) AND'
      '            f_s.id = fr.src_fldr_id AND'
      '            f_e.id = fr.dest_fldr_id AND'
      '            f_e.sort_id < f_s.sort_id'
      '     ) v'
      'WHERE v.route_next_right = 1 '
      'ORDER BY v.sort_id'
      ' '
      ' ')
    Macros = <>
    Left = 590
    Top = 244
    ParamData = <
      item
        DataType = ftString
        Name = 'ROUTE_ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_folders_prevSRC_FLDR_ID: TFloatField
      FieldName = 'SRC_FLDR_ID'
    end
    object m_Q_folders_prevID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_folders_prevDEST_FLDR_ID: TFloatField
      FieldName = 'DEST_FLDR_ID'
    end
    object m_Q_folders_prevNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object m_Q_folders_prevROUTE_NEXT_RIGHT: TStringField
      FieldName = 'ROUTE_NEXT_RIGHT'
      Size = 250
    end
  end
  object m_Q_sub_line: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_sub_lineBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select b.doc_id,b.nmcl_id,ni.name as nmcl_name,b.assort_id,a.nam' +
        'e as assort_name '
      
        'from doc_action_price_bogof b, nomenclature_items ni, assortment' +
        ' a'
      'where b.nmcl_id = ni.id'
      '  and b.assort_id = a.id'
      '  and doc_id = :id'
      'and nmcl_id = :NMCL_ID'
      'and assort_id = :ASSORT_ID'
      ' '
      ' '
      ' '
      ' ')
    Macros = <>
    Left = 678
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end>
    object m_Q_sub_lineNMCL_ID: TFloatField
      DisplayLabel = '��� ������'
      FieldName = 'NMCL_ID'
      Origin = 'b.NMCL_ID'
    end
    object m_Q_sub_lineNMCL_NAME: TStringField
      DisplayLabel = '������������ ������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.NAME'
      Size = 100
    end
    object m_Q_sub_lineASSORT_ID: TFloatField
      DisplayLabel = '��� ������������'
      FieldName = 'ASSORT_ID'
      Origin = 'b.ASSORT_ID'
    end
    object m_Q_sub_lineASSORT_NAME: TStringField
      DisplayLabel = '������������ ������������'
      FieldName = 'ASSORT_NAME'
      Origin = 'a.NAME'
      Size = 100
    end
    object m_Q_sub_lineDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Origin = 'b.doc_id'
    end
  end
  object m_Q_wrk_par: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT'
      
        '( select TO_NUMBER(const_number) from constants where const_name' +
        ' = '#39'DOC_ACTION_WORK_FLDR'#39' ) as fldr,'
      
        '( select TO_NUMBER(const_number) from constants where const_name' +
        ' = '#39'DOC_ACTION_WORK_ROUTE'#39' ) as route'
      'FROM DUAL')
    Left = 768
    Top = 132
    object m_Q_wrk_parFLDR: TFloatField
      FieldName = 'FLDR'
    end
    object m_Q_wrk_parROUTE: TFloatField
      FieldName = 'ROUTE'
    end
  end
  object m_Q_del_rtt: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'delete from doc_disc_goods_items'
      'where DOC_ID = :ID and'
      'ORG_ID = :LINE_ID'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Macros = <>
    Left = 44
    Top = 424
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'LINE_ID'
        ParamType = ptUnknown
      end>
  end
  object m_Q_ins_all_rtt: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'INSERT INTO doc_disc_goods_items(doc_id, org_id)'
      'SELECT :DOC_ID as doc_id, o.id as org_id'
      'FROM groups_organizations g, organizations o, org_groups og'
      'WHERE g.grp_id = og.id'
      '  AND o.id = g.org_id'
      '  AND o.enable = '#39'Y'#39
      '  AND og.type_id = 10'
      '  AND o.org_type_id = 2'
      
        '  AND NOT EXISTS (SELECT a.org_id FROM doc_disc_goods_items a WH' +
        'ERE a.doc_id = :DOC_ID AND a.org_id = o.id)')
    Macros = <>
    Left = 421
    Top = 400
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_upd_rtt: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      
        '  retail.prc_rtl_doc_disc_goods_add_rtt(:sq_doc_id, :sq_org_id, ' +
        ':act_price, :price_bef_act, :begin_date, :end_date, :urgent_repr' +
        'ice);'
      'end;'
      ' ')
    Macros = <>
    Left = 752
    Top = 536
    ParamData = <
      item
        DataType = ftInteger
        Name = 'sq_doc_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'sq_org_id'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'act_price'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'price_bef_act'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'urgent_reprice'
        ParamType = ptInput
      end>
  end
  object m_Q_orgs_list: TMLQuery
    BeforeOpen = m_Q_orgs_listBeforeOpen
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_list
    SQL.Strings = (
      'SELECT * '
      '  FROM ('
      '    SELECT org_id,'
      '           org_name,'
      '           store_items,'
      '           day_store,'
      '           model_type,'
      
        '           round(((act_price - cur_price_in) / nullif(cur_price_' +
        'in,0))*100,2) as margin_af_act,'
      
        '           case when ((act_price - cur_price_in) / nullif(cur_pr' +
        'ice_in,0)) < 0 '
      
        '                     then round(((act_price - cur_price_in)* nvl' +
        '(store_items,0)),2)  '
      '                else null'
      '           end total_profit,'
      '           out_price,'
      '           out_price_nd,'
      '           last_inc_price_rtt,'
      '           action_type_name,'
      '           action_begin_date,'
      '           action_end_date,  '
      '           action_out_price, '
      '           items_week_1,'
      '           items_week_2,'
      '           items_week_3,'
      '           price_no_act,'
      '           cur_price_in,'
      '           act_price'
      '      FROM'
      '      ('
      '        SELECT pi.org_id,'
      '               o.name org_name,'
      '               buf.store_items, '
      
        '               round((nvl(store_items,0) - nvl(pkg_rtl_purch_org' +
        '_par.fnc_get_window_par(:NMCL_ID, :ASSORT_ID,o.id),0))/NULLIF((i' +
        'tems_week_1+items_week_2+items_week_3)/21, 0),2) as day_store,'
      
        '               trim(rpad(pkg_rtl_rs_nmcl_model.fnc_get_nmcl_mode' +
        'l_org_name(:NMCL_ID, :ASSORT_ID, null, o.id),250)) as model_type' +
        ','
      
        '               nvl(pi.cur_price_in, pkg_rtl_prices.fnc_cur_in_pr' +
        'ice(:NMCL_ID, 7, o.id)) as cur_price_in,'
      
        '               nvl(pi.out_price, pkg_rtl_prices.fnc_cur_out_pric' +
        'e(:NMCL_ID,:ASSORT_ID ,o.id)) AS out_price,'
      
        '               nvl(pi.out_price_nd, PKG_RTL_PRICES.fnc_cur_out_p' +
        'rice_nd(:NMCL_ID,:ASSORT_ID ,o.id)) as out_price_nd,'
      
        '               trunc((SELECT MAX(bin.last_inc_price) KEEP (dense' +
        '_rank last order by last_inc_date) AS last_inc_price'
      '                         FROM cnt_nmcl_last_inc bin'
      '                         WHERE bin.nmcl_id = :NMCL_ID'
      '                         AND bin.assortment_id = :ASSORT_ID'
      
        '                         AND bin.org_id = o.id)) AS last_inc_pri' +
        'ce_rtt,'
      
        '                pkg_rtl_prices.fnc_price_no_act(:NMCL_ID, :ASSOR' +
        'T_ID, o.id) as price_no_act,'
      '                act.action_type_name,'
      '                act.begin_date action_begin_date,'
      '                act.end_date action_end_date,  '
      '                act.out_price action_out_price,'
      '                d.act_price,'
      '                ac.id as assort_class_id,'
      '                items_week_1,'
      '                items_week_2,'
      '                items_week_3'
      '         FROM doc_disc_goods_items pi, '
      '              doc_disc_goods d,'
      '              organizations o,'
      '              assort_class_nmcl acn,'
      '              assort_class ac,'
      '             (SELECT sum(store_items) AS store_items,'
      '                             sum(items_week_1) AS items_week_1,'
      '                             sum(items_week_2) AS items_week_2,'
      '                             sum(items_week_3) AS items_week_3,'
      '                             org_id '
      '                           FROM (           '
      '                            SELECT buf.store_items ,'
      
        '                              cast(NULL AS NUMBER) AS items_week' +
        '_1,'
      
        '                              cast(NULL AS NUMBER) AS items_week' +
        '_2,'
      
        '                              cast(NULL AS NUMBER) AS items_week' +
        '_3,'
      '                              buf.org_id'
      '                            FROM buf_nmcl_data buf,'
      '                                 mv_buf_nmcl_data mv'
      '                            WHERE  buf.nmcl_id = :NMCL_ID'
      '                             AND buf.assortment_id = :ASSORT_ID'
      '                             AND buf.rep_date = TRUNC(SYSDATE)'
      '                             AND mv.nmcl_id(+) = buf.nmcl_id'
      '                             AND mv.org_id(+) = buf.org_id'
      
        '                             AND mv.assortment_id(+) = buf.assor' +
        'tment_id'
      '                           UNION ALL'
      
        '                            SELECT cast(NULL AS NUMBER) AS store' +
        '_items,'
      '                              mv.items_week_1,'
      '                              mv.items_week_2,'
      '                              mv.items_week_3,'
      '                              mv.org_id'
      '                           FROM   mv_buf_nmcl_data mv'
      '                             WHERE  mv.nmcl_id = :NMCL_ID'
      '                             AND mv.assortment_id = :ASSORT_ID'
      '                           )'
      '                           GROUP BY org_id ) buf,'
      '                     (select act.name as action_type_name, '
      '                             da.begin_date, da.end_date,  '
      '                             da.doc_id , l.out_price, l.org_id'
      '                        from rtl_act_price_list l,'
      '                             rtl_action_types act,'
      '                             doc_action_price da'
      '                       where l.nmcl_id = :NMCL_ID'
      
        '                         and nvl(nullif(l.assortment_id,7), :ASS' +
        'ORT_ID) = :ASSORT_ID'
      '                         and l.action_type_id = act.id'
      '                         and da.doc_id = l.doc_id) act'
      '         WHERE pi.doc_id = :ID'
      '         AND pi.doc_id = d.doc_id'
      '         and o.id = pi.org_id'
      '         and o.org_type_id = 2'
      '         and o.enable = '#39'Y'#39
      '         AND act.org_id(+) = o.id'
      '         AND buf.org_id(+) = o.id '
      '         AND acn.nmcl_id(+) = d.nmcl_id  '
      '         AND acn.assortment_id(+) = d.assortment_id'
      '         AND acn.org_id(+) is null'
      '         AND ac.id(+) = acn.class_id)'
      '    union all'
      '    SELECT  3 as org_id,'
      '            org_name,'
      '            store_items,'
      '            null as day_store,'
      '            model_type,'
      '            null as margin_af_act,'
      '            null as total_profit,'
      '            null as out_price,'
      '            null as out_price_nd,'
      '            null as last_inc_price_rtt,'
      '            null as action_type_name,'
      '            null as action_begin_date,'
      '            null as action_end_date,  '
      '            null as  action_out_price, '
      '            null as items_week_1,'
      '            null as items_week_2,'
      '            null as items_week_3,'
      '            null as price_no_act,'
      '            null as cur_price_in,'
      '            null as act_price'
      '      FROM ('
      '        SELECT a.doc_id as id,'
      '               3 as line_id,'
      '               '#39'��'#39' as grp_name,'
      '               '#39'�������������'#39' as org_name,'
      '                ac.name as model_type,'
      '                nvl((SELECT buf.store_items '
      '                  FROM buf_nmcl_data buf'
      '                 WHERE buf.org_id = 3 '
      '                   AND buf.nmcl_id = a.nmcl_id'
      '                   AND buf.assortment_id = a.assortment_id'
      
        '                   AND buf.rep_date = TRUNC(SYSDATE)),0) as stor' +
        'e_items,'
      '                a.act_price as act_price,'
      
        '                nvl(pkg_goods_prices.fnc_cur_price_in(a.nmcl_id,' +
        ' 3, a.assortment_id),0) as cur_price_in'
      '          from doc_disc_goods a,'
      '                assort_class_nmcl acn,'
      '                assort_class ac '
      '         where a.doc_id = :ID'
      '           and acn.nmcl_id = a.nmcl_id'
      '           and acn.assortment_id = a.assortment_id'
      '           and acn.org_id is null '
      '           and acn.class_id = ac.id'
      '           and acn.class_id = 10'
      '           )'
      '      )'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 624
    Top = 536
    ParamData = <
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_orgs_listORG_ID: TFloatField
      DisplayLabel = 'ID ���'
      FieldName = 'ORG_ID'
      Origin = 'ORG_ID'
    end
    object m_Q_orgs_listORG_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'ORG_NAME'
      Origin = 'ORG_NAME'
      Size = 250
    end
    object m_Q_orgs_listSTORE_ITEMS: TFloatField
      DisplayLabel = '������� �� ������ ���'
      FieldName = 'STORE_ITEMS'
      Origin = 'STORE_ITEMS'
    end
    object m_Q_orgs_listMODEL_TYPE: TStringField
      DisplayLabel = '������� �� ���'
      FieldName = 'MODEL_TYPE'
      Origin = 'MODEL_TYPE'
      Size = 250
    end
    object m_Q_orgs_listDAY_STORE: TFloatField
      DisplayLabel = '����� ������ � ����'
      FieldName = 'DAY_STORE'
      Origin = 'DAY_STORE'
    end
    object m_Q_orgs_listOUT_PRICE: TFloatField
      DisplayLabel = '���� �� �����'
      FieldName = 'OUT_PRICE'
      Origin = 'OUT_PRICE'
      currency = True
    end
    object m_Q_orgs_listOUT_PRICE_ND: TFloatField
      DisplayLabel = '���� ��� �����'
      FieldName = 'OUT_PRICE_ND'
      Origin = 'OUT_PRICE_ND'
      currency = True
    end
    object m_Q_orgs_listLAST_INC_PRICE_RTT: TFloatField
      DisplayLabel = '���� ���������� ������� ������ �� ���'
      FieldName = 'LAST_INC_PRICE_RTT'
      Origin = 'LAST_INC_PRICE_RTT'
      currency = True
    end
    object m_Q_orgs_listACTION_TYPE_NAME: TStringField
      DisplayLabel = '����������� ����� | ���'
      FieldName = 'ACTION_TYPE_NAME'
      Origin = 'ACTION_TYPE_NAME'
      Size = 250
    end
    object m_Q_orgs_listACTION_BEGIN_DATE: TDateTimeField
      DisplayLabel = '����������� ����� | ������ �������� | c'
      FieldName = 'ACTION_BEGIN_DATE'
      Origin = 'ACTION_BEGIN_DATE'
    end
    object m_Q_orgs_listACTION_END_DATE: TDateTimeField
      DisplayLabel = '����������� ����� | ������ �������� | ��'
      FieldName = 'ACTION_END_DATE'
      Origin = 'ACTION_END_DATE'
    end
    object m_Q_orgs_listACTION_OUT_PRICE: TFloatField
      DisplayLabel = '����������� ����� | ���� �������'
      FieldName = 'ACTION_OUT_PRICE'
      Origin = 'ACTION_END_DATE'
      currency = True
    end
    object m_Q_orgs_listMARGIN_AF_ACT: TFloatField
      DisplayLabel = '% ������� ����� ������� ������'
      FieldName = 'MARGIN_AF_ACT'
      Origin = 'ACTION_END_DATE'
    end
    object m_Q_orgs_listITEMS_WEEK_1: TFloatField
      DisplayLabel = '�������|1 ���.'
      FieldName = 'ITEMS_WEEK_1'
      Origin = 'ACTION_END_DATE'
    end
    object m_Q_orgs_listITEMS_WEEK_2: TFloatField
      DisplayLabel = '�������|2 ���.'
      FieldName = 'ITEMS_WEEK_2'
      Origin = 'ACTION_END_DATE'
    end
    object m_Q_orgs_listITEMS_WEEK_3: TFloatField
      DisplayLabel = '�������|3 ���.'
      FieldName = 'ITEMS_WEEK_3'
      Origin = 'ACTION_END_DATE'
    end
    object m_Q_orgs_listTOTAL_PROFIT: TFloatField
      DisplayLabel = '�������� ������� �������'
      FieldName = 'TOTAL_PROFIT'
      Origin = 'TOTAL_PROFIT'
      currency = True
    end
    object m_Q_orgs_listPRICE_NO_ACT: TFloatField
      DisplayLabel = '���� ������� �� �����'
      FieldName = 'PRICE_NO_ACT'
      Origin = 'PRICE_NO_ACT'
    end
    object m_Q_orgs_listCUR_PRICE_IN: TFloatField
      DisplayLabel = '���������������� ����'
      FieldName = 'CUR_PRICE_IN'
    end
    object m_Q_orgs_listACT_PRICE: TFloatField
      DisplayLabel = '���� �� �����'
      FieldName = 'ACT_PRICE'
    end
  end
  object m_Q_reasons: TMLQuery
    BeforeOpen = m_Q_reasonsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT r.id, r.name, r.note'
      'FROM rtl_reason_disc r,'
      '     rtl_act_type_reason ra'
      'WHERE enabled = '#39'Y'#39
      '  AND r.id = ra.disc_reason_id '
      '  AND ra.act_type_id = :ACT_TYPE_ID'
      '%WHERE_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 124
    Top = 367
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ACT_TYPE_ID'
        ParamType = ptInput
      end>
    object m_Q_reasonsID: TFloatField
      DisplayWidth = 20
      FieldName = 'ID'
      Origin = 'r.ID'
    end
    object m_Q_reasonsNAME: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'NAME'
      Origin = 'r.NAME'
      Size = 250
    end
    object m_Q_reasonsNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.RTL_REASON_DISC.NOTE'
      Visible = False
      Size = 250
    end
  end
  object m_Q_route: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select fnc_get_route(:SRC_FLDR_ID, :DEST_FLDR_ID) as route_id fr' +
        'om dual')
    Macros = <>
    Left = 582
    Top = 300
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SRC_FLDR_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DEST_FLDR_ID'
        ParamType = ptInput
      end>
    object m_Q_routeROUTE_ID: TFloatField
      FieldName = 'ROUTE_ID'
    end
  end
  object m_Q_reason_rejection: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select reason_rejection as note from doc_disc_goods where doc_id' +
        ' =-1')
    UpdateObject = m_U_reason_rejection
    Macros = <>
    Left = 41
    Top = 496
    object m_Q_reason_rejectionNOTE: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'NOTE'
      Origin = 'reason_rejection'
      Size = 250
    end
  end
  object m_U_reason_rejection: TMLUpdateSQL
    ModifySQL.Strings = (
      'begin'
      '    update doc_disc_goods'
      '       set reason_rejection = :NOTE'
      '     where doc_id = :DOC_ID;'
      'end;')
    IgnoreRowsAffected = True
    Left = 155
    Top = 496
  end
  object m_Q_edit_reason_rejection: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '    update doc_disc_goods'
      '       set reason_rejection = :NOTE'
      '     where doc_id = :DOC_ID;'
      'end;')
    Macros = <>
    Left = 65
    Top = 576
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NOTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m1: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'NOTE'
      Origin = 'reason_rejection'
      Size = 250
    end
  end
  object m_Q_chk_docs: TMLQuery
    BeforeOpen = m_Q_chk_docsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'with cur as  (select dg1.nmcl_id, dg1.assortment_id, dgi1.org_id' +
        ','
      '                      dg1.begin_date, dg1.end_date'
      '                 from doc_disc_goods dg1,'
      '                      doc_disc_goods_items dgi1'
      '                where dg1.doc_id = :DOC_ID'
      '                  and dg1.doc_id = dgi1.doc_id)'
      '                  '
      'select * '
      ' from ('
      'select count(*) as c,'
      
        '       trim(rpad(max('#39'�� ������ ������������ '#39' || dg.nmcl_id || ' +
        #39' � ����� '#39' || o.name || '#39' ���� ����������� � ���������� "������' +
        ' ������� �� ���" � ����� '#39' || f.name || '#39' (Id ��������� = '#39' || d' +
        'g.doc_id || '#39'). ����������?'#39' ),250)) as note'
      '  from doc_disc_goods dg,'
      '       documents d,'
      '       folders f,'
      '       doc_disc_goods_items dgi,'
      '       organizations o, cur'
      ' where dg.doc_id = d.id'
      '   and d.fldr_id in (13712611, 13712612, 13712613)'
      '   and d.fldr_id = f.id'
      '   and d.status != '#39'D'#39
      '   and dg.doc_id != :DOC_ID'
      '   and dg.doc_id = dgi.doc_id  '
      '   and dgi.org_id = o.id'
      '   and cur.org_id = dgi.org_id'
      '   and cur.nmcl_id = dg.nmcl_id'
      '   and cur.assortment_id = dg.assortment_id'
      '   and (cur.begin_date between dg.begin_date and dg.end_date or'
      '        cur.end_date between dg.begin_date and dg.end_date)  '
      ' '
      'union all'
      'select count(*) as c,'
      
        '       trim(rpad(max('#39'�� ������ ������������ '#39' || dap.nmcl_id ||' +
        ' '#39' � ����� '#39' || o.name || '#39' ���� ����������� � ���������� "�����' +
        ' �� ���" � ����� '#39' || f.name || '#39' (Id ��������� = '#39' || dap.doc_i' +
        'd || '#39'). ����������?'#39' ),250)) as note '
      '  from doc_action_price dap,'
      '       documents d,'
      '       folders f,'
      '       doc_action_price_items dapi,'
      '       organizations o, cur'
      ' where dap.doc_id = d.id'
      '   and d.status != '#39'D'#39
      '   and d.fldr_id in (2467, 2468)'
      '   and d.fldr_id = f.id '
      '   and dap.doc_id = dapi.doc_id'
      '   and dapi.org_id = o.id'
      '   and cur.org_id = dapi.org_id'
      '   and cur.nmcl_id = dap.nmcl_id'
      '   and cur.assortment_id = dap.assortment_id'
      
        '   and (cur.begin_date between dap.begin_date and dap.end_date o' +
        'r'
      '        cur.end_date between dap.begin_date and dap.end_date)   '
      ')'
      'order by c desc')
    Macros = <>
    Left = 284
    Top = 471
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_chk_docsC: TFloatField
      FieldName = 'C'
    end
    object m_Q_chk_docsNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
  end
  object m_Q_fikt_upd: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select  begin_date,'
      '           end_date,'
      '           act_price,'
      '           price_bef_act,'
      '           urgent_reprice'
      '  from doc_disc_goods'
      ' where doc_id = -1')
    UpdateObject = m_U_fikt_upd
    Macros = <>
    Left = 48
    Top = 626
    object m_Q_fikt_updBEGIN_DATE: TDateTimeField
      FieldName = 'BEGIN_DATE'
    end
    object m_Q_fikt_updEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object m_Q_fikt_updACT_PRICE: TFloatField
      FieldName = 'ACT_PRICE'
    end
    object m_Q_fikt_updPRICE_BEF_ACT: TFloatField
      FieldName = 'PRICE_BEF_ACT'
    end
    object m_Q_fikt_updURGENT_REPRICE: TFloatField
      FieldName = 'URGENT_REPRICE'
    end
  end
  object m_U_fikt_upd: TMLUpdateSQL
    Left = 144
    Top = 626
  end
  object m_Q_chk_sel_docs: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select COUNT(*) c from (      '
      '   select count(*) as c'
      '     from doc_disc_goods dg,'
      '          documents d'
      '    where exists (select 1 '
      '                    from doc_disc_goods d '
      '                   where doc_id in (select to_number(value) '
      
        '                                      from tmp_report_param_valu' +
        'es '
      '                                     where id = :SQ_ID) '
      '                     and d.nmcl_id = dg.nmcl_id '
      '                     and d.assortment_id = dg.assortment_id)'
      '     and d.fldr_id in (:FLDR_NEW, :FLDR_SOGL, :FLDR_UTV)'
      '     and dg.doc_id = d.id'
      '     group by dg.nmcl_id, dg.assortment_id)'
      'where c > 1')
    Macros = <>
    Left = 308
    Top = 560
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_NEW'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_SOGL'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_UTV'
        ParamType = ptInput
      end>
    object m_Q_chk_sel_docsC: TFloatField
      FieldName = 'C'
    end
  end
  object m_Q_chg_fldr: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      'for ii in (select doc_id,'
      
        '                       fnc_get_route(d.fldr_id,fnc_get_constant(' +
        #39'FLDR_DISC_GOODS_IN_ACT'#39') ) as route_id'
      '               from doc_disc_goods dg,'
      '                        documents d'
      '              where doc_id in (select to_number(value) '
      '                                 from tmp_report_param_values '
      '                                where id = :SQ_ID)'
      '                and dg.doc_id = d.id) '
      'loop'
      '    prc_chg_fldr(ii.doc_id, ii.route_id, null);'
      'end loop;'
      'end;')
    Macros = <>
    Left = 404
    Top = 571
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
    object m2: TFloatField
      FieldName = 'C'
    end
  end
  object m_SP_copy_doc: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'RETAIL.PRC_COPY_DOC_DISC_GOODS'
    Left = 136
    Top = 432
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'P_DEST_DOC_ID'
        ParamType = ptOutput
      end>
  end
  object m_Q_reject_zam_kd: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select reject_zam_kd as note from doc_disc_goods where doc_id =-' +
        '1')
    UpdateObject = m_U_reject_zam_kd
    Macros = <>
    Left = 409
    Top = 640
    object m_Q_reject_zam_kdNOTE: TStringField
      FieldName = 'NOTE'
      Origin = 'TSDBMAIN.DOC_DISC_GOODS.REJECT_ZAM_KD'
      Size = 250
    end
  end
  object m_U_reject_zam_kd: TMLUpdateSQL
    ModifySQL.Strings = (
      'begin'
      '    update doc_disc_goods'
      '       set reject_zam_kd = :NOTE'
      '     where doc_id = :DOC_ID;'
      'end;')
    IgnoreRowsAffected = True
    Left = 531
    Top = 640
  end
  object m_Q_edit_reject_zam_ld: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'begin'
      '    update doc_disc_goods'
      '       set reject_zam_kd = :NOTE'
      '     where doc_id = :DOC_ID;'
      'end;')
    Macros = <>
    Left = 657
    Top = 632
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NOTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m4: TStringField
      DisplayLabel = '������� ������'
      FieldName = 'NOTE'
      Origin = 'reason_rejection'
      Size = 250
    end
  end
  object m_DS_list: TDataSource
    DataSet = m_Q_list
    Left = 624
    Top = 480
  end
  object m_Q_get_out_price: TMLQuery
    BeforeOpen = m_Q_get_out_priceBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select nvl(PKG_RTL_PRICES.fnc_cur_out_price_nd(:NMCL_ID,:ASSORT_' +
        'ID ,:ORG_ID),pkg_rtl_prices.fnc_cur_out_price(:NMCL_ID,:ASSORT_I' +
        'D ,:ORG_ID)) as out_price_nd, '
      
        'pkg_rtl_prices.fnc_cur_out_price(:NMCL_ID,:ASSORT_ID ,:ORG_ID) A' +
        'S out_price from dual')
    Macros = <>
    Left = 212
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_get_out_priceOUT_PRICE_ND: TFloatField
      DisplayLabel = '���� ��� �����'
      FieldName = 'OUT_PRICE_ND'
    end
    object m_Q_get_out_priceOUT_PRICE: TFloatField
      DisplayLabel = '���� �� ����� �� �����'
      FieldName = 'OUT_PRICE'
    end
  end
  object m_Q_del_docs: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select to_number(value) as doc_id'
      'FROM tmp_report_param_values where id = :sq_id')
    Macros = <>
    Left = 60
    Top = 696
    ParamData = <
      item
        DataType = ftInteger
        Name = 'sq_id'
        ParamType = ptInput
      end>
    object m_Q_del_docsDOC_ID: TFloatField
      FieldName = 'DOC_ID'
    end
  end
  object m_Q_folders_next_all: TMLQuery
    BeforeOpen = m_Q_folders_next_allBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT v.src_fldr_id,'
      '       v.id, '
      '       v.dest_fldr_id, '
      '       v.name,'
      '       v.route_next_right,'
      '       case when fldr_qnt = 1 then 1 else 0 end as enabled'
      'FROM ('
      '      SELECT fr.src_fldr_id, '
      '             fr.id, '
      '             fr.dest_fldr_id, '
      '             fr.name,'
      '             fr.sort_id,'
      
        '             rpad(fnc_mask_route_right(fr.id,'#39'move'#39',fr.src_fldr_' +
        'id),250) AS route_next_right,'
      
        '             (select count(distinct value) from tmp_report_param' +
        '_values where id = :sq) as fldr_qnt'
      '      FROM fldr_routes fr,'
      '           folders f_s,'
      '           folders f_e'
      '      WHERE'
      '            f_s.id = fr.src_fldr_id AND'
      '            f_e.id = fr.dest_fldr_id AND'
      '            f_e.sort_id > f_s.sort_id AND'
      '           fr.src_fldr_id = :fldr_id'
      '     ) v'
      'WHERE v.route_next_right = 1 '
      'ORDER BY v.sort_id'
      ' '
      ' ')
    Macros = <>
    Left = 700
    Top = 370
    ParamData = <
      item
        DataType = ftInteger
        Name = 'sq'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'fldr_id'
        ParamType = ptInput
      end>
    object m_Q_folders_next_allSRC_FLDR_ID: TFloatField
      FieldName = 'SRC_FLDR_ID'
    end
    object m_Q_folders_next_allID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_folders_next_allDEST_FLDR_ID: TFloatField
      FieldName = 'DEST_FLDR_ID'
    end
    object m_Q_folders_next_allNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object m_Q_folders_next_allROUTE_NEXT_RIGHT: TStringField
      FieldName = 'ROUTE_NEXT_RIGHT'
      Size = 250
    end
    object m_Q_folders_next_allENABLED: TFloatField
      FieldName = 'ENABLED'
    end
  end
  object m_Q_folders_prev_all: TMLQuery
    BeforeOpen = m_Q_folders_prev_allBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT v.src_fldr_id,'
      '       v.id, '
      '       v.dest_fldr_id, '
      '       v.name,'
      '       v.route_next_right,'
      '       case when fldr_qnt = 1 then 1 else 0 end as enabled'
      'FROM ('
      '      SELECT fr.src_fldr_id, '
      '             fr.id, '
      '             fr.dest_fldr_id, '
      '             fr.name,'
      '             fr.sort_id,'
      
        '             rpad(fnc_mask_route_right(fr.id,'#39'move'#39',fr.src_fldr_' +
        'id),250) AS route_next_right,'
      
        '             (select count(distinct value) from tmp_report_param' +
        '_values where id = :sq) as fldr_qnt'
      '      FROM fldr_routes fr,'
      '           folders f_s,'
      '           folders f_e'
      '      WHERE'
      '            f_s.id = fr.src_fldr_id AND'
      '            f_e.id = fr.dest_fldr_id AND'
      '            f_e.sort_id < f_s.sort_id AND'
      '           fr.src_fldr_id = :fldr_id'
      ''
      '     ) v'
      'WHERE v.route_next_right = 1 '
      'ORDER BY v.sort_id'
      ' ')
    Macros = <>
    Left = 703
    Top = 426
    ParamData = <
      item
        DataType = ftInteger
        Name = 'sq'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'fldr_id'
        ParamType = ptInput
      end>
    object m_Q_folders_prev_allSRC_FLDR_ID: TFloatField
      FieldName = 'SRC_FLDR_ID'
    end
    object m_Q_folders_prev_allID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_folders_prev_allDEST_FLDR_ID: TFloatField
      FieldName = 'DEST_FLDR_ID'
    end
    object m_Q_folders_prev_allNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object m_Q_folders_prev_allROUTE_NEXT_RIGHT: TStringField
      FieldName = 'ROUTE_NEXT_RIGHT'
      Size = 250
    end
    object m_Q_folders_prev_allENABLED: TFloatField
      FieldName = 'ENABLED'
    end
  end
  object m_Q_docs: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select vv.doc_id, vv.fldr_id,'
      'vv.nmcl_id, vv.assortment_id,'
      'vv.reason_id,'
      'vv.act_price, vv.price_bef_act, vv.price_bef_act_on_card,'
      
        'vv.begin_date, vv.end_date, vv.action_type_id, count(vv.org_id) ' +
        'as count_org, decode((select count(org_id) from doc_disc_goods_i' +
        'tems where doc_id=vv.doc_id and org_id=3),1,1,0) as org_id_3,'
      
        ' (select max(1) from groups_organizations og, organizations o, d' +
        'oc_disc_goods_items d where doc_id=vv.doc_id and o.id =d.org_id ' +
        'and o.id = og.org_id and og.grp_id = 75) as sm_mini,'
      
        ' (select max(1) from groups_organizations og, organizations o, d' +
        'oc_disc_goods_items d where doc_id=vv.doc_id and o.id =d.org_id ' +
        'and o.id = og.org_id and og.grp_id = 74) as gm,'
      
        'min(margin_af_act) as min_margin_af_act, max(total_profit) as ma' +
        'x_total_profit from('
      
        'select v.doc_id, v.fldr_id, v.nmcl_id, v.assortment_id, v.reason' +
        '_id, v.act_price, v.price_bef_act, v.price_bef_act_on_card,'
      'v.begin_date, v.end_date, v.action_type_id, v.org_id,'
      
        'round(((v.act_price - v.cur_price_in) / nullif(v.cur_price_in,0)' +
        ')*100,2) as margin_af_act,'
      
        '  case when ((v.act_price - v.cur_price_in) / nullif(v.cur_price' +
        '_in,0)) < 0 '
      
        '                 then round(((v.act_price - v.cur_price_in)* nvl' +
        '(store_items,0)),2)  '
      '            else null'
      '       end total_profit from('
      'select a.doc_id, d.fldr_id,'
      'a.nmcl_id, a.assortment_id,'
      'a.reason_id,'
      'a.act_price, a.price_bef_act, a.price_bef_act_on_card,'
      'a.begin_date, a.end_date, a.action_type_id, aa.org_id, '
      
        'pkg_rtl_prices.fnc_cur_in_price(a.nmcl_id, a.assortment_id, aa.o' +
        'rg_id) as cur_price_in,'
      
        '(select store_items from buf_nmcl_data where rep_date=trunc(sysd' +
        'ate) and nmcl_id=a.nmcl_id and assortment_id=a.assortment_id and' +
        ' org_id=aa.org_id) as store_items'
      'from doc_disc_goods a, documents d, fldr_routes r, '
      'doc_disc_goods_items aa'
      'where a.doc_id in (select to_number(value) '
      
        '                          FROM tmp_report_param_values where id ' +
        '= :sq_id)'
      'and a.doc_id = d.id'
      'and a.doc_id=aa.doc_id(+)'
      'and d.fldr_id = r.src_fldr_id'
      'and r.id= :route_id'
      
        'group by a.doc_id, d.fldr_id,a.nmcl_id,  a.assortment_id, a.reas' +
        'on_id, a.begin_date, a.end_date, a.action_type_id, a.act_price, ' +
        'a.price_bef_act, a.price_bef_act_on_card, aa.org_id)v)vv'
      
        'group by vv.doc_id, vv.fldr_id, vv.nmcl_id, vv.assortment_id, vv' +
        '.reason_id, vv.act_price, vv.price_bef_act, vv.price_bef_act_on_' +
        'card, vv.begin_date, vv.end_date,vv.action_type_id')
    Macros = <>
    Left = 704
    Top = 486
    ParamData = <
      item
        DataType = ftInteger
        Name = 'sq_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'route_id'
        ParamType = ptInput
      end>
    object m_Q_docsDOC_ID: TFloatField
      FieldName = 'DOC_ID'
    end
    object m_Q_docsFLDR_ID: TFloatField
      FieldName = 'FLDR_ID'
    end
    object m_Q_docsNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
    end
    object m_Q_docsASSORTMENT_ID: TFloatField
      FieldName = 'ASSORTMENT_ID'
    end
    object m_Q_docsREASON_ID: TStringField
      FieldName = 'REASON_ID'
      Size = 50
    end
    object m_Q_docsACT_PRICE: TFloatField
      DisplayLabel = '���� �� �����'
      FieldName = 'ACT_PRICE'
    end
    object m_Q_docsPRICE_BEF_ACT: TFloatField
      DisplayLabel = '���� �� �����'
      FieldName = 'PRICE_BEF_ACT'
    end
    object m_Q_docsBEGIN_DATE: TDateTimeField
      FieldName = 'BEGIN_DATE'
    end
    object m_Q_docsEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object m_Q_docsACTION_TYPE_ID: TFloatField
      FieldName = 'ACTION_TYPE_ID'
    end
    object m_Q_docsCOUNT_ORG: TFloatField
      DisplayLabel = '���������� ���'
      FieldName = 'COUNT_ORG'
    end
    object m_Q_docsORG_ID_3: TFloatField
      DisplayLabel = '������� ��������������'
      FieldName = 'ORG_ID_3'
    end
    object m_Q_docsMIN_MARGIN_AF_ACT: TFloatField
      DisplayLabel = '��� % ������� ����� ������� ������'
      FieldName = 'MIN_MARGIN_AF_ACT'
    end
    object m_Q_docsMAX_TOTAL_PROFIT: TFloatField
      DisplayLabel = '���� �������� ������� �������'
      FieldName = 'MAX_TOTAL_PROFIT'
    end
    object m_Q_docsPRICE_BEF_ACT_ON_CARD: TFloatField
      DisplayLabel = '���� �� ����� �� �����'
      FieldName = 'PRICE_BEF_ACT_ON_CARD'
    end
    object m_Q_docsGM: TFloatField
      DisplayLabel = '���-�� ��� ������ ��'
      FieldName = 'GM'
    end
    object m_Q_docsSM_MINI: TFloatField
      DisplayLabel = '���-�� ��� ������ �� � ����'
      FieldName = 'SM_MINI'
    end
  end
  object m_Q_chk_nmcl: TMLQuery
    BeforeOpen = m_Q_chk_nmclBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select count(*) as c'
      '  from doc_disc_goods dg,'
      '       documents d'
      ' where nmcl_id = :NMCL_ID'
      '   and assortment_id = :ASSORTMENT_ID'
      '   and dg.doc_id = d.id'
      '   and d.fldr_id in (:FLDR_NEW, :FLDR_SOGL, :FLDR_UTV)'
      '   and dg.doc_id != :DOC_ID')
    Macros = <>
    Left = 684
    Top = 559
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORTMENT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_NEW'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_SOGL'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_UTV'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_chk_nmclC: TFloatField
      FieldName = 'C'
    end
  end
  object m_Q_cnt: TMLQuery
    BeforeOpen = m_Q_cntBeforeOpen
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_list
    SQL.Strings = (
      'select cnt_id, cnt_name from ('
      'select distinct cnt_id,'
      
        'TRIM(RPAD(pkg_contractors.fnc_contr_full_name(cnt_id) ,250))  cn' +
        't_name from cnt_nmcl_coupon'
      'where nmcl_id=:NMCL_ID'
      'and end_date is null)'
      '%WHERE_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 200
    Top = 368
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_cntCNT_ID: TFloatField
      DisplayLabel = 'ID ����������'
      DisplayWidth = 20
      FieldName = 'CNT_ID'
      Origin = 'cnt_id'
    end
    object m_Q_cntCNT_NAME: TStringField
      DisplayLabel = '���������'
      FieldName = 'CNT_NAME'
      Origin = 'cnt_name'
      Size = 250
    end
  end
end
