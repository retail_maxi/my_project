//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmCompetitorsRepriceSummList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTLIST"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceSummList::m_ACT_set_begin_end_dateSet(
      TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                        m_ACT_set_begin_end_date->EndDate);   
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummList::FormShow(TObject *Sender)
{
  TTSFDocumentList::FormShow(Sender);

  m_ACT_set_begin_end_date->BeginDate =  m_dm->m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;
  m_ACT_set_begin_end_date->EndDate =   m_dm->m_Q_list->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

