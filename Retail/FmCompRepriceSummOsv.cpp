//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmCompRepriceSummOsv.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFCompRepriceSummOsv::TFCompRepriceSummOsv(TComponent* p_owner,
                                                      TTSDCustom *p_dm_custom,
                                                      const AnsiString &p_caption
                                                      ): TTSFDialog(p_owner,
                                                                                                p_dm_custom),
                                                                                       m_dm((TDCompetitorsRepriceSumm*)p_dm_custom)
{
  this->Caption = p_caption;
}
//---------------------------------------------------------------------------

__fastcall TFCompRepriceSummOsv::~TFCompRepriceSummOsv()
{

}
//---------------------------------------------------------------------------

void __fastcall TFCompRepriceSummOsv::m_ACT_apply_updatesUpdate(
      TObject *Sender)
{
   m_ACT_apply_updates->Enabled = false;
}
//---------------------------------------------------------------------------











