//---------------------------------------------------------------------------

#ifndef DmZValidationH
#define DmZValidationH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMOPERATION.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDZValidation : public TTSDOperation
{
__published:	// IDE-managed Components
   TMLQuery *m_Q_tax;
   TFloatField *m_Q_taxCHECKED;
   TFloatField *m_Q_taxID;
   TFloatField *m_Q_taxCNT_ID;
   TStringField *m_Q_taxTAXPAYER_NAME;
   TStringField *m_Q_taxMANAGER;
   TStringField *m_Q_taxTAXATION_NAME;
   TMLUpdateSQL *m_U_tax;
   TStringField *m_Q_mainTP_NAME;
   TFloatField *m_Q_mainINVOICE_AMOUNT;
   TFloatField *m_Q_mainBUH_Z_AMOUNT;
   TFloatField *m_Q_mainSRPM_AMOUNT;
   TFloatField *m_Q_mainRTL_Z_AMOUNT;
   TMLQuery *m_Q_ins_param;
   TMLQuery *m_Q_del_param;
   TMLQuery *m_Q_ins_all_tax;
        TFloatField *m_Q_mainRTL_SRPM_AMOUNT;
        TFloatField *m_Q_mainORG_ID;
        TStringField *m_Q_mainORG_NAME;
   void __fastcall m_Q_taxCHECKEDChange(TField *Sender);
   void __fastcall m_Q_taxBeforeOpen(TDataSet *DataSet);
   void __fastcall m_Q_mainBeforeOpen(TDataSet *DataSet);
private:	// User declarations
    int m_sq_tax; //��������� �����������������
public:		// User declarations
   TDateTime m_begin_date;
   TDateTime m_end_date;
   bool m_act_tax;
    void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                    const TDateTime &p_end_date);
  __property int SqTax  = { read=m_sq_tax, write = m_sq_tax};

   __fastcall TDZValidation(TComponent* p_owner,
                                        AnsiString p_prog_id);
   __fastcall ~TDZValidation();

};
//---------------------------------------------------------------------------
#endif
 