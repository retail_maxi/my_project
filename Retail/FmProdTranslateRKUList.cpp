//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmProdTranslateRKUList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFProdTranslateRKUList::m_ACT_show_histExecute(TObject *Sender)
{
  TDProdTranslateRKU *m_dm = static_cast<TDProdTranslateRKU*>(DMRefbook);

  m_dm->showHist = m_DCB_hist->State == cbChecked;
  m_GB_hist->Visible = m_dm->showHist;

  if(m_dm->showHist)
  {
    if(m_dm->m_Q_hist->Active) m_dm->m_Q_hist->Close();
    m_dm->m_Q_hist->Open();
  }
}
//---------------------------------------------------------------------------

