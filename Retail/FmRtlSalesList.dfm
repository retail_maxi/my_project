inherited FRtlSalesList: TFRtlSalesList
  Left = 751
  Top = 312
  Width = 930
  Height = 546
  Caption = 'FRtlSalesList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 388
    Width = 914
    Height = 5
    Cursor = crVSplit
    Align = alBottom
  end
  inherited m_P_main_control: TPanel
    Top = 458
    Width = 914
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 817
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 429
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 489
    Width = 914
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 914
    Height = 362
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 53
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 71
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 84
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 106
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 138
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 104
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 151
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 183
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 37
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 133
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 39
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'COMPLETE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SALE_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'BUYER_SUM'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'COMPLETE_USER_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'COMPLETE_USER_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'HOST'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CASH'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 914
    inherited m_P_tb_main: TPanel
      Width = 867
      inherited m_TB_main: TToolBar
        Width = 867
        object m_TBTN_sep6: TToolButton
          Left = 302
          Top = 2
          Width = 8
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_DBLCB_organizations: TDBLookupComboBox
          Left = 310
          Top = 2
          Width = 185
          Height = 21
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = m_DS_orgs
          TabOrder = 1
          OnClick = m_DBLCB_organizationsClick
        end
        object ToolButton2: TToolButton
          Left = 495
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 19
          Style = tbsSeparator
        end
        object m_SBTN_period: TSpeedButton
          Left = 503
          Top = 2
          Width = 250
          Height = 22
          Action = m_ACT_period
        end
        object ToolButton1: TToolButton
          Left = 753
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 18
          Style = tbsSeparator
        end
        object m_CB_tabac: TCheckBox
          Left = 761
          Top = 2
          Width = 233
          Height = 22
          Caption = '���������� ��� �� �������� ��������'
          TabOrder = 0
          OnClick = m_CB_tabacClick
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 867
    end
  end
  object MLDBGrid1: TMLDBGrid [5]
    Left = 0
    Top = 393
    Width = 914
    Height = 65
    Align = alBottom
    DataSource = m_DS_checks
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = m_DBG_listDblClick
    OnKeyPress = m_DBG_listKeyPress
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    OnGetCellParams = m_DBG_listGetCellParams
    KeyColumnFind = -1
    ValueColumnFind = -1
    Columns = <
      item
        FieldName = 'CHECK_NUM'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'CHECK_DATE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'CHECK_SUM'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'PRINT_DATE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'PRINT_USER_ID'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'PRINT_USER_NAME'
        Title.TitleButton = True
        Width = 200
        Footers = <>
      end
      item
        FieldName = 'HOST'
        Title.TitleButton = True
        Width = 200
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'Sales'
      Caption = 'm_ACT_set_begin_end_date'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 40074
      EndDate = 40074
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
    object m_ACT_period: TAction
      Category = 'List'
      OnExecute = m_ACT_periodExecute
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DRtlSales.m_Q_list
  end
  object m_DS_checks: TDataSource
    DataSet = DRtlSales.m_Q_checks
    Top = 64
  end
  object m_DS_orgs: TDataSource
    DataSet = DRtlSales.m_Q_orgs
    Left = 256
    Top = 8
  end
end
