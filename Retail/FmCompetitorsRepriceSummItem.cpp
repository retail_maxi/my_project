//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmCompetitorsRepriceSummItem.h"
#include "FmCompRepriceSummOsv.h"
#include "TSErrors.h" 
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLDBPanel"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------


void __fastcall TFCompetitorsRepriceSummItem::m_ACT_editExecute(
      TObject *Sender)
{
  TTSFDocumentWithLines::m_ACT_editExecute(Sender);
/*if (m_dm->m_Q_linesLINE_ID->IsNull)
  {
    TTSFDocumentWithLines::m_ACT_appendExecute(Sender);
  }
  else  {
    long line_id = m_dm->m_Q_linesLINE_ID->AsInteger;
    TTSFDocumentWithLines::m_ACT_editExecute(Sender);
         if( line_id > 0 )
        m_dm->m_Q_lines->Locate(m_dm->m_Q_linesLINE_ID->FieldName,line_id,TLocateOptions());

    }  */

}

  __fastcall TFCompetitorsRepriceSummItem::~TFCompetitorsRepriceSummItem(){

  };
//---------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceSummItem::RefreshItem()
{
    //�������� ������ �����������
    if (  m_dm->m_Q_competitors->Active) m_dm->m_Q_competitors->Close();
    m_dm->m_Q_competitors->Open();

     //������� m_Q_lines
    m_dm->m_Q_lines->Close();
    m_dm->m_Q_lines->SQL->Clear();

    ResetItem();

    m_dm->m_Q_lines->SQL->AddStrings(m_dm->m_Q_lines_start->SQL);

    m_dm->m_Q_competitors->First();
    int i=1; AnsiString colname = "col";
    while (!m_dm->m_Q_competitors->Eof)
    {
       //�������� ������� � m_Q_lines
       TFloatField *f1 = new TFloatField(this);
       f1->FieldName = colname+"_min_"+i;
       f1->Name= AnsiString("m_Q_lines")+colname+"_min_"+i;
       f1->DisplayLabel = m_dm->m_Q_competitorsNAME->AsString+"|min";
       f1->DataSet = m_dm->m_Q_lines;
       f1->Tag =   m_dm->m_Q_competitorsID->AsInteger; //id ���������� ��� ����������� ����

       TFloatField *f2 = new TFloatField(this);
       f2->FieldName = colname+"_max_"+i;
       f2->Name= AnsiString("m_Q_lines")+colname+"_max_"+i;
       f2->DisplayLabel = m_dm->m_Q_competitorsNAME->AsString+"|max";
       f2->DataSet = m_dm->m_Q_lines;
       f2->Tag =   m_dm->m_Q_competitorsID->AsInteger; //id ���������� ��� ����������� ����

       //�������� SQL-�����
       m_dm->m_Q_lines->SQL->Add(" ");
       m_dm->m_Q_lines->SQL->Add(" ,(select min(min_comp_price)  ");
       m_dm->m_Q_lines->SQL->Add("    from cc ");
       m_dm->m_Q_lines->SQL->Add("    where cc.nmcl_id = ni.id ");
       m_dm->m_Q_lines->SQL->Add("      and cc.comp_id = "+m_dm->m_Q_competitorsID->AsString+" ");
       m_dm->m_Q_lines->SQL->Add("  ) AS "+colname+"_min_"+i+" ");

       m_dm->m_Q_lines->SQL->Add(" ");
       m_dm->m_Q_lines->SQL->Add(" ,(select max(max_comp_price)  ");
       m_dm->m_Q_lines->SQL->Add("    from cc ");
       m_dm->m_Q_lines->SQL->Add("    where cc.nmcl_id = ni.id ");
       m_dm->m_Q_lines->SQL->Add("      and cc.comp_id = "+m_dm->m_Q_competitorsID->AsString+" ");
       m_dm->m_Q_lines->SQL->Add("  ) AS "+colname+"_max_"+i+" ");
       i++;
       m_dm->m_Q_competitors->Next();
    }
      
    //�������� � sql  m_Q_lines
    m_dm->m_Q_lines->SQL->AddStrings(m_dm->m_Q_lines_end->SQL );

    //�������

    TParam *res = m_dm->m_Q_lines->Macros->FindParam("WHERE_EXPRESSION");
    if( res )      res->Value = "";
    res = NULL;
    res = m_dm->m_Q_lines->Macros->FindParam("ORDER_EXPRESSION");
    if( res)        res->Value = "";

    m_DS_lines->DataSet = NULL;
    m_DS_lines->DataSet = m_dm->m_Q_lines;

    m_DBG_lines->Columns->RebuildColumns();
    //������������ ������ � ��������� �����
    for( int i = 0; i < m_DBG_lines->Columns->Count; i++ )
       m_DBG_lines->Columns->Items[i]->Title->TitleButton = true;
    m_dm->m_Q_lines->DoFilter(m_dm->m_Q_lines->FilterSettings);
    m_dm->m_Q_lines->DoOrder(m_dm->m_Q_lines->OrderSettings);

    for (int i=0; i<m_DBG_lines->Columns->Count; i++)
    {
       m_DBG_lines->Columns->Items[i]->Visible = m_dm->m_Q_lines->Fields->Fields[i]->Visible;
    }

    m_dm->m_Q_lines->Open();
    m_DBG_lines->ReadReg();
}


//--------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceSummItem::ResetItem()
{

  for (int i=m_dm->m_Q_lines->FieldCount; i>m_dm->m_Q_lines_start->FieldCount; i--)
  {
      TField *f =  m_dm->m_Q_lines->Fields->Fields[i-1];
      delete f;
  }
  
/*  for (int i=0; i<m_dm->m_Q_lines->FieldCount; i++)
  {
      if(m_dm->m_Q_lines_start->Fields->FieldByName(m_dm->m_Q_lines->Fields->Fields[i-1]->FeildName)){
        TField *f =  m_dm->m_Q_lines->Fields->Fields[i-1];
        delete f;
      }
  }  */
}
//--------------------------------------------------------------------------
void __fastcall TFCompetitorsRepriceSummItem::FormShow(TObject *Sender)
{
     RefreshItem();
     TTSFDocumentWithLines::FormShow(Sender);
}
//---------------------------------------------------------------------------



void __fastcall TFCompetitorsRepriceSummItem::m_DBG_linesColEnter(
      TObject *Sender)
{
    m_dm->CurCompId = m_DBG_lines->SelectedField->Tag;
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_editUpdate(TObject *Sender)
{
TTSFDocumentWithLines::m_ACT_editUpdate(Sender);
/*  m_ACT_edit->Enabled =  (m_DBG_lines->SelectedField->Tag > 0) &&
                           (m_dm->UpdateRegimeItem != urView)*/

                           ;


}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_deleteUpdate(
      TObject *Sender)
{
  TTSFDocumentWithLines::m_ACT_deleteUpdate(Sender);
  /*m_ACT_delete->Enabled =  (m_DBG_lines->SelectedField->Tag > 0) &&
                           (m_dm->UpdateRegimeItem != urView) && !m_dm->m_Q_linesLINE_ID->IsNull;*/
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_viewUpdate(TObject *Sender)
{
  TTSFDocumentWithLines::m_ACT_viewUpdate(Sender);
 /*    m_ACT_view->Enabled =  (m_DBG_lines->SelectedField->Tag > 0 &&
                             !m_DBG_lines->SelectedField->Value.IsNull() );*/
}
//---------------------------------------------------------------------------



void __fastcall TFCompetitorsRepriceSummItem::m_DBG_linesDblClick(
      TObject *Sender)
{
 //if (m_ACT_edit->Enabled ) m_ACT_editExecute(Sender);
 //else
  if (m_ACT_view->Enabled ) m_ACT_viewExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   m_DBG_lines->WriteReg();
   TTSFDocumentWithLines::FormClose(Sender, Action);
   ResetItem();
}
//---------------------------------------------------------------------------


void __fastcall TFCompetitorsRepriceSummItem::m_ACT_add_linesExecute(
      TObject *Sender)
{
  if( m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();
  m_dm->m_SP_fill_lines->ParamByName("P_DOC_ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
  m_dm->m_SP_fill_lines->ExecProc();
  if( m_dm->m_SP_fill_lines->ParamByName("P_DOC_ID")->AsInteger >0)
    RefreshItem();
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_add_linesUpdate(
      TObject *Sender)
{
   m_ACT_add_lines->Enabled =  m_dm->m_Q_lines->RecordCount==0 && m_dm->UpdateRegimeItem != urView ;
   m_LV_org_grps->Enabled =  m_dm->m_Q_lines->RecordCount==0 && m_dm->UpdateRegimeItem != urView ;
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_LOV_org_grpsAfterApply(
      TObject *Sender)
{
  if (m_dm->m_Q_org_grpsQNT->AsInteger ==0){
   m_dm->m_Q_itemORG_GROUP_ID->Clear();
   m_dm->m_Q_itemORG_GROUP_NAME->Clear();  
   throw ETSError("� ��������� ����� ��� �����������!");
  }
  else{
     RefreshItem();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_DBG_linesGetCellParams(
      TObject *Sender, TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
     if (m_dm->m_Q_linesPERCENT_MIN->AsInteger > 10 &&
         Column->FieldName == "PERCENT_MIN")
   {
      Background = TColor(0x7280FA);
      AFont->Color = clWindowText;
   }

   if (m_dm->m_Q_linesPERCENT_MAX->AsInteger > 10 &&
         Column->FieldName == "PERCENT_MAX")
   {
      Background = TColor(0x7280FA);
      AFont->Color = clWindowText;
   }

}
//---------------------------------------------------------------------------


void __fastcall TFCompetitorsRepriceSummItem::m_ACT_osnovUpdate(
      TObject *Sender)
{
m_ACT_osnov -> Enabled = (m_DBG_lines->SelectedField->Tag > 0)&&
                         !m_dm->m_Q_linesNMCL_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_osnovExecute(
      TObject *Sender)
{
if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
     if(m_dm->m_Q_osnov->Active)
      m_dm->m_Q_osnov->Close();
     m_dm->m_Q_osnov->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
     m_dm->m_Q_osnov->ParamByName("GROUP_ID")->AsInteger = m_DBG_lines->SelectedField->Tag;
     m_dm->m_Q_osnov->ParamByName("NMCL_ID")->AsInteger = m_dm->m_Q_linesNMCL_ID->AsInteger;

     m_dm->m_Q_osnov->Open();

     AnsiString comp_name = m_DBG_lines->SelectedField->DisplayLabel;
     comp_name = comp_name.SubString(1,comp_name.Pos('|')-1);
     AnsiString caption =      comp_name+": "+m_dm->m_Q_linesNMCL_NAME->AsString;
     TFCompRepriceSummOsv *f = new TFCompRepriceSummOsv(this,m_dm,caption);
     try
     {
       f->ShowModal();
       if(m_dm->m_Q_osnov->Active)
         m_dm->m_Q_osnov->Close();
     }
     __finally
     {
       delete f;
     }
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_veiw_info_nmclUpdate(
      TObject *Sender)
{
  m_ACT_veiw_info_nmcl->Enabled = !m_dm->m_Q_linesLINE_ID->IsNull;        
}
//---------------------------------------------------------------------------

void __fastcall TFCompetitorsRepriceSummItem::m_ACT_veiw_info_nmclExecute(
      TObject *Sender)
{
  int l_nmcl_id;
  int l_assortment_id;

  l_nmcl_id = m_dm->m_Q_linesNMCL_ID->AsInteger;
  l_assortment_id = m_dm->m_Q_linesASSORT_ID->AsInteger;

  TMLParams params;
  params.InitParam("NMCL_ID", l_nmcl_id);
  params.InitParam("ASSORT_ID", l_assortment_id);

  m_view_nmcl_data_ctrl.Execute(true,params);

}
//---------------------------------------------------------------------------

