inherited FTTNOutItem: TFTTNOutItem
  Left = 726
  Top = 342
  Width = 870
  Height = 640
  Caption = 'FTTNOutItem'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 552
    Width = 854
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 757
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 466
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 272
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 583
    Width = 854
  end
  inherited m_P_main_top: TPanel
    Width = 854
    inherited m_P_tb_main: TPanel
      Width = 807
      inherited m_TB_main: TToolBar
        Width = 807
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 807
    end
  end
  inherited m_P_header: TPanel
    Width = 854
    Height = 111
    object m_L_doc_number: TLabel
      Left = 96
      Top = 12
      Width = 36
      Height = 13
      Caption = '��� �'
    end
    object m_L_doc_date: TLabel
      Left = 240
      Top = 12
      Width = 11
      Height = 13
      Caption = '��'
    end
    object m_L_dest_org: TLabel
      Left = 368
      Top = 12
      Width = 49
      Height = 13
      Caption = '��� �����'
    end
    object m_L_amount_in: TLabel
      Left = 8
      Top = 78
      Width = 118
      Height = 13
      Caption = '����� � ������� �����'
    end
    object m_L_amount_out: TLabel
      Left = 248
      Top = 78
      Width = 138
      Height = 13
      Caption = '����� � ����� ����������'
    end
    object m_L_note: TLabel
      Left = 56
      Top = 46
      Width = 63
      Height = 13
      Caption = '����������'
    end
    object m_DBP_doc_number: TMLDBPanel
      Left = 136
      Top = 8
      Width = 97
      Height = 21
      DataField = 'DOC_NUMBER'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
    end
    object m_DBP_doc_date: TMLDBPanel
      Left = 256
      Top = 8
      Width = 97
      Height = 21
      DataField = 'DOC_DATE'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object m_LLV_dest_org: TMLLovListView
      Left = 424
      Top = 8
      Width = 313
      Height = 21
      Lov = m_LOV_dest_org
      UpDownWidth = 313
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 2
      ParentColor = False
    end
    object m_DBP_amount_in: TMLDBPanel
      Left = 136
      Top = 74
      Width = 97
      Height = 21
      DataField = 'AMOUNT_IN'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
    end
    object m_DBP_amount_out: TMLDBPanel
      Left = 400
      Top = 74
      Width = 97
      Height = 21
      DataField = 'AMOUNT_OUT'
      DataSource = m_DS_item
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 5
    end
    object m_DBE_note: TDBEdit
      Left = 136
      Top = 42
      Width = 601
      Height = 21
      DataField = 'NOTE'
      DataSource = m_DS_item
      TabOrder = 3
    end
  end
  inherited m_DBG_lines: TMLDBGrid
    Top = 137
    Width = 854
    Height = 382
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    UseMultiTitle = True
    Columns = <
      item
        FieldName = 'LINE_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CARD_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MEAS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ASSORTMENT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ASSORTMENT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ITEMS_QNT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'IN_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'OUT_PRICE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'STORE_QNT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'RTL_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_lines_control: TPanel
    Top = 519
    Width = 854
  end
  inherited m_DS_lines: TDataSource
    DataSet = DTTNOut.m_Q_lines
    Left = 24
    Top = 184
  end
  inherited m_DS_item: TDataSource
    DataSet = DTTNOut.m_Q_item
    Left = 376
  end
  object m_LOV_dest_org: TMLLov
    DataFieldKey = 'DEST_ORG_ID'
    DataFieldValue = 'DEST_ORG_NAME'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_orgs
    AutoOpenList = True
    Left = 504
    Top = 26
  end
  object m_DS_orgs: TDataSource
    DataSet = DTTNOut.m_Q_orgs
    Left = 472
    Top = 26
  end
  object m_BCS_item: TDLBCScaner
    Tick = 50
    BeginKey = 2
    EndKey = 3
    OnBegin = m_BCS_itemBegin
    OnEnd = m_BCS_itemEnd
    Left = 439
    Top = 3
  end
end
