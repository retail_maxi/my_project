inherited DZValidation: TDZValidation
  Left = 747
  Top = 126
  Height = 640
  Width = 870
  inherited m_DB_main: TDatabase
    AliasName = 'TEST'
  end
  inherited m_Q_fill_param_val: TQuery
    UpdateObject = nil
  end
  inherited m_Q_main: TMLQuery
    BeforeOpen = m_Q_mainBeforeOpen
    SQL.Strings = (
      'with tax as'
      '(select to_number(pv.value) as id'
      '   from param_values pv'
      '  where pv.id = :SQ_TAX_ID'
      '    and to_number(pv.value) in (select r.taxpayer_id '
      '                                  from rtl_srpm r, '
      '                                       documents d, '
      '                                       folders f'
      '                                 where r.doc_id = d.id'
      '                                   and d.doc_type_id = 1846085'
      '                                   and f.id = d.fldr_id'
      
        '                                   and f.id in (fnc_get_constant' +
        '('#39'FLDR_SRPM_VAL'#39'), fnc_get_constant('#39'FLDR_SRPM_ARC'#39'), fnc_get_co' +
        'nstant('#39'FLDR_SRPM_RECALC'#39')) '
      '                                   and d.status <> '#39'D'#39
      
        '                                   and r.year between EXTRACT(YE' +
        'AR FROM :BEGIN_DATE) and EXTRACT(YEAR FROM :END_DATE)'
      
        '                                   and r.month between EXTRACT(M' +
        'ONTH FROM :BEGIN_DATE) and EXTRACT(MONTH FROM :END_DATE)'
      '                                   and r.create_sf is null)'
      '),'
      'sftax as'
      '(select to_number(pv.value) as id'
      '   from param_values pv'
      '  where pv.id = :SQ_TAX_ID'
      '    and to_number(pv.value) in (select r.taxpayer_id '
      '                                  from rtl_srpm r, '
      '                                       documents d, '
      '                                       folders f'
      '                                 where r.doc_id = d.id'
      '                                   and d.doc_type_id = 1846085'
      '                                   and f.id = d.fldr_id'
      
        '                                   and f.id in (fnc_get_constant' +
        '('#39'FLDR_SRPM_VAL'#39'), fnc_get_constant('#39'FLDR_SRPM_ARC'#39'), fnc_get_co' +
        'nstant('#39'FLDR_SRPM_RECALC'#39')) '
      '                                   and d.status <> '#39'D'#39
      
        '                                   and r.year between EXTRACT(YE' +
        'AR FROM :BEGIN_DATE) and EXTRACT(YEAR FROM :END_DATE)'
      
        '                                   and r.month between EXTRACT(M' +
        'ONTH FROM :BEGIN_DATE) and EXTRACT(MONTH FROM :END_DATE)'
      '                                   and r.create_sf = 1)'
      ')'
      'select * from'
      '(select'
      ' (select'
      '    case'
      
        '     when count(*)  > 1 then trim(rpad('#39'�� ���������� ����������' +
        '�������� c id: '#39'||csl(tp.id),250))'
      '     else max(tp.name)'
      '    end'
      '   from taxpayer tp, tax t'
      '  where t.id = tp.id'
      ' ) as tp_name,'
      ' org.id as org_id,'
      ' org.name as org_name,'
      ' --z(��)'
      ' (select sum(z.report_sum)'
      '    from buh_zreports z, tax t, documents d'
      '   where z.report_date between :BEGIN_DATE and :END_DATE'
      '     and z.fiscal_taxpayer_id = t.id'
      '     and z.org_id=org.id'
      '     and d.id = z.doc_id '
      '     and d.status <> '#39'D'#39
      '     and d.fldr_id = fnc_get_constant('#39'FLDR_BUH_ZREPORTS_UCH'#39') '
      ' ) as buh_z_amount,'
      ' --z(��) -����'
      ' (select sum(z.srpm_sum)'
      '    from buh_zreports z, '
      '         tax t, '
      '         documents d'
      '   where z.report_date between :BEGIN_DATE and :END_DATE'
      '     and z.fiscal_taxpayer_id = t.id'
      '     and z.org_id=org.id'
      '     and d.id = z.doc_id '
      '     and d.status <> '#39'D'#39
      '     and d.fldr_id = fnc_get_constant('#39'FLDR_BUH_ZREPORTS_UCH'#39')'
      ' ) as srpm_amount,'
      ' --z(��)'
      ' case when org.id in (52,55) then null'
      '  else (select sum(z.report_sum)'
      '          from rtl_zreports z,'
      '               buh_zreports bz,'
      '               documents d,'
      '               documents d1, '
      '               tax t'
      '         where z.doc_id = d.id'
      '           and z.fiscal_taxpayer_id = t.id'
      '           and z.report_date between :BEGIN_DATE and :END_DATE'
      '           and d.src_org_id = org.id'
      '           and d.status <> '#39'D'#39
      '           and bz.ground_doc_id = z.doc_id'
      '           and bz.doc_id = d1.id'
      '           and d1.status <> '#39'D'#39
      
        '           and d1.fldr_id = fnc_get_constant('#39'FLDR_BUH_ZREPORTS_' +
        'UCH'#39')'
      '      )'
      ' end as rtl_z_amount,'
      ' (select sum(r.amount) '
      '    from rtl_srpm r, '
      '         documents d, '
      '         folders f, '
      '         tax t'
      '   where r.doc_id = d.id'
      '     and d.doc_type_id = 1846085'
      '     and f.id = d.fldr_id'
      
        '     and f.id in (fnc_get_constant('#39'FLDR_SRPM_VAL'#39'), fnc_get_con' +
        'stant('#39'FLDR_SRPM_ARC'#39'), fnc_get_constant('#39'FLDR_SRPM_RECALC'#39')) '
      '     and d.status <> '#39'D'#39
      
        '     and r.year between EXTRACT(YEAR FROM :BEGIN_DATE) and EXTRA' +
        'CT(YEAR FROM :END_DATE)'
      
        '     and r.month between EXTRACT(MONTH FROM :BEGIN_DATE) and EXT' +
        'RACT(MONTH FROM :END_DATE)'
      '     and r.taxpayer_id = t.id'
      '     and r.org_id = org.id'
      '     and r.create_sf is null'
      ' ) as rtl_srpm_amount,'
      ' --�����-�������'
      ' (select sum(i.amount)'
      '    from invoice i,'
      '         buh_zreports z,'
      '         tax t,'
      '         documents d'
      '   where i.doc_id = z.doc_id'
      '     and z.org_id = org.id'
      '     and i.doc_type_id = 5325944'
      '     and i.owner_id = t.id'
      '     and i.doc_date between :BEGIN_DATE and :END_DATE'
      '     and d.id = z.doc_id '
      '     and d.status <> '#39'D'#39
      ' ) as invoice_amount'
      'from organizations org,'
      '     buh_zreports z,'
      '     tax t'
      'where z.report_date between :BEGIN_DATE and :END_DATE'
      '  and z.fiscal_taxpayer_id = t.id'
      '  and z.org_id = org.id'
      'UNION'
      'select'
      ' (select'
      '    case'
      
        '     when count(*)  > 1 then trim(rpad('#39'�� ���������� ����������' +
        '�������� c id: '#39'||csl(tp.id),250))'
      '     else max(tp.name)'
      '    end'
      '    from taxpayer tp, sftax t'
      '   where t.id = tp.id'
      ' ) as tp_name,'
      ' null as org_id,'
      ' '#39'��� ���'#39' as org_name,'
      ' --z(��)'
      ' (select sum(z.report_sum)'
      '    from buh_zreports z, '
      '         sftax t, '
      '         documents d'
      '   where z.report_date between :BEGIN_DATE and :END_DATE'
      '     and z.fiscal_taxpayer_id = t.id'
      '     and d.id = z.doc_id '
      '     and d.status <> '#39'D'#39
      '     and d.fldr_id = fnc_get_constant('#39'FLDR_BUH_ZREPORTS_NUCH'#39')'
      ' ) as buh_z_amount,'
      ' --z(��) -����'
      ' (select sum(z.srpm_sum)'
      '    from buh_zreports z, '
      '         sftax t, '
      '         documents d'
      '   where z.report_date between :BEGIN_DATE and :END_DATE'
      '     and z.fiscal_taxpayer_id = t.id'
      '     and d.id = z.doc_id '
      '     and d.status <> '#39'D'#39
      '     and d.fldr_id = fnc_get_constant('#39'FLDR_BUH_ZREPORTS_NUCH'#39')'
      '  ) as srpm_amount,'
      ' --z(��)'
      ' null as rtl_z_amount,'
      ' (select sum(r.amount) '
      '    from rtl_srpm r, '
      '         documents d, '
      '         folders f, '
      '         sftax t'
      '   where r.doc_id = d.id'
      '     and d.doc_type_id = 1846085'
      '     and f.id = d.fldr_id'
      
        '     and f.id in (fnc_get_constant('#39'FLDR_SRPM_VAL'#39'), fnc_get_con' +
        'stant('#39'FLDR_SRPM_ARC'#39'), fnc_get_constant('#39'FLDR_SRPM_RECALC'#39')) '
      '     and d.status <> '#39'D'#39
      
        '     and r.year between EXTRACT(YEAR FROM :BEGIN_DATE) and EXTRA' +
        'CT(YEAR FROM :END_DATE)'
      
        '     and r.month between EXTRACT(MONTH FROM :BEGIN_DATE) and EXT' +
        'RACT(MONTH FROM :END_DATE)'
      '     and r.taxpayer_id = t.id'
      '     and r.create_sf = 1'
      '  ) as rtl_srpm_amount,'
      ' --�����-�������'
      ' (select sum(i.amount)'
      '    from invoice i,'
      '         pay_documents pd,'
      '         rtl_srpm z,'
      '         documents d,'
      '         document_links dl,'
      '         sftax t'
      '   where i.doc_id = pd.doc_id'
      '     and pd.doc_id = dl.dest_doc_id'
      '     and dl.src_doc_id = z.doc_id'
      '     and d.id = pd.doc_id AND d.status <> '#39'D'#39
      '     and i.owner_id = t.id'
      '     and i.doc_date between :BEGIN_DATE and :END_DATE'
      '     and i.doc_type_id = 1729'
      ' ) as invoice_amount'
      'from buh_zreports z,'
      '     sftax t'
      'where z.report_date between :BEGIN_DATE and :END_DATE'
      '  and z.fiscal_taxpayer_id = t.id'
      ')')
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_TAX_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_TAX_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_mainTP_NAME: TStringField
      DisplayLabel = '����������������'
      FieldName = 'TP_NAME'
      FixedChar = True
      Size = 18
    end
    object m_Q_mainORG_ID: TFloatField
      FieldName = 'ORG_ID'
    end
    object m_Q_mainORG_NAME: TStringField
      DisplayLabel = '���'
      DisplayWidth = 15
      FieldName = 'ORG_NAME'
      Size = 250
    end
    object m_Q_mainINVOICE_AMOUNT: TFloatField
      DisplayLabel = '����� ������-������'
      FieldName = 'INVOICE_AMOUNT'
      currency = True
    end
    object m_Q_mainRTL_SRPM_AMOUNT: TFloatField
      DisplayLabel = '������� ����� ���� �� ��������� ������'
      FieldName = 'RTL_SRPM_AMOUNT'
      currency = True
    end
    object m_Q_mainBUH_Z_AMOUNT: TFloatField
      DisplayLabel = '����� Z-�������(��)'
      FieldName = 'BUH_Z_AMOUNT'
      currency = True
    end
    object m_Q_mainSRPM_AMOUNT: TFloatField
      DisplayLabel = '����� ���Z'
      FieldName = 'SRPM_AMOUNT'
      currency = True
    end
    object m_Q_mainRTL_Z_AMOUNT: TFloatField
      DisplayLabel = '����� Z-�������(��)'
      FieldName = 'RTL_Z_AMOUNT'
      currency = True
    end
  end
  object m_Q_tax: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_taxBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select '
      
        '  nvl((select max(1) from param_values where id = :SQ_ID and to_' +
        'number(value) =tp.id),2) as checked,'
      '  tp.id, '
      '  tp.cnt_id,'
      '  tp.name as taxpayer_name,'
      '  m.note as manager, '
      '  ts.name as taxation_name'
      'from taxpayer tp, managers m, taxation_system ts'
      'where tp.cnt_id = m.cnt_id'
      '  and tp.tax_sys_id = ts.id'
      '  and tp.end_date is null'
      '%WHERE_ACT_TAX'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    UpdateObject = m_U_tax
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_ACT_TAX'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 24
    Top = 208
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
    object m_Q_taxCHECKED: TFloatField
      DisplayLabel = ' '
      FieldName = 'CHECKED'
      OnChange = m_Q_taxCHECKEDChange
    end
    object m_Q_taxID: TFloatField
      DisplayLabel = 'ID �����������������'
      FieldName = 'ID'
      Origin = 'tp.id'
      ReadOnly = True
    end
    object m_Q_taxCNT_ID: TFloatField
      DisplayLabel = 'ID �����������'
      FieldName = 'CNT_ID'
      Origin = 'tp.cnt_id'
      ReadOnly = True
    end
    object m_Q_taxTAXPAYER_NAME: TStringField
      DisplayLabel = '����������������'
      FieldName = 'TAXPAYER_NAME'
      Origin = 'tp.name'
      ReadOnly = True
      Size = 100
    end
    object m_Q_taxMANAGER: TStringField
      DisplayLabel = '�����������'
      FieldName = 'MANAGER'
      Origin = 'm.note'
      ReadOnly = True
      Size = 250
    end
    object m_Q_taxTAXATION_NAME: TStringField
      DisplayLabel = '������� ���������������'
      FieldName = 'TAXATION_NAME'
      Origin = 'ts.name'
      ReadOnly = True
      Size = 100
    end
  end
  object m_U_tax: TMLUpdateSQL
    IgnoreRowsAffected = True
    Left = 80
    Top = 208
  end
  object m_Q_ins_param: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'insert into param_values'
      '(id, value)'
      'values (:SQ_ID, :VALUE)')
    Macros = <>
    Left = 32
    Top = 280
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'VALUE'
        ParamType = ptInput
      end>
  end
  object m_Q_del_param: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'delete from param_values'
      'where id = :SQ_ID and to_number(value) = :VALUE')
    Macros = <>
    Left = 104
    Top = 280
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'VALUE'
        ParamType = ptInput
      end>
  end
  object m_Q_ins_all_tax: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'insert into param_values'
      '(id, value)'
      '(select :SQ_ID as id, tp.id as value'
      'from taxpayer tp'
      'where tp.end_date is null'
      'minus '
      'select :SQ_ID as id, to_number(value) as value'
      'from param_values'
      'where id = :SQ_ID)')
    Macros = <>
    Left = 40
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SQ_ID'
        ParamType = ptInput
      end>
  end
end
