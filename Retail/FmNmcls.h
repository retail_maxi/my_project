//---------------------------------------------------------------------------

#ifndef FmNmclsH
#define FmNmclsH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMSUBITEM.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Db.hpp>
#include <Grids.hpp>
#include "DmInvAct.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFNmcls : public TTSFSubItem
{
__published:
  TDataSource *m_DS_nmcls;
  TAction *m_ACT_select_all;
  TSpeedButton *m_SBTN_select_all;
  TCheckBox *m_CB_nonzero;
  TToolButton *m_TBTN_sep4;
  TPageControl *m_PC_nmcl;
  TTabSheet *m_TS_goods;
  TMLDBGrid *m_DBG_nmcls;
  TTabSheet *m_TS_invs;
  TMLDBGrid *m_DBG_nmcls_i;
  TDataSource *m_DS_nmcls_i;
  void __fastcall m_ACT_select_allExecute(TObject *Sender);
  void __fastcall m_ACT_select_allUpdate(TObject *Sender);
  void __fastcall m_CB_nonzeroClick(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_PC_nmclChange(TObject *Sender);
private:
  TDInvAct *m_dm;
public:
  __fastcall TFNmcls(TComponent* p_owner, TTSDCustom *p_dm)
             :TTSFSubItem(p_owner,p_dm),
             m_dm(static_cast<TDInvAct*>(p_dm))
             {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
