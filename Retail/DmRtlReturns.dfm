inherited DRtlReturns: TDRtlReturns
  OldCreateOrder = False
  Left = 436
  Top = 28
  Height = 828
  Width = 950
  inherited m_Q_clear_tmp_rep_param_val: TQuery
    Left = 995
    Top = 22
  end
  inherited m_Q_help_url: TQuery
    Left = 659
    Top = 78
  end
  inherited m_DB_main: TDatabase
    AliasName = 'GG'
    Params.Strings = (
      'USER NAME=ROBOT'
      'PASSWORD=RBT')
    Left = 24
    Top = 14
  end
  inherited m_CC_main: TTSConnectionContainer
    Left = 88
    Top = 14
  end
  inherited m_Q_sq_next_val: TQuery
    Left = 32
    Top = 78
  end
  inherited m_Q_var_type: TQuery
    Left = 200
    Top = 78
  end
  inherited m_Q_const_val: TQuery
    Left = 288
    Top = 78
  end
  inherited m_Q_const_type: TQuery
    Left = 376
    Top = 78
  end
  inherited m_Q_sys_date: TQuery
    Left = 464
    Top = 78
  end
  inherited m_Q_module_caption: TQuery
    Left = 720
    Top = 78
  end
  inherited m_LG_main: TMLLogin
    Left = 160
    Top = 22
  end
  inherited m_Q_sys_user: TQuery
    Left = 544
    Top = 78
  end
  inherited m_Q_reports: TQuery
    Left = 235
    Top = 22
  end
  inherited m_Q_fill_tmp_rep_param_val: TQuery
    Left = 451
    Top = 22
  end
  inherited m_U_fill_tmp_rep_param_val: TUpdateSQL
    Left = 707
    Top = 22
  end
  inherited m_Q_sys_right: TQuery
    Left = 624
    Top = 78
  end
  inherited m_Q_fill_param_val: TQuery
    Left = 328
    Top = 22
  end
  inherited m_U_fill_param_val: TUpdateSQL
    Left = 579
    Top = 22
  end
  inherited m_Q_clear_param_val: TQuery
    Left = 843
    Top = 22
  end
  inherited m_Q_reg_def: TQuery
    Left = 499
    Top = 78
  end
  inherited m_U_reg_def: TUpdateSQL
    Left = 579
    Top = 78
  end
  inherited m_Q_var_val: TQuery
    Top = 78
  end
  inherited m_DB_add_main: TDatabase
    Left = 24
    Top = 46
  end
  inherited m_CC_add_main: TTSConnectionContainer
    Left = 88
    Top = 38
  end
  inherited m_Q_org_par_home_org: TQuery
    Left = 567
    Top = 78
  end
  inherited m_Q_list: TMLQuery
    SQL.Strings = (
      'SELECT d.id AS id,'
      '       d.src_org_id AS doc_src_org_id,'
      '       o.name AS doc_src_org_name,'
      '       d.create_date AS doc_create_date,'
      '       d.comp_name AS doc_create_comp_name,'
      '       d.author AS doc_create_user,'
      '       d.modify_date AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user AS doc_modify_user,'
      '       d.status AS doc_status,'
      
        '       decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �' +
        '� ������'#39','#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) AS doc' +
        '_fldr_right,'
      '       r.issued_date AS issued_date,'
      '       z.fiscal_num AS fiscal_num,'
      '       z.report_num AS report_num,'
      '       z.report_date AS report_date,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(r.cash_user_id), 250)' +
        ' AS cash_user_name,'
      '       c.check_num AS check_num,'
      '       c.check_date AS check_date,'
      '       c.check_sum AS check_sum,'
      '       r.out_return_sum AS out_return_sum,'
      '       r.note AS note,'
      '       r.return_number AS return_number,'
      '       decode(r.cash, '#39'N'#39', '#39'������'#39', '#39'���'#39') as cash,'
      '       r.buf_id'
      'FROM documents d,'
      '     folders f,'
      '     organizations o,'
      '     rtl_returns r,'
      '     rtl_zreports z,'
      '     rtl_checks c,'
      '     contractors t'
      'WHERE f.doc_type_id = :DOC_TYPE_ID'
      '  AND f.id = d.fldr_id'
      '  AND o.id = d.src_org_id'
      '  AND fnc_mask_folder_right(f.id,'#39'Select'#39') = 1'
      '  AND r.doc_id = d.id'
      
        '  AND r.issued_date BETWEEN :BEGIN_DATE AND :END_DATE + 1 - 1/24' +
        '/3600'
      '  AND z.doc_id(+) = r.report_doc_id'
      '  AND c.report_doc_id(+) = r.check_doc_id'
      '  AND c.check_num(+) = r.check_num'
      '  AND t.id(+) = r.cash_user_id'
      
        '  AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_id = :ORG_' +
        'ID))'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      '')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 32
    Top = 134
    ParamData = <
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_listISSUED_DATE: TDateTimeField
      DisplayLabel = '���� ��������'
      FieldName = 'ISSUED_DATE'
      Origin = 'r.issued_date'
    end
    object m_Q_listFISCAL_NUM: TFloatField
      DisplayLabel = '� ��'
      FieldName = 'FISCAL_NUM'
      Origin = 'z.fiscal_num'
    end
    object m_Q_listREPORT_NUM: TFloatField
      DisplayLabel = '� Z-�����'
      FieldName = 'REPORT_NUM'
      Origin = 'z.report_num'
    end
    object m_Q_listREPORT_DATE: TDateTimeField
      DisplayLabel = '���� Z-������'
      FieldName = 'REPORT_DATE'
      Origin = 'z.report_date'
    end
    object m_Q_listCASH_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'CASH_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(r.cash_user_id), 250) '
      Size = 250
    end
    object m_Q_listCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'c.check_num'
    end
    object m_Q_listCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'c.check_date'
    end
    object m_Q_listCHECK_SUM: TFloatField
      DisplayLabel = '����� ����'
      FieldName = 'CHECK_SUM'
      Origin = 'c.check_sum'
    end
    object m_Q_listOUT_RETURN_SUM: TFloatField
      DisplayLabel = '����� ��������'
      FieldName = 'OUT_RETURN_SUM'
      Origin = 'r.out_return_sum'
    end
    object m_Q_listNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      Origin = 'r.note'
      Size = 250
    end
    object m_Q_listRETURN_NUMBER: TFloatField
      DisplayLabel = '����� ��������'
      FieldName = 'RETURN_NUMBER'
      Origin = 'r.return_number'
    end
    object m_Q_listCASH: TStringField
      DisplayLabel = '���/������'
      FieldName = 'CASH'
      Origin = 'decode(r.cash, '#39'N'#39', '#39'������'#39', '#39'���'#39')'
      Size = 6
    end
    object m_Q_listBUF_ID: TFloatField
      FieldName = 'BUF_ID'
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'SELECT r.doc_id AS id,'
      '       r.report_doc_id AS report_doc_id,'
      '       r.issued_date AS issued_date,'
      '       z.fiscal_num AS fiscal_num,'
      '       z.report_num AS report_num,'
      '       z.report_date AS report_date,'
      '       r.adm_user_id AS adm_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(r.adm_user_id), 250) ' +
        'AS adm_user_name,'
      '       r.cash_user_id AS cash_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(r.cash_user_id), 250)' +
        ' AS cash_user_name,'
      '       r.ret_user_id AS ret_user_id,'
      
        '       nvl(RPAD(pkg_contractors.fnc_contr_name(r.ret_user_id), 2' +
        '50), '#39'����'#39') AS ret_user_name,'
      '       r.check_doc_id AS check_doc_id,'
      '       c.check_num AS check_num,'
      '       c.check_date AS check_date,'
      '       c.check_sum AS check_sum,'
      '       r.out_return_sum AS out_return_sum,'
      '       r.note AS note,'
      '       r.return_number AS return_number,'
      '       r.cash,'
      '       r.buy_fio,'
      '       sysdate - 360 as start_date,'
      '       trunc(sysdate) as end_date,'
      '       r.is_check,'
      '       r.decision_return,'
      '       r.cashier_error,'
      '       r.buf_id'
      'FROM rtl_returns r,'
      '     rtl_zreports z,'
      '     rtl_checks c,'
      '     contractors t'
      
        'WHERE r.doc_id = (SELECT id FROM documents WHERE id = :ID AND fl' +
        'dr_id = :FLDR_ID)'
      '  AND z.doc_id(+) = r.report_doc_id'
      '  AND c.report_doc_id(+) = r.check_doc_id'
      '  AND c.check_num(+) = r.check_num'
      '  AND t.id(+) = r.cash_user_id'
      ''
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Top = 134
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FLDR_ID'
        ParamType = ptInput
      end>
    object m_Q_itemREPORT_DOC_ID: TFloatField
      FieldName = 'REPORT_DOC_ID'
    end
    object m_Q_itemISSUED_DATE: TDateTimeField
      FieldName = 'ISSUED_DATE'
    end
    object m_Q_itemFISCAL_NUM: TFloatField
      FieldName = 'FISCAL_NUM'
    end
    object m_Q_itemREPORT_NUM: TFloatField
      FieldName = 'REPORT_NUM'
    end
    object m_Q_itemREPORT_DATE: TDateTimeField
      FieldName = 'REPORT_DATE'
      Visible = False
    end
    object m_Q_itemCASH_USER_ID: TFloatField
      FieldName = 'CASH_USER_ID'
    end
    object m_Q_itemCASH_USER_NAME: TStringField
      FieldName = 'CASH_USER_NAME'
      Size = 250
    end
    object m_Q_itemCHECK_DOC_ID: TFloatField
      FieldName = 'CHECK_DOC_ID'
    end
    object m_Q_itemCHECK_NUM: TFloatField
      FieldName = 'CHECK_NUM'
    end
    object m_Q_itemCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object m_Q_itemCHECK_SUM: TFloatField
      FieldName = 'CHECK_SUM'
    end
    object m_Q_itemOUT_RETURN_SUM: TFloatField
      FieldName = 'OUT_RETURN_SUM'
    end
    object m_Q_itemNOTE: TStringField
      FieldName = 'NOTE'
      Size = 250
    end
    object m_Q_itemRETURN_NUMBER: TFloatField
      FieldName = 'RETURN_NUMBER'
    end
    object m_Q_itemRET_USER_ID: TFloatField
      FieldName = 'RET_USER_ID'
    end
    object m_Q_itemRET_USER_NAME: TStringField
      FieldName = 'RET_USER_NAME'
      Size = 250
    end
    object m_Q_itemCASH: TStringField
      FieldName = 'CASH'
      FixedChar = True
      Size = 1
    end
    object m_Q_itemBUY_FIO: TStringField
      FieldName = 'BUY_FIO'
      Size = 250
    end
    object m_Q_itemSTART_DATE: TDateTimeField
      FieldName = 'START_DATE'
    end
    object m_Q_itemIS_CHECK: TStringField
      FieldName = 'IS_CHECK'
      FixedChar = True
      Size = 1
    end
    object m_Q_itemDECISION_RETURN: TStringField
      FieldName = 'DECISION_RETURN'
      FixedChar = True
      Size = 1
    end
    object m_Q_itemCASHIER_ERROR: TStringField
      FieldName = 'CASHIER_ERROR'
      FixedChar = True
      Size = 1
    end
    object m_Q_itemADM_USER_ID: TFloatField
      FieldName = 'ADM_USER_ID'
    end
    object m_Q_itemADM_USER_NAME: TStringField
      FieldName = 'ADM_USER_NAME'
      Size = 250
    end
    object m_Q_itemBUF_ID: TFloatField
      FieldName = 'BUF_ID'
    end
    object m_Q_itemEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
  end
  inherited m_U_item: TMLUpdateSQL
    ModifySQL.Strings = (
      'update rtl_returns'
      'set'
      '  REPORT_DOC_ID = :REPORT_DOC_ID,'
      '  CASH_USER_ID = :CASH_USER_ID,'
      '  CHECK_DOC_ID = :CHECK_DOC_ID,'
      '  CHECK_NUM = :CHECK_NUM,'
      '  NOTE = :NOTE,'
      '  RETURN_NUMBER = :RETURN_NUMBER,'
      '  RET_USER_ID =:RET_USER_ID,'
      '  CASH = decode(:RET_USER_ID, -1, '#39'Y'#39', :CASH),'
      '  BUY_FIO =:BUY_FIO,'
      '  IS_CHECK=:IS_CHECK,'
      '  CASHIER_ERROR=:CASHIER_ERROR,'
      '  DECISION_RETURN=:DECISION_RETURN,'
      '  ADM_USER_ID=:ADM_USER_ID'
      'where'
      '  DOC_ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into rtl_returns'
      
        '  (DOC_ID, REPORT_DOC_ID, ISSUED_DATE, CASH_USER_ID, CHECK_DOC_I' +
        'D, '
      'CHECK_NUM, OUT_RETURN_SUM, IN_RETURN_SUM, NOTE, '
      'RETURN_NUMBER,RET_USER_ID, CASH, '
      'BUY_FIO,IS_CHECK,CASHIER_ERROR,DECISION_RETURN,ADM_USER_ID)'
      'values'
      
        '  (:ID, :REPORT_DOC_ID, :ISSUED_DATE, :CASH_USER_ID, :CHECK_DOC_' +
        'ID, '
      
        ':CHECK_NUM, 0, 0, :NOTE, :RETURN_NUMBER, :RET_USER_ID, decode(:R' +
        'ET_USER_ID, -1, '#39'Y'#39', :CASH), '
      ':BUY_FIO,:IS_CHECK,:CASHIER_ERROR,:DECISION_RETURN,:ADM_USER_ID)'
      ' ')
    DeleteSQL.Strings = (
      'BEGIN'
      '  prc_delete_doc(:OLD_ID);'
      'END; ')
    Top = 134
  end
  inherited m_Q_doc_type_id: TQuery
    Left = 248
    Top = 134
  end
  inherited m_SP_prc_insert_doc: TStoredProc
    Left = 40
    Top = 190
  end
  inherited m_SP_cr_doc_numb: TStoredProc
    Left = 152
    Top = 190
  end
  inherited m_Q_free_doc: TQuery
    Left = 248
    Top = 190
  end
  inherited m_SP_chg_fldr: TStoredProc
    Left = 328
    Top = 190
  end
  inherited m_SP_status_doc: TStoredProc
    Left = 416
    Top = 190
  end
  inherited m_SP_fr_doc_numb: TStoredProc
    Left = 512
    Top = 190
  end
  inherited m_Q_folder: TRxQuery
    Left = 144
    Top = 246
  end
  inherited m_Q_fldr_insert: TQuery
    Left = 48
    Top = 246
  end
  inherited m_Q_document: TQuery
    Left = 232
    Top = 246
  end
  inherited m_Q_doc_fldr_route: TQuery
    Left = 616
    Top = 190
  end
  inherited m_SP_chg_fldrs: TStoredProc
    Left = 712
    Top = 190
  end
  inherited m_Q_doc_param: TMLQuery
    Left = 368
    Top = 246
  end
  inherited m_U_doc_param: TUpdateSQL
    Left = 464
    Top = 246
  end
  inherited m_Q_acc_serv: TQuery
    Left = 648
    Top = 134
  end
  inherited m_Q_load_history: TQuery
    Left = 423
    Top = 78
  end
  object m_Q_lines: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_linesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT w.line_id, w.nmcl_id, w.nmcl_name, w.assort_id, w.assort_' +
        'name, w.bar_code,'
      'w.out_price, w.quantity, w.IS_ONE_PIECE'
      'FROM'
      '(SELECT rl.line_id AS line_id,'
      '       ni.id AS nmcl_id,'
      '       ni.name AS nmcl_name,'
      '       a.id AS assort_id,'
      '       a.name AS assort_name,'
      '       rl.bar_code AS bar_code,'
      '       rl.out_price AS out_price,'
      '       rl.quantity AS quantity,'
      '       0 AS IS_ONE_PIECE'
      'FROM rtl_return_lines rl,'
      '     nomenclature_items ni,'
      '     assortment a'
      'WHERE rl.doc_id = :ID'
      '  AND ni.id = rl.nmcl_id'
      '  AND a.id = rl.assort_id'
      '  and (rl.is_one_piece is null'
      'or rl.is_one_piece != 1)'
      ''
      'union'
      ''
      'SELECT max(rl.line_id) AS line_id,'
      '       ni.id AS nmcl_id,'
      '       ni.name AS nmcl_name,'
      '       a.id AS assort_id,'
      '       a.name AS assort_name,'
      '       rl.bar_code AS bar_code,'
      
        '       round(sum(rl.out_price*rl.quantity)/sum(rl.quantity),2) A' +
        'S out_price,'
      '       sum(rl.quantity) AS quantity,'
      '      1 AS IS_ONE_PIECE'
      'FROM rtl_return_lines rl,'
      '     nomenclature_items ni,'
      '     assortment a'
      'WHERE rl.doc_id = :ID'
      '  AND ni.id = rl.nmcl_id'
      '  AND a.id = rl.assort_id'
      '  and rl.is_one_piece = 1'
      'group by ni.id, ni.name, a.id,'
      '         a.name, rl.bar_code'
      ') w'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      '')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 48
    Top = 310
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_linesLINE_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'LINE_ID'
      Origin = 'w.line_id'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'NMCL_ID'
      Origin = 'w.nmcl_id'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'w.NMCL_NAME'
      Size = 100
    end
    object m_Q_linesASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Origin = 'w.assort_id'
      Visible = False
    end
    object m_Q_linesASSORT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORT_NAME'
      Origin = 'w.ASSORT_NAME'
      Size = 100
    end
    object m_Q_linesBAR_CODE: TStringField
      DisplayLabel = '�����-���'
      FieldName = 'BAR_CODE'
      Origin = 'w.BAR_CODE'
      Size = 30
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'w.OUT_PRICE'
    end
    object m_Q_linesQUANTITY: TFloatField
      DisplayLabel = '���.'
      FieldName = 'QUANTITY'
      Origin = 'w.QUANTITY'
    end
    object m_Q_linesIS_ONE_PIECE: TFloatField
      FieldName = 'IS_ONE_PIECE'
      Origin = 'w.IS_ONE_PIECE'
      Visible = False
    end
  end
  object m_Q_zreports: TMLQuery
    BeforeOpen = m_Q_zreportsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT r.doc_id,'
      '       r.fiscal_num,'
      '       r.report_num,'
      '       r.report_date,'
      '       d.create_date,'
      '       RPAD(t.name, 250) AS taxpayer_name'
      '  FROM rtl_zreports r, documents d, taxpayer t'
      ' WHERE r.report_date IS NULL'
      '   AND r.doc_id = d.id'
      '   AND r.fiscal_taxpayer_id = t.id'
      '   AND d.src_org_id = session_globals.get_home_org_id'
      '   AND (:REPORT_DOC_ID = -1'
      
        '           or r.fiscal_taxpayer_id = (select max(z.fiscal_taxpay' +
        'er_id) from rtl_zreports z'
      
        '                                               where z.doc_id = ' +
        ':REPORT_DOC_ID))'
      '%WHERE_CLAUSE'
      ' ORDER BY RPAD(t.name, 250), d.create_date, fiscal_num')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 48
    Top = 366
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_zreportsCREATE_DATE: TDateTimeField
      DisplayLabel = '���� ����.'
      DisplayWidth = 65
      FieldName = 'CREATE_DATE'
      Origin = 'd.CREATE_DATE'
    end
    object m_Q_zreportsREPORT_DATE: TDateTimeField
      DisplayLabel = '���� Z-������'
      DisplayWidth = 50
      FieldName = 'REPORT_DATE'
      Origin = 'R.REPORT_DATE'
      Visible = False
    end
    object m_Q_zreportsTAXPAYER_NAME: TStringField
      DisplayLabel = '��. ����'
      DisplayWidth = 100
      FieldName = 'TAXPAYER_NAME'
      Origin = 'RPAD(t.name, 250)'
      Size = 250
    end
    object m_Q_zreportsDOC_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'DOC_ID'
      Origin = 'R.DOC_ID'
      Visible = False
    end
    object m_Q_zreportsFISCAL_NUM: TFloatField
      DisplayLabel = '� ��'
      DisplayWidth = 25
      FieldName = 'FISCAL_NUM'
      Origin = 'R.FISCAL_NUM'
    end
    object m_Q_zreportsREPORT_NUM: TFloatField
      DisplayLabel = '� Z-������'
      DisplayWidth = 40
      FieldName = 'REPORT_NUM'
      Origin = 'R.REPORT_NUM'
    end
  end
  object m_Q_cashs: TMLQuery
    BeforeOpen = m_Q_cashsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT print_user_id AS cash_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250) ' +
        'AS cash_user_name'
      'FROM ('
      '      SELECT print_user_id'
      '      FROM rtl_checks'
      '      WHERE report_doc_id = :REPORT_DOC_ID'
      '      GROUP BY print_user_id'
      '     )'
      '%WHERE_EXPRESSION'
      'ORDER BY cash_user_id'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 136
    Top = 366
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_cashsCASH_USER_ID: TFloatField
      DisplayLabel = 'ID'
      DisplayWidth = 50
      FieldName = 'CASH_USER_ID'
      Origin = 'print_user_id'
    end
    object m_Q_cashsCASH_USER_NAME: TStringField
      DisplayLabel = '������'
      DisplayWidth = 100
      FieldName = 'CASH_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250)'
      Size = 250
    end
  end
  object m_Q_checks: TMLQuery
    BeforeOpen = m_Q_checksBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT  /*+ INDEX(C XIF_RTL_CHECKS_DR)  INDEX(D XPK_DOCUMENTS) *' +
        '/    '
      '       report_doc_id,'
      '       check_num,'
      '       check_date,'
      '       check_sum,'
      '       cash'
      'FROM rtl_checks c, documents d,'
      '     folders f'
      
        'WHERE c.check_date between :START_DATE and :END_DATE + 1 - 1/24/' +
        '3600'
      'and c.report_doc_id = d.id'
      'and d.src_org_id = :HOME_ORG_ID'
      'and f.id = d.fldr_id '
      'and f.doc_type_id = 188942'
      '%WHERE_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 224
    Top = 366
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'START_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_checksREPORT_DOC_ID: TFloatField
      FieldName = 'REPORT_DOC_ID'
      Origin = 'TSDBMAIN.RTL_CHECKS.REPORT_DOC_ID'
      Visible = False
    end
    object m_Q_checksCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'CHECK_NUM'
    end
    object m_Q_checksCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'CHECK_DATE'
    end
    object m_Q_checksCHECK_SUM: TFloatField
      DisplayLabel = '����� ����'
      FieldName = 'CHECK_SUM'
      Origin = 'CHECK_SUM'
    end
    object m_Q_checksCASH: TStringField
      FieldName = 'CASH'
      Origin = 'TSDBMAIN.RTL_CHECKS.CASH'
      FixedChar = True
      Size = 1
    end
  end
  object m_Q_sale_lines: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT w.doc_id, w.line_id, w.nmcl_id, w.nmcl_name, w.assort_id,'
      '       w.assort_name, w.weight_code, w.bar_code, w.out_price,'
      '       w.in_price, w.quantity, w.nds, w.shop_dep_id'
      'FROM'
      '('
      'SELECT sl.doc_id AS doc_id,'
      '       sl.line_id AS line_id,'
      '       sl.nmcl_id AS nmcl_id,'
      '       ni.name AS nmcl_name,'
      '       sl.assort_id AS assort_id,'
      '       a.name AS assort_name,'
      '       sl.weight_code AS weight_code,'
      '       sl.bar_code AS bar_code,'
      '       sl.out_price AS out_price,'
      '       sl.in_price AS in_price,'
      '       sl.quantity AS quantity,'
      '       sl.nds as nds,'
      '       sl.shop_dep_id'
      'FROM rtl_sale_lines sl,'
      '     rtl_checks c,'
      '     nomenclature_items ni,'
      '     assortment a'
      'WHERE sl.doc_id = c.sale_doc_id'
      '  AND c.report_doc_id = :REPORT_DOC_ID'
      '  AND c.check_num = :CHECK_NUM'
      '  AND ni.id = sl.nmcl_id'
      '  AND sl.check_num = :CHECK_NUM'
      '  AND a.id = sl.assort_id'
      '  and sl.is_one_piece is null'
      '  and sl.line_id not in (SELECT rl2.sale_line_id'
      '                           FROM rtl_sale_lines sl2,'
      '                                rtl_return_lines rl2,'
      '                                nomenclature_items ni2,'
      '                                assortment a2'
      '                  WHERE rl2.doc_id = :DOC_ID'
      '                  AND sl2.doc_id = rl2.sale_doc_id'
      '                  AND sl2.line_id = rl2.sale_line_id'
      '                  AND ni2.id = sl2.nmcl_id'
      '                  AND sl2.check_num = :CHECK_NUM'
      '                  and a2.id = sl2.assort_id)'
      ''
      'union'
      'SELECT max(sl.doc_id) AS doc_id,'
      '       max(sl.line_id) AS line_id,'
      '       sl.nmcl_id AS nmcl_id,'
      '       ni.name AS nmcl_name,'
      '       sl.assort_id AS assort_id,'
      '       a.name AS assort_name,'
      '       sl.weight_code AS weight_code,'
      '       sl.bar_code AS bar_code,'
      
        '       round(sum(sl.out_price*sl.quantity)/sum(sl.quantity),2) A' +
        'S out_price,'
      
        '       round(sum(sl.in_price*sl.quantity)/sum(sl.quantity),2) AS' +
        ' in_price,'
      '       sum(sl.quantity) AS quantity,'
      '       sl.nds as nds,'
      '       sl.shop_dep_id'
      'FROM rtl_sale_lines sl,'
      '     rtl_checks c,'
      '     nomenclature_items ni,'
      '     assortment a'
      'WHERE sl.doc_id = c.sale_doc_id'
      '  AND c.report_doc_id = :REPORT_DOC_ID'
      '  AND c.check_num = :CHECK_NUM'
      '  AND ni.id = sl.nmcl_id'
      '  AND sl.check_num = :CHECK_NUM'
      '  AND a.id = sl.assort_id'
      '  and sl.is_one_piece is not null'
      
        '  and sl.line_id not in (SELECT rl2.sale_line_id FROM rtl_sale_l' +
        'ines sl2,'
      '                                rtl_return_lines rl2,'
      '                                nomenclature_items ni2,'
      '                                assortment a2'
      '                  WHERE rl2.doc_id = :DOC_ID'
      '                  AND sl2.doc_id = rl2.sale_doc_id'
      '                  AND sl2.line_id = rl2.sale_line_id'
      '                  AND ni2.id = sl2.nmcl_id'
      '                  AND sl2.check_num = :CHECK_NUM'
      '                  and a2.id = sl2.assort_id)'
      '  '
      
        'group by sl.nmcl_id, ni.name, sl.assort_id, a.name, sl.weight_co' +
        'de, sl.bar_code, sl.nds,'
      '       sl.shop_dep_id'
      ') w'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 48
    Top = 430
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CHECK_NUM'
        ParamType = ptInput
      end>
    object m_Q_sale_linesLINE_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'LINE_ID'
      Origin = 'w.LINE_ID'
    end
    object m_Q_sale_linesNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'NMCL_ID'
      Origin = 'w.NMCL_ID'
    end
    object m_Q_sale_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'w.NMCL_NAME'
      Size = 100
    end
    object m_Q_sale_linesASSORT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORT_NAME'
      Origin = 'w.ASSORT_NAME'
      Size = 100
    end
    object m_Q_sale_linesBAR_CODE: TStringField
      DisplayLabel = '�����-���'
      FieldName = 'BAR_CODE'
      Origin = 'w.BAR_CODE'
      Size = 30
    end
    object m_Q_sale_linesOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'w.OUT_PRICE'
    end
    object m_Q_sale_linesQUANTITY: TFloatField
      DisplayLabel = '����������'
      FieldName = 'QUANTITY'
      Origin = 'w.QUANTITY'
    end
    object m_Q_sale_linesWEIGHT_CODE: TFloatField
      FieldName = 'WEIGHT_CODE'
    end
    object m_Q_sale_linesDOC_ID: TFloatField
      FieldName = 'DOC_ID'
    end
    object m_Q_sale_linesASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
    end
    object m_Q_sale_linesIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
    end
    object m_Q_sale_linesNDS: TFloatField
      FieldName = 'NDS'
    end
    object m_Q_sale_linesSHOP_DEP_ID: TFloatField
      FieldName = 'SHOP_DEP_ID'
    end
  end
  object m_Q_nmcls: TMLQuery
    BeforeOpen = m_Q_nmclsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select DISTINCT'
      '       ni.id AS nmcl_id,'
      '       ni.name AS nmcl_name'
      'from goods_record_cards g,'
      '     retail_goods_cards r,'
      '     nomenclature_items ni,'
      '     rtl_zreports rz,'
      '     taxpayer t,'
      '     view_mngr_nomenclature vmn'
      'where g.id = r.card_id'
      'and g.nmcl_id = ni.id'
      'and rz.doc_id = :REPORT_DOC_ID'
      'and t.id = rz.fiscal_taxpayer_id'
      'and vmn.mngr_id = t.cnt_id'
      'and vmn.nmcl_id = g.nmcl_id'
      'and vmn.org_id = :HOME_ORG_ID'
      '%WHERE_CLAUSE'
      'ORDER BY ni.name')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 48
    Top = 510
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_nmclsNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      DisplayWidth = 20
      FieldName = 'NMCL_ID'
      Origin = 'NI.ID'
    end
    object m_Q_nmclsNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'NI.NAME'
      Size = 100
    end
  end
  object m_Q_assorts: TMLQuery
    BeforeOpen = m_Q_assortsBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select DISTINCT'
      '       a.id AS assort_id,'
      '       a.name AS assort_name'
      'from goods_record_cards g,'
      '     retail_goods_cards r,'
      '     assortment a'
      'where g.id = r.card_id'
      'and g.nmcl_id = :NMCL_ID'
      'and g.assortment_id = a.id'
      '%WHERE_CLAUSE'
      'ORDER BY a.name'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 128
    Top = 510
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_Q_assortsASSORT_ID: TFloatField
      DisplayLabel = 'ID ������������'
      DisplayWidth = 20
      FieldName = 'ASSORT_ID'
      Origin = 'A.ID'
    end
    object m_Q_assortsASSORT_NAME: TStringField
      DisplayLabel = '�����������'
      FieldName = 'ASSORT_NAME'
      Origin = 'A.NAME'
      Size = 100
    end
  end
  object m_Q_recalc: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'UPDATE rtl_returns'
      
        'SET out_return_sum = NVL((SELECT SUM(ROUND(out_price * quantity,' +
        ' 2)) FROM rtl_return_lines WHERE doc_id = :DOC_ID), 0),'
      
        '    in_return_sum = NVL((SELECT SUM(ROUND(in_price * quantity, 2' +
        ')) FROM rtl_return_lines WHERE doc_id = :DOC_ID), 0)'
      'WHERE doc_id = :DOC_ID'
      ' ')
    Left = 376
    Top = 494
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end>
  end
  object m_Q_price: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT NVL(pkg_rtl_prices.fnc_cur_out_price(:NMCL_ID, :ASSORT_ID' +
        '), 0) AS out_price,'
      
        '       NVL(pkg_rtl_prices.fnc_cur_in_price(:NMCL_ID, :ASSORT_ID)' +
        ', 0) AS in_price'
      'FROM dual'
      ' ')
    Left = 376
    Top = 550
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end>
    object m_Q_priceOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
    end
    object m_Q_priceIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
    end
  end
  object m_Q_cash_ret: TMLQuery
    BeforeOpen = m_Q_cash_retBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select complete_user_id, cash_user_name from ('
      'SELECT complete_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(complete_user_id), 25' +
        '0) AS cash_user_name'
      'FROM ('
      
        'select complete_user_id from rtl_sales rs, rtl_zreports z, docum' +
        'ents d'
      
        'where rs.complete between d.create_date and nvl(z.report_date, s' +
        'ysdate)'
      'and d.id = z.doc_id'
      'and z.doc_id = :REPORT_DOC_ID'
      'group by complete_user_id)'
      'union all'
      
        'select -1 as complete_user_id, '#39'����'#39' as cash_user_name from dua' +
        'l)'
      '%WHERE_EXPRESSION'
      'ORDER BY 1'
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 136
    Top = 422
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_cash_retCOMPLETE_USER_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'COMPLETE_USER_ID'
      Origin = 'COMPLETE_USER_ID'
    end
    object m_Q_cash_retCASH_USER_NAME: TStringField
      DisplayLabel = '���'
      FieldName = 'CASH_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(complete_user_id), 250)'
      Size = 250
    end
  end
  object m_Q_zks: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select max(doc_id) as zks from doc_report_zks_s2'
      'where doc_s_id = :REPORT_DOC_ID'
      ' ')
    Macros = <>
    Left = 224
    Top = 422
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_zksZKS: TFloatField
      FieldName = 'ZKS'
    end
  end
  object m_Q_line: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT doc_id,'
      '       line_id,'
      '       sale_doc_id,'
      '       sale_line_id,'
      '       nmcl_id,'
      
        '       (SELECT ni.name FROM nomenclature_items ni WHERE ni.id = ' +
        'nmcl_id) AS nmcl_name,'
      '       assort_id,'
      
        '       (SELECT a.name FROM assortment a WHERE a.id = NVL(assort_' +
        'id,7)) AS assort_name,'
      '       weight_code,'
      '       bar_code,'
      '       out_price,'
      '       in_price,'
      '       quantity,'
      '       nds,'
      '       shop_dep_id,'
      '      IS_ONE_PIECE'
      'FROM rtl_return_lines'
      'WHERE doc_id = :DOC_ID'
      '  AND line_id = :LINE_ID'
      ' ')
    UpdateObject = m_U_line
    Macros = <>
    Left = 128
    Top = 314
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_lineDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Origin = 'DOC_ID'
    end
    object m_Q_lineLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Origin = 'LINE_ID'
    end
    object m_Q_lineSALE_DOC_ID: TFloatField
      FieldName = 'SALE_DOC_ID'
      Origin = 'SALE_DOC_ID'
    end
    object m_Q_lineSALE_LINE_ID: TFloatField
      FieldName = 'SALE_LINE_ID'
      Origin = 'SALE_LINE_ID'
    end
    object m_Q_lineNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'NMCL_ID'
    end
    object m_Q_lineNMCL_NAME: TStringField
      FieldName = 'NMCL_NAME'
      Origin = 'SELECT ni.name FROM nomenclature_items ni WHERE ni.id = nmcl_id'
      Size = 100
    end
    object m_Q_lineASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Origin = 'ASSORT_ID'
    end
    object m_Q_lineASSORT_NAME: TStringField
      FieldName = 'ASSORT_NAME'
      Origin = 'SELECT a.name FROM assortment a WHERE a.id = NVL(assort_id,7)'
      Size = 100
    end
    object m_Q_lineWEIGHT_CODE: TFloatField
      FieldName = 'WEIGHT_CODE'
      Origin = 'WEIGHT_CODE'
    end
    object m_Q_lineBAR_CODE: TStringField
      FieldName = 'BAR_CODE'
      Origin = 'BAR_CODE'
      Size = 30
    end
    object m_Q_lineOUT_PRICE: TFloatField
      FieldName = 'OUT_PRICE'
      Origin = 'OUT_PRICE'
    end
    object m_Q_lineIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      Origin = 'IN_PRICE'
    end
    object m_Q_lineQUANTITY: TFloatField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
    end
    object m_Q_lineNDS: TFloatField
      FieldName = 'NDS'
      Origin = 'NDS'
    end
    object m_Q_lineSHOP_DEP_ID: TFloatField
      FieldName = 'SHOP_DEP_ID'
      Origin = 'SHOP_DEP_ID'
    end
    object m_Q_lineIS_ONE_PIECE: TFloatField
      FieldName = 'IS_ONE_PIECE'
    end
  end
  object m_U_line: TMLUpdateSQL
    InsertSQL.Strings = (
      '     insert into rtl_return_lines'
      
        '  (DOC_ID, LINE_ID, SALE_DOC_ID, SALE_LINE_ID, NMCL_ID, ASSORT_I' +
        'D, WEIGHT_CODE,'
      
        '   BAR_CODE, OUT_PRICE, IN_PRICE, QUANTITY, NDS, SHOP_DEP_ID, IS' +
        '_ONE_PIECE)'
      ''
      
        '    select id, sq_rtl_return_lines.nextval, doc_id, line_id, nmc' +
        'l_id, assort_id, weight_code, bar_code, out_price, in_price, qnt' +
        ', nds, shop_dep_id, is_one_piece'
      '    from ('
      
        '     select :DOC_ID as id, doc_id, line_id, nmcl_id, assort_id, ' +
        'weight_code, bar_code, out_price, in_price, least( :QUANTITY ,qu' +
        'antity) as qnt, nds, shop_dep_id, is_one_piece'
      '     from rtl_sale_lines'
      
        '     where (doc_id, nmcl_id, nvl(assort_id,7),nvl(weight_code,0)' +
        ', nvl(bar_code,'#39'-1'#39'), nvl(is_one_piece,0), decode(is_one_piece,n' +
        'ull,line_id,is_one_piece)) in'
      
        '     ( select doc_id, nmcl_id, nvl(assort_id,7),nvl(weight_code,' +
        '0), nvl(bar_code,'#39'-1'#39'), nvl(is_one_piece,0), decode(is_one_piece' +
        ',null,line_id,is_one_piece)'
      '       from rtl_sale_lines'
      '       where doc_id = :SALE_DOC_ID'
      '       and line_id = :SALE_LINE_ID'
      '     )'
      '     union'
      
        '     select :DOC_ID, null, null, :nmcl_id, :assort_id, :weight_c' +
        'ode, :bar_code, :out_price, :in_price, :QUANTITY, null, null, nu' +
        'll from dual'
      '     where :SALE_DOC_ID is null and :SALE_LINE_ID is null)')
    DeleteSQL.Strings = (
      'delete from rtl_return_lines'
      'where'
      '  DOC_ID = :OLD_DOC_ID and'
      
        '  (LINE_ID = :OLD_LINE_ID or (:OLD_IS_ONE_PIECE =1 and IS_ONE_PI' +
        'ECE = :OLD_IS_ONE_PIECE and nmcl_id = :OLD_NMCL_ID AND ASSORT_ID' +
        ' = :OLD_ASSORT_ID ))'
      '')
    IgnoreRowsAffected = True
    Left = 224
    Top = 314
  end
  object m_Q_check_nmcl: TMLQuery
    BeforeOpen = m_Q_check_nmclBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select count(*) as ch'
      'from goods_record_cards grc'
      'where grc.org_id = :ORG_ID'
      'and grc.nmcl_id = :NMCL_ID'
      'and grc.assortment_id = :ASSORT_ID'
      'and grc.rec_card_type_id = 1458')
    Macros = <>
    Left = 48
    Top = 590
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ASSORT_ID'
        ParamType = ptInput
      end>
    object m_Q_check_nmclCH: TFloatField
      FieldName = 'CH'
    end
  end
  object m_Q_staff_list: TMLQuery
    BeforeOpen = m_Q_staff_listBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select s.cnt_id as adm_user_id,'
      
        '       trim(rpad(pkg_contractors.fnc_contr_full_name(s.cnt_id), ' +
        '250)) as adm_user_name'
      'from staffmen s,'
      '    staff_table st'
      'where s.fire_date IS NULL'
      'and st.id = s.staff_table_id'
      'and st.dep_id in (select d.id'
      '                  from departments d'
      '                  where d.disable_date is null'
      '                  start with id = (select dep_id'
      
        '                                   from staff.selfcontrol_organi' +
        'zation so'
      '                                   where so.org_id = :ORG_ID)'
      '                  connect by prior id = master_id)'
      '%WHERE_CLAUSE')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
        Value = '0=0'
      end>
    Left = 304
    Top = 318
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_staff_listADM_USER_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'ADM_USER_ID'
      Origin = 's.cnt_id'
    end
    object m_Q_staff_listADM_USER_NAME: TStringField
      DisplayLabel = '�.�.�.'
      FieldName = 'ADM_USER_NAME'
      Origin = 'trim(rpad(pkg_contractors.fnc_contr_full_name(s.cnt_id), 250))'
      Size = 250
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 544
    Top = 248
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_Q_form_quan: TMLQuery
    BeforeOpen = m_Q_form_quanBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select Case when ni.meas_unit_id != 1 and trunc(nvl(:QUANTITY, 0' +
        ')) != nvl(:QUANTITY, 0) then 1 else 0 end as err_quant'
      '  from nomenclature_items ni'
      ' where ni.id = :NMCL_ID'
      '   and :QUANTITY = :QUANTITY')
    Macros = <>
    Left = 136
    Top = 590
    ParamData = <
      item
        DataType = ftFloat
        Name = 'QUANTITY'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'QUANTITY'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'QUANTITY'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'QUANTITY'
        ParamType = ptInput
      end>
    object m_Q_form_quanERR_QUANT: TFloatField
      FieldName = 'ERR_QUANT'
    end
  end
  object m_Q_get_taxpayer: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select fiscal_taxpayer_id'
      '  from rtl_zreports'
      ' where doc_id = :REPORT_DOC_ID'
      ' ')
    Macros = <>
    Left = 224
    Top = 494
    ParamData = <
      item
        DataType = ftInteger
        Name = 'REPORT_DOC_ID'
        ParamType = ptInput
      end>
    object m_Q_get_taxpayerFISCAL_TAXPAYER_ID: TFloatField
      FieldName = 'FISCAL_TAXPAYER_ID'
      Origin = 'TSDBMAIN.RTL_ZREPORTS.FISCAL_TAXPAYER_ID'
    end
  end
  object m_Q_alc: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcBeforeOpen
    AfterInsert = m_Q_alcAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select s.doc_id,'
      '           s.line_id,'
      '           s.sub_line_id,'
      '           s.nmcl_id,'
      '           s.assort_id,'
      '           s.pdf_bar_code, '
      '           s.egais_price,'
      '           s.volume,'
      '           s.price_mrz,'
      '           s.is_full,'
      '           s.check_num, '
      '           s.report_num,'
      '           s.inn,'
      '           s.kpp,'
      '           s.alc_code'
      '  from rtl_return_line_egais s,'
      '           egais_pref ep  '
      'where s.doc_id = :DOC_ID'
      'and s.line_id = :LINE_ID'
      'and ep.alccode(+) = s.alc_code')
    Macros = <>
    Left = 376
    Top = 632
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_alcDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Visible = False
    end
    object m_Q_alcLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Visible = False
    end
    object m_Q_alcSUB_LINE_ID: TFloatField
      FieldName = 'SUB_LINE_ID'
    end
    object m_Q_alcNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Visible = False
    end
    object m_Q_alcASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Visible = False
    end
    object m_Q_alcPDF_BAR_CODE: TStringField
      DisplayLabel = '��� �������� �����'
      FieldName = 'PDF_BAR_CODE'
      Size = 100
    end
    object m_Q_alcEGAIS_PRICE: TFloatField
      DisplayLabel = '���� �����'
      FieldName = 'EGAIS_PRICE'
    end
    object m_Q_alcVOLUME: TFloatField
      DisplayLabel = '�����'
      FieldName = 'VOLUME'
    end
    object m_Q_alcPRICE_MRZ: TFloatField
      DisplayLabel = '���'
      FieldName = 'PRICE_MRZ'
    end
    object m_Q_alcIS_FULL: TStringField
      FieldName = 'IS_FULL'
      FixedChar = True
      Size = 1
    end
    object m_Q_alcCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
    end
    object m_Q_alcREPORT_NUM: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'REPORT_NUM'
    end
    object m_Q_alcINN: TStringField
      DisplayLabel = '���'
      FieldName = 'INN'
    end
    object m_Q_alcKPP: TStringField
      DisplayLabel = '���'
      FieldName = 'KPP'
    end
    object m_Q_alcALC_CODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'ALC_CODE'
      Size = 30
    end
  end
  object m_Q_alco: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_alcoBeforeOpen
    AfterInsert = m_Q_alcoAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select s.doc_id,'
      '           s.line_id,'
      '           s.sub_line_id,'
      '           s.nmcl_id,'
      '           s.assort_id,'
      '           s.pdf_bar_code, '
      '           s.egais_price,'
      '           s.volume,'
      '           s.price_mrz,'
      '           s.is_full,'
      '           s.check_num, '
      '           s.report_num,'
      '           s.inn,'
      '           s.kpp,'
      '           s.alc_code'
      '  from rtl_return_line_egais s,'
      '           egais_pref ep  '
      'where s.doc_id = :DOC_ID'
      'and s.line_id = :LINE_ID'
      'and s.sub_line_id = :SUB_LINE_ID'
      'and ep.alccode(+) = s.alc_code')
    Macros = <>
    Left = 248
    Top = 632
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SUB_LINE_ID'
        ParamType = ptInput
      end>
    object m_Q_alcoDOC_ID: TFloatField
      FieldName = 'DOC_ID'
      Visible = False
    end
    object m_Q_alcoLINE_ID: TFloatField
      FieldName = 'LINE_ID'
      Visible = False
    end
    object m_Q_alcoSUB_LINE_ID: TFloatField
      FieldName = 'SUB_LINE_ID'
    end
    object m_Q_alcoNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Visible = False
    end
    object m_Q_alcoASSORT_ID: TFloatField
      FieldName = 'ASSORT_ID'
      Visible = False
    end
    object m_Q_alcoPDF_BAR_CODE: TStringField
      DisplayLabel = '��� �������� �����'
      FieldName = 'PDF_BAR_CODE'
      Size = 100
    end
    object m_Q_alcoEGAIS_PRICE: TFloatField
      DisplayLabel = '���� �����'
      FieldName = 'EGAIS_PRICE'
    end
    object m_Q_alcoVOLUME: TFloatField
      DisplayLabel = '�����'
      FieldName = 'VOLUME'
    end
    object m_Q_alcoPRICE_MRZ: TFloatField
      DisplayLabel = '���'
      FieldName = 'PRICE_MRZ'
    end
    object m_Q_alcoIS_FULL: TStringField
      FieldName = 'IS_FULL'
      FixedChar = True
      Size = 1
    end
    object m_Q_alcoCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
    end
    object m_Q_alcoREPORT_NUM: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'REPORT_NUM'
    end
    object m_Q_alcoINN: TStringField
      DisplayLabel = '���'
      FieldName = 'INN'
    end
    object m_Q_alcoKPP: TStringField
      DisplayLabel = '���'
      FieldName = 'KPP'
    end
    object m_Q_alcoALC_CODE: TStringField
      DisplayLabel = '�������'
      FieldName = 'ALC_CODE'
      Size = 30
    end
  end
  object m_U_alco: TMLUpdateSQL
    InsertSQL.Strings = (
      'insert into  rtl_return_line_egais (doc_id,'
      '           line_id,'
      '           sub_line_id,'
      '           nmcl_id,'
      '           assort_id,'
      '           pdf_bar_code, '
      '           egais_price,'
      '           volume,'
      '           is_full,'
      '           check_num, '
      '           report_num,'
      '           inn,'
      '           kpp,'
      '           alc_code)'
      'values ('
      '  :DOC_ID,'
      '  :LINE_ID,'
      
        '  1+ (select nvl(max(sub_line_id), 0) from rtl_return_line_egais' +
        ' where doc_id = :DOC_ID and line_id = :LINE_ID  ),'
      '  :NMCL_ID,'
      '  :ASSORT_ID,'
      '  :PDF_BAR_CODE,'
      '  :EGAIS_PRICE,'
      '  :VOLUME,'
      '  :IS_FULL,'
      '  :CHEK_NUM,'
      '  :REPORT_NUM,'
      '  :INN,'
      '  :KPP,'
      '  :ALC_CODE'
      ')')
    DeleteSQL.Strings = (
      
        'delete from rtl_return_line_egais where doc_id = :OLD_ID and lin' +
        'e_id = '
      ':OLD_LINE_ID and sub_line_id = :OLD_SUB_LINE_ID')
    IgnoreRowsAffected = True
    Left = 304
    Top = 632
  end
  object m_Q_pdf_bar_code: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_pdf_bar_codeBeforeOpen
    AfterInsert = m_Q_pdf_bar_codeAfterInsert
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'insert into  rtl_return_line_egais (doc_id,'
      '           line_id,'
      '           sub_line_id,'
      '           nmcl_id,'
      '           assort_id,'
      '           pdf_bar_code, '
      '           egais_price,'
      '           volume,'
      '           is_full,'
      '           check_num, '
      '           report_num,'
      '           inn,'
      '           kpp,'
      '           alc_code)'
      'select  '
      '  :DOC_ID,'
      '  :LINE_ID,'
      
        '  1+ (select nvl(max(sub_line_id), 0) from rtl_return_line_egais' +
        ' where doc_id = :DOC_ID and line_id = :LINE_ID  ),'
      '           nmcl_id,'
      '           assort_id,'
      '  :PDF_BAR_CODE,'
      '           egais_price,'
      '           volume,'
      '           is_full,'
      '           check_num, '
      '           report_num,'
      '           inn,'
      '           kpp,'
      ' pkg_bar_code.get_egais_alccode(:PDF_BAR_CODE)'
      'from rtl_sale_line_egais where pdf_bar_code = :PDF_BAR_CODE'
      
        '   and doc_id = (select max(doc_id)  from   rtl_sale_line_egais ' +
        'where pdf_bar_code = :PDF_BAR_CODE)'
      ''
      '')
    Macros = <>
    Left = 536
    Top = 632
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'LINE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end>
  end
  object m_get_PDF_BAR_CODE: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'SELECT max(pkg_bar_code.fnc_get_PDF_CODE(:PDF_BAR_CODE))   as PD' +
        'F_BAR_CODE_out'
      '        FROM dual')
    Macros = <>
    Left = 440
    Top = 647
    ParamData = <
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end>
    object m_get_PDF_BAR_CODEPDF_BAR_CODE_OUT: TMemoField
      FieldName = 'PDF_BAR_CODE_OUT'
      BlobType = ftMemo
      Size = 4000
    end
  end
  object m_chek_pdf: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT max(1)        as is_need_crt'
      '        FROM dual'
      '    where pkg_bar_code.get_egais_alccode(:PDF_BAR_CODE)'
      
        '    = (select max(pref_alccode) from egais_nmcl where nmcl_id = ' +
        ':NMCL_ID)'
      'or  pkg_bar_code.get_egais_alccode(:PDF_BAR_CODE)='
      
        '   (select max(pref_alccode) from egais_nmcl where nmcl_id in  (' +
        'select max (main_nmcl_id)  from nmcl_alcosets where nmcl_id= :NM' +
        'CL_ID) )')
    Macros = <>
    Left = 624
    Top = 632
    ParamData = <
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PDF_BAR_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NMCL_ID'
        ParamType = ptInput
      end>
    object m_chek_pdfIS_NEED_CRT: TFloatField
      FieldName = 'IS_NEED_CRT'
    end
  end
  object m_Q_find_check: TMLQuery
    BeforeOpen = m_Q_find_checkBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select  /*+ INDEX(C XIF_RTL_CHECKS_FR)   INDEX(D XPK_DOCUMENTS)*' +
        '/ '
      '    CHECK_NUM,'
      '    CHECK_DATE,'
      '    CHECK_SUM,'
      '    REPORT_DOC_ID AS ID,'
      '    CASH'
      'FROM rtl_checks c, documents d,'
      '     folders f'
      'where fn_number = :fn_number'
      '  and fn_doc_id = :fn_doc_id'
      '  and c.report_doc_id = d.id'
      '  and d.src_org_id = :HOME_ORG_ID'
      '  and f.id = d.fldr_id '
      '  and f.doc_type_id = 188942')
    Macros = <>
    Left = 224
    Top = 590
    ParamData = <
      item
        DataType = ftString
        Name = 'fn_number'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'fn_doc_id'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'HOME_ORG_ID'
        ParamType = ptInput
      end>
    object m_Q_find_checkCHECK_NUM: TFloatField
      FieldName = 'CHECK_NUM'
    end
    object m_Q_find_checkCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object m_Q_find_checkCHECK_SUM: TFloatField
      FieldName = 'CHECK_SUM'
    end
    object m_Q_find_checkID: TFloatField
      FieldName = 'ID'
    end
    object m_Q_find_checkCASH: TStringField
      FieldName = 'CASH'
      FixedChar = True
      Size = 1
    end
  end
end
