//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmTTNOutItem.h"
#include "RtlCardOut.h"
#include "FmRtlCardOut.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBGridEh"
#pragma link "MLActionsControls"
#pragma link "MLDBGrid"
#pragma link "TSFMDOCUMENTWITHLINES"
#pragma link "MLDBPanel"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void __fastcall TFTTNOutItem::m_ACT_editUpdate(TObject *Sender)
{
  m_ACT_edit->Enabled = !m_dm->m_Q_linesID->IsNull &&
                        (m_dm->UpdateRegimeItem == urInsert ||
                         m_dm->UpdateRegimeItem == urEdit && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->FldrNew);
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutItem::m_ACT_appendExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();

  TCardOut card_out = ExecRtlCardOutDlg(this,m_dm->m_DB_main->Handle,NULL,NULL,-1,-1);

  if (card_out.CardId == -1) return;

  m_dm->m_Q_line->ParamByName("ID")->AsInteger = -1;
  m_dm->m_Q_line->ParamByName("LINE_ID")->AsInteger = -1;
  m_dm->m_Q_line->Open();
  //������
  m_dm->m_Q_line->First();
  while(!m_dm->m_Q_line->Eof) {
    m_dm->m_Q_line->Delete();
    m_dm->m_Q_line->Next();
  }
  m_dm->m_Q_line->Insert();
  int line_id = m_dm->m_Q_lineLINE_ID->AsInteger;
  m_dm->m_Q_lineCARD_ID->AsInteger = card_out.CardId;
  m_dm->m_Q_lineNMCL_ID->AsInteger = card_out.NmclId;
  m_dm->m_Q_lineASSORTMENT_ID->AsInteger = card_out.AssortId;
  m_dm->m_Q_lineITEMS_QNT->AsFloat = card_out.ItemsQnt;
  m_dm->m_Q_lineOUT_PRICE->AsFloat = card_out.OutPrice;
  m_dm->m_Q_lineNOTE->AsString = card_out.Note;
  m_dm->m_Q_get_in_price->Close();
  m_dm->m_Q_get_in_price->Open();
  m_dm->m_Q_lineIN_PRICE->AsFloat = m_dm->m_Q_get_in_priceVAL->AsFloat;
  m_dm->m_Q_line->Post();
  MLApplyUpdatesBDEDataSets(m_dm->m_DB_main,m_dm->LineDataSets);
  m_dm->m_Q_line->Close();

  m_dm->RefreshItemDataSets();
  m_dm->m_Q_lines->Locate("LINE_ID",line_id,TLocateOptions());
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutItem::m_ACT_appendUpdate(TObject *Sender)
{
  m_ACT_append->Enabled =(m_dm->UpdateRegimeItem == urInsert ||
                          m_dm->UpdateRegimeItem == urEdit && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->FldrNew);
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutItem::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = !m_dm->m_Q_linesID->IsNull &&
                          (m_dm->UpdateRegimeItem == urInsert ||
                           m_dm->UpdateRegimeItem == urEdit && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->FldrNew);
}
//---------------------------------------------------------------------------

AnsiString __fastcall TFTTNOutItem::BeforeItemApply(TWinControl **p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete) return "";

  if (m_dm->m_Q_itemDEST_ORG_ID->IsNull)
  {
    *p_focused = m_LLV_dest_org;
    return "�������� �����!";
  }

  return "";
}
//---------------------------------------------------------------------------


void __fastcall TFTTNOutItem::m_BCS_itemBegin(TObject *Sender)
{
  m_temp_grid_options = m_DBG_lines->OptionsML;
  m_DBG_lines->OptionsML = TMLGridOptions() << goWithoutFind;        
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutItem::m_BCS_itemEnd(TObject *Sender)
{
try
  {
    if (m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();
    m_dm->m_Q_barcode_info->Close();
    m_dm->m_Q_barcode_info->ParamByName("BARCODE")->AsString = m_BCS_item->GetResult();
    m_dm->m_Q_barcode_info->Open();
    if ( m_dm->m_Q_barcode_infoCARD_ID->IsNull || !m_dm->m_Q_barcode_infoERROR->IsNull )
    {
      //ShowMessage("����� �� ������ � ��������� ������! ");
      AnsiString msg = m_dm->m_Q_barcode_infoERROR->AsString;
      if (msg.IsEmpty()) msg = "����� �� ������ � ��������� ������!";
      ShowMessage(msg);
    }
    else
    {
      //�������/������� ������
      TCardOut card_out;
      card_out.CardId = m_dm->m_Q_barcode_infoCARD_ID->AsInteger;
      card_out.NmclId = m_dm->m_Q_barcode_infoNMCL_ID->AsInteger;
      card_out.AssortId = m_dm->m_Q_barcode_infoASSORT_ID->AsInteger;
      card_out.InPrice = m_dm->m_Q_barcode_infoIN_PRICE->AsFloat;
      card_out.OutPrice = m_dm->m_Q_barcode_infoOUT_PRICE->AsFloat;
      card_out.ItemsQnt = m_dm->m_Q_barcode_infoITEMS_QNT->AsFloat;

      card_out.ReasonId = -1;
      card_out.LotId = -1;
      card_out.Note =  "";
      if( card_out.NmclId > 0 )
      {
          //m_dm->InsertNewCard(card_out);
          m_dm->UpdateCard(card_out);
      }
      m_dm->RefreshItemDataSets();
      m_dm->m_Q_lines->Locate(m_dm->m_Q_linesNMCL_ID->FieldName,card_out.NmclId, TLocateOptions());
      if (m_dm->m_Q_barcode_infoITEMS_QNT->AsFloat == 0 )
        m_ACT_edit->Execute();
    }

    m_dm->m_Q_barcode_info->Close();
    //m_ACT_append->Execute();
  }
  __finally
  {
    m_DBG_lines->OptionsML = m_temp_grid_options;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFTTNOutItem::FormShow(TObject *Sender)
{
    TTSFItem::FormShow(Sender);
    m_BCS_item->Active = (m_dm->UpdateRegimeItem != urView);
}
//---------------------------------------------------------------------------
