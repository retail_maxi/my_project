//---------------------------------------------------------------------------

#ifndef DmDocDiscGoodsH
#define DmDocDiscGoodsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDDocDiscGoods : public TTSDDocumentWithLines
{
__published:	// IDE-managed Components
  TStringField *m_Q_listDOC_NUMBER;
  TDateTimeField *m_Q_listDOC_DATE;
  TStringField *m_Q_listNOTE;
  TStringField *m_Q_linesNOTE;
  TFloatField *m_Q_listNMCL_ID;
  TStringField *m_Q_listNMCL_NAME;
  TFloatField *m_Q_listASSORTMENT_ID;
  TStringField *m_Q_listASSORTMENT_NAME;
  TFloatField *m_Q_listACTION_TYPE_ID;
  TStringField *m_Q_listACTION_TYPE_NAME;
  TDateTimeField *m_Q_listBEGIN_DATE;
  TDateTimeField *m_Q_listEND_DATE;
  TMLQuery *m_Q_nmcls;
  TFloatField *m_Q_nmclsID;
  TStringField *m_Q_nmclsNAME;
  TMLQuery *m_Q_acts;
  TFloatField *m_Q_actsID;
  TStringField *m_Q_actsNAME;
  TStringField *m_Q_actsNOTE;
  TMLQuery *m_Q_assorts;
  TFloatField *m_Q_assortsID;
  TStringField *m_Q_assortsNAME;
  TMLQuery *m_Q_orgs;
  TFloatField *m_Q_orgsID;
  TStringField *m_Q_orgsNAME;
  TStringField *m_Q_linesORG_NAME;
  TStringField *m_Q_lineORG_NAME;
  TStringField *m_Q_lineNOTE;
  TQuery *m_Q_org_groups;
  TFloatField *m_Q_org_groupsID;
  TStringField *m_Q_org_groupsNAME;
  TFloatField *m_Q_lineGRP_ID;
  TStringField *m_Q_linesGRP_NAME;
  TMLQuery *m_Q_is_assorted;
  TFloatField *m_Q_is_assortedVAL;
   TFloatField *m_Q_listACT_PRICE;
        TMLQuery *m_Q_sel_orgs;
        TFloatField *m_Q_sel_orgsID;
        TStringField *m_Q_sel_orgsNAME;
        TFloatField *m_Q_sel_orgsCHECKED;
        TMLUpdateSQL *m_U_sel_orgs;
        TMLQuery *m_Q_ins_orgs;
        TMLQuery *m_Q_sel_org;
        TFloatField *m_Q_sel_orgID;
        TStringField *m_Q_sel_orgNAME;
        TFloatField *m_Q_sel_orgIM_INDEX;
        TFloatField *m_Q_sel_orgCHECKED;
        TFloatField *m_Q_sel_orgMASTER_ID;
        TMLUpdateSQL *m_U_sel_org;
        TMLQuery *m_Q_folders_next;
        TFloatField *m_Q_folders_nextSRC_FLDR_ID;
        TFloatField *m_Q_folders_nextID;
        TFloatField *m_Q_folders_nextDEST_FLDR_ID;
        TStringField *m_Q_folders_nextNAME;
        TStringField *m_Q_folders_nextROUTE_NEXT_RIGHT;
        TMLQuery *m_Q_folders_prev;
        TFloatField *m_Q_folders_prevSRC_FLDR_ID;
        TFloatField *m_Q_folders_prevID;
        TFloatField *m_Q_folders_prevDEST_FLDR_ID;
        TStringField *m_Q_folders_prevNAME;
        TStringField *m_Q_folders_prevROUTE_NEXT_RIGHT;
        TFloatField *m_Q_linesITEMS_WEEK_1;
        TFloatField *m_Q_linesITEMS_WEEK_2;
        TFloatField *m_Q_linesITEMS_WEEK_3;
        TFloatField *m_Q_linesOUT_PRICE;
        TFloatField *m_Q_linesOUT_PRICE_ND;
        TFloatField *m_Q_linesDAY_STORE;
        TMLQuery *m_Q_sub_line;
        TFloatField *m_Q_sub_lineNMCL_ID;
        TStringField *m_Q_sub_lineNMCL_NAME;
        TFloatField *m_Q_sub_lineASSORT_ID;
        TStringField *m_Q_sub_lineASSORT_NAME;
        TFloatField *m_Q_sub_lineDOC_ID;
  TQuery *m_Q_wrk_par;
  TFloatField *m_Q_wrk_parFLDR;
  TFloatField *m_Q_wrk_parROUTE;
        TStringField *m_Q_listGROUP_NAME;
        TStringField *m_Q_listCAT_MEN_NAME;
        TMLQuery *m_Q_del_rtt;
        TMLQuery *m_Q_ins_all_rtt;
        TMLQuery *m_Q_upd_rtt;
        TMLQuery *m_Q_orgs_list;
        TFloatField *m_Q_orgs_listORG_ID;
        TStringField *m_Q_orgs_listORG_NAME;
        TStringField *m_Q_itemDOC_NUMBER;
        TDateTimeField *m_Q_itemDOC_DATE;
        TDateTimeField *m_Q_itemBEGIN_DATE;
        TDateTimeField *m_Q_itemEND_DATE;
        TFloatField *m_Q_itemACTION_TYPE_ID;
        TStringField *m_Q_itemACTION_TYPE_NAME;
        TFloatField *m_Q_itemNMCL_ID;
        TStringField *m_Q_itemNMCL_NAME;
        TFloatField *m_Q_itemASSORTMENT_ID;
        TStringField *m_Q_itemASSORTMENT_NAME;
        TStringField *m_Q_itemREASON_NAME;
        TStringField *m_Q_itemNOTE;
        TFloatField *m_Q_itemACT_PRICE;
        TFloatField *m_Q_itemPRICE_BEF_ACT;
        TDateTimeField *m_Q_itemPULL_DATE;
        TStringField *m_Q_itemCAT_MEN_NAME;
        TMLQuery *m_Q_reasons;
        TFloatField *m_Q_reasonsID;
        TStringField *m_Q_reasonsNAME;
        TStringField *m_Q_reasonsNOTE;
        TFloatField *m_Q_itemURGENT_REPRICE;
        TStringField *m_Q_listREASON_ID;
        TStringField *m_Q_listREASON_NAME;
        TFloatField *m_Q_listPRICE_BEF_ACT;
        TFloatField *m_Q_linesFACT_MARGIN;
        TFloatField *m_Q_linesSTORE_ITEMS;
        TFloatField *m_Q_linesCUR_PRICE_IN;
        TDateTimeField *m_Q_linesINC_DATE_RC;
        TDateTimeField *m_Q_linesINC_DATE_RTT;
        TStringField *m_Q_linesACTION_TYPE_NAME;
        TDateTimeField *m_Q_linesACTION_BEGIN_DATE;
        TDateTimeField *m_Q_linesACTION_END_DATE;
        TFloatField *m_Q_linesACTION_OUT_PRICE;
        TDateTimeField *m_Q_linesPULL_DATE;
        TFloatField *m_Q_linesFACT_MARGIN_SUM;
        TFloatField *m_Q_linesMARGIN_AF_ACT;
        TFloatField *m_Q_linesTOTAL_PROFIT;
        TMLQuery *m_Q_route;
        TFloatField *m_Q_routeROUTE_ID;
        TMLQuery *m_Q_reason_rejection;
        TMLUpdateSQL *m_U_reason_rejection;
        TStringField *m_Q_itemREASON_REJECTION;
        TStringField *m_Q_listREASON_REJECTION;
        TStringField *m_Q_itemREASON_ID;
        TStringField *m_Q_reason_rejectionNOTE;
        TMLQuery *m_Q_edit_reason_rejection;
        TStringField *m1;
        TFloatField *m_Q_linesMODEL_TYPE_ID;
        TStringField *m_Q_linesMODEL_TYPE;
        TMLQuery *m_Q_chk_docs;
        TFloatField *m_Q_chk_docsC;
        TMLQuery *m_Q_fikt_upd;
        TMLUpdateSQL *m_U_fikt_upd;
        TDateTimeField *m_Q_fikt_updBEGIN_DATE;
        TDateTimeField *m_Q_fikt_updEND_DATE;
        TFloatField *m_Q_fikt_updACT_PRICE;
        TFloatField *m_Q_fikt_updPRICE_BEF_ACT;
        TFloatField *m_Q_fikt_updURGENT_REPRICE;
        TMLQuery *m_Q_chk_sel_docs;
        TFloatField *m_Q_chk_sel_docsC;
        TMLQuery *m_Q_chg_fldr;
        TFloatField *m2;
        TStoredProc *m_SP_copy_doc;
        TFloatField *m_Q_itemAGENT_ID;
        TFloatField *m_Q_listIS_RETURNED;
        TFloatField *m_Q_listAGENT_ID;
        TFloatField *m_Q_itemSUPP_COMP;
        TFloatField *m_Q_listURGENT_REPRICE;
        TFloatField *m_Q_listSUPP_COMP;
        TFloatField *m_Q_listREST_RC;
        TFloatField *m_Q_orgs_listSTORE_ITEMS;
        TFloatField *m_Q_orgs_listDAY_STORE;
        TStringField *m_Q_orgs_listMODEL_TYPE;
        TFloatField *m_Q_orgs_listOUT_PRICE;
        TFloatField *m_Q_orgs_listOUT_PRICE_ND;
        TStringField *m_Q_orgs_listACTION_TYPE_NAME;
        TDateTimeField *m_Q_orgs_listACTION_BEGIN_DATE;
        TDateTimeField *m_Q_orgs_listACTION_END_DATE;
        TFloatField *m_Q_orgs_listACTION_OUT_PRICE;
        TFloatField *m_Q_orgs_listMARGIN_AF_ACT;
        TFloatField *m_Q_orgs_listITEMS_WEEK_1;
        TFloatField *m_Q_orgs_listITEMS_WEEK_2;
        TFloatField *m_Q_orgs_listITEMS_WEEK_3;
        TFloatField *m_Q_orgs_listLAST_INC_PRICE_RTT;
        TFloatField *m_Q_orgs_listTOTAL_PROFIT;
        TMLQuery *m_Q_reject_zam_kd;
        TMLUpdateSQL *m_U_reject_zam_kd;
        TMLQuery *m_Q_edit_reject_zam_ld;
        TStringField *m4;
        TStringField *m_Q_listREJECT_ZAM_KD;
        TStringField *m_Q_reject_zam_kdNOTE;
        TFloatField *m_Q_orgs_listPRICE_NO_ACT;
        TDataSource *m_DS_list;
        TFloatField *m_Q_listCAT_MEN_ID;
        TFloatField *m_Q_listGROUP_ID;
        TFloatField *m_Q_linesACT_PRICE;
        TFloatField *m_Q_linesMIN_MARGIN_AF_ACT;
        TFloatField *m_Q_linesMAX_TOTAL_PROFIT;
        TMLQuery *m_Q_get_out_price;
        TFloatField *m_Q_get_out_priceOUT_PRICE_ND;
        TMLQuery *m_Q_del_docs;
        TFloatField *m_Q_del_docsDOC_ID;
        TMLQuery *m_Q_folders_next_all;
        TFloatField *m_Q_folders_next_allSRC_FLDR_ID;
        TFloatField *m_Q_folders_next_allID;
        TFloatField *m_Q_folders_next_allDEST_FLDR_ID;
        TStringField *m_Q_folders_next_allNAME;
        TStringField *m_Q_folders_next_allROUTE_NEXT_RIGHT;
        TFloatField *m_Q_folders_next_allENABLED;
        TMLQuery *m_Q_folders_prev_all;
        TFloatField *m_Q_folders_prev_allSRC_FLDR_ID;
        TFloatField *m_Q_folders_prev_allID;
        TFloatField *m_Q_folders_prev_allDEST_FLDR_ID;
        TStringField *m_Q_folders_prev_allNAME;
        TStringField *m_Q_folders_prev_allROUTE_NEXT_RIGHT;
        TFloatField *m_Q_folders_prev_allENABLED;
        TMLQuery *m_Q_docs;
        TFloatField *m_Q_docsDOC_ID;
        TFloatField *m_Q_docsFLDR_ID;
        TFloatField *m_Q_docsNMCL_ID;
        TFloatField *m_Q_docsASSORTMENT_ID;
        TStringField *m_Q_docsREASON_ID;
        TFloatField *m_Q_docsACT_PRICE;
        TFloatField *m_Q_docsPRICE_BEF_ACT;
        TDateTimeField *m_Q_docsBEGIN_DATE;
        TDateTimeField *m_Q_docsEND_DATE;
        TFloatField *m_Q_docsACTION_TYPE_ID;
        TFloatField *m_Q_docsCOUNT_ORG;
        TFloatField *m_Q_docsORG_ID_3;
        TFloatField *m_Q_docsMIN_MARGIN_AF_ACT;
        TFloatField *m_Q_docsMAX_TOTAL_PROFIT;
        TMLQuery *m_Q_chk_nmcl;
        TFloatField *m_Q_chk_nmclC;
        TFloatField *m_Q_orgs_listCUR_PRICE_IN;
        TFloatField *m_Q_orgs_listACT_PRICE;
        TMLQuery *m_Q_cnt;
        TFloatField *m_Q_cntCNT_ID;
        TStringField *m_Q_cntCNT_NAME;
        TFloatField *m_Q_itemCNT_ID;
        TFloatField *m_Q_itemPRICE_BEF_ACT_ON_CARD;
        TFloatField *m_Q_itemCNT_SALE;
        TFloatField *m_Q_itemCNT_PRICE_COMP;
        TStringField *m_Q_itemCNT_NAME;
        TStringField *m_Q_listCNT_NAME;
        TFloatField *m_Q_listCNT_SALE;
        TFloatField *m_Q_listCNT_PRICE_COMP;
        TFloatField *m_Q_get_out_priceOUT_PRICE;
        TFloatField *m_Q_docsPRICE_BEF_ACT_ON_CARD;
        TFloatField *m_Q_docsGM;
        TStringField *m_Q_listACTION_ORG_VOL;
        TStringField *m_Q_listACTION_ORG_CHER;
        TStringField *m_Q_listACTION_ORG_ARCH;
        TFloatField *m_Q_listIS_CURRENT_ACTION;
        TFloatField *m_Q_linesIS_CURRENT_ACTION_ORG;
        TFloatField *m_Q_linesGM;
        TFloatField *m_Q_linesSM_MINI;
        TFloatField *m_Q_docsSM_MINI;
        TFloatField *m_Q_linesCAPACITY;
        TStringField *m_Q_itemCNT_COMP_SALE;
        TStringField *m_Q_itemCNT_METHOD_COMP;
        TStringField *m_Q_listNAME_USER;
        TStringField *m_Q_chk_docsNOTE;
        TFloatField *m_Q_linesMAX_LINE_ID;
        TStringField *m_Q_listCNT_COMP_SALE;
        TStringField *m_Q_listCNT_METHOD_COMP;
        TStringField *m_Q_listMOD_USER_NAME;
  void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_assortsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_lineAfterInsert(TDataSet *DataSet);
  void __fastcall m_Q_orgsBeforeOpen(TDataSet *DataSet);
  void __fastcall m_Q_is_assortedBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_sub_lineBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_orgs_listBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_sel_orgsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_chk_docsBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_get_out_priceBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_folders_next_allBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_folders_prev_allBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_chk_nmclBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_cntBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_reasonsBeforeOpen(TDataSet *DataSet);
protected:
  TS_DOC_LINE_SQ_NAME("sq_doc_disc_goods_items")
private:
  TDateTime __fastcall GetBeginDate();
  TDateTime __fastcall GetEndDate();
  ETSUpdateRegime m_update_regime_sub_line;
  bool m_can_grp_chg;
  int m_sq_fldr, m_fldr_new, m_fldr_sogl, m_fldr_utv, m_fldr_in_act, m_fldr_arc, m_fldr_cansel;
public:
 
  __property ETSUpdateRegime UpdateRegimeSubLine = {read=m_update_regime_sub_line};
  void __fastcall SetBeginEndDate(const TDateTime &p_begin_date,
                                  const TDateTime &p_end_date);

  __property TDateTime BeginDate = {read=GetBeginDate};
  __property TDateTime EndDate = {read=GetEndDate};
   __property int SqFldr = {read=m_sq_fldr, write=m_sq_fldr};
  __property int FldrNew = {read = m_fldr_new};
  __property int FldrSogl = {read = m_fldr_sogl};
   __property int FldrUtv = {read = m_fldr_utv};
  __property int FldrAct = {read = m_fldr_in_act};
   __property int FldrArc = {read = m_fldr_arc};
  __property int FldrCansel = {read = m_fldr_cansel};
  __property bool CanGrpChg = {read = m_can_grp_chg};
  __fastcall TDDocDiscGoods(TComponent* p_owner,
                           AnsiString p_prog_id);
  void __fastcall ApplyUpdatesItem();
ML_BEGIN_DATA_SETS
  ML_BASE_DATA_SETS(TTSDDocumentWithLines)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_org_groups)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_fikt_upd)
  ML_DATA_SET_ENTRY((*ItemDataSets),m_Q_sel_org)
ML_END_DATA_SETS
};
//---------------------------------------------------------------------------
#endif
