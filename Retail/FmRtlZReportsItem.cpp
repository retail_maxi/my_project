//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlZReportsItem.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFRtlZReportsItem::TFRtlZReportsItem(TComponent *p_owner,
                                                TTSDDocument *p_dm_document): TTSFDocument(p_owner,
                                                                                           p_dm_document),
                                                                              m_dm(static_cast<TDRtlZReports*>(DMDocument))
{

}
//---------------------------------------------------------------------------
void __fastcall TFRtlZReportsItem::FormShow(TObject *Sender)
{
  TTSFDocument::FormShow(Sender);
  if (m_dm->m_Q_oper->Active) m_dm->m_Q_oper->Close();
  m_dm->m_Q_oper->Open();
  if (m_dm->m_Q_sprm->Active) m_dm->m_Q_sprm->Close();
  m_dm->m_Q_sprm->Open();
}
//---------------------------------------------------------------------------



