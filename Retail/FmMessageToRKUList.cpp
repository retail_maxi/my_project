//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "FmMessageToRKUList.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFMessageToRKUList::TFMessageToRKUList(TComponent* p_owner,
                                                  TTSDDocument *p_dm_document)
  : TTSFDocumentList(p_owner, p_dm_document),
  m_dm(static_cast<TDMessageToRKU*>(DMDocument))
{
}
//---------------------------------------------------------------------------
 