//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmSetCnt.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "dbtree_l"
#pragma link "MLUpdateSQL"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

void SetCntDlg(TComponent* p_owner, int p_doc_id, TTSDCustom* p_dm, bool p_edit)
{
  TFSetCnt *form = new TFSetCnt(p_owner, p_dm, p_edit);
  try
  {
    form->AfterCreate();

    try
    {
      
      form->m_Q_cnt->ParamByName("DOC_ID")->AsInteger = p_doc_id;
      form->m_Q_cnt->Open();
      form->m_Q_curr_dep->ParamByName("DEP_ID")->AsInteger = -1;
      form->m_Q_curr_dep->Open();
      form->m_Q_department->Open();
      form->m_Q_all_cnt->ParamByName("DOC_ID")->AsInteger = p_doc_id;
      form->m_Q_all_cnt->Open();
      form->ShowModal() == IDOK;
    }
    __finally
    {
      form->m_Q_all_cnt->Close();
      form->m_Q_cnt->Close();
      form->m_Q_curr_dep->Close();
      form->m_Q_department->Close();
    }
  }
  __finally
  {
    form->BeforeDestroy();
    delete form;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSetCnt::m_ACT_addExecute(TObject *Sender)
{

    if( Application->MessageBox(AnsiString("��������� �����������?").c_str(),
                                Application->Title.c_str(),
                                MB_ICONQUESTION | MB_YESNO) == IDYES )
    { 
      m_Q_add_cnt->ParamByName("DOC_ID")->AsInteger = m_Q_cnt->ParamByName("DOC_ID")->AsInteger;
      m_Q_add_cnt->ParamByName("SQ_ID")->AsInteger = DMCustom->FillTmpReportParamValues(m_DBG_all_cnt, "CNT_ID");
      m_Q_add_cnt->ExecSQL();

      m_Q_all_cnt->Close();
      m_Q_cnt->Close();
      m_Q_cnt->Open();
      m_Q_all_cnt->Open();
    }


}
//---------------------------------------------------------------------------

void __fastcall TFSetCnt::m_ACT_addUpdate(TObject *Sender)
{

    m_ACT_add->Enabled = m_Q_all_cntCNT_ID->AsInteger > 0 && is_edit ;

}
//---------------------------------------------------------------------------

void __fastcall TFSetCnt::m_ACT_deleteExecute(TObject *Sender)
{
  if( Application->MessageBox(AnsiString("�������� �����������?").c_str(),
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES )
  {

    m_Q_delete_cnt->ParamByName("DOC_ID")->AsInteger = m_Q_cnt->ParamByName("DOC_ID")->AsInteger;
    m_Q_delete_cnt->ParamByName("SQ_ID")->AsInteger = DMCustom->FillTmpReportParamValues(m_DBG_cnt, "CNT_ID");
    m_Q_delete_cnt->ExecSQL();

    m_Q_all_cnt->Close();
    m_Q_cnt->Close();
    m_Q_cnt->Open();
    m_Q_all_cnt->Open();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSetCnt::m_ACT_deleteUpdate(TObject *Sender)
{
  m_ACT_delete->Enabled = m_Q_cntCNT_ID->AsInteger > 0 && is_edit ;
}
//---------------------------------------------------------------------------




void __fastcall TFSetCnt::m_Q_all_cntBeforeOpen(TDataSet *DataSet)
{
   m_Q_all_cnt->ParamByName("DEP_ID")->AsInteger = m_Q_curr_depDEP_ID->AsInteger;
}
//---------------------------------------------------------------------------


void __fastcall TFSetCnt::m_Q_curr_depDEP_IDChange(TField *Sender)
{
    if (m_Q_all_cnt->Active) m_Q_all_cnt->Close();
    m_Q_all_cnt->Open();
}
//---------------------------------------------------------------------------




void __fastcall TFSetCnt::FormShow(TObject *Sender)
{
  m_DBLUB_dep->Enabled = is_edit;        
}
//---------------------------------------------------------------------------


void __fastcall TFSetCnt::m_Q_cntIS_MAINChange(TField *Sender)
{
 try
 {
  m_Q_upd_cnt->ParamByName("DOC_ID")->AsInteger = m_Q_cntDOC_ID->AsInteger;
  m_Q_upd_cnt->ParamByName("CNT_ID")->AsInteger = m_Q_cntCNT_ID->AsInteger;
  m_Q_upd_cnt->ParamByName("IS_MAIN")->AsInteger = m_Q_cntIS_MAIN->AsInteger;
  m_Q_upd_cnt->ExecSQL();
  }
  __finally
  {
    m_Q_cnt->Close();
    m_Q_cnt->Open();
  }        
}
//---------------------------------------------------------------------------

