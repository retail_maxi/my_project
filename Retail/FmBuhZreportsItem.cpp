//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmBuhZreportsItem.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "RXCtrls"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFBuhZreportsItem::TFBuhZreportsItem(TComponent *p_owner,
                                                TTSDDocument *p_dm_document): TTSFDocument(p_owner,
                                                                                           p_dm_document),
                                                                              m_dm(static_cast<TDBuhZreports*>(DMDocument))
{

}
//---------------------------------------------------------------------------



void __fastcall TFBuhZreportsItem::m_ACT_show_groundExecute(
      TObject *Sender)
{
   TTSDocumentControl *m_doc = new  TTSDocumentControl("TSRetail.RtlZReports");
   try
   {
     if( m_doc->CanExecuteView ) m_doc->ExecuteView(m_dm->m_Q_itemGROUND_DOC_ID->AsInteger);
   }
   __finally
   {
     if( m_doc ) delete m_doc;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFBuhZreportsItem::FormShow(TObject *Sender)
{
  TTSFDocument::FormShow(Sender);

    m_ACT_next_fldr->Visible = false;

    if( !m_ACT_prev_fldr->Visible &&
        m_ACT_next_fldr->Visible ) m_SBTN_next_fldr->Left = 0;
    else if( m_ACT_prev_fldr->Visible &&
        m_ACT_next_fldr->Visible ) m_SBTN_prev_fldr->Left = 0;

    m_TB_main_control_buttons_document->AutoSize = false;
    m_TB_main_control_buttons_document->AutoSize = true;

    //���������� ������ �������� ����� �� ����������
    if (m_dm->m_Q_folders_next->Active) m_dm->m_Q_folders_next->Close();
    m_dm->m_Q_folders_next->ParamByName("ID")->AsInteger = m_dm->m_Q_itemID->AsInteger;
    m_PM_fldr_next->Items->Clear();
    m_dm->m_Q_folders_next->Open();
    if (!m_dm->m_Q_folders_next->Eof)
      m_RSB_fldr_next->Caption = m_dm->m_Q_folders_nextNAME->AsString;
    while (!m_dm->m_Q_folders_next->Eof)
    {
      TMenuItem *NewItem = new TMenuItem(m_PM_fldr_next);
      NewItem->Caption = m_dm->m_Q_folders_nextNAME->AsString;
      NewItem->Tag = m_dm->m_Q_folders_nextID->AsInteger;
      NewItem->ImageIndex = 16;
      NewItem->OnClick = PMFldrNextClick;
      m_PM_fldr_next->Items->Add(NewItem);
      m_dm->m_Q_folders_next->Next();
    }

    m_RSB_fldr_next->Visible = m_dm->m_Q_itemDOC_STATUS->AsString != 'D' &&
                               m_dm->m_Q_folders_next->RecordCount;

    m_TB_fldr_next->Left =0;
    m_TB_main_control_buttons_document->Left =0;
    m_TB_fldr_next->AutoSize = false;
    m_TB_fldr_next->AutoSize = true;

    m_dm->m_Q_folders_next->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFBuhZreportsItem::PMFldrNextClick(TObject *Sender)
{
  if( m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();

  //������� �� �������� �����
  TMenuItem *ClickedItem = dynamic_cast<TMenuItem*>(Sender);
  if (ClickedItem)
  {
    //�������� ����� �� �������, ��������� �� ��������� ���������
    m_dm->m_Q_folder->Close();
    m_dm->m_Q_folder->Open();

    long item_id = m_dm->m_Q_itemID->AsInteger;
    long fldr_id = m_dm->m_Q_folderFLDR_ID->AsInteger;
    AnsiString fldr_name = m_dm->m_Q_folderFLDR_NAME->AsString;
    AnsiString fldr_right = m_dm->m_Q_folderFLDR_RIGHT->AsString;
    long route_id = ClickedItem->Tag;
    AnsiString route_name = ClickedItem->Caption;
    AnsiString route_right = m_dm->m_Q_folderROUTE_NEXT_RIGHT->AsString;
    long dest_fldr_id = m_dm->m_Q_folderFLDR_NEXT_ID->AsInteger;
    AnsiString dest_fldr_name = m_dm->m_Q_folderFLDR_NEXT_NAME->AsString;
    AnsiString dest_fldr_right = m_dm->m_Q_folderFLDR_NEXT_RIGHT->AsString;
    AnsiString params = "";

    BeforeMoveRoute(
      item_id,fldr_id,fldr_name,fldr_right,route_id,route_name,route_right,
      dest_fldr_id,dest_fldr_name,dest_fldr_right,&params
    );

    Close();

    m_dm->ChangeFldr(ClickedItem->Tag,item_id,params);

    AfterMoveRoute(
      item_id,fldr_id,fldr_name,fldr_right,route_id,route_name,route_right,
      dest_fldr_id,dest_fldr_name,dest_fldr_right
    );
  }
}
//---------------------------------------------------------------------------

