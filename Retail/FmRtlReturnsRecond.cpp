//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlReturnsRecond.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFMSUBITEM"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFRtlReturnsRecond::TFRtlReturnsRecond(TComponent* p_owner,
                                            TDRtlReturns *p_dm_doc_with_ln,
                                            ETSUpdateRegime p_regime):
                                TTSFSubItem(p_owner,p_dm_doc_with_ln),
                                            m_dm(p_dm_doc_with_ln),
                                            m_regime(p_regime)
{
  m_dm->ReDefineDataSources(this);
  m_start_win_control = ActiveControl;
  if(m_dm->m_Q_item->State == dsInsert && !m_dm->EditCond)
  {
     m_CB_cash_error->ItemIndex = 0;
     m_CB_decision_return->ItemIndex = 1;
     m_CB_is_check->ItemIndex = 0;
     m_cash_error = "Y";
     m_decision_return = "N";
     m_is_check = "Y";
  }

  if(m_dm->m_Q_item->State == dsEdit || m_dm->EditCond)
  {
    m_cash_error = m_dm->m_Q_itemCASHIER_ERROR->AsString;
    m_decision_return = m_dm->m_Q_itemDECISION_RETURN->AsString;
    m_is_check = m_dm->m_Q_itemIS_CHECK->AsString;
    
    if(m_dm->m_Q_itemCASHIER_ERROR->AsString == 'Y')
    {
       m_CB_cash_error->ItemIndex=0;
    }

    if(m_dm->m_Q_itemCASHIER_ERROR->AsString == 'N')
    {
       m_CB_cash_error->ItemIndex=1;
    }

    if(m_dm->m_Q_itemDECISION_RETURN->AsString == 'Y')
    {
       m_CB_decision_return->ItemIndex=0;
    }

    if(m_dm->m_Q_itemDECISION_RETURN->AsString == 'N')
    {
       m_CB_decision_return->ItemIndex=1;
    }

    if(m_dm->m_Q_itemIS_CHECK->AsString == 'Y')
    {
       m_CB_is_check->ItemIndex=0;
    }

    if(m_dm->m_Q_itemIS_CHECK->AsString == 'N')
    {
       m_CB_is_check->ItemIndex=1;
    }
  }

}
//---------------------------------------------------------------------------


void __fastcall TFRtlReturnsRecond::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  BeforeApply(Sender);
  ModalResult = mrOk;
  m_dm->m_Q_itemCASHIER_ERROR->AsString = m_cash_error;
  m_dm->m_Q_itemDECISION_RETURN->AsString = m_decision_return;
  m_dm->m_Q_itemIS_CHECK->AsString = m_is_check;
  m_dm->ApplyUpdatesItem();
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsRecond::BeforeApply(TObject *Sender)
{
  if ( m_CB_cash_error->Text.IsEmpty() || m_CB_cash_error->Text == " " ||
       m_CB_decision_return->Text.IsEmpty() || m_CB_decision_return->Text == " " ||
       m_CB_is_check->Text.IsEmpty() || m_CB_is_check->Text == " " )
  {
    AnsiString msg = "�� ������� ������� ��������";
    throw ETSError(msg);
  }
  m_dm->EditCond = true;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsRecond::m_CB_cash_errorChange(TObject *Sender)
{
   char s[1] = "";

   switch(m_CB_cash_error->ItemIndex)
   {
      case 0: s[0] = 'Y'; break;
      case 1: s[0] = 'N'; break;
   }
   m_cash_error = s[0];
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsRecond::m_CB_decision_returnChange(
      TObject *Sender)
{
   char s[1] = "";

   switch(m_CB_decision_return->ItemIndex)
   {
      case 0: s[0] = 'Y'; break;
      case 1: s[0] = 'N'; break;
   }
   m_decision_return = s[0];
}
//---------------------------------------------------------------------------

void __fastcall TFRtlReturnsRecond::m_CB_is_checkChange(TObject *Sender)
{
   char s[1] = "";

   switch(m_CB_is_check->ItemIndex)
   {
      case 0: s[0] = 'Y'; break;
      case 1: s[0] = 'N'; break;
   }
   m_is_check = s[0];
}
//---------------------------------------------------------------------------

