//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmOperRtlSales.h"
#include "TSErrors.h"
#include "TSFormControls.h"
#include "FmOperRtlSalesPeriod.h"
#include "FmOperRtlSalesItem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TFOperRtlSales::TFOperRtlSales(TComponent* p_owner, TTSDOperation *p_dm):
    TTSFOperation(p_owner,p_dm),
    m_dm(static_cast<TDOperRtlSales*>(p_dm))
{

}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSales::FormShow(TObject *Sender)
{
  TTSFOperation::FormShow(Sender);
  if (m_dm->BeginDate.DateTimeString().Length()>11)
    m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString().SubString(1, m_dm->BeginDate.DateTimeString().Length()-3);
  else
    m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString();
  if (m_dm->EndDate.DateTimeString().Length())
    m_SBTN_period->Caption =m_SBTN_period->Caption  + " �� " + m_dm->EndDate.DateTimeString().SubString(1, m_dm->EndDate.DateTimeString().Length()-3);
  else
    m_SBTN_period->Caption =m_SBTN_period->Caption + " �� " + m_dm->EndDate.DateTimeString();

  m_DBLCB_organizations->KeyValue = m_dm->OrgId;
  if (m_dm->HomeOrgId != 3)
    m_DBLCB_organizations->Enabled = false;
  else
    m_DBLCB_organizations->Enabled = true;

}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSales::m_ACT_periodExecute(TObject *Sender)
{
  if(TSExecModuleDlg<TFOperRtlSalesPeriod>(this,m_dm))
  {
    if (m_dm->BeginDate.DateTimeString().Length()>11)
      m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString().SubString(1, m_dm->BeginDate.DateTimeString().Length()-3);
    else
      m_SBTN_period->Caption = "� " + m_dm->BeginDate.DateTimeString();
    if (m_dm->EndDate.DateTimeString().Length())
      m_SBTN_period->Caption = m_SBTN_period->Caption  + " �� " + m_dm->EndDate.DateTimeString().SubString(1, m_dm->EndDate.DateTimeString().Length()-3);
    else
      m_SBTN_period->Caption = m_SBTN_period->Caption + " �� " + m_dm->EndDate.DateTimeString();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSales::m_CB_tabacClick(TObject *Sender)
{
   m_dm->IsTabac = m_CB_tabac->Checked;
   m_dm->RefreshDataSets();     
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSales::m_DBLCB_organizationsClick(TObject *Sender)
{
  m_dm->OrgId = m_DBLCB_organizations->KeyValue;      
}
//---------------------------------------------------------------------------
void __fastcall TFOperRtlSales::m_ACT_viewExecute(TObject *Sender)
{
  if(TSExecModuleDlg<TFOperRtlSalesItem>(this,m_dm));
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSales::m_DBG_mainDblClick(TObject *Sender)
{
   m_ACT_viewExecute(Sender);     
}
//---------------------------------------------------------------------------

