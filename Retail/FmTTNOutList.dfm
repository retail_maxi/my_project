inherited FTTNOutList: TFTTNOutList
  Left = 523
  Top = 151
  Width = 870
  Height = 640
  Caption = 'FTTNOutList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 552
    Width = 854
    object SpeedButton1: TSpeedButton [0]
      Left = 0
      Top = 2
      Width = 137
      Height = 25
      Action = m_ACT_load_excel
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000008484000084840000848400008484000000000000FFFF
        0000848484008484840084848400848484008484840084848400848484008484
        84000000000084840000FFFF0000FFFF0000FFFFFF0000000000FF00FF000000
        0000FFFF00008484000084840000848400008484000084840000848400000000
        000084840000FFFF0000FFFF0000FFFFFF0000000000FF00FF00FF00FF00FF00
        FF0000000000FFFF000084840000848400008484000000000000000000008484
        0000FFFF0000FFFF0000FFFFFF00000000008484000000000000FF00FF00FF00
        FF00FF00FF0000000000FFFF000084840000000000008484000084840000FFFF
        0000FFFF0000FFFFFF0000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00000000000000000084840000FFFF0000FFFF0000FFFF
        0000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000000084840000FFFF0000FFFF0000FFFF0000FFFF
        FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000000084840000FFFF0000FFFF0000FFFFFF00FFFFFF000000
        0000848400008484840000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000000084840000FFFF0000FFFF0000FFFFFF000000000000000000FFFF
        000084840000848400008484840000000000FF00FF00FF00FF00FF00FF000000
        000084840000FFFF0000FFFF0000FFFFFF0000000000FF00FF00FF00FF000000
        0000FFFF000084840000848400008484840000000000FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0000000000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 757
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 369
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 583
    Width = 854
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 854
    Height = 526
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    UseMultiTitle = True
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 53
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 71
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 84
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 106
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 138
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 104
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 151
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 183
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 37
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 133
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 39
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DEST_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'AMOUNT_IN'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'AMOUNT_OUT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 854
    inherited m_P_tb_main: TPanel
      Width = 807
      inherited m_TB_main: TToolBar
        Width = 807
        object ToolButton1: TToolButton
          Left = 302
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_DBLCB_organizations: TDBLookupComboBox
          Left = 310
          Top = 2
          Width = 185
          Height = 21
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = m_DS_orgs
          TabOrder = 0
          OnClick = m_DBLCB_organizationsClick
        end
        object ToolButton2: TToolButton
          Left = 495
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 18
          Style = tbsSeparator
        end
        object m_SBTN_set_date: TSpeedButton
          Left = 503
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 807
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'TTNOut'
      Caption = 'm_ACT_set_begin_end_date'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 40085
      EndDate = 40085
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
    object m_ACT_load_excel: TAction
      Category = 'List'
      Caption = '��������� �� Excel'
      Hint = '��������� �� Excel'
      ImageIndex = 7
      OnExecute = m_ACT_load_excelExecute
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DTTNOut.m_Q_list
  end
  object m_DS_orgs: TDataSource
    DataSet = DTTNOut.m_Q_orgs2
    Left = 256
    Top = 8
  end
end
