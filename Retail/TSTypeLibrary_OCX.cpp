// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// C++ TLBWRTR : $Revision:   1.134.1.41  $
// File generated on 15.07.2017 13:47:27 from Type Library described below.

// ************************************************************************ //
// Type Lib: L:\TSTypeLibrary\TSTypeLibrary.tlb (1)
// IID\LCID: {92D1487E-A494-401D-B723-2667DA9E97C5}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Parent TypeLibrary:
//   (0) v1.0 TSRetail, (P:\Modules\Retail\TSRetail.tlb)
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#if defined(USING_ATL)
#include <atl\atlvcl.h>
#endif

#include "TSTypeLibrary_OCX.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Tstypelibrary_tlb
{


};     // namespace Tstypelibrary_tlb
