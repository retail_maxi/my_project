//---------------------------------------------------------------------------

#ifndef FmInvActHstH
#define FmInvActHstH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDIALOG.h"
#include <Db.hpp>
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmInvAct.h"
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
//---------------------------------------------------------------------------

class TFInvActHst : public TTSFDialog
{
__published:
        TDataSource *m_DS_history;
        TMLDBGrid *m_DBG_history;
private:
  TDInvAct *m_dm;
public:
  __fastcall TFInvActHst(TComponent* p_owner,
                         TTSDCustom *p_dm_custom): TTSFDialog(p_owner,
                                                              p_dm_custom),
                                                   m_dm(static_cast<TDInvAct*>(p_dm_custom)) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
