//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmCompetitorsRepriceSummLine.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmDocumentLine"
#pragma link "MLDBPanel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------



void __fastcall TFCompetitorsRepriceSummLine::FormShow(TObject *Sender)
{
   if (m_dm->m_Q_comp_one->Active) m_dm->m_Q_comp_one->Close();
   m_dm->m_Q_comp_one->Open();
   TTSFDocumentLine::FormShow(Sender);
}
//---------------------------------------------------------------------------


AnsiString __fastcall TFCompetitorsRepriceSummLine::BeforeLineApply(TWinControl *p_focused)
{
  if ((m_dm->m_Q_lineRESOLVE_PRICE->IsNull))
  {
    m_DBE_COMP_PRICE->SetFocus();
    return "������� ����";
  }

  if( !Warning.IsEmpty() ) TSExecMessageDlg(this,tmWarning,Warning);

  return "";
}

