//---------------------------------------------------------------------------

#ifndef FmCheckSumH
#define FmCheckSumH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLLovView.h"
#include "DmIncome.h"
#include "MLLov.h"
#include <Db.hpp>
#include "TSFMDIALOG.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TFmCheckSum : public TTSFDialog
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *m_E_sum;
private:	// User declarations
       TDIncome* m_dm;
public:		// User declarations
    __fastcall TFmCheckSum(TComponent* p_owner, TDIncome* p_dm, AnsiString sum);
};
//---------------------------------------------------------------------------
#endif
