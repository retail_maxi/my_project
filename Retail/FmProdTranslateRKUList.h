//---------------------------------------------------------------------------

#ifndef FmProdTranslateRKUListH
#define FmProdTranslateRKUListH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMREFBOOK.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "DmProdTranslateRKU.h"
#include "TSFmRefbook.h"
#include <Menus.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFProdTranslateRKUList : public TTSFRefbook
{
__published:
  TGroupBox *m_GB_hist;
  TMLDBGrid *m_DBG_hist;
  TCheckBox *m_DCB_hist;
  TAction *m_ACT_show_hist;
  TDataSource *m_DS_hist;
  TSplitter *m_SPL_hist;
  void __fastcall m_ACT_show_histExecute(TObject *Sender);
public:
  __fastcall TFProdTranslateRKUList(TComponent* p_owner,
                               TTSDRefbook *p_dm_refbook): TTSFRefbook(p_owner,
                                                                       p_dm_refbook) {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
