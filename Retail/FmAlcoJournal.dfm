inherited FAlcoJournal: TFAlcoJournal
  Left = 566
  Top = 311
  Width = 847
  Height = 497
  Caption = 'FAlcoJournal'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 410
    Width = 831
    Height = 27
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 734
      Height = 27
      inherited m_BBTN_close: TBitBtn
        Anchors = []
        ParentBiDiMode = False
      end
    end
    object m_TB_gen: TToolBar
      Left = 0
      Top = 0
      Width = 121
      Height = 27
      Align = alLeft
      AutoSize = True
      ButtonHeight = 25
      EdgeBorders = []
      TabOrder = 1
      object m_SBTN_generate: TBitBtn
        Left = 0
        Top = 2
        Width = 121
        Height = 25
        Action = m_ACT_generate
        Caption = '������������ �����'
        TabOrder = 0
      end
    end
    object m_TB_reload_list: TToolBar
      Left = 565
      Top = 0
      Width = 169
      Height = 27
      Align = alRight
      AutoSize = True
      ButtonHeight = 25
      EdgeBorders = []
      TabOrder = 2
      object m_SBTN_reload_list: TBitBtn
        Left = 0
        Top = 2
        Width = 161
        Height = 25
        Action = m_ACT_reload_list
        Anchors = [akTop, akRight]
        Caption = '���������� �� ������� ����'
        TabOrder = 0
      end
      object btn1: TToolButton
        Left = 161
        Top = 2
        Width = 8
        Caption = 'btn1'
        Style = tbsSeparator
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 437
    Width = 831
    Height = 22
  end
  inherited m_P_main_top: TPanel
    Width = 831
    inherited m_P_tb_main: TPanel
      Width = 784
      inherited m_TB_main: TToolBar
        Width = 784
        Height = 24
        ButtonHeight = 22
        inherited m_SB_ods: TSpeedButton
          Height = 22
        end
        inherited m_SB_xlsx: TSpeedButton
          Height = 22
        end
        object ToolButton1: TToolButton
          Left = 294
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 13
          Style = tbsSeparator
        end
        object ToolButton2: TToolButton
          Left = 302
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 14
          Style = tbsSeparator
        end
        object m_SB__SBTN_set_date: TSpeedButton
          Left = 310
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 784
    end
  end
  object m_P_3: TPanel [3]
    Left = 0
    Top = 26
    Width = 831
    Height = 27
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    object m_TB_1: TToolBar
      Left = 0
      Top = 0
      Width = 687
      Height = 27
      Align = alLeft
      AutoSize = True
      ButtonWidth = 24
      EdgeBorders = []
      Images = m_IL_main
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Wrapable = False
      object m_L_taxpayer: TLabel
        Left = 0
        Top = 2
        Width = 105
        Height = 22
        Caption = '����������������: '
        Layout = tlCenter
      end
      object btn3: TToolButton
        Left = 105
        Top = 2
        Width = 8
        Caption = 'btn3'
        ImageIndex = 18
        Style = tbsSeparator
      end
      object m_LV_taxpayer: TMLLovListView
        Left = 113
        Top = 2
        Width = 285
        Height = 22
        Lov = m_LOV_taxpayer
        UpDownWidth = 329
        UpDownHeight = 121
        Interval = 500
        TabStop = True
        TabOrder = 1
        ParentColor = False
      end
      object btn2: TToolButton
        Left = 398
        Top = 2
        Width = 8
        Caption = 'btn2'
        ImageIndex = 18
        Style = tbsSeparator
      end
      object m_L_2: TLabel
        Left = 406
        Top = 2
        Width = 36
        Height = 22
        Align = alLeft
        Caption = '�����: '
        Layout = tlCenter
      end
      object btn15: TToolButton
        Left = 442
        Top = 2
        Width = 8
        Caption = 'm3'
        ImageIndex = 17
        Style = tbsSeparator
      end
      object MLDBPanel1: TMLDBPanel
        Left = 450
        Top = 2
        Width = 237
        Height = 22
        DataField = 'NAME'
        DataSource = m_DS_org
        Align = alLeft
        Alignment = taLeftJustify
        BevelWidth = 0
        BorderStyle = bsSingle
        Color = clBtnFace
        UseDockManager = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object m_DBG_main1: TMLDBGrid [4]
    Left = 0
    Top = 53
    Width = 831
    Height = 357
    Align = alClient
    DataSource = m_DS_main
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    UseMultiTitle = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    OnGetCellParams = m_DBG_main1GetCellParams
    Columns = <
      item
        FieldName = 'NUM'
        Footers = <>
      end
      item
        FieldName = 'COD_NAME_INP'
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID_INP'
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME_INP'
        Footers = <>
      end
      item
        FieldName = 'CODE_INP'
        Footers = <>
      end
      item
        FieldName = 'SENDER_NAME'
        Footers = <>
      end
      item
        FieldName = 'SENDER_INN'
        Footers = <>
      end
      item
        FieldName = 'TTN_DATE'
        Footers = <>
      end
      item
        FieldName = 'TTN_NUMBER'
        Footers = <>
      end
      item
        FieldName = 'CAP_INP'
        Footers = <>
      end
      item
        FieldName = 'QNT_INP'
        Footers = <>
      end
      item
        FieldName = 'SUM_INP'
        Footers = <>
      end
      item
        FieldName = 'MARK_CH'
        Footers = <>
      end
      item
        FieldName = 'DATE_CH'
        Footers = <>
      end
      item
        FieldName = 'CODE_CH'
        Footers = <>
      end
      item
        FieldName = 'COD_NAME_CH'
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID_CH'
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME_CH'
        Footers = <>
      end
      item
        FieldName = 'CAP_CH'
        Footers = <>
      end
      item
        FieldName = 'QNT_CH'
        Footers = <>
      end
      item
        FieldName = 'SUM_CH'
        Footers = <>
      end
      item
        FieldName = 'DOC_ID_INC'
        Footers = <>
      end
      item
        FieldName = 'DOC_ID_CH'
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_complete: TAction
      Category = 'Custom'
      Caption = '���������'
      Hint = '���������� ������ ���������'
      ImageIndex = 11
    end
    object m_ACT_uncomplete: TAction
      Category = 'Custom'
      Caption = '�������� ����������'
      Hint = '�������� ����������'
      ImageIndex = 0
    end
    object m_ACT_save: TAction
      Caption = '��������� reg'
      Hint = '��������� reg'
      ImageIndex = 1
    end
    object m_ACT_save_dll: TAction
      Caption = '��������� dll'
    end
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'ML'
      Caption = '� 05.06.2002 �� 05.06.2002'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 41766
      EndDate = 41766
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
    object m_ACT_generate: TAction
      Caption = '������������ �����'
      Hint = '������������ �����'
      OnExecute = m_ACT_generateExecute
    end
    object m_ACT_reload_list: TAction
      Caption = '���������� �� ������� ����'
      Hint = '���������� �� ������� ����'
      OnExecute = m_ACT_reload_listExecute
    end
  end
  inherited m_ACTL_main_add: TActionList
    Left = 480
    Top = 0
    inherited m_ACT_xlsx: TAction
      Visible = True
    end
  end
  inherited m_PM_main_add: TPopupMenu
    Left = 144
    Top = 128
  end
  object m_DS_main: TDataSource
    DataSet = DAlcoJournal.m_Q_main
    Left = 16
    Top = 72
  end
  object m_DS_taxpayer: TDataSource
    DataSet = DAlcoJournal.m_Q_taxpayer
    Left = 224
    Top = 32
  end
  object m_LOV_taxpayer: TMLLov
    DataFieldKey = 'ID'
    DataFieldValue = 'NAME'
    DataSource = m_DS_taxpayer_fict
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_taxpayer
    AutoOpenList = True
    OnAfterApply = m_LOV_taxpayerAfterApply
    Left = 264
    Top = 32
  end
  object m_DS_taxpayer_fict: TDataSource
    DataSet = DAlcoJournal.m_Q_taxpayer_fict
    Left = 192
    Top = 32
  end
  object m_DS_org: TDataSource
    DataSet = DAlcoJournal.m_Q_org
    Left = 601
    Top = 26
  end
end
