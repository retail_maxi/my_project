//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmAlcoJournal.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "MLDBPanel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFAlcoJournal::TFAlcoJournal(TComponent* p_owner, TTSDOperation *p_dm_operation)
        :TTSFOperation(p_owner,p_dm_operation),
         m_dm(static_cast<TDAlcoJournal*>(DMOperation))
{
}
//---------------------------------------------------------------------------
void __fastcall TFAlcoJournal::FormShow(TObject *Sender)
{
   TTSFOperation::FormShow(Sender);
   m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
   m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
   if (m_dm->m_Q_taxpayer->Active) m_dm->m_Q_taxpayer->Close();
   m_dm->m_Q_taxpayer->Open();
   if (m_dm->m_Q_taxpayer_fict->Active) m_dm->m_Q_taxpayer_fict->Close();
   m_dm->m_Q_taxpayer_fict->Open();
   if (m_dm->m_Q_org->Active) m_dm->m_Q_org->Close();
   m_dm->m_Q_org->ParamByName("ID")->AsInteger = m_dm->m_home_org_id;
   m_dm->m_Q_org->Open();
}
//---------------------------------------------------------------------------



void __fastcall TFAlcoJournal::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                        m_ACT_set_begin_end_date->EndDate);       
}
//---------------------------------------------------------------------------
void __fastcall TFAlcoJournal::m_ACT_generateExecute(TObject *Sender)
{
  m_dm->m_gen = 1;
  m_dm->RefreshDataSets();
  
  /*if (m_dm->m_Q_journal->Active) m_dm->m_Q_journal->Close();
  m_dm->m_Q_journal->ParamByName("taxpayer_id")->AsInteger = m_dm->m_Q_taxpayer_fictID->AsInteger;
  m_dm->m_Q_journal->ParamByName("org_id")->AsInteger =m_dm->m_home_org_id;
  m_dm->m_Q_journal->ParamByName("gen")->AsInteger =m_dm->m_gen;
  m_dm->m_Q_journal->Open(); */

}
//---------------------------------------------------------------------------
void __fastcall TFAlcoJournal::m_ACT_reload_listExecute(TObject *Sender)
{
  m_dm->m_SP_fill_buf_alco_jour->ParamByName("P_DATE")->AsDateTime = int(m_dm->GetSysDate());
  m_dm->m_SP_fill_buf_alco_jour->ExecProc();
  m_dm->RefreshDataSets();
}
//---------------------------------------------------------------------------


void __fastcall TFAlcoJournal::m_DBG_main1GetCellParams(TObject *Sender,
      TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
   if (m_dm->m_Q_mainNMCL_ID_INP->IsNull && m_dm->m_Q_mainNMCL_ID_CH->IsNull)
        /*&& !State.Contains(gdSelected)) */
    {
      AFont->Style = TFontStyles()<< fsBold;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFAlcoJournal::m_LOV_taxpayerAfterApply(TObject *Sender)
{
    m_dm->RefreshDataSets();   
}
//---------------------------------------------------------------------------

