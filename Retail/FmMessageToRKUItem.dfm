inherited FMessageToRKUItem: TFMessageToRKUItem
  Left = 347
  Top = 309
  ActiveControl = m_DBE_rku
  Caption = 'FMessageToRKUItem'
  ClientHeight = 315
  ClientWidth = 696
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 265
    Width = 696
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 599
    end
    inherited m_TB_main_control_buttons_item: TToolBar
      Left = 308
    end
    inherited m_TB_main_control_buttons_document: TToolBar
      Left = 114
    end
    object ToolBar1: TToolBar
      Left = -16
      Top = 0
      Width = 130
      Height = 31
      Align = alRight
      AutoSize = True
      ButtonHeight = 25
      EdgeBorders = []
      TabOrder = 3
      Wrapable = False
      object SpeedButton2: TSpeedButton
        Left = 0
        Top = 2
        Width = 130
        Height = 25
        Action = m_ACT_resend
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00840000008400000084000000840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF008400000084000000840000008400000084000000840000008400
          00008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF008400
          00008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00840000008400000084000000FF00FF00FF00FF00FF00FF008400
          000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF008400000084000000FF00FF00FF00FF00840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF008400000084000000FF00FF00840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF008400000084000000FF00FF00840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF008400000084000000840000008400000084000000FF00FF00FF00FF008400
          000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0084000000840000008400000084000000FF00FF00FF00FF008400
          00008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00840000008400000084000000FF00FF00FF00FF00FF00
          FF00840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF008400000084000000840000008400000084000000FF00FF00FF00FF00FF00
          FF00FF00FF008400000084000000840000008400000084000000840000008400
          00008400000084000000FF00FF00FF00FF0084000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00840000008400000084000000840000008400
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 296
    Width = 696
  end
  inherited m_P_main_top: TPanel
    Width = 696
    inherited m_P_tb_main: TPanel
      Width = 649
      inherited m_TB_main: TToolBar
        Width = 649
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 649
    end
  end
  object m_PC_item: TPageControl [3]
    Left = 0
    Top = 26
    Width = 696
    Height = 239
    ActivePage = m_TS_first
    Align = alClient
    TabOrder = 3
    object m_TS_first: TTabSheet
      Caption = '���������'
      object m_L_type_mess: TLabel
        Left = 55
        Top = 100
        Width = 79
        Height = 13
        Caption = '��� ���������'
        FocusControl = m_LV_type_message
      end
      object m_L_host: TLabel
        Left = 47
        Top = 40
        Width = 88
        Height = 13
        Caption = '��� ����������'
      end
      object m_L_rku: TLabel
        Left = 99
        Top = 70
        Width = 36
        Height = 13
        Caption = '� ���'
        FocusControl = m_DBE_rku
      end
      object m_L_send_date: TLabel
        Left = 407
        Top = 10
        Width = 94
        Height = 13
        Caption = '���� �����������'
      end
      object m_L_cre_user_id: TLabel
        Left = 3
        Top = 190
        Width = 132
        Height = 13
        Caption = '������������ ���������'
      end
      object m_L_doc_number: TLabel
        Left = 64
        Top = 10
        Width = 71
        Height = 13
        Caption = '� ���������'
      end
      object m_L_doc_date: TLabel
        Left = 214
        Top = 10
        Width = 77
        Height = 13
        Caption = '���� ��������'
      end
      object m_L_note: TLabel
        Left = 75
        Top = 125
        Width = 58
        Height = 13
        Caption = 'C��������'
        FocusControl = m_DBM_note
      end
      object m_SB_show_rku: TSpeedButton
        Left = 375
        Top = 65
        Width = 23
        Height = 22
        Action = m_ACT_show_RKU
      end
      object m_LV_type_message: TMLLovListView
        Left = 140
        Top = 95
        Width = 451
        Height = 21
        Lov = m_LOV_type_mess
        UpDownWidth = 456
        UpDownHeight = 121
        Interval = 500
        TabStop = True
        TabOrder = 5
        ParentColor = False
      end
      object m_DBE_rku: TDBEdit
        Left = 140
        Top = 65
        Width = 236
        Height = 21
        DataField = 'RKU'
        DataSource = m_DS_item
        ReadOnly = True
        TabOrder = 4
      end
      object m_DBP_send_date: TMLDBPanel
        Left = 510
        Top = 5
        Width = 101
        Height = 21
        DataField = 'SEND_DATE'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 2
      end
      object m_DBP_doc_number: TMLDBPanel
        Left = 140
        Top = 5
        Width = 61
        Height = 21
        DataField = 'DOC_NUMBER'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 0
      end
      object m_DBP_doc_date: TMLDBPanel
        Left = 295
        Top = 5
        Width = 101
        Height = 21
        DataField = 'DOC_DATE'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 1
      end
      object m_DBP_cre_user: TMLDBPanel
        Left = 140
        Top = 185
        Width = 451
        Height = 21
        DataField = 'CREATE_USER_NAME'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 7
      end
      object m_DBM_note: TDBMemo
        Left = 140
        Top = 124
        Width = 451
        Height = 51
        DataField = 'NOTE'
        DataSource = m_DS_item
        MaxLength = 250
        TabOrder = 6
      end
      object m_DBP_host: TMLDBPanel
        Left = 140
        Top = 35
        Width = 256
        Height = 21
        DataField = 'HOST'
        DataSource = m_DS_item
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Color = clBtnFace
        UseDockManager = True
        ParentColor = False
        TabOrder = 3
      end
    end
    object m_TS_second: TTabSheet
      Caption = '���������� ��������'
      ImageIndex = 1
      object m_DBG_not_read: TMLDBGrid
        Left = 0
        Top = 21
        Width = 688
        Height = 190
        Align = alClient
        DataSource = m_DS_not_read
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterColor = clWindow
        AutoFitColWidths = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
        Columns = <
          item
            FieldName = 'CNT_ID'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'CNT_NAME'
            Title.TitleButton = True
            Width = 200
            Footers = <>
          end
          item
            FieldName = 'RKU'
            Title.TitleButton = True
            Footers = <>
          end
          item
            FieldName = 'IS_READ'
            Title.TitleButton = True
            Width = 54
            KeyList.Strings = (
              'Y'
              'N')
            Checkboxes = True
            Footers = <>
          end
          item
            FieldName = 'READ_DATE'
            Title.TitleButton = True
            Footers = <>
          end>
      end
      object m_P_show_all: TPanel
        Left = 0
        Top = 0
        Width = 688
        Height = 21
        Align = alTop
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 2
          Top = 4
          Width = 63
          Height = 13
          Caption = '����������'
        end
        object m_CB_is_read: TComboBox
          Left = 72
          Top = 0
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = '���'
          OnChange = m_CB_is_readChange
          Items.Strings = (
            '���'
            '�����������'
            '�� �����������')
        end
      end
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_show_RKU: TAction
      Category = 'ShowRKU'
      Caption = '...'
      Hint = '�������� ������ ���'
      OnExecute = m_ACT_show_RKUExecute
      OnUpdate = m_ACT_show_RKUUpdate
    end
    object m_ACT_show_all: TAction
      Category = 'ShowRKU'
      Caption = '�������� ���'
      Hint = '�������� ���'
    end
    object m_ACT_resend: TAction
      Caption = '��������� ��������'
      ImageIndex = 2
      OnExecute = m_ACT_resendExecute
      OnUpdate = m_ACT_resendUpdate
    end
  end
  inherited m_DS_item: TDataSource
    DataSet = DMessageToRKU.m_Q_item
  end
  object m_LOV_type_mess: TMLLov
    DataFieldKey = 'TYPE_MESSAGE_ID'
    DataFieldValue = 'MESSAGE_TYPE'
    DataSource = m_DS_item
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_type_mess
    AutoOpenList = True
    Left = 370
    Top = 145
  end
  object m_DS_type_mess: TDataSource
    DataSet = DMessageToRKU.m_Q_type_mess_list
    Left = 335
    Top = 145
  end
  object m_DS_not_read: TDataSource
    DataSet = DMessageToRKU.m_Q_not_read
    Left = 33
    Top = 120
  end
end
