//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmAlcoJournalNew.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "MLDBPanel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall
TFAlcoJournalNew::TFAlcoJournalNew( TComponent* p_owner, TTSDOperation *p_dm_operation ):
	TTSFOperation( p_owner, p_dm_operation ),
    alcoReportControl_( "REP_ALCO_JOURNAL_NEW" ),
    m_dm( static_cast< TDAlcoJournalNew* >( DMOperation ) )
{
}
//---------------------------------------------------------------------------
void __fastcall TFAlcoJournalNew::FormShow(TObject *Sender)
{
    TTSFOperation::FormShow( Sender );

    m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
    m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;

    if ( m_dm->m_Q_taxpayer->Active ) m_dm->m_Q_taxpayer->Close();
    m_dm->m_Q_taxpayer->Open();
    if ( m_dm->m_Q_taxpayer_fict->Active ) m_dm->m_Q_taxpayer_fict->Close();
    m_dm->m_Q_taxpayer_fict->Open();

    if ( m_dm->m_Q_org->Active ) m_dm->m_Q_org->Close();
    m_dm->m_Q_org->Open();

    if (m_dm->m_Q_address->Active) m_dm->m_Q_address->Close();
    m_dm->m_Q_address->Open();

    m_P_3->DoubleBuffered = true;
}
//---------------------------------------------------------------------------



void __fastcall
TFAlcoJournalNew::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
	m_dm->SetBeginEndDate(
    	m_ACT_set_begin_end_date->BeginDate,
    	m_ACT_set_begin_end_date->EndDate
    );
}



void __fastcall 
TFAlcoJournalNew::m_DBG_main1GetCellParams(
	TObject *Sender,
    TColumnEh *Column, TFont *AFont, TColor &Background,
    TGridDrawState State )
{
	if ( m_dm->m_Q_mainCHECK_DATE->IsNull )
    	AFont->Style = TFontStyles() << fsBold;
}



void __fastcall
TFAlcoJournalNew::m_LOV_taxpayerAfterApply( TObject *Sender )
{
    m_dm->RefreshDataSets();
}



void __fastcall
TFAlcoJournalNew::m_ACT_print_formExecute( TObject *Sender )
{
    TMLParams params;
    params.InitParam( "TAXPAYER_ID", m_dm->m_Q_taxpayer_fictID->AsInteger );
    params.InitParam( "BEGIN_DATE", m_ACT_set_begin_end_date->BeginDate );
	params.InitParam( "END_DATE", m_ACT_set_begin_end_date->EndDate );
	params.InitParam( "ADDRESS_STRING", WideString( m_dm->m_Q_addressSHOW_ADDRESS->AsString ) );

    alcoReportControl_.Execute( true, "TSReport3Server.TSFReport3", params );
}

