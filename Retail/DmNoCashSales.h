//---------------------------------------------------------------------------

#ifndef DmNoCashSalesH
#define DmNoCashSalesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMOPERATION.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDNoCashSales : public TTSDOperation
{
__published:	// IDE-managed Components
        TFloatField *m_Q_mainFISCAL_NUM;
        TDateTimeField *m_Q_mainCHECK_DATE;
        TFloatField *m_Q_mainMAXI_SUM;
        TFloatField *m_Q_mainRMI_SUM;
        TStringField *m_Q_mainNOTE;
        TMLQuery *m_Q_check_dates;
        TDateTimeField *m_Q_check_datesCHECK_DATE;
        TMLQuery *m_Q_data_by_date;
        TFloatField *m_Q_data_by_dateFISCAL_NUM;
        TDateTimeField *m_Q_data_by_dateCHECK_DATE;
        TDateTimeField *m_Q_data_by_dateCHECK_DAY;
        TFloatField *m_Q_data_by_dateMAXI_SUM;
        TFloatField *m_Q_data_by_dateRMI_SUM;
        TStringField *m_Q_data_by_dateNOTE;
        TFloatField *m_Q_data_by_dateNDS;
        TMLQuery *m_Q_check_fn;
        TFloatField *m_Q_check_fnFISCAL_NUM;
        TMLQuery *m_Q_check_org;
        TStringField *m_Q_check_orgNAME;
        void __fastcall m_Q_mainBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_check_datesBeforeOpen(TDataSet *DataSet);
private:	// User declarations
    TDateTime __fastcall GetBeginDate();
    TDateTime __fastcall GetEndDate();
    TDateTime begin_date;
    TDateTime end_date;
public:		// User declarations
  bool __fastcall DoConvertXLSXDefault(AnsiString p_file_name);
  void __fastcall SetBeginEndDate(
        const TDateTime &p_begin_date,
        const TDateTime &p_end_date);

    __property TDateTime BeginDate = {read=GetBeginDate};
    __property TDateTime EndDate = {read=GetEndDate};

        __fastcall TDNoCashSales(TComponent* p_owner,AnsiString p_prog_id);
};
//---------------------------------------------------------------------------
#endif
