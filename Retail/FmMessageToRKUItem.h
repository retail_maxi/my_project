//---------------------------------------------------------------------------

#ifndef FmMessageToRKUItemH
#define FmMessageToRKUItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMDOCUMENT.h"
#include "DmMessageToRKU.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLLov.h"
#include "MLLovView.h"
#include "MLDBPanel.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Grids.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFMessageToRKUItem : public TTSFDocument
{
__published:	// IDE-managed Components
    TAction *m_ACT_show_RKU;
    TPageControl *m_PC_item;
    TTabSheet *m_TS_first;
    TTabSheet *m_TS_second;
    TLabel *m_L_type_mess;
    TLabel *m_L_host;
    TLabel *m_L_rku;
    TLabel *m_L_send_date;
    TLabel *m_L_cre_user_id;
    TLabel *m_L_doc_number;
    TLabel *m_L_doc_date;
    TLabel *m_L_note;
    TSpeedButton *m_SB_show_rku;
    TMLLovListView *m_LV_type_message;
    TDBEdit *m_DBE_rku;
    TMLDBPanel *m_DBP_send_date;
    TMLDBPanel *m_DBP_doc_number;
    TMLDBPanel *m_DBP_doc_date;
    TMLDBPanel *m_DBP_cre_user;
    TDBMemo *m_DBM_note;
    TMLDBPanel *m_DBP_host;
    TMLLov *m_LOV_type_mess;
    TDataSource *m_DS_type_mess;
    TDataSource *m_DS_not_read;
    TMLDBGrid *m_DBG_not_read;
    TPanel *m_P_show_all;
    TAction *m_ACT_show_all;
        TComboBox *m_CB_is_read;
        TLabel *Label1;
   TToolBar *ToolBar1;
   TSpeedButton *SpeedButton2;
   TAction *m_ACT_resend;
    void __fastcall m_ACT_show_RKUUpdate(TObject *Sender);
    void __fastcall m_ACT_show_RKUExecute(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
        void __fastcall m_CB_is_readChange(TObject *Sender);
        void __fastcall m_ACT_next_fldrExecute(TObject *Sender);
   void __fastcall m_ACT_resendExecute(TObject *Sender);
   void __fastcall m_ACT_resendUpdate(TObject *Sender);
private:	// User declarations
    TDMessageToRKU *m_dm;
public:		// User declarations
    __fastcall TFMessageToRKUItem(TComponent* p_owner, TTSDDocument *p_dm_document);
};
//---------------------------------------------------------------------------
#endif
