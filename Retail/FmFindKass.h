//---------------------------------------------------------------------------

#ifndef FmFindKassH
#define FmFindKassH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFmOperation.h"
#include "DmFindKass.h"
#include "MLActionsControls.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Db.hpp>
#include <Grids.hpp>
#include <DBCtrls.hpp>
#include "TSFormControls.h"
#include "TSFormContainer.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TFFindKass : public TTSFOperation
{
__published:
  TSpeedButton *m_SBTN_set_date;
  TToolBar *m_TB_open;
  TSpeedButton *m_SBTN_open;
  TAction *m_ACT_open_sale;
  TAction *m_ACT_find_check;
  TDataSource *m_DS_list;
        TPageControl *PageControl1;
        TTabSheet *TabSheet1;
        TMLDBGrid *m_DBG_list;
        TPanel *m_P_find;
        TLabel *m_L_check_num;
        TLabel *m_L_fiscal_num;
        TSpeedButton *m_SBTN_find_check;
        TLabel *m_L_chec_date;
        TEdit *m_E_check_num;
        TEdit *m_E_fiscal_num;
        TDateTimePicker *m_DTP_check_date;
        TTabSheet *TabSheet2;
        TDataSource *m_DS_list_tmp;
        TMLDBGrid *m_DBG_tmp_list;
        TToolBar *ToolBar1;
        TSpeedButton *SpeedButton1;
        TToolButton *ToolButton1;
        TAction *m_ACT_load_excel;
        TOpenDialog *m_OP_file;
  void __fastcall m_ACT_find_checkExecute(TObject *Sender);
  void __fastcall m_ACT_find_checkUpdate(TObject *Sender);
  void __fastcall m_ACT_open_saleExecute(TObject *Sender);
  void __fastcall m_ACT_open_saleUpdate(TObject *Sender);
  void __fastcall m_DBG_listDblClick(TObject *Sender);
        void __fastcall m_ACT_load_excelExecute(TObject *Sender);
private:
  TDFindKass *m_dm;
public:
  __fastcall TFFindKass(TComponent* p_owner, TTSDOperation *p_dm_operation);
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------

