//---------------------------------------------------------------------------

#ifndef DmRtlPlanMarginSeedH
#define DmRtlPlanMarginSeedH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMREFBOOK.h"
#include "TSReportContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------

class TDRtlPlanMarginSeed : public TTSDRefbook
{
__published:
        TFloatField *m_Q_listPURCHASE;
        TFloatField *m_Q_listTRANSFER;
        TFloatField *m_Q_listPRICE_GM;
        TFloatField *m_Q_listPRICE_SM;
        TFloatField *m_Q_itemPURCHASE;
        TFloatField *m_Q_itemTRANSFER;
        TFloatField *m_Q_itemPRICE_GM;
        TFloatField *m_Q_itemPRICE_SM;
protected:
  TS_REFB_SQ_NAME("sq_rtl_pm_seed")
public:
  __fastcall TDRtlPlanMarginSeed(TComponent* p_owner,
                           AnsiString p_prog_id): TTSDRefbook(p_owner,
                                                              p_prog_id)
                                                              {};
};
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
