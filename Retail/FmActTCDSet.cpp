//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmActTCDSet.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------


bool ExecActTCDSetDlg(TComponent *p_owner, TDIncome *p_dm)
{
  bool res = false;

  try
  {
    p_dm->m_Q_act_tcd->Open();

    TFActTCDSet *f_act_tcd_set = new TFActTCDSet(p_owner,p_dm);
    try
    {
      p_dm->ReDefineDataSources(f_act_tcd_set);


      res = f_act_tcd_set->ShowModal() == IDOK;

      if( res )
      {
        if( (p_dm->m_Q_act_tcd->State == dsInsert) ||
            (p_dm->m_Q_act_tcd->State == dsEdit) )
          p_dm->m_Q_act_tcd->Post();

        p_dm->ClearTmpReportParamValues2(p_dm->SqId);
        p_dm->SqId = p_dm->FillTmpReportParamValues2(p_dm->SqId);
        p_dm->m_Q_load_act_tcd->ParamByName("P_SQ_ID")->AsInteger = p_dm->SqId;
        p_dm->m_Q_load_act_tcd->ParamByName("P_DOC_ID")->AsInteger = p_dm->m_Q_itemID->AsInteger;
        p_dm->m_Q_load_act_tcd->ExecSQL();

        p_dm->RefreshItemDataSets();
      }
    }
    __finally
    {
      delete f_act_tcd_set;
    }
  }
  __finally
  {
    if( p_dm->m_Q_act_tcd->Active ) p_dm->m_Q_act_tcd->Close();
  }

  return res;
}
//---------------------------------------------------------------------------
