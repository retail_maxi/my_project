//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocRtlCouponDiscItem.h"
#include "FmDocRtlCouponDiscOrgs.h"
#include "FmGetBuhDoc.h"
#include "Barcode.h"
#include "FmActTCDSet.h"
#include "FmViewBuhDoc.h"
#include "MLFuncs.h"
#include "FmAccountSet.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

AnsiString __fastcall TFDocRtlCouponDiscItem::BeforeItemApply(TWinControl **p_focused)
{
  if (m_dm->UpdateRegimeItem == urDelete) return "";
/*
  if (m_dm->m_Q_itemDEST_ORG_ID->IsNull)
  {
    *p_focused = m_LV_RTT_org;
    return "�������� ���!";
  }
*/  
  if (m_dm->m_Q_itemBEGIN_DATE->IsNull)
  {
    *p_focused = m_DBE_begin_date;
    return "������� ���� ������ ��������";
  }

  if (m_dm->m_Q_itemEND_DATE->IsNull)
  {
    *p_focused = m_DBE_end_date;
    return "������� ���� ��������� ��������";
  }
  return "";
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscItem::FormShow(TObject *Sender)
{
  TTSFDocumentWithLines::FormShow(Sender);
  if (m_dm->m_Q_orgs_name->Active) m_dm->m_Q_orgs_name->Close();
  m_dm->m_Q_orgs_name->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscItem::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  TTSFDocumentWithLines::FormClose(Sender, Action);
  if(m_dm->m_Q_orgs_name->Active)m_dm->m_Q_orgs_name->Close();
  m_dm->m_Q_orgs_name->Open();   
}

//---------------------------------------------------------------------------



void __fastcall TFDocRtlCouponDiscItem::m_ACT_sel_orgsExecute(
      TObject *Sender)
{
  if(m_dm->NeedUpdatesItem()) m_dm->ApplyUpdatesItem();
  if (!ExecDocRtlCouponDiscOrgsDlg(this, m_dm->m_DB_main->Handle, m_dm->m_Q_itemID->AsInteger)) return;
  if(m_dm->m_Q_orgs_name->Active)m_dm->m_Q_orgs_name->Close();
  m_dm->m_Q_orgs_name->Open();   
}
//---------------------------------------------------------------------------

void __fastcall TFDocRtlCouponDiscItem::m_ACT_sel_orgsUpdate(
      TObject *Sender)
{
  m_ACT_sel_orgs->Enabled = m_dm->UpdateRegimeItem != urView;   
}
//---------------------------------------------------------------------------

