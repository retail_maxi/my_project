//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmOperRtlSalesItem.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLDBPanel"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFOperRtlSalesItem::TFOperRtlSalesItem(TComponent* p_owner, TDOperRtlSales *p_dm):
                               TTSFSubItem(p_owner,p_dm),
                                            m_dm(p_dm),
                                            m_check("SHOP_CHECK")
{
  m_dm->ReDefineDataSources(this);
  m_start_win_control = ActiveControl;
  FPressOK = false;
}
//---------------------------------------------------------------------------

bool __fastcall TFOperRtlSalesItem::GetPressOK()
{
  return FPressOK;
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSalesItem::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  FPressOK = true;
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSalesItem::m_ACT_print_checkExecute(
      TObject *Sender)
{
  TMLParams v_par;
  v_par.InitParam("REPORT_DOC_ID", m_dm->m_Q_item_checksREPORT_DOC_ID->AsInteger);
  v_par.InitParam("CHECK_NUM", m_dm->m_Q_item_checksCHECK_NUM->AsInteger);
  m_check.Execute(true, v_par);
}
//---------------------------------------------------------------------------

void __fastcall TFOperRtlSalesItem::m_ACT_print_checkUpdate(
      TObject *Sender)
{
    m_ACT_print_check->Enabled = true;
}
//---------------------------------------------------------------------------


void __fastcall TFOperRtlSalesItem::FormShow(TObject *Sender)
{
if (m_dm->m_Q_item->Active)
    m_dm->m_Q_item->Close();
    m_dm->m_Q_item->ParamByName("ID")->AsInteger = m_dm->m_Q_mainID->AsInteger;
    m_dm->m_Q_item->Open();
    if (m_dm->m_Q_item_checks->Active)
    m_dm->m_Q_item_checks->Close();  m_dm->m_Q_item_checks->Open();
    if (m_dm->m_Q_lines->Active)
    m_dm->m_Q_lines->Close();  m_dm->m_Q_lines->Open();
}
