inherited FBillList: TFBillList
  Left = 568
  Top = 185
  Width = 718
  Height = 547
  Caption = 'FBillList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 459
    Width = 702
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 605
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 217
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 490
    Width = 702
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 702
    Height = 433
    TitleFont.Name = 'MS Sans Serif'
    FooterFont.Name = 'MS Sans Serif'
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_RIGHT'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ON_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'AMOUNT'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ACC_NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CNT_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'CNT_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MNGR_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MNGR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SRC_ORG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'ACC_DOC_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOP_INFO'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 150
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 702
    inherited m_P_tb_main: TPanel
      Width = 655
      inherited m_TB_main: TToolBar
        Width = 655
        object ToolButton1: TToolButton
          Left = 302
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_DBLCB_organizations: TDBLookupComboBox
          Left = 310
          Top = 2
          Width = 185
          Height = 21
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = m_DS_orgs
          TabOrder = 0
          OnClick = m_DBLCB_organizationsClick
        end
        object m1: TToolButton
          Left = 495
          Top = 2
          Width = 8
          Caption = 'm1'
          ImageIndex = 18
          Style = tbsSeparator
        end
        object m_SBTN_set_date: TSpeedButton
          Left = 503
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 655
    end
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_delete: TAction
      OnUpdate = m_ACT_insertUpdate
    end
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'Bill'
      Caption = 'm_ACT_set_begin_end_date'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 40084
      EndDate = 40084
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DBill.m_Q_list
    Left = 8
    Top = 64
  end
  object m_DS_orgs: TDataSource
    DataSet = DBill.m_Q_orgs
    Left = 256
    Top = 8
  end
end
