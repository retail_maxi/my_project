//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvActLine.h"
#include "FmInvActAlco.h"
#include "MLFuncs.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmDocumentLine"
#pragma link "MLDBPanel"
#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma link "RXCtrls"
#pragma link "DLBCScaner"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFInvActLine::TFInvActLine(TComponent* p_owner, TTSDDocumentWithLines *p_dm)
                                     : TTSFDocumentLine(p_owner,p_dm),
                                     m_dm(static_cast<TDInvAct*>(p_dm)),
                                     doc_sheet_ctrl("TSRetail.InvSheet")
{
  formLoaded = false;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::FormShow(TObject *Sender)
{
  TTSFDocumentLine::FormShow(Sender);
  m_BCS_item->Active = (m_dm->UpdateRegimeItem != urView);
  
  m_TP_start_date->DateTime = m_dm->m_Q_lineSTART_DATE->AsDateTime - int(m_dm->m_Q_lineSTART_DATE->AsDateTime);
  m_TP_start_date->Enabled = (m_dm->UpdateRegimeLine != urView);

  m_dm->m_Q_sheet_lines->Close();
  m_dm->m_Q_sheet_lines->Open();

  m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();

  m_DBCHB_for_recalc->Enabled = m_dm->UpdateRegimeLine != urView && m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr ;
  m_DBE_fact_items->ReadOnly = m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->NewFldr ||
                               !m_dm->m_Q_lineTRANS_RKU_COEF->IsNull;
  m_DBE_from->ReadOnly = m_dm->UpdateRegimeLine == urView ||
                         m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->NewFldr ||
                         m_dm->m_Q_lineTRANS_RKU_COEF->IsNull;
  m_DBE_to->ReadOnly = m_dm->UpdateRegimeLine == urView ||
                       m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->NewFldr ||
                       m_dm->m_Q_lineTRANS_RKU_COEF->IsNull;

  m_DBE_from->Enabled = !m_DBE_from->ReadOnly;
  m_DBE_to->Enabled = !m_DBE_to->ReadOnly;
  m_DBE_from->TabStop = m_DBE_from->Enabled;
  m_DBE_to->TabStop = m_DBE_to->Enabled;

  if(m_DBE_from->ReadOnly)
  {
    m_DBE_from->Color = clBtnFace;
    m_DBE_fact_items->Color = clWindow;
  }
  else
  {
    m_DBE_from->Color = clWindow;
    m_DBE_fact_items->Color = clBtnFace;
  }
  if(m_DBE_to->ReadOnly)
  {
    m_DBE_to->Color = clBtnFace;
    m_DBE_fact_items->Color = clWindow;
  }
  else
  {
    m_DBE_to->Color = clWindow;
    m_DBE_fact_items->Color = clBtnFace;
  }

  m_DBCB_NON_PREPARE->ReadOnly = m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->ChkFldr;
 // if (m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->ChkFldr) m_DBE_fact_items->ReadOnly = true;
 // ������ ���-�� ������ � ����� "��������"
  if ((m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->RecalcFldr) &&
                                  m_dm->m_Q_lineIS_RECALC->AsInteger == 1)
  m_DBE_recalc_items->ReadOnly = false;
  else m_DBE_recalc_items->ReadOnly = true;

  m_DBCHB_for_recalc->Enabled = m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->ChkFldr;

  formLoaded = true;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_TP_start_dateChange(TObject *Sender)
{
  if (m_dm->UpdateRegimeLine != urView)
  {
    if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
    m_dm->m_Q_lineSTART_DATE->AsDateTime = int(m_dm->m_Q_lineSTART_DATE->AsDateTime) +
                                           m_TP_start_date->DateTime - int(m_TP_start_date->DateTime);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_clear_start_dateUpdate(TObject *Sender)
{
  m_ACT_clear_start_date->Enabled = m_dm->UpdateRegimeLine != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_clear_start_dateExecute(TObject *Sender)
{
  if (m_dm->m_Q_line->State == dsBrowse) m_dm->m_Q_line->Edit();
  m_dm->m_Q_lineSTART_DATE->AsDateTime = m_dm->m_Q_itemDEF_START_DATE->AsDateTime;

  m_TP_start_date->DateTime = m_dm->m_Q_lineSTART_DATE->AsDateTime - int(m_dm->m_Q_lineSTART_DATE->AsDateTime);
  m_TP_start_date->Enabled = m_dm->UpdateRegimeLine != urView;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_DBE_fact_itemsChange(TObject *Sender)
{
/*  if (Visible)
  {
    m_dm->m_Q_unbind_lines->ParamByName("ID")->AsInteger = m_dm->m_Q_lineID->AsInteger;
    m_dm->m_Q_unbind_lines->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_lineLINE_ID->AsInteger;
    m_dm->m_Q_unbind_lines->ExecSQL();
    m_dm->m_Q_sheet_lines->Close();
    m_dm->m_Q_sheet_lines->Open();
  } */
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_view_sheetUpdate(TObject *Sender)
{
  m_ACT_view_sheet->Enabled = doc_sheet_ctrl.CanExecuteView && !m_dm->m_Q_sheet_linesDOC_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_view_sheetExecute(TObject *Sender)
{
  TMLParams params;
  params.InitParam("LINE_ID",m_dm->m_Q_sheet_linesLINE_ID->AsInteger);
  doc_sheet_ctrl.ExecuteView(m_dm->m_Q_sheet_linesDOC_ID->AsInteger,params);
}
//---------------------------------------------------------------------------


void __fastcall TFInvActLine::m_DS_lineDataChange(TObject *Sender, TField *Field)
{
  if (Visible && m_dm->UpdateRegimeItem == urEdit && Field && Field == m_dm->m_Q_lineSTART_DATE)
  {
    m_DS_line->OnDataChange = NULL;
    m_dm->ApplyUpdatesLine();
    m_dm->RefreshLineDataSets();
    m_DS_line->OnDataChange = m_DS_lineDataChange;
  }
}
//---------------------------------------------------------------------------


void __fastcall TFInvActLine::m_DBE_fromChange(TObject *Sender)
{
  /*if(!formLoaded) return;

  try
  {
    if((m_DBE_from->Text != "" && m_DBE_from->Text[m_DBE_from->Text.Length()] == ',') ||
      (m_DBE_to->Text != "" && m_DBE_to->Text[m_DBE_to->Text.Length()] == ',')) return;
  }
  catch(...)
  {
    return;
  }

  try
  {
    if (m_dm->m_Q_line->State != dsEdit) m_dm->m_Q_line->Edit();
    m_dm->m_Q_lineFACT_ITEMS->AsFloat = m_DBE_from->Text.ToDouble() + m_DBE_to->Text.ToDouble() * m_DBE_coef->Text.ToDouble();
  }
  catch(...){}
  m_dm->m_Q_line->Post();*/
}
//---------------------------------------------------------------------------



void __fastcall TFInvActLine::m_DBE_fromExit(TObject *Sender)
{
  if(!formLoaded) return;
  
  try
  {
    if (m_dm->m_Q_line->State != dsEdit) m_dm->m_Q_line->Edit();
    m_dm->m_Q_lineFACT_ITEMS->AsFloat = MLDRound( (m_DBE_from->Text.ToDouble() + m_DBE_to->Text.ToDouble() * m_DBE_coef->Text.ToDouble()), 3);
  }
  catch(...){}
  m_dm->m_Q_line->Post();
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_show_hstExecute(TObject *Sender)
{
  TFInvActHst *f = new TFInvActHst(this,m_dm);
  try
  {
    m_dm->m_Q_fact_hst->Open();
    f->ShowModal();
    m_dm->m_Q_fact_hst->Close();
  }
  __finally
  {
    delete f;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_insExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Insert();
  TSExecModuleDlg<TFInvActAlco>(this,m_dm);
}
//---------------------------------------------------------------------------


void __fastcall TFInvActLine::m_ACT_editExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Edit();
  
  TSExecModuleDlg<TFInvActAlco>(this,m_dm);
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_delExecute(TObject *Sender)
{
  if (m_dm->NeedUpdatesLine() ) m_dm->ApplyUpdatesLine();
  m_dm->m_Q_alco->Close();
  m_dm->m_Q_alco->Open();
  m_dm->m_Q_alco->Delete();
  m_dm->m_Q_alco->ApplyUpdates();
  if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_insUpdate(TObject *Sender)
{
  m_ACT_ins->Enabled = m_dm->UpdateRegimeLine != urView
                       && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_editUpdate(TObject *Sender)
{
  m_ACT_edit->Enabled = m_dm->UpdateRegimeLine != urView
                        && !m_dm->m_Q_alcSUB_LINE_ID->IsNull;
                        //&& m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_ACT_delUpdate(TObject *Sender)
{
  m_ACT_del->Enabled = m_dm->UpdateRegimeLine != urView
                     && !m_dm->m_Q_alcSUB_LINE_ID->IsNull
                     && m_dm->m_Q_itemFLDR_ID->AsInteger == m_dm->NewFldr;
}
//---------------------------------------------------------------------------




void __fastcall TFInvActLine::m_BCS_itemBegin(TObject *Sender)
{
  m_temp_grid_options = m_DBG_alco->OptionsML;
  m_DBG_alco->OptionsML = TMLGridOptions() << goWithoutFind;
  m_DBG_sheet_lines->OptionsML = TMLGridOptions() << goWithoutFind;
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::m_BCS_itemEnd(TObject *Sender)
{
  try
  {
    /*String s = m_BCS_item->GetResult();
    while (!m_dm->m_Q_alc->Eof)
    {
      if (m_dm->m_Q_alcPDF_BAR_CODE->AsString == s)
        throw ETSError("������ �������� ����� ��� ���������!");
      m_dm->m_Q_alc->Next();
    }
    */
    if (m_dm->NeedUpdatesItem() ) m_dm->ApplyUpdatesItem();
    if (m_dm->m_Q_alcALCCODE->AsString != m_dm->m_Q_alccodePREF_ALCCODE->AsString)
        throw ETSError("������������� ����� " +m_dm->m_Q_alcALCCODE->AsString+ " �� ������������� ������!");
    m_dm->m_Q_pdf_bar_code->Close();
    m_dm->m_Q_pdf_bar_code->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_lineID->AsInteger;
    m_dm->m_Q_pdf_bar_code->ParamByName("LINE_ID")->AsInteger = m_dm->m_Q_lineLINE_ID->AsInteger;
    m_dm->m_Q_pdf_bar_code->ParamByName("PDF_BAR_CODE")->AsString = m_BCS_item->GetResult();
    m_dm->m_Q_pdf_bar_code->ParamByName("FLDR")->AsInteger = m_dm->m_Q_folderFLDR_ID->AsInteger;
    m_dm->m_Q_pdf_bar_code->ExecSQL();

    if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
    m_dm->m_Q_alc->Open();
  }
  __finally
  {
    m_DBG_alco->OptionsML = m_temp_grid_options;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFInvActLine::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  m_BCS_item->Active = false;
}
//---------------------------------------------------------------------------


