inherited FDocRtlCouponDiscLine: TFDocRtlCouponDiscLine
  Left = 323
  Top = 387
  Caption = 'FDocRtlCouponDiscLine'
  ClientHeight = 272
  ClientWidth = 740
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object m_L_qnt: TLabel [0]
    Left = 4
    Top = 90
    Width = 174
    Height = 13
    Caption = '���, ������������ ������������'
  end
  object Label5: TLabel [1]
    Left = 4
    Top = 125
    Width = 99
    Height = 13
    Caption = '������� ������ (%)'
  end
  object Label9: TLabel [2]
    Left = 4
    Top = 186
    Width = 127
    Height = 13
    Caption = '����������� ����������'
  end
  object m_L_src_amount: TLabel [3]
    Left = 4
    Top = 55
    Width = 176
    Height = 13
    Caption = '���, ������������ ������������'
  end
  object Label1: TLabel [4]
    Left = 5
    Top = 158
    Width = 78
    Height = 13
    Caption = '���� �� ������'
  end
  inherited m_P_main_control: TPanel
    Top = 222
    Width = 740
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 546
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 449
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 253
    Width = 740
  end
  inherited m_P_main_top: TPanel
    Width = 740
    TabOrder = 3
    inherited m_P_tb_main_add: TPanel
      Left = 693
    end
    inherited m_P_tb_main: TPanel
      Width = 693
      inherited m_TB_main: TToolBar
        Width = 693
        ButtonHeight = 22
        object ToolButton1: TToolButton
          Left = 160
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 11
          Style = tbsSeparator
        end
        object ToolButton2: TToolButton
          Left = 168
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 12
          Style = tbsSeparator
        end
      end
    end
  end
  object m_DBE_repcent: TDBEdit [8]
    Left = 185
    Top = 120
    Width = 69
    Height = 21
    DataField = 'PERCENT_DISC'
    DataSource = m_DS_line
    TabOrder = 2
  end
  object m_DBE_limit: TDBEdit [9]
    Left = 185
    Top = 182
    Width = 69
    Height = 21
    DataField = 'LIMIT_QNT'
    DataSource = m_DS_line
    TabOrder = 4
  end
  object m_LV_nmcl: TMLLovListView [10]
    Left = 185
    Top = 53
    Width = 545
    Height = 21
    Lov = m_LOV_nmcl
    UpDownWidth = 545
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 5
    ParentColor = False
  end
  object m_LV_assort: TMLLovListView [11]
    Left = 185
    Top = 87
    Width = 545
    Height = 21
    Lov = m_LOV_assort
    UpDownWidth = 545
    UpDownHeight = 121
    Interval = 500
    TabStop = True
    TabOrder = 6
    ParentColor = False
  end
  object m_DBE_price_disc: TDBEdit [12]
    Left = 186
    Top = 151
    Width = 69
    Height = 21
    DataField = 'PRICE_DISC'
    DataSource = m_DS_line
    TabOrder = 7
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_insert: TAction
      Visible = False
    end
  end
  inherited m_ACTL_main_add: TActionList
    Left = 104
  end
  inherited m_DS_line: TDataSource [19]
    DataSet = DDocRtlCouponDisc.m_Q_line
    Left = 8
  end
  inherited m_PM_main_add: TPopupMenu [20]
    Left = 104
  end
  object m_DS_nmcl: TDataSource
    DataSet = DDocRtlCouponDisc.m_Q_nmcl_list
    Left = 288
    Top = 53
  end
  object m_LOV_nmcl: TMLLov
    DataFieldKey = 'NMCL_ID'
    DataFieldValue = 'NMCL_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'NMCL_ID'
    DataFieldValues = 'NMCL_NAME'
    DataSourceList = m_DS_nmcl
    AutoOpenList = True
    Left = 336
    Top = 53
  end
  object m_DS_assort: TDataSource
    DataSet = DDocRtlCouponDisc.m_Q_assort
    Left = 256
    Top = 86
  end
  object m_LOV_assort: TMLLov
    DataFieldKey = 'ASSORT_ID'
    DataFieldValue = 'ASSORT_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_assort
    AutoOpenList = True
    Left = 296
    Top = 86
  end
end
