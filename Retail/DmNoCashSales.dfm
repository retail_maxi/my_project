inherited DNoCashSales: TDNoCashSales
  Left = 264
  Top = 221
  Height = 640
  Width = 870
  inherited m_DB_main: TDatabase
    AliasName = 'Shop20'
  end
  inherited m_Q_main: TMLQuery
    BeforeOpen = m_Q_mainBeforeOpen
    SQL.Strings = (
      'select rz.fiscal_num,'
      '       rc.check_date,'
      '       trunc(rc.check_date) as check_day,'
      '       nvl((select out_sale_sum'
      '        from rtl_sales'
      '        where doc_id=rs.doc_id'
      '          and sd.taxpayer_id=150),0) as maxi_sum,'
      '       nvl((select out_sale_sum'
      '        from rtl_sales'
      '        where doc_id=rs.doc_id'
      '          and sd.taxpayer_id=2000906),0) as rmi_sum,'
      '       '#39' '#39' as note'
      'from rtl_sales rs,'
      '     rtl_checks rc,'
      '     rtl_zreports rz,'
      '     rtl_sale_deps rsd,'
      '     shop_departments sd'
      'where rc.report_doc_id=rz.doc_id'
      '  and rc.sale_doc_id=rs.doc_id'
      '  and rs.doc_id=rsd.doc_id'
      '  and rsd.dep_id=sd.id'
      '  and check_date between :BEGIN_DATE and :END_DATE +1-1/24/3600'
      '  and rc.cash = '#39'N'#39
      '  order by 3,1,2'
      ' '
      ' '
      ' ')
    UpdateObject = nil
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_mainFISCAL_NUM: TFloatField
      DisplayLabel = '� ����������� ������������'
      FieldName = 'FISCAL_NUM'
    end
    object m_Q_mainCHECK_DATE: TDateTimeField
      DisplayLabel = '����� �������'
      FieldName = 'CHECK_DATE'
    end
    object m_Q_mainMAXI_SUM: TFloatField
      DisplayLabel = '����� �������|�� ��� "�����"'
      FieldName = 'MAXI_SUM'
    end
    object m_Q_mainRMI_SUM: TFloatField
      DisplayLabel = '����� �������|�� ���'
      FieldName = 'RMI_SUM'
    end
    object m_Q_mainNOTE: TStringField
      DisplayLabel = '����������'
      FieldName = 'NOTE'
      FixedChar = True
      Size = 1
    end
  end
  object m_Q_check_dates: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_check_datesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct trunc(check_date) as check_date'
      'from rtl_checks'
      'where check_date between :BEGIN_DATE and :END_DATE +1-1/24/3600'
      'order by check_date ')
    Macros = <>
    Left = 32
    Top = 184
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_check_datesCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
  end
  object m_Q_data_by_date: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_mainBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select rz.fiscal_num,'
      '       rc.check_date,'
      '       trunc(rc.check_date) as check_day,'
      '       nvl((select out_sale_sum'
      '        from rtl_sales'
      '        where doc_id=rs.doc_id'
      '          and sd.taxpayer_id=150),0) as maxi_sum,'
      '       nvl((select out_sale_sum'
      '        from rtl_sales'
      '        where doc_id=rs.doc_id'
      '          and sd.taxpayer_id=2000906),0) as rmi_sum,'
      '       '#39' '#39' as note,'
      
        '       (select round(sum(out_price * quantity / (1 + nds/100) * ' +
        'nds / 100), 2)'
      '        from rtl_sale_lines where doc_id=rs.doc_id) as nds'
      'from rtl_sales rs,'
      '     rtl_checks rc,'
      '     rtl_zreports rz,'
      '     rtl_sale_deps rsd,'
      '     shop_departments sd'
      'where rc.report_doc_id=rz.doc_id'
      '  and rc.sale_doc_id=rs.doc_id'
      '  and rs.doc_id=rsd.doc_id'
      '  and rsd.dep_id=sd.id'
      
        '  and rc.check_date between :CHECK_DATE and :CHECK_DATE +1-1/24/' +
        '3600'
      '  and rz.fiscal_num=:FISCAL_NUM'
      '  and rc.cash = '#39'N'#39
      '  order by 3,1,2'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Macros = <>
    Left = 32
    Top = 232
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'CHECK_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CHECK_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FISCAL_NUM'
        ParamType = ptInput
      end>
    object m_Q_data_by_dateFISCAL_NUM: TFloatField
      FieldName = 'FISCAL_NUM'
    end
    object m_Q_data_by_dateCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object m_Q_data_by_dateCHECK_DAY: TDateTimeField
      FieldName = 'CHECK_DAY'
    end
    object m_Q_data_by_dateMAXI_SUM: TFloatField
      FieldName = 'MAXI_SUM'
    end
    object m_Q_data_by_dateRMI_SUM: TFloatField
      FieldName = 'RMI_SUM'
    end
    object m_Q_data_by_dateNOTE: TStringField
      FieldName = 'NOTE'
      FixedChar = True
      Size = 1
    end
    object m_Q_data_by_dateNDS: TFloatField
      FieldName = 'NDS'
    end
  end
  object m_Q_check_fn: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_check_datesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select distinct rz.fiscal_num as fiscal_num'
      'from rtl_checks rc,'
      '     rtl_zreports rz'
      
        'where rc.check_date between :CHECK_DATE and :CHECK_DATE +1-1/24/' +
        '3600'
      '  and rc.report_doc_id=rz.doc_id'
      'order by rz.fiscal_num')
    Macros = <>
    Left = 120
    Top = 184
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'CHECK_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CHECK_DATE'
        ParamType = ptInput
      end>
    object m_Q_check_fnFISCAL_NUM: TFloatField
      FieldName = 'FISCAL_NUM'
      Origin = 'TSDBMAIN.RTL_ZREPORTS.FISCAL_NUM'
    end
  end
  object m_Q_check_org: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_check_datesBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select trim(rpad(o.name,250)) as name'
      'from organizations o'
      'where id='
      '    (select org_id'
      '    from shop_departments'
      '    where id ='
      '        (select dep_id from rtl_sale_deps'
      '        where rownum=1))'
      ' '
      ' '
      ' ')
    UpdateObject = m_U_main
    Macros = <>
    Left = 200
    Top = 184
    object m_Q_check_orgNAME: TStringField
      FieldName = 'NAME'
      Size = 250
    end
  end
end
