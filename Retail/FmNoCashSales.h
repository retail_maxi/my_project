//---------------------------------------------------------------------------

#ifndef FmNoCashSalesH
#define FmNoCashSalesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmNoCashSales.h"
#include "MLActionsControls.h"
#include "TSFmOperation.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <DBActns.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "DBGridEh.hpp"
#include "MLDBGrid.h"
#include <Db.hpp>
#include <Grids.hpp>
#include "TSReportControls.h"
#include "XLSXConverter.h"
//---------------------------------------------------------------------------
class TFNoCashSales : public TTSFOperation
{
__published:	// IDE-managed Components
        TMLDBGrid *m_DBG_main;
        TDataSource *m_DS_main;
        TMLSetBeginEndDate *m_ACT_set_begin_end_date;
        TSpeedButton *m_SB_set_date;
        TSaveDialog *SaveDialog1;
        TAction *m_ACT_xlsx_;
        TToolBar *ToolBar1;
        TSpeedButton *SpeedButton2;
        TSplitter *Splitter2;
        TSpeedButton *SpeedButton1;
        TSplitter *Splitter1;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall m_ACT_set_begin_end_dateSet(TObject *Sender);
        void __fastcall m_ACT_printUpdate(TObject *Sender);
        void __fastcall m_ACT_printExecute(TObject *Sender);
        void __fastcall m_ACT_xlsx_Update(TObject *Sender);
        void __fastcall m_ACT_xlsx_Execute(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
        TDNoCashSales *m_dm;
        TTSReportControl m_rep_no_cash_sales;
        bool on_convert_;
public:		// User declarations
        __fastcall TFNoCashSales(TComponent* p_owner, TTSDOperation *p_dm_operation);
};
//---------------------------------------------------------------------------
#endif
