inherited DRtlSales: TDRtlSales
  OldCreateOrder = False
  Left = 245
  Top = 140
  Height = 546
  Width = 728
  inherited m_Q_list: TMLQuery
    BeforeOpen = m_Q_listBeforeOpen
    SQL.Strings = (
      'WITH fldrs AS'
      '('
      '    SELECT ff.id, ff.name, ff.doc_type_id FROM folders ff '
      '    WHERE ff.doc_type_id = :DOC_TYPE_ID'
      '    AND fnc_mask_folder_right(ff.id,'#39'Select'#39') = 1'
      ')'
      'select * from ('
      'SELECT /*+ INDEX(D XPK_DOCUMENTS)*/'
      '       d.id AS id,'
      '       d.src_org_id AS doc_src_org_id,'
      '       o.name AS doc_src_org_name,'
      '       d.create_date AS doc_create_date,'
      '       d.comp_name AS doc_create_comp_name,'
      '       d.author AS doc_create_user,'
      '       d.modify_date AS doc_modify_date,'
      '       d.modify_comp_name AS doc_modify_comp_name,'
      '       d.modify_user AS doc_modify_user,'
      '       d.status AS doc_status,'
      
        '       decode(d.status,'#39'F'#39','#39'��������'#39','#39'D'#39','#39'�����'#39','#39'W'#39','#39'������ �' +
        '� ������'#39','#39'������'#39') AS doc_status_name,'
      '       d.status_change_date AS doc_status_change_date,'
      '       d.fldr_id AS doc_fldr_id,'
      '       f.name AS doc_fldr_name,'
      
        '       rpad(fnc_mask_folder_right(f.id,:ACCESS_MASK),250) AS doc' +
        '_fldr_right,'
      '       s.complete AS complete,'
      '       s.out_sale_sum AS sale_sum,'
      '       s.buyer_sum AS buyer_sum,'
      '       s.complete_user_id AS complete_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(s.complete_user_id), ' +
        '250) AS complete_user_name,'
      '       s.host AS host,'
      
        '       (select max(decode(cash, '#39'Y'#39', '#39'���'#39', '#39'������'#39')) from rtl_' +
        'checks where sale_doc_id = s.doc_id) as cash'
      'FROM documents d,'
      '     fldrs f,'
      '     organizations o,'
      '     rtl_sales s'
      'WHERE f.doc_type_id = :DOC_TYPE_ID'
      '  AND f.id = d.fldr_id'
      '  AND o.id = d.src_org_id'
      '  AND s.doc_id = d.id'
      '  AND s.complete BETWEEN :BEGIN_DATE AND :END_DATE'
      
        '  AND ((:ORG_ID = -1) OR (:ORG_ID != -1 AND d.src_org_id = :ORG_' +
        'ID))'
      '  %WHERE_TABAC)'
      '%WHERE_EXPRESSION'
      '%ORDER_EXPRESSION'
      ''
      ''
      ''
      ''
      ''
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_TABAC'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WHERE_EXPRESSION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
        Value = 255
      end
      item
        DataType = ftString
        Name = 'ACCESS_MASK'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DOC_TYPE_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ORG_ID'
        ParamType = ptInput
      end>
    inherited m_Q_listID: TFloatField
      Origin = 'ID'
    end
    inherited m_Q_listDOC_SRC_ORG_NAME: TStringField
      Origin = 'DOC_SRC_ORG_NAME'
    end
    inherited m_Q_listDOC_CREATE_DATE: TDateTimeField
      Origin = 'DOC_CREATE_DATE'
    end
    inherited m_Q_listDOC_CREATE_COMP_NAME: TStringField
      Origin = 'DOC_CREATE_COMP_NAME'
    end
    inherited m_Q_listDOC_CREATE_USER: TStringField
      Origin = 'DOC_CREATE_USER'
    end
    inherited m_Q_listDOC_MODIFY_DATE: TDateTimeField
      Origin = 'DOC_MODIFY_DATE'
    end
    inherited m_Q_listDOC_MODIFY_COMP_NAME: TStringField
      Origin = 'DOC_MODIFY_COMP_NAME'
    end
    inherited m_Q_listDOC_MODIFY_USER: TStringField
      Origin = 'DOC_MODIFY_USER'
    end
    inherited m_Q_listDOC_STATUS_NAME: TStringField
      Origin = 'DOC_STATUS_NAME'
    end
    inherited m_Q_listDOC_STATUS_CHANGE_DATE: TDateTimeField
      Origin = 'DOC_STATUS_CHANGE_DATE'
    end
    inherited m_Q_listDOC_FLDR_NAME: TStringField
      Origin = 'DOC_FLDR_NAME'
    end
    inherited m_Q_listDOC_FLDR_RIGHT: TStringField
      Origin = 'DOC_FLDR_RIGHT'
    end
    object m_Q_listCOMPLETE: TDateTimeField
      DisplayLabel = '���� �������� �� ���'
      FieldName = 'COMPLETE'
      Origin = 'COMPLETE'
    end
    object m_Q_listSALE_SUM: TFloatField
      DisplayLabel = '�����'
      FieldName = 'SALE_SUM'
      Origin = 'SALE_SUM'
    end
    object m_Q_listBUYER_SUM: TFloatField
      DisplayLabel = '������� �����������'
      FieldName = 'BUYER_SUM'
      Origin = 'BUYER_SUM'
    end
    object m_Q_listCOMPLETE_USER_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'COMPLETE_USER_ID'
      Origin = 'COMPLETE_USER_ID'
    end
    object m_Q_listCOMPLETE_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'COMPLETE_USER_NAME'
      Origin = 'COMPLETE_USER_NAME'
      Size = 250
    end
    object m_Q_listHOST: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
    object m_Q_listCASH: TStringField
      DisplayLabel = '��� �������'
      FieldName = 'CASH'
      Origin = 'CASH'
      Size = 6
    end
  end
  inherited m_Q_item: TMLQuery
    SQL.Strings = (
      'SELECT doc_id AS id,'
      '       complete AS complete,'
      '       out_sale_sum AS sale_sum,'
      '       buyer_sum AS buyer_sum,'
      '       complete_user_id AS complete_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(complete_user_id), 25' +
        '0) AS complete_user_name,'
      '       host AS host'
      'FROM rtl_sales s'
      'WHERE doc_id = :ID'
      
        '  AND :ID = (SELECT id FROM documents WHERE id = :ID AND fldr_id' +
        ' = :FLDR_ID)'
      ''
      ''
      '')
    object m_Q_itemCOMPLETE: TDateTimeField
      FieldName = 'COMPLETE'
    end
    object m_Q_itemSALE_SUM: TFloatField
      FieldName = 'SALE_SUM'
    end
    object m_Q_itemBUYER_SUM: TFloatField
      FieldName = 'BUYER_SUM'
    end
    object m_Q_itemCOMPLETE_USER_ID: TFloatField
      FieldName = 'COMPLETE_USER_ID'
    end
    object m_Q_itemCOMPLETE_USER_NAME: TStringField
      FieldName = 'COMPLETE_USER_NAME'
      Size = 250
    end
    object m_Q_itemHOST: TStringField
      FieldName = 'HOST'
      Size = 64
    end
  end
  object m_Q_checks: TMLQuery
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_list
    SQL.Strings = (
      'SELECT check_num AS check_num,'
      '       check_date AS check_date,'
      '       check_sum AS check_sum,'
      '       print_date AS print_date,'
      '       print_user_id AS print_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250) ' +
        'AS print_user_name,'
      '       host AS host'
      'FROM rtl_checks'
      'WHERE sale_doc_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 48
    Top = 296
    ParamData = <
      item
        DataType = ftFloat
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_checksCHECK_NUM: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'CHECK_NUM'
    end
    object m_Q_checksCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'CHECK_DATE'
    end
    object m_Q_checksCHECK_SUM: TFloatField
      DisplayLabel = '�����'
      FieldName = 'CHECK_SUM'
      Origin = 'CHECK_SUM'
    end
    object m_Q_checksPRINT_DATE: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'PRINT_DATE'
      Origin = 'PRINT_DATE'
    end
    object m_Q_checksPRINT_USER_ID: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'PRINT_USER_ID'
      Origin = 'PRINT_USER_ID'
    end
    object m_Q_checksPRINT_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'PRINT_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250)'
      Size = 250
    end
    object m_Q_checksHOST: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
  end
  object m_DS_list: TDataSource
    DataSet = m_Q_list
    Left = 368
    Top = 128
  end
  object m_Q_item_checks: TMLQuery
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_item
    SQL.Strings = (
      'SELECT report_doc_id AS report_doc_id,'
      '       check_num AS check_num,'
      '       check_date AS check_date,'
      '       check_sum AS check_sum,'
      '       print_date AS print_date,'
      '       print_user_id AS print_user_id,'
      
        '       RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250) ' +
        'AS print_user_name,'
      '       host AS host'
      'FROM rtl_checks'
      'WHERE sale_doc_id = :ID'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' '
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 156
    Top = 296
    ParamData = <
      item
        DataType = ftFloat
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_item_checksREPORT_DOC_ID: TFloatField
      FieldName = 'REPORT_DOC_ID'
      Visible = False
    end
    object FloatField1: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUM'
      Origin = 'CHECK_NUM'
    end
    object DateTimeField1: TDateTimeField
      DisplayLabel = '���� ����'
      FieldName = 'CHECK_DATE'
      Origin = 'CHECK_DATE'
    end
    object FloatField2: TFloatField
      DisplayLabel = '�����'
      FieldName = 'CHECK_SUM'
      Origin = 'CHECK_SUM'
    end
    object DateTimeField2: TDateTimeField
      DisplayLabel = '���� ������'
      FieldName = 'PRINT_DATE'
      Origin = 'PRINT_DATE'
    end
    object FloatField3: TFloatField
      DisplayLabel = 'ID �������'
      FieldName = 'PRINT_USER_ID'
      Origin = 'PRINT_USER_ID'
    end
    object StringField1: TStringField
      DisplayLabel = '������'
      FieldName = 'PRINT_USER_NAME'
      Origin = 'RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250)'
      Size = 250
    end
    object StringField2: TStringField
      DisplayLabel = '���������'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
  end
  object m_Q_lines: TMLQuery
    DatabaseName = 'TSDBMain'
    DataSource = m_DS_item
    SQL.Strings = (
      'SELECT rsl.line_id AS line_id,'
      '       rsl.nmcl_id AS nmcl_id,'
      '       ni.name AS nmcl_name,'
      '       rsl.assort_id AS assort_id,'
      '       a.name AS assort_name,'
      '       rsl.weight_code AS weight_code,'
      '       rsl.bar_code AS bar_code,'
      '       rsl.out_price AS out_price,'
      '       rsl.quantity AS quantity,'
      '       sd.dep_number AS dep_number,'
      '       rsl.nds AS nds,'
      '       rsl.check_num AS check_number,'
      '       rsl.card_id AS card_id'
      'FROM rtl_sale_lines rsl,'
      '     nomenclature_items ni,'
      '     assortment a,'
      '     shop_departments sd'
      'WHERE rsl.doc_id = :ID'
      '  AND ni.id = rsl.nmcl_id'
      '  AND a.id = rsl.assort_id'
      '  AND sd.id = rsl.shop_dep_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 56
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_linesLINE_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'LINE_ID'
      Origin = 'rsl.line_id'
    end
    object m_Q_linesNMCL_ID: TFloatField
      DisplayLabel = 'ID ������'
      FieldName = 'NMCL_ID'
      Origin = 'rsl.nmcl_id'
    end
    object m_Q_linesNMCL_NAME: TStringField
      DisplayLabel = '������������'
      FieldName = 'NMCL_NAME'
      Origin = 'ni.name'
      Size = 100
    end
    object m_Q_linesASSORT_ID: TFloatField
      DisplayLabel = 'ID ������.'
      FieldName = 'ASSORT_ID'
      Origin = 'rsl.assort_id'
      Visible = False
    end
    object m_Q_linesASSORT_NAME: TStringField
      DisplayLabel = '������.'
      FieldName = 'ASSORT_NAME'
      Origin = 'a.name'
      Size = 100
    end
    object m_Q_linesWEIGHT_CODE: TFloatField
      DisplayLabel = '������ ����'
      FieldName = 'WEIGHT_CODE'
      Origin = 'rsl.weight_code'
    end
    object m_Q_linesBAR_CODE: TStringField
      DisplayLabel = '�����-���'
      FieldName = 'BAR_CODE'
      Origin = 'rsl.bar_code'
      Size = 30
    end
    object m_Q_linesOUT_PRICE: TFloatField
      DisplayLabel = '����'
      FieldName = 'OUT_PRICE'
      Origin = 'rsl.out_price'
    end
    object m_Q_linesQUANTITY: TFloatField
      DisplayLabel = '���.'
      FieldName = 'QUANTITY'
      Origin = 'rsl.quantity'
    end
    object m_Q_linesDEP_NUMBER: TFloatField
      DisplayLabel = '� ������'
      FieldName = 'DEP_NUMBER'
      Origin = 'sd.dep_number'
      Visible = False
    end
    object m_Q_linesNDS: TFloatField
      DisplayLabel = '���'
      FieldName = 'NDS'
      Origin = 'rsl.nds'
      Visible = False
    end
    object m_Q_linesCHECK_NUMBER: TFloatField
      DisplayLabel = '� ����'
      FieldName = 'CHECK_NUMBER'
      Origin = 'rsl.check_number'
      Visible = False
    end
    object m_Q_linesCARD_ID: TFloatField
      DisplayLabel = 'ID ��������'
      FieldName = 'CARD_ID'
      Origin = 'rsl.card_id'
      Visible = False
    end
  end
  object m_DS_item: TDataSource
    DataSet = m_Q_item
    Left = 440
    Top = 128
  end
  object m_Q_kass: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT '#39' �����'#39' host FROM DUAL'
      'UNION ALL'
      'select host from rtl_checks '
      
        'where check_date BETWEEN TRUNC(:BEGIN_DATE)-1/3600/24 AND TRUNC(' +
        ':END_DATE)+1-1/3600/24'
      'group by host '
      'order by host')
    Macros = <>
    Left = 144
    Top = 360
    ParamData = <
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_kassHOST: TStringField
      FieldName = 'HOST'
      Size = 64
    end
  end
  object m_Q_kass_cnt: TMLQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select DISTINCT print_user_id, TRIM(RPAD(pkg_contractors.fnc_con' +
        'tr_name(print_user_id), 250)) print_user_name,'
      'host, check_date'
      'from rtl_checks'
      
        'where check_date BETWEEN TRUNC(:BEGIN_DATE)-1/3600/24 AND TRUNC(' +
        ':END_DATE)+1-1/3600/24'
      'AND check_num = :NUM'
      'AND (host = :HOST OR :HOST = '#39'-1'#39')'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 208
    Top = 360
    ParamData = <
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NUM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'HOST'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'HOST'
        ParamType = ptInput
      end>
    object m_Q_kass_cntPRINT_USER_ID: TFloatField
      DisplayLabel = 'ID'
      FieldName = 'PRINT_USER_ID'
      Origin = 'PRINT_USER_ID'
    end
    object m_Q_kass_cntPRINT_USER_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'PRINT_USER_NAME'
      Origin = 'TRIM(RPAD(pkg_contractors.fnc_contr_name(print_user_id), 250))'
      Size = 250
    end
    object m_Q_kass_cntHOST: TStringField
      DisplayLabel = '��'
      FieldName = 'HOST'
      Origin = 'HOST'
      Size = 64
    end
    object m_Q_kass_cntCHECK_DATE: TDateTimeField
      DisplayLabel = '����'
      FieldName = 'CHECK_DATE'
      Origin = 'check_date'
    end
  end
  object m_Q_orgs: TQuery
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT id, name, num'
      '  FROM (SELECT -1 as id, '#39'���'#39' as name, 0 as num'
      '          FROM dual'
      '         UNION ALL'
      '        SELECT id, name, 1 as num'
      '          FROM organizations'
      '         WHERE enable = '#39'Y'#39')'
      ' ORDER BY num, name'
      ' ')
    Left = 544
    Top = 240
    object m_Q_orgsID: TFloatField
      FieldName = 'ID'
      Origin = 'TSDBMAIN.ORGANIZATIONS.ID'
    end
    object m_Q_orgsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
end
