//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmNoCashSales.h"
#include "XLSXConverter.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MLActionsControls"
#pragma link "TSFmOperation"
#pragma link "DBGridEh"
#pragma link "MLDBGrid"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TFNoCashSales::TFNoCashSales(TComponent* p_owner,TTSDOperation *p_dm_operation):
                          TTSFOperation(p_owner,p_dm_operation),
                          m_dm(static_cast<TDNoCashSales*>(DMOperation)),
                          m_rep_no_cash_sales("REP_NO_CASH_SALES",&DoFormNew)
{
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::FormShow(TObject *Sender)
{
  m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
  m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;
  if (m_dm->m_Q_check_dates->Active) m_dm->m_Q_check_dates->Close();
  m_dm->m_Q_check_dates->Open();
  TTSFOperation::FormShow(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::m_ACT_set_begin_end_dateSet(TObject *Sender)
{
    m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,m_ACT_set_begin_end_date->EndDate);
    m_dm->RefreshDataSets();
    if (m_dm->m_Q_check_dates->Active) m_dm->m_Q_check_dates->Close();
    m_dm->m_Q_check_dates->Open();
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::m_ACT_printUpdate(TObject *Sender)
{
  m_ACT_print->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::m_ACT_printExecute(TObject *Sender)
{
  TMLParams params;
  params.InitParam("BEGIN_DATE", m_ACT_set_begin_end_date->BeginDate);
  params.InitParam("END_DATE", m_ACT_set_begin_end_date->EndDate);
  m_rep_no_cash_sales.Execute(FormState.Contains(fsModal),params);
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::m_ACT_xlsx_Update(TObject *Sender)
{
  m_ACT_xlsx_->Enabled = !m_dm->m_Q_mainFISCAL_NUM->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::m_ACT_xlsx_Execute(TObject *Sender)
{
  //���������� ������� ������ �������� �����
  //� ������� ���� �� �������� ��� ���������
  if (m_SD_xlsx->Execute())
  {
    if (FileExists(m_SD_xlsx->FileName))
    {
      if (Application->MessageBox("���� ��� ����������. ��������?",
                               Application->Title.c_str(),
                               MB_ICONQUESTION | MB_YESNO) == IDYES)
      {
        DeleteFile(m_SD_xlsx->FileName);
      }
      else
      {
        return;
      }
    }

    //��������� �����
    try
    {
      /*int max_row = StrToInt64(m_dm->GetConstValue("MAX_ROW_2010"));
      XLSXConverter *xlsx = new XLSXImport(m_SD_xlsx->FileName, m_dm->m_Q_main, max_row);
      xlsx->Convert();
      delete xlsx;*/
      m_dm->DoConvertXLSXDefault(m_SD_xlsx->FileName);
    }
    catch(...)
    {
      Screen->Cursor = crDefault;
      throw;
    }
    //���������� ������� ����������� ����
    //� ������������ ���������� ��� ����������
    //
    if( Application->MessageBox(AnsiString("������ ���� " +
                                         m_SD_xlsx->FileName +
                                         ". ������� ���?").c_str(),
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES )
      ShellExecute(Application->Handle,"open",(m_SD_xlsx->FileName).c_str(),NULL,NULL,SW_SHOWNORMAL);

  }
}
//---------------------------------------------------------------------------

void __fastcall TFNoCashSales::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  if (m_dm->m_Q_check_dates->Active) m_dm->m_Q_check_dates->Close();
  m_dm->CancelUpdates();
  TTSFOperation::FormClose(Sender, Action);
}
//---------------------------------------------------------------------------

