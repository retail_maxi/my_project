//---------------------------------------------------------------------------

#ifndef DmBillH
#define DmBillH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include "TSDMDOCUMENTWITHLINES.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDBill : public TTSDDocumentWithLines
{
__published:
  TStringField *m_Q_listDOC_NUMBER;
  TDateTimeField *m_Q_listDOC_DATE;
  TDateTimeField *m_Q_listON_DATE;
  TFloatField *m_Q_listAMOUNT;
  TStringField *m_Q_listNOTE;
  TStringField *m_Q_listACC_NOTE;
  TFloatField *m_Q_listCNT_ID;
  TStringField *m_Q_listCNT_NAME;
  TFloatField *m_Q_listMNGR_ID;
  TStringField *m_Q_listMNGR_NAME;
  TFloatField *m_Q_itemFLDR_ID;
  TFloatField *m_Q_itemSRC_ORG_ID;
  TStringField *m_Q_itemSRC_ORG_NAME;
  TStringField *m_Q_itemDOC_NUMBER;
  TDateTimeField *m_Q_itemDOC_DATE;
  TDateTimeField *m_Q_itemON_DATE;
  TFloatField *m_Q_itemAMOUNT;
  TStringField *m_Q_itemNOTE;
  TFloatField *m_Q_itemACC_DOC_ID;
  TStringField *m_Q_itemCNT_NAME;
  TStringField *m_Q_itemACC_NOTE;
  TStringField *m_Q_itemACC_DOC_NUMBER;
  TDateTimeField *m_Q_itemACC_DOC_DATE;
  TFloatField *m_Q_itemACC_DOC_AMOUNT;
  TStringField *m_Q_itemMNGR_NOTE;
  TFloatField *m_Q_itemCONTRACT_ID;
  TStringField *m_Q_itemCONTRACT_NOTE;
  TFloatField *m_Q_linesNMCL_ID;
  TStringField *m_Q_linesNMCL_NAME;
  TStringField *m_Q_linesMEAS_NAME;
  TFloatField *m_Q_linesASSORTMENT_ID;
  TStringField *m_Q_linesASSORTMENT_NAME;
  TFloatField *m_Q_linesQNT;
  TFloatField *m_Q_linesIN_PRICE;
  TFloatField *m_Q_linesAMOUNT_IN;
  TStringField *m_Q_linesNOTE;
  TFloatField *m_Q_lineNMCL_ID;
  TStringField *m_Q_lineNMCL_NAME;
  TFloatField *m_Q_lineASSORTMENT_ID;
  TStringField *m_Q_lineASSORTMENT_NAME;
  TStringField *m_Q_lineMU_NAME;
  TFloatField *m_Q_lineQNT;
  TFloatField *m_Q_lineIN_PRICE;
  TFloatField *m_Q_lineAMOUNT_IN;
  TStringField *m_Q_lineBAR_CODE;
  TStringField *m_Q_lineNOTE;
  TFloatField *m_Q_linesSTORED_QNT;
  TFloatField *m_Q_linesNON_STORED_QNT;
  TFloatField *m_Q_linesIN_PRICE_ARP;
  TFloatField *m_Q_linesIN_AMOUNT_ARP;
  TFloatField *m_Q_linesDELTA_QNT;
  TFloatField *m_Q_listSRC_ORG_ID;
  TStringField *m_Q_listSRC_ORG_NAME;
  TStringField *m_Q_itemDEST_ORG_NAME;
  TMLQuery *m_Q_only_vld_arp;
        TFloatField *m_Q_listACC_DOC_ID;
        TStringField *m_Q_itemDOP_INFO;
        TStringField *m_Q_listDOP_INFO;
        TStringField *m_Q_itemTTN_NUMBER;
        TDateTimeField *m_Q_itemTTN_DATE;
        TStringField *m_Q_linesNOTE_REASON;
        TMLUpdateSQL *m_U_reason;
        TMLQuery *m_Q_reason;
        TFloatField *m_Q_reasonDOC_ID;
        TFloatField *m_Q_reasonLINE_ID;
        TFloatField *m_Q_reasonNMCL_ID;
        TFloatField *m_Q_reasonASSORTMENT_ID;
        TStringField *m_Q_reasonNOTE_REASON;
        TQuery *m_Q_orgs;
        TFloatField *m_Q_orgsID;
        TStringField *m_Q_orgsNAME;
        TFloatField *m_Q_itemCNT_ID;
        TStringField *m_Q_itemDOC_NUM;
  void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
        void __fastcall m_Q_reasonBeforeOpen(TDataSet *DataSet);

private:
  bool m_view_only_vld_arp;
  TS_DOC_LINE_SQ_NAME("SQ_RTL_BILL_ITEMS")
  TDateTime __fastcall GetBeginDate() const {return m_Q_list->ParamByName("BEGIN_DATE")->AsDateTime;};
  TDateTime __fastcall GetEndDate() const {return m_Q_list->ParamByName("END_DATE")->AsDateTime;};
  void __fastcall SetViewOnlyVldArp(bool p_value);
  int m_home_org_id;
  int __fastcall GetOrgId();
  void __fastcall SetOrgId(int p_value);
public:
    int m_fldr_rdy;
  __fastcall TDBill(TComponent* p_owner, AnsiString p_prog_id);
  __fastcall ~TDBill();
  void __fastcall SetBeginEndDate(TDateTime p_begin_date, TDateTime p_end_date);
  __property int FldrRdy = { read = m_fldr_rdy };
  __property TDateTime BeginDate = { read = GetBeginDate };
  __property TDateTime EndDate = { read = GetEndDate };
  __property bool ViewOnlyVldArp = {read=m_view_only_vld_arp, write=SetViewOnlyVldArp};
  __property int OrgId = {read=GetOrgId, write=SetOrgId};
  __property int HomeOrgId  = { read=m_home_org_id };
};
//---------------------------------------------------------------------------
#endif
