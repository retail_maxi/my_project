inherited FDocDiscGoodsUpdSel: TFDocDiscGoodsUpdSel
  Left = 667
  Top = 230
  Caption = '��������� ���������'
  ClientHeight = 553
  ClientWidth = 475
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 503
    Width = 475
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 258
      Width = 217
      inherited m_SBTN_save: TBitBtn
        Width = 120
        Caption = '� ����� �� ���'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF840000840000FF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FF008400008400840000FF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF84000000840000840000840084
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF840000
          00840000840000FF00008400008400008400008400840000FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FF008400008400008400FF00FF00FF0000840000
          8400008400840000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF00
          00840000FF00FF00FFFF00FF00FF00008400008400008400840000FF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF00FF00FFFF00FFFF00FFFF00FF00
          FF00008400008400008400840000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF00008400008400008400FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FF00FF00008400008400840000FF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF000084000084
          00840000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF00FF00008400008400840000FF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF
          00008400008400FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF00FF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      end
      inherited m_BBTN_close: TBitBtn
        Left = 120
      end
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 161
      inherited m_SBTN_insert: TSpeedButton
        Visible = False
      end
    end
    object m_BB__BTN_close: TBitBtn
      Left = 1
      Top = 2
      Width = 96
      Height = 25
      Action = m_ACT_sel_all
      Cancel = True
      Caption = '�������� ���'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      NumGlyphs = 2
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 534
    Width = 475
    Panels = <
      item
        Style = psOwnerDraw
        Width = 18
      end
      item
        Width = 80
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 475
    inherited m_P_tb_main_add: TPanel
      Left = 428
    end
    inherited m_P_tb_main: TPanel
      Width = 428
      inherited m_TB_main: TToolBar
        Width = 428
      end
    end
  end
  object m_P_top: TPanel [3]
    Left = 0
    Top = 26
    Width = 475
    Height = 89
    Align = alTop
    TabOrder = 3
    object m_L_act_price: TLabel
      Left = 14
      Top = 9
      Width = 76
      Height = 13
      Caption = '���� �� �����'
    end
    object m_L_price_bef_act: TLabel
      Left = 16
      Top = 34
      Width = 74
      Height = 13
      Caption = '���� �� �����'
    end
    object m_L_period: TLabel
      Left = 18
      Top = 58
      Width = 138
      Height = 13
      Caption = '������ ��������  ������ �'
    end
    object m_L_end: TLabel
      Left = 290
      Top = 58
      Width = 12
      Height = 13
      Caption = '��'
    end
    object m_DBDE_begin: TDBDateEdit
      Left = 166
      Top = 55
      Width = 121
      Height = 21
      DataField = 'BEGIN_DATE'
      DataSource = m_DS_upd_param
      NumGlyphs = 2
      TabOrder = 2
    end
    object m_DBDE_END_DATE: TDBDateEdit
      Left = 308
      Top = 55
      Width = 121
      Height = 21
      DataField = 'END_DATE'
      DataSource = m_DS_upd_param
      NumGlyphs = 2
      TabOrder = 3
    end
    object m_DBE_act_price: TDBEdit
      Left = 104
      Top = 6
      Width = 121
      Height = 21
      DataField = 'ACT_PRICE'
      DataSource = m_DS_upd_param
      TabOrder = 0
    end
    object m_DBE_price_bef_act: TDBEdit
      Left = 104
      Top = 30
      Width = 121
      Height = 21
      DataField = 'PRICE_BEF_ACT'
      DataSource = m_DS_upd_param
      TabOrder = 1
    end
    object m_DBCB_urgent_reprice: TDBCheckBox
      Left = 258
      Top = 8
      Width = 129
      Height = 17
      Caption = '������� ����������'
      DataField = 'URGENT_REPRICE'
      DataSource = m_DS_upd_param
      TabOrder = 4
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  object m_GR_rtt: TGroupBox [4]
    Left = 0
    Top = 115
    Width = 475
    Height = 388
    Align = alClient
    Caption = '������ ���'
    TabOrder = 4
    object m_DBG_list: TMLDBGrid
      Left = 2
      Top = 15
      Width = 471
      Height = 371
      Align = alClient
      DataSource = m_DS_orgs
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnKeyDown = m_DBG_listKeyDown
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterColor = clWindow
      AutoFitColWidths = True
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
      Columns = <
        item
          FieldName = 'ID'
          Title.TitleButton = True
          Width = 86
          Footers = <>
        end
        item
          FieldName = 'NAME'
          Title.TitleButton = True
          Width = 200
          Footers = <>
        end
        item
          FieldName = 'CHECKED'
          Title.TitleButton = True
          KeyList.Strings = (
            '1'
            '2')
          Checkboxes = True
          Footers = <>
        end>
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_sel_all: TAction
      Category = 'SubItem'
      Caption = '�������� ���'
      OnExecute = m_ACT_sel_allExecute
    end
  end
  object m_DS_upd_param: TDataSource
    DataSet = DDocDiscGoods.m_Q_fikt_upd
    Left = 304
    Top = 8
  end
  object m_DS_orgs: TDataSource
    DataSet = DDocDiscGoods.m_Q_sel_org
    Left = 28
    Top = 216
  end
end
