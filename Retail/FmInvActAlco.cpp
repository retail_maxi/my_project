//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmInvActAlco.h"
#include "TSErrors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "RXDBCtrl"
#pragma link "ToolEdit"
#pragma link "MLLov"
#pragma link "MLLovView"
#pragma link "MLDBPanel"
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
__fastcall TFInvActAlco::TFInvActAlco(TComponent* p_owner,
                                  TTSDDocumentWithLines* p_dm) : TTSFDialog( p_owner, p_dm),
                                  m_dm(static_cast<TDInvAct*>(p_dm))

{

}
//---------------------------------------------------------------------------
/*bool ExecInvActAlcoDlg(TComponent* p_owner, TTSDDocumentWithLines *p_dm, TDateTime *p_date, AnsiString *p_note)
{

}  */
//---------------------------------------------------------------------------
void __fastcall TFInvActAlco::m_ACT_apply_updatesExecute(TObject *Sender)
{
  if (m_dm->m_Q_alcoALCCODE->IsNull)
  {
   m_LV_alccode->SetFocus() ;
   throw ETSError("�������� �������!");
  }

  if ((m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->RecalcFldr) &&
       m_dm->m_Q_lineIS_RECALC->AsInteger == 1 &&
       m_dm->m_Q_alcoRECALC_ITEMS->IsNull)
  {
    m_DBE_recalc_items->SetFocus() ;
    throw ETSError("��������� ���� ���� �� ���������!");
  }

  if (!(m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->NewFldr ||
      !m_dm->m_Q_lineTRANS_RKU_COEF->IsNull)&&
       m_dm->m_Q_alcoFACT_ITEMS->IsNull)
  {
    m_DBE_fact_items->SetFocus() ;
    throw ETSError("��������� ���� ����!");
  }

  /*if (m_dm->m_Q_alcoQNT->IsNull)
  {
   m_DBE_qnt->SetFocus() ;
   throw ETSError("������� ����������!");
  }
  m_LV_alccode->SetFocus();
  if ((!m_dm->m_Q_alcoPDF_BAR_CODE->IsNull) && (m_dm->m_Q_alcoQNT->AsInteger != 1))
  {
   m_DBE_qnt->SetFocus() ;
   throw ETSError("������ ������ ����������!");
  }*/
  m_dm->m_Q_alco->ApplyUpdates();
  TTSFDialog::m_ACT_apply_updatesExecute(Sender);
  if (m_dm->m_Q_alc->Active) m_dm->m_Q_alc->Close();
  m_dm->m_Q_alc->Open();
}
//---------------------------------------------------------------------------
void __fastcall TFInvActAlco::FormShow(TObject *Sender)
{
// ������ ���-�� ������ � ����� "��������"
  if ((m_dm->m_Q_folderFLDR_ID->AsInteger == m_dm->RecalcFldr) &&
                                  m_dm->m_Q_lineIS_RECALC->AsInteger == 1)
  m_DBE_recalc_items->ReadOnly = false;
  else m_DBE_recalc_items->ReadOnly = true;

  m_DBE_fact_items->ReadOnly = m_dm->m_Q_folderFLDR_ID->AsInteger != m_dm->NewFldr ||
                               !m_dm->m_Q_lineTRANS_RKU_COEF->IsNull;
}
//---------------------------------------------------------------------------

