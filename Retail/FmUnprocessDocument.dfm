inherited FUnprocessDocument: TFUnprocessDocument
  Left = 686
  Top = 342
  Width = 973
  Height = 590
  Caption = 'FUnprocessDocument'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 503
    Width = 957
    Height = 27
    TabOrder = 1
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 860
      Height = 27
      inherited m_BBTN_close: TBitBtn
        Anchors = []
        ParentBiDiMode = False
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 530
    Width = 957
    Height = 22
  end
  inherited m_P_main_top: TPanel
    Width = 957
    TabOrder = 3
    inherited m_P_tb_main: TPanel
      Width = 910
      inherited m_TB_main: TToolBar
        Width = 910
        Height = 24
        ButtonHeight = 22
        inherited m_SB_ods: TSpeedButton
          Height = 22
        end
        inherited m_SB_xlsx: TSpeedButton
          Height = 22
        end
        object ToolButton1: TToolButton
          Left = 294
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 13
          Style = tbsSeparator
        end
        object btn_SBTN_set_date: TSpeedButton
          Left = 302
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
        object ToolButton2: TToolButton
          Left = 454
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 14
          Style = tbsSeparator
        end
        object m_L_org: TLabel
          Left = 462
          Top = 2
          Width = 73
          Height = 22
          Caption = '�����������  '
          Layout = tlCenter
          Visible = False
        end
        object m_LV_org: TMLLovListView
          Left = 535
          Top = 2
          Width = 229
          Height = 22
          Lov = m_LOV_org
          UpDownWidth = 329
          UpDownHeight = 121
          Interval = 500
          ButtonDeleteVisible = False
          TabStop = True
          TabOrder = 0
          ParentColor = False
          Visible = False
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 910
    end
  end
  object m_DBG_main: TMLDBGrid [3]
    Left = 0
    Top = 26
    Width = 957
    Height = 477
    Align = alClient
    DataSource = m_DS_main
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    UseMultiTitle = True
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    Columns = <
      item
        FieldName = 'ORG_NAME'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'DOC_ID'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'DOC_NAME'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'CREATE_DATE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'AUTHOR'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'CNT_ID'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'POSITION'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'MODIFY_USER'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'FLDR_NAME'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'MODIFY_DATE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'SENDER_NAME'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'RECEIVER_NAME'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'SUPPLIER_NAME'
        Title.TitleButton = True
        Width = 100
        Footers = <>
      end
      item
        FieldName = 'DOP_INFO'
        Width = 200
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_complete: TAction
      Category = 'Custom'
      Caption = '���������'
      Hint = '���������� ������ ���������'
      ImageIndex = 11
    end
    object m_ACT_uncomplete: TAction
      Category = 'Custom'
      Caption = '�������� ����������'
      Hint = '�������� ����������'
      ImageIndex = 0
    end
    object m_ACT_save: TAction
      Caption = '��������� reg'
      Hint = '��������� reg'
      ImageIndex = 1
    end
    object m_ACT_save_dll: TAction
      Caption = '��������� dll'
    end
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'ML'
      Caption = '� 05.06.2002 �� 05.06.2002'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 41766
      EndDate = 41766
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
  end
  object m_DS_main: TDataSource
    DataSet = DUnprocessDocument.m_Q_main
    Left = 16
    Top = 72
  end
  object m_DS_org: TDataSource
    DataSet = DUnprocessDocument.m_Q_org
    Left = 680
  end
  object m_LOV_org: TMLLov
    DataFieldKey = 'ID'
    DataFieldValue = 'NAME'
    DataSource = m_DS_org_fict
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_org
    AutoOpenList = True
    Left = 712
  end
  object m_DS_org_fict: TDataSource
    DataSet = DUnprocessDocument.m_Q_org_fict
    Left = 648
  end
end
