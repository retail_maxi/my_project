//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmDocDiscGoodsList.h"
#include "TSFmDBEditDialog.h"
#include "FmDocDiscGoodsUpdSel.h"
#include "TSErrors.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsList::m_ACT_set_begin_end_dateSet(
      TObject *Sender)
{
  m_dm->SetBeginEndDate(m_ACT_set_begin_end_date->BeginDate,
                        m_ACT_set_begin_end_date->EndDate);
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::FormShow(TObject *Sender)
{
    TTSFDocumentList::FormShow(Sender);

    m_ACT_set_begin_end_date->BeginDate = m_dm->BeginDate;
    m_ACT_set_begin_end_date->EndDate = m_dm->EndDate;

    if (m_dm->m_Q_orgs_list->Active) m_dm->m_Q_orgs_list->Close();
    m_dm->m_Q_orgs_list->Open();
    ShowFldrMenu();
    ShowFldrMenuPrev();

}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_ACT_group_processExecute(
      TObject *Sender)
{
    long sq_id = m_dm->FillTmpReportParamValues(m_DBG_list, "ID");
    long doc_id = m_dm->m_Q_listID->AsInteger;
    TFDocDiscGoodsUpdSel *f = new TFDocDiscGoodsUpdSel(this, m_dm,true);
    try
    {
        if (m_dm->m_Q_sel_org->Active) m_dm->m_Q_sel_org->Close();
        m_dm->m_Q_sel_org->ParamByName("ALL_RTT")->AsInteger = 1;
        m_dm->m_Q_sel_org->Open();
        if (m_dm->m_Q_chk_sel_docs->Active)  m_dm->m_Q_chk_sel_docs->Close();
        m_dm->m_Q_chk_sel_docs->ParamByName("SQ_ID")->AsInteger = sq_id;
        m_dm->m_Q_chk_sel_docs->ParamByName("FLDR_NEW")->AsInteger = m_dm->FldrNew;
        m_dm->m_Q_chk_sel_docs->ParamByName("FLDR_SOGL")->AsInteger = m_dm->FldrSogl;
        m_dm->m_Q_chk_sel_docs->ParamByName("FLDR_UTV")->AsInteger = m_dm->FldrUtv;
        m_dm->m_Q_chk_sel_docs->Open();
        if (m_dm->m_Q_chk_sel_docsC->AsInteger > 0)
        {
          if( Application->MessageBox("�� ��������� �������� ������� �������������� ���������. ����������?",
                                        Application->Title.c_str(),
                                        MB_ICONQUESTION | MB_YESNO) == IDNO )
          return;
        }
        if (m_dm->m_Q_fikt_upd->Active) m_dm->m_Q_fikt_upd->Close();
        m_dm->m_Q_fikt_upd->Open();
        m_dm->m_Q_fikt_upd->Edit();
        try
        {
            f->ShowModal();
            if (f->PressOK)
            {
                TStrings *StringList = new TStringList();
                try
                {
                   m_dm->m_Q_sel_org->First();
                   while (!m_dm->m_Q_sel_org->Eof)
                   {
                     if (m_dm->m_Q_sel_org->FieldByName("CHECKED")->AsInteger == 1)
                     {
                       StringList->Add(AnsiString(m_dm->m_Q_sel_org->FieldByName("ID")->AsInteger));
                     }
                     m_dm->m_Q_sel_org->Next();
                   }
                   m_dm->m_Q_upd_rtt->ParamByName("sq_doc_id")->AsInteger = sq_id;
                   m_dm->m_Q_upd_rtt->ParamByName("sq_org_id")->AsInteger = m_dm->FillTmpReportParamValues(StringList);
                   m_dm->m_Q_upd_rtt->ParamByName("act_price")->AsFloat = m_dm->m_Q_fikt_updACT_PRICE->AsFloat;
                   m_dm->m_Q_upd_rtt->ParamByName("price_bef_act")->AsFloat = m_dm->m_Q_fikt_updPRICE_BEF_ACT->AsFloat;
                   m_dm->m_Q_upd_rtt->ParamByName("begin_date")->AsDateTime = m_dm->m_Q_fikt_updBEGIN_DATE->AsDateTime;
                   m_dm->m_Q_upd_rtt->ParamByName("end_date")->AsDateTime = m_dm->m_Q_fikt_updEND_DATE->AsDateTime;
                   m_dm->m_Q_upd_rtt->ParamByName("urgent_reprice")->AsInteger = m_dm->m_Q_fikt_updURGENT_REPRICE->AsInteger;
                   m_dm->m_Q_upd_rtt->ExecSQL();
                   m_dm->m_Q_chg_fldr->ParamByName("SQ_ID")->AsInteger = sq_id;
                   m_dm->m_Q_chg_fldr->ExecSQL();
                   m_dm->RefreshListDataSets();
                   m_dm->m_Q_list->Locate(m_dm->m_Q_listID->FieldName,doc_id,TLocateOptions());
                }
                 __finally
                {
                  delete StringList;
                }
            }
        }
        __finally
        {
            if (m_dm->m_Q_sel_org->Active) m_dm->m_Q_sel_org->Close();
            if (m_dm->m_Q_fikt_upd->Active) m_dm->m_Q_fikt_upd->Close();
        }
    }
    __finally
    {
        if (f) delete f;
    }
    m_dm->ClearTmpReportParamValues(sq_id);
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_ACT_group_processUpdate(
      TObject *Sender)
{
   m_ACT_group_process->Enabled = m_dm->CanGrpChg && !m_dm->m_Q_listID->IsNull
                                  && m_dm->m_Q_listDOC_FLDR_ID->AsInteger != m_dm->FldrAct
                                  && m_dm->m_Q_listDOC_FLDR_ID->AsInteger != m_dm->FldrArc
                                  && m_dm->m_Q_listDOC_FLDR_ID->AsInteger != m_dm->FldrCansel
                                  && !m_dm->m_Q_listACTION_TYPE_ID->IsNull
                                  && !m_dm->m_Q_listNMCL_ID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_ACT_copyUpdate(TObject *Sender)
{
    m_ACT_copy->Enabled = m_dm->CanInsertItem && !m_dm->m_Q_listID->IsNull;
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_ACT_copyExecute(TObject *Sender)
{
    m_dm->m_SP_copy_doc->ParamByName("P_DOC_ID")->AsInteger = m_dm->m_Q_listID->AsInteger;
    m_dm->m_SP_copy_doc->ExecProc();

    if (!m_dm->m_SP_copy_doc->ParamByName("P_DEST_DOC_ID")->IsNull)
    {
        long m_doc_id = m_dm->m_SP_copy_doc->ParamByName("P_DEST_DOC_ID")->AsInteger;
        m_dm->RefreshListDataSets();
        m_dm->m_Q_list->Locate(m_dm->m_Q_listID->FieldName,
                                                  m_doc_id,
                                                  TLocateOptions());

        TTSFDocumentList::m_ACT_editExecute(Sender);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_DBG_listGetCellParams(
      TObject *Sender, TColumnEh *Column, TFont *AFont, TColor &Background,
      TGridDrawState State)
{
  if( (m_dm->m_Q_listDOC_STATUS->AsString == 'D') &&
      !State.Contains(gdSelected) )
  {
    Background = clRed;
    AFont->Color = clWindow;
  }

  if ((m_dm->m_Q_listIS_RETURNED->AsInteger == 1) &&
      !State.Contains(gdSelected))
  {
    Background = clYellow;
    AFont->Color = clBlack;
  }


  if ((m_dm->m_Q_listIS_CURRENT_ACTION->AsInteger == 1) &&
      !State.Contains(gdSelected))
  {
    Background = clAqua;
    AFont->Color = clBlack;
  }

}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsList::m_ACT_view_nmcl_salesExecute(
      TObject *Sender)
{
    TMLParams p_params;
    p_params.InitParam("NMCL_ID",m_dm->m_Q_listNMCL_ID->AsInteger);
    p_params.InitParam("ASSORT_ID",m_dm->m_Q_listASSORTMENT_ID->AsInteger);
    p_params.InitParam("AGENT_ID",m_dm->m_Q_listAGENT_ID->AsInteger);


    if (!m_view_nmcl_sales.CanExecute)
    {
       MessageDlg("��� ���� �� ��������!",
       mtConfirmation, TMsgDlgButtons() << mbOK, 0);
       return;
    }
    else m_view_nmcl_sales.Execute(true, p_params);
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_ACT_view_nmcl_salesUpdate(
      TObject *Sender)
{
   m_ACT_view_nmcl_sales->Enabled = m_view_nmcl_sales.CanExecute &&
                                     !m_dm->m_Q_listNMCL_ID->IsNull;
}
//---------------------------------------------------------------------------


void __fastcall TFDocDiscGoodsList::m_ACT_deleteExecute(TObject *Sender)
{
     if( Application->MessageBox("������� ���������?",
                              Application->Title.c_str(),
                              MB_ICONQUESTION | MB_YESNO) == IDYES )
  if (m_dm->m_Q_del_docs->Active) m_dm->m_Q_del_docs->Close();
        long sq_id = m_dm->FillTmpReportParamValues(m_DBG_list, "ID");
        m_dm->m_Q_del_docs->ParamByName("sq_id")->AsInteger = sq_id;
        m_dm->m_Q_del_docs->Open();
        m_dm->m_Q_del_docs->First();
  while (!m_dm->m_Q_del_docs->Eof) {
          m_dm->DeleteItem(m_dm->m_Q_del_docsDOC_ID->AsInteger);
          m_dm->m_Q_del_docs->Next();
        }
  m_dm->m_Q_del_docs->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::ShowFldrMenu()
{
    //���������� ������ �������� ����� �� ����������
    if (m_dm->m_Q_folders_next_all->Active)
        m_dm->m_Q_folders_next_all->Close();
    m_PM_fldr_next->Items->Clear();
    m_dm->m_Q_folders_next_all->Open();
   // if (!m_dm->m_Q_folders_next_all->Eof)
    //    m_RSB_fldr_next->Caption = m_dm->m_Q_folders_next_allNAME->AsString;

   // m_RSB_fldr_next->Enabled =  !m_dm->m_Q_folders_next_all->IsEmpty();
    
    while (!m_dm->m_Q_folders_next_all->Eof)
    {
       TMenuItem *NewItem = new TMenuItem(m_PM_fldr_next);
       NewItem->Caption = m_dm->m_Q_folders_next_allNAME->AsString;
       NewItem->Tag = m_dm->m_Q_folders_next_allID->AsInteger;
       NewItem->ImageIndex = 16;
       NewItem->Enabled = (m_dm->m_Q_folders_next_allENABLED->AsInteger == 1);
       NewItem->OnClick = PMFldrNextClick;
       m_PM_fldr_next->Items->Add(NewItem);
       m_dm->m_Q_folders_next_all->Next();
    }


    m_TB_next->AutoSize = false;
    m_TB_next->AutoSize = true;
    m_dm->m_Q_folders_next_all->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::PMFldrNextClick(TObject *Sender)
{
  TMenuItem *ClickedItem = dynamic_cast<TMenuItem *>(Sender);
    if (ClickedItem)
    {
          long doc_id = m_dm->m_Q_listID->AsInteger;
        if (m_dm->m_Q_docs->Active) m_dm->m_Q_docs->Close();
        long sq_id = m_dm->FillTmpReportParamValues(m_DBG_list, "ID");
        m_dm->m_Q_docs->ParamByName("sq_id")->AsInteger = sq_id;
        m_dm->m_Q_docs->ParamByName("route_id")->AsInteger = ClickedItem->Tag;
        m_dm->m_Q_docs->Open();
        m_dm->m_Q_docs->First();
        AnsiString reason_rejection="";
        AnsiString reject_zam_kd="";

        long route_id = ClickedItem->Tag;
        long old_route_id = ClickedItem->Tag;

         int route_cansel = getRouteId(m_dm->FldrNew, m_dm->FldrCansel);
         int route_sogl = getRouteId(m_dm->FldrNew, m_dm->FldrSogl);

        while (!m_dm->m_Q_docs->Eof)
        {
          route_id = old_route_id;
          long fldr_id = m_dm->m_Q_docsFLDR_ID->AsInteger;
          if ((route_id == getRouteId(m_dm->FldrNew, m_dm->FldrAct)) && ((m_dm->m_Q_docsMIN_MARGIN_AF_ACT->AsInteger <= -5) ||
                                  (m_dm->m_Q_docsMAX_TOTAL_PROFIT->AsInteger >= 10000))) route_id = getRouteId(m_dm->FldrNew, m_dm->FldrUtv);
          if ((route_id == getRouteId(m_dm->FldrSogl, m_dm->FldrAct)) && ((m_dm->m_Q_docsMIN_MARGIN_AF_ACT->AsInteger <= -5) ||
                                  (m_dm->m_Q_docsMAX_TOTAL_PROFIT->AsInteger >= 10000))) route_id = getRouteId(m_dm->FldrSogl, m_dm->FldrUtv);
          ClickedItem->Tag = route_id;

          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && (m_dm->m_Q_docsBEGIN_DATE->IsNull || m_dm->m_Q_docsEND_DATE->IsNull))
                {
                throw ETSError("������� ������ �������� ������ � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }

          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && m_dm->m_Q_docsACTION_TYPE_ID->IsNull)
                {
                throw ETSError("������� ��� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }
          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && m_dm->m_Q_docsNMCL_ID->IsNull)
                {
                throw ETSError("�������� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }
          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && m_dm->m_Q_docsASSORTMENT_ID->IsNull)
                {
                throw ETSError("�� ������ ����������� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));;
                }
          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && m_dm->m_Q_docsREASON_ID->IsNull)
                {
                throw ETSError("������� ������� ������ � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }
          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && (m_dm->m_Q_docsCOUNT_ORG->AsInteger == 0 || m_dm->m_Q_docsORG_ID_3->AsInteger == 1))
                {
                throw ETSError("�� ������� ���, �� ������� ��������� ��������� ������ � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }
          if (ClickedItem->Tag != route_cansel && ClickedItem->Tag != route_sogl && !(m_dm->m_Q_docsPRICE_BEF_ACT->IsNull) && (m_dm->m_Q_docsACT_PRICE->AsFloat >= m_dm->m_Q_docsPRICE_BEF_ACT->AsFloat))
                {
                throw ETSError("���� �� ����� ������ ���� ������ ���� ��� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }
          if (ClickedItem->Tag == route_sogl  && !(m_dm->m_Q_docsPRICE_BEF_ACT->IsNull) && (m_dm->m_Q_docsACT_PRICE->AsFloat >= m_dm->m_Q_docsPRICE_BEF_ACT->AsFloat))
                {
                throw ETSError("���� �� ����� ������ ���� ������ ���� ��� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }

          if (ClickedItem->Tag != route_cansel && ClickedItem->Tag != route_sogl && !(m_dm->m_Q_docsPRICE_BEF_ACT_ON_CARD->IsNull) && (m_dm->m_Q_docsACT_PRICE->AsFloat >= m_dm->m_Q_docsPRICE_BEF_ACT_ON_CARD->AsFloat))
                {
                throw ETSError("���� �� ����� ������ ���� ������ ���� �� ����� �� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }
          if (ClickedItem->Tag == route_sogl  && !(m_dm->m_Q_docsPRICE_BEF_ACT_ON_CARD->IsNull) && (m_dm->m_Q_docsACT_PRICE->AsFloat >= m_dm->m_Q_docsPRICE_BEF_ACT_ON_CARD->AsFloat))
                {
                throw ETSError("���� �� ����� ������ ���� ������ ���� �� ����� �� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }

          if (fldr_id == m_dm->FldrNew && ClickedItem->Tag != route_cansel && (m_dm->m_Q_docsBEGIN_DATE->AsDateTime > m_dm->m_Q_docsEND_DATE->AsDateTime))
                {
                throw ETSError("���� ��������� ����� ������ ���� ������ ���� ������ � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                }

         //�������� ��� ��
           if (m_dm->m_Q_docsGM->AsInteger!=0 && ClickedItem->Tag != route_cansel)
                 {
                 if (m_dm->m_Q_docsPRICE_BEF_ACT->IsNull) throw ETSError("����� ��� ���� ��. ������� ���� ��� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                 }
           //�������� ��� ��, ����
           if (m_dm->m_Q_docsSM_MINI->AsInteger!=0 && ClickedItem->Tag != route_cansel)
                 {

                 if (m_dm->m_Q_docsPRICE_BEF_ACT_ON_CARD->IsNull) throw ETSError("����� ��� ���� �� ��� ����. ������� ���� �� ����� �� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));

                 }
                // ������ ����������� ���� ���������� � �����
                int route_new_act = getRouteId(m_dm->FldrNew, m_dm->FldrAct);
                int route_sogl_act = getRouteId(m_dm->FldrSogl, m_dm->FldrAct);
                int route_utv_act = getRouteId(m_dm->FldrUtv, m_dm->FldrAct);
                if (route_id == route_new_act || route_id == route_sogl_act || route_id == route_utv_act)
          {
                    if (m_dm->m_Q_docsACT_PRICE->IsNull)
                        {
                          throw ETSError("�� ������� ���� �� ����� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger));
                        }
                    if (m_dm->m_Q_chk_nmcl->Active)  m_dm->m_Q_chk_nmcl->Close();
                    m_dm->m_Q_chk_nmcl->Open();
                    if (m_dm->m_Q_chk_nmclC->AsInteger > 0)
                        {
                           if( Application->MessageBox(AnsiString("�� ������� � ��������� ID="+IntToStr(m_dm->m_Q_docsDOC_ID->AsInteger)+" ������� �������������� ���������. ����������?").c_str(),
                                            Application->Title.c_str(),
                                            MB_ICONQUESTION | MB_YESNO) == IDNO )
                         return;
                         }
          }

                // ������ �����������, ���� ���������� � �����
                int route_new_cans = getRouteId(m_dm->FldrNew, m_dm->FldrCansel);
                int route_sogl_cans = getRouteId(m_dm->FldrSogl, m_dm->FldrCansel);
                int route_utv_cans = getRouteId(m_dm->FldrUtv, m_dm->FldrCansel);
                if (route_id == route_new_cans || route_id == route_sogl_cans || route_id == route_utv_cans )
                {
                        if (m_dm->m_Q_reason_rejection->Active) m_dm->m_Q_reason_rejection->Close();
                        m_dm->m_Q_reason_rejection->Open();
                        m_dm->m_Q_reason_rejection->Edit();

                        if (reason_rejection=="") {
                        TSExecDBEditDlg(this,"������� ������ �� ����������",m_dm->m_Q_reason_rejection,"NOTE",500);
                        reason_rejection=m_dm->m_Q_reason_rejectionNOTE->AsString;
                        }
                        

                        if (reason_rejection!="")
                        {
                        m_dm->m_Q_edit_reason_rejection->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_docsDOC_ID->AsInteger;
                        m_dm->m_Q_edit_reason_rejection->ParamByName("NOTE")->AsString = reason_rejection;
                        m_dm->m_Q_edit_reason_rejection->ExecSQL();
                        m_dm->RefreshListDataSets();
                        }
                        else return;

                 }


               // ������ �����������, ���� ���������� ������� �� ������������
                int route_utv_sogl = getRouteId(m_dm->FldrUtv, m_dm->FldrSogl);
                if (route_id == route_utv_sogl)
                {
                  if (m_dm->m_Q_reject_zam_kd->Active) m_dm->m_Q_reject_zam_kd->Close();
                  m_dm->m_Q_reject_zam_kd->Open();
                  m_dm->m_Q_reject_zam_kd->Edit();

                  if (reject_zam_kd=="") {
                  TSExecDBEditDlg(this,"������� ���������� �� ����������",m_dm->m_Q_reject_zam_kd,"NOTE",500);
                  reject_zam_kd=m_dm->m_Q_reject_zam_kdNOTE->AsString;
                  }

                  if (reject_zam_kd!="")
                     {
                      m_dm->m_Q_edit_reject_zam_ld->ParamByName("DOC_ID")->AsInteger = m_dm->m_Q_docsDOC_ID->AsInteger;
                      m_dm->m_Q_edit_reject_zam_ld->ParamByName("NOTE")->AsString = reject_zam_kd;
                      m_dm->m_Q_edit_reject_zam_ld->ExecSQL();
                      m_dm->RefreshListDataSets();

                     }
                  else return;

                }

           try {
               m_dm->ChangeFldr(ClickedItem->Tag,m_dm->m_Q_docsDOC_ID->AsInteger,ClickedItem->Tag);
             } catch (char error) {
               ShowMessage(error);
            return;}
          m_dm->m_Q_docs->Next();
        }

         m_dm->RefreshListDataSets(true);
         m_dm->m_Q_list->Locate(m_dm->m_Q_listID->FieldName,doc_id,TLocateOptions());
    }   
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::ShowFldrMenuPrev()
{
    //���������� ������ �������� ����� �� ����������
    if (m_dm->m_Q_folders_prev_all->Active)
        m_dm->m_Q_folders_prev_all->Close();
    m_PM_fldr_prev->Items->Clear();
    m_dm->m_Q_folders_prev_all->Open();
  /*  if (!m_dm->m_Q_folders_prev_all->Eof)
        m_RSB_fldr_prev->Caption = m_dm->m_Q_folders_prev_allNAME->AsString;

    m_RSB_fldr_prev->Enabled =  !m_dm->m_Q_folders_prev_all->IsEmpty(); */

    while (!m_dm->m_Q_folders_prev_all->Eof)
    {
       TMenuItem *NewItem = new TMenuItem(m_PM_fldr_prev);
       NewItem->Caption = m_dm->m_Q_folders_prev_allNAME->AsString;
       NewItem->Tag = m_dm->m_Q_folders_prev_allID->AsInteger;
       NewItem->ImageIndex = 15;
       NewItem->Enabled = (m_dm->m_Q_folders_prev_allENABLED->AsInteger == 1);
       NewItem->OnClick = PMFldrNextClick;
       m_PM_fldr_prev->Items->Add(NewItem);
       m_dm->m_Q_folders_prev_all->Next();
    }

    m_TB_prev->AutoSize = false;
    m_TB_prev->AutoSize = true;
    m_dm->m_Q_folders_prev->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFDocDiscGoodsList::m_PM_fldr_prevPopup(TObject *Sender)
{
m_dm->SqFldr = m_dm->FillTmpReportParamValues(m_DBG_list, "DOC_FLDR_ID");
  ShowFldrMenuPrev();
}
//---------------------------------------------------------------------------

void __fastcall TFDocDiscGoodsList::m_PM_fldr_nextPopup(TObject *Sender)
{
  m_dm->SqFldr = m_dm->FillTmpReportParamValues(m_DBG_list, "DOC_FLDR_ID");
  ShowFldrMenu();
}
//---------------------------------------------------------------------------
int __fastcall TFDocDiscGoodsList::getRouteId(int p_src_fldr_id, int p_dest_fldr_id)

{
  if (m_dm->m_Q_route->Active) m_dm->m_Q_route->Close();
  m_dm->m_Q_route->ParamByName("SRC_FLDR_ID")->AsInteger = p_src_fldr_id;
  m_dm->m_Q_route->ParamByName("DEST_FLDR_ID")->AsInteger = p_dest_fldr_id;
  m_dm->m_Q_route->Open();
  return m_dm->m_Q_routeROUTE_ID->AsInteger;
}
//---------------------------------------------------------------------------


