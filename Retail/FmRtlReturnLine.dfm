inherited FRtlReturnLine: TFRtlReturnLine
  Left = 611
  Top = 398
  Caption = '�������'
  ClientHeight = 479
  ClientWidth = 732
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 429
    Width = 732
    TabOrder = 3
    inherited m_TB_main_control_buttons_dialog: TToolBar
      Left = 538
    end
    inherited m_TB_main_control_buttons_sub_item: TToolBar
      Left = 441
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 460
    Width = 732
  end
  inherited m_P_main_top: TPanel
    Width = 732
    TabOrder = 0
    inherited m_P_tb_main_add: TPanel
      Left = 685
    end
    inherited m_P_tb_main: TPanel
      Width = 685
      inherited m_TB_main: TToolBar
        Width = 685
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 256
    Width = 732
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 12
      Width = 59
      Height = 13
      Caption = '����������'
    end
    object DBEdit1: TDBEdit
      Left = 88
      Top = 8
      Width = 89
      Height = 21
      DataField = 'QUANTITY'
      DataSource = m_DS_line
      TabOrder = 0
    end
  end
  object m_DBG_lines: TMLDBGrid [4]
    Left = 0
    Top = 26
    Width = 732
    Height = 230
    Align = alClient
    DataSource = m_DS_lines
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    KeyColumnFind = -1
    ValueColumnFind = -1
    Columns = <
      item
        FieldName = 'LINE_ID'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'ASSORT_NAME'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'BAR_CODE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'OUT_PRICE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'QUANTITY'
        Title.TitleButton = True
        Footers = <>
      end>
  end
  object Panel2: TPanel [5]
    Left = 0
    Top = 26
    Width = 585
    Height = 263
    BevelOuter = bvNone
    TabOrder = 5
    object Label2: TLabel
      Left = 48
      Top = 20
      Width = 31
      Height = 13
      Caption = '�����'
    end
    object Label3: TLabel
      Left = 12
      Top = 52
      Width = 67
      Height = 13
      Caption = '�����������'
    end
    object Label4: TLabel
      Left = 16
      Top = 84
      Width = 59
      Height = 13
      Caption = '����������'
    end
    object MLLovListView1: TMLLovListView
      Left = 192
      Top = 16
      Width = 369
      Height = 21
      Lov = MLLov1
      UpDownWidth = 369
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 0
      ParentColor = False
    end
    object MLLovListView2: TMLLovListView
      Left = 192
      Top = 48
      Width = 369
      Height = 21
      Lov = MLLov2
      UpDownWidth = 369
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 1
      ParentColor = False
    end
    object MLDBPanel1: TMLDBPanel
      Left = 88
      Top = 16
      Width = 89
      Height = 21
      DataField = 'NMCL_ID'
      DataSource = m_DS_line
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 2
    end
    object MLDBPanel2: TMLDBPanel
      Left = 88
      Top = 48
      Width = 89
      Height = 21
      DataField = 'ASSORT_ID'
      DataSource = m_DS_line
      BevelOuter = bvLowered
      BorderWidth = 1
      Color = clWhite
      UseDockManager = True
      ParentColor = False
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Left = 88
      Top = 80
      Width = 89
      Height = 21
      DataField = 'QUANTITY'
      DataSource = m_DS_line
      TabOrder = 4
    end
  end
  object m_P_lines_control: TPanel [6]
    Left = 0
    Top = 396
    Width = 732
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 6
    object m1: TRxSpeedButton
      Left = 0
      Top = 2
      Width = 97
      Height = 25
      Caption = '    ...'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00DDDD
        DDDDDDDDDD0900DDDDDDDDDDDD099900DDDD00000009999900DD099999999999
        990000000009999900DDDDDDDD099900DDDDDDDDDD0900DDDDDDDDDDDD00DDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      Layout = blGlyphLeft
    end
    object m_TB_lines_control_button: TToolBar
      Left = 0
      Top = 0
      Width = 97
      Height = 33
      Align = alLeft
      AutoSize = True
      ButtonHeight = 25
      Caption = 'm_TB_lines_control_button'
      EdgeBorders = []
      TabOrder = 0
      object m_SBTN_delete: TSpeedButton
        Left = 0
        Top = 2
        Width = 97
        Height = 25
        Action = m_ACT_del
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF000000
          0000C0C0C0000000800000008000000080000000800000008000000080000000
          80000000800000008000000080000000800000000000FF00FF00FF00FF000000
          0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
          FF000000FF000000FF000000FF000000800000000000FF00FF00FF00FF000000
          0000C0C0C0000000FF000000FF000000FF000000FF000000FF000000FF000000
          FF000000FF000000FF000000FF000000800000000000FF00FF00FF00FF000000
          0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C00000000000FF00FF00FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  object m_DBG_alco: TMLDBGrid [7]
    Left = 0
    Top = 291
    Width = 732
    Height = 105
    TabStop = False
    Align = alBottom
    DataSource = m_DS_alc
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    Columns = <
      item
        FieldName = 'SUB_LINE_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'PDF_BAR_CODE'
        Title.TitleButton = True
        Width = 120
        Footers = <>
      end
      item
        FieldName = 'EGAIS_PRICE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'VOLUME'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'PRICE_MRZ'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'IS_FULL'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'CHECK_NUM'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'REPORT_NUM'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'INN'
        Title.TitleButton = True
        Width = 80
        Footers = <>
      end
      item
        FieldName = 'KPP'
        Title.TitleButton = True
        Width = 80
        Footers = <>
      end
      item
        FieldName = 'ALC_CODE'
        Title.TitleButton = True
        Width = 104
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    inherited m_ACT_insert: TAction
      Enabled = False
      Visible = False
    end
    object m_ACT_del: TAction
      Caption = '�������'
      OnExecute = m_ACT_delExecute
      OnUpdate = m_ACT_delUpdate
    end
  end
  object m_DS_lines: TDataSource
    DataSet = DRtlReturns.m_Q_sale_lines
    Left = 16
    Top = 40
  end
  object m_DS_line: TDataSource
    DataSet = DRtlReturns.m_Q_line
    Left = 16
    Top = 112
  end
  object m_DS_nmcls: TDataSource
    DataSet = DRtlReturns.m_Q_nmcls
    Left = 152
    Top = 42
  end
  object m_DS_assorts: TDataSource
    DataSet = DRtlReturns.m_Q_assorts
    Left = 152
    Top = 90
  end
  object MLLov1: TMLLov
    DataFieldKey = 'NMCL_ID'
    DataFieldValue = 'NMCL_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'NMCL_ID'
    DataFieldValues = 'NMCL_NAME'
    DataSourceList = m_DS_nmcls
    AutoOpenList = True
    Left = 200
    Top = 42
  end
  object MLLov2: TMLLov
    DataFieldKey = 'ASSORT_ID'
    DataFieldValue = 'ASSORT_NAME'
    DataSource = m_DS_line
    DataFieldKeys = 'ASSORT_ID'
    DataFieldValues = 'ASSORT_NAME'
    DataSourceList = m_DS_assorts
    AutoOpenList = True
    Left = 200
    Top = 90
  end
  object m_BCS_line: TDLBCScaner
    Tick = 50
    BeginKey = 2
    EndKey = 3
    OnBegin = m_BCS_lineBegin
    OnEnd = m_BCS_lineEnd
    Left = 287
    Top = 3
  end
  object m_DS_alc: TDataSource
    DataSet = DRtlReturns.m_Q_alc
    Left = 40
    Top = 312
  end
end
