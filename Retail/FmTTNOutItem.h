//---------------------------------------------------------------------------

#ifndef FmTTNOutItemH
#define FmTTNOutItemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DmTTNOut.h"
#include "FmTTNOutLine.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include "TSFMDOCUMENTWITHLINES.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "MLDBPanel.h"
#include "MLLov.h"
#include "MLLovView.h"
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Dialogs.hpp>
#include "DLBCScaner.h"
//---------------------------------------------------------------------------
class TFTTNOutItem : public TTSFDocumentWithLines
{
__published:
  TLabel *m_L_doc_number;
  TMLDBPanel *m_DBP_doc_number;
  TLabel *m_L_doc_date;
  TMLDBPanel *m_DBP_doc_date;
  TLabel *m_L_dest_org;
  TMLLovListView *m_LLV_dest_org;
  TMLLov *m_LOV_dest_org;
  TDataSource *m_DS_orgs;
  TLabel *m_L_amount_in;
  TMLDBPanel *m_DBP_amount_in;
  TLabel *m_L_amount_out;
  TMLDBPanel *m_DBP_amount_out;
  TLabel *m_L_note;
  TDBEdit *m_DBE_note;
        TDLBCScaner *m_BCS_item;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall m_ACT_editUpdate(TObject *Sender);
  void __fastcall m_ACT_appendExecute(TObject *Sender);
  void __fastcall m_ACT_appendUpdate(TObject *Sender);
  void __fastcall m_ACT_deleteUpdate(TObject *Sender);
        void __fastcall m_BCS_itemBegin(TObject *Sender);
        void __fastcall m_BCS_itemEnd(TObject *Sender);
private:
  TS_FORM_LINE_SHOW(TFTTNOutLine)
  TDTTNOut* m_dm;
  AnsiString __fastcall BeforeItemApply(TWinControl **p_focused);
  TMLGridOptions  m_temp_grid_options;
public:
  __fastcall TFTTNOutItem(TComponent *p_owner, TTSDDocumentWithLines *p_dm)
    : TTSFDocumentWithLines(p_owner,p_dm),
    m_dm(static_cast<TDTTNOut*>(p_dm))
    {};
};
//---------------------------------------------------------------------------
#endif
