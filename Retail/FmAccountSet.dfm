object FAccountSet: TFAccountSet
  Left = 380
  Top = 351
  ActiveControl = m_DBG_tp_outcomes
  BorderStyle = bsDialog
  Caption = '����� ��� ������'
  ClientHeight = 213
  ClientWidth = 723
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object m_P_main_control: TPanel
    Left = 0
    Top = 173
    Width = 723
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object m_P_main_control_buttons: TPanel
      Left = 508
      Top = 0
      Width = 215
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object m_BBTN_close: TBitBtn
        Left = 112
        Top = 8
        Width = 97
        Height = 25
        Hint = '�������� ���������'
        Cancel = True
        Caption = '������'
        ModalResult = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object m_BBTN_save: TBitBtn
        Left = 8
        Top = 8
        Width = 97
        Height = 25
        Hint = '������� ������'
        Caption = '�������'
        Default = True
        ModalResult = 1
        TabOrder = 0
        Glyph.Data = {
          4E010000424D4E01000000000000760000002800000012000000120000000100
          040000000000D800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
          FFFFFF000000FFFFF44FFFFFFFFFFF000000FFFF4224FFFFFFFFFF000000FFF4
          22224FFFFFFFFF000000FF42222224FFFFFFFF000000F4222A22224FFFFFFF00
          0000F222AFA2224FFFFFFF000000FA2AFFFA2224FFFFFF000000FFAFFFFFA222
          4FFFFF000000FFFFFFFFFA2224FFFF000000FFFFFFFFFFA2224FFF000000FFFF
          FFFFFFFA2224FF000000FFFFFFFFFFFFA2224F000000FFFFFFFFFFFFFA224F00
          0000FFFFFFFFFFFFFFA22F000000FFFFFFFFFFFFFFFAFF000000FFFFFFFFFFFF
          FFFFFF000000FFFFFFFFFFFFFFFFFF000000}
      end
    end
  end
  object m_DBG_tp_outcomes: TMLDBGrid
    Left = 0
    Top = 0
    Width = 723
    Height = 173
    Align = alClient
    DataSource = m_DS_rtl_bills
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    AllowedOperations = [alopUpdateEh, alopAppendEh]
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterColor = clWindow
    UseMultiTitle = True
    AutoFitColWidths = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    Columns = <
      item
        FieldName = 'DOC_ID'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        Title.TitleButton = True
        Width = 99
        Footers = <>
      end
      item
        FieldName = 'DOC_DATE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'ON_DATE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'AMOUNT'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'CNT_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'CNT_NAME'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'MNGR_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'MNGR_NAME'
        Title.TitleButton = True
        Width = 150
        Footers = <>
      end
      item
        FieldName = 'SRC_ORG_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'SRC_ORG_NAME'
        Title.TitleButton = True
        Width = 121
        Footers = <>
      end>
  end
  object m_DS_rtl_bills: TDataSource
    DataSet = DIncome.m_Q_rtl_bill_list
  end
end
