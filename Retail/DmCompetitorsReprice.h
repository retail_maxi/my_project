//---------------------------------------------------------------------------

#ifndef DmCompetitorsRepriceH
#define DmCompetitorsRepriceH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSDMDOCUMENTWITHLINES.h"
#include "MLCustomContainer.h"
#include "MLLogin.h"
#include "MLQuery.h"
#include "MLUpdateSQL.h"
#include "RxQuery.hpp"
#include "TSConnectionContainer.h"
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TDCompetitorsReprice : public TTSDDocumentWithLines
{
__published:	// IDE-managed Components
   TDateTimeField *m_Q_listISSUED_DATE;
   TStringField *m_Q_listDOC_NUMBER;
   TFloatField *m_Q_listORG_ID;
   TStringField *m_Q_listNOTE;
   TStringField *m_Q_listORG_NAME;
   TDateTimeField *m_Q_itemISSUED_DATE;
   TStringField *m_Q_itemDOC_NUMBER;
   TFloatField *m_Q_itemORG_ID;
   TStringField *m_Q_itemNOTE;
   TStringField *m_Q_itemORG_NAME;
   TFloatField *m_Q_lineNMCL_ID;
   TStringField *m_Q_lineNMCL_NAME;
   TFloatField *m_Q_lineNEW_PRICE;
   TStringField *m_Q_lineNOTE;
   TMLQuery *m_Q_comp_one;
   TMLQuery *m_Q_competitors;
   TFloatField *m_Q_competitorsID;
   TFloatField *m_Q_competitorsORG_ID;
   TStringField *m_Q_competitorsNAME;
   TMLQuery *m_Q_lines_start;
   TMLQuery *m_Q_lines_end;
   TFloatField *m_Q_lines_startID;
   TFloatField *m_Q_lines_startLINE_ID;
   TFloatField *m_Q_lines_startNMCL_ID;
   TStringField *m_Q_lines_startNMCL_NAME;
   TStringField *m_Q_lines_startMU_NAME;
   TFloatField *m_Q_lines_startNEW_PRICE;
   TFloatField *m_Q_linesNMCL_ID;
   TStringField *m_Q_linesNMCL_NAME;
   TStringField *m_Q_linesMU_NAME;
   TFloatField *m_Q_linesNEW_PRICE;
   TStringField *m_Q_comp_oneCOMP_NAME;
   TFloatField *m_Q_lineCOMP_PRICE;
   TFloatField *m_Q_lineCOMP_ID;
   TFloatField *m_Q_linesOUT_PRICE;
   TFloatField *m_Q_linesIN_PRICE;
   TFloatField *m_Q_lines_startIN_PRICE;
   TFloatField *m_Q_lines_startOUT_PRICE;
   TMLQuery *m_Q_org;
   TFloatField *m_Q_orgID;
   TStringField *m_Q_orgNAME;
   void __fastcall m_Q_itemAfterInsert(TDataSet *DataSet);
   void __fastcall m_Q_itemBeforeClose(TDataSet *DataSet);
   void __fastcall m_Q_lineAfterInsert(TDataSet *DataSet);
   void __fastcall m_Q_lineBeforeOpen(TDataSet *DataSet);
   void __fastcall m_Q_comp_oneBeforeOpen(TDataSet *DataSet);
   void __fastcall m_Q_competitorsBeforeOpen(TDataSet *DataSet);
   void __fastcall m_Q_linesBeforeOpen(TDataSet *DataSet);
   void __fastcall m_Q_listBeforeOpen(TDataSet *DataSet);
private:	// User declarations
  int m_cur_comp_id;
  int m_home_org_id;
protected:
  TS_DOC_LINE_SQ_NAME("SQ_DOC_COMP_REPRICE_ITEMS")
public:		// User declarations
   __fastcall TDCompetitorsReprice(TComponent* p_owner,AnsiString p_prog_id);
   __property int CurCompId = {read = m_cur_comp_id, write = m_cur_comp_id};
   void __fastcall SetBeginEndDate(const TDateTime &p_begin_date, const TDateTime &p_end_date);
};
//---------------------------------------------------------------------------
#endif
