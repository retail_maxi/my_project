//---------------------------------------------------------------------------

#ifndef ProdTranslateRKUImplH
#define ProdTranslateRKUImplH
//---------------------------------------------------------------------------

#include "TSModuleTemplate.h"
#include "DmProdTranslateRKU.h"
#include "FmProdTranslateRKUList.h"
#include "FmProdTranslateRKUItem.h"
#include "TSRetail_TLB.h"
//---------------------------------------------------------------------------

#undef TS_ADDITION_INTERFACE_DECLARE
#define TS_ADDITION_INTERFACE_DECLARE TS_REFBOOK_INTERFACE_DECLARE
#undef TS_ADDITION_INTERFACE_ENTRY
#define TS_ADDITION_INTERFACE_ENTRY TS_REFBOOK_INTERFACE_ENTRY
#undef TS_ADDITION_INTERFACE_IMPL
#define TS_ADDITION_INTERFACE_IMPL TS_REFBOOK_INTERFACE_IMPL

TS_FORM_LIST_COCLASS_IMPL(ProdTranslateRKU)
//---------------------------------------------------------------------------

#endif
//---------------------------------------------------------------------------
