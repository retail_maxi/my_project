inherited FInvActList: TFInvActList
  Left = 750
  Top = 249
  Width = 1016
  Height = 630
  Caption = 'FInvActList'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 532
    Width = 1000
    Height = 41
    object m_SBTN_create_for_group: TSpeedButton [0]
      Left = 2
      Top = 2
      Width = 143
      Height = 25
      Action = m_ACT_create_act_for_group
    end
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 903
      Height = 41
    end
    inherited m_TB_main_control_buttons_list: TToolBar
      Left = 515
      Height = 41
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 573
    Width = 1000
  end
  inherited m_DBG_list: TMLDBGrid
    Width = 1000
    Height = 506
    TitleFont.Name = 'MS Sans Serif'
    FooterRowCount = 1
    FooterFont.Name = 'MS Sans Serif'
    SumList.Active = True
    Columns = <
      item
        FieldName = 'ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_SRC_ORG_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_CREATE_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_COMP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_MODIFY_USER'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_STATUS_CHANGE_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_ID'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_FLDR_RIGHT'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUMBER'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DEF_START_DATE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'GR_DEP_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Visible = False
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SUMIN'
        PickList.Strings = ()
        Title.TitleButton = True
        Footer.ValueType = fvtSum
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'SUMOUT'
        PickList.Strings = ()
        Title.TitleButton = True
        Footer.ValueType = fvtSum
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE_NON_PREPARE'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 200
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MAX_ERR_A'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 50
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'MAX_ERR_B'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 50
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'NOTE_AUDITOR'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'REASON_NAME'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'DOC_NUM_RECALC'
        PickList.Strings = ()
        Title.TitleButton = True
        Width = 100
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'BONUS_COUNTER_AB'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end
      item
        FieldName = 'BONUS_AUDITOR_AB'
        PickList.Strings = ()
        Title.TitleButton = True
        KeyList.Strings = ()
        Footers = <>
      end>
  end
  inherited m_P_main_top: TPanel
    Width = 1000
    inherited m_P_tb_main: TPanel
      Width = 953
      inherited m_TB_main: TToolBar
        Width = 953
        object ToolButton1: TToolButton
          Left = 302
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 17
          Style = tbsSeparator
        end
        object m_DBLCB_organizations: TDBLookupComboBox
          Left = 310
          Top = 2
          Width = 185
          Height = 21
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = m_DS_orgs
          TabOrder = 0
          OnClick = m_DBLCB_organizationsClick
        end
        object ToolButton2: TToolButton
          Left = 495
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 18
          Style = tbsSeparator
        end
        object m_SBTN_set_date: TSpeedButton
          Left = 503
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 953
    end
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'InvAct'
      Caption = 'm_ACT_set_begin_end_date'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 40053
      EndDate = 40053
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
    object m_ACT_create_act_for_group: TAction
      Category = 'InvAct'
      Caption = '��� �� ������ ���������'
      OnExecute = m_ACT_create_act_for_groupExecute
      OnUpdate = m_ACT_create_act_for_groupUpdate
    end
  end
  inherited m_DS_list: TDataSource
    DataSet = DInvAct.m_Q_list
    Left = 24
    Top = 64
  end
  object m_DS_orgs: TDataSource
    DataSet = DInvAct.m_Q_orgs
    Left = 256
    Top = 8
  end
end
