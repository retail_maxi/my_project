//---------------------------------------------------------------------------

#ifndef FmAssortExceptionListH
#define FmAssortExceptionListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TSFMREFBOOK.h"
#include "DmAssortException.h"
#include "TSFmRefbook.h"
#include "DBGridEh.hpp"
#include "MLActionsControls.h"
#include "MLDBGrid.h"
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBActns.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFAssortExceptionList : public TTSFRefbook
{
__published:	// IDE-managed Components
private:	// User declarations
        TDAssortException *m_dm;

public:		// User declarations
        __fastcall TFAssortExceptionList(TComponent* p_owner, TTSDRefbook *p_dm_refbook);
};
//---------------------------------------------------------------------------
#endif
