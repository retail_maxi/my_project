inherited DAlcoJournalNew: TDAlcoJournalNew
  OldCreateOrder = False
  Left = 795
  Top = 347
  Width = 741
  inherited m_DB_main: TDatabase
    AliasName = 'ComLabs'
    Params.Strings = (
      'USER NAME=robot'
      'PASSWORD=rbt')
  end
  inherited m_Q_main: TMLQuery
    BeforeOpen = m_Q_mainBeforeOpen
    SQL.Strings = (
      'select'
      '    ready_tbl.num,'
      '    trunc( ready_tbl.check_date ) as check_date,'
      '    ready_tbl.order_by_date,'
      #9'ready_tbl.bar_code,'
      '    ready_tbl.nmcl_id,'
      '    ready_tbl.nmcl_name,'
      '    ready_tbl.alco_code,'
      '    ready_tbl.capacity,'
      '    ready_tbl.quantity'
      'from'
      '('
      '    select'
      '        tbl.num,'
      '        tbl.check_date,'
      '        tbl.order_by_date,'
      '        case'
      
        '            when lag( tbl.bar_code ) over ( order by order_by_da' +
        'te, check_date nulls last, nmcl_name ) is NULL then tbl.bar_code'
      '            else NULL'
      '        end bar_code,'
      '        tbl.nmcl_id,'
      '        tbl.nmcl_name,'
      '        tbl.alco_code,'
      '        tbl.capacity,'
      '        tbl.quantity'
      '        from'
      '        ('
      '                  select'
      
        '                         row_number() over (order by trunc( rc.c' +
        'heck_date ), rc.check_date, ni.name) as num,'
      '                         rc.check_date,'
      
        '                         trunc( rc.check_date ) as order_by_date' +
        ','
      '                         NULL as bar_code,'
      '                         ni.id as nmcl_id,'
      '                         ni.name as nmcl_name,'
      '                         aci.code as alco_code,'
      '                         nmcli.capacity,'
      '                         rsl.quantity'
      '                    from '
      '                        rtl_sale_lines rsl,'
      '                        rtl_checks rc,'
      '                        shop_departments sd,'
      '                        nomenclature_items ni,'
      
        '                        (select id from products_types start wit' +
        'h id = 723 connect by prior id = master_prod_id) alc,'
      '                        documents d,'
      '                        nmcl_info nmcli,'
      '                        nmcl_alco_class nac,'
      
        '                        alco_class_items aci                    ' +
        ' '
      '                   where '
      '                        rc.sale_doc_id = rsl.doc_id and'
      '                        rc.check_num = rsl.check_num and'
      '                        sd.id = rsl.shop_dep_id and'
      
        '                        sd.taxpayer_id in (2000906, 150, 91, 152' +
        ', 142, 145) and'
      '                        sd.taxpayer_id = :TAX_PAYER and'
      '                        ni.id = rsl.nmcl_id and'
      '                        ni.prod_type_id = alc.id and'
      '                        d.id = rsl.doc_id and'
      '                        nmcli.nmcl_id = ni.id and'
      '                        nac.nmcl_id = ni.id and'
      '                        aci.id = nac.alco_class_id and'
      
        '                        aci.version_id = ( select acv.ID from al' +
        'co_class_versions acv where rc.check_date between acv.begin_date' +
        ' and nvl( acv.end_date, rc.check_date + 1 ) ) and'
      
        '                        d.fldr_id in(fnc_get_constant('#39'FLDR_RTL_' +
        'SALES_CNF'#39'), fnc_get_constant('#39'FLDR_RTL_SALES_ARC'#39')) and'
      
        '                        rc.check_date between :BEGIN_DATE and :E' +
        'ND_DATE + 1 - ( 1 / 24 / 60 / 60 )'
      '                    '
      '                     UNION ALL'
      '                     '
      '                   select'
      '                         NULL  as row1,'
      '                         NULL as check_date,'
      
        '                         trunc( rc.check_date ) as order_by_date' +
        ','
      '                         '#39'�����'#39' as bar_code,'
      '                         ni.id as nmcl_id,'
      '                         ni.name as nmcl_name,'
      '                         aci.code as alco_code,'
      '                         NULL as capacity,'
      '                         sum( rsl.quantity ) as quantity'
      '                    from '
      '                        rtl_sale_lines rsl,'
      '                        rtl_checks rc,'
      '                        shop_departments sd,'
      '                        nomenclature_items ni,'
      
        '                        (select id from products_types start wit' +
        'h id = 723 connect by prior id = master_prod_id) alc,'
      '                        documents d,'
      '                        nmcl_info nmcli,'
      '                        nmcl_alco_class nac,'
      
        '                        alco_class_items aci                    ' +
        ' '
      '                   where '
      '                        rc.sale_doc_id = rsl.doc_id and'
      '                        rc.check_num = rsl.check_num and'
      '                        sd.id = rsl.shop_dep_id and'
      
        '                        sd.taxpayer_id in (2000906, 150, 91, 152' +
        ', 142, 145) and'
      '                        sd.taxpayer_id = :TAX_PAYER and'
      '                        ni.id = rsl.nmcl_id and'
      '                        ni.prod_type_id = alc.id and'
      '                        d.id = rsl.doc_id and'
      '                        nmcli.nmcl_id = ni.id and'
      '                        nac.nmcl_id = ni.id and'
      '                        aci.id = nac.alco_class_id and'
      
        '                        aci.version_id = ( select acv.ID from al' +
        'co_class_versions acv where rc.check_date between acv.begin_date' +
        ' and nvl( acv.end_date, rc.check_date + 1 ) ) and'
      
        '                        d.fldr_id in(fnc_get_constant('#39'FLDR_RTL_' +
        'SALES_CNF'#39'), fnc_get_constant('#39'FLDR_RTL_SALES_ARC'#39')) and'
      
        '                        rc.check_date between :BEGIN_DATE and :E' +
        'ND_DATE + 1 - ( 1 / 24 / 60 / 60 )'
      
        '                   group by ni.id, ni.name, aci.code, trunc( rc.' +
        'check_date )'
      '        ) tbl'
      '    order by order_by_date, check_date nulls last, nmcl_name'
      ') ready_tbl'
      ''
      ' '
      ' '
      ' '
      ' '
      ' ')
    UpdateObject = nil
    ParamData = <
      item
        DataType = ftInteger
        Name = 'TAX_PAYER'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'begin_date'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'end_date'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'TAX_PAYER'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'END_DATE'
        ParamType = ptInput
      end>
    object m_Q_mainNUM: TFloatField
      DisplayLabel = '� �/�'
      FieldName = 'NUM'
      Origin = 'rownum'
    end
    object m_Q_mainCHECK_DATE: TDateTimeField
      DisplayLabel = '���� ��������� �������'
      FieldName = 'CHECK_DATE'
      Origin = 'tbl.check_date'
    end
    object m_Q_mainBAR_CODE: TStringField
      DisplayLabel = '��������� ���'
      FieldName = 'BAR_CODE'
      Origin = 'tbl.bar_code'
      Size = 30
    end
    object m_Q_mainNMCL_ID: TFloatField
      FieldName = 'NMCL_ID'
      Origin = 'tbl.nmcl_id'
    end
    object m_Q_mainNMCL_NAME: TStringField
      DisplayLabel = '������������ ���������'
      FieldName = 'NMCL_NAME'
      Origin = 'tbl.nmcl_name'
      Size = 100
    end
    object m_Q_mainALCO_CODE: TStringField
      DisplayLabel = '��� ���� ���������'
      FieldName = 'ALCO_CODE'
      Origin = 'aci.code'
      Size = 10
    end
    object m_Q_mainCAPACITY: TFloatField
      DisplayLabel = '������� (�)'
      FieldName = 'CAPACITY'
      Origin = 'tbl.capacity'
    end
    object m_Q_mainQUANTITY: TFloatField
      DisplayLabel = '���������� ����'
      FieldName = 'QUANTITY'
      Origin = 'tbl.quantity'
    end
    object m_Q_mainORDER_BY_DATE: TDateTimeField
      FieldName = 'ORDER_BY_DATE'
    end
  end
  object m_Q_taxpayer: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select txp.id, txp.cnt_id, txp.name, cntrs.inn, cc.kpp'
      '    from '
      '        taxpayer txp,'
      '        companies_contractors cc,'
      '        contractors cntrs'
      '    where '
      '        txp.id in (2000906,150,91,152,142,145) and'
      '        cc.cnt_id = txp.cnt_id and'
      '        cntrs.id = txp.cnt_id'
      '%WHERE_CLAUSE'
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end>
    Left = 32
    Top = 200
    object m_Q_taxpayerID: TFloatField
      DisplayWidth = 50
      FieldName = 'ID'
      Origin = 'txp.id'
    end
    object m_Q_taxpayerNAME: TStringField
      DisplayLabel = '������������'
      DisplayWidth = 200
      FieldName = 'NAME'
      Origin = 'txp.name'
      Size = 200
    end
    object m_Q_taxpayerCNT_ID: TFloatField
      DisplayWidth = 60
      FieldName = 'CNT_ID'
      Origin = 'txp.cnt_id'
      Visible = False
    end
    object m_Q_taxpayerINN: TStringField
      DisplayWidth = 100
      FieldName = 'INN'
      Origin = 'cntrs.inn'
      Visible = False
      Size = 100
    end
    object m_Q_taxpayerKPP: TStringField
      DisplayWidth = 100
      FieldName = 'KPP'
      Origin = 'cc.kpp'
      Visible = False
      Size = 100
    end
  end
  object m_Q_taxpayer_fict: TMLQuery
    CachedUpdates = True
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      
        'select 0 as id, rpad('#39' '#39',250) as name, rpad('#39' '#39',250) as inn, rpa' +
        'd('#39' '#39',250) as kpp'
      'from dual'
      ' ')
    UpdateObject = m_U_taxpayer_fict
    Macros = <>
    Left = 120
    Top = 200
    object m_Q_taxpayer_fictID: TFloatField
      DisplayWidth = 30
      FieldName = 'ID'
      Origin = 'id'
    end
    object m_Q_taxpayer_fictNAME: TStringField
      FieldName = 'NAME'
      Origin = 'name'
      Size = 250
    end
    object m_Q_taxpayer_fictINN: TStringField
      FieldName = 'INN'
      Origin = 'inn'
      Size = 250
    end
    object m_Q_taxpayer_fictKPP: TStringField
      FieldName = 'KPP'
      Origin = 'kpp'
      Size = 250
    end
  end
  object m_U_taxpayer_fict: TMLUpdateSQL
    Left = 216
    Top = 200
  end
  object m_Q_org: TMLQuery
    CachedUpdates = True
    BeforeOpen = m_Q_orgBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'select name from organizations '
      'where id = :ID')
    Macros = <>
    Left = 32
    Top = 264
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_orgNAME: TStringField
      FieldName = 'NAME'
      Origin = 'TSDBMAIN.ORGANIZATIONS.NAME'
      Size = 250
    end
  end
  object m_SP_fill_tmp_alco_class_item_v: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'GLOBALREF.PRC_FILL_TMP_ALCO_CLASS_ITEM_V'
    Left = 320
    Top = 136
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'P_BEGIN_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'P_END_DATE'
        ParamType = ptInput
      end>
  end
  object m_SP_fill_buf_alco_jour: TStoredProc
    DatabaseName = 'TSDBMain'
    StoredProcName = 'GLOBALREF.PRC_FILL_BUF_ALCO_JOUR'
    Left = 112
    Top = 264
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'P_DATE'
        ParamType = ptInput
      end>
  end
  object m_Q_address: TMLQuery
    BeforeOpen = m_Q_addressBeforeOpen
    DatabaseName = 'TSDBMain'
    SQL.Strings = (
      'SELECT oa.org_id,'
      '       oa.addr_type_id, '
      '       a_t.name AS addr_type_name,'
      '       oa.zip, '
      '       oa.place_id, '
      '       TRIM(RPAD(pt.name || '#39' '#39' || p.name,254)) AS place_name, '
      '       p.phone_code AS place_phone_code,'
      '       oa.country_id, '
      '       c.name AS country_name, '
      '       oa.street,'
      '       oa.building, '
      '       oa.ex_building, '
      '       oa.room, '
      '       oa.full_name,'
      
        '       (c.name || '#39', '#39' || TRIM(RPAD(pt.name || '#39' '#39' || p.name,254' +
        ')) || '#39', '#39' || oa.street || '#39', '#39' || oa.building || '#39' '#39' || oa.ex_b' +
        'uilding) as show_address'
      'FROM org_addresses oa,'
      '     address_type a_t,'
      '     countries c,'
      '     places p,'
      '     places_types pt'
      'WHERE oa.org_id = :ID'
      '      AND a_t.id = oa.addr_type_id'
      '      AND c.id (+) = oa.country_id'
      '      AND p.id (+) = oa.place_id'
      '      AND pt.id (+) = p.place_type_id'
      '%WHERE_CLAUSE'
      '%ORDER_EXPRESSION'
      ' '
      ' ')
    Macros = <
      item
        DataType = ftString
        Name = 'WHERE_CLAUSE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ORDER_EXPRESSION'
        ParamType = ptInput
      end>
    Left = 216
    Top = 264
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    object m_Q_addressORG_ID: TFloatField
      FieldName = 'ORG_ID'
      Origin = 'OA.ORG_ID'
      Visible = False
    end
    object m_Q_addressADDR_TYPE_ID: TFloatField
      FieldName = 'ADDR_TYPE_ID'
      Origin = 'OA.ADDR_TYPE_ID'
      Visible = False
    end
    object m_Q_addressADDR_TYPE_NAME: TStringField
      DisplayLabel = '��� ������'
      FieldName = 'ADDR_TYPE_NAME'
      Origin = 'A_T.NAME'
      Size = 100
    end
    object m_Q_addressZIP: TStringField
      DisplayLabel = '�������� ������'
      FieldName = 'ZIP'
      Origin = 'OA.ZIP'
      FixedChar = True
      Size = 6
    end
    object m_Q_addressPLACE_ID: TFloatField
      FieldName = 'PLACE_ID'
      Origin = 'OA.PLACE_ID'
      Visible = False
    end
    object m_Q_addressPLACE_NAME: TStringField
      DisplayLabel = '��������������'
      FieldName = 'PLACE_NAME'
      Origin = 'TRIM(RPAD(PT.NAME || '#39' '#39' || P.NAME,254))'
      Size = 254
    end
    object m_Q_addressPLACE_PHONE_CODE: TStringField
      DisplayLabel = '��� ������'
      FieldName = 'PLACE_PHONE_CODE'
      Origin = 'P.PHONE_CODE'
      Size = 10
    end
    object m_Q_addressCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Origin = 'OA.COUNTRY_ID'
      Visible = False
    end
    object m_Q_addressCOUNTRY_NAME: TStringField
      DisplayLabel = '������'
      FieldName = 'COUNTRY_NAME'
      Origin = 'C.NAME'
      Size = 50
    end
    object m_Q_addressSTREET: TStringField
      DisplayLabel = '�����'
      FieldName = 'STREET'
      Origin = 'OA.STREET'
      Size = 100
    end
    object m_Q_addressBUILDING: TFloatField
      DisplayLabel = '���'
      FieldName = 'BUILDING'
      Origin = 'OA.BUILDING'
    end
    object m_Q_addressEX_BUILDING: TStringField
      DisplayLabel = '����� ����'
      FieldName = 'EX_BUILDING'
      Origin = 'OA.EX_BUILDING'
      FixedChar = True
      Size = 1
    end
    object m_Q_addressROOM: TFloatField
      DisplayLabel = '��������'
      FieldName = 'ROOM'
      Origin = 'OA.ROOM'
    end
    object m_Q_addressFULL_NAME: TStringField
      DisplayLabel = '������ ���'
      FieldName = 'FULL_NAME'
      Origin = 'OA.FULL_NAME'
      Size = 250
    end
    object m_Q_addressSHOW_ADDRESS: TMemoField
      FieldName = 'SHOW_ADDRESS'
      BlobType = ftMemo
      Size = 452
    end
  end
end
