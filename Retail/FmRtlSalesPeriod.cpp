//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FmRtlSalesPeriod.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

__fastcall TFRtlSalesPeriod::TFRtlSalesPeriod(TComponent* p_owner, TDRtlSales* p_dm)
    :TTSFDialog(p_owner,p_dm),
    m_dm(p_dm)
{

}

void __fastcall TFRtlSalesPeriod::FormShow(TObject *Sender)
{
  m_DTP_DateFrom->DateTime = m_dm->BeginDate;
  m_DTP_TimeFrom->DateTime = m_dm->BeginDate;
  m_DTP_DateTo->DateTime = m_dm->EndDate;
  m_DTP_TimeTo->DateTime = m_dm->EndDate;
}
//---------------------------------------------------------------------------

void __fastcall TFRtlSalesPeriod::m_ACT_apply_updatesExecute(
      TObject *Sender)
{
  m_dm->SetBeginEndDate(int(m_DTP_DateFrom->DateTime) + m_DTP_TimeFrom->DateTime - int(m_DTP_TimeFrom->DateTime),
                        int(m_DTP_DateTo->DateTime) + m_DTP_TimeTo->DateTime - int(m_DTP_TimeTo->DateTime));
}
//---------------------------------------------------------------------------

