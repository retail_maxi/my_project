//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "DmMessagesRKU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

__fastcall TDMessagesRKU::TDMessagesRKU(TComponent* p_owner,
                                        AnsiString p_prog_id)
    : TTSDOperation(p_owner, p_prog_id),
    m_home_org_id(StrToInt64(GetVarValue("HOME_ORG_ID"))),
    m_doc_type_from_rku_id(StrToInt64(GetConstValue("DT_MESSAGES_FROM_RKU"))),
    m_doc_type_to_rku_id(StrToInt64(GetConstValue("DT_MESSAGES_TO_RKU"))),
    m_from_rku(GetSysRight(POLICY_OF_DOCUMENT_TYPE,"TSRetail.MessageFromRKU",TTSDocumentTypeAccess().AccessMask)),
    m_to_rku(GetSysRight(POLICY_OF_DOCUMENT_TYPE,"TSRetail.MessageToRKU",TTSDocumentTypeAccess().AccessMask))
{
  m_Q_main->ParamByName("DOC_TYPE_FROM_RKU")->AsInteger = m_doc_type_from_rku_id;
  m_Q_main->ParamByName("DOC_TYPE_TO_RKU")->AsInteger = m_doc_type_to_rku_id;
  m_Q_main->ParamByName("ACCESS_MASK")->AsString = TTSDocumentAccess().AccessMask;

  m_sel_doc_type = "WHERE id IN (";
  if (m_from_rku.CanInsert) m_sel_doc_type = m_sel_doc_type + IntToStr(m_doc_type_from_rku_id) + ",";
  if (m_to_rku.CanInsert)  m_sel_doc_type = m_sel_doc_type + IntToStr(m_doc_type_to_rku_id) + ",";
  m_sel_doc_type = m_sel_doc_type + "0)";

  m_Q_doc_ids->ParamByName("DOC_TYPE_FROM_RKU")->AsInteger = m_doc_type_from_rku_id;
  m_Q_doc_ids->ParamByName("DOC_TYPE_TO_RKU")->AsInteger = m_doc_type_to_rku_id;
  m_Q_doc_ids->ParamByName("ACCESS_MASK")->AsString = TTSDocumentAccess().AccessMask;

  m_Q_new_docs->ParamByName("DOC_TYPE_FROM_RKU")->AsInteger = m_doc_type_from_rku_id;
  m_Q_new_docs->ParamByName("DOC_TYPE_TO_RKU")->AsInteger = m_doc_type_to_rku_id;
  m_Q_new_docs->ParamByName("ACCESS_MASK")->AsString = TTSDocumentAccess().AccessMask;
  m_rec_id_list = "";
  m_last_id = 0;
  SetBeginEndDate(int(GetSysDate()) - 7,int(GetSysDate()));

  m_Q_orgs->Open();
  if (m_home_org_id == 3)
    SetOrgId(-1);
  else
    SetOrgId(m_home_org_id);
}
//---------------------------------------------------------------------------

__fastcall TDMessagesRKU::~TDMessagesRKU()
{
  if( m_Q_orgs->Active ) m_Q_orgs->Close();
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDMessagesRKU::GetBeginDate()
{
  return m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

TDateTime __fastcall TDMessagesRKU::GetEndDate()
{
  return m_Q_main->ParamByName("END_DATE")->AsDateTime;
}
//---------------------------------------------------------------------------

void __fastcall TDMessagesRKU::SetBeginEndDate(const TDateTime &p_begin_date,
                                             const TDateTime &p_end_date)
{
  if( (int(m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime) == int(p_begin_date)) &&
      (int(m_Q_main->ParamByName("END_DATE")->AsDateTime) == int(p_end_date)) ) return;

  m_Q_main->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_main->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  m_Q_doc_ids->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_doc_ids->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  m_Q_new_docs->ParamByName("BEGIN_DATE")->AsDateTime = int(p_begin_date);
  m_Q_new_docs->ParamByName("END_DATE")->AsDateTime = int(p_end_date);

  if( m_Q_main->Active ) RefreshDataSets();
  if (m_Q_doc_ids->Active) m_Q_doc_ids->Close();
  m_Q_doc_ids->Open();
  m_last_id = m_Q_doc_idsLAST_ID->AsInteger;   
  
}
//---------------------------------------------------------------------------

int __fastcall TDMessagesRKU::GetOrgId()
{
  return m_Q_main->ParamByName("ORG_ID")->AsInteger;
}
//---------------------------------------------------------------------------

void __fastcall TDMessagesRKU::SetOrgId(int p_value)
{
  if( GetOrgId() == p_value ) return;

  m_Q_main->ParamByName("ORG_ID")->AsInteger = p_value;

  if( m_Q_main->Active )
  {
    m_Q_main->Close();
    m_Q_main->Open();
  }
}
//---------------------------------------------------------------------------

bool __fastcall TDMessagesRKU::GetCanViewItem()
{
  return TTSDocumentAccess(m_Q_mainDOC_FLDR_RIGHT->AsString).CanSelect;
}
//---------------------------------------------------------------------------

bool __fastcall TDMessagesRKU::GetCanInsertItem()
{
  return m_from_rku.CanInsert || m_to_rku.CanInsert;
}
//---------------------------------------------------------------------------

bool __fastcall TDMessagesRKU::GetCanEditItem()
{
  return TTSDocumentAccess(m_Q_mainDOC_FLDR_RIGHT->AsString).CanUpdate &&
         (m_Q_mainDOC_STATUS->AsString != "D");
}
//---------------------------------------------------------------------------

bool __fastcall TDMessagesRKU::GetCanDeleteItem()
{
  return TTSDocumentAccess(m_Q_mainDOC_FLDR_RIGHT->AsString).CanDelete &&
         (m_Q_mainDOC_STATUS->AsString != "D");
}
//---------------------------------------------------------------------------



