inherited FAlcoJournalNew: TFAlcoJournalNew
  Left = 730
  Top = 260
  Width = 830
  Height = 635
  Caption = 'FAlcoJournalNew'
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited m_P_main_control: TPanel
    Top = 548
    Width = 814
    Height = 27
    inherited m_TB_main_control_buttons_custom: TToolBar
      Left = 717
      Height = 27
      inherited m_BBTN_close: TBitBtn
        Anchors = []
        ParentBiDiMode = False
      end
    end
    object m_TB_reload_list: TToolBar
      Left = 612
      Top = 0
      Width = 105
      Height = 27
      Align = alRight
      AutoSize = True
      ButtonHeight = 25
      EdgeBorders = []
      TabOrder = 1
      object m_SBTN_reload_list: TBitBtn
        Left = 0
        Top = 2
        Width = 97
        Height = 25
        Action = m_ACT_print_form
        Anchors = [akTop, akRight]
        Caption = '������'
        TabOrder = 0
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF00FF0000000000FF00FF0000000000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000FFFF0000FFFF0000FF
          FF00FF00FF00FF00FF00000000000000000000000000FF00FF0000000000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084848400848484008484
          8400FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF00FF00FF00FF000000000000000000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF00FF0000000000FF00FF000000000000000000FF00FF00FF00
          FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF0000000000FF00FF0000000000FF00FF0000000000FF00FF00FF00
          FF00FF00FF0000000000FFFFFF00000000000000000000000000000000000000
          0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
          FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000FFFFFF000000000000000000000000000000
          000000000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
          000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      end
      object btn1: TToolButton
        Left = 97
        Top = 2
        Width = 8
        Caption = 'btn1'
        Style = tbsSeparator
      end
    end
  end
  inherited m_SB_main: TStatusBar
    Top = 575
    Width = 814
    Height = 22
  end
  inherited m_P_main_top: TPanel
    Width = 814
    inherited m_P_tb_main: TPanel
      Width = 767
      inherited m_TB_main: TToolBar
        Width = 767
        Height = 24
        ButtonHeight = 22
        inherited m_SB_ods: TSpeedButton
          Height = 22
        end
        inherited m_SB_xlsx: TSpeedButton
          Height = 22
        end
        object ToolButton1: TToolButton
          Left = 294
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          ImageIndex = 13
          Style = tbsSeparator
        end
        object ToolButton2: TToolButton
          Left = 302
          Top = 2
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 14
          Style = tbsSeparator
        end
        object m_SB__SBTN_set_date: TSpeedButton
          Left = 310
          Top = 2
          Width = 152
          Height = 22
          Action = m_ACT_set_begin_end_date
        end
      end
    end
    inherited m_P_tb_main_add: TPanel
      Left = 767
    end
  end
  object m_P_3: TPanel [3]
    Left = 0
    Top = 26
    Width = 814
    Height = 92
    Align = alTop
    BevelOuter = bvNone
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    object m_L_2: TLabel
      Left = 88
      Top = 37
      Width = 21
      Height = 13
      Caption = '���'
      Layout = tlCenter
    end
    object m_L_taxpayer: TLabel
      Left = 10
      Top = 10
      Width = 102
      Height = 13
      Caption = '���������������� '
      Layout = tlCenter
    end
    object m_L_address: TLabel
      Left = 78
      Top = 64
      Width = 31
      Height = 13
      Caption = '�����'
    end
    object m_L_inn: TLabel
      Left = 416
      Top = 9
      Width = 24
      Height = 13
      Caption = '���'
      Layout = tlCenter
    end
    object m_L_kpp: TLabel
      Left = 613
      Top = 9
      Width = 23
      Height = 13
      Caption = '���'
      Layout = tlCenter
    end
    object MLDBPanel1: TMLDBPanel
      Left = 117
      Top = 34
      Width = 242
      Height = 22
      DataField = 'NAME'
      DataSource = m_DS_org
      Alignment = taLeftJustify
      BevelWidth = 0
      BorderStyle = bsSingle
      Color = clBtnFace
      UseDockManager = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 0
    end
    object m_LV_taxpayer: TMLLovListView
      Left = 117
      Top = 6
      Width = 285
      Height = 22
      Lov = m_LOV_taxpayer
      UpDownWidth = 329
      UpDownHeight = 121
      Interval = 500
      TabStop = True
      TabOrder = 1
      ParentColor = False
    end
    object m_DBE_inn: TDBEdit
      Left = 446
      Top = 6
      Width = 154
      Height = 21
      TabStop = False
      AutoSize = False
      Color = clBtnFace
      DataField = 'INN'
      DataSource = m_DS_taxpayer_fict
      TabOrder = 2
    end
    object m_DBE_kpp: TDBEdit
      Left = 643
      Top = 6
      Width = 154
      Height = 21
      TabStop = False
      AutoSize = False
      Color = clBtnFace
      DataField = 'KPP'
      DataSource = m_DS_taxpayer_fict
      TabOrder = 3
    end
    object m_DBM_address: TDBMemo
      Left = 117
      Top = 61
      Width = 681
      Height = 22
      TabStop = False
      Color = clBtnFace
      DataField = 'SHOW_ADDRESS'
      DataSource = m_DS_address
      TabOrder = 4
    end
  end
  object m_DBG_main1: TMLDBGrid [4]
    Left = 0
    Top = 118
    Width = 814
    Height = 430
    Align = alClient
    DataSource = m_DS_main
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterColor = clWindow
    UseMultiTitle = True
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking]
    OnGetCellParams = m_DBG_main1GetCellParams
    Columns = <
      item
        FieldName = 'NUM'
        Title.TitleButton = True
        Footers = <>
      end
      item
        Alignment = taCenter
        FieldName = 'CHECK_DATE'
        Title.TitleButton = True
        Width = 110
        Footers = <>
      end
      item
        FieldName = 'BAR_CODE'
        Title.TitleButton = True
        Width = 110
        Footers = <>
      end
      item
        FieldName = 'NMCL_ID'
        Title.TitleButton = True
        Visible = False
        Footers = <>
      end
      item
        FieldName = 'NMCL_NAME'
        Title.TitleButton = True
        Width = 500
        Footers = <>
      end
      item
        FieldName = 'ALCO_CODE'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'CAPACITY'
        Title.TitleButton = True
        Footers = <>
      end
      item
        FieldName = 'QUANTITY'
        Title.TitleButton = True
        Footer.ValueType = fvtSum
        Footer.FieldName = 'QUANTITY'
        Footers = <>
      end>
  end
  inherited m_ACTL_main: TActionList
    object m_ACT_complete: TAction
      Category = 'Custom'
      Caption = '���������'
      Hint = '���������� ������ ���������'
      ImageIndex = 11
    end
    object m_ACT_uncomplete: TAction
      Category = 'Custom'
      Caption = '�������� ����������'
      Hint = '�������� ����������'
      ImageIndex = 0
    end
    object m_ACT_save: TAction
      Caption = '��������� reg'
      Hint = '��������� reg'
      ImageIndex = 1
    end
    object m_ACT_save_dll: TAction
      Caption = '��������� dll'
    end
    object m_ACT_set_begin_end_date: TMLSetBeginEndDate
      Category = 'ML'
      Caption = '� 05.06.2002 �� 05.06.2002'
      OnSet = m_ACT_set_begin_end_dateSet
      BeginDate = 41766
      EndDate = 41766
      MinBeginDate = 1
      MaxBeginDate = 100000
      MinEndDate = 1
      MaxEndDate = 100000
      SetCaption = True
    end
    object m_ACT_generate: TAction
      Caption = '������������ �����'
      Hint = '������������ �����'
    end
    object m_ACT_reload_list: TAction
      Caption = '���������� �� ������� ����'
      Hint = '���������� �� ������� ����'
    end
    object m_ACT_print_form: TAction
      Category = 'Single'
      Caption = '������'
      Hint = '������'
      ImageIndex = 12
      OnExecute = m_ACT_print_formExecute
    end
  end
  inherited m_ACTL_main_add: TActionList
    Left = 480
    Top = 0
    inherited m_ACT_xlsx: TAction
      Visible = True
    end
  end
  inherited m_PM_main_add: TPopupMenu
    Left = 144
    Top = 128
  end
  object m_DS_main: TDataSource
    DataSet = DAlcoJournalNew.m_Q_main
    Left = 24
    Top = 64
  end
  object m_DS_taxpayer: TDataSource
    DataSet = DAlcoJournalNew.m_Q_taxpayer
    Left = 248
    Top = 32
  end
  object m_LOV_taxpayer: TMLLov
    DataFieldKey = 'ID'
    DataFieldValue = 'NAME'
    DataSource = m_DS_taxpayer_fict
    DataFieldKeys = 'ID'
    DataFieldValues = 'NAME'
    DataSourceList = m_DS_taxpayer
    Mappings.Strings = (
      'INN=INN'
      'KPP=KPP')
    AutoOpenList = True
    OnAfterApply = m_LOV_taxpayerAfterApply
    Left = 288
    Top = 32
  end
  object m_DS_taxpayer_fict: TDataSource
    DataSet = DAlcoJournalNew.m_Q_taxpayer_fict
    Left = 216
    Top = 32
  end
  object m_DS_org: TDataSource
    DataSet = DAlcoJournalNew.m_Q_org
    Left = 329
    Top = 58
  end
  object m_DS_address: TDataSource
    DataSet = DAlcoJournalNew.m_Q_address
    Left = 385
    Top = 82
  end
end
